(function() {
    var app = angular.module('eSahai', ["ui.router", "ui.bootstrap", "ngMap","angularjs-dropdown-multiselect","angular-geohash","firebase"]);
    app.config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/Home');

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'loginCtrl'
            })
            .state('Home', {
                url: '/Home',
                templateUrl: 'templates/home.html',
                controller: 'homeCtrl'
            })
            .state('User-Type',{
                url: '/User-Type',
                templateUrl: 'templates/userType.html',
                controller: 'userTypeCtrl'
            })
            .state('New-User-Type',{
                url: '/New-User-Type',
                templateUrl: 'templates/userTypeForm.html',
                controller: 'userTypeCtrl'
            })
            .state('Update-User-Type',{
                url: '/Update-User-Type',
                templateUrl: 'templates/userTypeForm.html',
                controller: 'userTypeCtrl'
            })
            .state('Users', {
                url: '/Users',
                templateUrl: 'templates/users.html',
                controller: 'usersCtrl'
            })
            .state('View-User', {
                url: '/View-User',
                templateUrl: 'templates/userFormView.html',
                controller: 'usersCtrl'
            })
            .state('New-User', {
                url: '/New-User',
                templateUrl: 'templates/userForm.html',
                controller: 'usersCtrl'
            })
            .state('Update-Users', {
                url: '/Update-User',
                templateUrl: 'templates/userForm.html',
                controller: 'usersCtrl'
            })
            .state('Group-Type',{
                url: '/Group-Type',
                templateUrl: 'templates/groupType.html',
                controller: 'groupTypeCtrl'
            })
            .state('New-Group-Type',{
                url: '/New-Group-Type',
                templateUrl: 'templates/groupTypeForm.html',
                controller: 'groupTypeCtrl'
            })
            .state('Update-Group-Type',{
                url: '/Update-Group-Type',
                templateUrl: 'templates/groupTypeForm.html',
                controller: 'groupTypeCtrl'
            })
            .state('Emergency-Type',{
                url: '/Emergency-Type',
                templateUrl: 'templates/emergencyType.html',
                controller: 'emergencyTypeCtrl'
            })
            .state('New-Emergency-Type',{
                url: '/New-Emergency-Type',
                templateUrl: 'templates/emergencyTypeForm.html',
                controller: 'emergencyTypeCtrl'
            })
            .state('Update-Emergency-Type',{
                url: '/Update-Emergency-Type',
                templateUrl: 'templates/emergencyTypeForm.html',
                controller: 'emergencyTypeCtrl'
            })
            .state('Vehicle-Type',{
                url: '/Vehicle-Type',
                templateUrl: 'templates/vehicleType.html',
                controller: 'vehicleTypeCtrl'
            })
            .state('New-Vehicle-Type',{
                url: '/New-Vehicle-Type',
                templateUrl: 'templates/vehicleTypeForm.html',
                controller: 'vehicleTypeCtrl'
            })
            .state('Update-Vehicle-Type',{
                url: '/Update-Vehicle-Type',
                templateUrl: 'templates/vehicleTypeForm.html',
                controller: 'vehicleTypeCtrl'
            })
            .state('Group-Master',{
                url: '/Group-Master',
                templateUrl: 'templates/groupMaster.html',
                controller: 'groupMasterCtrl'
            })
            .state('View-Group-Master',{
                url: '/View-Group-Master',
                templateUrl: 'templates/groupMasterFormView.html',
                controller: 'groupMasterCtrl'
            })
            .state('New-Group-Master',{
                url: '/New-Group-Master',
                templateUrl: 'templates/groupMasterForm.html',
                controller: 'groupMasterCtrl'
            })
            .state('Update-Group-Master',{
                url: '/Update-Group-Master',
                templateUrl: 'templates/groupMasterForm.html',
                controller: 'groupMasterCtrl'
            })
            .state('Ambulance-Master',{
                url: '/Ambulance-Master',
                templateUrl: 'templates/ambulanceMaster.html',
                controller: 'ambulanceMasterCtrl'
            })
            .state('View-Ambulance-Master',{
                url: '/View-Ambulance-Master',
                templateUrl: 'templates/ambulanceMasterFormView.html',
                controller: 'ambulanceMasterCtrl'
            })
            .state('New-Ambulance-Master',{
                url: '/New-Ambulance-Master',
                templateUrl: 'templates/ambulanceMasterForm.html',
                controller: 'ambulanceMasterCtrl'
            })
            .state('Update-Ambulance-Master',{
                url: '/Update-Ambulance-Master',
                templateUrl: 'templates/ambulanceMasterForm.html',
                controller: 'ambulanceMasterCtrl'
            })
            .state('Customer-Master',{
                url: '/Customer-Master',
                templateUrl: 'templates/customerMaster.html',
                controller: 'customerMasterCtrl'
            })
            .state('View-Customer-Master',{
                url: '/View-Customer-Master',
                templateUrl: 'templates/customerMasterFormView.html',
                controller: 'customerMasterCtrl'
            })
            .state('New-Customer-Master',{
                url: '/New-Customer-Master',
                templateUrl: 'templates/customerMasterForm.html',
                controller: 'customerMasterCtrl'
            })
            .state('Update-Customer-Master',{
                url: '/Update-Customer-Master',
                templateUrl: 'templates/customerMasterForm.html',
                controller: 'customerMasterCtrl'
            })
            .state('Driver-Master',{
                url: '/Driver-Master',
                templateUrl: 'templates/driverMaster.html',
                controller: 'driverMasterCtrl'
            })
            .state('View-Driver-Master',{
                url: '/View-Driver-Master',
                templateUrl: 'templates/driverMasterFormView.html',
                controller: 'driverMasterCtrl'
            })
            .state('New-Driver-Master',{
                url: '/New-Driver-Master',
                templateUrl: 'templates/driverMasterForm.html',
                controller: 'driverMasterCtrl'
            })
            .state('Update-Driver-Master',{
                url: '/Update-Driver-Master',
                templateUrl: 'templates/driverMasterForm.html',
                controller: 'driverMasterCtrl'
            })
            .state('Map',{
                url: '/Map',
                templateUrl: 'templates/map.html',
                controller: 'mapCtrl'
            })
            .state('Booking',{
                url: '/Booking',
                templateUrl: 'templates/booking.html',
                controller: 'bookingCtrl'
            })
            .state('New-Booking',{
                url: '/New-Booking',
                templateUrl: 'templates/bookingForm.html',
                controller: 'bookingCtrl'
            })
            .state('Update-Booking',{
                url: '/Update-Booking',
                templateUrl: 'templates/bookingForm.html',
                controller: 'bookingCtrl'
            })
            .state('Assign-Booking',{
                url: '/Assign-Booking',
                templateUrl: 'templates/assignBooking.html',
                controller: 'bookingCtrl'
            })
            .state('Ambulances',{
                url: '/Ambulances',
                templateUrl: 'templates/ambulances.html',
                controller: 'ambulancesCtrl'
            })
            .state('Customer-Attributes',{
                url: '/Customer-Attributes',
                templateUrl: 'templates/customerAttributes.html',
                controller: 'customerAttributesCtrl'
            })
            .state('New-Customer-Attribute',{
                url: '/New-Customer-Attribute',
                templateUrl: 'templates/customerAttributesForm.html',
                controller: 'customerAttributesCtrl'
            })
            .state('Update-Customer-Attribute',{
                url: '/Update-Customer-Attribute',
                templateUrl: 'templates/customerAttributesForm.html',
                controller: 'customerAttributesCtrl'
            })
            .state('Ambulance-Attributes',{
                url: '/Ambulance-Attributes',
                templateUrl: 'templates/ambulanceAttributes.html',
                controller: 'ambulanceAttributesCtrl'
            })
            .state('New-Ambulance-Attribute',{
                url: '/New-Ambulance-Attribute',
                templateUrl: 'templates/ambulanceAttributesForm.html',
                controller: 'ambulanceAttributesCtrl'
            })
            .state('Update-Ambulance-Attribute',{
                url: '/Update-Ambulance-Attribute',
                templateUrl: 'templates/ambulanceAttributesForm.html',
                controller: 'ambulanceAttributesCtrl'
            })
            .state('Group-Attributes',{
                url: '/Group-Attributes',
                templateUrl: 'templates/groupAttributes.html',
                controller: 'groupAttributesCtrl'
            })
            .state('New-Group-Attribute',{
                url: '/New-Group-Attribute',
                templateUrl: 'templates/groupAttributesForm.html',
                controller: 'groupAttributesCtrl'
            })
            .state('Update-Group-Attribute',{
                url: '/Update-Group-Attribute',
                templateUrl: 'templates/groupAttributesForm.html',
                controller: 'groupAttributesCtrl'
            })
            .state('SMS-Settings',{
                url: '/SMS-Settings',
                templateUrl: 'templates/smsSettings.html',
                controller: 'smsSettingsCtrl'
            })
            .state('Rate-Card',{
                url: '/Rate-Card',
                templateUrl: 'templates/rateCard.html',
                controller: 'rateCardCtrl'
            })
            .state('APP-Version', {
                url: '/APP-Version',
                templateUrl: 'templates/appVersion.html',
                controller: 'appVersionCtrl'
            })
            .state('Change-Password', {
                url: '/Change-Password',
                templateUrl: 'templates/changePwd.html',
                controller: 'changePwdCtrl'
            })
            .state('Terms-And-Conditions',{
                url: '/Terms-And-Conditions',
                templateUrl: 'templates/termsNcondi.html',
                controller: 'termsNcondiCtrl'
            })
            .state('Add-New-Terms',{
                url: '/Add-New-Terms',
                templateUrl: 'templates/NewTermsNcondi.html',
                controller: 'termsNcondiCtrl'
            })
            .state('Update-Terms&Conditions',{
                url: '/Update-Terms&Conditions',
                templateUrl: 'templates/NewTermsNcondi.html',
                controller: 'termsNcondiCtrl'
            })
            .state('Command-Center',{
                url: '/Command-Center',
                templateUrl: 'templates/CommandCenter.html',
                controller: 'commandCenterCtrl'
            })
            .state('PN-Logs',{
                url: '/PN-Logs',
                templateUrl: 'templates/pnLogs.html',
                controller: 'pnLogsCtrl'
            })
            .state('SMS-Logs',{
                url: '/SMS-Logs',
                templateUrl: 'templates/smsLogs.html',
                controller: 'smsLogsCtrl'
            })
            .state('FAQ', {
                url: '/FAQ',
                templateUrl: 'templates/faq.html',
                controller: 'faqCtrl'
            })
            .state('Add-FAQ', {
                url: '/Add-FAQ',
                templateUrl: 'templates/newFaqForm.html',
                controller: 'faqCtrl'
            })
            .state('Update-FAQ', {
                url: '/Update-FAQ',
                templateUrl: 'templates/newFaqForm.html',
                controller: 'faqCtrl'
            })
            .state('Privacy-And-Policies', {
                url: '/Privacy-And-Policies',
                templateUrl: 'templates/privacyNpolicies.html',
                controller: 'privacyNpoliciesCtrl'
            })
            .state('Add-New-Policies', {
                url: '/Add-New-Policies',
                templateUrl: 'templates/NewPrivacyNpolicies.html',
                controller: 'privacyNpoliciesCtrl'
            })
            .state('Update-Privacy&Policies', {
                url: '/Update-Privacy&Policies',
                templateUrl: 'templates/NewPrivacyNpolicies.html',
                controller: 'privacyNpoliciesCtrl'
            })
            .state('Come-To-Know-By', {
                url: '/Come-To-Know-By',
                templateUrl: 'templates/comeToKnowBy.html',
                controller: 'comeToKnowByCtrl'
            })
            .state('Add-Come-To-Know-By', {
                url: '/Add-Come-To-Know-By',
                templateUrl: 'templates/newComeToKnowBy.html',
                controller: 'comeToKnowByCtrl'
            })
            .state('Update-Come-To-Know-By', {
                url: '/Update-Come-To-Know-By',
                templateUrl: 'templates/newComeToKnowBy.html',
                controller: 'comeToKnowByCtrl'
            })
            .state('Add-Country', {
                url: '/Add-Country',
                templateUrl: 'templates/countryListForm.html',
                controller: 'countryListCtrl'
            })
            .state('Country-List', {
                url: '/Country-List',
                templateUrl: 'templates/countryList.html',
                controller: 'countryListCtrl'
            })
            .state('Update-Country', {
                url: '/Update-Country',
                templateUrl: 'templates/countryListForm.html',
                controller: 'countryListCtrl'
            })
            .state('View-Country', {
                url: '/View-Country',
                templateUrl: 'templates/CountryFormView.html',
                controller: 'countryListCtrl'
            })
                /*status list and geo Hashes*/
                .state('Add-Status', {
                    url: '/Add-Status',
                    templateUrl: 'templates/statusListForm.html',
                    controller: 'statusListCtrl'
                })
                .state('Status-List', {
                    url: '/Status-List',
                    templateUrl: 'templates/statusList.html',
                    controller: 'statusListCtrl'
                })
                .state('Update-Status', {
                    url: '/Update-Status',
                    templateUrl: 'templates/statusListForm.html',
                    controller: 'statusListCtrl'
                })
                .state('View-Status', {
                    url: '/View-Status',
                    templateUrl: 'templates/statusFormView.html',
                    controller: 'statusListCtrl'
                })
            .state('Add-GeoHashes', {
                url: '/Add-GeoHashes',
                templateUrl: 'templates/geoHashesForm.html',
                controller: 'geoHashesCtrl'
            })
            .state('Geo-Hashes', {
                url: '/Geo-Hashes',
                templateUrl: 'templates/geoHashes.html',
                controller: 'geoHashesCtrl'
            })
            .state('Update-GeoHashes', {
                url: '/Update-GeoHashes',
                templateUrl: 'templates/geoHashesForm.html',
                controller: 'geoHashesCtrl'
            })
            .state('View-GeoHashes', {
                url: '/View-GeoHashes',
                templateUrl: 'templates/geoHashesFormView.html',
                controller: 'geoHashesCtrl'
            })
            .state('Feedback', {
                url: '/Feedback',
                templateUrl: 'templates/feedback.html',
                controller: 'feedbackCtrl'
            })
	    .state('Promo-Banners', {
                url: '/Promo-Banners',
                templateUrl: 'templates/promoBanner.html',
                controller: 'promoBannerCtrl'
            })
            .state('Update-Promo-Banner', {
                url: '/Update-Promo-Banner',
                templateUrl: 'templates/promoBannerForm.html',
                controller: 'promoBannerCtrl'
            })
            .state('New-Promo-Banner', {
                url: '/New-Promo-Banner',
                templateUrl: 'templates/promoBannerForm.html',
                controller: 'promoBannerCtrl'
            })
            .state('View-Promo-Banner', {
                url: '/View-Promo-Banner',
                templateUrl: 'templates/promoBannerView.html',
                controller: 'promoBannerCtrl'
            })

    });

    app.constant("TableConstants", {
        'system_users': 'system_users',
        'user_types': 'user_types',
        'group_types': 'group_types',
        'emergency_types': 'emergency_types',
        'vehicle_type': 'vehicle_type',
        'group_master': 'group_master',
        'ambulance_master': 'ambulance_master',
        'driver_master': 'driver_master',
        'customer_master': 'customer_master',
        'group_attributes': 'group_attributes',
        'customer_attributes': 'customer_attributes',
        'ambulance_attributes': 'ambulance_attributes',
        'emergency_booking': 'emergency_booking',
        'ambulance_driver': 'ambulance_driver',
        'tracking': 'tracking',
        'customer_e_types': 'customer_e_types',
        'ambulance_e_types': 'ambulance_e_types',
        'group_e_types': 'group_e_types',
        'transferBooking': 'transferBooking',
        'sms_details': 'sms_details',
        'rate_card': 'rate_card',
        'app_version': 'app_version',
        'terms_conditions': 'terms_conditions',
        'app_settings': 'app_settings',
        'pn_log': 'pn_log',
        'sms_log': 'sms_log',
        'faq': 'faq',
        'privacy_policy': 'privacy_policy',
        'come_to_know_by': 'come_to_know_by',
        'country_list': 'country_list',
	'status_list': 'status_list',
	'geo_hashes': 'geo_hashes',
        'feedback': 'feedback'

    });
        app.constant("statusConstants", {
            's01': 'NORESP',
            's02': 'NOAMB',
            's03': 'TC',
            's04': 'CANCFAKE',
            's05': 'CAN',
            's06': 'Pending',
            's07': 'NORESPTNSF',
            's08': 'ACTD',
            's09': 'BOOKING ACCEPTED',
            's10': 'WTEL',
            's11': 'AEL',
            's12': 'WBTD',
            's13': 'RD',
            's14': 'TC',
            's15': 'STARTED',
            's16': 'completed',
            's17': 'CAN',


            
            'sh01': 'No Response',
            'sh02': 'No Ambulance',
            'sh03': 'Trip Closed',
            'sh04': 'Cancelled (Fake)',
            'sh05': 'Cancelled',
            'sh06': 'Pending',
            'sh07': 'No Response on transfer',
            'sh08': 'ACCEPTED',
            'sh09': 'BOOKING ACCEPTED',
            'sh10': 'WAY TO EMERGENCY LOCATION',
            'sh11': 'AT EMERGENCY LOCATION',
            'sh12': 'WAY BACK TO DESTINATION',
            'sh13': 'REACHED DESTINATION',
            'sh14': 'TRIP CLOSED',
            'sh15': 'STARTED',
            'sh16': 'completed',
            'sh17': 'cancelled',


            'NORESP': 'No Response',
            'NOAMB': 'No Ambulance',
            'TC': 'Trip Closed',
            'CANCFAKE': 'Cancelled (Fake)',
            'CAN': 'Cancelled',
            'Pending': 'Pending',
            'NORESPTNSF': 'No Response on transfer',
            'ACTD': 'Accepted',
            'BOOKING ACCEPTED': 'Booking Accepted',
            'WTEL': 'Way To Emergency Location',
            'AEL': 'At Emergency Location',
            'WBTD': 'WAY BACK TO Destination',
            'RD': 'Reached Destination',
            'STARTED': 'Started',
            'completed': 'Completed',
            'Not Booked Yet': 'Not Booked Yet'


        })

  
    app.config(function($provide) {
        $provide.decorator('$uiViewScroll', function($delegate) {
            return function(uiViewElement) {
                // var top = uiViewElement.getBoundingClientRect().top;
                // window.scrollTo(0, (top - 30));
                // Or some other custom behaviour...
            };
        });
    });
}());
