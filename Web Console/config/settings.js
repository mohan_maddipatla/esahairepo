(function() {
    var app = angular.module('eSahai');

    var request = function($http, secure) {
        var setup = {
            //'protocol': 'http',
            'protocol': 'https',
            //'host': '192.168.0.129',
            //'port': '3000',
            //'host': '54.200.233.193', // dev
            //'host' : '54.148.57.28',  // qa
            'host': 'prod.esahai.in',
            //'host': 'qa.esahai.in',
            'port': 'esahai',
            'prefix': 'db',
            'paths': {
                login: '/login',
                insertRecord: '/insertRecord',
                updateRecord: '/updateRecord',
                deleteRecord: '/deleteRecord',
                fetchDataByField: '/fetchDataByField',
                tableDesc: '/tableDesc',
                getDashboardDetails: '/getDashboardDetails',
                swagger: '/swagger',
                createBooking: '/createBooking',
                getAmbulanceList: '/getAmbulanceList',
                getNearByAmbulanceList: '/getNearByAmbulanceList',
                assignBooking: '/assignBooking',
                getAmbulanceDriverStatusList: '/getAmbulanceDriverStatusList',
                cancelBooking: '/cancelBooking',
                getAmbulanceTrackDetails: '/getAmbulanceTrackDetails',
                updateAmbulanceDriverStatus: '/updateAmbulanceDriverStatus',
                transferByCommandCentre: '/transferByCommandCentre',
                checkCustomerExistsOrNot: '/checkCustomerExistsOrNot',
                getAmbulanceTypes: '/getAmbulanceTypes',
                getNearByHospitalList: '/getNearByHospitalList',
                createBookingByCommandCentre: '/createBookingByCommandCentre',
                assignBookingByCommandCentre: '/assignBookingByCommandCentre',
                getNearByAmbulanceListSpecificType: '/getNearByAmbulanceListSpecificType',
                getDistanceNEta: '/getDistanceNEta',
                uploadExcel: '/uploadExcel',
                changePassword: '/changePassword',
                getGroupMasterList: '/getGroupMasterList',
                commonLimitOrActiveCheck: '/commonLimitOrActiveCheck',
		getStatusList: '/getStatusList',
                getCountryList:'/getCountryList',
	        getFeedbackList:'/getFeedbackList',
		uploadBanner:'/uploadBanner',
                getPromos: '/getPromos',
		updatePromoBanner:'/updateBanner'











            },
            'url': function(key) {
                if (setup.paths.hasOwnProperty(key)) {
                    /*return setup.protocol + '://' + setup.host + ':' + setup.port + '/' + setup.prefix + setup.paths[key]*/
                    return setup.protocol + '://' + setup.host + '/' + setup.port + '/' + setup.prefix + setup.paths[key]
                } else {
                    return 'invalid service'
                }
            }
        }

        // Making a service call
        var serviceCall = function(key, type, input, config, cb) {
            if (key != 'login') {
                config.headers['Content-Type'] = 'application/json';
            }
            if (type == 'delete') {
                console.log("Type : ", type, ' :: ', setup.url(key), ' :: ', input);
                input = JSON.stringify(input);
                $.ajax({
                    url: setup.url(key),
                    type: 'DELETE',
                    data: input,
                    headers: config.headers,
                    cache: false,
                    success: function(data) {
                        console.log("DELETE SUCCESS :: ", data);
                        cb(data);
                    },
                    error: function(err) {
                        console.log("ERROR DELETE EVENT :: ", err)
                        var data = { statusMessage: 'Server is down. Please try after some time.' }
                        cb(data);
                    }
                });

            } else {
                console.log(type, ' :: ', key, ' :: ', setup.url(key), ' :::: ', input, ' ::::: ', config);
                $http[type](setup.url(key), input, config)
                    .success(function(data) {
                        cb(data);
                    })
                    .error(function(err) {
                        console.log('ERROR :: ', err);
                        var data = { statusMessage: 'Server is down. Please try after some time.' }
                        cb(data);
                    });
            }
        }

        var setObj = function(key, data) {
            window.sessionStorage.setItem(secure.encode(key), secure.encode(JSON.stringify(data)))
        }
        var getObj = function(key) {
            var obj = window.sessionStorage.getItem(secure.encode(key));
            if (obj) {
                var obj = secure.decode(obj);
                try {
                    return JSON.parse(obj);
                } catch (e) {
                    return { distrub: true }
                }
            } else {
                return null;
            }
        }

        var setItem = function(key, data) {
            window.sessionStorage.setItem(secure.encode(key), secure.encode(data));
        }
        var getItem = function(key) {
            var item = window.sessionStorage.getItem(secure.encode(key));
            if (item) {
                return secure.decode(item);
            } else {
                return null;
            }
        }

        var removeItem = function(key) {
            window.sessionStorage.removeItem(secure.encode(key));
        }

        var sortNumber = function(a, b) {
            return a - b;
        }

        return {
            'service': serviceCall,
            'setObj': setObj,
            'getObj': getObj,
            'setItem': setItem,
            'getItem': getItem,
            'removeItem': removeItem,
            'setup': setup,
            'sort': sortNumber,
        }
    }
    app.factory('request', ['$http', 'secure', request]);

    app.directive('ckEditor', [function() {
        return {
            require: '?ngModel',
            link: function($scope, elm, attr, ngModel) {
                var ck = CKEDITOR.replace(elm[0]);

                ck.on('pasteState', function() {
                    $scope.$apply(function() {
                        ngModel.$setViewValue(ck.getData());
                    });
                });

                ngModel.$render = function(value) {
                    ck.setData(ngModel.$modelValue);
                };
            }
        };
    }]);



    app.service('ctrlComm', function() {
        var ctrlPocket = {};
        var put = function(key, value) {
            ctrlPocket[key] = value;
        };
        var get = function(key) {
            return ctrlPocket[key];
        };
        var del = function(key) {
            delete ctrlPocket[key];
        };
        return {
            put: put,
            get: get,
            del: del
        };
    });

    app.controller('confirmBoxCtrl', ['$scope', '$modalInstance', 'input', function($scope, $modalInstance, input) {
        $scope.input = input;
        $scope.ok = function() {
            $modalInstance.close(true);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }]);


    app.directive('fileModel', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function() {
                    scope.$apply(function() {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

    app.directive('callDate', function($timeout, $parse) {
        return {
            //scope: true,   // optionally create a child scope
            link: function(scope, element, attrs) {
                var model = $parse(attrs.callDate);
                scope.$watch(model, function(value) {
                    console.log("scope" + value)
                    if (value === true) {
                        $timeout(function() {
                            element[0].focus();
                        });
                    }
                });
            }
        };
    });


    app.service('fileUpload', ['$http', 'request', function($http, request) {
        this.uploadFileToUrl = function(file, key, obj, url, config, callType, cb) {
            var fd = new FormData();
            fd.append(key, file);
            fd.append('jsondata', JSON.stringify(obj))
            config.headers['Content-Type'] = undefined;
            $http[callType](request.setup.url(url), fd, {
                    transformRequest: angular.identity,
                    headers: config.headers
                })
                .success(function(data) {
                    if (cb) cb(data);
                    console.log("-------- success ", data);
                })
                .error(function(err) {
                    if (cb) cb(err);
                    console.log("-------- error ", err);
                });
        }
    }]);

    app.directive('exportFile', function() {
            return {
                restrict: 'A',
                link: function($scope, element, attrs) {
                    element.bind('click', function() {
                        var blob = new Blob([document.getElementById('exportable').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, attrs.exportFile + ".xls");
                    })
                }
            }
        })
        // app.filter('timeInShort',function(){
        //  return function (input){
        //      if(typeof input != 'string'){
        //          var d = new Date(input);
        //          var w = ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        //          var m = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sep","Oct","Nov","Dec"];
        //          if(d.getDate() < 10){ var day = '0'+parseInt(d.getDate());
        //          }else{ var day = d.getDate();
        //          }

    //          var yy = d.getFullYear().toString().substr(2,4);

    //          return yy+"'"+m[d.getMonth()]+'-'+day+' '+w[d.getDay()];;           
    //      }else{
    //          return input;
    //      }
    //  }
    // });

})();
