(function() {
    var app = angular.module('eSahai');
    var customerMasterCtrl = function($scope, $rootScope, request, ctrlComm, fileUpload, $filter, TableConstants, $timeout) {
        console.log("in customerMasterCtrl : " + TableConstants.customer_master);
        $scope.tabActive('customerMaster');
        console.log($scope.admin)
        if ($scope.admin.userType === "GROUP ADMIN") {
            window.location.hash = '#/Home';
        }
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.customer = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        /*    $(document).ready(function() {
                $("#startdate").datepicker({}).on('changeDate', function(selected) {
                    $scope.$watch('fromDate', function(oldDate,newDate) {
                        if(oldDate == ''){
                            $scope.fromDate = newDate;
                            selected.date = new Date(newDate)
                        }
                        var minDate = new Date(selected.date.valueOf());
                    $('#enddate').datepicker('setStartDate', minDate);
                    })
                    
                });
            });*/
        $(document).ready(function() {

            $('#enddate').datepicker('setStartDate', $scope.fromDate);
            $("#startdate").datepicker({}).on('changeDate', function(selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#enddate').datepicker('setStartDate', minDate);
            });


        });
        $scope.customerAttr = [];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};
        /** datepicker **/
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.dateformat = "yyyy-MM-dd";
        $scope.today();
        $scope.showcalendar = function($event) {
            $scope.showdp = true;
        };
        $scope.showdp = false;
        $scope.dtmax = new Date();
        /** end **/
        $scope.getCustomerMasterList = function(cb) {
            $scope.loader(true);
            var req = {};
            var dateFilter = {};
            req.tableName = TableConstants.customer_master;
            req.params = {};
            dateFilter.fieldName = 'created_date';
            dateFilter.startDate = $scope.fromDate;
            console.log("NEW DATE ", $scope.fromDate, $filter('date')(new Date(), 'yyyy-MM-dd'));
            if ($scope.fromDate != $filter('date')(new Date(), 'yyyy-MM-dd')) {
                dateFilter.endDate = $filter('date')(new Date(new Date($scope.toDate).getTime() + (24 * 60 * 60 * 1000)), 'yyyy-MM-dd');
            }
            req.dateFilter = dateFilter;
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    angular.forEach(response.Result, function(obj) {
                        if (obj.is_active == 'Yes' || obj.is_active == 'Active') {
                            obj.is_active = 'Active';
                        } else {
                            obj.is_active = 'InActive';
                        }
                    })
                    $scope.customerMasterList = response.Result;
                    $scope.totalItems = $scope.customerMasterList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }

        if (window.location.hash == "#/Customer-Master") {
            $scope.getCustomerMasterList();
        } else if (window.location.hash == "#/New-Customer-Master") {
            $scope.page.title = 'Add Customer';
            $scope.page.type = 'post';
            $scope.customer.device_type = 'Android';
            $scope.customer.country_code = '+91';
            //document.getElementById("imageName").src = 'images/upload.jpg';
        } else if (window.location.hash == "#/Update-Customer-Master") {
            $scope.page.title = 'Update Customer';
            $scope.page.type = 'put';
            console.log(ctrlComm.get('customerObj'))
            prePoppulateValues(ctrlComm.get('customerObj'));
        } else if (window.location.hash == "#/View-Customer-Master") {
            $scope.page.title = 'View Customer';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('customerObj'));
        }



        var addNewCustomerMaster = function() {
            window.location.hash = "#/New-Customer-Master";
        }

        var viewCustomerMaster = function(customer) {
            ctrlComm.put('customerObj', customer);
            window.location.hash = '#/View-Customer-Master';
        }

        $scope.addAttribute = function() {
            angular.forEach($scope.customerAttr, function(obj) {
                if (obj.flag == 'edit') {
                    obj.flag = undefined;
                }
                var temp = [];
                console.log(obj)
                angular.forEach($scope.customer_e_types_list, function(obj1) {
                    console.log(obj1)
                    if (obj.customer_e_type_id != obj1.id) {
                        //$scope.customer_e_types_list
                        temp.push(obj1);
                        //console.log(obj1)
                    }
                })
                $scope.customer_e_types_list = temp;
            })
            console.log($scope.customer_e_types_list)
            $scope.customerAttr.push({ 'customer_e_type_id': '', 'attribute_details': '' });
        }

        $scope.saveAttribute = function(attrObj, customer) {
            $scope.loader(true);
            if (attrObj.customer_e_type_id != '' && attrObj.customer_e_type_id != undefined && attrObj.attribute_details != '' && attrObj.attribute_details != undefined) {
                var req = {};
                req.tableName = TableConstants.customer_attributes;
                var params = {};
                angular.forEach($scope.customerAttributesList, function(obj) {
                    if (obj.id == attrObj.customer_e_type_id)
                        attrObj.attribute_name = obj.type_name;
                })
                params = angular.copy(attrObj);
                params.customer_id = customer.customer_id;

                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    console.log(response)
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(customer)
                        $scope.notification('Attribute saved successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            } else if (!attrObj.customer_e_type_id) {
                $scope.notification('Please select attibute', 'danger');
            } else if (!attrObj.attribute_details) {
                $scope.notification('Please enter attibute value', 'danger');
            }
        }

        $scope.updateAttribute = function(attrObj, customer) {
            if (attrObj.customer_e_type_id != '' && attrObj.customer_e_type_id != undefined && attrObj.attribute_details != '' && attrObj.attribute_details != undefined) {
                var req = {};
                req.tableName = TableConstants.customer_attributes;
                var params = {};
                angular.forEach($scope.customerAttributesList, function(obj) {
                    if (obj.id == attrObj.customer_e_type_id)
                        attrObj.attribute_name = obj.type_name;
                })
                params = angular.copy(attrObj);
                delete params['id'];
                delete params['flag'];
                var conditionalParams = {};
                conditionalParams.id = attrObj.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(customer)
                        $scope.notification('Attribute updated successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            } else if (!attrObj.customer_e_type_id) {
                $scope.notification('Please select attibute', 'danger');
            } else if (!attrObj.attribute_details) {
                $scope.notification('Please enter attibute value', 'danger');
            }
        }

   $scope.deleteAttribute = function(attrObj, customer) {
            console.log("customer",attrObj);
            var input = {
                text: 'Are you sure you want to delete',
                name: attrObj.attribute_name
            }
            $scope.confirmation(input, function() {
                confirmAttributeDelete(attrObj, customer);
            });
        }

        function confirmAttributeDelete(attrObj, customer) {      
	      if (attrObj.id) {
                var req = {};
                req.tableName = TableConstants.customer_attributes;
                var params = {};
                params.id = attrObj.id;
                req.params = params;
                request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (typeof response == 'string') {
                        response = JSON.parse(response)
                    }
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(customer);
                        $scope.notification('Attribute deleted successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)
                    }
                })

            } else {
                var temp = []
                angular.forEach($scope.customerAttr, function(obj) {
                    if (obj.id != undefined) {
                        temp.push(obj)
                    }
                })
                $scope.customerAttr = temp;
            }
        }

        $scope.editAttribute = function(attrObj, customer) {
            //console.log(JSON.stringify(attrObj))
            var temp = [];
            var arr = [];
            var a = [];
            angular.forEach($scope.customerAttr, function(obj) {
                a.push(obj.customer_e_type_id)

            })
            angular.forEach($scope.customerAttr, function(obj) {
                if (obj.flag == 'edit') {
                    obj.flag = undefined;
                }

                angular.forEach($scope.global_customer_e_types_list, function(obj1) {
                    if (obj.customer_e_type_id != obj1.id) {
                        if (arr.indexOf(obj1.id) == -1 && a.indexOf(obj1.id) == -1) {
                            temp.push(obj1);
                            arr.push(obj1.id);
                        }

                    }

                });
            })

            angular.forEach($scope.global_customer_e_types_list, function(obj) {
                if (attrObj.customer_e_type_id == obj.id) {
                    temp.push(obj);
                }
            })

            $scope.customer_e_types_list = temp;
            angular.forEach($scope.customerAttr, function(obj) {
                if (obj.id == attrObj.id) {
                    obj.flag = 'edit';
                }
            })
        }

        function prePoppulateAttrValues(customer, cb) {
            var req = {};
            req.tableName = TableConstants.customer_attributes;
            req.params = {};
            req.params.customer_id = customer.customer_id;
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.customerAttr = response.Result;
                    console.log(angular.toJson($scope.customerAttr));
                    var req = {};
                    req.tableName = TableConstants.customer_e_types;
                    req.params = {};
                    console.log(angular.toJson(req));
                    request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                        $scope.loader(false);
                        if (response.statusCode == '300') {
                            $scope.customer_e_types_list = response.Result;
                            $scope.global_customer_e_types_list = response.Result;
                        }
                    })

                    if (cb) {
                        cb()
                    }
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                }
            })
        }

        function prePoppulateValues(customer) {
            console.log("customer" + angular.toJson(customer))
            if (customer) {
                if ($scope.page.type == 'put') {
                    customer.is_active == 'Active' ? customer.is_active = true : customer.is_active = false;
                    /*if (document.getElementById('imageName') != undefined) {
                        document.getElementById("imageName").src = customer.file_url;
                    }*/
                }
                $scope.customer = customer;
                prePoppulateAttrValues(customer, function() {})
            } else {
                window.location.hash = "#/Customer-Master";
            }
        }

        $scope.chooseFile = function() {
            document.getElementById("uploadFile").click();
        }

        $('#uploadFile').change(function() {
            readURL();
        })

        function readURL() {
            var file = $('#uploadFile')[0].files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imageName').attr('src', e.target.result);
                }

                reader.readAsDataURL(file);
            }
        }

        var validate = function(cb) {
            if (!$scope.customer.customer_name) {
                $scope.err.customer_name = true;
            } else {
                delete $scope.err.customer_name;
            }

            if ($scope.customer.customer_email) {
                if (!(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/).test($scope.customer.customer_email)) {
                    $scope.err.customer_email_cdn = true;

                } else {
                    delete $scope.err.customer_email;
                    delete $scope.err.customer_email_cdn;
                }
            }

            if (!$scope.customer.customer_mobile_number) {
                $scope.err.customer_mobile_number = true;
            } else {
	    	      // if  country code prefixed

                if($scope.customer.customer_mobile_number[0]==1 ||$scope.customer.customer_mobile_number[0]+$scope.customer.customer_mobile_number[1]==91){
                console.log("Ssssssssssss",$scope.customer.customer_mobile_number)

                if (!(/^\d{12}$/).test($scope.customer.customer_mobile_number)) {
                    /*|| ($scope.user.mobile.length)>10*/
                    $scope.err.customer_mobile_number_cdn = true;

                } else {
                    delete $scope.err.customer_mobile_number;
                    delete $scope.err.customer_mobile_number_cdn;
                    //delete $scope.err.mobile_cdn;

                }
                }else {
                if (!(/^\d{10}$/).test($scope.customer.customer_mobile_number)) {
                    /*|| ($scope.user.mobile.length)>10*/
                    $scope.err.customer_mobile_number_cdn = true;

                } else {
                    delete $scope.err.customer_mobile_number;
                    delete $scope.err.customer_mobile_number_cdn;
                    //delete $scope.err.mobile_cdn;

                }
            }
	}	
            /* if (Object.keys($scope.err).length == 0) {

                 if (document.getElementById("imageName").src.split('/')[document.getElementById("imageName").src.split('/').length - 1] == 'upload.jpg') {
                     $scope.notification("please upload photo", 'danger');
                 } else {
                     if (document.getElementById('uploadFile').files.length && (document.getElementById('uploadFile').files[0].size / 1000).toFixed(2) > 1000) {
                         $scope.notification("Image should not exceed 1MB", 'danger');
                     } else {
                         if (cb) cb();
                     }
                 }
             }*/
            if (Object.keys($scope.err).length == 0) {


                if (cb) cb();
            }
        }


        var saveCustomerMaster = function(customer) {
            validate(function() {
                $scope.loader(true);
                customer.is_active = 'Active';
                var req = {};
                req.tableName = TableConstants.customer_master;
                var params = {};
			      //for prefixing country code 

                console.log("saveCustomerMaster", customer.country_code)
                var a = customer.country_code.split('+')[1]
                console.log("a.a is" + a)
                customer['customer_mobile_number'] = a + customer['customer_mobile_number'];
                params = angular.copy(customer);;
                /*  if ($scope.image_url == undefined) {
                      $scope.image_url = $('#uploadFile')[0].files[0];
                  }*/
                req.params = params;
                delete req.params['country_code'];
                console.log(angular.toJson(req));
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);

                    if (data.statusCode == '300') {
                        $scope.notification('Customer created successfully');
                        window.location.hash = '#/Customer-Master';
                    } else if (data.statusCode == '222') {
                        if (data.statusMessage.search('customer_mobile_number') > 0) {
                            $scope.notification('Phone number already exists, Try with other', 'danger');
                        }
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }


        /*
        $scope.loader(true);
        customer.is_active = 'Active';
        var req = {};
        req.tableName = TableConstants.customer_master;
        var params = {};
                
        params = angular.copy(customer);;
        if ($scope.image_url == undefined) {
            $scope.image_url = $('#uploadFile')[0].files[0];
        }
        req.params = params;
        console.log(angular.toJson(req));

        fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'insertRecord', $scope.CONFIG, 'post', function(data) {
            $scope.loader(false);
            if (data.statusCode == '300') {
                $scope.notification(data.statusMessage);
                window.location.hash = '#/Customer-Master';
            } else if (data.statusCode == '204') {
                $scope.notification(data.statusMessage, "danger");
                $timeout(function() {
                    $scope.logout();
                }, 2000)

            }
        });*/



        var updateCustomerMaster = function(customer) {
                validate(function() {
                    console.log(customer)
                    $scope.loader(true);
                    var req = {};
                    req.tableName = TableConstants.customer_master;
                    var params = {};
                    if (customer.is_active == true) {
                        customer.is_active = 'Active';
                    } else {
                        customer.is_active = 'InActive';
                    }
                    params = angular.copy(customer);
                    delete params['customer_id'];
                    delete params['country_code'];
                    var conditionalParams = {};
                    conditionalParams.customer_id = customer.customer_id;
                    req.params = params;
                    req.conditionalParams = conditionalParams;
                    console.log(angular.toJson(req))
                    request.service('updateRecord', 'put', req, $scope.CONFIG, function(data) {
                        $scope.loader(false);
                        if (data.statusCode == '300') {
                            $scope.notification('Customer updated successfully');
                            window.location.hash = '#/Customer-Master';
                        } else if (data.statusCode == '222') {
                            if (data.statusMessage.search('customer_mobile_number') > 0) {
                                $scope.notification('Phone number already exists, Try with other', 'danger');
                            }

                        } else if (data.statusCode == '204') {
                            $scope.notification(data.statusMessage, "danger");
                            $timeout(function() {
                                $scope.logout();
                            }, 2000)

                        }
                    })
                })
            }
            /*});*/

        /* $scope.loader(true);
         var req = {};
         req.tableName = TableConstants.customer_master;
         var params = {};
         if (customer.is_active == true) {
             customer.is_active = 'Active';
         } else {
             customer.is_active = 'InActive';
         }
         params = angular.copy(customer);
         delete params['customer_id'];
         var conditionalParams = {};
         conditionalParams.customer_id = customer.customer_id;
         req.params = params;
         req.conditionalParams = conditionalParams;
         fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'updateRecord', $scope.CONFIG, 'put', function(data) {
             console.log(data)
             $scope.loader(false);
             if (data.statusCode == '300') {
                 $scope.notification(data.statusMessage);
                 window.location.hash = '#/Customer-Master';
             } else if (data.statusCode == '204') {
                 $scope.notification(data.statusMessage, "danger");
                 $timeout(function() {
                     $scope.logout();
                 }, 2000)

             }
         });*/



        var editCustomerMaster = function(customer) {
            ctrlComm.put('customerObj', customer);
            window.location.hash = "#/Update-Customer-Master";
        }


        var cancelCustomerMaster = function() {
            window.location.hash = "#/Customer-Master";
        }

        var deleteCustomerMaster = function(customer) {
            var input = {
                text: 'Are you sure you want to delete',
                name: customer.display_name
            }
            $scope.confirmation(input, function() {
                confirmDelete(customer);
            });
        }

        function confirmDelete(customer) {
            /*console.log(customer)
            var req = {};
            req.tableName = TableConstants.customer_master;
            var params = {};
            params.customer_id = customer.customer_id;
            req.params = params;
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == '300') {
                    $scope.getCustomerMasterList(function() {
                        $scope.notification(response.statusMessage);
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else if (response.statusCode == '400' && response.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record", "danger");
                }
            })*/
            var req = {};
            req.tableName = TableConstants.customer_master;
            var params = {};
            params.is_active = 'InActive'
            var conditionalParams = {};
            conditionalParams.customer_id = customer.customer_id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                /*$scope.loader(false);*/
                if (response.statusCode == '300') {
                    $scope.getCustomerMasterList(function() {
                        $scope.notification("Customer Deleted Successfully");
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })


        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var importExcelDoc = function() {
            var file = $scope.myFile;
            if (file && (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/vnd.ms-excel")) {
                $scope.loader(true);
                var obj = {};
                fileUpload.uploadFileToUrl(file, 'customer_master', obj, 'uploadExcel', $scope.CONFIG, 'post', function(data) {
                    $scope.loader(false);
                    console.log("data", data);
                    if (data.statusCode == '300') {
                        $('#uploadFile').val("");
                        $scope.notification('File uploaded successfully.', 'success');
                        $scope.getCustomerMasterList(function() {
                            $scope.totalItems = $scope.customerMasterList.length;
                            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                $scope.tableRows.push($scope.totalItems);
                            if ($scope.totalItems != 0) {
                                $scope.tableRows = $scope.tableRows.sort(request.sort);
                                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                if ($scope.tableRows[1]) {
                                    $scope.viewby = $scope.tableRows[1];
                                } else {
                                    $scope.viewby = $scope.tableRows[0];
                                }
                                $scope.setItemsPerPage($scope.viewby);
                            }
                        });
                        $scope.upldfile = {};
                    } else if (data.statusCode == '446') {
                        $('#uploadFile').val("");
                        $scope.notification(data.statusMessage, 'danger');
                        $scope.getCustomerMasterList();
                        $scope.upldfile = {};


                    }
                });
            } else {
                $scope.notification('Upload excel spreadsheet only(.xls or .xlsx). ', 'info');
            }
        }

        $scope.fileUpLoad = function() {
            $scope.upldfile = {};
            document.getElementById("uploadFile").click();
        }
        $scope.catchUploadedFile = function() {
            var f = document.getElementById('uploadFile').files[0];
            if (f) {
                $scope.$apply(function() {
                    $scope.upldfile = {
                        name: f.name,
                        size: (f.size / 1024).toFixed(2) + ' KB'
                    }
                });
            }
        }



        $scope.cancelImport = function() {
            $scope.upldfile = {};
            $('#uploadFile').val("");
        }

        $scope.importExcelDoc = importExcelDoc;





        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewCustomerMaster = addNewCustomerMaster;
        $scope.saveCustomerMaster = saveCustomerMaster;
        $scope.updateCustomerMaster = updateCustomerMaster;
        $scope.deleteCustomerMaster = deleteCustomerMaster;
        $scope.cancelCustomerMaster = cancelCustomerMaster;
        $scope.editCustomerMaster = editCustomerMaster;
        $scope.viewCustomerMaster = viewCustomerMaster;
    }
    app.controller('customerMasterCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'fileUpload', '$filter', 'TableConstants', '$timeout', customerMasterCtrl]);
}());
