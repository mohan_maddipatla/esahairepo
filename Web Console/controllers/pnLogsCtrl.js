(function() {
    var app = angular.module('eSahai');
    var pnLogsCtrl = function($scope, $rootScope, request, ctrlComm, fileUpload, $filter, TableConstants, $timeout) {
        console.log("in pnLogsCtrl : " + TableConstants.pn_log);
        $scope.tabActive('pn', 'logs');
        console.log($scope.admin)
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.customer = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.customerAttr = [];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};
        $(document).ready(function() {
            $('#enddate').datepicker('setStartDate', $scope.fromDate);
            $("#startdate").datepicker({}).on('changeDate', function(selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#enddate').datepicker('setStartDate', minDate);
            });
        });
        if ($scope.admin.userType === "GROUP ADMIN") {
            window.location.hash = '#/Home';
        }

        $scope.getPNLogs = function(cb) {
            $scope.loader(true);
            var req = {};
            var dateFilter = {};
            req.tableName = TableConstants.pn_log;
            req.params = {};
            dateFilter.fieldName = 'created_datetime';
            dateFilter.startDate = $scope.fromDate;
            //            dateFilter.endDate = $scope.toDate;
            if ($scope.fromDate != $filter('date')(new Date(), 'yyyy-MM-dd'))
                dateFilter.endDate = $scope.toDate;

            req.dateFilter = dateFilter;
            var orderByParams = {};
            orderByParams.created_datetime = '';
            req.orderByParams = orderByParams;
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.pnLogsList = response.Result;
                    $scope.totalItems = $scope.pnLogsList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }

        if (window.location.hash == "#/PN-Logs") {
            $scope.getPNLogs();
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;
    }
    app.controller('pnLogsCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'fileUpload', '$filter', 'TableConstants', '$timeout', pnLogsCtrl]);
}());
