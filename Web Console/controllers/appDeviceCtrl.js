(function() {
    var app = angular.module('eSahai');
    var appDeviceCtrl = function($scope, $rootScope, request, ctrlComm, $http, TableConstants) {
        console.log("in app device ctrl");
        $scope.appMaster = {};
        $scope.err = {};
        $scope.page = {};

        $scope.tabActive('addDevice');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.tableRows = [5, 10, 15, 20, 30, 40]
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }


        function validate(cb) {
            // var file = document.getElementById('appIcon').files[0];
            // if(!file){ $scope.err.appIcon = true; }else{ delete $scope.err.appIcon;
            //  $scope.appMaster.appIcon = new FormData();
            //  $scope.appMaster.appIcon.append('UploadAppIcon',file);
            // }
            if (!$scope.appMaster.appId) { $scope.err.appId = true; } else { delete $scope.err.appId; }
            if (!$scope.appMaster.appName) { $scope.err.appName = true; } else { delete $scope.err.appName; }
            if (!$scope.appMaster.topicName) { $scope.err.topicName = true; } else { delete $scope.err.topicName; }
            if (!$scope.appMaster.gcmCert) { $scope.err.gcmCert = true; } else { delete $scope.err.gcmCert; }
            if (!$scope.appMaster.appleCert) { $scope.err.appleCert = true; } else { delete $scope.err.appleCert; }
            if (!$scope.appMaster.mapId) { $scope.err.mapId = true; } else { delete $scope.err.mapId; }
            if (Object.keys($scope.err).length == 0) {
                if (cb) cb();
            }
        }

        var getAppDevices = function(cb) {
            var req = {};
            req.tableName = TableConstants.appDeviceModule;
            req.params = {};
            req.token = $scope.admin['token'];
            req.userId = $scope.admin['userId'];
            request.service('fetchDataByField', 'post', req, function(response) {
                console.log(response)
                    /*$http.post('http://192.168.0.129:3001/Retailer/fetchWholeData', { 'tableName': 'deviceTable' }).success(function(response) {*/
                if (response.statusCode == '200') {
                    $scope.devicesList = response.Result;
                    $scope.totalItems = $scope.devicesList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                }
            });
        }

        $scope.getAppsList(function() {
            console.log($scope.appsList)
        })

        var hash = window.location.hash;

        if (hash == "#/APP-Device") {
            getAppDevices();
        } else if (hash == "#/New-APP-Device") {
            $scope.page.title = 'Add New Device'
            $scope.page.type = 'post'
        } else if (hash == "#/Update-APP-Device") {
            $scope.page.title = 'Update Device'
            $scope.page.type = 'put'
        }

        $scope.getUsersByAppId = function(appId) {
            console.log(appId)
            var req = {};
            req.tableName = 'app_user'
            req.params = {
                'appId': appId
            };
            req.adminURL = true;
            req.userId = $scope.admin['userId'];
            req.token = $scope.admin['token'];
            request.service('fetchDataByField', 'post', req, function(response) {
                if (response.statusCode == '200') {
                    $scope.usersList = response.Result;
                }
            })
        }


        var addNewDevice = function() {
            window.location.hash = "#/New-APP-Device"
        }

        var saveDevice = function(device) {
            console.log(device)
            var req = {};
            req.tableName = 'deviceTable';
            var params = {};
            params.appId = device.appId;
            params.userId = device.userId;
            params.deviceId = device.deviceId;
            params.deviceType = device.deviceType;
            params.deviceOs = device.deviceOs;
            params.deviceIMEI = device.deviceIMEI;
            params.deviceNetwork = device.deviceNetwork;
            params.deviceModel = device.deviceModel;
            params.screenSize = device.screenSize;
            params.gcmToken = device.gcmToken;
            if (device.isOnline == true) {
                params.isOnline = 'Yes';
            } else {
                params.isOnline = 'No';
            }
            if (device.isActive == true) {
                params.isActive = 'Yes';
            } else {
                params.isActive = 'No';
            }
            if (device.allowNotification == true) {
                params.allowNotification = 'Yes';
            } else {
                params.allowNotification = 'No';
            }
            if (device.allowLocation == true) {
                params.allowLocation = 'Yes';
            } else {
                params.allowLocation = 'No';
            }
            req.params = params;
            req.token = $scope.admin['token'];
            req.userId = $scope.admin['userId'];

            console.log(angular.toJson(req))

            request.service('insertRecord', 'post', req, function(response) {
                    if (response.statusCode == '200') {
                        window.location.hash = "#/APP-Device"
                        $scope.notification(response.statusMessage);
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
                /*validate(function(){
                    $scope.appMaster.userAccessId = $scope.admin.userAccessId;
                    $scope.appMaster.status = ($scope.appMaster.status) ? 'Yes': 'No' ;
                    $scope.loader(true);
                    request.service('appMaster',$scope.page.type,appMaster,function (data){
                        $scope.loader(false);
                        if(data.ResultCode >= 200 && data.ResultCode <=299){
                            window.location.hash="#/APP-Master"
                            $scope.notification(data.ReplyMessage);
                        }else{
                            $scope.notification(data.ReplyMessage);
                        }
                    });
                });*/
        }

        var updateDeviceForm = function(device) {
            $scope.loader(true);
            if (device.isOnline == true) {
                device.isOnline = 'Yes';
            } else {
                device.isOnline = 'No';
            }
            if (device.isActive == true) {
                device.isActive = 'Yes';
            } else {
                device.isActive = 'No';
            }
            if (device.allowNotification == true) {
                device.allowNotification = 'Yes';
            } else {
                device.allowNotification = 'No';
            }
            if (device.allowLocation == true) {
                device.allowLocation = 'Yes';
            } else {
                device.allowLocation = 'No';
            }
            var req = {};
            req.tableName = 'deviceTable';
            var params = {};
            params = angular.copy(device);
            delete params['deviceId'];
            req.params = params;
            var conditionalParams = {};
            conditionalParams.deviceId = device.deviceId;
            req.conditionalParams = conditionalParams;
            req.token = $scope.admin['token'];
            req.userId = $scope.admin['userId'];
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, function(response) {
                console.log(response)
                $scope.loader(false);
                if (response.statusCode == '200') {
                    window.location.hash = "#/APP-Device"
                    $scope.notification(response.statusMessage);
                } else {
                    $scope.notification(response.statusMessage);
                }

            })
        }

        var updateDevice = function(device) {
            console.log(device)

            ctrlComm.put('deviceObj', device);
            window.location.hash = "#/Update-APP-Device"
        }

        if (window.location.hash == "#/Update-APP-Device") {
            prePoppulateValues(ctrlComm.get('deviceObj'));
        }

        function prePoppulateValues(obj) {
            if (obj) {
                obj.appId = obj.appId.toString();
                $scope.getUsersByAppId(obj.appId);
                obj.userId = obj.userId.toString();
                obj.allowNotification = (obj.allowNotification == 'Yes') ? true : false;
                obj.isOnline = (obj.isOnline == 'Yes') ? true : false;
                obj.isActive = (obj.isActive == 'Yes') ? true : false;
                $scope.device = obj;
                // $scope.appMaster.is_active = (obj.is_active) ? true : false;
            } else {
                window.location.hash = "#/APP-Device"
            }
        }

        var cancelAddDevice = function() {
            window.location.hash = "#/APP-Device";
        }

        var deleteDevice = function(device) {
            var input = {
                text: 'Are you sure you want to delete',
                name: device.deviceId
            }
            $scope.confirmation(input, function() {
                confirmDelete(device);
            });
        }

        function confirmDelete(device) {
            var req = {};
            req.tableName = TableConstants.deviceModule
            var params = {};
            params.deviceId = device.deviceId;
            req.params = params;
            req.token = $scope.admin['token'];
            req.userId = $scope.admin['userId'];
            $scope.loader(true);
            request.service('deleteRecord', 'delete', req, function(response) {
                $scope.loader(false);
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }

                if (response.statusCode == '300') {
                    $scope.getAppDevices(function() {
                        $scope.notification(response.statusMessage);
                    })
                } else {
                    $scope.notification(response.statusMessage);
                }
            })
        }

        var chooseAppIcon = function() {
            delete $scope.err.appIcon;
            document.getElementById('appIcon').click();
        }

        var changeAppIcon = function(element) {
            if (element.files[0]) {
                var type = element.files[0].type.split('/')[0]
                if (type == 'image') {
                    $scope.splashScreen_source = false;
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $scope.appIcon = event.target.result;
                        $scope.$apply();
                    }
                    if (element.files[0]) {
                        reader.readAsDataURL(element.files[0]);
                    }
                } else {
                    $scope.notification("Select only images.", 'danger');
                    $("#appIcon").val("");
                    $scope.appIcon = '';
                }
            }
        }

        $scope.chooseAppIcon = chooseAppIcon;
        $scope.changeAppIcon = changeAppIcon;
        $scope.addNewDevice = addNewDevice;
        $scope.saveDevice = saveDevice;
        $scope.updateDevice = updateDevice;
        $scope.cancelAddDevice = cancelAddDevice;
        $scope.deleteDevice = deleteDevice;
        $scope.updateDeviceForm = updateDeviceForm;
        $scope.getAppDevices = getAppDevices

    }
    app.controller('appDeviceCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', 'TableConstants', appDeviceCtrl]);
}());
