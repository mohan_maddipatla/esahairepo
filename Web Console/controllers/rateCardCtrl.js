(function() {
    var app = angular.module('eSahai');
    var rateCardCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("tableName: " + TableConstants.rate_card);
        $scope.tabActive('rateCard', 'settings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.user = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        //$scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType === "EMPLOYEE" || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
            window.location.hash = '#/Home';
        }
        $scope.getCardRateObj = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.rate_card;
            req.params = {};
            /*var dateFilter = {};
            dateFilter.fieldName = 'createDateTime';
            dateFilter.startDate = $scope.fromDate;
            dateFilter.endDate = $scope.toDate;
            req.dateFilter = dateFilter;*/

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    console.log(response.Result[0])
                    if (response.Result.length > 0) {
                        $scope.page.type = 'put';
                    } else {
                        $scope.page.type = 'post';
                    }
                    $scope.rateCardObj = response.Result[0];
                    if ($scope.rateCardObj.status == 'Active') {
                        $scope.rateCardObj.status = true;
                    } else {
                        $scope.rateCardObj.status = false;
                    }
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                } else {
                    $scope.rateCardObj = {};
                }
            })
        }


        /*$scope.getAppsList(function() {
            console.log($scope.appsList)
        })*/
        $scope.getCardRateObj();
        if (window.location.hash == "#/Rate-Card") {

        }

        var validate = function(cb) {

            if (!$scope.rateCardObj.main_content) { $scope.err.main_content = true; } else { delete $scope.err.main_content; }
            if (!$scope.rateCardObj.start_date) { $scope.err.start_date = true; } else { delete $scope.err.start_date; }
            if (!$scope.rateCardObj.end_date) { $scope.err.end_date = true; } else { delete $scope.err.end_date; }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }


        var saveRateCard = function(rateCardObj) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.rate_card;
                var params = {};
                if (rateCardObj.status == true) {
                    rateCardObj.status = 'Active';
                } else {
                    rateCardObj.status = 'InActive';
                }
                params = angular.copy(rateCardObj);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        $scope.notification(response.statusMessage);
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });
        }

        var updateRateCard = function(rateCardObj) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.rate_card;
                var params = {};
                if (rateCardObj.status == true) {
                    rateCardObj.status = 'Active';
                } else {
                    rateCardObj.status = 'InActive';
                }
                params = angular.copy(rateCardObj);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = rateCardObj.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        $scope.notification(response.statusMessage);
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            });
        }



        var cancel = function() {
            window.location.hash = "#/Home";
        }






        $scope.saveRateCard = saveRateCard;
        $scope.updateRateCard = updateRateCard;
        $scope.cancel = cancel;
    }


    app.controller('rateCardCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', rateCardCtrl]);
}());
