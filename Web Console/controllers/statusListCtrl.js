(function() {
    var app = angular.module('eSahai');
    var statusListCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout) {
        $scope.tabActive('statusList', 'appSettings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.page = {};
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {};
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.status = {};
        $scope.err = {};
        $scope.getStatusList = function(cb) {
            console.log("sssssssssss")
            $scope.loader(true);
            var req = {};

            req.tableName = TableConstants.status_list;
            req.params = {};

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                console.log("Ddddddddddddddddddd", response)
                $scope.loader(false);
                if (response.statusCode == '300') {

                    $scope.statusList = response.Result;
                    $scope.totalItems = $scope.statusList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }


        if (window.location.hash == "#/Status-List") {
            $scope.page.title = 'Status List';
            $scope.getStatusList();

        } else if (window.location.hash == "#/Add-Status") {
            $scope.page.title = 'Add Status';
            $scope.page.type = 'post';

        } else if (window.location.hash == "#/Update-Status") {
            $scope.page.title = 'Update Status';
            $scope.page.type = 'put';
            console.log(ctrlComm.get('statusObj'))
            prePoppulateValues(ctrlComm.get('statusObj'));
        } else if (window.location.hash == "#/View-Status") {
            $scope.page.title = 'View Status';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('statusObj'));
        }
        var addNewStatus = function() {
            window.location.hash = "#/Add-Status";
        }

        var viewStatus = function(status) {
            ctrlComm.put('statusObj', status);
            window.location.hash = '#/View-Status';
        }

        function prePoppulateValues(status) {
            console.log("country" + angular.toJson(status))
            if (status) {
                console.log("sddddddddddddd")


                $scope.status = status;
            } else {
                console.log("sddddddddddddd")
                window.location.hash = "#/Status-List";
            }
        }
        var saveStatus = function(status) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.status_list;
                var params = {};

                params = angular.copy(status);;

                req.params = params;
                console.log(angular.toJson(req));
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);

                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = '#/Status-List';
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }


        var updateStatus = function(status) {
            validate(function() {
                console.log(status)
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.status_list;
                var params = {};

                params = angular.copy(status);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = status.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = '#/Status-List';
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }



        var validate = function(cb) {
            if (!$scope.status.status) {
                $scope.err.status = true;
            } else {
                delete $scope.err.status;
            }
           /* if (!$scope.status.short_code) {
                $scope.err.code = true;
            } else {
                delete $scope.err.code;
            }*/


            if (Object.keys($scope.err).length == 0) {


                if (cb) cb();
            }
        }
        var editStatus = function(status) {
        	console.log("statusList>>>>>>>>>>>",status)
            ctrlComm.put('statusObj', status);
            window.location.hash = "#/Update-Status";
        }


        var cancelStatus = function() {
            window.location.hash = "#/Status-List";
        }

        /* var deleteCountry = function(s) {
            var input = {
                text: 'Are you sure you want to delete',
                name: country.country
            }
            $scope.confirmation(input, function() {
                confirmDelete(country);
            });
        }
*/
        /*function confirmDelete(country) {

            var req = {};
            req.tableName = TableConstants.country_list;
            var params = {};
            params.id = country.id;
            req.params = params;
            console.log(angular.toJson(req))
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == "300") {
                    console.log("ddddddddddddddd")
                    $scope.notification(response.statusMessage);
                    $scope.getCountryList();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })


        }*/

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewStatus = addNewStatus;
        $scope.saveStatus = saveStatus;
        $scope.updateStatus = updateStatus;
        // $scope.deleteCountry = deleteCountry;
         $scope.cancelStatus = cancelStatus
        $scope.editStatus = editStatus
        $scope.viewStatus = viewStatus


    }
    app.controller('statusListCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout', statusListCtrl]);
}());
