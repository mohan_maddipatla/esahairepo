(function() {
    var app = angular.module('eSahai');
    var ambulanceMasterCtrl = function($scope, $rootScope, request, ctrlComm, fileUpload, $filter, TableConstants, $timeout) {
        console.log("in ambulanceMasterCtrl : " + TableConstants.ambulance_master);
        $scope.tabActive('ambulanceMaster');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.ambulance = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        //$scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');

        $scope.emergencyTypeOptions = [];
        $scope.selectedEmergencyType = [];
        $scope.ambulanceAttr = [];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};


        /*$scope.dropdownSetting = {
            scrollable: true,
            scrollableHeight: 'auto',
            showCheckAll: false,
            showUncheckAll: false,
        }*/
        var basic_phone = '';
        $scope.$watch('ambulance.ambulance_mobile', function(val) {
            if (val) {
                if (!val.toString().match(/^[0-9]*$/)) {
                    $scope.ambulance.ambulance_mobile = basic_phone;
                } else {
                    if (val.length <= 10) {
                        basic_phone = val;
                    } else {
                        $scope.ambulance.ambulance_mobile = basic_phone;
                    }
                }
            }
        });
        $scope.dropdownSettings = {
            scrollable: true,
            scrollableHeight: '200px',
            showCheckAll: false,
            showUncheckAll: false
        }

        $scope.translationText = {
            buttonDefaultText: '- Select Emergency Type -',
        }

        $scope.getAmbulanceMasterList = function(cb) {
                $scope.loader(true);
                console.log($scope.tableRows)
                var req = {};
                req.tableName = TableConstants.ambulance_master;
                req.params = {};
                if ($scope.admin.userType === "GROUP ADMIN") {
                    req.params.group_id = $scope.admin.groupId;
                }
                /*if($scope.fromDate != undefined && $scope.toDate != undefined){*/
                /* var dateFilter = {};
                 dateFilter.fieldName = 'created_date';
                 dateFilter.startDate = $scope.fromDate;
                 dateFilter.endDate = $scope.toDate;
                 req.dateFilter = dateFilter;*/
                /*}*/
                console.log(angular.toJson(req));
                request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        console.log(JSON.stringify($scope.admin))
                        $scope.getGlobalGroupMasterList(function() {
                            $scope.getVehicleTypeList(function() {
                                $scope.getEmergencyTypeList(function() {
                                    angular.forEach(response.Result, function(obj) {
                                        angular.forEach($scope.globalGroupMasterList, function(obj1) {
                                            if (obj1.group_id == obj.group_id) {
                                                obj.groupName = obj1.group_name;
                                            }
                                        })
                                        angular.forEach($scope.vehicleTypeList, function(obj2) {
                                            if (obj2.vehicle_id == obj.vehicle_type) {
                                                obj.vehicleTypeName = obj2.type_name;
                                            }
                                        })
                                        angular.forEach($scope.emergencyTypeList, function(obj3) {
                                            if (obj3.id == obj.emergency_type) {
                                                obj.emergencyTypeName = obj3.type_name;
                                            }
                                        })
                                    })
                                })
                            })
                            $scope.loader(false);
                        })
                        if (window.localStorage.getItem('hash') != undefined) {
                            window.localStorage.removeItem('hash')
                        }

                        $scope.ambulanceMasterList = response.Result;

                        /*    angular.forEach($scope.ambulanceMasterList, function(obj) {
                                if (obj.emergency_type != null) {
                                    angular.forEach(obj.emergency_type.split(','), function(obj1) {
                                    var temp = [];
                                    console.log($scope.emergencyTypeList)
                                        angular.forEach($scope.emergencyTypeList, function(obj2) {
                                            console.log(obj2)
                                            temp.push(obj2.type_name)
                                        })
                                    })
                                }

                            })*/
                        $scope.totalItems = $scope.ambulanceMasterList.length;
                        if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                            $scope.tableRows.push($scope.totalItems);

                        if ($scope.totalItems != 0) {
                            $scope.tableRows = $scope.tableRows.sort(request.sort);
                            $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                            if ($scope.tableRows[1]) {
                                $scope.viewby = $scope.tableRows[1];
                            } else {
                                $scope.viewby = $scope.tableRows[0];
                            }
                            $scope.setItemsPerPage($scope.viewby);
                        }
                        if (cb) cb();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)

                    }
                })
            }
            /*$scope.CategoriesSelected = [{ "id": 1 }, { "id": 2 }];
            $scope.Categoires = [];


            for (var i = 0; i < 10; i++) {
                $scope.Categoires.push({ id: i + 1, label: 'label' + (i + 1) })
            }*/
        if (window.location.hash == "#/Ambulance-Master") {
            $scope.getAmbulanceMasterList();

        } else if (window.location.hash == "#/New-Ambulance-Master") {
            $scope.page.title = 'Add Ambulance';
            $scope.page.type = 'post';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
                window.location.hash = '#/Ambulance-Master';
            }
            document.getElementById("imageName").src = 'images/upload.jpg';
            console.log($scope.Categoires)
            $scope.ambulance.device_type = 'Android';
            $scope.getEmergencyTypeList(function() {
                angular.forEach($scope.emergencyTypeList, function(obj) {
                    $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
                })
            })
        } else if (window.location.hash == "#/Update-Ambulance-Master") {
            $scope.page.title = 'Update Ambulance';
            $scope.page.type = 'put';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
                window.location.hash = '#/Ambulance-Master';
            }
            if (window.localStorage.getItem('hash') == undefined) {
                prePoppulateValues(ctrlComm.get('ambulanceObj'));
            }
            $scope.getAmbulanceMasterList();
            $scope.getEmergencyTypeList(function() {
                angular.forEach($scope.emergencyTypeList, function(obj) {
                    $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
                })
            })
        } else if (window.location.hash == '#/View-Ambulance-Master') {
            $scope.page.title = 'View Ambulance';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('ambulanceObj'));
            $scope.getEmergencyTypeList(function() {
                angular.forEach($scope.emergencyTypeList, function(obj) {
                    $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
                })
            })

        }

        $scope.chooseFile = function() {
            document.getElementById("uploadFile").click();
        }

        $('#uploadFile').change(function() {
            readURL();
        })

        function readURL() {
            var file = $('#uploadFile')[0].files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imageName').attr('src', e.target.result);
                }

                reader.readAsDataURL(file);
            }
        }
        $scope.addAttribute = function() {
            angular.forEach($scope.ambulanceAttr, function(obj) {
                if (obj.flag == 'edit') {
                    obj.flag = undefined;
                }
                var temp = [];
                angular.forEach($scope.ambulance_e_types_list, function(obj1) {
                    if (obj.ambulance_e_type_id != obj1.id) {
                        //$scope.customer_e_types_list
                        temp.push(obj1);
                        //console.log(obj1)
                    }
                })
                $scope.ambulance_e_types_list = temp;
            })
            $scope.ambulanceAttr.push({ 'ambulance_e_type_id': '', 'attribute_details': '' });
        }

        $scope.saveAttribute = function(attrObj, ambulance) {
            $scope.loader(true);
            if (attrObj.ambulance_e_type_id != '' && attrObj.ambulance_e_type_id != undefined && attrObj.attribute_details != '' && attrObj.attribute_details != undefined) {
                console.log(ambulance)
                var req = {};
                req.tableName = TableConstants.ambulance_attributes;
                var params = {};
                angular.forEach($scope.ambulanceAttributesList, function(obj) {
                    if (obj.id == attrObj.ambulance_e_type_id)
                        attrObj.attribute_name = obj.type_name;
                })
                params = angular.copy(attrObj);
                params.ambulance_id = ambulance.amb_id;
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(ambulance)
                        $scope.notification('Attribute saved successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            } else if (!attrObj.ambulance_e_type_id) {
                $scope.notification('Please select attibute', 'danger');
            } else if (!attrObj.attribute_details) {
                $scope.notification('Please enter attibute value', 'danger');
            }
        }

        $scope.updateAttribute = function(attrObj, ambulance) {
            if (attrObj.ambulance_e_type_id != '' && attrObj.ambulance_e_type_id != undefined && attrObj.attribute_details != '' && attrObj.attribute_details != undefined) {
                var req = {};
                req.tableName = TableConstants.ambulance_attributes;
                var params = {};
                angular.forEach($scope.ambulanceAttributesList, function(obj) {
                    if (obj.id == attrObj.ambulance_e_type_id)
                        attrObj.attribute_name = obj.type_name;
                })
                params = angular.copy(attrObj);
                delete params['id'];
                delete params['flag'];
                var conditionalParams = {};
                conditionalParams.id = attrObj.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(ambulance)
                        $scope.notification('Attribute updated successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            } else if (!attrObj.ambulance_e_type_id) {
                $scope.notification('Please select attibute', 'danger');
            } else if (!attrObj.attribute_details) {
                $scope.notification('Please enter attibute value', 'danger');
            }
        }

	$scope.deleteAttribute = function(attrObj, ambulance) {
                var input = {
                    text: 'Are you sure you want to delete',
                    name: attrObj.attribute_name
                }
                console.log("ambulance", attrObj)
                $scope.confirmation(input, function() {
                    confirmAttributeDelete(attrObj, ambulance);
                });
            }
        function confirmAttributeDelete(attrObj, ambulance) {        
	    if (attrObj.id) {
                var req = {};
                req.tableName = TableConstants.ambulance_attributes;
                var params = {};
                params.id = attrObj.id;
                req.params = params;
                request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (typeof response == 'string') {
                        response = JSON.parse(response)
                    }
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(ambulance);
                        $scope.notification('Attribute deleted successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)
                    }
                })
            } else {
                var temp = []
                angular.forEach($scope.ambulanceAttr, function(obj) {
                    if (obj.id != undefined) {
                        temp.push(obj)
                    }
                })
                $scope.ambulanceAttr = temp;
            }
        }

        $scope.editAttribute = function(attrObj, ambulance) {
            var temp = [];
            var arr = [];
            var a = [];
            angular.forEach($scope.ambulanceAttr, function(obj) {
                a.push(obj.ambulance_e_type_id)

            })
            angular.forEach($scope.ambulanceAttr, function(obj) {
                if (obj.flag == 'edit') {
                    obj.flag = undefined;
                }

                angular.forEach($scope.global_ambulance_e_types_list, function(obj1) {
                    if (obj.ambulance_e_type_id != obj1.id) {
                        if (arr.indexOf(obj1.id) == -1 && a.indexOf(obj1.id) == -1) {
                            temp.push(obj1);
                            arr.push(obj1.id);
                        }

                    }

                });
            })

            angular.forEach($scope.global_ambulance_e_types_list, function(obj) {
                if (attrObj.ambulance_e_type_id == obj.id) {
                    temp.push(obj);
                }
            })

            $scope.ambulance_e_types_list = temp;
            angular.forEach($scope.ambulanceAttr, function(obj) {
                if (obj.id == attrObj.id) {
                    obj.flag = 'edit';
                }
            })


            /*angular.forEach($scope.ambulanceAttr, function(obj) {
                if (obj.id == attrObj.id) {
                    obj.flag = 'edit';
                }
            })*/
        }

        function prePoppulateAttrValues(ambulance, cb) {
            var req = {};
            req.tableName = TableConstants.ambulance_attributes;
            req.params = {};
            req.params.ambulance_id = ambulance.amb_id;
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.ambulanceAttr = response.Result;

                    var req = {};
                    req.tableName = TableConstants.ambulance_e_types;
                    req.params = {};
                    console.log(angular.toJson(req));
                    request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                        $scope.loader(false);
                        if (response.statusCode == '300') {
                            $scope.ambulance_e_types_list = response.Result;
                            $scope.global_ambulance_e_types_list = response.Result;
                        }
                    })
                    if (cb) {
                        cb()
                    }
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                }
            })
        }



        var addNewAmbulanceMaster = function() {
            window.location.hash = "#/New-Ambulance-Master";
        }

        function prePoppulateValues(ambulance) {
            if (ambulance) {
                if ($scope.page.type == 'put') {
                    ambulance.is_active == 'Active' ? ambulance.is_active = true : ambulance.is_active = false;
                    if (document.getElementById('imageName') != undefined) {
                        document.getElementById("imageName").src = ambulance.file_url;
                    }
                }
                if(ambulance.emergency_type!=null && ambulance.emergency_type!=""){
                angular.forEach(ambulance.emergency_type.split(','), function(obj) {
                    $scope.selectedEmergencyType.push({ 'id': obj })
                })
                }else{
                        $scope.selectedEmergencyType = [];
                }

		var emergencyTypeNames = [];
                angular.forEach($scope.selectedEmergencyType, function(obj) {
                    angular.forEach($scope.emergencyTypeList, function(obj1) {
                        if (obj.id == obj1.id) {
                            emergencyTypeNames.push(obj1.type_name)
                        }
                    })
                })
                ambulance.emergencyTypeNames = emergencyTypeNames.join();
                $scope.ambulance = ambulance;
                prePoppulateAttrValues(ambulance, function() {})
            } else {
                window.location.hash = "#/Ambulance-Master";
            }
        }

        var validate = function(cb) {
            if (!$scope.ambulance.ambulance_number) {
                $scope.err.ambulance_number = true;
            } else {
                delete $scope.err.ambulance_number;
            }

            if (!$scope.ambulance.group_id) {
                $scope.err.group_id = true;
            } else {
                delete $scope.err.group_id;
            }

            if (!$scope.ambulance.vehicle_type) {
                $scope.err.vehicle_type = true;
            } else {
                delete $scope.err.vehicle_type;
            }

            if (!$scope.selectedEmergencyType.length) {
                $scope.err.selectedEmergencyType = true;
            } else {
                delete $scope.err.selectedEmergencyType;
            }

            if (!$scope.ambulance.ambulance_mobile) {
                $scope.err.ambulance_mobile = true;
            } else {
                if (!(/^\d{10}$/).test($scope.ambulance.ambulance_mobile)) {
                    $scope.err.ambulance_mobile_cdn = true;

                } else {
                    delete $scope.err.ambulance_mobile;
                    delete $scope.err.ambulance_mobile_cdn;

                }
            }



            if (Object.keys($scope.err).length == 0) {
                if (document.getElementById("imageName").src.split('/')[document.getElementById("imageName").src.split('/').length - 1] == 'upload.jpg') {
                    $scope.notification("please upload photo", 'danger');
                } else {
                    if (document.getElementById('uploadFile').files.length && (document.getElementById('uploadFile').files[0].size / 1000).toFixed(2) > 1000) {
                        $scope.notification("Image should not exceed 1MB", 'danger');
                    } else {
                        if (cb) cb();
                    }
                }

            }
        }


        var saveAmbulanceMaster = function(ambulance) {
            validate(function() {
                $scope.loader(true);
                ambulance.is_active = 'Active';
                ambulance.emergency_type = [];
                angular.forEach($scope.selectedEmergencyType, function(obj) {
                    ambulance.emergency_type.push(obj.id);
                })
                ambulance.emergency_type = ambulance.emergency_type.join();
                var req = {};
                req.tableName = TableConstants.ambulance_master;
                req.params = ambulance;
                if ($scope.image_url == undefined) {
                    $scope.image_url = $('#uploadFile')[0].files[0];
                }
                console.log(angular.toJson(req));
                fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'insertRecord', $scope.CONFIG, 'post', function(data) {
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification('Ambulance created successfully');
                        window.localStorage.setItem('hash', 'list')
                        window.location.hash = '#/Ambulance-Master';
                    } else if (data.statusCode == '222') {
                        if (data.statusMessage.search('ambulance_mobile_uni') > 0) {
                            $scope.notification('Phone number already exists, Try with other', 'danger');
                        } else if (data.statusMessage.search('ambulance_number') > 0) {
                            $scope.notification('Ambulance number already exists, Try with other', 'danger');
                        }
                    } else {
                        $scope.notification(data.statusMessage, 'danger');
                    }
                });
                /*var req = {};
                req.tableName = TableConstants.ambulance_master;
                var params = {};
                if (group.is_active == true) {
                    group.is_active = 'Active';
                } else {
                    group.is_active = 'InActive';
                }
                params = angular.copy(group);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Ambulance-Master"
                        $scope.notification(response.statusMessage);
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })*/
            });
        }


        var viewAmbulanceMaster = function(ambulance) {
            ctrlComm.put('ambulanceObj', ambulance);
            window.location.hash = '#/View-Ambulance-Master';
        }

        var updateAmbulanceMaster = function(ambulance) {
            validate(function() {
                $scope.loader(true);
                /*   var req = {};
                   req.tableName = TableConstants.ambulance_master;
                   var params = {};
                   if (group.is_active == true) {
                       group.is_active = 'Active';
                   } else {
                       group.is_active = 'InActive';
                   }
                   params = angular.copy(group);
                   delete params['amb_id'];
                   delete params['groupName'];
                   var conditionalParams = {};
                   conditionalParams.amb_id = group.amb_id;
                   req.params = params;
                   req.conditionalParams = conditionalParams;
                   console.log(angular.toJson(req))
                   request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                           $scope.loader(false);
                           if (response.statusCode == '300') {
                               window.location.hash = "#/Group-Master"
                               $scope.notification(response.statusMessage);
                           } else if (response.statusCode == '204') {
                               $scope.notification(response.statusMessage, "danger");
                               $timeout(function() {
                                   $scope.logout();
                               }, 2000)

                           } else {
                               $scope.notification(response.statusMessage);
                           }

                       })*/

                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.ambulance_master;
                var params = {};
                if (ambulance.is_active == true) {
                    ambulance.is_active = 'Active';
                } else {
                    ambulance.is_active = 'InActive';
                }
                ambulance.emergency_type = [];
                angular.forEach($scope.selectedEmergencyType, function(obj) {
                    ambulance.emergency_type.push(obj.id);
                })
                ambulance.emergency_type = ambulance.emergency_type.join();
                params = angular.copy(ambulance);
                delete params['amb_id'];
                delete params['groupName'];
                delete params['vehicleTypeName'];
                delete params['emergencyTypeName'];
                delete params['emergencyTypeNames'];
                var conditionalParams = {};
                conditionalParams.amb_id = ambulance.amb_id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                req.params = params;
                if ($('#uploadFile')[0].files[0] != null) {
                    $scope.image_url = $('#uploadFile')[0].files[0];
                }
                fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'updateRecord', $scope.CONFIG, 'put', function(data) {
                    console.log(data)
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification('Ambulance updated successfully');
                        //delete banner['circleIds'];
                        window.location.hash = '#/Ambulance-Master';
                    } else if (data.statusCode == '222') {
                        if (data.statusMessage.search('ambulance_mobile_uni') > 0) {
                            $scope.notification('Phone number already exists, Try with other', 'danger');
                        } else if (data.statusMessage.search('ambulance_number') > 0) {
                            $scope.notification('Ambulance number already exists, Try with other', 'danger');
                        }
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(data.statusMessage, 'danger');
                    }
                });
            });
        }

        var editAmbulanceMaster = function(ambulance) {
            ctrlComm.put('ambulanceObj', ambulance);
            window.location.hash = "#/Update-Ambulance-Master";
        }


        var cancelAmbulanceMaster = function() {
            window.location.hash = "#/Ambulance-Master";
        }

        $scope.logoutAmbulance = function(ambulance) {
            var input = {
                text: 'Are you sure you want to Log out ambulance',
                name: ambulance.ambulance_number

            }
            $scope.confirmation(input, function() {
                confirmAmbulanceLogout(ambulance);
            });
        }

        function confirmAmbulanceLogout(ambulance) {
            var req = {};
            req.tableName = TableConstants.ambulance_master;
            var params = {};
            params.login_status = 'NotLoggedIn'
            delete params['id'];
            var conditionalParams = {};
            conditionalParams.amb_id = ambulance.amb_id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                /*$scope.loader(false);*/
                if (response.statusCode == '300') {
                    $scope.getAmbulanceMasterList(function() {
                        $scope.notification("Ambulance Logged Off Successfully");
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })
        }

        var deleteAmbulanceMaster = function(ambulance) {
            var input = {
                text: 'Are you sure you want to delete',
                name: ambulance.display_name
            }
            $scope.confirmation(input, function() {
                confirmDelete(ambulance);
            });
        }

        function confirmDelete(ambulance) {
            console.log(angular.toJson(ambulance))
                /*var req = {};
                req.emergency_type = ambulance.emergency_type;
                req.tableName = TableConstants.ambulance_master;
                var params = {};
                params.amb_id = ambulance.amb_id;
                req.params = params;*/
                /* request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                     if (typeof response == 'string') {
                         response = JSON.parse(response)
                     }
                     if (response.statusCode == '300') {
                         $scope.getAmbulanceMasterList(function() {
                             $scope.notification(response.statusMessage);
                         })

                     } else if (response.statusCode == '204') {
                         $scope.notification(response.statusMessage, "danger");
                         $timeout(function() {
                             $scope.logout();

                         }, 2000)
                     } else if (response.statusCode == '400' && response.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                         $scope.notification("Cannot delete parent record", "danger");
                     }
                 })*/


            var req = {};
            req.tableName = TableConstants.ambulance_master;
            var params = {};
            params.is_active = 'InActive'
            delete params['id'];
            var conditionalParams = {};
            conditionalParams.amb_id = ambulance.amb_id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            var input = {};
            input.type = 'ambulance';
            input.ambulance_id = ambulance.amb_id;
            input.is_active = 'InActive';
            console.log("input", input)


            request.service('commonLimitOrActiveCheck', 'post', input, $scope.CONFIG, function(response) {
                $scope.loader(false);
                console.log(response)
                if (response.statusCode == '300') {


                    request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                        /*$scope.loader(false);*/
                        if (response.statusCode == '300') {
                            $scope.getAmbulanceMasterList(function() {
                                $scope.notification("Ambulance Deleted Successfully");
                            })
                        } else if (response.statusCode == '204') {
                            $scope.notification(response.statusMessage, "danger");
                            $timeout(function() {
                                $scope.logout();
                            }, 2000)
                        } else {
                            $scope.notification(response.statusMessage);
                        }
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage, "danger");
                    //driver.is_active = !driver.is_active;
                }
            })




        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }
        var importExcelDoc = function() {
            var file = $scope.myFile;
            if (file && (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/vnd.ms-excel")) {
                $scope.loader(true);
                var obj = {};
                fileUpload.uploadFileToUrl(file, 'ambulance_master', obj, 'uploadExcel', $scope.CONFIG, 'post', function(data) {
                    $scope.loader(false);
                    console.log("data", data);
                    if (data.statusCode == '300') {
                        $('#uploadFile').val("");
                        $scope.notification('File uploaded successfully.', 'success');
                        $scope.getAmbulanceMasterList(function() {
                            $scope.totalItems = $scope.ambulanceMasterList.length;
                            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                $scope.tableRows.push($scope.totalItems);
                            if ($scope.totalItems != 0) {
                                $scope.tableRows = $scope.tableRows.sort(request.sort);
                                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                if ($scope.tableRows[1]) {
                                    $scope.viewby = $scope.tableRows[1];
                                } else {
                                    $scope.viewby = $scope.tableRows[0];
                                }
                                $scope.setItemsPerPage($scope.viewby);
                            }
                        });
                        $scope.upldfile = {};
                    } else if (data.statusCode == '446') {
                        $('#uploadFile').val("");
                        $scope.notification(data.statusMessage, 'danger');
                        $scope.getAmbulanceMasterList();
                        $scope.upldfile = {};


                    }
                });
            } else {
                $scope.notification('Upload excel spreadsheet only(.xls or .xlsx). ', 'info');
            }
        }

        $scope.fileUpLoad = function() {
            $scope.upldfile = {};
            document.getElementById("uploadFile").click();
        }
        $scope.catchUploadedFile = function() {
            var f = document.getElementById('uploadFile').files[0];
            if (f) {
                $scope.$apply(function() {
                    $scope.upldfile = {
                        name: f.name,
                        size: (f.size / 1024).toFixed(2) + ' KB'
                    }
                });
            }
        }



        $scope.cancelImport = function() {
            $scope.upldfile = {};
            $('#uploadFile').val("");
        }

        $scope.checkAmbulanceMapping = function(ambObj) {
            $scope.loader(true);
            req = {};
            req.type = 'ambulance';
            req.ambulance_id = ambObj.amb_id;
            req.is_active = ambObj.is_active == true ? 'Active' : 'InActive';
            request.service('commonLimitOrActiveCheck', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                console.log(response)
                if (response.statusCode == '300') {
                    if (ambObj.is_active == false) {
                        $scope.notification("By doing this action, ambulance will be logged out from the mobile", "danger")
                    }
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage, "danger");
                    ambObj.is_active = !ambObj.is_active;
                }
            })
        }
        $scope.importExcelDoc = importExcelDoc;




        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewAmbulanceMaster = addNewAmbulanceMaster;
        $scope.saveAmbulanceMaster = saveAmbulanceMaster;
        $scope.updateAmbulanceMaster = updateAmbulanceMaster;
        $scope.deleteAmbulanceMaster = deleteAmbulanceMaster;
        $scope.cancelAmbulanceMaster = cancelAmbulanceMaster;
        $scope.editAmbulanceMaster = editAmbulanceMaster;
        $scope.viewAmbulanceMaster = viewAmbulanceMaster;
    }


    app.controller('ambulanceMasterCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'fileUpload', '$filter', 'TableConstants', '$timeout', ambulanceMasterCtrl]);
}());
