(function() {
    var app = angular.module('eSahai');
    var appSettingsCtrl = function($scope, request) {
        $scope.tabActive('appSettings', 'settings');

        var getAppSettingsList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = 'app_meta_data';
            req.params = {};
            console.log(angular.toJson(req));

            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '200') {
                    if (response.Result.length > 0 && response.Result[0] != undefined) {
                        if (response.Result[0].unique_login == 'false') {
                            response.Result[0].unique_login = false;
                            response.Result[0].max_logins = ''
                        } else {
                            response.Result[0].unique_login = true;

                        }
                        $scope.setting = response.Result[0];
                        $scope.settingList = 'update';

                    } else {
                        $scope.setting = {};
                        $scope.setting.unique_login = false;
                        $scope.settingList = 'insert';
                    }

                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                } else {
                    $scope.usersList = [];
                }
            })
        }


        if (window.location.hash == "#/App-Settings") {
            getAppSettingsList();
        }

        var saveAppSettings = function(settingsObj) {
            //     getAppList();
            if ($scope.settingList == 'insert') {
                var req = {};
                req.tableName = 'app_meta_data';
                var params = {};
                params.unique_login = settingsObj.unique_login;
                if (settingsObj.unique_login == true) {
                    params.max_logins = settingsObj.max_logins;
                }
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '200') {
                        getSettingsObj();
                        $scope.notification("Settings saved successfully");
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })

            } else {
                var req = {};
                req.tableName = 'app_meta_data';
                var params = {};
                if (settingsObj.unique_login == true) {
                    settingsObj.unique_login = 'true';
                } else {
                    settingsObj.unique_login = 'false';
                }
                params.unique_login = settingsObj.unique_login;
                if (settingsObj.unique_login == 'true') {
                    params.max_logins = settingsObj.max_logins;
                }
                req.params = params;
                var conditionalParams = {};
                conditionalParams.id = $scope.settingList[0].id;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '200') {
                        // getSettingsObj();
                        $scope.notification("Settings saved successfully");
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            }
        }

        $scope.cancelSettings = function() {
            window.location.hash = "#/"
        }

        $scope.saveAppSettings = saveAppSettings;
    }


    app.controller('appSettingsCtrl', ['$scope', 'request', appSettingsCtrl]);
}());
