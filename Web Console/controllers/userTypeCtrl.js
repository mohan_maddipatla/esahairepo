(function() {
    var app = angular.module('eSahai');
    var userTypeCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("in userTypeCtrl--> " + TableConstants.user_types);
        $scope.tabActive('userType', 'settings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.userType = {};
        $scope.page = {};
        $scope.err = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.toDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType === "EMPLOYEE" || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
            window.location.hash = '#/Home';
        }




        $scope.getUserTypeList = function() {
            $scope.totalItems = $scope.userTypeList.length;
            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                $scope.tableRows.push($scope.totalItems);

            if ($scope.totalItems != 0) {
                $scope.tableRows = $scope.tableRows.sort(request.sort);
                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                if ($scope.tableRows[1]) {
                    $scope.viewby = $scope.tableRows[1];
                } else {
                    $scope.viewby = $scope.tableRows[0];
                }
                $scope.setItemsPerPage($scope.viewby);
            }
        };
        if (window.location.hash == "#/User-Type") {
            $scope.getUserTypeList();
        } else if (window.location.hash == "#/New-User-Type") {
            $scope.page.title = 'Add New User Type';
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-User-Type") {
            $scope.page.title = 'Update User Type';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('userTypeObj'));
        }

        function prePoppulateValues(userType) {
            if (userType) {
                $scope.userType = userType;
            } else {
                window.location.hash = "#/User-Type"
            }
        }

        var validate = function(cb) {
            if (!$scope.userType.type_name) {
                $scope.err.type_name = true;
            } else {
                delete $scope.err.type_name;
            }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var addNewUserType = function() {
            window.location.hash = '#/New-User-Type';
        }

        var cancelUserType = function() {
            window.location.hash = "#/User-Type"
        }

        var saveUserType = function(userType) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.user_types;
                var params = {};
                params = angular.copy(userType);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/User-Type"
                        $scope.notification(response.statusMessage);
			$scope.getUserTypeList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            })
        }

        var editUserType = function(userType) {
            ctrlComm.put('userTypeObj', userType);
            window.location.hash = "#/Update-User-Type"
        }

        var updateUserType = function(userType) {
            validate(function() {
                /*$scope.loader(true);*/
                var req = {};
                req.tableName = TableConstants.user_types;
                var params = {};
                params = angular.copy(userType);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = userType.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    /*$scope.loader(false);*/
                    if (response.statusCode == '300') {
                        window.location.hash = "#/User-Type"
                        $scope.notification(response.statusMessage);
			$scope.getUserTypeList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });
        }

        var deleteUserType = function(obj) {
            var input = {
                text: 'Are you sure you want to delete',
                name: obj.name
            }
            $scope.confirmation(input, function() {
                confirmDeleteUserType(obj);
            });
        }

        function confirmDeleteUserType(obj) {
            var req = {};
            req.tableName = TableConstants.user_types;
            var params = {};
            params.id = obj.id;
            req.params = params;
            //$scope.loader(true);
            console.log(angular.toJson(obj));
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(data) {
                if (typeof data == 'string') {
                    data = JSON.parse(data)
                }
                //$scope.loader(false);
                if (data.statusCode == '300') {
                    $scope.getUserTypeList(function() {
                        $scope.notification(data.statusMessage);
                        var req = {};
                        req.tableName = TableConstants.user_types;
                        req.params = {};
                        console.log(angular.toJson(req));
                        request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                            $scope.loader(false);
                            if (response.statusCode == '300') {
                                $scope.userTypeList = response.Result;
                                $scope.totalItems = $scope.userTypeList.length;
                                if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                    $scope.tableRows.push($scope.totalItems);

                                if ($scope.totalItems != 0) {
                                    $scope.tableRows = $scope.tableRows.sort(request.sort);
                                    $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                    if ($scope.tableRows[1]) {
                                        $scope.viewby = $scope.tableRows[1];
                                    } else {
                                        $scope.viewby = $scope.tableRows[0];
                                    }
                                    $scope.setItemsPerPage($scope.viewby);
                                }
                            } else if (response.statusCode == '204') {
                                $scope.notification(response.statusMessage, "danger");
                                $timeout(function() {
                                    $scope.logout();

                                }, 2000)
                            }
                        })
                    });
                } else if (data.statusCode == '400' && data.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record", "danger");
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(data.statusMessage);
                }
            });
        }
        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewUserType = addNewUserType;
        $scope.cancelUserType = cancelUserType;
        $scope.saveUserType = saveUserType;
        $scope.updateUserType = updateUserType;
        $scope.editUserType = editUserType;
        $scope.deleteUserType = deleteUserType;
    }


    app.controller('userTypeCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', userTypeCtrl]);
}());
