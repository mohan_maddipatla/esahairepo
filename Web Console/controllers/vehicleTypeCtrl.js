(function() {
    var app = angular.module('eSahai');
    var vehicleTypeCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("in vehicleTypeCtrl--> " + TableConstants.vehicle_type);
        $scope.tabActive('vehicleType', 'settings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.vehicleType = {};
        $scope.page = {};
        $scope.err = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.toDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType === "EMPLOYEE") {
            window.location.hash = '#/Home';
        }

        if (window.location.hash == "#/Vehicle-Type") {
            $scope.getVehicleTypeList(function() {
                $scope.totalItems = $scope.vehicleTypeList.length;
                if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                    $scope.tableRows.push($scope.totalItems);

                if ($scope.totalItems != 0) {
                    $scope.tableRows = $scope.tableRows.sort(request.sort);
                    $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                    if ($scope.tableRows[1]) {
                        $scope.viewby = $scope.tableRows[1];
                    } else {
                        $scope.viewby = $scope.tableRows[0];
                    }
                    $scope.setItemsPerPage($scope.viewby);
                }
            });
        } else if (window.location.hash == "#/New-Vehicle-Type") {
            $scope.page.title = 'Add Vehicle Type';
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Vehicle-Type") {
            $scope.page.title = 'Update Vehicle Type';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('vehicleTypeObj'));
        }

        function prePoppulateValues(vehicleType) {
            if (vehicleType) {
                $scope.vehicleType = vehicleType;
            } else {
                window.location.hash = "#/Vehicle-Type"
            }
        }

        var validate = function(cb) {
            if (!$scope.vehicleType.type_name) {
                $scope.err.type_name = true;
            } else {
                delete $scope.err.type_name;
            }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var addNewVehicleType = function() {
            window.location.hash = '#/New-Vehicle-Type';
        }

        var cancelVehicleType = function() {
            window.location.hash = "#/Vehicle-Type"
        }

        var saveVehicleType = function(vehicleType) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.vehicle_type;
                var params = {};
                params = angular.copy(vehicleType);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Vehicle-Type"
                        $scope.notification(response.statusMessage);
			$scope.getVehicleTypeList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            })
        }

        var editVehicleType = function(vehicleType) {
            ctrlComm.put('vehicleTypeObj', vehicleType);
            window.location.hash = "#/Update-Vehicle-Type";
        }

        var updateVehicleType = function(vehicleType) {
            validate(function() {
                /*$scope.loader(true);*/
                var req = {};
                req.tableName = TableConstants.vehicle_type;
                var params = {};
                params = angular.copy(vehicleType);
                delete params['vehicle_id'];
                var conditionalParams = {};
                conditionalParams.vehicle_id = vehicleType.vehicle_id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    /*$scope.loader(false);*/
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Vehicle-Type"
                        $scope.notification(response.statusMessage);
			$scope.getVehicleTypeList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });
        }

        var deleteVehicleType = function(obj) {
            var input = {
                text: 'Are you sure you want to delete',
                name: obj.name
            }
            $scope.confirmation(input, function() {
                confirmDeleteVehicleType(obj);
            });
        }

        function confirmDeleteVehicleType(obj) {
            var req = {};
            req.tableName = TableConstants.vehicle_type;
            var params = {};
            params.vehicle_id = obj.vehicle_id;
            req.params = params;
            $scope.loader(true);
            console.log(angular.toJson(obj));
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(data) {
                $scope.loader(false);
                if (typeof data == 'string') {
                    data = JSON.parse(data)
                }
                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    var req = {};
                    req.tableName = TableConstants.vehicle_type;
                    req.params = {};
                    console.log(angular.toJson(req));
                    request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                        /*$scope.loader(false);*/
                        if (response.statusCode == '300') {
                            $scope.vehicleTypeList = response.Result;
                            $scope.totalItems = $scope.vehicleTypeList.length;
                            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                $scope.tableRows.push($scope.totalItems);

                            if ($scope.totalItems != 0) {
                                $scope.tableRows = $scope.tableRows.sort(request.sort);
                                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                if ($scope.tableRows[1]) {
                                    $scope.viewby = $scope.tableRows[1];
                                } else {
                                    $scope.viewby = $scope.tableRows[0];
                                }
                                $scope.setItemsPerPage($scope.viewby);
                            }
                        } else if (response.statusCode == '204') {
                            $scope.notification(response.statusMessage, "danger");
                            $timeout(function() {
                                $scope.logout();

                            }, 2000)
                        }
                    })
                } else if (response.statusCode == '400' && response.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record", "danger");
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(data.statusMessage);
                }
            });
        }
        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewVehicleType = addNewVehicleType;
        $scope.saveVehicleType = saveVehicleType;
        $scope.cancelVehicleType = cancelVehicleType;
        $scope.updateVehicleType = updateVehicleType;
        $scope.editVehicleType = editVehicleType;
        $scope.deleteVehicleType = deleteVehicleType;
    }
    app.controller('vehicleTypeCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', vehicleTypeCtrl]);
}());
