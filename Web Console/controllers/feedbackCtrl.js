(function() {
    var app = angular.module('eSahai');
    var feedbackCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout,$filter) {
        $scope.tabActive('feedBack');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.page = {};
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd'); 
        $scope.pageChanged = function() {};
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.fb = {};
        $scope.err = {};
        $scope.getFeedbackList = function(cb) {
            $scope.loader(true);
            var req = {};

            req.tableName = TableConstants.feedback;
            var dateFilter = {};
            dateFilter.fieldName = 'created_date';
            dateFilter.startDate = $scope.fromDate;
            if(dateFilter.startDate != $scope.toDate){
                dateFilter.endDate = $scope.toDate;
            }
            req.dateFilter = dateFilter;
            req.params = {};

            console.log(angular.toJson(req));
            request.service('getFeedbackList', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {

                    $scope.feedBackList = response.Result;
                    $scope.totalItems = $scope.feedBackList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }


        if (window.location.hash == "#/Feedback") {
            $scope.page.title = 'Feedback';
            $scope.getFeedbackList();

        } 


        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;


    }
    app.controller('feedbackCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout','$filter', feedbackCtrl]);
}());
