(function() {
    var app = angular.module('eSahai');
    var emergencyTypeCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("in emergencyTypeCtrl--> " + TableConstants.emergency_types);
        $scope.tabActive('emergencyType', 'settings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.emergencyType = {};
        $scope.page = {};
        $scope.err = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.toDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType === "EMPLOYEE" || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
            window.location.hash = '#/Home';
        }

     /*   $scope.getEmergencyTypeList(function() {
            $scope.totalItems = $scope.emergencyTypeList.length;
            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                $scope.tableRows.push($scope.totalItems);

            if ($scope.totalItems != 0) {
                $scope.tableRows = $scope.tableRows.sort(request.sort);
                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                if ($scope.tableRows[1]) {
                    $scope.viewby = $scope.tableRows[1];
                } else {
                    $scope.viewby = $scope.tableRows[0];
                }
                $scope.setItemsPerPage($scope.viewby);
            }
        });*/
            $scope.getEmergencyList = function() {
                var req = {};
                req.tableName = TableConstants.emergency_types;
                req.params = {};
                console.log(angular.toJson(req));
                request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        $scope.emergencyTypeList = response.Result;
			$scope.emergencyTypeList.sort(function(a, b) {

                                return (a.type_name.toUpperCase() > b.type_name.toUpperCase()) ? 1 : ((b.type_name.toUpperCase() > a.type_name.toUpperCase()) ? -1 : 0); });


                        /*$scope.emergencyTypeList.sort(function(a, b) {
                            return (a.type_name > b.type_name) ? 1 : ((b.type_name > a.type_name) ? -1 : 0);
                        });*/

                        $scope.totalItems = $scope.emergencyTypeList.length;
                        if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                            $scope.tableRows.push($scope.totalItems);

                        if ($scope.totalItems != 0) {
                            $scope.tableRows = $scope.tableRows.sort(request.sort);
                            $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                            if ($scope.tableRows[1]) {
                                $scope.viewby = $scope.tableRows[1];
                            } else {
                                $scope.viewby = $scope.tableRows[0];
                            }
                            $scope.setItemsPerPage($scope.viewby);
                        }
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)
                    }
                })

            };


        if (window.location.hash == "#/Emergency-Type") {
	            $scope.getEmergencyList();

	} else if (window.location.hash == "#/New-Emergency-Type") {
            $scope.page.title = 'Add Emergency Type';
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Emergency-Type") {
            $scope.page.title = 'Update Emergency Type';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('emergencyTypeObj'));
        }

        function prePoppulateValues(emergencyType) {
            if (emergencyType) {
                $scope.emergencyType = emergencyType;
		if(emergencyType.status =='Active') {
		 $scope.emergencyType.status= true;
		} else {
		$scope.emergencyType.status =  false;

		}
            } else {
                window.location.hash = "#/Emergency-Type"
            }
        }

        var validate = function(cb) {
            if (!$scope.emergencyType.type_name) {
                $scope.err.type_name = true;
            } else {
                delete $scope.err.type_name;
            }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var addNewEmergencyType = function() {
            window.location.hash = '#/New-Emergency-Type';
        }

        var cancelEmergencyType = function() {
            window.location.hash = "#/Emergency-Type"
        }

        var saveEmergencyType = function(groupType) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.emergency_types;
                var params = {};
                params = angular.copy(groupType);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Emergency-Type"
                        $scope.notification(response.statusMessage);
                    }  else if( response.statusCode == '222' ){
                          $scope.notification('Duplicate Emergency Type', "danger");

                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            })
        }
        
        
        var editEmergencyType = function(emergencyType) {
            ctrlComm.put('emergencyTypeObj', emergencyType);
            var input = {
                text: 'Are you sure you want to Activate',
                name: emergencyType.name
            }
            $scope.confirmation(input, function() {
                updateEmergencyType(emergencyType);
            });
        }

        var updateEmergencyType = function(emergencyType) {
            var req = {};
            req.tableName = TableConstants.emergency_types;
            req.params = {};
            req.params.status = 'Active'

            var conditionalParams = {};
            conditionalParams.id = emergencyType.id;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    window.location.hash = "#/Emergency-Type"
                    $scope.notification(response.statusMessage);
                    $scope.getEmergencyList();
                } else if (response.statusCode == '204' || response.statusCode == '412') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })

        }


     
        var deleteEmergencyType = function(obj) {
            var input = {
                text: 'Are you sure you want to delete',
                name: obj.name
            }
            $scope.confirmation(input, function() {
                confirmDeleteEmergencyType(obj);
            });
        }

        function confirmDeleteEmergencyType(obj) {
            var req = {};
            req.tableName = TableConstants.emergency_types;
            var params = {};
            params.id = obj.id;
	    params.status ='InActive';
            req.params = params;
            $scope.loader(true);
            console.log(angular.toJson(obj));
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(data) {
                $scope.loader(false);
                if (typeof data == 'string') {
                    data = JSON.parse(data)
                }
                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    var req = {};
                    req.tableName = TableConstants.emergency_types;
                    req.params = {};
                    console.log(angular.toJson(req));
                    request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                        /*$scope.loader(false);*/
                        if (response.statusCode == '300') {
					window.location.hash = "#/Emergency-Type"
					$scope.getEmergencyList();
                           /* $scope.emergencyTypeList = response.Result;
                            $scope.totalItems = $scope.emergencyTypeList.length;
                            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                $scope.tableRows.push($scope.totalItems);

                            if ($scope.totalItems != 0) {
                                $scope.tableRows = $scope.tableRows.sort(request.sort);
                                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                if ($scope.tableRows[1]) {
                                    $scope.viewby = $scope.tableRows[1];
                                } else {
                                    $scope.viewby = $scope.tableRows[0];
                                }
                                $scope.setItemsPerPage($scope.viewby);
                            }*/
                        } else if (response.statusCode == '204') {
                            $scope.notification(response.statusMessage, "danger");
                            $timeout(function() {
                                $scope.logout();

                            }, 2000)
                        }
                    })
                }else if(data.statusCode == '412'){
		$scope.notification(data.statusMessage, "danger");
			
		} else if (data.statusCode == '204'  ) {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else if (data.statusCode == '400' && data.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record", "danger");
	            $scope.getEmergencyList();
                } else {
                    $scope.notification(data.statusMessage);
                }
            });
        }
        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewEmergencyType = addNewEmergencyType;
        $scope.cancelEmergencyType = cancelEmergencyType;
        $scope.saveEmergencyType = saveEmergencyType;
        $scope.updateEmergencyType = updateEmergencyType;
        $scope.editEmergencyType = editEmergencyType;
        $scope.deleteEmergencyType = deleteEmergencyType;
    }
    app.controller('emergencyTypeCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', emergencyTypeCtrl]);
}());
