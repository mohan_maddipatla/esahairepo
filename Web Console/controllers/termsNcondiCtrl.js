(function() {
    var app = angular.module('eSahai');
    var termsNcondiCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout) {
        $scope.tabActive('tNc', 'appSettings');
        $scope.page = {};
        var getTermsAndConditions = function() {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.terms_conditions;
            req.params = {};
            console.log(angular.toJson(req));

            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                console.log(response)
                $scope.loader(false);
                if (response.Result.length) {
                    $scope.terms_and_conditions = response.Result[0].terms_and_conditions;
                    $('#termsAndConditions').html($scope.terms_and_conditions);
                    ctrlComm.put('termsAndConditions', $scope.terms_and_conditions);
                } else {
                    $('#termsAndConditions').html(' <h3 style="text-align:center">No terms and conditions has been added.</h3>');
                }
            })
        }

        if (window.location.hash == "#/Add-New-Terms") {
            $scope.termsAndConditions = null;
            $scope.page.title = "Add T&C";
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Terms&Conditions") {
            $scope.page.title = "Update T&C";
            $scope.page.type = 'put';
            if (ctrlComm.get('termsAndConditions') != undefined) {
                $scope.terms_and_conditions = ctrlComm.get('termsAndConditions');
            } else {
                window.location.hash = '#/Terms-And-Conditions';
            }
        } else if (window.location.hash = "#/Terms-And-Conditions") {

            getTermsAndConditions();
        }

        var addNewTermsCondition = function() {
            window.location.hash = "#/Add-New-Terms"
        }

        var updateTermsConditions = function() {
            window.location.hash = "#/Update-Terms&Conditions"
        }


        var updateCondition = function(terms) {
            if (terms.length >= 100) {

                var req = {};
                req.tableName = TableConstants.terms_conditions;
                var params = {};
                params.terms_and_conditions = angular.copy(terms);
                var conditionalParams = {};
                conditionalParams.userId = terms.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Terms-And-Conditions"
                        $scope.notification(response.statusMessage);
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            } else {
                $scope.notification("Expecting min. of 100 characters for Terms-And-Conditions", "danger")
            }
        }

        var cancelCondition = function() {
            window.location.hash = "#/Terms-And-Conditions"
        }



        $scope.addNewTermsCondition = addNewTermsCondition;
        $scope.updateTermsConditions = updateTermsConditions;
        $scope.cancelCondition = cancelCondition;
        $scope.updateCondition = updateCondition;

    }
    app.controller('termsNcondiCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout', termsNcondiCtrl]);
}());
