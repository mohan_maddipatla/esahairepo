(function() {
    var app = angular.module('eSahai');
    var appVersionCtrl = function($scope, request, TableConstants) {
        $scope.tabActive('appVersion', 'appSettings');
        $scope.app = {};
        $scope.err = {};
        var getAppVersionList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.app_version;
            req.params = {};
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    angular.forEach(response.Result, function(obj) {
                        obj.build_date = obj.build_date.split(' ')[0];
                        if (obj.force_upgrade == 0) {
                            obj.force_upgrade = false;
                        } else if (obj.force_upgrade == 1) {
                            obj.force_upgrade = true;
                        }
                    })
                    $scope.appVersionList = response.Result;
                    $scope.loader(false);
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }


        if (window.location.hash == "#/APP-Version") {
            getAppVersionList();
        }

        var updateAppVersion = function(app) {
            if (app.force_upgrade == true) {
                app.force_upgrade = 1;
            } else {
                app.force_upgrade = 0;
            }
            var req = {};
            req.tableName = TableConstants.app_version;
            var params = {};
            params.force_upgrade = app.force_upgrade;
            params.build_date = app.build_date;
            params.build_os = app.build_os;
            params.version_no = app.version_no;
            params.app_type = app.app_type;
            req.params = params;
            var conditionalParams = {};
            conditionalParams.id = app.id;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    getAppVersionList();
                    $scope.notification("App Version settings updated successfully");
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })

        }

        $scope.cancel = function() {
                $('.collapse').collapse('hide')
            }
            /* var validate = function(app, cb) {
            if (!app.version_no) { $scope.err.version_no = true; } else { delete $scope.err.version_no; }
            if (!app.build_date) { $scope.err.build_date = true; } else { delete $scope.err.build_date; }
            if (!app.build_os) { $scope.err.build_os = true; } else { delete $scope.err.build_os; }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }
*/
        $scope.updateAppVersion = updateAppVersion;
    }

    app.controller('appVersionCtrl', ['$scope', 'request', 'TableConstants', appVersionCtrl]);
}());
