(function() {
    var app = angular.module('eSahai');
    var ambulancesCtrl = function($scope, $rootScope, request, ctrlComm, fileUpload, $filter, TableConstants, $timeout, $firebaseObject, $firebaseArray,statusConstants) {
        console.log("in ambulancesCtrl : " + TableConstants.ambulance_driver);
        /*var ref = new Firebase(URL);
        var obj = $firebaseObject(ref);
        console.log(ref);
        obj.$loaded().then(function() {
        console.log("loaded record:", obj.$id, obj.someOtherKeyInData);

       angular.forEach(obj, function(value, key) {
          console.log(key, value);
       });
     });


     $scope.data = obj;*/
        /* var ref = firebase.database().ref();
         var obj = $firebaseObject(ref.child('BookingId46f249/AmbulanceID-123456'));
         obj.$remove().then(function(ref){
             console.log("delete successfully");
         },function(error){
             console.log("error"+error);
         })*/

        /*obj.$loaded().then(function() {
            console.log(obj)
            angular.forEach(obj, function(value, key) {
                var obj = value;
                if(key == 'AmbulanceID-A125'){
                    obj.$remove().then(function(ref){
                        console.log("reference"+ref);
                    },function(error){
                        console.log("error"+error);
                    })
                 console.log(key, value);
                }

            });
            $scope.data = obj;
        });*/
        /* var a = {
             'latitude': 122222,
             'longitude': 23223,
             'status': false
         }
         firebase.database().ref('/bookingId/').once('value').then(function(snapshot) {
             var val = snapshot.val();
         });*/

        /*list.$add({ foo: "bar" }).then(function(ref){
            console.log("added")
        })*/
        //var ref = firebase.database().ref('/BookingId46f249/AmbulanceID-A126');
        //var obj = $firebaseObject(ref);
        //ref.set({'myKey':a});
        //obj.$bindTo($scope, "data");
        // console.log($scope.data)
        //$scope.data = $firebaseObject(ref);
        //console.log($scope.data)
        $scope.tabActive('ambulance', 'job');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.ambulance = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        //$scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');

        $scope.emergencyTypeOptions = [];
        $scope.selectedEmergencyType = [];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};


        /*$scope.dropdownSetting = {
            scrollable: true,
            scrollableHeight: 'auto',
            showCheckAll: false,
            showUncheckAll: false,
        }*/

        $scope.dropdownSettings = {
            scrollable: true,
            scrollableHeight: '200px',
            showCheckAll: false,
            showUncheckAll: false
        }

        $scope.translationText = {
            buttonDefaultText: '- Select Emergency Type -',
        }

        $scope.getAmbulancesList = function(cb) {
            $scope.loader(true);
            var req = {};
            console.log(angular.toJson(req));
            if ($scope.admin.userType === "GROUP ADMIN") {
                req.groupId = $scope.admin.groupId;
                console.log($scope.admin.groupId)
            }
            request.service('getAmbulanceDriverStatusList', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                console.log(response)
                if (response.statusCode == '300') {
                    //$scope.tempAmbulancesList = [];
                    /*if ($scope.admin.userType === "GROUP ADMIN") {
                        console.log($scope.admin.groupId)
                        angular.forEach(response.responseData, function(obj) {
                            if (obj.driverDetails.group_id == $scope.admin.groupId) {
                                $scope.tempAmbulancesList.push(obj)
                            }
                        })*/
                    /*}else{*/
                    //$scope.tempAmbulancesList = response.responseData;
                    /*}*/
                    $scope.ambulancesList = response.responseData;
                    /*   angular.forEach($scope.ambulancesList, function(obj) {
                           console.log(obj)
                           if (obj.updated_date) {
                               obj.temp_updated_date = obj.updated_date;
                               var t = new Date(obj.updated_date.replace(/-/g, '/')).getTime();
                               obj.updated_date = timeConversion(new Date().getTime() - t) + ' ago';
                           }
                           if (obj.ambulanceDetails) {
                               obj.ambulance_number = obj.ambulanceDetails.ambulance_number;
                               obj.vehicleTypeName = obj.ambulanceDetails.vehicleTypeName;
                           }
                           if (obj.driverDetails) {
                               obj.driver_name = obj.driverDetails.driver_name;
                           }
                       }) */

                    $scope.temp_array = [];
                    angular.forEach(response.responseData, function(obj) {
                        var amb = {};
                        console.log(obj)
                        if (obj.updated_date) {
                            amb.temp_updated_date = obj.updated_date;
                            var t = new Date(obj.updated_date.replace(/-/g, '/')).getTime();
                            amb.updated_date = timeConversion(new Date().getTime() - t) + ' ago';
                        }
                        if (obj.ambulanceDetails) {
                            amb.ambulance_number = obj.ambulanceDetails.ambulance_number;
                            amb.vehicleTypeName = obj.ambulanceDetails.vehicleTypeName;
                        }
                        if (obj.driverDetails) {
                            amb.driver_name = obj.driverDetails.driver_name;
                        }

                        amb.is_booked = obj.is_booked;
                        amb.booking_id = obj.booking_id;
                       // amb.booking_status = obj.booking_status;
                        var status =obj.booking_status;
                
                        amb.booking_status =statusConstants[status];
                        amb.status = obj.status;
                        amb.ambulance_id = obj.ambulance_id;
                        $scope.temp_array.push(amb)
                    })
                    $scope.ambulancesList = angular.copy($scope.temp_array);


                    $scope.totalItems = $scope.ambulancesList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        if (!$scope.userSearch) {
                            $scope.setItemsPerPage($scope.viewby);
                        }
                    }
                    if (cb) cb();
                } else if (response.statusCode == '500') {
                    $scope.ambulancesList = [];
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                }
            })
        }

        function timeConversion(millisec) {
            var seconds = (millisec / 1000).toFixed(1);

            var minutes = (millisec / (1000 * 60)).toFixed(1);

            var hours = (millisec / (1000 * 60 * 60)).toFixed(1);

            var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

            if (seconds < 60) {
                return seconds + " Sec";
            } else if (minutes < 60) {
                return minutes + " Min";
            } else if (hours < 24) {
                return hours + " Hrs";
            } else {
                return days + " Days"
            }
        }

        /*$scope.CategoriesSelected = [{ "id": 1 }, { "id": 2 }];
        $scope.Categoires = [];


        for (var i = 0; i < 10; i++) {
            $scope.Categoires.push({ id: i + 1, label: 'label' + (i + 1) })
        }*/
        if (window.location.hash == "#/Ambulances") {
            $scope.getAmbulancesList();
        } else if (window.location.hash == "#/New-Ambulance-Master") {
            $scope.page.title = 'Add Ambulance';
            $scope.page.type = 'post';

            console.log($scope.Categoires)
            $scope.ambulance.device_type = 'Android';
        } else if (window.location.hash == "#/Update-Ambulance-Master") {
            $scope.page.title = 'Update Ambulance';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('ambulanceObj'));
        } else if (window.location.hash == '#/View-Ambulance-Master') {
            $scope.page.title = 'View Ambulance';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('ambulanceObj'));
        }

        $scope.getEmergencyTypeList(function() {
            angular.forEach($scope.emergencyTypeList, function(obj) {
                $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
            })
        })

        $scope.addAttribute = function() {
            $scope.ambulanceAttr.push({ 'attribute_name': '', 'attribute_details': '' });
        }

        $scope.saveAttribute = function(attrObj, ambulance) {
            console.log(ambulance)
            var req = {};
            req.tableName = TableConstants.ambulance_attributes;
            var params = {};
            params = angular.copy(attrObj);
            params.ambulance_id = ambulance.amb_id;
            req.params = params;
            console.log(angular.toJson(req))
            request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    prePoppulateAttrValues(ambulance)
                    $scope.notification(response.statusMessage);
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                } else {
                    $scope.notification(response.statusMessage);
                }
            })
        }

        $scope.updateAttribute = function(attrObj, ambulance) {
            var req = {};
            req.tableName = TableConstants.ambulance_attributes;
            var params = {};
            params = angular.copy(attrObj);
            delete params['id'];
            delete params['flag'];
            var conditionalParams = {};
            conditionalParams.id = attrObj.id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    prePoppulateAttrValues(ambulance)
                    $scope.notification(response.statusMessage);
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                } else {
                    $scope.notification(response.statusMessage);
                }

            })
        }

        $scope.deleteAttribute = function(attrObj, ambulance) {
            var req = {};
            req.tableName = TableConstants.ambulance_attributes;
            var params = {};
            params.id = attrObj.id;
            req.params = params;
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == '300') {
                    prePoppulateAttrValues(ambulance);
                    $scope.notification(response.statusMessage);
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)
                }
            })
        }

        $scope.editAttribute = function(attrObj, ambulance) {
            angular.forEach($scope.ambulanceAttr, function(obj) {
                if (obj.id == attrObj.id) {
                    obj.flag = 'edit';
                }
            })
        }

        $scope.updateAmbulanceDriverStatus = function(ambulanceObj) {
            var req = {};
            req.ambulanceId = ambulanceObj.ambulance_id;
            req.status = 'Offline';
            request.service('updateAmbulanceDriverStatus', 'post', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300' || response.statusCode == '500') {
                    if (response.statusMessage == 'errPN') {
                        $scope.notification("Status updated successfully but PN not sent");
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                    $scope.getAmbulancesList()
                } else {
                    $scope.notification(response.statusMessage, 'danger');
                }
            })
        }


        function prePoppulateAttrValues(ambulance, cb) {
            var req = {};
            req.tableName = TableConstants.ambulance_attributes;
            req.params = {};
            req.params.ambulance_id = ambulance.amb_id;
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.ambulanceAttr = response.Result;
                    if (cb) {
                        cb()
                    }
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                }
            })
        }



        var addNewAmbulanceMaster = function() {
            window.location.hash = "#/New-Ambulance-Master";
        }

        function prePoppulateValues(ambulance) {
            if (ambulance) {
                if ($scope.page.type == 'put') {
                    ambulance.is_active == 'Active' ? ambulance.is_active = true : ambulance.is_active = false;
                    document.getElementById("imageName").src = ambulance.file_url;
                }
                angular.forEach(ambulance.emergency_type.split(','), function(obj) {
                    $scope.selectedEmergencyType.push({ 'id': obj })
                })
                $scope.ambulance = ambulance;
                console.log($scope.ambulance)
                prePoppulateAttrValues(ambulance, function() {})
            } else {
                window.location.hash = "#/Ambulance-Master";
            }
        }

        var validate = function(cb) {

            if (!$scope.group.email) {
                $scope.err.email = true;
            } else {
                if (!(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/).test($scope.group.email)) {
                    $scope.err.email_cdn = true;

                } else {
                    delete $scope.err.email;
                    delete $scope.err.email_cdn;
                }
            }

            if (!$scope.group.password) {
                $scope.err.password = true;
            } else {
                if (!(/^.{5,8}$/).test($scope.group.password)) {
                    $scope.err.password_cdn = true;

                } else {
                    delete $scope.err.password;
                    delete $scope.err.password_cdn;
                }
            }
            if (!$scope.group.firstName) {
                $scope.err.firstName = true;
            } else {
                delete $scope.err.firstName;
            }

            if (!$scope.group.lastName) {
                $scope.err.lastName = true;
            } else {
                delete $scope.err.lastName;
            }
            if (!$scope.group.displayName) { $scope.err.displayName = true; } else { delete $scope.err.displayName; }
            if (!$scope.group.userType) { $scope.err.userType = true; } else { delete $scope.err.userType; }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }


        var saveAmbulanceMaster = function(ambulance) {
            /*validate(function() {*/
            $scope.loader(true);
            if (ambulance.is_active == true) {
                ambulance.is_active = 'Active';
            } else {
                ambulance.is_active = 'InActive';
            }
            ambulance.emergency_type = [];
            angular.forEach($scope.selectedEmergencyType, function(obj) {
                ambulance.emergency_type.push(obj.id);
            })
            ambulance.emergency_type = ambulance.emergency_type.join();
            var req = {};
            req.tableName = TableConstants.ambulance_master;
            req.params = ambulance;
            console.log(angular.toJson(req));
            fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'insertRecord', $scope.CONFIG, 'post', function(data) {
                $scope.loader(false);
                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    //delete banner['circleIds'];
                    window.location.hash = '#/Ambulance-Master';
                }
            });
            /*var req = {};
            req.tableName = TableConstants.ambulance_master;
            var params = {};
            if (group.is_active == true) {
                group.is_active = 'Active';
            } else {
                group.is_active = 'InActive';
            }
            params = angular.copy(group);
            req.params = params;
            console.log(angular.toJson(req))
            request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    window.location.hash = "#/Ambulance-Master"
                    $scope.notification(response.statusMessage);
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                } else {
                    $scope.notification(response.statusMessage);
                }
            })*/
            /*});*/
        }


        var viewAmbulanceMaster = function(ambulance) {
            ctrlComm.put('ambulanceObj', ambulance);
            window.location.hash = '#/View-Ambulance-Master';
        }

        var updateAmbulanceMaster = function(ambulance) {
            /*validate(function() {*/
            $scope.loader(true);
            /*   var req = {};
               req.tableName = TableConstants.ambulance_master;
               var params = {};
               if (group.is_active == true) {
                   group.is_active = 'Active';
               } else {
                   group.is_active = 'InActive';
               }
               params = angular.copy(group);
               delete params['amb_id'];
               delete params['groupName'];
               var conditionalParams = {};
               conditionalParams.amb_id = group.amb_id;
               req.params = params;
               req.conditionalParams = conditionalParams;
               console.log(angular.toJson(req))
               request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                       $scope.loader(false);
                       if (response.statusCode == '300') {
                           window.location.hash = "#/Group-Master"
                           $scope.notification(response.statusMessage);
                       } else if (response.statusCode == '204') {
                           $scope.notification(response.statusMessage, "danger");
                           $timeout(function() {
                               $scope.logout();
                           }, 2000)

                       } else {
                           $scope.notification(response.statusMessage);
                       }

                   })*/


            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.ambulance_master;
            var params = {};
            if (ambulance.is_active == true) {
                ambulance.is_active = 'Active';
            } else {
                ambulance.is_active = 'InActive';
            }
            params = angular.copy(ambulance);
            delete params['amb_id'];
            delete params['groupName'];
            delete params['vehicleTypeName'];
            delete params['emergencyTypeName'];
            var conditionalParams = {};
            conditionalParams.amb_id = ambulance.amb_id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            req.params = params;
            fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'updateRecord', $scope.CONFIG, 'put', function(data) {
                console.log(data)
                $scope.loader(false);
                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    //delete banner['circleIds'];
                    window.location.hash = '#/Ambulance-Master';
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            });
            /*});*/
        }

        var editAmbulanceMaster = function(ambulance) {
            ctrlComm.put('ambulanceObj', ambulance);
            window.location.hash = "#/Update-Ambulance-Master";
        }


        var cancelAmbulanceMaster = function() {
            window.location.hash = "#/Ambulance-Master";
        }

        var deleteAmbulanceMaster = function(ambulance) {
            var input = {
                text: 'Are you sure you want to delete',
                name: ambulance.display_name
            }
            $scope.confirmation(input, function() {
                confirmDelete(ambulance);
            });
        }

        function confirmDelete(ambulance) {
            console.log(ambulance)
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.ambulance_master;
            var params = {};
            params.amb_id = ambulance.amb_id;
            req.params = params;
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == '300') {
                    $scope.getAmbulanceMasterList(function() {
                        $scope.notification(response.statusMessage);
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)
                }
            })


        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewAmbulanceMaster = addNewAmbulanceMaster;
        $scope.saveAmbulanceMaster = saveAmbulanceMaster;
        $scope.updateAmbulanceMaster = updateAmbulanceMaster;
        $scope.deleteAmbulanceMaster = deleteAmbulanceMaster;
        $scope.cancelAmbulanceMaster = cancelAmbulanceMaster;
        $scope.editAmbulanceMaster = editAmbulanceMaster;
        $scope.viewAmbulanceMaster = viewAmbulanceMaster;
    }


    app.controller('ambulancesCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'fileUpload', '$filter', 'TableConstants', '$timeout', '$firebaseObject', '$firebaseArray','statusConstants', ambulancesCtrl]);
}());
