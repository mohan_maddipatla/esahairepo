(function() {
    var app = angular.module('eSahai');
    var faqCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout) {
        $scope.tabActive('faq', 'appSettings');
        $scope.ubtBtns = {};
        $scope.faq = { faq_type: 'android' };
        $scope.page = {};
        $scope.faqList = [];
        $scope.app_id = "customer";

        var hash = window.location.hash;

        var getFaqList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.faq;
            var params = {};
            /*if ($scope.faq.faq_type == 'android') {
                params.faq_type = 'android';
            } else {
                params.faq_type = 'ios';
            }*/
            req.params = params;
            console.log(req, ":::::::::::::: req")
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(data) {
                $scope.loader(false);
                console.log(data)
                if (data.statusCode == '300') {
                    /*       var faqList = [];
                           angular.forEach(data.Result, function(obj) {
                               if ($scope.faq.faq_type == 'android' && (obj.faq_type == 'android' || obj.faq_type == 'both')) {
                                   faqList.push(obj)

                               } else if ($scope.faq.faq_type == 'ios' && (obj.faq_type == 'ios' || obj.faq_type == 'both')) {
                                   faqList.push(obj)
                               }
                           })*/
                    $scope.faqList = data.Result;
                    $scope.globalList = data.Result;
                    var faqList = [];
                    var faqFilteredList = [];
                    if ($scope.app_id == 'customer') {
                        angular.forEach($scope.globalList, function(obj) {
                            if (obj.app_id == "1") {

                                faqFilteredList.push(obj);
                            }
                        })
                    } else if ($scope.app_id == 'ambulance') {
                        angular.forEach($scope.globalList, function(obj) {
                            if (obj.app_id == "0") {
                                faqFilteredList.push(obj);
                            }
                        })
                    }
                    console.log("faqFilteredList", faqFilteredList)
                    faqList = angular.copy(faqFilteredList);
                    $scope.faqList = faqList;
                    if (cb) cb();
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                } else {
                    $scope.notification(data.ReplyMessage, 'danger');
                }
            });
        }

        if (hash == "#/FAQ") {
            getFaqList();
        } else if (window.location.hash == "#/Add-FAQ") {
            $scope.page.type = 'post'
        } else if (window.location.hash == "#/Update-FAQ") {
            $scope.page.type = 'put'
            if (ctrlComm.get('faqObj')) {
                $scope.faq = ctrlComm.get('faqObj');
                /*                if ($scope.faq.faq_type == 'both') {
                                    $scope.faq.faqType1 = true;
                                    $scope.faq.faqType2 = true;
                                } else if ($scope.faq.faq_type == 'android') {
                                    $scope.faq.faqType1 = true;
                                } else if ($scope.faq.faq_type == 'ios') {
                                    $scope.faq.faqType2 = true;
                                }*/
                if ($scope.faq.app_id == "0") {
                    $scope.faq.app_id = 'ambulance';
                } else if ($scope.faq.app_id == "1") {
                    $scope.faq.app_id = 'customer';
                }


            } else {
                window.location.hash = "#/FAQ"
            }
        }


        var validate = function(cb) {
            /*    if (!$scope.faq.question && !$scope.faq.answer && !$scope.faq.faqType1 && !$scope.faq.faqType2) {
                    $scope.notification('Enter question,answer and type.', 'danger');
                } else if (!$scope.faq.question) {
                    $scope.notification('Enter question', 'danger');
                } else if (!$scope.faq.answer) {
                    $scope.notification('Enter answer.', 'danger');
                } else if (!$scope.faq.faqType1 && !$scope.faq.faqType2) {
                    $scope.notification('Select type.', 'danger');
                } else if (cb) {
                    cb();
                }*/
            if (!$scope.faq.question && !$scope.faq.answer && !$scope.faq.app_id) {
                $scope.notification('Enter question,answer , answer and app.', 'danger');
            } else if (!$scope.faq.question) {
                $scope.notification('Enter question', 'danger');
            } else if (!$scope.faq.answer) {
                $scope.notification('Enter answer.', 'danger');
            } else if (!$scope.faq.app_id) {
                $scope.notification('Enter app.', 'danger');
            } else if (cb) {
                cb();
            }
        }

        var saveFAQ = function(faq) {
            console.log("in saveFAQ", faq)
                /*   if (faq.faqType1 == true && faq.faqType2 == true) {
                       faq.faq_type = "both";
                   } else if ($scope.faq.faqType1 == true) {
                       faq.faq_type = "android";
                   } else if ($scope.faq.faqType2 == true) {
                       faq.faq_type = "ios";
                   }*/
            faq.faq_type = "android";
            if (faq.app_id == 'customer') {
                faq.app_id = '1';
            } else if (faq.app_id == 'ambulance') {
                faq.app_id = '0';
            }

            var req = {};
            req.tableName = TableConstants.faq;
            var params = {};
            params = angular.copy(faq);
            delete params['faqType1']
            delete params['faqType2']
            req.params = params;
            console.log(angular.toJson(req))
            validate(function() {
                $scope.loader(true);
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = "#/FAQ"
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(data.statusMessage, 'danger');
                    }
                });
            });
        }


        var addFAQ = function() {
            window.location.hash = "#/Add-FAQ"
        }
        var updateFAQ = function(faq) {
            /*     if ($scope.faq.faqType1 == true && $scope.faq.faqType2 == true) {
                     $scope.faq.faq_type = "both";
                 } else if ($scope.faq.faqType1 == true) {
                     $scope.faq.faq_type = "android";
                 } else if ($scope.faq.faqType2 == true) {
                     $scope.faq.faq_type = "ios";
                 }*/

            if (faq.app_id == 'customer') {
                faq.app_id = '1';
            } else if (faq.app_id == 'ambulance') {
                faq.app_id = '0';
            }
            $scope.faq.faq_type = "android";

            var req = {};
            req.tableName = TableConstants.faq;
            var params = {};
            params = angular.copy($scope.faq);
            req.params = params;
            delete params['faqType1']
            delete params['faqType2']
            delete params['faq_id']
            var conditionalParams = {};
            console.log("faq" + faq)
            conditionalParams.faq_id = faq.faq_id;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            validate(function() {
                $scope.loader(true);
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = "#/FAQ"
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(data.statusMessage, 'danger');
                    }
                });
            });
            /*if (faq.faq_type == "4") {

                $scope.faq.faqType1 = true;
                $scope.faq.faqType2 = true;
            } else if (faq.faq_type == "0") {
                $scope.faq.faqType1 = true;
            } else if (faq.faq_type == "1") {
                $scope.faq.faqType2 = true;
            }

            faq.faqType1 = $scope.faq.faqType1;
            faq.faqType2 = $scope.faq.faqType2;
            ctrlComm.put('updateFAQ', faq);
            window.location.hash = "#/Update-FAQ"*/
        }

        var editFAQ = function(faqObj) {
            ctrlComm.put('faqObj', faqObj);
            console.log(ctrlComm.get('faqObj'))
            window.location.hash = '#/Update-FAQ';
        }
        var cancelFAQ = function() {
            window.location.hash = "#/FAQ"
        }

        $scope.showUbtBtns = function(key) {
            ($scope.ubtBtns[key] == true) ? ($scope.ubtBtns[key] = false) : ($scope.ubtBtns = {}, $scope.ubtBtns[key] = true);
        }

        var deleteFAQ = function(faq) {
            var input = {
                text: 'Are you sure you want to delete',
                name: ''
            }
            $scope.confirmation(input, function() {
                confirmDelete(faq);
            });
        }

        function confirmDelete(faq) {

            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.faq;
            var params = {};
            params.faq_id = faq.faq_id;
            req.params = params;
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(data) {
                $scope.loader(false);
                if (typeof data == 'string') {
                    data = JSON.parse(data)
                }
                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    getFaqList();
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                } else {
                    $scope.notification(data.statusMessage);
                }
            });
        }



        $scope.saveFAQ = saveFAQ;
        $scope.addFAQ = addFAQ;
        $scope.updateFAQ = updateFAQ;
        $scope.cancelFAQ = cancelFAQ;
        $scope.deleteFAQ = deleteFAQ;
        $scope.getFaqList = getFaqList;
        $scope.editFAQ = editFAQ;
    }
    app.controller('faqCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout', faqCtrl]);
}());
