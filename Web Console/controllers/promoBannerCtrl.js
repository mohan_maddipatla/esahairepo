(function() {
    var app = angular.module('eSahai');
    var promoBannerCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout, fileUpload, $filter) {
        $scope.tabActive('promoBanner');
        $scope.pb = {}
        $scope.err = {};
        $scope.page = {};
        $scope.promo = {};
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.customer = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {};
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');

        $scope.getPromosList = function(cb) {
            $scope.loader(true);
            var req = {};
            var dateFilter = {};
            req.tableName = "promo_banners";
            req.params = {};
            dateFilter.fieldName = 'created_date';
            dateFilter.startDate = $scope.fromDate;
            if ($scope.fromDate != $filter('date')(new Date(), 'yyyy-MM-dd')) {
                dateFilter.endDate = $filter('date')(new Date(new Date($scope.toDate).getTime() + (24 * 60 * 60 * 1000)), 'yyyy-MM-dd');
            }

            req.dateFilter = dateFilter;
            var orderByParams = {};
            orderByParams.created_date = '';
            req.orderByParams = orderByParams;
            console.log(angular.toJson(req));
            request.service('getPromos', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.promosList = response.Result;
                    $scope.totalItems = $scope.promosList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }
        $scope.cancelImport = function() {
            $scope.upldfile = {};
            $('#uploadFile').val("");
        }
        var hash = window.location.hash;
        if (window.location.hash == "#/Promo-Banners") {
            $scope.getPromosList();
        } else if (hash == "#/New-Promo-Banner") {
            $scope.page.title = 'Add New Banner';
            $scope.page.type = 'post';
        } else if (hash == "#/Update-Promo-Banner") {
            $scope.page.title = 'Update Banner'
            $scope.page.type = 'put'
            prePoppulateValues(ctrlComm.get('pbObj'));
        } else if (window.location.hash == "#/View-Promo-Banner") {
            $scope.page.title = 'View Promo Banners';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('promoObj'));
        }

        $('#uploadFile').change(function() {
            readURL();
        })

        function readURL() {
            var file = $('#uploadFile')[0].files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imageName').attr('src', e.target.result);
                }

                reader.readAsDataURL(file);
            }

        }
        var addNewBanner = function() {
            window.location.hash = '#/New-Promo-Banner';
        }
        $scope.chooseFile = function() {
            var a = document.getElementById("uploadFile").click();

        }

        $('#uploadFile').change(function() {
            readURL();
        })

        var editPromo = function(pb) {
            ctrlComm.put('pbObj', pb);
            window.location.hash = '#/Update-Promo-Banner';
        }

        function prePoppulateValues(pb) {
            if (pb) {
                console.log("promo", pb)

                if ($scope.page.type == 'put') {
                    document.getElementById("imageName").src = pb.banner_path;
                }
               // pb.status == 'Active' ? pb.status = true : pb.status = false;
                pb.status= true;
                $scope.promo = pb;
                $scope.pb = pb;
                $scope.pb.fromDate = $filter('date')(new Date(pb.start_date), 'yyyy-MM-dd');
                $scope.pb.toDate = $filter('date')(new Date(pb.end_date), 'yyyy-MM-dd');


            } else {
                window.location.hash = '#/Promo-Banners';
            }
        }
        var saveBanner = function(pb) {
            pb.start_date = $filter('date')(new Date(pb.fromDate), 'yyyy-MM-dd');

            pb.end_date = $filter('date')(new Date(pb.toDate), 'yyyy-MM-dd');
            if (pb.status == true) {
                pb.status = 'Active';
            } else {
                pb.status = 'InActive';
            }
            validate(function() {
                date(function() {

                    $scope.loader(true);
                    var file = $scope.image_url;
                    if (file && (file.type == "image/jpeg" || file.type == "image/png") && file != undefined) {

                        fileUpload.uploadFileToUrl(file, 'banner', pb, 'uploadBanner', $scope.CONFIG, 'post', function(data) {
                            $scope.loader(false);
                            if (data.statusCode == 300) {
                                $scope.notification(data.statusMessage);
                                $('#uploadFile').val("");


                                window.location.hash = '#/Promo-Banners';
                                $scope.upldfile = {};
                            }
                        });
                    } else {
                        $scope.loader(false);

                        $scope.notification('plese upload .jpg or .png', 'danger');

                    }
                });
            });

        }
        var updatePromoBanner = function(pb) {
            console.log("promo", pb)
		pb.status=true;
            pb.start_date = $filter('date')(new Date(pb.fromDate), 'yyyy-MM-dd');

            pb.end_date = $filter('date')(new Date(pb.toDate), 'yyyy-MM-dd');
            if (pb.status == true) {
                pb.status = 'Active';
            } else {
                pb.status = 'InActive';
            }
            validate(function() {
                date(function() {
                    $scope.loader(true);
                    var file = $scope.image_url;
                    var req = {};
                    var params = {};

                    params.start_date = pb.start_date;
                    params.end_date = pb.end_date;
                    params.status = pb.status;
                    params.offerid = pb.offerid;
                    req.params = params;
                    request.service('updatePromoBanner', 'post', req, $scope.CONFIG, function(data) {

                        $scope.loader(false);
                        if (data.statusCode == 300) {
                            $scope.notification(data.statusMessage);
                            $('#uploadFile').val("");


                            window.location.hash = '#/Promo-Banners';
                            $scope.upldfile = {};
                        } else {
                            $scope.notification(data.statusMessage);

                        }
                    });

                });
            });


        }
        var validate = function(cb) {
            if ($scope.page.type == 'post') {

                if (!$scope.pb.title) { $scope.err.title = true; } else {
                    if (!(/[a-zA-Z]+/).test($scope.pb.title)) {
                        $scope.err.title_cdn = true;
                    } else {
                        if ($scope.pb.title.length > 255) {
                            $scope.err.title_cdn1 = true;

                        } else {
                            delete $scope.err.title;
                            delete $scope.err.title_cdn;
                            delete $scope.err.title_cdn1;

                        }

                    }
                }
                if (!$scope.pb.description) { $scope.err.description = true; } else {
                    if ($scope.pb.description.length > 255) {
                        $scope.err.description_cdn = true;

                    } else {
                        delete $scope.err.description;
                        delete $scope.err.description_cdn;
                    }
                }
            }
            if (!$scope.pb.fromDate) { $scope.err.fromDate = true; } else { delete $scope.err.fromDate; }
            if (!$scope.pb.toDate) { $scope.err.toDate = true; } else { delete $scope.err.toDate; }


            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();

            }
        }



        var date = function(cb) {
            $scope.pb.fromDate = $filter('date')(new Date($scope.pb.fromDate), 'yyyy-MM-dd');

            $scope.pb.toDate = $filter('date')(new Date($scope.pb.toDate), 'yyyy-MM-dd');

            if ($scope.pb.toDate < $scope.pb.fromDate) {
                $scope.notification('End Date should be less than Start Date', 'danger')
            } else {

                if (cb) cb();
            }

        }

        $(document).ready(function() {

            $('#enddate').datepicker('setStartDate', $scope.fromDate);

            $("#startdate").datepicker({}).on('changeDate', function(selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#enddate').datepicker('setStartDate', minDate);
            });


        });
        var viewPromos = function(promo) {
            ctrlComm.put('promoObj', promo);
            window.location.hash = '#/View-Promo-Banner';
        }


        $scope.cancel = function() {
            window.location.hash = '#/Promo-Banners'
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        $scope.addNewBanner = addNewBanner;
        $scope.saveBanner = saveBanner;
        $scope.editPromo = editPromo;
        $scope.viewPromo = viewPromos;
        $scope.updatePromoBanner = updatePromoBanner;



    }
    app.controller('promoBannerCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout', 'fileUpload', '$filter', promoBannerCtrl]);
}());

