(function() {
    var app = angular.module('eSahai');
    var geoHashesCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout) {
        $scope.tabActive('geoHashes', 'appSettings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.page = {};
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.geoHash = {};
        $scope.err = {};
        $scope.getGeoHashesList = function(cb) {
            $scope.loader(true);
            var req = {};

            req.tableName = TableConstants.geo_hashes;
            req.params = {};

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {

                    $scope.geoHashList = response.Result;
                    $scope.totalItems = $scope.geoHashList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }


        if (window.location.hash == "#/Geo-Hashes") {
            $scope.page.title = 'Geo Hashes';
            $scope.getGeoHashesList();

        } else if (window.location.hash == "#/Add-GeoHashes") {
            $scope.page.title = 'Add Geo Hashes';
            $scope.page.type = 'post';

        } else if (window.location.hash == "#/Update-GeoHashes") {
            $scope.page.title = 'Update GeoHashes';
            $scope.page.type = 'put';
            console.log(ctrlComm.get('geoHashObj'))
            prePoppulateValues(ctrlComm.get('geoHashObj'));
        } else if (window.location.hash == "#/View-GeoHashes") {
            $scope.page.title = 'View GeoHashes';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('geoHashObj'));
        }
        var addNewGeoHash = function() {
            window.location.hash = "#/Add-GeoHashes";
        }

        var viewGeoHash = function(geoHash) {
            ctrlComm.put('geoHashObj', geoHash);
            window.location.hash = '#/View-GeoHashes';
        }

        function prePoppulateValues(geoHash) {
            console.log("geoHash" + angular.toJson(geoHash))
            if (geoHash) {
                console.log("sddddddddddddd")


                $scope.geoHash = geoHash;
            } else {
                console.log("sddddddddddddd")
                window.location.hash = "#/Geo-Hashes";
            }
        }
        var saveGeoHash = function(geoHash) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.geo_hashes;
                var params = {};

                params = angular.copy(geoHash);;

                req.params = params;
                console.log(angular.toJson(req));
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);

                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = '#/Geo-Hashes';
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }


        var updateGeoHash = function(geoHash) {
            validate(function() {
                console.log(geoHash)
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.geo_hashes;
                var params = {};

                params = angular.copy(geoHash);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = geoHash.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = '#/Geo-Hashes';
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }



        var validate = function(cb) {
            if (!$scope.geoHash.geo_hash) {
                $scope.err.geo_hash = true;
            } else {
                delete $scope.err.geo_hash;
            }
          /*  if (!$scope.geoHash.code) {
                $scope.err.code = true;
            } else {
                delete $scope.err.code;
            }
*/

            if (Object.keys($scope.err).length == 0) {


                if (cb) cb();
            }
        }
        var editGeoHash = function(geoHash) {
            ctrlComm.put('geoHashObj', geoHash);
            window.location.hash = "#/Update-GeoHashes";
        }


        var cancelGeoHash = function() {
            window.location.hash = "#/Geo-Hashes";
        }

 /*       var deleteCountry = function(geoHash) {
            var input = {
                text: 'Are you sure you want to delete',
                name: geoHash.geoHash
            }
            $scope.confirmation(input, function() {
                confirmDelete(geoHash);
            });
        }

        function confirmDelete(geoHash) {

            var req = {};
            req.tableName = TableConstants.country_list;
            var params = {};
            params.id = geoHash.id;
            req.params = params;
            console.log(angular.toJson(req))
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == "300") {
                    console.log("ddddddddddddddd")
                    $scope.notification(response.statusMessage);
                    $scope.getCountryList();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })


        }
*/
        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewGeoHash = addNewGeoHash;
        $scope.saveGeoHash = saveGeoHash;
        $scope.updateGeoHash = updateGeoHash;
         $scope.cancelGeoHash = cancelGeoHash;
        $scope.editGeoHash = editGeoHash;
        $scope.viewGeoHash = viewGeoHash


    }
    app.controller('geoHashesCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout', geoHashesCtrl]);
}());
