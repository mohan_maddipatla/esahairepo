(function() {
    var app = angular.module('eSahai');
    var bookingCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout, geohash, statusConstants) {
        console.log("tableName: " + TableConstants.emergency_booking);
        $scope.tabActive('booking', 'job');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.user = {};
        $scope.err = {};
        $scope.booking = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.isCustomerExists = false;
        $scope.othersFlag = false;
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};
        $(document).ready(function() {

            $('#enddate').datepicker('setStartDate', $scope.fromDate);
            $("#startdate").datepicker({}).on('changeDate', function(selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#enddate').datepicker('setStartDate', minDate);
            });


        });
        $scope.InitOthers = function(bookedBy) {
                if (bookedBy == '0') {
                    $scope.othersFlag = true;
                } else {
                    $scope.othersFlag = false;
                }
            }
            /* var ref = firebase.database().ref('/Bookings');
             var arrObj = $firebaseArray(ref);*/

        /*arrObj.$loaded().then(function() {

            // To iterate the key/value pairs of the object, use angular.forEach()
            angular.forEach(arrObj, function(value, key) {
                //console.log(key, value);
            });
        });*/

        var basic_phone = '';
        $scope.$watch('booking.customer_mobile', function(val) {
            if (val) {
                if (!val.toString().match(/^[0-9]*$/)) {
                    $scope.booking.customer_mobile = basic_phone;
                } else {
                    if (val.length <= 10) {
                        basic_phone = val;
                    } else {
                        $scope.booking.customer_mobile = basic_phone;
                    }
                }
            }
        });
        $('#customer_mobile').on('click', function() {
            $scope.customer = 'false';

        })



        $scope.getBookingsList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.emergency_booking;
            req.params = {};
            if ($scope.admin.userType === "GROUP ADMIN") {
                req.params.group_id = $scope.admin.groupId;
            }
            var dateFilter = {};
            dateFilter.fieldName = 'created_datetime';
            dateFilter.startDate = $scope.fromDate;
            // dateFilter.endDate = $scope.toDate;
            if ($scope.fromDate != $filter('date')(new Date(), 'yyyy-MM-dd')) {
                //        dateFilter.endDate = $scope.toDate;
                dateFilter.endDate = $filter('date')(new Date(new Date($scope.toDate).getTime() + (24 * 60 * 60 * 1000)), 'yyyy-MM-dd');
            }
            req.dateFilter = dateFilter;
            var orderByParams = {};
            orderByParams.created_datetime = '';
            req.orderByParams = orderByParams;

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    $scope.bookingsList = [];
                    $scope.tempBookingsList = response.Result;
                    angular.forEach($scope.tempBookingsList, function(obj) {
                      angular.forEach($scope.globalStatusList, function(obj1) {
                            if (obj.booking_status == obj1.short_code) {

                                obj.booking_status = obj1.status;

                            }
                        })
                        var req1 = {};
                        req1.tableName = TableConstants.tracking;
                        var params = {};
                        params.booking_id = obj.booking_id;
                        req1.params = params;
                        var orderByParams = {};
                        orderByParams.created_date = '';
                        req1.orderByParams = orderByParams;
                        /*    request.service('fetchDataByField', 'post', req1, $scope.CONFIG, function(response) {
                                if (response.statusCode == '300') {
                                    if (response.Result[0] != undefined && response.Result[0].booking_status != undefined) {
                                        obj.booking_status = response.Result[0].booking_status;
                                    }
                                }
                            })*/
                        //console.log(obj)
			obj.booking_time=obj.created_datetime;
                        if (obj.created_datetime) {
                            obj.temp_created_datetime = obj.created_datetime;
                            var dateTimer = (obj.updated_datetime != undefined && obj.updated_datetime != null) ? obj.updated_datetime : obj.created_datetime;
                            if (timeConversion(new Date().getTime() - new Date(dateTimer.replace(/-/g, '/')).getTime()).split(' ')[1] == 'Min') {
                                obj.countingTime = parseFloat(timeConversion(new Date().getTime() - new Date(dateTimer.replace(/-/g, '/')).getTime()).split(' ')[0]);
                                if (obj.countingTime > 15.00) {
                                    //console.log("booking cancelled for"+obj.booking_id)
                                   // if (obj.booking_status == "No Response" || obj.booking_status == "No Ambulance" || obj.booking_status == "No Response on transfer") {
                                    if (obj.booking_status == statusConstants.s01 || obj.booking_status == statusConstants.s02 || obj.booking_status == statusConstants.s07 ) {
                                        obj.Notification = true;
                                        $scope.cancelBooking(obj);
                                    }
                                }
                            } else {
                                if (timeConversion(new Date().getTime() - new Date(dateTimer.replace(/-/g, '/')).getTime()).split(' ')[1] != 'Sec') {
                                  //  if (obj.booking_status == "No Response" || obj.booking_status == "No Ambulance" || obj.booking_status == "No Response on transfer") {
                                    if (obj.booking_status == statusConstants.s01 || obj.booking_status == statusConstants.s02 || obj.booking_status == statusConstants.s07) {
                                        obj.Notification = true;
                                        $scope.cancelBooking(obj);
                                    }
                                    obj.countingTime = 20.00;

                                } else {
                                    obj.countingTime = 0.00;
                                }
                                //cancel expired bookings
                                /* var cancelObj = {
                                     customer_id:obj.customer_id,
                                     booking_id:obj.booking_id,

                                 }*/
                                /*if(obj.booking_status == "No Response" || obj.booking_status=="No Ambulance" || obj.booking_status=="No Response on transfer"){
                                    $scope.cancelBooking(obj);
                                }*/
                                //$scope.cancelBooking(obj);
                            }
                            obj.created_datetime = timeConversion(new Date().getTime() - new Date(obj.created_datetime.replace(/-/g, '/')).getTime()) + ' ago';
                            if (obj.updated_datetime != undefined) {
                                obj.temp_updated_datetime = obj.updated_datetime;
                                obj.updated_datetime = timeConversion(new Date().getTime() - new Date(obj.updated_datetime.replace(/-/g, '/')).getTime()) + ' ago';

                            }
                        }
                        if (obj.distance) {
                            obj.distance = (obj.distance / 1000).toFixed(2);
                        }
                        /* $http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + parseFloat(obj.emergency_latitude) + ',' + parseFloat(obj.emergency_longitude)).then(function(response) {
                                 if (response.data.status == 'OK') {
                                     obj.location = response.data.results[0].formatted_address;
                                 }
                             })*/
                        /* var params = {};
                         var temp = {};
                         temp.latitude = obj.emergency_latitude;
                         temp.longitude = obj.emergency_longitude;
                         temp.status = obj.booking_status;
                         params[obj.booking_id] = temp;
                         arrObj.$add(params).then(function(response) {
                             //console.log("response" + response);
                         });*/


                    })
                    if (request.getItem('booking_filter') != undefined) {
                        angular.forEach($scope.tempBookingsList, function(obj) {
                       //     if (request.getItem('booking_filter') == 'completed' && obj.booking_status === 'Trip Closed') {
                            if (request.getItem('booking_filter') == 'completed'  && obj.booking_status === statusConstants.sh03) {
                                $scope.bookingsList.push(obj);
                            }

                         //   if (request.getItem('booking_filter') == 'cancelled' && (obj.booking_status === 'Cancelled (Fake)' || obj.booking_status === 'Cancelled')) {
                            if (request.getItem('booking_filter') == 'cancelled' && (obj.booking_status === statusConstants.sh04 || obj.booking_status === statusConstants.sh05 )) {
                                $scope.bookingsList.push(obj);
                            }

                    //        if (request.getItem('booking_filter') == 'inprogress' && !(obj.booking_status === 'Trip Closed' || obj.booking_status === 'Cancelled (Fake)' || obj.booking_status === 'Cancelled')) {
                            if (request.getItem('booking_filter') == 'inprogress' && !(obj.booking_status === statusConstants.sh03 || obj.booking_status === statusConstants.sh04 || obj.booking_status === statusConstants.sh05 )) {
                                $scope.bookingsList.push(obj);
                            }


                        })
                    } else {
                        $scope.bookingsList = $scope.tempBookingsList;
                    }
                    $scope.loader(false);
                    $scope.totalItems = $scope.bookingsList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }


                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                } else {
                    $scope.usersList = [];
                }
            })
        }

        $scope.refreshBooking = function() {
            $scope.getBookingsList();
        }

        function timeConversion(millisec) {
            var seconds = (millisec / 1000).toFixed(1);

            var minutes = (millisec / (1000 * 60)).toFixed(1);

            var hours = (millisec / (1000 * 60 * 60)).toFixed(1);

            var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

            if (seconds < 60) {
                return seconds + " Sec";
            } else if (minutes < 60) {
                return minutes + " Min";
            } else if (hours < 24) {
                return hours + " Hrs";
            } else {
                return days + " Days"
            }
        }

        $scope.createCustomerBtn = false;
        $scope.verifyCustomer = function(booking) {
            validate(function() {
                $scope.loader(true);
                var req = {};
              //  req.customerMobile = booking.customer_mobile;
	      //for prefixing country code 
                var a = booking.country_code.split('+')[1]
                console.log("a.a is" + a)

                req.customerMobile = a + booking.customer_mobile;
                request.service('checkCustomerExistsOrNot', 'post', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    console.log(response)
                    if (response.statusCode == '300') {
                        $scope.notification(response.statusMessage);
                        $scope.booking.customer_name = response.responseData.customer_name;
                        $scope.booking.customer_id = response.responseData.customer_id;
                     //   $scope.booking.customer_mobile = booking.customer_mobile;
                        ctrlComm.put('customerNumber', booking.customer_mobile);
                        $scope.booking.customer_age = response.responseData.age;//for hidden age
                        var req1 = {};
                        request.service('getAmbulanceTypes', 'post', req1, $scope.CONFIG, function(response1) {
                            if (response1.statusCode == '300') {
                                $scope.isCustomerExists = true;
                                $scope.ambulanceTypes = response1.responseData;
                                $scope.booking.group_type_id = $scope.ambulanceTypes[0].id;
                            }
                        })
                    } else if (response.statusCode == '411' && response.statusMessage === 'No customer exists with this mobile number') {
                        $scope.notification('No customer exists with this mobile number,Please enter details to create new', "danger");
                        /*To get customer_mobile prepoppulated*/
                        ctrlComm.put('customerNumber', booking.customer_mobile);
                        /* to remove customr name if not exists*/
                        $scope.booking.customer_name = '';
                        /*To enable create customer button if no customer exists*/
                        $scope.customer = 'true';
                        $scope.createCustomerBtn = true;
                        $scope.page.title = 'Add Customer'

                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        ctrlComm.put('customerNumber', booking.customer_mobile);
                        if (response.statusMessage == 'This customer is already on trip') {
                            $scope.isCustomerExists = false;
                            $scope.customer = 'false';
                            //$scope.createCustomerBtn = false;    
                            $scope.notification(response.statusMessage, "danger");
                        } else {
                            $scope.isCustomerExists = false;
                            $scope.createCustomerBtn = true;
                            $scope.notification(response.statusMessage, "danger");

                        }
                    }
                })
            })
        }

        /*$scope.getNearByAmbulances = function(booking) {
            var req = {};
            req.emergencyGeoHash = geohash.encode(booking.emergency_latitude, booking.emergency_longitude);
            req.latitude = booking.emergency_latitude;
            req.longitude = booking.emergency_longitude;
            req.groupType = booking.groupType;
            request.service('getNearByHospitalList', 'post', req, $scope.CONFIG, function(response) {
                    console.log(response)
                if (response.statusCode == '300') {
                }
            })
        }*/




        var validate = function(cb) {
            if (!$scope.booking.customer_mobile) {
                $scope.err.customer_mobile = true;
            } else {
                if (!(/^\d{10}$/).test($scope.booking.customer_mobile)) {
                    $scope.err.customer_mobile_cdn = true;

                } else {
                    delete $scope.err.customer_mobile;
                    delete $scope.err.customer_mobile_cdn;
                }

            }
            console.log(Object.keys($scope.err).length)
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }


        $scope.trackBooking = function(obj) {
            window.localStorage.setItem('booking_id', obj.booking_id);
            window.location.hash = '#/Map';
        }









        if (window.location.hash == "#/Booking") {
            $scope.getBookingsList();
        } else if (window.location.hash == "#/New-Booking") {
            $scope.page.title = 'Add Booking';
            $scope.page.type = 'post';
            $scope.booking.reason = 'No Internet';
            $scope.booking.country_code = '+91';
        } else if (window.location.hash == "#/Update-User") {
            $scope.page.title = 'Update User';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('userObj'));
        } else if (window.location.hash == '#/View-User') {
            $scope.page.title = 'View User';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('userObj'));
        } else if (window.location.hash == '#/Assign-Booking') {

            prePoppulateBookingValue(ctrlComm.get('bookingObj'));
        }

        var viewUser = function(user) {
            ctrlComm.put('userObj', user);
            window.location.hash = '#/View-User';
        }

        var map;

        function initMap(bookingObj, ambulance) {
            console.log(bookingObj);
            console.log(ambulance);
            console.log(ambulance.length);
            console.log(ambulance instanceof Array)
            console.log(typeof ambulance);
            if (!ambulance.length && ambulance instanceof Array == false) {
                calculateAndDisplayRoute(bookingObj, ambulance);
            } else if (ambulance.length > 0) {
                $scope.markerObj = [];
                angular.forEach(ambulance, function(obj) {
                    if ($scope.center == undefined) {
                        $scope.center = parseFloat(obj.latitude) + ', ' + parseFloat(obj.longitude);
                    }
                    var params = {};
                    params.pos = parseFloat(obj.latitude) + ', ' + parseFloat(obj.longitude);
                    params.id = obj.id;
                    params.ambulance_number = obj.ambulance_number;
                    params.image = 'images/marker_ambulance.png';
                    params.driver_name = obj.driverDetails.driver_name;
                    params.group_name = obj.group_name;
                    params.vehicleTypeName = obj.vehicleTypeName;
                    $scope.markerObj.push(params)
                })
            } else {

                if (ctrlComm.get('action') == undefined || ctrlComm.get('action') != 'transfer') {
                    var req = {};
                    req.tableName = TableConstants.emergency_booking;
                    var params = {};
                    //params.booking_status = 'No Ambulance';
                    params.booking_status = statusConstants.s02;
                    var conditionalParams = {};
                    conditionalParams.booking_id = bookingObj.booking_id;
                    req.params = params;
                    req.conditionalParams = conditionalParams;
                    console.log(angular.toJson(req))
                    request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                        if (response.statusCode == '300') {

                        }

                    })
                }

                $scope.notification("No Ambulances Found", 'danger');
            }
        }

        function calculateAndDisplayRoute(bookingObj, ambulanceObj) {
            console.log(bookingObj)
            console.log(ambulanceObj)
            $scope.directionObj = {};
            var origin = [];
            var destination = [];
            origin.push(ambulanceObj.latitude, ambulanceObj.longitude);
            destination.push(bookingObj.emergency_latitude, bookingObj.emergency_longitude);
            $scope.directionObj.origin = origin;
            $scope.directionObj.destination = destination;
            $scope.customMarkerList = [{
                pos: origin,
                image: {
                    url: 'images/marker_ambulance.png',

                },
                state: 'ambulance',
                driver_name: ambulanceObj.driverDetails.driver_name,
                ambulance_number: ambulanceObj.ambulance_number,
                vehicle_type: ambulanceObj.vehicleTypeName,
                driver_mobile: ambulanceObj.driverDetails.driver_mobile_number

            }, {
                pos: destination,
                image: {
                    url: 'images/emergency.png',

                },
                state: 'emergency',
                customer_name: bookingObj.customer_name,
                emergency_type: bookingObj.emergency_type,
                customer_mobile: bookingObj.customer_mobile
            }]
        }

        /*function prePoppulateBookingValue(bookingObj) {
            if (bookingObj != undefined) {
                var params = {};
                var response = {}
                params.latitude = bookingObj.emergency_latitude;
                params.longitude = bookingObj.emergency_longitude;
                //request.service('getNearByAmbulanceList', 'post', params, $scope.CONFIG, function(response) {
                    response.responseData = [{"id":"50","ambulance_id":"6","driver_id":"4","status":"Online","created_date":"2016-09-29 17:16:17","updated_date":"2016-09-29 17:24:24","longitude":"78.3867","latitude":"17.4343","geoHashCode":null,"distance":4.0225,"duration":"11 mins"},{"id":"51","ambulance_id":"6","driver_id":"4","status":"Online","created_date":"2016-09-29 17:16:31","updated_date":null,"longitude":"78.3859558","latitude":"17.4442056","geoHashCode":null,"distance":0.48269999999999996,"duration":"2 mins"}]


                    $scope.nearAmbulanceList = response.responseData
                    angular.forEach($scope.nearAmbulanceList,function(obj){
                        angular.forEach($scope.globalAmbulanceList,function(obj1){
                            if(obj.ambulance_id == obj1.amb_id){
                                obj.ambulance_number = obj1.ambulance_number;
                            }
                        })
                    })
                    $scope.amb_obj = JSON.stringify(response.responseData[0])
                    $scope.amb_obj1 = JSON.parse($scope.amb_obj)
                    initMap(bookingObj, $scope.nearAmbulanceList[0]);
                //})

            } else {
                window.location.hash = '#/Booking';
            }
        }*/

        function prePoppulateBookingValue(bookingObj) {

            console.log(angular.toJson(bookingObj))
            $scope.loader(true);
            $scope.bookingObj = bookingObj;
            console.log(angular.toJson($scope.bookingObj))
            if ($scope.bookingObj != undefined) {
                var params = {};
                params.latitude = $scope.bookingObj.emergency_latitude;
                params.longitude = $scope.bookingObj.emergency_longitude;
                params.emergencyGeoHash = $scope.bookingObj.emergency_geoHashCode;
                params.group_id = $scope.bookingObj.group_id;
                params.emergency_type = $scope.bookingObj.emergency_type_id;
                params.group_type_id = $scope.bookingObj.group_type_id;
                /*params.commandCenter = true;
                params.booking_id = $scope.bookingObj.booking_id;*/
                console.log(angular.toJson(params))
                request.service('getNearByAmbulanceListSpecificType', 'post', params, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        $scope.nearAmbulanceList = response.responseData;
                        console.log($scope.nearAmbulanceList);
                        $scope.ambObj = [];
                        /* var params = {};
                         params.pos = parseFloat(response.Result[0].latitude) + ', ' + parseFloat(response.Result[0].longitude);
                         params.id = response.Result[0].id;
                         params.bookingStatus = response.Result[0].booking_status;
                         params.image = 'images/ambulance.png';
                         $scope.moverObj.push(params)*/
                        angular.forEach($scope.nearAmbulanceList, function(obj) {
                            angular.forEach($scope.globalAmbulanceList, function(obj1) {
                                if (obj.ambulance_id == obj1.amb_id) {
                                    obj.ambulance_number = obj1.ambulance_number + ' (~' + obj.distance + 'Kms )';
                                }
                            });
                        })
                        $scope.loader(false);
                        console.log("status"+bookingObj.booking_status)
                       console.log(bookingObj.booking_status != statusConstants.s06 && bookingObj.booking_status != statusConstants.s01 && bookingObj.booking_status != statusConstants.s02)
                      if (bookingObj.booking_status != 'Pending' && bookingObj.booking_status != 'No Response' && bookingObj.booking_status != 'No Ambulance') {
                      //  if (bookingObj.booking_status != statusConstants.s06 && bookingObj.booking_status != statusConstants.s01 && bookingObj.booking_status != statusConstants.s02 ) {
                            // if (bookingObj.booking_status != 'Pending') {
				
                            $scope.page.title = 'Transfer Booking';
                            $scope.page.type = 'put';
                            //$scope.amb_obj = JSON.stringify($scope.nearAmbulanceList[0])
                            $scope.getDetails($scope.amb_obj);
                        } else {
                            //if (!$scope.amb_obj) {
                            $scope.page.title = 'Assign Booking';
                            $scope.page.type = 'post';
                            if ($scope.bookingObj.location == undefined) {
                                $scope.bookingObj.location = $scope.bookingObj.eme_address;
                            }
			   if($scope.bookingObj.is_drop_location == false){
                            if ($scope.bookingObj.group_id != 'Any') {
                                angular.forEach($scope.globalGroupMasterList, function(obj) {
                                    if (obj.group_id == $scope.bookingObj.group_id) {
                                        $scope.bookingObj.hospital_latitude = obj.group_latitude;
                                        $scope.bookingObj.hospital_longitude = obj.group_longitude;
                                    }
                                })
                            } else {
                                $scope.bookingObj.hospital_latitude = 0;
                                $scope.bookingObj.hospital_longitude = 0
                            }
			}
                            if (!$scope.nearAmbulanceList.length) {
                                $scope.page.type = null;
                            }
                            initMap($scope.bookingObj, $scope.nearAmbulanceList);
                        }
                    } else {
                        $scope.loader(false);
                        $scope.page.title = 'Assign Booking';
                        $scope.page.type = null;
                        $scope.nearAmbulanceList = [];
                        initMap($scope.bookingObj, $scope.nearAmbulanceList);
                    }


                })

            } else {
                window.location.hash = '#/Booking';
            }
        }


        $scope.getDetails = function(ambulance) {
            console.log(angular.toJson(ambulance));
            if (ambulance != undefined) {

                var req = {};
                $scope.amb_obj1 = JSON.parse(ambulance);
                req.tableName = TableConstants.driver_master;
                req.params = {};
                req.params.driver_id = $scope.amb_obj1.driver_id;
                if ($scope.admin.userType === "GROUP ADMIN") {
                    req.params.group_id = $scope.admin.groupId;
                }
                request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        $scope.driverObj = response.Result[0];
                    }
                })
                console.log(JSON.parse(ambulance))
                initMap(ctrlComm.get('bookingObj'), JSON.parse(ambulance));
            } else {
                initMap($scope.bookingObj, $scope.nearAmbulanceList);
            }
        }



        $scope.cancelBooking = function(bookingObj) {
            console.log(bookingObj)
            var input = {
                text: 'Are you sure you want to cancel',
                name: bookingObj.booking_id
            }
            if (bookingObj.Notification == true) {
                confirmCancel(bookingObj);
            } else {

                $scope.confirmation(input, function() {
                    confirmCancel(bookingObj);
                });
            }
        }

        $scope.createCustomerConfirm = function() {
            var input = {
                text: 'Are you sure you want to create a customer',
                name: '',
                type: 'booking'
            }
            $scope.confirmation(input, function() {
                confirmCreate();
            });
        }

        function confirmCreate() {
            window.location.hash = '#/New-Customer-Master';
        }
        /*To create customer on emergency if no customer exists*/
        $scope.createCustomer = function(customer) {
            validateCustomerBooking(function() {
                var r = customer['reason']
                delete customer['reason']
             //   customer['customer_mobile_number'] = customer.customer_mobile;
                console.log("booking.customer_mobile is" + customer.country_code)
                	      //for prefixing country code 

		var a = customer.country_code.split('+')[1]
                console.log("a.a is" + a)
                var countryCode = customer.country_code;
                customer['customer_mobile_number'] = a + customer.customer_mobile;
                var mobile = a + customer.customer_mobile;
                delete customer['customer_mobile']
                delete customer['group_type_id']
                delete customer['customer_id'];
                delete customer['country_code'];

                //customer.is_active = 'Active';
                var req = {};
                req.tableName = TableConstants.customer_master;
                var params = {};

                params = angular.copy(customer);

                req.params = params;
                console.log(angular.toJson(req));
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);

                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        /*  $scope.notification(response.statusMessage);
                        $scope.booking.customer_name = response.responseData.customer_name;
                        $scope.booking.customer_id = response.responseData.customer_id;*/
                        //$scope.booking.customer_age = response.responseData.age;
                        $scope.page.title = 'Add Booking';
                        $scope.customer = 'false';
                        $scope.isCustomerExists = true;
                      //  $scope.booking.customer_mobile = customer.customer_mobile_number;
                        $scope.booking.customer_mobile = mobile;
                        $scope.booking.country_code = countryCode;
                        $scope.booking.reason = r;

                        var req1 = {};
                        request.service('getAmbulanceTypes', 'post', req1, $scope.CONFIG, function(response1) {
                            if (response1.statusCode == '300') {
                                $scope.isCustomerExists = true;
                                $scope.ambulanceTypes = response1.responseData;
                                $scope.booking.group_type_id = $scope.ambulanceTypes[0].id;
                            }
                        })
                    } else if (data.statusCode == '222') {
                        if (data.statusMessage.search('customer_mobile_number') > 0) {
                            $scope.notification('Phone number already exists, Try with other', 'danger');
                        }
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }


        function confirmCancel(bookingObj) {

            console.log(bookingObj)
            var req = {};
            req.customer_id = bookingObj.customer_id;
            req.booking_id = bookingObj.booking_id;
            console.log(angular.toJson(req));
            request.service('cancelBooking', 'post', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300' || response.statusCode == '413') {
                    if (bookingObj.Notification != undefined && bookingObj.Notification == true) {

                    } else
                        $scope.notification(response.statusMessage);
                    $scope.getBookingsList();
                } else if (response.statusCode == '500') {
                    $scope.notification(response.statusMessage, 'danger');
                    $scope.getBookingsList();
                } else {
                    if (bookingObj.Notification != undefined && bookingObj.Notification == true) {

                    } else
                        $scope.notification(response.statusMessage, 'danger');
                }
            })


        }



        $scope.cancel = function() {
            window.location.hash = '#/Booking';
        }






        var addNewUser = function() {
            window.location.hash = "#/New-User"
        }

        function prePoppulateValues(user) {
            if (user) {
                if ($scope.page.type == 'put') {
                    user.isActive == 'Active' ? user.isActive = true : user.isActive = false;
                }
                $scope.user = user;
            } else {
                window.location.hash = "#/Users";
            }
        }

        $scope.assignAmbulanceBooking = function(booking) {
            if (booking != undefined) {
                booking = JSON.parse(booking);
                console.log("BBOOING OBJ",angular.toJson($scope.bookingObj))
                var req = {};
                var req1 = {};
                req1.tableName = TableConstants.customer_master;
                req1.params = {};
                req1.params.customer_id = $scope.bookingObj.customer_id;
                request.service('fetchDataByField', 'post', req1, $scope.CONFIG, function(response1) {
                    if (response1.statusCode == '300') {
                        if (response1.Result[0] && response1.Result[0].age == undefined) {
                            req.customer_age = response1.Result[0].age;
                        }

                        var conditionalParams = {};
                        conditionalParams.booking_id = $scope.bookingObj.booking_id;
                        var params = {};
                        params.ambulance_id = booking.ambulance_id;
                        angular.forEach($scope.globalAmbulanceList, function(obj) {
                            if (obj.amb_id == booking.ambulance_id) {
                                params.ambulance_number = obj.ambulance_number;
                            }
                        })
                        params.distance = booking.distance;
                        params.duration = booking.duration;
                        params.cost = '';
                        params.ambulance_start_latitude = booking.latitude;
                        params.ambulance_start_longitude = booking.longitude;
                        params.ambulance_start_geoHashCode = geohash.encode(booking.latitude, booking.longitude)
			if($scope.bookingObj.drop_latitude==null && $scope.bookingObj.drop_longitude==null){
                        req.hospital_latitude = $scope.bookingObj.hospital_latitude;
                        req.hospital_longitude = $scope.bookingObj.hospital_longitude;
			req.hospital_geoHashCode = geohash.encode(req.hospital_latitude, req.hospital_longitude);
			}
                        req.driverId = booking.driver_id;
                       // req.hospital_geoHashCode = geohash.encode(req.hospital_latitude, req.hospital_longitude);
                        req.params = params;
                        req.conditionalParams = conditionalParams;
			
                        console.log(angular.toJson(req))
                        request.service('assignBookingByCommandCentre', 'post', req, $scope.CONFIG, function(response) {
                            console.log(response)
                            if (response.statusCode == '300') {
                                $scope.notification(response.statusMessage);
                                window.location.hash = '#/Booking';
                            } else {
                                $scope.notification(response.statusMessage, "danger")
                            }

                        })
                    } else if (response1.statusCode == '204') {
                        $scope.notification(response1.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response1.statusMessage, "danger")
                    }
                })

            } else {
                $scope.notification("Please select the ambulance", 'danger')
            }



        }

        var validateCustomerBooking = function(cb) {
            if (!$scope.booking.customer_mobile) {
                $scope.err.customer_number = true;
            } else {
                delete $scope.err.customer_mobile;

                delete $scope.err.customer_mobile_cdn;
            }
            if (!$scope.booking.customer_name) {
                $scope.err.customer_name = true;
            } else {
                delete $scope.err.customer_name;
            }
            console.log(Object.keys($scope.err).length)
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }

        var validateBooking = function(cb) {
            if (!$scope.booking.is_self) {
                $scope.err.is_self = true;
            } else {
                delete $scope.err.is_self;
            }
            if (!$scope.booking.emergency_type_id) {
                $scope.err.emergency_type_id = true;
            } else {
                delete $scope.err.emergency_type_id;
            }
            if (!$scope.booking.address) {
                $scope.err.address = true;
            } else {
                delete $scope.err.address;
            }
            if (!$scope.booking.emergency_latitude) {
                $scope.err.emergency_latitude = true;
            } else {
                delete $scope.err.emergency_latitude;
            }
            if (!$scope.booking.emergency_longitude) {
                $scope.err.emergency_longitude = true;
            } else {
                delete $scope.err.emergency_longitude;
            }
            if ($scope.booking.patient_number) {
                if (!(/^\d{10}$/).test($scope.booking.patient_number)) {
                    $scope.err.patient_number_cdn = true;

                } else {
                    delete $scope.err.patient_number_cdn;
                }
            } else {
                delete $scope.err.patient_number_cdn;
            }
            if ($scope.booking.drop_location) {


                delete $scope.err.drop_location;
            }
            if ($scope.eType == undefined) {
                $scope.notification('Please select Drop Location or Hospital', 'danger')
                $scope.err.eType = true;

            } else if ($scope.eType == 'drop_location') {
                if (!$scope.booking.drop_location) {
                    $scope.err.drop_location = true;
                } else {
                    delete $scope.err.group_id;
                    delete $scope.err.drop_location;
                    delete $scope.err.eType;
                }

            } else if ($scope.eType == 'Hospital') {
                if ($scope.booking.group_id == '' || $scope.booking.group_id == undefined) {

                    $scope.err.group_id = true;
                    $scope.notification('No Hospitals found,try with Drop Location', 'danger')

                } else {
                    delete $scope.err.group_id;
                    delete $scope.err.eType;
                }

            }
         /*   if ($scope.booking.group_id == '') {
                if ($scope.booking.drop_location == undefined) {

                    $scope.err.eType = true;
                    $scope.notification('No Hospitals found,try with Drop Location', 'danger')
                } else {
                    delete $scope.err.eType;


                }
            }*/
            console.log($scope.err)
            console.log(Object.keys($scope.err).length)
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }



        $scope.createBooking = function(booking) {
            validateBooking(function() {
                booking.eme_address = $('#city_country').val();
                angular.forEach($scope.emergencyTypeList, function(obj) {
                    if (obj.id == booking.emergency_type_id) {
                        booking.emergency_type = obj.type_name;

                    }
                })
                booking.booked_by = 'admin';
                angular.forEach($scope.nearByHospitalList, function(obj1) {
                    if (booking.group_id == obj1.group_id) {
                        booking.hospital = obj1.group_name.split("(")[0];
                    }

                })
                booking.emergency_geoHashCode = geohash.encode(booking.emergency_latitude, booking.emergency_longitude);
                var req = {};
                req.customer_age = booking.customer_age;
                req.params = booking;
			      //for prefixing country code 

                var a = booking.country_code.split('+')[1]
                console.log("a.a is" + a)

                req.params['customer_mobile'] = a + ctrlComm.get('customerNumber');
		//  handling name and age instead of sending null if not exists
                if (req.params['customer_name'] == 'null') {
                    req.params['customer_name'] == '';
                }
                if (req.customer_age == 'null' || req.customer_age == '') {
                    req['customer_name'] == 0;
                }
                delete req.params['customer_mobile_number']
                delete req.params['customer_age'];
                delete req.params['address'];
                delete req.params['country_code'];
                if ($scope.eType == 'Hospital') {
                    booking.drop_geoHashCode = geohash.encode(booking.drop_latitude, booking.drop_longitude);
                    req.params.hospital = booking.hospital;
                }
                if ($scope.eType == 'drop_location') {
                    req.params.group_id = 'Any';
                    req.params.hospital = $scope.booking.drop_location;

                }
                
		delete req.params['drop_location'];
                console.log(angular.toJson(req))
                request.service('createBookingByCommandCentre', 'post', req, $scope.CONFIG, function(response) {
                    console.log(response)
                    if (response.statusCode == '300') {
                        $scope.notification("Booking Created Successfully, Booking Id - " + response.responseData['booking_id']);
                        ctrlComm.put('bookingObj', response.responseData);
                        window.location.hash = '#/Assign-Booking';
                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            })
        }




        $scope.assignBooking = function(booking, action) {
     console.log(angular.toJson(booking));
            var input = {
                text: 'Are you sure you want to ' + action,
                name: booking.booking_id
            }
            $scope.confirmation(input, function() {
                confirmAction(booking, action);
            });
        }

        function confirmAction(booking, action) {
            console.log(booking)
            var req = {};
            req.tableName = TableConstants.emergency_booking;
            var params = {};
            params.booking_id = booking.booking_id;
            req.params = params;
            if ($scope.admin.userType === "GROUP ADMIN") {
                req.params.group_id = $scope.admin.groupId;
            }

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                var dateTime = (response.Result[0].updated_datetime != undefined && response.Result[0].updated_datetime != null) ? response.Result[0].updated_datetime : response.Result[0].created_datetime;
                if (timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] != 'Min' && timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] != 'Sec') {
                    $scope.notification("Booking Time has been expired to take an action", "danger");
                }/* else if ((response.Result[0].booking_status == 'No Response' || response.Result[0].booking_status == 'No Ambulance' || response.Result[0].booking_status == 'No Response on transfer') && timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] == 'Min' && parseFloat(timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[0]) >= 15.00) {*/
                else if ((response.Result[0].booking_status == statusConstants.s01 || response.Result[0].booking_status == statusConstants.s02 || response.Result[0].booking_status == statusConstants.s07 ) && timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] == 'Min' && parseFloat(timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[0]) >= 15.00) {

                    $scope.notification("Booking Time has been expired to take an action", "danger");
                }else {
                    ctrlComm.put('bookingObj', booking);
                    ctrlComm.put('action', action);
                    window.location.hash = '#/Assign-Booking';
                }
                /* } else {
                     ctrlComm.put('bookingObj', booking);
                     window.location.hash = '#/Assign-Booking';
                 }*/
            })


        }


        $scope.transferBooking = function(bookingObj) {
            var req = {};
            req.booking_id = bookingObj.booking_id;
            request.service('transferBooking', 'post', req, $scope.CONFIG, function(response) {
                    console.log(response)
                    if (response.statusCode == 300) {
                        $scope.notification(response.statusMessage);
                        $scope.getBookingsList();
                    } else {
                        $scope.notification(response.statusMessage, 'danger');
                    }
                })
                /* var req = {};
                 req.tableName = TableConstants.emergency_booking;
                 var params = {};
                 params.booking_id = booking.booking_id;
                 req.params = params;
                 if ($scope.admin.userType === "GROUP ADMIN") {
                     req.params.group_id = $scope.admin.groupId;
                 }

                 console.log(angular.toJson(req));
           
                 request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                     console.log("TIME SPLIT TIME ",parseFloat(timeConversion(new Date().getTime() - new Date(response.Result[0].updated_datetime.replace(/-/g, '/')).getTime()).split(' ')[0]))
                     if (response.Result[0].booking_status == 'No Response on transfer' && parseFloat(timeConversion(new Date().getTime() - new Date(response.Result[0].updated_datetime.replace(/-/g, '/')).getTime()).split(' ')[0]) >= 5.00) {
                         $scope.notification("Booking Time has been expired to take an action", "danger")
                     } else {
                        
                         var req1 = {};
                         req1.booking_id = bookingObj.booking_id;
                         request.service('transferBooking', 'post', req1, $scope.CONFIG, function(response) {
                             console.log(response)
                             if (response.statusCode == 300) {
                                 $scope.notification(response.statusMessage);
                                 $scope.getBookingsList();
                             } else {
                                 $scope.notification(response.statusMessage, 'danger');
                             }
                         })
                     }
                 })*/

        }



        var saveBooking = function(booking) {
            var req = {};
            var params = {};
            booking.emergency_geoHashCode = geohash.encode(booking.emergency_latitude, booking.emergency_longitude);
            params = angular.copy(booking);
            delete params['address'];
            req.params = params;
            console.log(angular.toJson(req))
            request.service('createBooking', 'post', req, $scope.CONFIG, function(response) {
                    console.log(response)
                    if (response.statusCode == 300) {
                        $scope.notification(response.statusMessage);
                        window.location.hash = '#/Booking';
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
                /* request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                     if (response.statusCode == '300') {
                         window.location.hash = "#/Users"
                         $scope.notification(response.statusMessage);
                     } else if (response.statusCode == '204') {
                         $scope.notification(response.statusMessage, "danger");
                         $timeout(function() {
                             $scope.logout();

                         }, 2000)

                     } else {
                         $scope.notification(response.statusMessage);
                     }
                 })*/
                /*});*/
        }

        var updateBooking = function(ambulanceObj, bookingObj) {
            if (ambulanceObj != undefined) {
                var ambulanceObj = JSON.parse(ambulanceObj);

                var req = {};
                var req1 = {};
                req1.tableName = TableConstants.customer_master;
                req1.params = {};
                req1.params.customer_id = $scope.bookingObj.customer_id;
                request.service('fetchDataByField', 'post', req1, $scope.CONFIG, function(response1) {
                    if (response1.statusCode == '300') {
                        req.customer_age = response1.Result[0].age;

                        angular.forEach($scope.globalDriverMasterList, function(obj) {
                            if (obj.driver_id == ambulanceObj.driver_id) {
                                req.distance = ambulanceObj.distance;
                                req.duration = ambulanceObj.duration;
                                req.cost = '';
                                req.driverName = obj.driver_name;
                                req.ambulanceNumber = ambulanceObj.ambulance_number;
                                req.driverMobileNumber = obj.driver_mobile_number;
                                req.driverProfilePic = obj.file_url;
                                req.emergencyLatitude = bookingObj.emergency_latitude;
                                req.emergencyLongitude = bookingObj.emergency_longitude;
                                req.ambulanceHospital = bookingObj.hospital;
                                req.driverId = obj.driver_id;
                            }
                        })
                        var params = {};
                        params.ambulance_id = ambulanceObj.ambulance_id;
                        params.ambulance_start_longitude = ambulanceObj.longitude;
                        params.ambulance_start_latitude = ambulanceObj.latitude;
                        params.ambulance_start_geoHashCode = ambulanceObj.geoHashCode;
                        params.distance = ambulanceObj.distance;
                        params.duration = ambulanceObj.duration;
                        params.cost = '';
                        req.params = params;
                        var conditionalParams = {};
                        conditionalParams.booking_id = bookingObj.booking_id;
                        req.conditionalParams = conditionalParams;
                        console.log(angular.toJson(req));
                        var req1 = {};
                        req1.tableName = TableConstants.emergency_booking;
                        var params = {};
                        params.booking_id = bookingObj.booking_id;
                        req1.params = params;
                        if ($scope.admin.userType === "GROUP ADMIN") {
                            req1.params.group_id = $scope.admin.groupId;
                        }

                        console.log(angular.toJson(req));

                        request.service('fetchDataByField', 'post', req1, $scope.CONFIG, function(response) {
                                var dateTime = (response.Result[0].updated_datetime != undefined && response.Result[0].updated_datetime != null) ? response.Result[0].updated_datetime : response.Result[0].created_datetime;
                                console.log("TIME SPLIT TIME ", parseFloat(timeConversion(new Date().getTime() - new Date(response.Result[0].updated_datetime.replace(/-/g, '/')).getTime()).split(' ')[0]))
                                    //if ((response.Result[0].booking_status == 'No Response' || response.Result[0].booking_status == 'No Ambulance' || response.Result[0].booking_status == 'No Response on transfer') && timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] == 'Min' && parseFloat(timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[0]) >= 15.00) {
                                if (timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] != 'Min' && timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] != 'Sec') {
                                    $scope.notification("Booking Time has been expired to take an action", "danger")
                                } /*else if (response.Result[0].booking_status == 'No Response on transfer' && timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] == 'Min' && parseFloat(timeConversion(new Date().getTime() - new Date(response.Result[0].updated_datetime.replace(/-/g, '/')).getTime()).split(' ')[0]) >= 15.00) {*/
                                else if (response.Result[0].booking_status == statusConstants.s07 &&  timeConversion(new Date().getTime() - new Date(dateTime.replace(/-/g, '/')).getTime()).split(' ')[1] == 'Min' && parseFloat(timeConversion(new Date().getTime() - new Date(response.Result[0].updated_datetime.replace(/-/g, '/')).getTime()).split(' ')[0]) >= 15.00) {

                                    $scope.notification("Booking Time has been expired to take an action", "danger")
                                } else {
                                           
					if(req.ambulanceNumber && req.ambulanceNumber.split(' ').length > 1){
                                            req.ambulanceNumber = req.ambulanceNumber.split(' ')[0];
                                       }
                                        console.log(angular.toJson(req));
                                    request.service('transferByCommandCentre', 'post', req, $scope.CONFIG, function(response) {
                                        console.log(response)
                                        if (response.statusCode == '300') {
                                            window.location.hash = '#/Booking';
                                            $scope.notification(response.statusMessage);
                                        } else {
                                            $scope.notification(response.statusMessage, 'danger');
                                        }
                                    })
                                }
                            })
                            /*request.service('transferByCommandCentre', 'post', req, $scope.CONFIG, function(response) {
                                console.log(response)
                                if (response.statusCode == '300') {
                                    window.location.hash = '#/Booking';
                                    $scope.notification(response.statusMessage);
                                } else {
                                    $scope.notification(response.statusMessage, 'danger');
                                }
                            })*/

                    } else if (response1.statusCode == '204') {
                        $scope.notification(response1.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response1.statusMessage, "danger")
                    }
                })
            } else {
                $scope.notification("Please select the ambulance", 'danger')
            }


        }

        /*$scope.cancelBooking = function() {
            window.location.hash = '#/Booking';
        }*/

        var updateUserObj = function(userObj) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.system_users;
                var params = {};
                if (userObj.isActive == true) {
                    userObj.isActive = 'Active';
                } else {
                    userObj.isActive = 'InActive';
                }
                params = angular.copy(userObj);
                delete params['userId'];
                delete params['userTypeName'];
                var conditionalParams = {};
                conditionalParams.userId = userObj.userId;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Users"
                        $scope.notification(response.statusMessage);
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            });
        }

        var updateUser = function(User) {
            ctrlComm.put('userObj', User);
            window.location.hash = "#/Update-User"
        }


        var cancelUser = function() {
            window.location.hash = "#/Users"
        }

        var deleteUser = function(user) {
            var input = {
                text: 'Are you sure you want to delete',
                name: user.display_name
            }
            $scope.confirmation(input, function() {
                confirmDelete(user);
            });
        }

        function confirmDelete(user) {
            console.log(user)
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.system_users;
            var params = {};
            params.userId = user.userId;
            req.params = params;
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == '300') {
                    $scope.getUsersList(function() {
                        $scope.notification(response.statusMessage);
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)
                }
            })


        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var addNewBooking = function() {
            window.location.hash = '#/New-Booking';
        }



        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewUser = addNewUser;
        $scope.saveBooking = saveBooking;
        $scope.updateUser = updateUser;
        $scope.deleteUser = deleteUser;
        $scope.cancelUser = cancelUser;
        $scope.updateUserObj = updateUserObj;
        $scope.viewUser = viewUser;
        $scope.addNewBooking = addNewBooking;
        $scope.updateBooking = updateBooking;


















        var map; // Google map object
        var geocoder = new google.maps.Geocoder(); // instantiate a geocoder object
        // get the address detail

	        function getDropLocation(latLng) {
            geocoder.geocode({ 'latLng': latLng }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    // Show the Google map in the div with the attribute id 'mapAttr'

                    function getAddressDetails1(results) {
                        //models which are to be used in the HTML page  

                        $scope.booking.drop_location = results[0].formatted_address;
                        console.log("ddddddddddddddddddddd", $scope.booking.drop_location);
                        $scope.booking.drop_longitude = results[0].geometry.location.lng();
                        $scope.booking.drop_latitude = results[0].geometry.location.lat();
                        // $scope.getNearHospitalsList();
                        // type of address inputted that was geocoded                  
                        if (results[0]) {
                            //address_components[] is an array containing the separate address components
                            for (var i = 0; i < results[0].address_components.length; i++) {
                                var addr = results[0].address_components[i];
                                $scope.$apply();
                            }

                        }

                    }

                    getAddressDetails1(results); // calling func to fill form details like Address Line 1,Address Line 2,District etc. when map is load
                    //Set event to display location to the marker when the marker is drag.



                }
            })
        }
        function getAddress(latLng) {
            // Make asynchronous call to Google geocoding API
            geocoder.geocode({ 'latLng': latLng }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    // Show the Google map in the div with the attribute id 'mapAttr'.
                    map = new google.maps.Map(document.getElementById('mapAttr'), {
                        zoom: 16, // Set the zoom level according to the address level of detail the user specified                 
                        center: results[0].geometry.location // Center the map at the specified location    
                    });
                    marker = new MarkerWithLabel({
                        map: map,
                        position: results[0].geometry.location, // Place a Google Marker at the same location as the map center 
                        labelAnchor: new google.maps.Point(-3, 30),
                        labelClass: "markerLabels", // marker label css class 
                        draggable: true //allow users to move a marker on the map by setting the marker's draggable property to true
                    });

                    //    addYourLocationButton(map,marker);
                    var locButton = document.getElementById('div')

                    function getAddressDetails(results) {
                        //models which are to be used in the HTML page              
                        //$scope.vendor.address = "";
                        /*$scope.vendor.city = "";
                        $scope.vendor.state = "";
                        $scope.vendor.country = "";
                        $scope.vendor.zip = "";*/
                        $scope.booking.address = results[0].formatted_address;
                        $scope.booking.emergency_longitude = results[0].geometry.location.lng();
                        $scope.booking.emergency_latitude = results[0].geometry.location.lat();
                        $scope.getNearHospitalsList();
                        // type of address inputted that was geocoded                  
                        if (results[0]) {
                            //address_components[] is an array containing the separate address components
                            for (var i = 0; i < results[0].address_components.length; i++) {
                                var addr = results[0].address_components[i];
                                // check if this entry in address_components has a type of country 
                                /*if (addr.types[0] == 'country')
                                    $scope.vendor.country = addr.long_name;
                                else if (addr.types[0] == ['administrative_area_level_1']) // State
                                    $scope.vendor.state = addr.long_name;
                                else if (addr.types[0] == 'postal_code') // Zip
                                    $scope.vendor.zip = parseInt(addr.short_name);
                                else if (addr.types[0] == ['locality']) // City
                                    $scope.vendor.city = addr.long_name;*/

                                $scope.$apply();
                            }

                        }
                        /*  var formattedAddress;
                          if (results[0].formatted_address != null) {
                              formattedAddress = results[0].formatted_address;
                              console.log('formattedAddress', formattedAddress)
                          }
                          if (results[0].formatted_address != null) {
                              formattedAddress = results[0].formatted_address;
                              var splits = formattedAddress.split(",");
                              $scope.s1 = splits[0];
                              $scope.street2 = splits[1];
                              $scope.s2 = splits[2];
                              $scope.s3 = splits[3];
                              if ($scope.s1 == 'Unnamed Road') {
                                  $scope.s1 = '';
                              }
                              if (typeof $scope.s2 == "undefined") {
                                  $scope.s2 = '';
                              }
                              if (typeof $scope.s3 == "undefined") {
                                  $scope.s3 = '';
                              }
                              $scope.street3 = $scope.s2 + ' ' + $scope.s3;

                              if ($scope.street3 == "undefined") {
                                  $scope.street3 = '';
                              }

                              var address = $scope.s1 + ',' + $scope.street2
                              $scope.vendor.address = address.replace(/^,|,$/g, '');


                              console.log("-------------------", $scope.user.address)

                              $scope.$apply();
                          }*/
                    }

                    getAddressDetails(results); // calling func to fill form details like Address Line 1,Address Line 2,District etc. when map is load
                    //Set event to display location to the marker when the marker is drag.
                    google.maps.event.addListener(marker, 'drag', function(e) {

                        marker.set('labelContent', ''); // removing marker label             
                        marker.label.setContent();
                        marker.getPosition(); // get the marker position return lat,lng 
                        marker.setPosition(e.latLng); // set marker position
                        var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng()); // storing lat,lng in object 
                        // Make asynchronous call to Google geocoding API
                        geocoder.geocode({ 'latLng': latlng }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                getAddressDetails(results); // calling func to filled form details like Address Line 1,Address Line 2,District etc. when marker is drag
                            }
                        });
                    });
                    //Set event to display marker where user is click
                    google.maps.event.addListener(map, 'click', function(e) {
                        marker.set('labelContent', '');
                        marker.label.setContent();
                        marker.getPosition(); // get the marker position return lat,lng                       
                        marker.setPosition(e.latLng); // set marker position
                        var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng()); // storing lat,lng in  object  
                        // Make asynchronous call to Google geocoding API                    
                        geocoder.geocode({ 'latLng': latlng }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                getAddressDetails(results); //calling func to fill form details like Address Line 1,Address Line 2,District etc. when user click on any location on map
                            }
                        });
                    });


                }
            })
        }

        $scope.getNearHospitalsList = function() {
	 if ($scope.eType == 'Hospital') {
            if ($scope.booking.emergency_latitude && $scope.booking.emergency_longitude) {
                var req = {};
                req.emergencyGeoHash = geohash.encode($scope.booking.emergency_latitude, $scope.booking.emergency_longitude);
                req.latitude = $scope.booking.emergency_latitude;
                req.longitude = $scope.booking.emergency_longitude;
                req.groupType = $scope.booking.group_type_id;
                req.emergency_type = $scope.booking.emergency_type_id;
                $scope.loader(true);
                request.service('getNearByHospitalList', 'post', req, $scope.CONFIG, function(response) {
                    console.log(response)
                    $scope.loader(false);
                    $scope.nearByHospitalList = [];
                    var obj = {};
                    if (response.statusCode == '300') {

                        if (response.responseData.length > 0) {
                            obj.group_name = 'Any Nearby Hospital';
                            obj.group_id = 'Any';
                            response.responseData.unshift(obj);
                            $scope.nearByHospitalList = response.responseData;
                            angular.forEach($scope.nearByHospitalList, function(obj) {
                                if (obj.group_id != 'Any') {
                                    obj.group_name = obj.group_name + ' (~' + obj.distance + 'Kms )';
                                }
                            })
                            $scope.booking.group_id = response.responseData[0].group_id;
                        } else {
                            obj.group_name = 'No Hospitals Found';
                            obj.group_id = '';
                            $scope.nearByHospitalList.push(obj);
                            $scope.booking.group_id = $scope.nearByHospitalList[0].group_id;
                        }

                    } else {
                        obj.group_name = 'No Hospitals Found';
                        obj.group_id = '';
                        $scope.nearByHospitalList.push(obj);
                        $scope.booking.group_id = $scope.nearByHospitalList[0].group_id;
                    }

                })

            }
        }
	}

        $scope.showDetails = function(e, ambulanceObj) {
            $scope.ambulance = ambulanceObj;
            console.log($scope.ambulance)
            $scope.map.showInfoWindow('foo-iw', $scope.ambulance.id);
        }

        $scope.showCustomInfo = function(e, customObj) {
            $scope.custom = customObj;
            $scope.map.showInfoWindow('custom-info', $scope.custom.state);
        }

        //https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png
        //create 'Your location' button             
        function addYourLocationButton(map) {

            // Create a div to hold the control.
            var controlDiv = document.createElement('div');

            // Set CSS for the 'Your location' button
            var firstChild = document.createElement('button');
            firstChild.style.backgroundColor = '#fff';
            firstChild.style.border = 'none';
            firstChild.style.outline = 'none';
            firstChild.style.width = '28px';
            firstChild.style.height = '28px';
            firstChild.style.borderRadius = '2px';
            firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
            firstChild.style.cursor = 'pointer';
            firstChild.style.marginRight = '10px';
            firstChild.style.padding = '0';
            firstChild.title = 'Your Location';
            controlDiv.appendChild(firstChild);
            $scope.firstChild = firstChild;
            // Set CSS for the button background 
            var secondChild = document.createElement('div');
            secondChild.style.margin = '5px';
            secondChild.style.width = '18px';
            secondChild.style.height = '18px';
            secondChild.style.backgroundImage = 'url(img/mylocation-sprite-2x.png)';
            secondChild.style.backgroundSize = '180px 18px';
            secondChild.style.backgroundPosition = '0 0';
            secondChild.style.backgroundRepeat = 'no-repeat';
            firstChild.appendChild(secondChild);

            google.maps.event.addListener(map, 'center_changed', function() {
                secondChild.style['background-position'] = '0 0';

            });
            //To register for event notifications, use the addListener() event handler.
            //That method takes an event to listen for, and a function to call when the specified event occurs.
            //Retrive the center location using 'center_changed' event


            //To display current location when the button is click.
            firstChild.addEventListener('click', function() {
                var imgX = '0',
                    animationInterval = setInterval(function() {
                        imgX = imgX === '-18' ? '0' : '-18';
                        secondChild.style['background-position'] = imgX + 'px 0';
                    }, 500);
                //Check if Geolocation is supported
                //If supported, run the getCurrentPosition() method. 
                //If the getCurrentPosition() method is successful, it returns a coordinates object. 


                if (window.navigator.geolocation) {
                    window.navigator.geolocation.getCurrentPosition(function(position) {

                        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        var marker = new MarkerWithLabel({
                            map: map,
                            position: latlng, // Place a Google Marker at the same location as the map center 
                            labelAnchor: new google.maps.Point(-3, 30),
                            labelClass: "markerLabels", // marker label css class 
                            draggable: true //allow users to move a marker on the map by setting the marker's draggable property to true
                        });
                        marker.visible = true;
                        map.setCenter(latlng);
                        clearInterval(animationInterval);
                        secondChild.style['background-position'] = '-144px 0';
                        addYourLocationButton(map);
                        getAddress(latlng);

                    });

                } else {
                    clearInterval(animationInterval);
                    secondChild.style['background-position'] = '0 0';
                }

            });
            controlDiv.index = 1;
            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);

        }

        $scope.initAutocomplete = function() {
            // Show the Google map in the div with the attribute id 'mapAttr'.   

            map = new google.maps.Map(document.getElementById('mapAttr'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoom: 16
            });


            //  addYourLocationButton(map, marker); // calling addYourLocationButton function to add 'Your location' button on map

            //Check if Geolocation is supported
            //If supported, run the getCurrentPosition() method. 
            //If the getCurrentPosition() method is successful, it returns a coordinates object. 
            window.navigator.geolocation.getCurrentPosition(function(place) {
            var input1 = document.getElementById('drop_location');
            var searchBox1 = new google.maps.places.SearchBox(input1);

            searchBox1.addListener('places_changed', function() {
                // Get the place details from the autocomplete object.
                var places = searchBox1.getPlaces();
                if (places.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    $scope.place = place;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

                    getDropLocation(latlng); // call this func to get address detail and here we are passing current location coordinate 


                });
            });
                var latlng = new google.maps.LatLng($scope.lat, $scope.lng);

                // Make asynchronous call to Google geocoding API
                geocoder.geocode({ 'latLng': latlng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map = new google.maps.Map(document.getElementById('mapAttr'), {
                            zoom: 16, // Set the zoom level according to the address level of detail the user specified                                 
                            center: results[0].geometry.location, // Center the map at the specified location       
                        });

                        getAddress(latlng); // call this func to get address detail and here we are passing current location coordinate                  

                    }
                })
            });

            // Create the search box and link it to the UI element.
            var input = document.getElementById('city_country');
            var searchBox = new google.maps.places.SearchBox(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });


            // Listen for the event fired when the user selects a prediction and retrieve more details for that place.
            searchBox.addListener('places_changed', function() {
                // Get the place details from the autocomplete object.
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    $scope.place = place;
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(35, 35)
                    };
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

                    getAddress(latlng); // call this func to get address detail and here we are passing current location coordinate 


                });
                map.fitBounds(bounds);
            });
            var input1 = document.getElementById('drop_location');
            var searchBox1 = new google.maps.places.SearchBox(input1);

            searchBox1.addListener('places_changed', function() {
                // Get the place details from the autocomplete object.
                var places = searchBox1.getPlaces();
                if (places.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    $scope.place = place;

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

                    getDropLocation(latlng); // call this func to get address detail and here we are passing current location coordinate 


                });
            });
        }


        $scope.location = function(eType) {
            $scope.eType = eType;
        }


    }
    app.controller('bookingCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', 'geohash','statusConstants', bookingCtrl]);
}());
