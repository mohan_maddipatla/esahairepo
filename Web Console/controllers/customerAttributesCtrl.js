(function() {
    var app = angular.module('eSahai');
    var customerAttributesCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("in customerAttributesCtrl--> " + TableConstants.customer_e_types);
        $scope.tabActive('customerAttribute', 'settings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.customerAttr = {};
        $scope.page = {};
        $scope.err = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.toDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType === "EMPLOYEE" || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
            window.location.hash = '#/Home';
        }
        $scope.getCustomerAttributesList = function() {
            console.log($scope.customerAttributesList.length);
            $scope.totalItems = $scope.customerAttributesList.length;
            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                $scope.tableRows.push($scope.totalItems);

            if ($scope.totalItems != 0) {
                $scope.tableRows = $scope.tableRows.sort(request.sort);
                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                if ($scope.tableRows[1]) {
                    $scope.viewby = $scope.tableRows[1];
                } else {
                    $scope.viewby = $scope.tableRows[0];
                }
                $scope.setItemsPerPage($scope.viewby);
            }
        };

        if (window.location.hash == "#/Customer-Attributes") {
            $scope.getCustomerAttributesList();
        } else if (window.location.hash == "#/New-Customer-Attribute") {
            $scope.page.title = 'Add Customer Attribute';
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Customer-Attribute") {
            $scope.page.title = 'Update Customer Attribute';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('customerAttrObj'));
        }

        function prePoppulateValues(customerAttrObj) {
            if (customerAttrObj) {
                $scope.customerAttr = customerAttrObj;
            } else {
                window.location.hash = "#/Customer-Attributes"
            }
        }

        var validate = function(cb) {
            if (!$scope.customerAttr.type_name) {
                $scope.err.type_name = true;
            } else {
                delete $scope.err.type_name;
            }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var addCustomerAttr = function() {
            window.location.hash = '#/New-Customer-Attribute';
        }

        var cancelCustomerAttr = function() {
            window.location.hash = "#/Customer-Attributes";
        }

        var saveCustomerAttr = function(customerAttr) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.customer_e_types;
                var params = {};
                params = angular.copy(customerAttr);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Customer-Attributes"
                        $scope.notification(response.statusMessage);
			$scope.getCustomerAttributesList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            })
        }

        var editCustomerAttribute = function(customerAttr) {
            ctrlComm.put('customerAttrObj', customerAttr);
            window.location.hash = "#/Update-Customer-Attribute";
        }

        var updateCustomerAttr = function(customerAttr) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.customer_e_types;
                var params = {};
                params = angular.copy(customerAttr);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = customerAttr.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Customer-Attributes"
                        $scope.notification(response.statusMessage);
			$scope.getCustomerAttributesList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });
        }

        var deleteCustomerAttribute = function(obj) {
            var input = {
                text: 'Are you sure you want to delete',
                name: obj.type_name
            }
            $scope.confirmation(input, function() {
                confirmDeleteCustomerAttribute(obj);
            });
        }

        function confirmDeleteCustomerAttribute(obj) {
            var req = {};
            req.tableName = TableConstants.customer_e_types;
            var params = {};
            params.id = obj.id;
            req.params = params;
            //$scope.loader(true);
            console.log(angular.toJson(obj));
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(data) {
                $scope.loader(false);
                if (typeof data == 'string') {
                    data = JSON.parse(data)
                }
                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    var req = {};
                    req.tableName = TableConstants.customer_e_types;
                    req.params = {};
                    console.log(angular.toJson(req));
                    request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                        $scope.loader(false);
                        if (response.statusCode == '300') {
                            $scope.customerAttributesList = response.Result;
                            console.log($scope.customerAttributesList)
                            $scope.totalItems = $scope.customerAttributesList.length;
                            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                $scope.tableRows.push($scope.totalItems);

                            if ($scope.totalItems != 0) {
                                $scope.tableRows = $scope.tableRows.sort(request.sort);
                                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                if ($scope.tableRows[1]) {
                                    $scope.viewby = $scope.tableRows[1];
                                } else {
                                    $scope.viewby = $scope.tableRows[0];
                                }
                                $scope.setItemsPerPage($scope.viewby);
                            }
                        } else if (response.statusCode == '204') {
                            $scope.notification(response.statusMessage, "danger");
                            $timeout(function() {
                                $scope.logout();
                            }, 2000)

                        }
                    })
                } else if (data.statusCode == '400' && data.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record", "danger");
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(data.statusMessage);
                }
            });
        }
        $scope.showAlltableRows = showAlltableRows;
        $scope.addCustomerAttr = addCustomerAttr;
        $scope.cancelCustomerAttr = cancelCustomerAttr;
        $scope.saveCustomerAttr = saveCustomerAttr;
        $scope.updateCustomerAttr = updateCustomerAttr;
        $scope.editCustomerAttribute = editCustomerAttribute;
        $scope.deleteCustomerAttribute = deleteCustomerAttribute;
    }
    app.controller('customerAttributesCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', customerAttributesCtrl]);
}());
