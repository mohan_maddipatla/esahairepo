(function() {
    var app = angular.module('eSahai');
    var loginCtrl = function($scope, $rootScope, request, $http) {

        if (request.getItem('userAccessId')) {
            window.location.hash = "#/"
        }

        /* if (request.getItem('appUserId')) {
             console.log("lkdjf")
             window.location.hash = "#/Home"
         }*/

        if (window.location.hash = "#/login") {
            if (request.getItem('appUserId')) {
                window.location.hash = "#/Home"
            }
            document.getElementById('page-content').style.minHeight = screen.height + 'px';
        }

        //Login.init();

        // $scope.login = {
        //  email:"admin@tatadocomo.com",
        //  password:"admin"
        // }
        $scope.login = {};
        $scope.err = {};
        var error = false;
        var validation = function(cb) {
            if (!$scope.login.email) { $scope.err.email = true; } else {
                if (!(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/).test($scope.login.email)) {
                    $scope.err.email_cdn = true;
                } else {
                    delete $scope.err.email;
                    delete $scope.err.email_cdn;
                }
            }
            if (!$scope.login.password) { $scope.err.password = true; } else { delete $scope.err.password; }
            if (Object.keys($scope.err).length == 0) {
                if (cb) cb();
            }
        }


        var adminLogin = function() {
            validation(function() {
                $scope.loader(true);
                request.service('login', 'post', $scope.login, {}, function(response) {
                        console.log(response)
                        $scope.loader(false);
                        if (response.statusCode == '300') {
                            request.setItem('statusCode', response.statusCode);
                            request.setItem('appUserId', response.userId.toString());
                            request.setItem('appEmail', response.email);
                            request.setItem('appToken', response.token);
                            request.setItem('displayName', response.displayName);
                            request.setItem('userType', response.userType.toUpperCase());
                            request.setItem('groupId', response.groupId.toString());
                            window.location.hash = "#/"
                            $scope.loginToggle();
                        } else {
                            $scope.notification(response.statusMessage, 'danger');
                        }
                    })
                    /*request.service('login','post',$scope.login,function (data){
                        console.log("data :: ",data);
                        $scope.loader(false);
                        if(data.ResultCode >= 200 && data.ResultCode <=299){
                            request.setItem('userAccessId',data.userAccessId);
                            request.setItem('admin',data.admin);
                            request.setItem('displayName',data.displayName);
                            window.location.hash="#/"
                            $scope.loginToggle();
                        }else{
                            $scope.notification(data.ReplyMessage,'danger');
                        }
                        
                    });*/
            });
        }

        $scope.adminLogin = adminLogin;
    }
    app.controller('loginCtrl', ['$scope', '$rootScope', 'request', '$http', loginCtrl]);
}());
