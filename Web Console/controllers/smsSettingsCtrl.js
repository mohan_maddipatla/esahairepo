(function() {
    var app = angular.module('eSahai');
    var smsSettingsCtrl = function($scope, $rootScope, request, ctrlComm, fileUpload, $filter, TableConstants, $timeout) {
        console.log("in smsSettingsCtrl : " + TableConstants.sms_details);
        $scope.tabActive('smsSetting', 'appSettings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.ambulance = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        //$scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');

        $scope.emergencyTypeOptions = [];
        $scope.selectedEmergencyType = [];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};

        var cancelSMSSetting = function() {
            window.location.hash = '#/Home';
        }

        var getSMSSettings = function() {
            $scope.loader(true);
            var req = {};
            req.tableName = 'sms_details';
            req.params = {};
            console.log(angular.toJson(req));

            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                console.log(angular.toJson(response))
                if (response.statusCode == '300') {
                    $scope.globalSMS = response.Result;
                    $scope.sms = {};
                    angular.forEach(response.Result, function(obj) {
                        $scope.sms[obj.fieldName] = obj.value;
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                } else {
                    $scope.usersList = [];
                }
            })
        }

        if (window.location.hash == "#/SMS-Settings") {
            getSMSSettings();
        }

        var updateSMSSetting = function(smsObj) {
            $scope.loader(true);
            var i = 0;
            angular.forEach($scope.globalSMS, function(obj) {
                var req = {};
                req.tableName = TableConstants.sms_details;
                var params = {};
                params['value'] = smsObj[obj.fieldName];
                var conditionalParams = {};
                conditionalParams.id = obj.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req));
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        i++;
                        if ($scope.globalSMS.length == i) {
                            $scope.notification(response.statusMessage);
                        }
                    }
                })
            })
        }




        $scope.cancelSMSSetting = cancelSMSSetting;
        $scope.updateSMSSetting = updateSMSSetting;
    }


    app.controller('smsSettingsCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'fileUpload', '$filter', 'TableConstants', '$timeout', smsSettingsCtrl]);
}());
