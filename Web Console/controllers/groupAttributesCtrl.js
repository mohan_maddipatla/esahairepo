(function() {
    var app = angular.module('eSahai');
    var groupAttributesCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("in groupAttributesCtrl--> " + TableConstants.group_e_types);
        $scope.tabActive('groupAttribute', 'settings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.groupAttr = {};
        $scope.page = {};
        $scope.err = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.toDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType === "EMPLOYEE" || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
            window.location.hash = '#/Home';
        }
        $scope.getGroupAttributesList = function() {
            $scope.totalItems = $scope.groupAttributesList.length;
            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                $scope.tableRows.push($scope.totalItems);

            if ($scope.totalItems != 0) {
                $scope.tableRows = $scope.tableRows.sort(request.sort);
                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                if ($scope.tableRows[1]) {
                    $scope.viewby = $scope.tableRows[1];
                } else {
                    $scope.viewby = $scope.tableRows[0];
                }
                $scope.setItemsPerPage($scope.viewby);
            }
        };

        if (window.location.hash == "#/Group-Attributes") {
	    $scope.getGroupAttributesList();
            $scope.getCustomerAttributesList();
        } else if (window.location.hash == "#/New-Group-Attribute") {
            $scope.page.title = 'Add Group Attribute';
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Group-Attribute") {
            $scope.page.title = 'Update Group Attribute';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('groupAttrObj'));
        }

        function prePoppulateValues(groupAttrObj) {
            if (groupAttrObj) {
                $scope.groupAttr = groupAttrObj;
            } else {
                window.location.hash = "#/Group-Attributes"
            }
        }

        var validate = function(cb) {
            if (!$scope.groupAttr.type_name) {
                $scope.err.type_name = true;
            } else {
                delete $scope.err.type_name;
            }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var addGroupAttr = function() {
            window.location.hash = '#/New-Group-Attribute';
        }

        var cancelGroupAttr = function() {
            window.location.hash = "#/Group-Attributes";
        }

        var saveGroupAttr = function(groupAttr) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.group_e_types;
                var params = {};
                params = angular.copy(groupAttr);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Group-Attributes"
                        $scope.notification(response.statusMessage);
			$scope.getGroupAttributesList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            })
        }

        var editGroupAttribute = function(groupAttr) {
            ctrlComm.put('groupAttrObj', groupAttr);
            window.location.hash = "#/Update-Group-Attribute";
        }

        var updateGroupAttr = function(groupAttr) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.group_e_types;
                var params = {};
                params = angular.copy(groupAttr);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = groupAttr.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Group-Attributes"
                        $scope.notification(response.statusMessage);
			$scope.getGroupAttributesList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });
        }

        var deleteGroupAttribute = function(obj) {
            var input = {
                text: 'Are you sure you want to delete',
                name: obj.type_name
            }
            $scope.confirmation(input, function() {
                confirmDeleteGroupAttribute(obj);
            });
        }

        function confirmDeleteGroupAttribute(obj) {
            var req = {};
            req.tableName = TableConstants.group_e_types;
            var params = {};
            params.id = obj.id;
            req.params = params;
            //$scope.loader(true);
            console.log(angular.toJson(obj));
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(data) {
                $scope.loader(false);
                if (typeof data == 'string') {
                    data = JSON.parse(data)
                }
                if (data.statusCode == '300') {
                    /*$scope.getGroupAttributesList(function() {*/
                    $scope.notification(data.statusMessage);
                    var req = {};
                    req.tableName = TableConstants.group_e_types;
                    req.params = {};
                    console.log(angular.toJson(req));
                    request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                            $scope.loader(false);
                            if (response.statusCode == '300') {
                                $scope.groupAttributesList = response.Result;
                                $scope.totalItems = $scope.groupAttributesList.length;
                                if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                    $scope.tableRows.push($scope.totalItems);

                                if ($scope.totalItems != 0) {
                                    $scope.tableRows = $scope.tableRows.sort(request.sort);
                                    $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                    if ($scope.tableRows[1]) {
                                        $scope.viewby = $scope.tableRows[1];
                                    } else {
                                        $scope.viewby = $scope.tableRows[0];
                                    }
                                    $scope.setItemsPerPage($scope.viewby);
                                }
                            } else if (response.statusCode == '204') {
                                $scope.notification(response.statusMessage, "danger");
                                $timeout(function() {
                                    $scope.logout();
                                }, 2000)

                            }
                        })
                        /*});*/
                } else if (data.statusCode == '400' && data.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record");
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(data.statusMessage);
                }
            });
        }
        $scope.showAlltableRows = showAlltableRows;
        $scope.addGroupAttr = addGroupAttr;
        $scope.cancelGroupAttr = cancelGroupAttr;
        $scope.saveGroupAttr = saveGroupAttr;
        $scope.updateGroupAttr = updateGroupAttr;
        $scope.editGroupAttribute = editGroupAttribute;
        $scope.deleteGroupAttribute = deleteGroupAttribute;
    }
    app.controller('groupAttributesCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', groupAttributesCtrl]);
}());
