(function() {
    var app = angular.module('eSahai');
    var privacyNpoliciesCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout) {
        $scope.tabActive('pNp', 'appSettings');
        $scope.page = {};
        var getPrivacyAndPolicies = function() {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.privacy_policy;
            req.params = {};
            console.log(angular.toJson(req));

            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                console.log(response)
                $scope.loader(false);
                if (response.Result.length) {
                    $scope.privacy_and_policy = response.Result[0].privacy_and_policy;
                    $('#privacyAndPolicy').html($scope.privacy_and_policy);
                    ctrlComm.put('privacyAndPolicy', $scope.privacy_and_policy);
                } else {
                    $('#privacyAndPolicy').html(' <h3 style="text-align:center">No Privacy and Policies has been added.</h3>');
                }
            });
        }

        if (window.location.hash == "#/Add-New-Policies") {
            $scope.termsAndConditions = null;
            $scope.page.title = "Add Privacy And Policies";
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Privacy&Policies") {
            $scope.page.title = "Update Privacy And Policies";
            $scope.page.type = 'put';
            if (ctrlComm.get('privacyAndPolicy') != undefined) {
                $scope.privacy_and_policy = ctrlComm.get('privacyAndPolicy');
            } else {
                window.location.hash = '#/Privacy-And-Policies';
            }
        } else if (window.location.hash = "#/Privacy-And-Policies") {

            getPrivacyAndPolicies();
        }

        var addPrivacyPolicy = function() {
            window.location.hash = "#/Add-New-Policies"
        }

        var updatePrivacyPolicy = function() {
            window.location.hash = "#/Update-Privacy&Policies"
        }



        var updatePolicy = function(terms) {
            if (terms.length >= 100) {

                var req = {};
                req.tableName = TableConstants.privacy_policy;
                var params = {};
                params.privacy_and_policy = angular.copy(terms);
                var conditionalParams = {};
                conditionalParams.userId = terms.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Privacy-And-Policies"
                        $scope.notification(response.statusMessage);
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            } else {
                $scope.notification("Expecting min. of 100 characters for privacy and policies", "danger")
            }
        }

        var cancelPolicy = function() {
            window.location.hash = "#/Privacy-And-Policies"
        }



        $scope.addPrivacyPolicy = addPrivacyPolicy;
        $scope.updatePrivacyPolicy = updatePrivacyPolicy;
        $scope.cancelPolicy = cancelPolicy;
        $scope.updatePolicy = updatePolicy;

    }
    app.controller('privacyNpoliciesCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout', privacyNpoliciesCtrl]);
}());
