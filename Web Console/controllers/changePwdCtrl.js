(function() {
    var app = angular.module('eSahai');
    var changePwdCtrl = function($scope, $rootScope, request) {
        $scope.psw = {};
        $scope.err = {};
        var validate = function(cb) {
            if (!$scope.psw.oldPassword) {
                $scope.err.oldPassword = true;
            } else {
                //if (!(/^.{6,10}$/).test($scope.psw.oldPassword)) {
                if ($scope.psw.oldPassword.length < 6) {
                    $scope.err.password_cdn = true;

                } else {
                    delete $scope.err.oldPassword;
                    delete $scope.err.password_cdn;
                    delete $scope.err.password_check;
                }

            }
            if (!$scope.psw.newPassword) {
                $scope.err.newPassword = true;
            } else {
                //if (!(/^.{6,10}$/).test($scope.psw.newPassword)) {
                if ($scope.psw.newPassword.length < 6) {
                    $scope.err.password_cdn1 = true;

                } else {
                    delete $scope.err.newPassword;
                    delete $scope.err.password_cdn1;
                }
            }
            if (!$scope.psw.confirm) {
                $scope.err.confirm = true;
            } else {
                delete $scope.err.confirm;
            }
            if ($scope.psw.confirm && $scope.psw.newPassword) {
                if ($scope.psw.confirm != $scope.psw.newPassword) {
                    $scope.err.confirmMatch = true;
                } else {
                    delete $scope.err.confirmMatch;
                }
            }
            if ($scope.psw.newPassword == $scope.psw.oldPassword && $scope.psw.newPassword.length > 1 && $scope.psw.oldPassword.length > 1) {
                $scope.err.password_cdn2 = true;
            } else {
                delete $scope.err.password_cdn2;
            }

            console.log($scope.err)

            if (Object.keys($scope.err).length == 0) {
                if (cb) cb();
            }
        }

        var savePassword = function(password) {
            validate(function() {
                var req = {};
                req.params = password;
                console.log(angular.toJson(req));
                $scope.loader(true);
                request.service('changePassword', 'post', password, $scope.CONFIG, function(data) {
                    $scope.loader(false);
                    if (data.statusCode == 300) {
                        $scope.err.password_check = true;
                        $scope.notification(data.statusMessage);
                        window.location.hash = '#/';
                    } else {
                        $scope.notification(data.statusMessage, 'danger');
                    }

                });
            });
        }

        var cancel = function() {
                window.location.hash = "#/";
            }
            /*  $scope.checkPassword =function (password,fn){
                    var req = {};
                        //req.params = password;
                        req.userId=request.getItem('appUserId');
                        console.log(angular.toJson(req));
                        //$scope.loader(true);
                        request.service('getCurrentPassword','post',req, $scope.CONFIG, function (data){
                            //$scope.loader(false);
                            if(data.statusCode == 300){
                                if (data.Result[0].password !=password ){
                                      $scope.err.password_check =true;
                                console.log("sssssssss", $scope.err.oldPassword)
                                }else{
                                    delete $scope.err.password_check;
                                }
                            }else{
                                $scope.notification(data.statusMessage,'danger');
                            }
                            
                        });
                }*/

        $scope.cancel = cancel;
        $scope.savePassword = savePassword;
    }
    app.controller('changePwdCtrl', ['$scope', '$rootScope', 'request', changePwdCtrl]);
}());
