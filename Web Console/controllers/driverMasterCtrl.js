(function() {
    var app = angular.module('eSahai');
    var driverMasterCtrl = function($scope, $rootScope, request, ctrlComm, fileUpload, $filter, TableConstants, $timeout) {
        console.log("in driverMasterCtrl : " + TableConstants.driver_master);
        $scope.tabActive('driverMaster');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.driver = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        //$scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};

        $scope.getDriverMasterList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.driver_master;
            req.params = {};
            if ($scope.admin.userType === "GROUP ADMIN") {
                req.params.group_id = $scope.admin.groupId;
            }
            /*var dateFilter = {};
            dateFilter.fieldName = 'created_date';
            dateFilter.startDate = $scope.fromDate;
            dateFilter.endDate = $scope.toDate;
            req.dateFilter = dateFilter;*/
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    angular.forEach(response.Result, function(obj) {
                        $scope.getGlobalGroupMasterList(function() {
                            angular.forEach($scope.globalGroupMasterList, function(obj1) {
                                if (obj.group_id == obj1.group_id) {
                                    obj.groupName = obj1.group_name;
                                }
                            })

                        })
                    })
                    $scope.driverMasterList = response.Result;
                    $scope.totalItems = $scope.driverMasterList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }

        if (window.location.hash == "#/Driver-Master") {
            $scope.getDriverMasterList();

        } else if (window.location.hash == "#/New-Driver-Master") {
            $scope.page.title = 'Add Driver';
            $scope.page.type = 'post';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
                window.location.hash = '#/Driver-Master';
            }
            if (document.getElementById('imageName') != undefined) {
                document.getElementById("imageName").src = 'images/upload.jpg';
            }
        } else if (window.location.hash == "#/Update-Driver-Master") {
            $scope.page.title = 'Update Driver'
            $scope.page.type = 'put';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
                window.location.hash = '#/Driver-Master';
            }
            prePoppulateValues(ctrlComm.get('driverObj'));
        } else if (window.location.hash == "#/View-Driver-Master") {
            $scope.page.title = 'View Driver'
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('driverObj'));
        }


        var addNewDriverMaster = function() {
            window.location.hash = "#/New-Driver-Master";
        }

        function prePoppulateValues(driver) {
            if (driver) {
                if ($scope.page.type == 'put') {
                    driver.is_active == 'Active' ? driver.is_active = true : driver.is_active = false;
                    if (document.getElementById('imageName') != undefined) {
                        document.getElementById("imageName").src = driver.file_url;
                    }
                }
                $scope.driver = driver;
                console.log(driver)
            } else {
                window.location.hash = "#/Driver-Master";
            }
        }

        $scope.chooseFile = function() {
            document.getElementById("uploadFile").click();
        }

        $('#uploadFile').change(function() {
            readURL();
        })


        function readURL() {
            var file = $('#uploadFile')[0].files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imageName').attr('src', e.target.result);
                }

                reader.readAsDataURL(file);
            }
        }

        var validate = function(cb) {
            if (!$scope.driver.group_id) {
                $scope.err.group_id = true;
            } else {
                delete $scope.err.group_id;
            }
            if (!$scope.driver.driver_name) {
                $scope.err.driver_name = true;
            } else {
                delete $scope.err.driver_name;
            }
            if (!$scope.driver.driver_mobile_number) {
                $scope.err.driver_mobile_number = true;
            } else {
                if (!(/^\d{10}$/).test($scope.driver.driver_mobile_number)) {
                    $scope.err.driver_mobile_number_cdn = true;
                } else {
                    delete $scope.err.driver_mobile_number;
                    delete $scope.err.driver_mobile_number_cdn;
                }
            }
            if (!$scope.driver.driver_email) {
                $scope.err.driver_email = true;
            } else {
                if (!(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/).test($scope.driver.driver_email)) {
                    $scope.err.driver_email_cdn = true;

                } else {
                    delete $scope.err.driver_email;
                    delete $scope.err.driver_email_cdn;
                }
            }
            if (!$scope.driver.license_number) {
                $scope.err.license_number = true;
            } else {
                delete $scope.err.license_number;
            }
            if (Object.keys($scope.err).length == 0) {
                if (document.getElementById("imageName").src.split('/')[document.getElementById("imageName").src.split('/').length - 1] == 'upload.jpg') {
                    $scope.notification("please upload photo", 'danger');
                } else {
                    if (document.getElementById('uploadFile').files.length && (document.getElementById('uploadFile').files[0].size / 1000).toFixed(2) > 1000) {
                        $scope.notification("Image should not exceed 1MB", 'danger');
                    } else {
                        if (cb) cb();
                    }
                }
            }
        }


        var saveDriverMaster = function(driver) {
            validate(function() {
                /* var req = {};
                 req.tableName = TableConstants.driver_master;
                 var params = {};
                 if (driver.is_active == true) {
                     driver.is_active = 'Active';
                 } else {
                     driver.is_active = 'InActive';
                 }
                 params = angular.copy(driver);
                 req.params = params;
                 console.log(angular.toJson(req))
                 request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                     if (response.statusCode == '300') {
                         window.location.hash = "#/Driver-Master"
                         $scope.notification(response.statusMessage);
                     } else if (response.statusCode == '204') {
                         $scope.notification(response.statusMessage, "danger");
                         $timeout(function() {
                             $scope.logout();
                         }, 2000)

                     } else {
                         $scope.notification(response.statusMessage);
                     }
                 })*/

                $scope.loader(true);
                driver.is_active = 'Active';
                driver.driver_pin = Math.floor(Math.random() * 9000) + 1000;

                var req = {};
                req.tableName = TableConstants.driver_master;
                req.params = driver;
                if ($scope.image_url == undefined) {
                    $scope.image_url = $('#uploadFile')[0].files[0];
                }
                console.log(angular.toJson(req));
                fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'insertRecord', $scope.CONFIG, 'post', function(data) {
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification('Driver created successfully');
                        window.location.hash = '#/Driver-Master';
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                });


            });

        }


        var viewDriverMaster = function(driver) {
            ctrlComm.put('driverObj', driver);
            window.location.hash = '#/View-Driver-Master';
        }

        var updateDriverMaster = function(driver) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.driver_master;
                var params = {};
                if (driver.is_active == true) {
                    driver.is_active = 'Active';
                } else {
                    driver.is_active = 'InActive';
                }
                params = angular.copy(driver);
                delete params['driver_id'];
                delete params['groupName'];
                var conditionalParams = {};
                conditionalParams.driver_id = driver.driver_id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                if ($('#uploadFile')[0].files[0] != null) {
                    $scope.image_url = $('#uploadFile')[0].files[0];
                }
                console.log(angular.toJson(req))
                fileUpload.uploadFileToUrl($scope.image_url, 'file', req, 'updateRecord', $scope.CONFIG, 'put', function(data) {
                    console.log(data)
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification('Driver updated successfully');
                        window.location.hash = '#/Driver-Master';
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                });
                /*request.service('updateRecord', 'put', req, $scope.CONFIG, 'put', function(response) {
                        $scope.loader(false);
                        if (response.statusCode == '300') {
                            window.location.hash = "#/Driver-Master"
                            $scope.notification(response.statusMessage);
                        } else if (response.statusCode == '204') {
                            $scope.notification(response.statusMessage, "danger");
                            $timeout(function() {
                                $scope.logout();
                            }, 2000)

                        } else {
                            $scope.notification(response.statusMessage);
                        }

                    })*/
            });
        }

        var editDriverMaster = function(driver) {
            ctrlComm.put('driverObj', driver);
            window.location.hash = "#/Update-Driver-Master";
        }


        var cancelDriverMaster = function() {
            window.location.hash = "#/Driver-Master";
        }

        var deleteDriverMaster = function(driver) {
            var input = {
                text: 'Are you sure you want to delete',
                name: driver.display_name
            }
            $scope.confirmation(input, function() {
                confirmDelete(driver);
            });
        }

        function confirmDelete(driver) {
            /*console.log(driver)
            var req = {};
            req.tableName = TableConstants.driver_master;
            var params = {};
            params.driver_id = driver.driver_id;
            req.params = params;
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == '300') {
                    $scope.getDriverMasterList(function() {
                        $scope.notification(response.statusMessage);
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else if (response.statusCode == '400' && response.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record", "danger");
                }
            })*/

            var req = {};
            req.tableName = TableConstants.driver_master;
            var params = {};
            params.is_active = 'InActive'
            var conditionalParams = {};
            conditionalParams.driver_id = driver.driver_id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            var input = {};
            input.type = 'driver';
            input.driver_id = driver.driver_id;


            request.service('commonLimitOrActiveCheck', 'post', input, $scope.CONFIG, function(response) {
                $scope.loader(false);
                console.log(response)
                if (response.statusCode == '300') {
                    request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                        /*$scope.loader(false);*/
                        if (response.statusCode == '300') {
                            $scope.getDriverMasterList(function() {
                                $scope.notification("Driver Deleted Successfully");
                            })
                        } else if (response.statusCode == '204') {
                            $scope.notification(response.statusMessage, "danger");
                            $timeout(function() {
                                $scope.logout();
                            }, 2000)
                        } else {
                            $scope.notification(response.statusMessage);
                        }
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage, "danger");
                   // driver.is_active = !driver.is_active;
                }
            })


        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var importExcelDoc = function() {
            var file = $scope.myFile;
            if (file && (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/vnd.ms-excel")) {
                $scope.loader(true);
                var obj = {};
                fileUpload.uploadFileToUrl(file, 'driver_master', obj, 'uploadExcel', $scope.CONFIG, 'post', function(data) {
                    $scope.loader(false);
                    console.log("data", data);
                    if (data.statusCode == '300') {
                        $('#uploadFile').val("");
                        $scope.notification('File uploaded successfully.', 'success');
                        $scope.getDriverMasterList(function() {
                            $scope.totalItems = $scope.driverMasterList.length;
                            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                $scope.tableRows.push($scope.totalItems);
                            if ($scope.totalItems != 0) {
                                $scope.tableRows = $scope.tableRows.sort(request.sort);
                                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                if ($scope.tableRows[1]) {
                                    $scope.viewby = $scope.tableRows[1];
                                } else {
                                    $scope.viewby = $scope.tableRows[0];
                                }
                                $scope.setItemsPerPage($scope.viewby);
                            }
                        });
                        $scope.upldfile = {};
                    }  else if (data.statusCode == '446') {
                        $('#uploadFile').val("");
                        $scope.notification(data.statusMessage, 'danger');
                        $scope.getDriverMasterList();
                        $scope.upldfile = {};


                    }
                });
            } else {
                $scope.notification('Upload excel spreadsheet only(.xls or .xlsx). ', 'info');
            }
        }

        $scope.fileUpLoad = function() {
            $scope.upldfile = {};
            document.getElementById("uploadFile").click();
        }
        $scope.catchUploadedFile = function() {
            var f = document.getElementById('uploadFile').files[0];
            if (f) {
                $scope.$apply(function() {
                    $scope.upldfile = {
                        name: f.name,
                        size: (f.size / 1024).toFixed(2) + ' KB'
                    }
                });
            }
        }



        $scope.cancelImport = function() {
            $scope.upldfile = {};
            $('#uploadFile').val("");
        }

        $scope.checkDriverMapping = function(driverObj) {
            console.log(driverObj)
            $scope.loader(true);
            req = {};
            req.type = 'driver';
            req.driver_id = driverObj.driver_id;
            request.service('commonLimitOrActiveCheck', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                console.log(response)
                if (response.statusCode == '300') {
                    if (driverObj.is_active == false) {
                        $scope.notification("By doing this action, driver from the mobile will be logged out automatically", "danger")
                    }
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage, "danger");
                    driverObj.is_active = !driverObj.is_active;
                }
            })
        }

        $scope.importExcelDoc = importExcelDoc;




        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewDriverMaster = addNewDriverMaster;
        $scope.saveDriverMaster = saveDriverMaster;
        $scope.updateDriverMaster = updateDriverMaster;
        $scope.deleteDriverMaster = deleteDriverMaster;
        $scope.cancelDriverMaster = cancelDriverMaster;
        $scope.editDriverMaster = editDriverMaster;
        $scope.viewDriverMaster = viewDriverMaster;
    }
    app.controller('driverMasterCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'fileUpload', '$filter', 'TableConstants', '$timeout', driverMasterCtrl]);
}());
