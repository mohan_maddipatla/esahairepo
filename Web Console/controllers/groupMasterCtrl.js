(function() {
    var app = angular.module('eSahai');
    var groupMasterCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout, geohash, fileUpload) {
        console.log("in groupMasterCtrl : " + TableConstants.group_master);
        $scope.tabActive('groupMaster');
        if ($scope.admin.userType === "GROUP ADMIN") {
            window.location.hash = '#/Home';
        }
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.group = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        //$scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');
        $scope.groupAttr = [];
        $scope.emergencyTypeOptions = [];
        $scope.selectedEmergencyType = [];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};
        $scope.dropdownSettings = {
            scrollable: true,
            scrollableHeight: '200px',
            showCheckAll: false,
            showUncheckAll: false
        }

        $scope.translationText = {
            buttonDefaultText: '- Select Emergency Type -',
        }


        $scope.getGroupMasterList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.group_master;
            req.params = {};
            /*var dateFilter = {};
            dateFilter.fieldName = 'created_date';
            dateFilter.startDate = $scope.fromDate;
            dateFilter.endDate = $scope.toDate;
            req.dateFilter = dateFilter;*/
            console.log(angular.toJson(req));
            request.service('getGroupMasterList', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.getGroupTypeList(function() {
                        angular.forEach(response.Result, function(obj) {
                            angular.forEach($scope.groupTypeList, function(obj1) {
                                if (obj1.id == obj.group_type) {
                                    obj.groupTypeName = obj1.type_name;
                                }
                            })
                        })
                    })

                    $scope.groupMasterList = response.Result;
                    $scope.totalItems = $scope.groupMasterList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                }
            })
        }

        /* $scope.getGroupMasterList(function() {

         });
         $scope.getEmergencyTypeList(function() {
             angular.forEach($scope.emergencyTypeList, function(obj) {
                 $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
             })
         })*/
        if (window.location.hash == "#/Group-Master") {
            $scope.getGroupMasterList();
        } else if (window.location.hash == "#/New-Group-Master") {
            $scope.page.title = 'Add Group';
            $scope.page.type = 'post';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
                window.location.hash = '#/Group-Master';
            }
            $scope.getEmergencyTypeList(function() {
                angular.forEach($scope.emergencyTypeList, function(obj) {
                    $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
                })
            })
        } else if (window.location.hash == "#/Update-Group-Master") {
            $scope.page.title = 'Update Group'
            $scope.page.type = 'put';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
                window.location.hash = '#/Group-Master';
            }
            $scope.getEmergencyTypeList(function() {
                    angular.forEach($scope.emergencyTypeList, function(obj) {
                        $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
                    })
                })
                /*if(window.localStorage.getItem('hash') == undefined){*/
            prePoppulateValues(ctrlComm.get('groupObj'));
            /*}*/
            //$scope.getGroupMasterList();
        } else if (window.location.hash == '#/View-Group-Master') {
            $scope.page.title = 'View Group';
            $scope.page.type = 'view';
            $scope.getEmergencyTypeList(function() {
                angular.forEach($scope.emergencyTypeList, function(obj) {
                    $scope.emergencyTypeOptions.push({ id: obj.id, label: obj.type_name })
                })
            })
            prePoppulateValues(ctrlComm.get('groupObj'));
        }

        var viewGroupMaster = function(group) {
            ctrlComm.put('groupObj', group);
            window.location.hash = '#/View-Group-Master';
        }



        var addNewGroupMaster = function() {
            window.location.hash = "#/New-Group-Master";
        }

        $scope.addAttribute = function() {
            angular.forEach($scope.groupAttr, function(obj) {
                if (obj.flag == 'edit') {
                    obj.flag = undefined;
                }
                var temp = [];
                console.log(obj)
                angular.forEach($scope.group_e_types_list, function(obj1) {
                    if (obj.group_e_type_id != obj1.id) {
                        temp.push(obj1);
                    }
                })
                $scope.group_e_types_list = temp;
            })

            $scope.groupAttr.push({ 'group_e_type_id': '', 'attribute_details': '' });
        }

        $scope.saveAttribute = function(attrObj, group) {
            $scope.loader(true);
            if (attrObj.group_e_type_id != '' && attrObj.group_e_type_id != undefined && attrObj.attribute_details != '' && attrObj.attribute_details != undefined) {

                var req = {};
                req.tableName = TableConstants.group_attributes;
                var params = {};
                angular.forEach($scope.groupAttributesList, function(obj) {
                    if (obj.id == attrObj.group_e_type_id)
                        attrObj.attribute_name = obj.type_name;
                })
                params = angular.copy(attrObj);
                params.group_id = group.group_id;
                req.params = params;
                console.log(angular.toJson(req))


                console.log(attrObj)
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(group)
                        $scope.notification('Attribute saved successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            } else if (!attrObj.group_e_type_id) {
                $scope.notification('Please select attibute', 'danger');
            } else if (!attrObj.attribute_details) {
                $scope.notification('Please enter attibute value', 'danger');
            }
        }

        $scope.updateAttribute = function(attrObj, group) {
            if (attrObj.group_e_type_id != '' && attrObj.group_e_type_id != undefined && attrObj.attribute_details != '' && attrObj.attribute_details != undefined) {
                var req = {};
                req.tableName = TableConstants.group_attributes;
                var params = {};
                angular.forEach($scope.groupAttributesList, function(obj) {
                    if (obj.id == attrObj.group_e_type_id)
                        attrObj.attribute_name = obj.type_name;
                })
                params = angular.copy(attrObj);
                delete params['id'];
                delete params['flag'];
                var conditionalParams = {};
                conditionalParams.id = attrObj.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    /*$scope.loader(false);*/
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(group)
                        $scope.notification('Attribute updated successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            } else if (!attrObj.group_e_type_id) {
                $scope.notification('Please select attibute', 'danger');
            } else if (!attrObj.attribute_details) {
                $scope.notification('Please enter attibute value', 'danger');
            }
        }

         $scope.deleteAttribute  = function(attrObj, group) {
            console.log("group",attrObj)

            var input = {
                text: 'Are you sure you want to delete',
                name: attrObj.attribute_name
            }
            $scope.confirmation(input, function() {
                confirmAttributeDelete(attrObj, group);
            });
        }

        function confirmAttributeDelete(attrObj, group) {
            if (attrObj.id) {
                var req = {};
                req.tableName = TableConstants.group_attributes;
                var params = {};
                params.id = attrObj.id;
                req.params = params;
                request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                    /*$scope.loader(false);*/
                    if (typeof response == 'string') {
                        response = JSON.parse(response)
                    }
                    if (response.statusCode == '300') {
                        prePoppulateAttrValues(group);
                        $scope.notification('Attribute deleted successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)
                    }
                })
            } else {
                var temp = []
                angular.forEach($scope.groupAttr, function(obj) {
                    if (obj.id != undefined) {
                        temp.push(obj)
                    }
                })
                $scope.groupAttr = temp;
            }
        }

        $scope.editAttribute = function(attrObj, group) {
            var temp = [];
            var arr = [];
            var a = [];
            angular.forEach($scope.groupAttr, function(obj) {
                a.push(obj.group_e_type_id)

            })
            angular.forEach($scope.groupAttr, function(obj) {
                if (obj.flag == 'edit') {
                    obj.flag = undefined;
                }

                angular.forEach($scope.global_group_e_types_list, function(obj1) {
                    if (obj.group_e_type_id != obj1.id) {
                        if (arr.indexOf(obj1.id) == -1 && a.indexOf(obj1.id) == -1) {
                            temp.push(obj1);
                            arr.push(obj1.id);
                        }

                    }

                });
            })

            angular.forEach($scope.global_group_e_types_list, function(obj) {
                if (attrObj.group_e_type_id == obj.id) {
                    temp.push(obj);
                }
            })

            $scope.group_e_types_list = temp;

            angular.forEach($scope.groupAttr, function(obj) {
                if (obj.id == attrObj.id) {
                    obj.flag = 'edit';
                }
            })
        }

        function prePoppulateAttrValues(group, cb) {
            var req = {};
            var dateFilter = {};
            req.tableName = TableConstants.group_attributes;
            req.params = {};
            req.params.group_id = group.group_id;
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                /*$scope.loader(false);*/
                if (response.statusCode == '300') {
                    $scope.groupAttr = response.Result;
                    var req = {};
                    req.tableName = TableConstants.group_e_types;
                    req.params = {};
                    console.log(angular.toJson(req));
                    request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                        /*$scope.loader(false);*/
                        if (response.statusCode == '300') {
                            $scope.group_e_types_list = response.Result;
                            $scope.global_group_e_types_list = response.Result;
                        }
                    })
                    if (cb) {
                        cb()
                    }
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                }
            })
        }

        function prePoppulateValues(group) {
            console.log(group)
            if (group) {
                if ($scope.page.type == 'put') {
                    group.is_active == 'Active' ? group.is_active = true : group.is_active = false;
                }

                if (group.emergency_type && group.emergency_type.split(',').length) {
                    angular.forEach(group.emergency_type.split(','), function(obj) {
                        if (obj != '') {
                            $scope.selectedEmergencyType.push({ 'id': obj })
                        }
                    })
                    var emergencyTypeNames = [];
                    angular.forEach($scope.selectedEmergencyType, function(obj) {
                        angular.forEach($scope.emergencyTypeList, function(obj1) {
                            if (obj.id == obj1.id) {
                                emergencyTypeNames.push(obj1.type_name)
                            }
                        })
                    })
                    group.emergencyTypeNames = emergencyTypeNames.join();
                }
                $scope.group = group;
                prePoppulateAttrValues(group, function() {})
            } else {
                window.location.hash = "#/Group-Master";
            }
        }

        $('.dropdown-toggle').on('click', function() {
            if ($scope.err.selectedEmergencyType)
                delete $scope.err.selectedEmergencyType;
            $('.dropdown-toggle').prop('style', 'border-color: #dbe1e8');
        })

        var validate = function(cb) {
            if (!$scope.group.group_name) {
                $scope.err.group_name = true;
            } else {
                delete $scope.err.group_name;
            }

            if (!$scope.group.group_short_name) {
                $scope.err.group_short_name = true;
            } else {
                delete $scope.err.group_short_name;
            }

            if (!$scope.group.group_type) {
                $scope.err.group_type = true;
            } else {
                delete $scope.err.group_type;
            }

            if (!$scope.group.group_limit) {
                $scope.err.group_limit = true;
            } else {
                if (!(/^[0-9]+$/).test($scope.group.group_limit)) {
                    $scope.err.group_limit_cdn = true;

                } else {
                    delete $scope.err.group_limit;
                    delete $scope.err.group_limit_cdn;
                }

            }

            if (!$scope.selectedEmergencyType.length) {
                $('.dropdown-toggle').prop('style', 'border-color: #e74c3c');
                $scope.err.selectedEmergencyType = true;
            } else {
                delete $scope.err.selectedEmergencyType;
            }

            if (!$scope.group.group_latitude) {
                $scope.err.group_latitude = true;
            } else {
                if (!(/^(\+|-)?(?:90(?:(?:\.0{1,15})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,15})?))$/).test($scope.group.group_latitude)) {
                    $scope.err.group_latitude_cdn = true;

                } else {
                    delete $scope.err.group_latitude;
                    delete $scope.err.group_latitude_cdn;
                }
            }

            if (!$scope.group.group_longitude) {
                $scope.err.group_longitude = true;
            } else {
                if (!(/^(\+|-)?(?:180(?:(?:\.0{1,15})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,15})?))$/).test($scope.group.group_longitude)) {
                    $scope.err.group_longitude_cdn = true;
                } else {
                    delete $scope.err.group_longitude;
                    delete $scope.err.group_longitude_cdn;
                }
            }

            if (!$scope.group.emergency_contact) {
                $scope.err.emergency_contact = true;
            } else {
                delete $scope.err.emergency_contact;
            }

            if (!$scope.group.emergency_email) {
                $scope.err.emergency_email = true;
            } else {
                if (!(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/).test($scope.group.emergency_email)) {
                    $scope.err.emergency_email_cdn = true;

                } else {
                    delete $scope.err.emergency_email;
                    delete $scope.err.emergency_email_cdn;
                }
            }

            if (!$scope.group.emergency_number) {
                $scope.err.emergency_number = true;
            } else {
                if (!(/^[0-9]{1,11}$/).test($scope.group.emergency_number)) {
                    /*|| ($scope.user.mobile.length)>10*/
                    $scope.err.emergency_number_cdn = true;

                } else {
                    delete $scope.err.emergency_number;
                    delete $scope.err.emergency_number_cdn;
                    //delete $scope.err.mobile_cdn;

                }

            }

            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }


        var saveGroupMaster = function(group) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.group_master;
                group.emergency_type = [];
                angular.forEach($scope.selectedEmergencyType, function(obj) {
                    group.emergency_type.push(obj.id);
                })
                group.emergency_type = group.emergency_type.join();
                var params = {};
                group.is_active = 'Active';
                group.geoHashCode = geohash.encode(group.group_latitude, group.group_longitude);
                params = angular.copy(group);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        $scope.notification('Group saved successfully');
                        window.location.hash = "#/Group-Master"
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });

        }


        var checkActiveStatus = function(group, cb) {
            if (group.is_active == 'InActive') {
                if (group.active_count == 0) {
                    if (cb) cb();
                } else {
                    $scope.notification("Cannot change the group  status," + group.active_count + " ambulance(s) is/are active in this group", "danger")
                }

            } else {
                if (cb) cb();
            }
        }
        var updateGroupMaster = function(group) {
            validate(function() {

                if (Number(group.active_count) <= Number(group.group_limit)) {
                    var req = {};
                    req.tableName = TableConstants.group_master;
                    group.emergency_type = [];
                    angular.forEach($scope.selectedEmergencyType, function(obj) {
                        group.emergency_type.push(obj.id);
                    })
                    group.emergency_type = group.emergency_type.join();
                    var params = {};
                    if (group.is_active == true) {
                        group.is_active = 'Active';
                    } else {
                        group.is_active = 'InActive';
                    }
                    checkActiveStatus(group, function() {
                        group.geoHashCode = geohash.encode(group.group_latitude, group.group_longitude);
                        params = angular.copy(group);
                        delete params['group_id'];
                        delete params['groupTypeName'];
                        delete params['emergencyTypeName'];
                        delete params['emergencyTypeNames'];
                        delete params['active_count'];
                        var conditionalParams = {};
                        conditionalParams.group_id = group.group_id;
                        req.params = params;
                        req.conditionalParams = conditionalParams;
                        console.log(angular.toJson(req))
                        request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                            if (response.statusCode == '300') {
                                window.location.hash = "#/Group-Master"
                                $scope.notification(response.statusMessage);
                            } else if (response.statusCode == '204') {
                                $scope.notification(response.statusMessage, "danger");
                                $timeout(function() {
                                    $scope.logout();
                                }, 2000)

                            } else {
                                $scope.notification(response.statusMessage);
                            }

                        })

                    });

                } else {
                    $scope.notification("Group limit should be more than active ambulances (" + group.active_count + ") of this group", "danger");
                }

            })

        }
        var editGroupMaster = function(group) {
            ctrlComm.put('groupObj', group);
            window.location.hash = "#/Update-Group-Master";
        }


        var cancelGroupMaster = function() {
            window.location.hash = "#/Group-Master";
        }

        var deleteGroupMaster = function(group) {
            var input = {
                text: 'Are you sure you want to delete',
                name: group.display_name
            }
            $scope.confirmation(input, function() {
                confirmDelete(group);
            });
        }

        function confirmDelete(group) {
            console.log(group)
                /*var req = {};
                req.tableName = TableConstants.group_master;
                var params = {};
                params.group_id = group.group_id;
                req.params = params;
                request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                    if (typeof response == 'string') {
                        response = JSON.parse(response)
                    }
                    if (response.statusCode == '300') {
                        $scope.getGroupMasterList(function() {
                            $scope.notification(response.statusMessage);
                        })
                    } else if (response.statusCode == '400' && response.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                        $scope.notification("Cannot delete parent record","danger");
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();

                        }, 2000)
                    }
                })*/
            if (group.active_count == 0) {
                var req = {};
                req.tableName = TableConstants.group_master;
                var params = {};
                group.is_active = 'InActive';
                params = angular.copy(group);
                delete params['group_id'];
                delete params['groupTypeName'];
                delete params['active_count'];
                var conditionalParams = {};
                conditionalParams.group_id = group.group_id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    /*$scope.loader(false);*/
                    if (response.statusCode == '300') {
                        $scope.getGroupMasterList(function() {
                            $scope.notification("Group Deleted Successfully");
                        })
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            } else {
                $scope.notification("Cannot delete the group ," + group.active_count + " ambulance(s) is/are active in this group",
                    "danger")
            }

        }


        var basic_phone = '';
        $scope.$watch('group.emergency_number', function(val) {
            if (val) {
                if (!val.toString().match(/^[0-9]*$/)) {
                    $scope.group.emergency_number = basic_phone;
                } else {
                    if (val.length <= 11) {
                        basic_phone = val;
                    } else {
                        $scope.group.emergency_number = basic_phone;
                    }
                }
            }
        });

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        var importExcelDoc = function() {
            var file = $scope.myFile;
            if (file && (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type == "application/vnd.ms-excel")) {
                $scope.loader(true);
                var obj = {};
                fileUpload.uploadFileToUrl(file, 'group_master', obj, 'uploadExcel', $scope.CONFIG, 'post', function(data) {
                    $scope.loader(false);
                    console.log("data", data);
                    if (data.statusCode == '300') {
                        $('#uploadFile').val("");
                        $scope.notification('File uploaded successfully.', 'success');
                        $scope.getGroupMasterList(function() {
                            $scope.totalItems = $scope.groupMasterList.length;
                            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                                $scope.tableRows.push($scope.totalItems);
                            if ($scope.totalItems != 0) {
                                $scope.tableRows = $scope.tableRows.sort(request.sort);
                                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                                if ($scope.tableRows[1]) {
                                    $scope.viewby = $scope.tableRows[1];
                                } else {
                                    $scope.viewby = $scope.tableRows[0];
                                }
                                $scope.setItemsPerPage($scope.viewby);
                            }
                        });
                        $scope.upldfile = {};
                    } else if (data.statusCode == "446") {
                        $('#uploadFile').val("");

                        $scope.notification(data.statusMessage, 'danger');
                        $scope.getGroupMasterList();
                        $scope.upldfile = {};

                    }
                });
            } else {
                $scope.notification('Upload excel spreadsheet only(.xls or .xlsx). ', 'info');
            }
        }

        $scope.fileUpLoad = function() {
            $scope.upldfile = {};
            document.getElementById("uploadFile").click();
        }
        $scope.catchUploadedFile = function() {
            var f = document.getElementById('uploadFile').files[0];
            if (f) {
                $scope.$apply(function() {
                    $scope.upldfile = {
                        name: f.name,
                        size: (f.size / 1024).toFixed(2) + ' KB'
                    }
                });
            }
        }



        $scope.cancelImport = function() {
            $scope.upldfile = {};
            $('#uploadFile').val("");
        }

        $scope.importExcelDoc = importExcelDoc;


        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewGroupMaster = addNewGroupMaster;
        $scope.saveGroupMaster = saveGroupMaster;
        $scope.updateGroupMaster = updateGroupMaster;
        $scope.deleteGroupMaster = deleteGroupMaster;
        $scope.cancelGroupMaster = cancelGroupMaster;
        $scope.editGroupMaster = editGroupMaster;
        $scope.viewGroupMaster = viewGroupMaster;
    }


    app.controller('groupMasterCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', 'geohash', 'fileUpload', groupMasterCtrl]);
}());
