(function() {
    var app = angular.module('eSahai');
    var countryListCtrl = function($scope, $rootScope, request, ctrlComm, fileUpload, $filter, TableConstants, $timeout) {
        $scope.tabActive('countryList', 'appSettings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.page = {};
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.country = {};
        $scope.err = {};
        $scope.getCountryList = function(cb) {
            console.log("sssssssssss")
            $scope.loader(true);
            var req = {};

            req.tableName = TableConstants.country_list;
            req.params = {};

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {

                    $scope.countryList = response.Result;
                    $scope.totalItems = $scope.countryList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }


        if (window.location.hash == "#/Country-List") {
            $scope.page.title = 'Country List';
            $scope.getCountryList();

        } else if (window.location.hash == "#/Add-Country") {
            $scope.page.title = 'Add Country';
            $scope.page.type = 'post';

        } else if (window.location.hash == "#/Update-Country") {
            $scope.page.title = 'Update Country';
            $scope.page.type = 'put';
            console.log(ctrlComm.get('countryObj'))
            prePoppulateValues(ctrlComm.get('countryObj'));
        } else if (window.location.hash == "#/View-Country") {
            $scope.page.title = 'View Country';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('countryObj'));
        }
        var addNewCountry = function() {
            window.location.hash = "#/Add-Country";
        }

        var viewCountry = function(country) {
            ctrlComm.put('countryObj', country);
            window.location.hash = '#/View-Country';
        }

        function prePoppulateValues(country) {
            console.log("country" + angular.toJson(country))
            if (country) {
                console.log("sddddddddddddd")


                $scope.country = country;
            } else {
                console.log("sddddddddddddd")
                window.location.hash = "#/Country-List";
            }
        }
        var saveCountry = function(country) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.country_list;
                var params = {};

                params = angular.copy(country);;

                req.params = params;
                console.log(angular.toJson(req));
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);

                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = '#/Country-List';
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }


        var updateCountry = function(country) {
            validate(function() {
                console.log(country)
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.country_list;
                var params = {};

                params = angular.copy(country);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = country.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(data) {
                    $scope.loader(false);
                    if (data.statusCode == '300') {
                        $scope.notification(data.statusMessage);
                        window.location.hash = '#/Country-List';
                    } else if (data.statusCode == '204') {
                        $scope.notification(data.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    }
                })
            })
        }



        var validate = function(cb) {
            if (!$scope.country.country) {
                $scope.err.country = true;
            } else {
                delete $scope.err.country;
            }
            if (!$scope.country.code) {
                $scope.err.code = true;
            } else {
                delete $scope.err.code;
            }


            if (Object.keys($scope.err).length == 0) {


                if (cb) cb();
            }
        }
        var editCountry = function(country) {
            ctrlComm.put('countryObj', country);
            window.location.hash = "#/Update-Country";
        }


        var cancelCountry = function() {
            window.location.hash = "#/Country-List";
        }

        var deleteCountry = function(country) {
            var input = {
                text: 'Are you sure you want to delete',
                name: country.country
            }
            $scope.confirmation(input, function() {
                confirmDelete(country);
            });
        }

        function confirmDelete(country) {

            var req = {};
            req.tableName = TableConstants.country_list;
            var params = {};
            params.id = country.id;
            req.params = params;
            console.log(angular.toJson(req))
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                if (typeof response == 'string') {
                    response = JSON.parse(response)
                }
                if (response.statusCode == "300") {
                    console.log("ddddddddddddddd")
                    $scope.notification(response.statusMessage);
                    $scope.getCountryList();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })


        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewCountry = addNewCountry;
        $scope.saveCountry = saveCountry;
        $scope.updateCountry = updateCountry;
        $scope.deleteCountry = deleteCountry;
        $scope.cancelCountry = cancelCountry;
        $scope.editCountry = editCountry;
        $scope.viewCountry = viewCountry;


    }
    app.controller('countryListCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'fileUpload', '$filter', 'TableConstants', '$timeout', countryListCtrl]);
}());
