(function() {
    var app = angular.module('eSahai');
    var groupTypeCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("in groupTypeCtrl--> " + TableConstants.group_types);
        $scope.tabActive('groupType', 'settings');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.groupType = {};
        $scope.page = {};
        $scope.err = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.toDate = $filter('date')(new Date(), 'MM-dd-yyyy');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType === "EMPLOYEE" || $scope.admin.userType === "CUSTOMER CARE EXECUTIVE") {
            window.location.hash = '#/Home';
        }




        $scope.getGroupTypeList = function() {
            $scope.totalItems = $scope.groupTypeList.length;
            if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                $scope.tableRows.push($scope.totalItems);

            if ($scope.totalItems != 0) {
                $scope.tableRows = $scope.tableRows.sort(request.sort);
                $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                if ($scope.tableRows[1]) {
                    $scope.viewby = $scope.tableRows[1];
                } else {
                    $scope.viewby = $scope.tableRows[0];
                }
                $scope.setItemsPerPage($scope.viewby);
            }
        };
        if (window.location.hash == "#/Group-Type") {
            $scope.getGroupTypeList();
        } else if (window.location.hash == "#/New-Group-Type") {
            $scope.page.title = 'Add Group Type';
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Group-Type") {
            $scope.page.title = 'Update Group Type';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('groupTypeObj'));
        }

        function prePoppulateValues(groupType) {
            if (groupType) {
                $scope.groupType = groupType;
            } else {
                window.location.hash = "#/Group-Type"
            }
        }

        var validate = function(cb) {
            if (!$scope.groupType.type_name) {
                $scope.err.type_name = true;
            } else {
                delete $scope.err.type_name;
            }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }

        var addNewGroupType = function() {
            window.location.hash = '#/New-Group-Type';
        }

        var cancelGroupType = function() {
            window.location.hash = "#/Group-Type"
        }

        var saveGroupType = function(groupType) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.group_types;
                var params = {};
                params = angular.copy(groupType);
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Group-Type"
                        $scope.notification(response.statusMessage);
			$scope.getGroupTypeList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            })
        }

        var editGroupType = function(groupType) {
            ctrlComm.put('groupTypeObj', groupType);
            window.location.hash = "#/Update-Group-Type";
        }

        var updateGroupType = function(groupType) {
            validate(function() {
                /*$scope.loader(true);*/
                var req = {};
                req.tableName = TableConstants.group_types;
                var params = {};
                params = angular.copy(groupType);
                delete params['id'];
                var conditionalParams = {};
                conditionalParams.id = groupType.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    /*$scope.loader(false);*/
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Group-Type"
                        $scope.notification(response.statusMessage);
			$scope.getGroupTypeList();
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });
        }

        var deleteGroupType = function(obj) {
            var input = {
                text: 'Are you sure you want to delete',
                name: obj.name
            }
            $scope.confirmation(input, function() {
                confirmDeleteGroupType(obj);
            });
        }

        function confirmDeleteGroupType(obj) {
            var req = {};
            req.tableName = TableConstants.group_types;
            var params = {};
            params.id = obj.id;
            req.params = params;
            //$scope.loader(true);
            console.log(angular.toJson(obj));
            request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(data) {
                //$scope.loader(false);
                if (typeof data == 'string') {
                    data = JSON.parse(data)
                }
                if (data.statusCode == '300') {
                    $scope.getGroupTypeList(function() {
                        $scope.notification(data.statusMessage);
                    });
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else if (response.statusCode == '400' && response.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                    $scope.notification("Cannot delete parent record", "danger");
                } else {
                    $scope.notification(data.statusMessage);
                }
            });
        }
        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewGroupType = addNewGroupType;
        $scope.cancelGroupType = cancelGroupType;
        $scope.saveGroupType = saveGroupType;
        $scope.updateGroupType = updateGroupType;
        $scope.editGroupType = editGroupType;
        $scope.deleteGroupType = deleteGroupType;
    }
    app.controller('groupTypeCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', groupTypeCtrl]);
}());
