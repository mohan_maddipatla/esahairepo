(function() {
    var app = angular.module('eSahai');
    var mapCtrl = function($scope, $rootScope, request, ctrlComm, $interval, $filter, TableConstants, $timeout, NgMap, $location, $http,statusConstants) {
        $scope.tabActive('map', 'job');
        NgMap.getMap().then(function(map) {
            $scope.map = map;
        });
        /*alert($(window).height());   // returns height of browser viewport
        alert($(document).height());
        alert(screen.height);*/
        $scope.hospitalStatus = false;
        $scope.hospital = false;
        $scope.ambulanceList = [];
        $scope.reset = false;
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');
        $scope.trackObj = {};
        $scope.countObj1 = 0;
        $scope.countObj2 = 0;
        $scope.countObj3 = 0;
        /* $scope.trackObj = {};
         $scope.trackObj.origin = [17.44426582,78.38600723];
         $scope.trackObj.destination = [16.289205,80.4257825]*/

        $scope.booking_id = null;
        var promise;
        $scope.getAmbulancesList = function(cb) {
            $scope.free = 0;
            $scope.offline = 0;
            $scope.onRoute = 0;

            /*  var free = 0;
              var offline = 0;
              var onRoute = 0;*/
            /*$scope.loader(true);*/
            var req = {};
            if ($scope.admin.userType === "GROUP ADMIN") {
                req.groupId = $scope.admin.groupId;
                console.log($scope.admin.groupId)
            }
            console.log(angular.toJson(req));
            request.service('getAmbulanceDriverStatusList', 'post', req, $scope.CONFIG, function(response) {
                /*$scope.loader(false);*/
                if (response.statusCode == '300') {
                    $scope.ambulanceList = [];
                    $scope.ambulancesList = response.responseData;
                    angular.forEach($scope.ambulancesList, function(obj) {
                        if (obj.ambulanceDetails) {
                            obj.ambulance_number = obj.ambulanceDetails.ambulance_number;
                            obj.vehicleTypeName = obj.ambulanceDetails.vehicleTypeName;
                            obj.ambulance_mobile = obj.ambulanceDetails.ambulance_mobile;
                            obj.group = obj.ambulanceDetails.group;
                        }
                        if (obj.driverDetails) {
                            obj.driver_name = obj.driverDetails.driver_name;
                            obj.driver_mobile_number = obj.driverDetails.driver_mobile_number;
                        }
                        var params = {};
                        var latlng = [];
                        latlng.push(parseFloat(obj.latitude), parseFloat(obj.longitude));
                        $scope.center = latlng;
                        params.pos = latlng;
                        params.id = obj.id;
                        params.ambulance_number = obj.ambulance_number;
                        params.driver_name = obj.driver_name;
                        params.status = obj.status;
                        params.vehicleTypeName = obj.vehicleTypeName;
                        params.ambulance_mobile = obj.ambulance_mobile;
                        params.group = obj.group;
                        params.image = {};
                        params.image.size = [30, 30];
                        params.image.origin = [0, 0];
                        params.image.anchor = [0, 32];
                        /*if (obj1.status == 'Offline') {
                            params.image.url = 'images/gray.png';
                        } else if (obj1.status == 'Online') {
                            params.image.url = 'images/green.png';
                        } else {
                            params.image.url = 'images/red.png';
                        }*/
                        if (obj.status == 'Offline') {
                            params.style = 'gray';
                            $scope.offline++;
                        } else if (obj.status == 'Online' && obj.is_booked == 'No') {
                            params.style = 'green';
                            $scope.free++;
                        } else if (obj.status == 'Online' && obj.is_booked == 'Yes') {
                            params.style = 'red';
                            $scope.onRoute++;
                        }
                        /*params.image.size = [30, 30];
                        params.image.origin = [0, 0];
                        params.image.anchor = [0, 32];
                        if(obj1.status == 'Offline'){
                          params.image.url = 'images/gray.png';
                        }else if(obj.status == 'Online'){
                          params.image.url = 'images/red.png';
                        }else {
                          params.image.url = 'images/green.png';
                        }*/
                        $scope.ambulanceList.push(params)
                    })
                    $scope.freeCount = $scope.free;
                    $scope.onRouteCount = $scope.onRoute;
                    $scope.offlineCount = $scope.offline;
                    var promise = $interval(function() {
                        console.log($location.path())
                        if ($location.path() == '/Map' && $scope.booking_id == null && !$scope.hospitalStatus || $scope.hospital) {
                            $scope.getAmbulancesList();
                        }
                        $interval.cancel(promise);
                    }, 1000 * 60);
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                }
            })
        }

        $scope.refresh = function() {
            $scope.bookingObj = '';
            $scope.booking_id = null;
            $scope.hospitalStatus = false;
            $scope.hospital = false;
            $scope.getAmbulancesList();
            $scope.trackObj = {};

        }

        $scope.getHospitalsList = function(status) {
            console.log(status)
            $scope.hospitalStatus = status;
            $scope.bookingObj = '';
            if (status == true) {
                $scope.hospitalObj = [];
                $scope.hospitalsList = [];
                angular.forEach($scope.globalGroupMasterList, function(obj1) {
                    console.log(obj1)
                    var params = {};
                    var latlng = [];
                    latlng.push(parseFloat(obj1.group_latitude), parseFloat(obj1.group_longitude));
                    $scope.center = latlng;
                    params.pos = latlng;
                    params.id = obj1.group_id;
                    params.group_name = obj1.group_name;
                    params.groupTypeName = obj1.groupTypeName;
                    params.group_address = obj1.group_address;
                    params.image = {};
                    /*  params.image.size = [30, 30];
                      params.image.origin = [0, 0];
                      params.image.anchor = [0, 32];*/
                    params.image.url = 'images/hospital.png'
                    $scope.hospitalObj.push(params);
                    $scope.hospitalsList.push(params);

                    console.log($scope.hospitalObj)

                })
            } else {
                $scope.getAmbulancesList();
            }
        }

        $scope.getBookingList = function() {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.emergency_booking;
            req.params = {};
            if ($scope.admin.userType === "GROUP ADMIN") {
                req.params.group_id = $scope.admin.groupId;
            }
            var dateFilter = {};
            dateFilter.fieldName = 'created_datetime';
            dateFilter.startDate = $scope.fromDate;
            dateFilter.endDate = $scope.toDate;
            req.dateFilter = dateFilter;
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.bookingsList = response.Result;
                    $scope.confirmedBookingList = [];
                    angular.forEach($scope.bookingsList, function(obj) {
                        if (obj.booking_status == 'Trip Closed') {
                            $scope.confirmedBookingList.push(obj)
                        }
                    })
                }
            })

        }
        if (window.location.hash == "#/Map" && $scope.booking_id == null) {
            $scope.getAmbulancesList();
            $scope.getBookingList();
            //$scope.innerHeight = (window.innerHeight - 110) + 'px';
            //console.log($scope.innerHeight)

        }




        /* $scope.trackConfirmedBooking = function(bookingObj) {
             $scope.loader(true);
             $scope.trackObj = {};
             bookingObj = JSON.parse(bookingObj);
             console.log(angular.toJson(bookingObj))
             $scope.booking_id = bookingObj.booking_id;
             var req = {};
             var params = {};
             req.tableName = TableConstants.tracking;
             params.booking_id = $scope.booking_id;
             req.params = params;
             var orderByParams = {};
             orderByParams.created_date = '';
             req.orderByParams = orderByParams;
             console.log(angular.toJson(req))
             request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                 console.log(response)
                 $scope.loader(false);
                 if (response.statusCode == '300') {
                     $scope.trackObj = response.Result[0];
                     console.log($scope.trackObj)
                     $scope.moverObj = [];
                     var params = {};
                     params.pos = parseFloat(response.Result[0].latitude) + ', ' + parseFloat(response.Result[0].longitude);
                     params.id = response.Result[0].id;
                     params.bookingStatus = response.Result[0].booking_status;
                     params.image = 'images/ambulance.png';
                     $scope.moverObj.push(params)
                     request.service('getAmbulanceDriverStatusList', 'post', { ambulance_id: $scope.trackObj.ambulance_id,driver_id: $scope.trackObj.driver_id,booking_id: $scope.trackObj.booking_id }, $scope.CONFIG, function(infoResponse) {
                         console.log(infoResponse)
                             $scope.InfoObj = infoResponse.responseData[0];
                             console.log($scope.InfoObj)
                             if (($scope.trackObj.booking_status.toUpperCase() === 'ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'WAY TO EMERGENCY LOCATION') && $scope.countObj1 == 0) {
                                 var origin = [];
                                 var destination = [];
                                 var waypoints = [];
                                 $scope.countObj1++;
                                 origin.push(parseFloat($scope.trackObj.latitude), parseFloat($scope.trackObj.longitude));
                                 destination.push(parseFloat(response.Result[response.Result.length - 1].emergency_latitude), parseFloat(response.Result[response.Result.length - 1].emergency_longitude));
                                 $scope.trackObj.origin = origin;
                                 $scope.trackObj.destination = destination;
                                 $scope.trackObj.waypoints = waypoints;

                                 $scope.customMarkerList = [{
                                     pos: origin,
                                     image: {
                                         url: 'images/ambulance_start.png',

                                     },
                                     state: 'ambulance',
                                     ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                     vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                     driver_name: $scope.InfoObj.driverDetails.driver_name,
                                     driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                                 }, {
                                     pos: destination,
                                     image: {
                                         url: 'images/emergency.png',

                                     },
                                     state: 'emergency',
                                     customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                     emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                     customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                                 }]
                                 console.log($scope.trackObj)


                             } else if (($scope.trackObj.booking_status.toUpperCase() === 'AT EMERGENCY LOCATION' || $scope.trackObj.booking_status.toUpperCase() === 'WAY BACK TO HOSPITAL' || $scope.trackObj.booking_status.toUpperCase() === 'TRIP CLOSED') && $scope.countObj2 == 0) {
                                 $scope.countObj2++;
                                 var origin = [];
                                 var destination = [];
                                 var waypoints = [];
                                 var loc = {};
                                 var location = {};
                                 var loc = { 'lat': parseFloat(response.Result[response.Result.length - 1].emergency_latitude), 'lng': parseFloat(response.Result[response.Result.length - 1].emergency_longitude) };
                                 origin.push(parseFloat($scope.trackObj.latitude), parseFloat($scope.trackObj.longitude));
                                 destination.push(parseFloat($scope.trackObj.end_latitude), parseFloat($scope.trackObj.end_longitude));
                                 location.location = loc;
                                 waypoints.push(location);
                                 $scope.trackObj.origin = origin;
                                 $scope.trackObj.destination = destination;
                                 $scope.trackObj.waypoints = waypoints;

                                 $scope.customMarkerList = [{
                                     pos: origin,
                                     image: {
                                         url: 'images/ambulance_start.png',

                                     },
                                     state: 'ambulance',
                                     ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                     vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                     driver_name: $scope.InfoObj.driverDetails.driver_name,
                                     driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                                 }, {
                                     pos: parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ', ' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude),
                                     image: {
                                         url: 'images/emergency.png',

                                     },
                                     state: 'emergency',
                                     customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                     emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                     customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                                 }, {
                                     pos: destination,
                                     image: {
                                         url: 'images/hospital.png',

                                     },
                                     state: 'hospital',
                                     hospital_name: $scope.InfoObj.bookingDetails.hospital,

                                 }]
                             } else if ($scope.countObj2 != 0 && $scope.countObj3 == 0) {
                                 var origin = [];
                                 var destination = [];
                                 var waypoints = [];
                                 var loc = {};
                                 var location = {};
                                 $scope.countObj3++;
                                 var loc = { 'lat': parseFloat(response.Result[response.Result.length - 1].emergency_latitude), 'lng': parseFloat(response.Result[response.Result.length - 1].emergency_longitude) };
                                 origin.push(parseFloat($scope.trackObj.latitude), parseFloat($scope.trackObj.longitude));
                                 destination.push(parseFloat($scope.trackObj.end_latitude), parseFloat($scope.trackObj.end_longitude));
                                 location.location = loc;
                                 waypoints.push(location);
                                 $scope.trackObj.origin = origin;
                                 $scope.trackObj.destination = destination;
                                 $scope.trackObj.waypoints = waypoints;
                                 $scope.customMarkerList = [{
                                     pos: origin,
                                     image: {
                                         url: 'images/ambulance_start.png',

                                     },
                                     state: 'ambulance',
                                     ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                     vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                     driver_name: $scope.InfoObj.driverDetails.driver_name,
                                     driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                                 }, {
                                     pos: parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ', ' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude),
                                     image: {
                                         url: 'images/emergency.png',

                                     },
                                     state: 'emergency',
                                     customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                     emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                     customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                                 }, {
                                     pos: destination,
                                     image: {
                                         url: 'images/hospital.png',

                                     },
                                     state: 'hospital',
                                     hospital_name: $scope.InfoObj.bookingDetails.hospital,

                                 }]
                             }

                         })

                      var promise = $interval(function() {
                          console.log($location.path())
                          if ($location.path() == '/Map' && $scope.booking_id != null) {
                              $scope.trackConfirmedBooking(JSON.stringify(bookingObj));
                          }
                          $interval.cancel(promise);
                      }, 1000 * 60)
                 }
             })
         }*/
        $scope.trackConfirmedBooking = function(bookingObj) {
            $scope.loader(true);
            $scope.trackObj = {};
            bookingObj = JSON.parse(bookingObj);
            console.log(angular.toJson(bookingObj))
            $scope.booking_id = bookingObj.booking_id;
            var req = {};
            var params = {};
            req.tableName = TableConstants.tracking;
            params.booking_id = $scope.booking_id;
            req.params = params;
            var orderByParams = {};
            orderByParams.created_date = '';
            req.orderByParams = orderByParams;
            console.log(angular.toJson(req))
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                console.log(response)
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.trackObj = response.Result[0];
                    console.log($scope.trackObj)
                    console.log($scope.trackObj.booking_status.toUpperCase())
                    $scope.moverObj = [];
                    var params = {};
                    params.pos = parseFloat(response.Result[0].latitude) + ', ' + parseFloat(response.Result[0].longitude);
                    params.id = response.Result[0].id;
                    params.bookingStatus = response.Result[0].booking_status;
                    params.image = 'images/ambulance.png';
                    $scope.moverObj.push(params);
                    var latlng = [];
                    latlng.push(parseFloat(response.Result[0].latitude), parseFloat(response.Result[0].longitude));
                    $scope.center = latlng;
                    request.service('getAmbulanceDriverStatusList', 'post', { ambulance_id: $scope.trackObj.ambulance_id, driver_id: $scope.trackObj.driver_id, booking_id: $scope.trackObj.booking_id }, $scope.CONFIG, function(infoResponse) {
                        console.log(infoResponse)
                        $scope.InfoObj = infoResponse.responseData[0];
                        console.log($scope.InfoObj)
                      //  if (($scope.trackObj.booking_status.toUpperCase() === 'BOOKING ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'WAY TO EMERGENCY LOCATION') && $scope.countObj1 == 0) {
                        if (($scope.trackObj.booking_status.toUpperCase() === statusConstants.s09 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.s08 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.s10 ) && $scope.countObj1 == 0) {
                            var origin = [];
                            var destination = [];
                            var waypoints = [];
                            $scope.countObj1++;
                            origin.push(parseFloat(response.Result[response.Result.length - 1].latitude), parseFloat(response.Result[response.Result.length - 1].longitude));
                            destination.push(parseFloat(response.Result[response.Result.length - 1].emergency_latitude), parseFloat(response.Result[response.Result.length - 1].emergency_longitude));
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;

                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                            }]
                            console.log($scope.customMarkerList)


                      //  } else if (($scope.trackObj.booking_status.toUpperCase() === 'AT EMERGENCY LOCATION' || $scope.trackObj.booking_status.toUpperCase() === 'WAY BACK TO HOSPITAL' || $scope.trackObj.booking_status.toUpperCase() === 'TRIP CLOSED') && $scope.countObj2 == 0) {
                        } else if (($scope.trackObj.booking_status.toUpperCase() === statusConstants.s11 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.s12 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.s14 ) && $scope.countObj2 == 0) {
                            $scope.countObj2++;
                            var origin = [];
                            var destination = [];
                            var waypoints = [];
                            var loc = {};
                            var location = {};
                            var loc = { 'lat': parseFloat(response.Result[response.Result.length - 1].emergency_latitude), 'lng': parseFloat(response.Result[response.Result.length - 1].emergency_longitude) };
                            origin.push(parseFloat(response.Result[response.Result.length - 1].latitude), parseFloat(response.Result[response.Result.length - 1].longitude));
                            destination.push(parseFloat($scope.trackObj.end_latitude), parseFloat($scope.trackObj.end_longitude));
                            location.location = loc;
                            waypoints.push(location);
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;

                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                            }, {
                                pos: parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ', ' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude),
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/hospital.png',

                                },
                                state: 'hospital',
                                hospital_name: $scope.InfoObj.bookingDetails.hospital,

                            }]
                            console.log($scope.customMarkerList)
                        } else if ($scope.countObj2 != 0 && $scope.countObj3 == 0) {
                            var origin = [];
                            var destination = [];
                            var waypoints = [];
                            var loc = {};
                            var location = {};
                            $scope.countObj3++;
                            var loc = { 'lat': parseFloat(response.Result[response.Result.length - 1].emergency_latitude), 'lng': parseFloat(response.Result[response.Result.length - 1].emergency_longitude) };
                            origin.push(parseFloat(response.Result[response.Result.length - 1].latitude), parseFloat(response.Result[response.Result.length - 1].longitude));
                            destination.push(parseFloat($scope.trackObj.end_latitude), parseFloat($scope.trackObj.end_longitude));
                            location.location = loc;
                            waypoints.push(location);
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;
                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                            }, {
                                pos: parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ', ' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude),
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/hospital.png',

                                },
                                state: 'hospital',
                                hospital_name: $scope.InfoObj.bookingDetails.hospital,

                            }]
                            console.log($scope.customMarkerList)
                        }

                    })

                    /*var promise = $interval(function() {
                        console.log($location.path())
                        if ($location.path() == '/Map' && $scope.booking_id != null) {
                            $scope.trackConfirmedBooking(JSON.stringify(bookingObj));
                        }
                        $interval.cancel(promise);
                    }, 1000 * 60)*/
                }
            })
        }

        $scope.trackHospital = function(hospitalObj) {
            /*hospitalObj = JSON.parse(hospitalObj);
            console.log(hospitalObj)
            $scope.mover = hospitalObj.pos.join();
            $scope.center = $scope.mover;
            $scope.mover_icon = 'images/hospital.png';
            console.log($scope.mover)*/
            hospitalObj = JSON.parse(hospitalObj)
            console.log(hospitalObj)
            $scope.hospitalObj = [];
            var params = {};
            var latlng = [];
            latlng.push(parseFloat(hospitalObj.pos[0]), parseFloat(hospitalObj.pos[1]));
            $scope.center = latlng;
            params.pos = latlng;
            params.id = hospitalObj.id;
            params.group_name = hospitalObj.group_name;
            params.groupTypeName = hospitalObj.groupTypeName;
            params.group_address = hospitalObj.group_address;
            params.image = {};
            /* params.image.size = [30, 30];
             params.image.origin = [0, 0];
             params.image.anchor = [0, 32];*/
            params.image.url = 'images/hospital.png'
            $scope.hospitalObj.push(params)

            /*var promise = $interval(function() {
                console.log($location.path())
                if ($location.path() == '/Map') {
                    $scope.trackHospital(JSON.stringify(hospitalObj));
                }
                $interval.cancel(promise);
            }, 1000 * 30)*/


            console.log($scope.hospitalObj)
        }

        $scope.mapTrack = function(booking_id) {
            $scope.booking_id = booking_id;
            window.localStorage.removeItem('booking_id');
            var req = {};
            var params = {};
            req.tableName = TableConstants.tracking;
            params.booking_id = $scope.booking_id;
            req.params = params;
            var orderByParams = {};
            orderByParams.created_date = '';
            req.orderByParams = orderByParams;
            console.log(angular.toJson(req))
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    $scope.trackObj = response.Result[0];
		   angular.forEach($scope.globalStatusList, function(obj1) {
                        if ($scope.trackObj.booking_status == obj1.short_code) {

                            $scope.trackObj.booking_status = obj1.status;

                        }
                    })

                    console.log($scope.trackObj)
                    console.log($scope.trackObj.booking_status.toUpperCase())
                        /*console.log($scope.trackObj.booking_status.toUpperCase() == 'WAY BACK TO HOSPITAL')
                        console.log($scope.countObj2)*/
                    $scope.moverObj = [];
                    var params = {};
                    params.pos = parseFloat(response.Result[0].latitude) + ', ' + parseFloat(response.Result[0].longitude);
                    params.image = 'images/ambulance.png';
                    params.id = response.Result[0].id;
                    params.bookingStatus = response.Result[0].booking_status;
                    //  $scope.moverObj.push(params);
                    var latlng = [];
                    latlng.push(parseFloat(response.Result[0].latitude), parseFloat(response.Result[0].longitude));
                    $scope.center = latlng;

                    request.service('getAmbulanceDriverStatusList', 'post', { ambulance_id: $scope.trackObj.ambulance_id, driver_id: $scope.trackObj.driver_id, booking_id: $scope.trackObj.booking_id }, $scope.CONFIG, function(infoResponse) {
                        $scope.InfoObj = infoResponse.responseData[0];
                      /*  if ($scope.trackObj.booking_status.toUpperCase() === 'STARTED' || $scope.trackObj.booking_status.toUpperCase() === 'BOOKING ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'WAY TO EMERGENCY LOCATION') {
                            var org = response.Result[0].latitude + ', ' + response.Result[0].longitude // using google.maps.LatLng class
                            var dest = response.Result[response.Result.length - 1].emergency_latitude + ', ' + response.Result[response.Result.length - 1].emergency_longitude; // using string

                            var directionsService = new google.maps.DirectionsService();
                            var request = {
                                origin: org, // LatLng|string
                                destination: dest, // LatLng|string
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            };
                            directionsService.route(request, function(response, status) {
                                if (status === 'OK') {
                                    var point = response.routes[0].legs[0];
                                    params.ETA = point;

                                    //$scope.moverObj.push(params);
                                }
                            });
                            params.flag = 'emergency';
                        } else {
                            var org = response.Result[0].latitude + ', ' + response.Result[0].longitude // using google.maps.LatLng class
                            var dest = $scope.trackObj.end_latitude + ', ' + $scope.trackObj.end_longitude; // using string

                            var directionsService = new google.maps.DirectionsService();
                            var request = {
                                origin: org, // LatLng|string
                                destination: dest, // LatLng|string
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            };
                            directionsService.route(request, function(response, status) {
                                if (status === 'OK') {
                                    var point = response.routes[0].legs[0];
                                    params.ETA = point;
                                    // $scope.moverObj.push(params);
                                }
                            });
                            params.flag = 'hospital';
                        }*/
                    //    if (($scope.trackObj.booking_status.toUpperCase() === 'STARTED' || $scope.trackObj.booking_status.toUpperCase() === 'BOOKING ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'WAY TO EMERGENCY LOCATION') && $scope.countObj1 == 0) {
                        if (($scope.trackObj.booking_status.toUpperCase() === statusConstants.sh15 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.sh09 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.sh08 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.sh10 ) && $scope.countObj1 == 0) {
		    
                            console.log("one")
                            var origin = [];
                            var destination = [];
                            var waypoints = [];
                            var loc = {};
                            var location = {};
                            $scope.countObj1++;
                            origin.push(parseFloat(response.Result[response.Result.length - 1].latitude), parseFloat(response.Result[response.Result.length - 1].longitude));
                            destination.push(parseFloat(response.Result[response.Result.length - 1].emergency_latitude), parseFloat(response.Result[response.Result.length - 1].emergency_longitude));
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;
                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number,


                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile,
                                patient_number: $scope.InfoObj.bookingDetails.patient_number
                            }]
                            console.log($scope.customMarkerList)
                            var org = response.Result[0].latitude + ', ' + response.Result[0].longitude // using google.maps.LatLng class
                            var dest = response.Result[response.Result.length - 1].emergency_latitude + ', ' + response.Result[response.Result.length - 1].emergency_longitude; // using string

                            var directionsService = new google.maps.DirectionsService();
                            var request = {
                                origin: org, // LatLng|string
                                destination: dest, // LatLng|string
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            };
                            directionsService.route(request, function(response, status) {
                                if (status === 'OK') {
                                    var point = response.routes[0].legs[0];
                                    params.ETA = point;

                                }
                            });
                            params.flag = 'emergency';
                 //       } else if (($scope.trackObj.booking_status.toUpperCase() === 'AT EMERGENCY LOCATION' || $scope.trackObj.booking_status.toUpperCase() === 'WAY BACK TO HOSPITAL' || $scope.trackObj.booking_status.toUpperCase() === 'REACHED HOSPITAL' || $scope.trackObj.booking_status.toUpperCase() === 'TRIP CLOSED') && $scope.countObj2 == 0) {
                        } else if (($scope.trackObj.booking_status.toUpperCase() === statusConstants.sh11 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.sh12 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.sh13 || $scope.trackObj.booking_status.toUpperCase() === statusConstants.sh14 ) && $scope.countObj2 == 0) {
		 
                            console.log("two")
                            var origin = [];
                            var destination = [];
                            var waypoints = [];
                            var loc = {};
                            var location = {};
                            $scope.countObj2++;
                            var loc = { 'lat': parseFloat(response.Result[response.Result.length - 1].emergency_latitude), 'lng': parseFloat(response.Result[response.Result.length - 1].emergency_longitude) };
                            origin.push(parseFloat(response.Result[response.Result.length - 1].latitude), parseFloat(response.Result[response.Result.length - 1].longitude));
                            destination.push(parseFloat($scope.trackObj.end_latitude), parseFloat($scope.trackObj.end_longitude));
                            location.location = loc;
                            waypoints.push(location);
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;
                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                            }, {
                                pos: parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ', ' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude),
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile,
                                patient_number: $scope.InfoObj.bookingDetails.patient_number
                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/hospital.png',

                                },
                                state: 'hospital',
                                hospital_name: $scope.InfoObj.bookingDetails.hospital,

                            }]
                            var org = response.Result[0].latitude + ', ' + response.Result[0].longitude // using google.maps.LatLng class
                            var dest = $scope.trackObj.end_latitude + ', ' + $scope.trackObj.end_longitude; // using string

                            var directionsService = new google.maps.DirectionsService();
                            var request = {
                                origin: org, // LatLng|string
                                destination: dest, // LatLng|string
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            };
                            directionsService.route(request, function(response, status) {
                                if (status === 'OK') {
                                    var point = response.routes[0].legs[0];
                                    params.ETA = point;
                                    // $scope.moverObj.push(params);
                                }
                            });
                            params.flag = 'hospital';
                            console.log($scope.customMarkerList)
                        } else if ($scope.countObj2 != 0 && $scope.countObj3 == 0) {
                            console.log("three")
                            var origin = [];
                            var destination = [];
                            var waypoints = [];
                            var loc = {};
                            var location = {};
                            $scope.countObj3++;
                            var loc = { 'lat': parseFloat(response.Result[response.Result.length - 1].emergency_latitude), 'lng': parseFloat(response.Result[response.Result.length - 1].emergency_longitude) };
                            origin.push(parseFloat(response.Result[response.Result.length - 1].latitude), parseFloat(response.Result[response.Result.length - 1].longitude));
                            destination.push(parseFloat($scope.trackObj.end_latitude), parseFloat($scope.trackObj.end_longitude));
                            location.location = loc;
                            waypoints.push(location);
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;
                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                            }, {
                                pos: parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ', ' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude),
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile,
                                patient_number: $scope.InfoObj.bookingDetails.patient_number
                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/hospital.png',

                                },
                                state: 'hospital',
                                hospital_name: $scope.InfoObj.bookingDetails.hospital,

                            }]
                            console.log($scope.customMarkerList)
                                var org = response.Result[0].latitude + ', ' + response.Result[0].longitude // using google.maps.LatLng class
                            var dest = $scope.trackObj.end_latitude + ', ' + $scope.trackObj.end_longitude; // using string

                            var directionsService = new google.maps.DirectionsService();
                            var request = {
                                origin: org, // LatLng|string
                                destination: dest, // LatLng|string
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            };
                            directionsService.route(request, function(response, status) {
                                if (status === 'OK') {
                                    var point = response.routes[0].legs[0];
                                    params.ETA = point;
                                    // $scope.moverObj.push(params);
                                }
                            });
                            params.flag = 'hospital';
                        }
                        $scope.moverObj.push(params);

                        /*if ($scope.trackObj.booking_status.toUpperCase() === 'STARTED' || $scope.trackObj.booking_status.toUpperCase() === 'BOOKING ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'ACCEPTED' || $scope.trackObj.booking_status.toUpperCase() === 'WAY TO EMERGENCY LOCATION') {
                            params.flag = 'emergency';
                            params.origin = parseFloat($scope.trackObj.latitude) + ', ' + parseFloat($scope.trackObj.longitude);
                            params.destination = parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ',' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude);
                            console.log("not REACHED to hospital")

                        } else if ($scope.trackObj.booking_status.toUpperCase() === 'AT EMERGENCY LOCATION' || $scope.trackObj.booking_status.toUpperCase() === 'WAY BACK TO HOSPITAL' || $scope.trackObj.booking_status.toUpperCase() === 'REACHED HOSPITAL' || $scope.trackObj.booking_status.toUpperCase() === 'TRIP CLOSED') {
                            console.log("two")
                            params.flag = 'hospital';
                            params.origin = parseFloat($scope.trackObj.latitude) + ',' + parseFloat($scope.trackObj.longitude);
                            params.destination = parseFloat($scope.trackObj.end_latitude) + ',' + parseFloat($scope.trackObj.end_longitude);
                            console.log("REACHED to hospital")
                        }
                        var etaReq = {};
                        etaReq.origin = params.origin;
                        etaReq.destination = params.destination;
                        console.log(angular.toJson(etaReq))
                        request.service('getDistanceNEta', 'post', etaReq, $scope.CONFIG, function(response) {
                            console.log(JSON.parse(response.responseData).rows[0].elements[0].duration.text)
                            params.eta = JSON.parse(response.responseData).rows[0].elements[0].duration.text;
                        })*/

                    })



                    var promise = $interval(function() {
                        console.log($location.path())
                        if ($location.path() == '/Map') {
                            $scope.mapTrack($scope.booking_id);
                        }
                        $interval.cancel(promise);
                    }, 1000 * 30)

                } else {
                    $scope.booking_id = null;
                }

            })


        }

        /*$scope.mapTrack = function(booking_id) {
            $scope.booking_id = booking_id;
            window.localStorage.removeItem('booking_id');
            var req = {};
            var params = {};
            req.tableName = TableConstants.tracking;
            params.booking_id = $scope.booking_id;
            req.params = params;
            var orderByParams = {};
            orderByParams.created_date = '';
            req.orderByParams = orderByParams;
            console.log(angular.toJson(req))
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                console.log(angular.toJson(response))
                if (response.statusCode == '300') {
                    $scope.trackObj = response.Result[0];
                    console.log(angular.toJson($scope.trackObj))
                    $scope.moverObj = [];
                    var params = {};
                    params.pos = parseFloat(response.Result[0].latitude) + ', ' + parseFloat(response.Result[0].longitude);
                    params.image = 'images/ambulance.png';
                    params.id = response.Result[0].id;
                    params.bookingStatus = response.Result[0].booking_status;
                    $scope.moverObj.push(params);

                    request.service('getAmbulanceDriverStatusList', 'post', { ambulance_id: $scope.trackObj.ambulance_id }, $scope.CONFIG, function(infoResponse) {
                        $scope.InfoObj = infoResponse.responseData[0];

                        var origin = [];
                        var destination = [];
                        var waypoints = [];
                        var loc = {};
                        var location = {};
                        if ($scope.trackObj.booking_status === 'Accepted' || $scope.trackObj.booking_status === 'Way to Emergency Location') {
                            console.log("2")
                            origin.push(parseFloat($scope.trackObj.latitude), parseFloat($scope.trackObj.longitude));
                            destination.push(parseFloat(response.Result[response.Result.length - 1].emergency_latitude), parseFloat(response.Result[response.Result.length - 1].emergency_longitude));
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;
                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                            }]

                        } else {
                            console.log("3")
                            var loc = { 'lat': parseFloat(response.Result[response.Result.length - 1].emergency_latitude), 'lng': parseFloat(response.Result[response.Result.length - 1].emergency_longitude) };
                            origin.push(parseFloat($scope.trackObj.latitude), parseFloat($scope.trackObj.longitude));
                            destination.push(parseFloat($scope.trackObj.end_latitude), parseFloat($scope.trackObj.end_longitude));
                            location.location = loc;
                            waypoints.push(location);
                            console.log(waypoints)
                            $scope.trackObj.origin = origin;
                            $scope.trackObj.destination = destination;
                            $scope.trackObj.waypoints = waypoints;
                            $scope.customMarkerList = [{
                                pos: origin,
                                image: {
                                    url: 'images/ambulance_start.png',

                                },
                                state: 'ambulance',
                                ambulance_no: $scope.InfoObj.ambulanceDetails.ambulance_number,
                                vehicle_type: $scope.InfoObj.ambulanceDetails.vehicleTypeName,
                                driver_name: $scope.InfoObj.driverDetails.driver_name,
                                driver_mobile: $scope.InfoObj.driverDetails.driver_mobile_number
                            }, {
                                pos: parseFloat(response.Result[response.Result.length - 1].emergency_latitude) + ', ' + parseFloat(response.Result[response.Result.length - 1].emergency_longitude),
                                image: {
                                    url: 'images/emergency.png',

                                },
                                state: 'emergency',
                                customer_name: $scope.InfoObj.bookingDetails.customer_name,
                                emergency_type: $scope.InfoObj.bookingDetails.emergency_type,
                                customer_mobile: $scope.InfoObj.bookingDetails.customer_mobile
                            }, {
                                pos: destination,
                                image: {
                                    url: 'images/hospital.png',

                                },
                                state: 'hospital',
                                hospital_name: $scope.InfoObj.bookingDetails.hospital,

                            }]
                        }
                    })

                    var promise = $interval(function() {
                        console.log($location.path())
                        if ($location.path() == '/Map') {
                            $scope.mapTrack($scope.booking_id);
                        }
                        $interval.cancel(promise);
                    }, 1000 * 10)
                } else {
                    $scope.booking_id = null;
                }
            })
        }*/

        $scope.refreshMover = function() {
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                /*$scope.loader(false);*/
                console.log(response)
                if (response.statusCode == '300') {
                    $scope.mover = parseFloat(response.Result[response.Result.length].latitude) + ', ' + parseFloat(response.Result[response.Result.length].longitude);
                    $scope.mover_icon = 'images/ambulance.png';
                }
            })
        }

        /*$scope.$on('$destroy', function() {
           $interval.cancel(promise);
        });*/


        if (window.localStorage.getItem('booking_id') != undefined) {
            $scope.mapTrack(window.localStorage.getItem('booking_id'));

        }

        $scope.showDetail = function(e, ambulanceObj) {
            $scope.ambulance = ambulanceObj;
            $scope.map.showInfoWindow('foo-iw', ambulanceObj.ambulance_number);
            //vm.map.showInfoWindow('foo-iw', shop.id);
        }

        $scope.showDetail1 = function(e, hospitalObj) {
            $scope.hospital = hospitalObj;
            $scope.map.showInfoWindow('foo-iwh', $scope.hospital.id);
        }

        $scope.showDetail2 = function(e, moverObj) {
            //$scope. = 1
            $scope.mover = moverObj;
            $scope.map.showInfoWindow('mover-info', $scope.mover.id);
        }

        $scope.showCustomInfo = function(e, customObj) {
            console.log(customObj)
            $scope.custom = customObj;
            $scope.map.showInfoWindow('custom-info', $scope.custom.state);
        }







        /*$rootScope.directions = [
          {origin:"Palo Alto", destination:"Gilroy", panelName:"p1"},
          {origin:"San Jose", destination:"Mountain View", panelName:"p2"}
        ];*/

        /*getLocation();


        function getLocation() {
        $scope.location = [];
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition,function(){
          console.log($scope.location)
                  
                });
            } else { 
                $scope.notification('Geolocation is not supported by this browser.','danger');
            }
        }
        function showPosition(position,cb) {
          $scope.location.push(position.coords.latitude)
          $scope.location.push(position.coords.longitude)
          if(cb){
            cb()
          }
            
        }*/


        /* $scope.data = [
             { foo: 1, bar: 1 },
             { foo: 2, bar: 2 },
             { foo: 3, bar: 3 },
             { foo: 4, bar: 4 },
             { foo: 5, bar: 5 },
             { foo: 6, bar: 6 },
             { foo: 7, bar: 7 }
         ];*/
        /*   $scope.ambulanceList = [{
               pos: [40.71, -74.21],
               image: {
                   url: 'images/red.png',
                   size: [30, 30],
                   origin: [0, 0],
                   anchor: [0, 32]
               }
           }, {
               pos: [40.72, -74.20],
               image: {
                   url: 'images/green.png',
                   size: [30, 30],
                   origin: [0, 0],
                   anchor: [0, 32]
               }
           }, {
               pos: [40.73, -74.19],
               image: {
                   url: 'images/gray.png',
                   size: [30, 30],
                   origin: [0, 0],
                   anchor: [0, 32]
               }
           }, {
               pos: [40.74, -74.18],
               image: {
                   url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                   size: [20, 32],
                   origin: [0, 0],
                   anchor: [0, 32]
               }
           }, {
               pos: [40.75, -74.17],
               image: {
                   url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                   size: [20, 32],
                   origin: [0, 0],
                   anchor: [0, 32]
               }
           }, {
               pos: [40.76, -74.16],
               image: {
                   url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                   size: [20, 32],
                   origin: [0, 0],
                   anchor: [0, 32]
               }
           }, {
               pos: [40.77, -74.15],
               image: {
                   url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                   size: [20, 32],
                   origin: [0, 0],
                   anchor: [0, 32]
               }
           }];*/


    }
    app.controller('mapCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$interval', '$filter', 'TableConstants', '$timeout', 'NgMap', '$location', '$http','statusConstants', mapCtrl]);
}());
