(function() {
    var app = angular.module('eSahai');
    var comeToKnowByCtrl = function($scope, $rootScope, request, ctrlComm, TableConstants, $timeout, $filter) {
        $scope.tabActive('comeToKnowBy', 'appSettings');
        console.log($scope.admin)

        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.customer = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');
        $scope.customerAttr = [];
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {};
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};

        $scope.getComeToKnowByList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.come_to_know_by;
            req.params = {};

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    angular.forEach(response.Result, function(obj) {
                        if (obj.status == 'Yes' || obj.status == 'Active') {
                            obj.status = 'Active';
                        } else {
                            obj.status = 'InActive';
                        }
                    })
                    $scope.comeToKnowByList = response.Result;
                    $scope.totalItems = $scope.comeToKnowByList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }
                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }

        if (window.location.hash == "#/Come-To-Know-By") {
            $scope.getComeToKnowByList();

        } else if (window.location.hash == "#/Add-Come-To-Know-By") {
            $scope.page.title = 'Add Come To Know By';
            $scope.page.type = 'post';
        } else if (window.location.hash == "#/Update-Come-To-Know-By") {
            $scope.page.title = 'Update Come To Know By';
            $scope.page.type = 'put';
            prePoppulateValues(ctrlComm.get('comeToKnowBy'));
        } else if (window.location.hash == "#/Come-To-Know-By") {
            $scope.page.title = 'View  Come To Know By';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('comeToKnowBy'));
        }



        var addNewComeToKnowBy = function() {
            window.location.hash = "#/Add-Come-To-Know-By";
        }

        var viewComeToKnowBy = function(com) {
            ctrlComm.put('comeToKnowBy', com);
            window.location.hash = '#/Come-To-Know-By';
        }

        function prePoppulateValues(com) {
            console.log("comeToKnowBy" + angular.toJson(com))
            if (com) {
                if ($scope.page.type == 'put') {
                    com.status == 'Active' ? com.status = true : com.status = false;
                    /*if (document.getElementById('imageName') != undefined) {
                        document.getElementById("imageName").src = customer.file_url;
                    }*/
                }
                $scope.com = com;
            } else {
                window.location.hash = "#/Come-To-Know-By";
            }
        }

        var saveComeToKnowBy = function(com) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.come_to_know_by;
            var params = {};
            if (com.status == true) {
                com.status = 'Active';
            } else {
                com.status = 'InActive';
            }

            params = angular.copy(com);;

            req.params = params;
            console.log(angular.toJson(req));
            request.service('insertRecord', 'post', req, $scope.CONFIG, function(data) {
                $scope.loader(false);

                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    window.location.hash = '#/Come-To-Know-By';
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }




        var updateComeToKnowBy = function(com) {
            console.log(com)
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.come_to_know_by;
            var params = {};
            if (com.status == true) {
                com.status = 'Active';
            } else {
                com.status = 'InActive';
            }
            params = angular.copy(com);
            delete params['id'];
            var conditionalParams = {};
            conditionalParams.id = com.id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(data) {
                $scope.loader(false);
                if (data.statusCode == '300') {
                    $scope.notification(data.statusMessage);
                    window.location.hash = '#/Come-To-Know-By';
                } else if (data.statusCode == '204') {
                    $scope.notification(data.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                }
            })
        }



        var editComeToKnowBy = function(com) {
            ctrlComm.put('comeToKnowBy', com);
            window.location.hash = "#/Update-Come-To-Know-By";
        }


        var cancelComeToKnowBy = function() {
            window.location.hash = "#/Come-To-Know-By";
        }

        var deleteComeToKnowBy = function(com) {
            var input = {
                text: 'Are you sure you want to delete',
                name: com.know_by
            }
            $scope.confirmation(input, function() {
                confirmDelete(com);
            });
        }

        function confirmDelete(com) {

            var req = {};
            req.tableName = TableConstants.come_to_know_by;
            var params = {};
            params.status = 'InActive'
            var conditionalParams = {};
            conditionalParams.id = com.id;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                if (response.statusCode == '300') {
                    $scope.getComeToKnowByList(function() {
                        $scope.notification("Deleted Successfully");
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)
                } else {
                    $scope.notification(response.statusMessage);
                }
            })


        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewComeToKnowBy = addNewComeToKnowBy;
        $scope.saveComeToKnowBy = saveComeToKnowBy;
        $scope.updateComeToKnowBy = updateComeToKnowBy;
        $scope.deleteComeToKnowBy = deleteComeToKnowBy;
        $scope.cancel = cancelComeToKnowBy;
        $scope.editComeToKnowBy = editComeToKnowBy;
        $scope.viewComeToKnowBy = viewComeToKnowBy;

    }
    app.controller('comeToKnowByCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', 'TableConstants', '$timeout', '$filter', comeToKnowByCtrl]);
}());
