(function() {
    var app = angular.module('eSahai');
    var commandCenterCtrl = function($scope, request, TableConstants, $timeout) {
        console.log("in commandCenterCtrl : " + TableConstants.app_settings);
        $scope.tabActive('ccs', 'appSettings');
        $scope.err = {};

        var getCCSettings = function() {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.app_settings;
            req.params = {};
            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                console.log(angular.toJson(response))
                if (response.statusCode == '300') {
                    $scope.cc = response.Result[0];
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                } else {
                    $scope.usersList = [];
                }
            })
        }

        if (window.location.hash == "#/Command-Center") {
            getCCSettings();
        }

        var updateCCS = function(ccObj) {
            validate(function() {
                $scope.loader(true);
                var req = {};
                req.tableName = TableConstants.app_settings;
                var params = {};
                params = angular.copy(ccObj);
                var conditionalParams = {};
                conditionalParams.id = ccObj.id;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req));
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    $scope.loader(false);
                    if (response.statusCode == '300') {
                        $scope.notification(response.statusMessage);
                        getCCSettings();
                    } else {
                        $scope.notification(response.statusMessage, 'danger');
                    }
                })

            })
        }

        var validate = function(cb) {

            if (!$scope.cc.value) {
                $scope.err.value = true;
            } else {
           // Limit increased to 15 and should accept '+'            
                if (!(/^[+][0-9]{1,15}$/).test($scope.cc.value)) {
                    $scope.err.value_cnd = true;
                } else {
                    delete $scope.err.value;
                    delete $scope.err.value_cnd;
                }
            }
            console.log($scope.err)
            if (Object.keys($scope.err).length == 0) {
                if (cb) cb();
            }
        }

        var cancel = function() {
            window.location.hash = '#/Home';
        }

        $scope.updateCCS = updateCCS;
        $scope.cancel = cancel;
    }


    app.controller('commandCenterCtrl', ['$scope', 'request', 'TableConstants', '$timeout', commandCenterCtrl]);
}());
