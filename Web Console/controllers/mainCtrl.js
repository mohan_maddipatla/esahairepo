	(function() {
	    var app = angular.module('eSahai');
	    var mainCtrl = function($scope, $rootScope, request, ctrlComm, $modal, TableConstants, $http, $filter, $timeout, $log) {

	        $scope.appUsersList = [];
	        $scope.admin = {};
	        $scope.appMasterList = false;

	        $scope.tabActive = function(tab, parentTab) {
	            $scope.activeTab = {};
	            $scope.activeTab[tab] = true;
	            $scope.activeTab.tab = parentTab;
	            if (!$scope.loginStatus) window.location.hash = "/login"
	        }

	        $scope.notification = function(text, type, title, delay) {
	            if (!type) type = 'info';
	            if (!title) title = 'Notification';
	            if (!delay) delay = 3000;
	            $.bootstrapGrowl('<h4> ' + title + ' </h4> <p> ' + text + ' </p>', {
	                type: type,
	                delay: delay,
	                allow_dismiss: true,
	                width: 'auto'
	            });
	        }


	        $scope.getCountryList = function(cb) {
	            var req = {};
	            request.service('getCountryList', 'get', req, $scope.CONFIG, function(response) {
	                if (response.statusCode == '300') {
	                    $scope.globalCountryList = response.responseData;
	                    if (cb) cb();
	                }


	            })

	        }





	        $scope.loader = function(status) {
	            if (status) {
	                $scope.NProgress = true;
	                NProgress.start();
	            } else {
	                $scope.NProgress = false;
	                NProgress.done();
	                //document.getElementById('loader').style.display = none;
	            }
	        }
	        $scope.appNamesById = {};
	        var getAppMasters = function(cb) {
	            if ($scope.admin.userAccessId) {
	                var input = { userAccessId: $scope.admin.userAccessId };
	                $scope.loader(true);
	                var req = {};
	                req.tableName = TableConstants.appMaster;
	                req.params = {}
	                request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                    console.log(response)
	                    if (response.statusCode == '300') {
	                        $scope.appMasterList = response.Result;
	                        $scope.totalItems = $scope.appMasterList.length;
	                        if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
	                            $scope.tableRows.push($scope.totalItems);

	                        if ($scope.totalItems != 0) {
	                            $scope.tableRows = $scope.tableRows.sort(request.sort);
	                            $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
	                            if ($scope.tableRows[1]) {
	                                $scope.viewby = $scope.tableRows[1];
	                            } else {
	                                $scope.viewby = $scope.tableRows[0];
	                            }
	                            $scope.setItemsPerPage($scope.viewby);
	                        }
	                        if (cb) cb();
	                    } else {

	                    }
	                });
	            }
	        }

	        /*  $scope.getServices = function(cb) {
	              var req = {};
	              req.tableName = TableConstants.serviceMaster;
	              req.params = {};


	              request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                  if (response.statusCode == '300') {
	                      $scope.servicesList = response.Result;
	                      if (cb) cb();
	                  }
	              });
	          }*/




	        $scope.getAppsList = function(cb) {
	            var req = {};
	            req.tableName = 'Apps'
	            req.params = {};
	            req.adminURL = true; //adminURL key specifies to use setup1(port : 3000) in settings.js file
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                if (response.statusCode == '300') {
	                    console.log(response)
	                    $scope.appsList = response.Result;
	                    if (cb) cb();
	                }


	            })

	        }

	        $scope.getStatusList = function(cb) {
	            var req = {};
	            request.service('getStatusList', 'get', req, $scope.CONFIG, function(response) {
	                if (response.statusCode == '300') {
	                    $scope.globalStatusList = response.responseData;
	                    if (cb) cb();
	                }


	            })

	        }



	        function checkAppSelection() {
	            if (!$scope.admin.appId) {
	                $scope.selApp = $scope.appMasterList[0];
	                $scope.admin.appId = $scope.selApp.app_id;
	                $scope.admin.appName = $scope.selApp.app_name;
	                request.setObj('selectedApp', $scope.selApp);
	            }
	        }

	        $scope.loadDashboard = function() {
	            request.service('getDashboardDetails', 'post', $scope.admin, $scope.CONFIG, function(dResponse) {
	                if (dResponse.statusCode == '300') {
	                    $scope.dObj = dResponse.Result[0];
	                }
	            })
	        }



	        $scope.initServices = function() {
	            //  $scope.getRolesList();
	            //$scope.getCircleMasterList();
	            // $scope.getAdminsList();
	            // $scope.getMwUsersList();
	            /*$scope.getAppList();
	            $scope.getServiceTypeList();
	            $scope.getServicesList();
	            $scope.getComponentsList();*/



	            $scope.getUserTypeList();
	            $scope.getGroupTypeList();
	            $scope.getEmergencyTypeList();
	            $scope.getVehicleTypeList();
	            $scope.getGlobalGroupMasterList();
	            $scope.getGlobalDriverMasterList();
	            $scope.getGlobalAmbulanceList();
	            $scope.getCustomerAttributesList();
	            $scope.getAmbulanceAttributesList();
	            $scope.getGroupAttributesList();
	            $scope.getStatusList();
	            $scope.getCountryList();
	            //   $scope.getMySchemePerformance();
	            // $scope.getBannersList();
	            // $scope.getBenefitsList();
	            //   $scope.getSchemeList();
	            // $scope.getErrorCodesList();
	            //     $scope.getMNCList();
	            // $scope.getFRCList();
	        }

	        $scope.getServiceTypeList = function(cb) {
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.serviceTypeTable;
	            req.params = {};
	            $scope.mwUsersList = [];
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(data) {
	                $scope.loader(false);
	                if (data.statusCode == 300) {
	                    var serviceTypeList = [];
	                    angular.forEach(data.Result, function(obj) {
	                        if (obj.isActive == 'Yes' && obj.isDefault != 'true') {
	                            serviceTypeList.push(obj);
	                        }
	                    })
	                    $scope.serviceTypeList = serviceTypeList;

	                } else if (data.statusCode == '204') {
	                    $scope.notification(data.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)
	                    if (cb) {
	                        cb();
	                    }
	                } else {
	                    $scope.notification(data.statusMessage);
	                    $scope.totalItems = 0;
	                }
	            });
	        }


	        $scope.getCircleMasterList = function(cb) {
	            var req1 = {};
	            req1.tableName = 'circle_master';
	            var params = {};
	            req1.params = params;
	            console.log(angular.toJson(req1))
	            request.service('fetchDataByField', 'post', req1, $scope.CONFIG, function(response) {
	                if (response.statusCode == '300') {
	                    console.log(response)
	                    $scope.circleMasterList = response.Result;
	                    if (request.getItem('circleMasterList') != undefined) {
	                        request.removeItem('circleMasterList')
	                    }
	                    request.setItem('circleMasterList', JSON.stringify($scope.circleMasterList));

	                    if (cb) cb();
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)

	                }
	            });
	        }


	        $scope.getAppList = function(cb) {
	            var req = {};
	            req.tableName = TableConstants.appMaster;
	            req.params = {};
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                console.log(response)
	                if (response.statusCode == '300') {
	                    $scope.appMasterList = response.Result;
	                    if (cb) cb();
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)
	                }
	            });
	        }


	        $scope.getServicesList = function(cb) {
	            var req = {};
	            req.tableName = TableConstants.serviceMaster;
	            req.params = {};
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                if (response.statusCode == '300') {
	                    $scope.servicesList = response.Result;


	                    if (cb) cb();
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)

	                }
	            });
	        }

	        $scope.getComponentsList = function(cb) {
	            $scope.tablesList = [];
	            var list = [];
	            var req = {};
	            req.tableName = 'appTables';
	            var params = {};
	            params.isSystemGenerated = '1';
	            req.params = params;
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                console.log(response)
	                if (response.statusCode == '300') {
	                    $scope.tablesList = response.Result;
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)

	                }



	            })
	        }

	        $scope.getUserTypeList = function(cb) {
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.user_types;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                $scope.loader(false);
	                if (response.statusCode == '300') {
	                    $scope.userTypeList = response.Result;
	                    $scope.userTypeList.sort(function(a, b) {
	                        return (a.type_name.toUpperCase() > b.type_name.toUpperCase()) ? 1 : ((b.type_name.toUpperCase() > a.type_name.toUpperCase()) ? -1 : 0);
	                    });
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)
	                }
	            })
	        }

	        $scope.getGroupTypeList = function(cb) {
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.group_types;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                $scope.loader(false);
	                if (response.statusCode == '300') {
	                    $scope.groupTypeList = response.Result;
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)
	                }
	            })
	        }

	        $scope.getEmergencyTypeList = function(cb) {
	            /*$scope.loader(true);*/
	            var req = {};
	            req.tableName = TableConstants.emergency_types;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                /*$scope.loader(false);*/
	                if (response.statusCode == '300') {
		       // $scope.emergencyTypeList = response.Result;
	                    $scope.emergencyTypeList=[];
	                    angular.forEach(response.Result, function(obj) {
	                        if (obj.status == 'Active') {
	                            $scope.emergencyTypeList.push(obj);
	                        }
	                    })
	                    $scope.emergencyTypeList.sort(function(a, b) {
	                        return (a.type_name.toUpperCase() > b.type_name.toUpperCase()) ? 1 : ((b.type_name.toUpperCase() > a.type_name.toUpperCase()) ? -1 : 0);
	                    });
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)
	                }
	            })
	        }

	        $scope.getVehicleTypeList = function(cb) {
	            /*$scope.loader(true);*/
	            var req = {};
	            req.tableName = TableConstants.vehicle_type;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                /*$scope.loader(false);*/
	                if (response.statusCode == '300') {
	                    $scope.vehicleTypeList = response.Result;
	                    $scope.vehicleTypeList.sort(function(a, b) {
	                        return (a.type_name.toUpperCase() > b.type_name.toUpperCase()) ? 1 : ((b.type_name.toUpperCase() > a.type_name.toUpperCase()) ? -1 : 0);
	                    });
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();

	                    }, 2000)
	                }
	            })
	        }

	        $scope.getGlobalGroupMasterList = function(cb) {
	            /*$scope.loader(true);*/
	            var req = {};
	            req.tableName = TableConstants.group_master;
	            req.params = {};
	            if ($scope.admin.userType === "GROUP ADMIN") {
	                req.params.group_id = $scope.admin.groupId;
	            }
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                /*$scope.loader(false);*/
	                if (response.statusCode == '300') {
	                    angular.forEach(response.Result, function(obj) {
	                        angular.forEach($scope.groupTypeList, function(obj1) {
	                            if (obj1.id == obj.group_type) {
	                                obj.groupTypeName = obj1.type_name;
	                            }
	                        })
	                    })
	                    $scope.globalGroupMasterList = response.Result;
	                    $scope.globalGroupMasterList.sort(function(a, b) {
	                        return (a.group_name.toUpperCase() > b.group_name.toUpperCase()) ? 1 : ((b.group_name.toUpperCase() > a.group_name.toUpperCase()) ? -1 : 0);
	                    });
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();
	                    }, 2000)

	                }
	            })
	        }
	        $scope.getGlobalDriverMasterList = function(cb) {
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.driver_master;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                $scope.loader(false);
	                if (response.statusCode == '300') {
	                    $scope.globalDriverMasterList = response.Result;
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();
	                    }, 2000)

	                }
	            })
	        }

	        $scope.getGlobalAmbulanceList = function(cb) {
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.ambulance_master;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                $scope.loader(false);
	                if (response.statusCode == '300') {
	                    $scope.globalAmbulanceList = response.Result;
	                    console.log($scope.globalAmbulanceList)
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();
	                    }, 2000)

	                }
	            })
	        }

	        $scope.getCustomerAttributesList = function(cb) {
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.customer_e_types;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                $scope.loader(false);
	                if (response.statusCode == '300') {
	                    $scope.customerAttributesList = response.Result;
	                    console.log($scope.customerAttributesList)
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();
	                    }, 2000)

	                }
	            })
	        }

	        $scope.getAmbulanceAttributesList = function(cb) {
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.ambulance_e_types;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                $scope.loader(false);
	                if (response.statusCode == '300') {
	                    $scope.ambulanceAttributesList = response.Result;
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();
	                    }, 2000)

	                }
	            })
	        }

	        $scope.getGroupAttributesList = function(cb) {
	            console.log(cb)
	            $scope.loader(true);
	            var req = {};
	            req.tableName = TableConstants.group_e_types;
	            req.params = {};
	            console.log(angular.toJson(req));
	            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
	                $scope.loader(false);
	                if (response.statusCode == '300') {
	                    $scope.groupAttributesList = response.Result;
	                    if (cb) {
	                        cb();
	                    }
	                } else if (response.statusCode == '204') {
	                    $scope.notification(response.statusMessage, "danger");
	                    $timeout(function() {
	                        $scope.logout();
	                    }, 2000)

	                }
	            })
	        }



	        $scope.logout = function(status) {
	            if (status) {
	                var input = { text: 'Are you sure you want to', name: 'logout' };
	                $scope.confirmation(input, function() {
	                    request.removeItem('statusCode');
	                    request.removeItem('appUserId');
	                    request.removeItem('appEmail');
	                    request.removeItem('appToken');
	                    request.removeItem('userType');
	                    location.reload();
	                });
	            } else {
	                request.removeItem('statusCode');
	                request.removeItem('appUserId');
	                request.removeItem('email');
	                request.removeItem('token');
	                request.removeItem('userType');
	                location.reload();
	            }
	        }

	        var userAppChanged = function(app) {
	            $scope.admin.appId = app.app_id;
	            $scope.admin.appName = app.app_name;
	            $scope.$broadcast('userAppChanged', app.appId);
	            request.setObj('selectedApp', app);
	        }

	        /*var confirmation = function(input, cb) {
	            if (cb) cb(status);
	            var modalInstance = $modal.open({
	                animation: true,
	                templateUrl: 'templates/confirmBox.html',
	                controller: 'confirmBoxCtrl',
	                size: 'md',
	                resolve: {
	                    input: function() {
	                        return { text: input.text, name: input.name };
	                    }
	                }
	            });
	            modalInstance.result.then(function(status) {
	                if (cb) cb(status);
	            }, function() {
	                console.log("Modal dismissed at: " + new Date());
	            });
	        }*/

	        var confirmation = function(input, cb) {
	            //if (cb) cb(status);
	            var modalInstance = $modal.open({
	                animation: true,
	                templateUrl: 'templates/confirmBox.html',
	                controller: 'confirmBoxCtrl',
	                size: 'md',
	                resolve: {
	                    input: function() {
	                        return { text: input.text, name: input.name };
	                    }
	                }
	            });
	            modalInstance.result.then(function(status) {
	                if (cb) cb(status);
	            }, function() {
	                if (input['type'] == 'booking') {
	                    window.location.hash = '#/Booking';
	                }
	                $log.info('Modal dismissed at: ' + new Date());
	            });
	        }

	        /*  var confirmation = function(input) {
	        	          

	        	            var modalInstance = $uibModal.open({
	        	                animation: $scope.animationsEnabled,
	        	                templateUrl: 'templates/confirmBox.html',
	        	                controller: 'appCtrl',
	        	                size: input,
	        	                resolve: {
	        	                    input: function() {
	        	                    	console.log("in input fn");;
	        	                    	console.log("input text ::",input.text);
	        	                    	console.log("input name ::",input.name);
	        	                        return { text: input.text, name: input.name };
	        	                    }
	        	                }

	        	            });

	        	            modalInstance.result.then(function(status) {
	        	                //if (cb) cb(status);
	        	                $scope.status=status;
	        	            }, function() {
	        	                $log.info('Modal dismissed at: ' + new Date());
	        	            });


	        	        }*/

	        $scope.clearSection = function() {
	            if (ctrlComm.get('pageNo') != undefined) {
	                ctrlComm.del('pageNo');
	            }
	        }

	        $scope.loginToggle = function() {
	            if (request.getItem('statusCode') && request.getItem('appUserId') && request.getItem('appEmail') && request.getItem('appToken')) {
	                $scope.loginStatus = true;
	                $scope.admin.statusCode = request.getItem('statusCode');
	                if ($scope.admin.statusCode == '302') {
	                    $scope.admin.statusMessage = request.getItem('statusMessage');
	                }
	                $scope.admin.userId = request.getItem('appUserId');
	                $scope.admin.email = request.getItem('appEmail');
	                $scope.admin.token = request.getItem('appToken');
	                $scope.admin.userType = request.getItem('userType');
	                $scope.admin.displayName = request.getItem('displayName');
	                if (request.getItem('userType') != 'ADMIN') {
	                    $scope.admin.groupId = request.getItem('groupId');
	                }

	                console.log(JSON.stringify($scope.admin));
	                $scope.CONFIG = {
	                    'headers': { 'userId': $scope.admin.userId, 'token': $scope.admin.token }
	                }


	                if (request.getItem('userType') == 'user' && request.getItem('statusCode') != '302') { // usertype == user
	                    $scope.usersAppList = JSON.parse(request.getItem('appsList'));
	                    angular.forEach($scope.usersAppList, function(obj) {
	                        if (obj.app_name == 'api_docs') {
	                            obj.href = request.setup['protocol'] + '://' + request.setup['host'] + ':' + request.setup['port'] + '/Retailer/#/';
	                        } else {
	                            obj.href = '#'
	                        }
	                    })
	                }
	                if ($scope.admin.statusCode == '302') {
	                    $scope.notification($scope.admin.statusMessage, 'danger');
	                }
	                $scope.loadDashboard();
	                //$scope.getCirclesList();
	                $scope.initServices();


	            } else {
	                $scope.loginStatus = false;
	                window.location.hash = "/login"
	            }
	        }
	        $scope.loginToggle();

	        $scope.userAppChanged = userAppChanged;
	        $scope.getAppMasters = getAppMasters;
	        $scope.confirmation = confirmation;

	        $scope.gotoHome = function() { window.location.hash = "/Home" }
	        $scope.gotoHotspot = function() { window.location.hash = "/Hotspot" }
	        $scope.gotoAdvertisement = function() { window.location.hash = "/Advertisement" }
	        $scope.gotoMessageLog = function() { window.location.hash = "/MessageLog" }
	        $scope.gotoErrorLog = function() { window.location.hash = "/ErrorLog" }
	        $scope.gotoBuildDeskTransactionLog = function() { window.location.hash = "/BuildDeskTransactionLog" }
	        $scope.gotoLoadMoneyTransactionLog = function() { window.location.hash = "/LoadMoneyTransactionLog" }
	        $scope.gotoSettings = function() { window.location.hash = "/Settings" }
	        $scope.gotoMessageTemplate = function() { window.location.hash = "/Message-Template" }
	        $scope.gotoMrupeeConstants = function() { window.location.hash = "/Mrupee-Constants" }
	        $scope.gotoMrupeeDetails = function() { window.location.hash = "/Mrupee-Details" }
	        $scope.gotoAPIMaster = function() { window.location.hash = "/API-Master" }
	        $scope.gotoEmailMaster = function() { window.location.hash = "/Email-Master" }
	        $scope.gotoCountryList = function() { window.location.hash = "/Country-List" }
	        $scope.gotoMCCMNC = function() { window.location.hash = "/MCC-MNC" }
	        $scope.gotoErrorCode = function() { window.location.hash = "/Error-Code" }
	        $scope.gotoErrorCodes = function() { window.location.hash = "/Error-Codes" }

	        $scope.gotoTermsAndConditions = function() { window.location.hash = "/Terms-And-Conditions" }
	        $scope.gotoSSID = function() { window.location.hash = "/SSID" }
	        $scope.gotoActivityLog = function() { window.location.hash = '/Activity-Log' }
	        $scope.gotoAPP = function() {
	            $scope.clearSection();
	            window.location.hash = "/APP"
	        }
	        $scope.gotoMWUsers = function() {
	            $scope.clearSection();
	            window.location.hash = "/MW-Users"
	        }
	        $scope.gotoMWUserPermission = function() { window.location.hash = "/MW-UserPermission" }
	        $scope.gotoAppTable = function() { window.location.hash = "/App-Table" }
	        $scope.gotoServiceMaster = function() { window.location.hash = '/Service-Master' }
	        $scope.gotoMasterTable = function() { window.location.hash = '/Table-Master' }
	        $scope.gotoData = function() { window.location.hash = '/Data' }
	        $scope.gotoAPIDoc = function() { window.location.hash = '/API-Doc' }
	        $scope.gotoAPPDevice = function() {
	            $scope.clearSection();
	            window.location.hash = '/APP-Device'
	        }
	        $scope.gotoAPPErrorLog = function() {
	            $scope.clearSection();
	            window.location.hash = '/App-Error-Log'
	        }
	        $scope.gotoRoles = function() {
	            $scope.clearSection();
	            window.location.hash = '/Roles'
	        }
	        $scope.gotoSchema = function() {
	            $scope.clearSection();
	            window.location.hash = '/Schema'
	        }
	        $scope.gotoAdmin = function() {
	            $scope.clearSection();
	            window.location.hash = '/Admins'
	        }
	        $scope.gotoServiceType = function() {
	            $scope.clearSection();
	            window.location.hash = '/Service-Type'
	        }
	        $scope.gotoAPILog = function() { window.location.hash = '/API-Log'; }
	        $scope.gotoBanner = function() { window.location.hash = '/Banners'; }
	        $scope.gotoCircleMaster = function() { window.location.hash = '/Circle-Master'; }
	        $scope.gotoBenefits = function() { window.location.hash = '/My-Benefits'; }
	        $scope.gotoMyComplaints = function() { window.location.hash = '/My-Complaints'; }
	        $scope.gotoScheme = function() { window.location.hash = '/My-Scheme'; }
	        $scope.gotoMyIncome = function() { window.location.hash = '/My-Income'; }
	        $scope.gotoMySchemePerformance = function() { window.location.hash = '/My-Scheme-Performance'; }
	        $scope.gotoActivationResponse = function() {
	            window.location.hash = '/Activation-Response';

	        }
	        $scope.gotoEVDRecharge = function() {
	            window.location.hash = '/EVD-Recharge';

	        }
	        $scope.gotoBlockNumber = function() {
	            window.location.hash = '/Block-Number';

	        }
	        $scope.gotoRechargeReversal = function() {
	            window.location.hash = '/Recharge-Reversal';

	        }
	        $scope.gotoMNC = function() {
	            window.location.hash = '/MCC-MNC'
	        }
	        $scope.gotoFRC = function() {
	            window.location.hash = '/FRC'
	        }
	        $scope.gotoAppVersion = function() {
	            window.location.hash = '/APP-VERSION'
	        }
	        $scope.gotoAppSettings = function() {
	            window.location.hash = '/App-Settings'
	        }
	        $scope.gotoMenuSettings = function() {
	            window.location.hash = '/Menu-Settings'
	        }
	        $scope.gotoComplaintLog = function() {
	            window.location.hash = '/Complaint-Log'
	        }
	        $scope.gotoPNLog = function() {
	            window.location.hash = '/PN-Log'
	        }



	        $scope.gotoUserType = function() {
	            window.location.hash = '/User-Type';
	        }
	        $scope.gotoUsers = function() {
	            window.location.hash = "/Users"
	        }
	        $scope.gotoGroupType = function() {
	            window.location.hash = '/Group-Type';
	        }
	        $scope.gotoEmergencyType = function() {
	            window.location.hash = '/Emergency-Type';
	        }
	        $scope.gotoVehicleType = function() {
	            window.location.hash = '/Vehicle-Type';
	        }
	        $scope.gotoGroupMaster = function() {
	            window.location.hash = '/Group-Master';
	        }
	        $scope.gotoAmbulanceMaster = function() {
	            window.location.hash = '/Ambulance-Master';
	        }
	        $scope.gotoDriverMaster = function() {
	            window.location.hash = '/Driver-Master';
	        }
	        $scope.gotoCustomerMaster = function() {
	            window.location.hash = '/Customer-Master';
	        }
	        $scope.gotoMap = function() {
	            window.location.hash = '/Map';
	        }
	        $scope.gotoBooking = function() {
	            if (request.getItem('booking_filter') != undefined) {
	                request.removeItem('booking_filter');
	            }
	            window.location.hash = '/Booking';
	        }
	        $scope.gotoAmbulance = function() {
	            window.location.hash = '/Ambulances';
	        }
	        $scope.gotoCustomerAttributes = function() {
	            window.location.hash = '/Customer-Attributes';
	        }
	        $scope.gotoAmbulanceAttributes = function() {
	            window.location.hash = '/Ambulance-Attributes';
	        }
	        $scope.gotoGroupAttributes = function() {
	            window.location.hash = '/Group-Attributes';
	        }
	        $scope.goToSMS = function() {
	            window.location.hash = '/SMS-Settings';
	        }
	        $scope.gotoRateCard = function() {
	            window.location.hash = '/Rate-Card';
	        }
	        $scope.gotoAppVersion = function() {
	            window.location.hash = '/APP-Version';
	        }
	        $scope.gotoTermsAndConditions = function() {
	            window.location.hash = "/Terms-And-Conditions";
	        }
	        $scope.goToCCS = function() {
	            window.location.hash = "/Command-Center";
	        }
	        $scope.goToPNLogs = function() {
	            window.location.hash = "/PN-Logs";
	        }
	        $scope.goToSMSLogs = function() {
	                window.location.hash = "/SMS-Logs";
	            }
	            /*goto functions for faq,privacy and policy and come to know by*/
	        $scope.gotoFAQ = function() { window.location.hash = "/FAQ" }
	        $scope.gotoPrivacyAndPolicies = function() { window.location.hash = "/Privacy-And-Policies" }
	        $scope.gotoComeToKnowBy = function() {
	            window.location.hash = "/Come-To-Know-By"
	        }
	        $scope.gotoCountryList = function() {
	            window.location.hash = "/Country-List";

	        }
	        $scope.gotoStatusList = function() {
	            window.location.hash = "/Status-List";

	        }
	        $scope.gotoGeoHashes = function() {
	            window.location.hash = "/Geo-Hashes";

	        }
	        $scope.gotoFeedback = function() {
	            window.location.hash = "/Feedback";
	            // body...
	        }
	        $scope.gotoPromoBanners = function() {
	            window.location.hash = '#/Promo-Banners';
	        }

	    }
	    app.controller('mainCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$modal', 'TableConstants', '$http', '$filter', '$timeout', '$log', mainCtrl]);
	}());
