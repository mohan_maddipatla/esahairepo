(function() {
    var app = angular.module('eSahai');
    var usersCtrl = function($scope, $rootScope, request, ctrlComm, $http, $filter, TableConstants, $timeout) {
        console.log("tableName: " + TableConstants.system_users);
        $scope.tabActive('userMaster');
        $scope.viewby = 10;
        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 15;
        $scope.user = {};
        $scope.totalItems = 0;
        $scope.tableRows = [5, 10, 15, 20, 30, 40];
        if ($scope.admin.userType === "GROUP ADMIN" || $scope.admin.userType == 'CUSTOMER CARE EXECUTIVE') {
            window.location.hash = '#/Home';
        }
        //$scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.toDate = $filter('date')(new Date(new Date().getTime() + 24 * 60 * 60 * 1000), 'yyyy-MM-dd');
        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.pageChanged = function() {
            // console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1;
        }
        $scope.page = {};
        $scope.err = {};

        $scope.getUsersList = function(cb) {
            $scope.loader(true);
            var req = {};
            req.tableName = TableConstants.system_users;
            req.params = {};
            /*var dateFilter = {};
            dateFilter.fieldName = 'createDateTime';
            dateFilter.startDate = $scope.fromDate;
            dateFilter.endDate = $scope.toDate;
            req.dateFilter = dateFilter;*/

            console.log(angular.toJson(req));
            request.service('fetchDataByField', 'post', req, $scope.CONFIG, function(response) {
                $scope.loader(false);
                if (response.statusCode == '300') {
                    $scope.getUserTypeList(function() {
                        angular.forEach(response.Result, function(obj) {
                            angular.forEach($scope.userTypeList, function(obj1) {
                                //console.log(obj1)
                                if (obj1.id == obj.userType) {
                                    obj.userTypeName = obj1.type_name;
                                }
                            })
                        })
                    })


                    $scope.usersList = response.Result;
                    angular.forEach($scope.usersList, function(obj2) {
                        angular.forEach($scope.globalGroupMasterList, function(obj3) {
                            if (obj2.group_id == obj3.group_id) {
                                obj2.groupName = obj3.group_name;
                            }
                        })

                    })
                    console.log($scope.usersList)
                        //$scope.changeList($scope.fromDate, $scope.toDate);
                    $scope.totalItems = $scope.usersList.length;
                    if ($scope.totalItems != 0 && $scope.tableRows.indexOf($scope.totalItems) == -1)
                        $scope.tableRows.push($scope.totalItems);

                    if ($scope.totalItems != 0) {
                        $scope.tableRows = $scope.tableRows.sort(request.sort);
                        $scope.tableRows.splice($scope.tableRows.indexOf($scope.totalItems) + 1);
                        if ($scope.tableRows[1]) {
                            $scope.viewby = $scope.tableRows[1];
                        } else {
                            $scope.viewby = $scope.tableRows[0];
                        }
                        $scope.setItemsPerPage($scope.viewby);
                    }


                    if (cb) cb();
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();

                    }, 2000)

                } else {
                    $scope.usersList = [];
                }
            })
        }


        /*$scope.getAppsList(function() {
            console.log($scope.appsList)
        })*/
        // $scope.getUsersList();
        if (window.location.hash == "#/Users") {
            $scope.getUsersList();
        } else if (window.location.hash == "#/New-User") {
            $scope.page.title = 'Add New User';
            $scope.page.type = 'post';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === 'CUSTOMER CARE EXECUTIVE') {
                window.location.hash = '#/Users';
            }
            console.log($scope.user)
                //$scope.getUsersList();

        } else if (window.location.hash == "#/Update-User") {
            $scope.page.title = 'Update User';
            $scope.page.type = 'put';
            if ($scope.admin.userType == 'EMPLOYEE' || $scope.admin.userType === 'CUSTOMER CARE EXECUTIVE') {
                window.location.hash = '#/Users';
            }
            prePoppulateValues(ctrlComm.get('userObj'));
        } else if (window.location.hash == '#/View-User') {
            $scope.page.title = 'View User';
            $scope.page.type = 'view';
            prePoppulateValues(ctrlComm.get('userObj'));
        }

        var viewUser = function(user) {
            ctrlComm.put('userObj', user);
            window.location.hash = '#/View-User';
        }




        var addNewUser = function() {
            window.location.hash = "#/New-User";
        }

        function prePoppulateValues(user) {
            if (user) {
                if ($scope.page.type == 'put') {
                    user.isActive == 'Active' ? user.isActive = true : user.isActive = false;
                    if (user.group_id == 0) {
                        user.group_id = '';
                    }
                }
                $scope.user = user;
            } else {
                window.location.hash = "#/Users"
            }
        }



        var validate = function(cb) {

            if (!$scope.user.email) {
                $scope.err.email = true;
            } else {
                if (!(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/).test($scope.user.email)) {
                    $scope.err.email_cdn = true;

                } else {
                    delete $scope.err.email;
                    delete $scope.err.email_cdn;
                }
            }

            if (!$scope.user.password) {
                $scope.err.password = true;
            } else {
                if (!(/^.{5,8}$/).test($scope.user.password)) {
                    $scope.err.password_cdn = true;

                } else {
                    delete $scope.err.password;
                    delete $scope.err.password_cdn;
                }
            }
            if (!$scope.user.firstName) {
                $scope.err.firstName = true;
            } else {
                delete $scope.err.firstName;
            }

            if (!$scope.user.lastName) {
                $scope.err.lastName = true;
            } else {
                delete $scope.err.lastName;
            }
            if (!$scope.user.displayName) { $scope.err.displayName = true; } else { delete $scope.err.displayName; }
            if (!$scope.user.userType) {
                $scope.err.userType = true;
            } else {
                delete $scope.err.userType;
                if ($scope.user.userType == '3') {
                    if (!$scope.user.group_id) { $scope.err.group_id = true; } else { delete $scope.err.group_id; }
                } else {
                    delete $scope.err.group_id;
                }
            }
            if (Object.keys($scope.err).length == 0) {

                if (cb) cb();
            }
        }


        var saveUser = function(user) {
            validate(function() {
                var req = {};
                req.tableName = TableConstants.system_users;
                var params = {};
                user.isActive = 'Active';
                if (user.userType == 1) {
                    user.group_id = 0;
                }
                params = angular.copy(user);
                delete params['groupName'];
                req.params = params;
                console.log(angular.toJson(req))
                request.service('insertRecord', 'post', req, $scope.CONFIG, function(response) {
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Users"
                        $scope.notification('User created successfully');
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else if (response.statusCode == '222') {
                        if (response.statusMessage.search('email') > 0) {
                            $scope.notification('Email already exists, Try with other', 'danger');
                        }
                    } else {
                        $scope.notification(response.statusMessage);
                    }
                })
            });
        }

        var updateUserObj = function(userObj) {
            validate(function() {
                /*$scope.loader(true);*/
                var req = {};
                req.tableName = TableConstants.system_users;
                var params = {};
                if (userObj.isActive == true) {
                    userObj.isActive = 'Active';
                } else {
                    userObj.isActive = 'InActive';
                }
                if (userObj.userType == 1) {
                    userObj.group_id = 0;
                }
                params = angular.copy(userObj);
                delete params['userId'];
                delete params['userTypeName'];
                delete params['groupName'];
                var conditionalParams = {};
                conditionalParams.userId = userObj.userId;
                req.params = params;
                req.conditionalParams = conditionalParams;
                console.log(angular.toJson(req))
                request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                    /*$scope.loader(false);*/
                    if (response.statusCode == '300') {
                        window.location.hash = "#/Users"
                        $scope.notification('User updated successfully');
                    } else if (response.statusCode == '222') {
                        if (response.statusMessage.search('email') > 0) {
                            $scope.notification('Email already exists, Try with other', 'danger');
                        }
                    } else if (response.statusCode == '204') {
                        $scope.notification(response.statusMessage, "danger");
                        $timeout(function() {
                            $scope.logout();
                        }, 2000)

                    } else {
                        $scope.notification(response.statusMessage);
                    }

                })
            });
        }

        var updateUser = function(User) {
            ctrlComm.put('userObj', User);
            window.location.hash = "#/Update-User"
        }


        var cancelUser = function() {
            window.location.hash = "#/Users"
        }

        var deleteUser = function(user) {
            var input = {
                text: 'Are you sure you want to delete',
                name: user.display_name
            }
            $scope.confirmation(input, function() {
                confirmDelete(user);
            });
        }

        function confirmDelete(user) {
            console.log(user)
                /* var req = {};
                 req.tableName = TableConstants.system_users;
                 var params = {};
                 params.userId = user.userId;
                 req.params = params;
                 request.service('deleteRecord', 'delete', req, $scope.CONFIG, function(response) {
                     if (typeof response == 'string') {
                         response = JSON.parse(response)
                     }
                     if (response.statusCode == '300') {
                         $scope.getUsersList(function() {
                             $scope.notification(response.statusMessage);
                         })
                     }else if (response.statusCode == '400' && response.statusMessage.indexOf("Cannot delete or update a parent row") > -1) {
                         $scope.notification("Cannot delete parent record","danger");
                     } else if (response.statusCode == '204') {
                         $scope.notification(response.statusMessage, "danger");
                         $timeout(function() {
                             $scope.logout();

                         }, 2000)
                     }
                 })*/
            var req = {};
            req.tableName = TableConstants.system_users;
            var params = {};
            user.isActive = 'InActive';
            params = angular.copy(user);
            delete params['userId'];
            delete params['userTypeName'];
            delete params['groupName'];
            var conditionalParams = {};
            conditionalParams.userId = user.userId;
            req.params = params;
            req.conditionalParams = conditionalParams;
            console.log(angular.toJson(req))
            request.service('updateRecord', 'put', req, $scope.CONFIG, function(response) {
                /*$scope.loader(false);*/
                if (response.statusCode == '300') {
                    $scope.getUsersList(function() {
                        $scope.notification("User Deleted Successfully");
                    })
                } else if (response.statusCode == '204') {
                    $scope.notification(response.statusMessage, "danger");
                    $timeout(function() {
                        $scope.logout();
                    }, 2000)

                } else {
                    $scope.notification(response.statusMessage);
                }

            })


        }

        var showAlltableRows = function() {
            $scope.viewby = $scope.totalItems;
            $scope.setItemsPerPage($scope.viewby);
        }



        $scope.showAlltableRows = showAlltableRows;
        $scope.addNewUser = addNewUser;
        $scope.saveUser = saveUser;
        $scope.updateUser = updateUser;
        $scope.deleteUser = deleteUser;
        $scope.cancelUser = cancelUser;
        $scope.updateUserObj = updateUserObj;
        $scope.viewUser = viewUser;


    }


    app.controller('usersCtrl', ['$scope', '$rootScope', 'request', 'ctrlComm', '$http', '$filter', 'TableConstants', '$timeout', usersCtrl]);
}());
