'use strict';
/*
 * Description: This is a service for eSaHai-Middleware Application
 * Author: Tech Vedika Software Pvt.Ltd.
 * Developer: Pallavi Kyatham
 */

//Required node modules
var express = require('express');
var dbOperations = require('./controllers/DBOperations.js');

var loginService  = require('./controllers/loginService.js');

var http = require('http');
var swaggerTools = require('./swagger-tools');
var constants = require("./Util/commonConstants.js");
var serverPort = constants.serverPort;
var serverUrl =constants.serverUrl;
var swaggerPath = "./api/swaggerAPI.json";
var logger=require('./logging/logger.js');

var booking = require("./controllers/booking.js");
var ambulanceAPI = require("./controllers/ambulanceAPI.js");
var customer = require("./controllers/customer.js");
var excel = require("./controllers/excel.js");
var promos = require("./controllers/promoBanners.js");
// swaggerRouter configuration
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();
var dbConnect = require('./Util/dbConnection.js');
var app = express();
app.configure(function(){
  app.set('port', process.env.PORT || serverPort);
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.urlencoded());
  app.use(express.methodOverride());
  app.all('/*', function(req, res, next) {
   // res.header('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Origin', '*');

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, PATH');

   // res.setHeader('Access-Control-Allow-Headers', 'content-type','X-token', 'X-userId');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,token,userId');

    //req.setHeader('Access-Control-Allow-Headers','token', 'userId');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    if('OPTIONS' === req.method) { next(); return; }
    else next();
});
 app.use(app.router);
});
app.configure('development', function(){
  app.use(express.errorHandler());
});
app.get('/', function(req, res){
  res.end("eSaHai App Server");
});

dbConnect.dbConnection();

//The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var swaggerDoc = require(swaggerPath);

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  //app.use(middleware.swaggerValidator());
  // Provide the security handlers
  app.use(middleware.swaggerSecurity({
    userId: function (req, def, scopes, callback) {
      if (req.userId()) {
        return callback();
      }
      req.res.status(401).json({
        message: "Please Authenticate"
      });
      req.res.end();
    }
}));

  app.use(middleware.swaggerRouter(
  {
    swaggerUi: './api/swaggerAPI.json',
    controllers: './controllers'

  }));

app.use(middleware.swaggerUi(
  {
    "apiDocs": "/esahai/api_docs",
//    "swaggerUi": "/swagger"
"swaggerUi": "/swagger"
  }
));
//swaggerUi.api.clientAuthorizations.add("key", new SwaggerClient.ApiKeyAuthorization("Authorization", "XXXX", "header"));

 
});
// Start the server
http.createServer(app).listen(serverPort, function () {
  logger.info('listening on port.....',serverPort);
});

app.post('/db/createTable', dbOperations.createTable);
app.post('/db/addColumn', dbOperations.addColumn);
app.delete('/db/deleteColumn', dbOperations.deleteColumn);
app.put('/db/updateColumn', dbOperations.updateColumn);
app.delete('/db/deleteTable', dbOperations.deleteTable);

app.post('/db/insertRecord', dbOperations.insertRecord);
app.put('/db/updateRecord', dbOperations.updateRecord);
app.delete('/db/deleteRecord', dbOperations.deleteRecord);
app.post('/db/fetchDataByField', dbOperations.getDataByField);
app.post('/db/tableDesc', dbOperations.getTableSchema);
app.post('/db/login', loginService.login);

app.post('/db/**/createTable', dbOperations.createTable);
app.post('/db/**/addColumn', dbOperations.addColumn);
app.delete('/db/**/deleteColumn', dbOperations.deleteColumn);
app.put('/db/**/updateColumn', dbOperations.updateColumn);
app.delete('/db/**/deleteTable', dbOperations.deleteTable);

app.post('/db/**/insertRecord', dbOperations.insertRecord);
app.put('/db/**/updateRecord', dbOperations.updateRecord);
app.delete('/db/**/deleteRecord', dbOperations.deleteRecord);
app.post('/db/**/fetchDataByField', dbOperations.getDataByField);

app.post('/db/getDashboardDetails', loginService.getDashboardDetails);
app.post('/db/*/logout', loginService.logout);

/** Ambulance APP API **/
app.post('/db/*/generateOTPAmbulance', loginService.generateOTPAmbulance);
app.post('/db/*/verifyAmbulanceOTP', loginService.verifyAmbulanceOTP);
app.post('/db/*/driverLogin', loginService.driverLogin);
app.post('/db/*/driverLogout', loginService.driverLogout);
app.post("/db/assignBooking",booking.assignBooking);
app.post("/db/transferBooking",booking.transferBooking);
app.post("/db/updateAmbulanceLocation",ambulanceAPI.trackAmbulance);
app.post("/db/updateAmbulanceDriverStatus",ambulanceAPI.updateAmbulanceDriverStatus);
app.post("/db/getTripList",booking.getTripList);
app.post("/db/updateAmbulanceStatus",ambulanceAPI.updateAmbulanceStatus);

/** Command Centre Booking API**/
app.post("/db/createBookingByCommandCentre",booking.createBookingByCommandCentre);
app.post("/db/assignBookingByCommandCentre",booking.assignBookingByCommandCentre);
app.post("/db/transferByCommandCentre",booking.transferByCommandCentre);
app.post('/db/checkCustomerExistsOrNot', customer.checkCustomerExistsOrNot);

/** Customer App API **/
app.post('/db/*/customerGenerateOTP', loginService.customerGenerateOTP);
app.post('/db/*/verifyCustomerOTP', loginService.verifyCustomerOTP);
app.post("/db/getCustomerprofile",booking.getCustomerprofile);
app.post("/db/createBooking",booking.createBooking);
app.post('/db/getCommandCentreNUmber',customer.getCommandCentreNUmber);
app.post("/db/checkMyBookingStatus",booking.checkMyBookingStatus);
app.post("/db/getActiveBooking",booking.getActiveBooking);
app.post('/db/updateProfile', customer.updateProfile);
app.post('/db/addfeedback', customer.addfeedback);
app.post("/db/getRateCard",customer.getRateCard);
app.post("/db/addEmergencyContactForCustomer",booking.addEmergencyContactForCustomer);
app.post("/db/updateEmergencyContactForCustomer",booking.updateEmergencyContactForCustomer);
app.post("/db/deleteEmergencyContactForCustomer",booking.deleteEmergencyContactForCustomer);
app.get('/db/getTermsAndConditions', customer.getTermsAndConditions);
app.post("/db/addBloodRequest", booking.insertBloodRequest);
app.post("/db/getEmergencyContactsForCustomer",booking.getEmergencyContactsForCustomer);
app.post("/db/updateCustomerProfile",booking.updateCustomerProfile);
app.post("/db/getEmergencies",booking.getEmergencies);

/** common API **/
app.post("/db/getNearByAmbulanceListSpecificType",booking.getNearByAmbulanceListSpecificType);
app.post("/db/getAmbulanceTypes",ambulanceAPI.getAmbulanceTypes);
app.post("/db/cancelBooking",booking.cancelBooking);

app.post('/db/addPatientInfo', customer.addPatientInfo);
app.post('/db/updatePatientInfo', customer.updatePatientInfo);
app.post('/db/getPatientInfo', customer.getPatientInfo);

app.get('/db/getAppVersion?**', customer.getAppVersion);
app.post('/db/updateDeviceToken', ambulanceAPI.updateDeviceToken);

app.post("/db/getDriversListByAmbulanceId",ambulanceAPI.getDriversListByAmbulanceId);
app.post("/db/updateBooking",booking.updateBooking);
app.post("/db/getAmbulanceList",booking.getAmbulanceList);
app.post("/db/getNearByAmbulanceList",booking.getNearByAmbulanceList);
app.post("/db/getNearByAmbulanceListByHospitalAndEmergeType",booking.getNearByAmbulanceListByHospitalAndEmergeType);
app.post("/db/getNearByHospitalList",booking.getNearByHospitalList);
app.post("/db/getEmergencyTypeList",ambulanceAPI.getEmergencyTypeList);
app.post("/db/getAmbulanceDriverStatusList",ambulanceAPI.getAmbulanceDriverStatusList);
app.get("/db/getStatusList",ambulanceAPI.getStatusList);
app.post("/db/getAmbulanceTrackDetails",ambulanceAPI.getAmbulanceTrackDetails);
app.post("/db/getTripDetails",booking.getTripDetails);
app.get('/db/getComeToKnowBy', customer.getComeToKnowBy);
app.get('/db/getPrivacyPolicy', customer.getPrivacyPolicy);
app.post('/db/faqList',customer.getFaqList);
app.post("/db/getAmbulanceProfile",ambulanceAPI.getAmbulanceProfile);
app.get('/db/getCountryList', customer.getCountryList);

app.post("/db/uploadExcel", excel.uploadExcel);
app.post('/db/changePassword', dbOperations.changePassword);
app.post('/db/forgotPassword', dbOperations.forgotPassword);
app.post("/db/commonLimitOrActiveCheck",dbOperations.commonLimitOrActiveCheck);
app.post('/db/getGroupMasterList',dbOperations.getGroupMasterList);
app.post("/db/uploadBanner",promos.uploadBanner);
app.post("/db/getPromos",promos.getPromos);
app.post('/db/getFeedbackList',dbOperations.getFeedbackList);
app.post("/db/updateBanner",promos.updateBanner);
