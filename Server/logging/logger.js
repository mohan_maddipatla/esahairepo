
var winston = require('winston');
winston.emitErrs = true;

var commonConstants = require('../Util/commonConstants.js');

var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: commonConstants.logFile,
            handleExceptions: true,
            json: true,
            maxsize: 104857600, //100MB
            maxFiles: 5,
            colorize: true
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};
