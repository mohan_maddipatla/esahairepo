var password = '@*%$!eSaHai';
var crypto = require("crypto");

key = new Buffer(32)
iv = new Buffer(16);
iv.fill(0);
algorithm = 'aes-256-cbc';
key.write(password,"utf8")
key.fill(0,11,32)

exports.encrypt = function(text){
	cipher = crypto.createCipheriv('aes-256-cbc', key.toString("utf8"), iv)
	output = cipher.update(text, 'utf8', 'base64')
	output += cipher.final('base64')
	return output
}

exports.decrypt = function decrypt(text){
  var decipher = crypto.createDecipheriv('aes-256-cbc', key.toString("utf8"), iv)
  var dec = decipher.update(text,'base64','utf8')
  dec += decipher.final();
  return dec;
}

/*var password = '@*%$!eSaHai';

var crypto = require("crypto");
exports.encrypt = function (toEncrypt) {  
	var cryptkey   = crypto.createHash('sha256').update(password).digest(),
	 iv = new Buffer([1, 2, 3, 4, 5, 6, 7, 8]);
	var pass =crypto.pbkdf2Sync(new Buffer(cryptkey).toString('base64'), iv, 1000, 32);
	var ivKey =crypto.pbkdf2Sync(new Buffer(cryptkey).toString('base64'), iv, 1000, 16);

	  var cipher = crypto.createCipheriv('aes256', pass, ivKey);
	  var crypted = cipher.update(toEncrypt, 'utf8', 'base64');
	  crypted += cipher.final('base64');
  return crypted;
}

exports.decrypt = function (toDecrypt) {
	var cryptkey   = crypto.createHash('sha256').update(password).digest(),
	 iv = new Buffer([1, 2, 3, 4, 5, 6, 7, 8]);
	

	var pass =crypto.pbkdf2Sync(new Buffer(cryptkey).toString('base64'), iv, 1000, 256/8);
	var ivKey =crypto.pbkdf2Sync(new Buffer(cryptkey).toString('base64'), iv, 1000, 128/8);


  var decipher = crypto.createDecipheriv('aes256', pass, ivKey);
  var dec = decipher.update(toDecrypt, 'base64', 'utf8');
  dec += decipher.final();
  return dec;
};*/
//console.log("Encryption = ",encrypt("73066574444"))
