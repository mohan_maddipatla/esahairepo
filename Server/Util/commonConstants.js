exports.appUserServiceId=46;
exports.appDeviceServiceId=47;
exports.apiDocsServiceId=35;
exports.mySqlServiceTypeId=7;
exports.emailServiceTypeId=18;
exports.httpServiceTypeId=24;
exports.soapServiceTypeId=25;
exports.callbackApiSeriveTypeId=31;
exports.mobileApiSeriveTypeId=32;
exports.serverUrl="http://prod.esahai.in/";
exports.serverPort = 3000;
exports.portName = "/esahai";
exports.logFile="/opt/esahai-logs/esahai.log";
exports.POST = "POST";
exports.statusCode="statusCode";
exports.statusMessage = "statusMessage";
exports.ambulanceImageUploadPath = "/var/www/ambulances/";
exports.ambulanceFilePath = "http://prod.esahai.in/ambulances/";
exports.driverImageUploadPath = "/var/www/drivers/";
exports.driverFilePath = "http://prod.esahai.in/drivers/";
exports.patientImageUploadPath = "/var/www/patients/";
exports.patientFilePath = "http://prod.esahai.in/patients/";
exports.googleAPIKEY = /*"AIzaSyCm4iaH1yVrrwoDaVdNvKpdAYnLUjPa89s";*//*"AIzaSyBILQohUbnLLLvNH-Y-rnLo-3bkeZHoJSs"*/"AIzaSyCpTuenOL_15K5LGH-YMuoK9xqAmbJ3HAc";
exports.distanceMatrixApiKey = "AIzaSyBILQohUbnLLLvNH-Y-rnLo-3bkeZHoJSs";
exports.distanceLimit = 25;//in km

/** Group Type Ids**/
exports.anyGroupTypeId = 8;
exports.hospitalGroupTypeId = 9;
exports.privateGroupTypeId = 10;
exports.govtGroupTypeId = 11;

/** Email Constants*/
exports.userName="noreplyesahai@gmail.com";
exports.password = "esahai123";
exports.host="Gmail";
exports.To = "bookings@esahai.in";

/** booking status constants*/
/*exports.Pending = "Pending";
exports.Accepted = "Accepted";
exports.ToEmergencyLocation = "Way to Emergency Location";
exports.AtEmergencyLocation = "At Emergency Location";
exports.WayToDestination = "Way back to Destination";
exports.ReachedDestination = "Reached Destination";
exports.TripClosed = "Trip Closed";
exports.Cancelled = "Cancelled";
exports.FakeCancel = "Cancelled (Fake)";*/


/** No Response constants*/
/*exports.NoAmbulanceFound = "No Ambulance";
exports.NoOneAccepted = "No Response";
exports.TransferredNoResponse="No Response on transfer";
*/

/** apis for SMS **/
exports.sendSmsPath ="http://api.smscountry.com/SMSCwebservice_bulk.aspx?";

/** PN TOPICS*/
exports.FAQ = "FAQ";
exports.termsNconditions = "TNC";
exports.privacyPolicy = "PRIVACY_POLICY";
exports.emergencyTypes = "EMERGENCY-TYPES";
exports.comeToKnow = "HOW-DO-YOU-KNOW";

/** Number of near by hospital to fetch */
exports.numberOfNearByHospitals = 10;

/** booking status constants*/
exports.Pending = "Pending";
exports.Accepted = "ACTD";
exports.ToEmergencyLocation = "WTEL";
exports.AtEmergencyLocation = "AEL";
exports.WayToDestination = "WBTD";
exports.ReachedDestination = "RD";
exports.TripClosed = "TC";
exports.Cancelled = "CAN";
exports.FakeCancel = "CANCFAKE";
exports.NoAmbulanceFound = "NOAMB";
exports.NoOneAccepted = "NORESP";
exports.TransferredNoResponse="NORESPTNSF";

/** country code for adding in ambulance and group sms**/
exports.indiaCode = "91";

exports.promoBannerUploadDir = "/var/www/promoBanners/";
exports.promoBannerPath = "http://prod.esahai.in/promoBanners/";

/** command Centre number*/
exports.commandCentreNumber = "+914047911911";

/**  medical taxi **/
exports.medicalTaxiId = 28;

/** chunk size for bulk pn**/
exports.customerSize = 1000;
