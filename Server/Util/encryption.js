var crypto = require('crypto');
var algorithm = 'aes-256-cbc';


var password = '@*%$!RetailerApp';

exports.encrypt = function (toEncrypt) {  
	var cryptkey   = crypto.createHash('sha256').update(password).digest(),
	 iv = new Buffer([1, 2, 3, 4, 5, 6, 7, 8]);

	var pass =crypto.pbkdf2Sync(cryptkey, iv, 1000, 32);
	var ivKey =crypto.pbkdf2Sync(cryptkey, iv, 1000, 16);
	
	  var cipher = crypto.createCipheriv('aes256', pass, ivKey)
	  var crypted = cipher.update(toEncrypt, 'utf8', 'base64')
	  crypted += cipher.final('base64');
  return crypted;
};

exports.decrypt = function (toDecrypt) {
	var cryptkey   = crypto.createHash('sha256').update(password).digest(),
	 iv = new Buffer([1, 2, 3, 4, 5, 6, 7, 8]);

	var pass =crypto.pbkdf2Sync(cryptkey, iv, 1000, 256/8);
	var ivKey =crypto.pbkdf2Sync(cryptkey, iv, 1000, 128/8);

  var decipher = crypto.createDecipheriv('aes256', pass, ivKey);
  var dec = decipher.update(toDecrypt, 'base64', 'utf8');
  dec += decipher.final('utf8');
  return dec;
};
