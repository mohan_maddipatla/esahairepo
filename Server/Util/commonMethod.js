var appUserDao = require("../Dao/appUserDao.js");
var statusConstant = require("../Util/statusConstants.json");
var commonConstants = require("../Util/commonConstants.js");
var crypto = require('crypto'),
    connections = require("../Util/dbConnection.js");
var logger = require("../logging/logger.js");
var distance = require('google-distance-matrix');

function generateRandomChars(howMany, chars) {
    try {
        chars = chars || "" + new Date().getTime();
        var rnd = crypto.randomBytes(howMany),
            value = new Array(howMany),
            len = chars.length;

        for (var i = 0; i < howMany; i++) {
            value[i] = chars[rnd[i] % len]
        };
        return value.join('');
    } catch (e) {
        return 'null';
    }

}
exports.getRandomUserAccessId = function() {
    var randomId = generateRandomChars(8);
  
    return randomId;
}

exports.getRandomBookingId = function() {
    var randomId = generateRandomChars(6);
   
    return randomId;
}


exports.verifyUser = function(data, fn) {
    var connection = connections.connection;
  //  if(data.ambApp!=undefined && (data.ambApp == true || data.ambApp == "true") ){
     if((data.ambApp!=undefined && (data.ambApp == true || data.ambApp == "true")) || (data.ambulanceApp!=undefined && (data.ambulanceApp == true || data.ambulanceApp == "true")) ){

        console.log("IN ambla ne i app ");
         var query = "select * from ambulance_master where amb_id = " + parseInt(data.userId) ;
        connection.query(query, function(err, res) {
          //  console.log("OOOO ",err);
        //    console.log("RES ",res);
            if (err) {
                fn(statusConstant.s108, err);
            } else {
                if (res.length > 0) {
                    if (res[0].token === data.token) {
                        fn(statusConstant.s103, res);
                    } else {
                        fn(statusConstant.s104, res);
                    }
                } else {
                    fn(statusConstant.s105, res);
                }
            }
        });
    }else if(data.customerApp!=undefined && (data.customerApp == true || data.customerApp == "true")){
        console.log("IN customer app ");
         var query = "select * from customer_master where customer_id = " + parseInt(data.userId) ;
        connection.query(query, function(err, res) {
            if (err) {
                fn(statusConstant.s108, err);
            } else {
                if (res.length > 0) {
                    if (res[0].token === data.token) {
                        //fn(statusConstant.s103, res);
                    	if(data['customerMobile']!=undefined){
                            if (res[0].customer_mobile_number == data.customerMobile) {
                            fn(statusConstant.s103, res);
                        } else {
                            fn(statusConstant.s106, res);
                        }
                    }else{
                             fn(statusConstant.s103, res);
                    }

                    } else {
                        fn(statusConstant.s104, res);
                    }
                } else {
                    fn(statusConstant.s105, res);
                }
            }
        });
    }
    else if (data.fromApp != undefined && (data.fromApp == true || data.fromApp == "true")) {
        var query = "select * from "+statusConstant.TABLE_APP_USER+" where generated_UserID = '" + data.userId + "'";
        connection.query(query, function(err, res) {
           logger.info("VERIFY USER ", res);
	logger.info("VERIFY USER LEGNTH ", res.length);
	   logger.info("UUUU VERIFY ",err); 
	   if (err) {
                fn(statusConstant.s108, err);
            } else {
                if (res.length > 0) {
                    if (res[0].token === data.token) {
                        fn(statusConstant.s103, res);
                    } else {
                        fn(statusConstant.s104, res);
                    }
		 /*  if(res[0].userStatus == "Active"){
                        if (res[0].token === data.token) {
                            fn(statusConstant.s103, res);
                        } else {
                            fn(statusConstant.s104, res);
                        }
                    }else{
                        fn(statusConstant.s129, res);
                    }*/
                } else {
                    fn(statusConstant.s105, res);
                }
            }
        });
    } else {
        var query = "select * from system_users where userId = '" + data.userId + "'";
        connection.query(query, function(err, res) {
            if (err) {
                fn(statusConstant.s108, err);
            } else {
                if (res.length > 0) {
                    if (res[0].token === data.token) {
                        var userType = "";
                        connection.query("select userType from system_users where userId = '" + data.userId + "'", function(err1, res1) {
                            if (!err1) {
                                if (res1 != undefined && res1.length > 0) {
                                    userType = res1[0].userType;
                                }
                                if (userType == "user") {
                                    var q = "select role_id from user_app_roles where user_id='" + data.userId + "'";
                                    connection.query(q, function(err2, res2) {
                                        if (!err2) {
                                            if (res2 != null && res2.length > 0) {
                                                var roleIds = "";
                                                for (var i = 0; i < res2.length; i++) {
                                                    roleIds = roleIds + (roleIds != "" ? "," : "") + res2[i].role_id;
                                                }
                                                connection.query("select * from access_roles where roleId in (" + roleIds + ")", function(err3, qdata) {
                                                    if (!err3) {
                                                        if (qdata != null && qdata.length > 0) {
                                                            var access = "";
                                                            for (var i = 0; i < qdata.length; i++) {
                                                                access = access + (access != "" ? "," : "") + qdata[i].access;
                                                            }
                                                        }
                                                        var method = "";
                                                        method = data.method;
                                                        if (access.toLowerCase().indexOf(method.toLowerCase()) > -1) {
                                                            console.log("ALL CLEAR" + method);
                                                            fn("success", res)
                                                        } else {
                                                            fn("err", err3);
                                                        }


                                                    }
                                                })
                                            }
                                        } else {
                                            fn("err", err2);
                                        }

                                    });
                                } else {

                                    fn(statusConstant.s103, res);
                                }
                            } else {
                                fn(statusConstant.s108, err);
                            }
                        });
                    } else {
                        fn(statusConstant.s104, res);
                    }

                } else {
                    fn(statusConstant.s105, res);
                }
            }
        });


    }
}
exports.appRoleCheckForUser = function(data, fn) {
    var connection = connections.connection;
    if (data.userType == "user") {
        var query = "select app_id from user_app_roles where user_id = '" + data.userId + "'";
        connection.query(query, function(err, res) {
            if (!err) {
                if (res != undefined && res.length > 0) {
                    var appIds = "";
                    for (var i = 0; i < res.length; i++) {
                        appIds = appIds + (appIds != "" ? "," : "") + res[i].app_id;
                    }
                    connection.query("select * from Apps where id in (" + appIds + ")", function(err, result) {
                        if (!err) {
                            if (result != undefined && result.length > 0) {
                                fn(statusConstant.s103, result);
                            }
                        }
                    })
                } else {
                    fn(statusConstant.s109, null);
                }
            }
        })
    } else {
        fn(statusConstant.s103, null);
    }


}
exports.getSetServicesList = function(fn) {
    var connection = connections.connection;
    var query = "select s.id,s.name,s.label,s.service_type_id,s.isDefault,st.type_name from services s, serviceType st where s.service_type_id=st.id";
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined && res.length > 0) {
                fn(res);
            } else {
                fn(statusContant.s111);
            }
        }
    })

}
exports.syncData = function(id) {
    var connection = connections.connection;
    var query = "SELECT * FROM serviceFieldValues where serviceId='" + id + "' order by serviceFieldsId desc limit 1";
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined && res.length > 0) {
                return res;
            } else {
                return null;
            }
        }
    })

}
exports.syncDatasoap = function(id, fn) {
    var connection = connections.connection;
    var query = "SELECT * FROM serviceFieldValues where serviceId='" + id + "' order by serviceFieldsId asc limit 1";
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined && res.length > 0) {
                fn(res);
            } else {
                fn(null);
            }
        } else {
            fn(null);
        }
    })

}
exports.generateOTP = function() {
//    var otp = Math.floor(Math.random() * 1000000);
var otp = Math.floor(Math.random() * 900000) + 100000;
    return otp;
}

exports.getErrorCodes = function(apiName,errorDesc,fn){
         var connection = connections.connection;
         connection.query("select * from error_codes where apiName='"+apiName+"' and errorDesc='"+errorDesc+"';",function(err,res){
                if(!err){
                        if(res!=undefined && res.length>0)
                        fn(res);
                }
         });
}


var convertMilesToKm  = function(miles,fn){
    var km = parseFloat(miles)*(1.609);
    console.log("KKKKKKKKKK ",km)
    fn(km);
}

exports.calculateDistance = function(origins,destinations,fn){
/** alternative for distance*/
/*var distanceDurationArray = [];
var duration = "0";
var GeoPoint = require('geopoint');
var latlong = origins[0].split(",");
var lat1 = parseFloat(latlong[0].toString());
var lon1 = parseFloat(latlong[1].toString());
logger.info("Lat Longs - Origin : ", lat1, lon1);
var latlong1 = destinations[0].split(",");
var lat2 = parseFloat(latlong1[0].toString());
var lon2 = parseFloat(latlong1[1].toString());
logger.log("Lat Longs end:: ", lat2, lon2);
point1 = new GeoPoint(lat1, lon1);
point2 = new GeoPoint(lat2, lon2);

var distance = point1.distanceTo(point2, true) // in km 
var distanceObj = {};
distanceObj = {
    "distance": distance,
    "duration": duration
}
distanceDurationArray.push(distanceObj);
fn("distance", distanceDurationArray);*/

distance.key(commonConstants.distanceMatrixApiKey);
distance.units('imperial');
 
distance.matrix(origins, destinations, function (err, distances) {
	logger.info("DISTANCE MATRIX API ERR============  ",err);
	logger.info(" Distaces ====================     ",distances);
    if (err) {
         console.log(err);
         fn("err")
    }
    if(!distances) {
         console.log('no distances');
         fn("noDistances");
    }
    if (distances.status == 'OK') {
        var distanceDurationArray = [];
       // var durationArray = [];
        for (var i=0; i < origins.length; i++) {
            for (var j = 0; j < destinations.length; j++) {
                var origin = distances.origin_addresses[i];
                var destination = distances.destination_addresses[j];
                if (distances.rows[0].elements[j].status == 'OK') {
                    var distance = distances.rows[i].elements[j].distance.value;
distance = parseFloat(distance/1000) ;
console.log("DIST VALUE ",distances.rows[i].elements[j].distance.value);
                    var duration = distances.rows[i].elements[j].duration.text;
                    console.log("DURAION ",duration);
                    console.log('Distance from ' + origin + ' to ' + destination + ' is ' + distance);
		distanceObj = {
                            "distance":distance,
                            "duration":duration
                        }
                        distanceDurationArray.push(distanceObj);
   
                } else {
                    console.log(destination + ' is not reachable by land from ' + origin);
                    //fn("notReachable");
                }
                if(j == destinations.length-1){
                    if(distanceDurationArray.length>0){
                        fn("distance",distanceDurationArray);
                    }
                    else
                        fn("notReachable");
                }
            }
        }
    }
});
}


exports.calculateDistanceForNearByHospitals = function(origins,destinations,fn){
        var distanceDurationArray = [];
        var duration = "0";
        var GeoPoint = require('geopoint');
        var latlong = origins[0].split(",");
        var lat1 = parseFloat(latlong[0].toString());
        var lon1 = parseFloat(latlong[1].toString());
        console.log("ORILJFG ",lat1,lon1);
        var latlong1 = destinations[0].split(",");
        var lat2 = parseFloat(latlong1[0].toString());
        var lon2 = parseFloat(latlong1[1].toString());
        console.log("LKLKL ",lat2,lon2);
        point1 = new GeoPoint(lat1,lon1);
        point2 = new GeoPoint(lat2,lon2);

        var distance = point1.distanceTo(point2, true)// in km 
        var distanceObj = {};
        distanceObj = {
                        "distance":distance,
                        "duration":duration
        }
        distanceDurationArray.push(distanceObj);
        fn("distance",distanceDurationArray);

}

exports.getRandomDriverPin = function() {
    var randomId = generateRandomChars(4);  
    return randomId;
}
