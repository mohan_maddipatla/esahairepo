var mailer = require("nodemailer");

var smtpTransport = mailer.createTransport({
    service : 'Gmail',
    secureConnection : false,
    port: 587,
    auth: {
        user:commonConstants.userName,
        pass:commonConstants.password
        }
    });


exports.sendEmail = function(data,emailList,subject,html){
      var mail = {
        from: commonConstants.userName,
        to: commonConstants.To,
        subject: subject,
        text: data,
        html:html
    }

smtpTransport.sendMail(mail, function(error, response){
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: ",response.response.toString());
        }

        smtpTransport.close();
    });


}
