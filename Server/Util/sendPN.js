var request = require('request'),
	logger=require('../logging/logger.js');
var connections = require('./dbConnection.js');
var generalDao = require('../Dao/generalDao.js');
var commonConstants = require("../Util/commonConstants.js");
exports.sendGeneralPNToTopics = function (topic, apiKey, data, message) {
	try { 
		logger.info('in android send push notification. key===',apiKey);
		var response={};
		response['title']=message;
		if (data != null) {
			response['body']=data;
		}
		var pnObj={};
		pnObj['send_to'] = "All";
		pnObj['message'] = (typeof data == "string")?data:JSON.stringify(data);
		pnObj['pn_type'] = topic;
		pnObj['user_type'] = "Customer";
		var jsonData = {"to": topic,"data": response};
		request({
		      url: 'https://gcm-http.googleapis.com/gcm/send', //URL to hit
		      method: 'POST',
		      headers: {
		        'Content-Type':'application/json',
		        'Authorization': 'key='+apiKey
		      },
		      body: JSON.stringify(jsonData) //Set the body as a string'
		  }, function(error, response, body){
		      if(error) {
		    	  pnObj['status'] = "Failed";
                  generalDao.insertPNLOG(pnObj);  	
		          logger.debug('error while pns to andriod topics====',error);
		      } else {
		    	  logger.info("json body===",JSON.stringify(jsonData));
		    	  logger.info('android body====',JSON.stringify(jsonData));
		          logger.info("android sc",response.statusCode,"body", body);
		          pnObj['status'] = "Success";
                  generalDao.insertPNLOG(pnObj);  	
		        
		      }
		  });
	} catch (err) {
		logger.debug("unable to process your request to send push notification", err);
		pnObj['status'] = "Exception";
        generalDao.insertPNLOG(pnObj);  
	}
};

exports.sendPNToAndroidDevices = function (deviceTokens, data, message,type,pnObj,ios) {
	try {
		
//		logger.info("SEND PNNNNNN ",deviceTokens);
		pnObj['message'] = (typeof message == "string")?message:JSON.stringify(message);
		pnObj['pn_type'] = type;

		apiKey = commonConstants.googleAPIKEY;
		logger.info('in android send push notification. key===',apiKey);
	
		var tokens=[];
		var i=0;
		var t="";
	
		iterateApp();
		function iterateApp() {
			if(i<deviceTokens.length) {
				if (deviceTokens[i] != null) {
					tokens.push(deviceTokens[i].trim());
					i++;
					iterateApp();
				} else {
					i++;
					iterateApp();
				}
			} else {
				var json=JSON.stringify(tokens);
			}
		}
		 var jsonData = {"registration_ids": tokens,"priority":"high","data": {"title": message,"body":data,"type":type},"time_to_live":30};
		logger.info("IOS ", ios);
                if(ios == true)
                        jsonData = {"registration_ids": tokens,"priority":"high","data": {"title": message,"body":data,"type":type},"time_to_live":30,"content_available":true,"notification": {"title": (type == "new_banner"?message.title:""),"body":(type == "new_banner"?message.description:data),"type":type,"sound": "default"}};
                else
                        jsonData = {"registration_ids": tokens,"priority":"high","data": {"title": message,"body":data,"type":type},"time_to_live":30};
//		logger.info("JSON PAYLOAF ", jsonData);

		  request({
		      url: 'https://gcm-http.googleapis.com/gcm/send', //URL to hit
		      method: 'POST',
		      headers: {
		        'Content-Type':'application/json',
		        'Authorization': 'key='+apiKey
		      },
		      body: JSON.stringify(jsonData) //Set the body as a string'
		  }, function(error, response, body){
		      if(error) {
		    	  pnObj['status'] = "Failed";
                  generalDao.insertPNLOG(pnObj);  	
		          logger.debug('error while sending andriod pn',error);
		          logger.info('error while sending andriod pn=======',error);
		      } else {
		    	  pnObj['status'] = "success -"+response.statusCode;
                  generalDao.insertPNLOG(pnObj);    
		    	  logger.debug('android body====',JSON.stringify(jsonData));
		          logger.info("android sc",response.statusCode,"body", body);
		      }
		    
		  });
	} catch (err) {
		pnObj['status'] = "ExceptionOccured";
        generalDao.insertPNLOG(pnObj);
		logger.debug("unable to process your request to send push notification", err);
	}
};
