var mysql = require("mariasql"),
	settings = require('../Config/settings');
var logger = require("../logging/logger.js");
//global variables
exports.connection = null;

//Database Connection
exports.dbConnection = function(){
	/*connection = mysql.createConnection({
		host: settings.mysql.host,
		user : settings.mysql.user,
		password:settings.mysql.password,
		database: settings.mysql.database
	});
	try{
		connection.connect(function(err){
			if(err) {
				console.log("emergency","[dbConnection()]-Error encountered while connecting",err);	
				exports.connection= null;
			}else{
				console.log("debug","[dbConnection()]-Database Connected");
				exports.connection=connection;
			}
		});
	}catch(e){
		console.log("debug","[dbConnection()]-Caught Exception"+e);
		exports.connection= null;
	}*/
	connection = new mysql({
			host : settings.mysql.host,
			user : settings.mysql.user,
			password : settings.mysql.password,
			db : settings.mysql.database
	  });
	  connect = connection.connect(function(err) {
		    if(err) {
		    	logger.info('error when connecting to db:', err);
		        exports.connection= null;
		      }else{
		    	  exports.connection=connection;
		      }
		    });
	  
	  connection.on('error', function(err) {
	    console.log('db error', err);
	    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
	      
	    	logger.info("reconnecting mariasql...")
	      exports.connection= null;
	    } else {
	    	logger.info('error while getting conn object')
	    	exports.connection= null;
	      throw err;
	    }
	  });
}
