//create app user
var connections = require("../Util/dbConnection.js"),
    commonConstants = require("../Util/commonConstants.js"),
    commonMethod = require("../Util/commonMethod.js");
var statusConstant = require("../Util/statusConstants.json");
var serviceDao = require("../Dao/serviceDao.js");
var logsDao = require("../Dao/logsDao.js");
var generalDao = require("../Dao/generalDao.js");
var multer = require('multer');
var rand = require("random-key");
var fs = require("fs");
var datetime = require("node-datetime");
var sendPNDao = require("../Dao/sendPNDao.js");
var logger = require('../logging/logger.js');
var encryptionMob = require("../Util/encryptionMob.js");
var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './banners');
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now());
    }
});

var upload = multer({ storage: storage }).single('file');

exports.insertData = function(req, fn) {
    try {
        var connection = connections.connection;
        var reqMethod = req.method;
        var tokReq = {
            token: req.headers.token,
            userId: req.headers.userid,
            method: req.method
        }
        if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
            tokReq['customerApp'] = req.body.customerApp;
            tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
        }
        var request = req;
        if (req.files != undefined) {
            request = req;
            console.log("IOOOOOOOOOOO ", request.body)
        }




        var req = req.body;
        if (req.fromApp != undefined && req.fromApp == true) {
            tokReq['fromApp'] = req.fromApp;
        }
        console.log("IOIOIO ", tokReq)
        commonMethod.verifyUser(tokReq, function(data, obj) {
            if (data == statusConstant.s103) {
			/** Group LIMTI**/
	/*		if(req.tableName == "ambulance_master"){
            		connection.query("select count(*) from ambulance_master where group_id="+req.params.group_id,function(err,res){
            			if(!err){
            				connection.query("select group_limit from group_master where group_id="+req.params.group_id,function(erG,resG){
            					if(!erG){
            						if(resG!=undefined){
            							if(resG.length>0){
            								if(resG[0].group_limit < res[0]['count(*)']){
            									fn("exceeded");
            								}
            							}
            						}
            					}
            				})
            			}
            		})
            	}*/
/** end**/
                if (req.tableName == statusConstant.TABLE_USER_APP_ROLES) {
                    if (req.userRole != undefined && req.userRole.length > 0) {
                        for (var i = 0; i < req.userRole.length; i++) {
                            var obj = req.userRole[i];
                            obj.userId = req.roleUserId;
                            logsDao.insertAppUserRoles(obj);
                        }
                        fn(statusConstant.SAVE_SUCCESS);
                    }
                } else if (req.tableName == statusConstant.TABLE_ACCESS_ROLES) {
                    if (req.accessRole != undefined && req.accessRole.length > 0) {
                        for (var i = 0; i < req.accessRole.length; i++) {
                            var obj = req.accessRole[i];
                            obj.roleId = req.roleId;
                            logsDao.insertAccessRoles(obj);
                        }
                        fn(statusConstant.SAVE_SUCCESS);
                    }
                } else {
                    // console.log("pppppppppppppppppppppp ", request.body)
                    if (request.files != undefined) {
                        //console.log("IOIPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP",request.body);
                        var JSONDATA = request.body;
			 var jsondata = JSONDATA.jsondata;
                        jsondata = (typeof jsondata == "string" ? JSON.parse(jsondata) : jsondata);
                       /* if(jsondata.tableName == "ambulance_master"){
                    		connection.query("select count(*) from ambulance_master where group_id="+jsondata.params.group_id,function(err,res){
                    			if(!err){
                    				connection.query("select group_limit from group_master where group_id="+jsondata.params.group_id,function(erG,resG){
                    					if(!erG){
                    						if(resG!=undefined){
                    							if(resG.length>0){
                    								if(resG[0].group_limit < res[0]['count(*)']){
                    									fn("exceeded");
                    								}
                    							}
                    						}
                    					}
                    				})
                    			}
                    		})
                    	}*/
                        upload(request, fn, function(err) {
                            fs.readFile(request.files.file.path, function(err, data) {
                                var dirname = "";
                                var fileName = "";
                                var jsondata = JSONDATA.jsondata;
                                console.log("OOPO jsondata ", jsondata, typeof jsondata);
                                jsondata = (typeof jsondata == "string" ? JSON.parse(jsondata) : jsondata);
                                console.log("PSTET ", jsondata, jsondata['tableName']);
                                var emergencyTypes = "";
                                if (jsondata['tableName'] == "ambulance_master") {
                                    dirname = commonConstants.ambulanceImageUploadPath;
                                    fileName = "ambulance-" + Date.now();
                                    emergencyTypes = jsondata.params.emergency_type;
                                    jsondata.systemParams = { amb_id: "" };
                                    console.log("OOOOOOOOOOOOO ", emergencyTypes, jsondata.systemParams);
                                    
                                } else if (jsondata['tableName'] == "driver_master") {
                                    dirname = commonConstants.driverImageUploadPath;
                                    fileName = "driver-" + Date.now();
                                } else if (jsondata['tableName'] == "customer_master") {
                                    dirname = commonConstants.patientImageUploadPath;
                                    fileName = "customer" + Date.now();
                                }

                                var newPath = dirname + fileName;
                                if (request.files.file.type) {
                                    newPath += "." + request.files.file.type.split("image/")[1];
                                    fileName += "." + request.files.file.type.split("image/")[1];
                                }
                                var image_url = "";
                                if (jsondata['tableName'] == "ambulance_master") {
                                    image_url = commonConstants.ambulanceFilePath + fileName;
                                } else if (jsondata['tableName'] == "driver_master") {
                                    image_url = commonConstants.driverFilePath + fileName;
                                } else if (jsondata['tableName'] == "customer_master") {
                                    image_url = commonConstants.patientFilePath + fileName;
                                }
                                fs.writeFile(newPath, data, function(err) {
                                    if (err) {
                                        console.log("ERRR ", err)
                                        fn("err", err);
                                    } else {
                                        var keys = [];
                                        //  console.log("OOPO jsondata ", JSONDATA);
                                        var jsondata = JSONDATA.jsondata;
                                        console.log("OOPO jsondata ", jsondata, typeof jsondata);
                                        jsondata = (typeof jsondata == "string" ? JSON.parse(jsondata) : jsondata);
                                        console.log("PSTET ", jsondata, jsondata['tableName']);

                                        var query = "insert into " + jsondata.tableName + "(";

                                        var randomValue = "";
                                        var randomKey = "";
                                        var ambulanceId = "";
                                        if (jsondata.tableName == "ambulance_master") {
                                            jsondata.systemParams = { amb_id: "" };
                                        }
                                        if (jsondata.systemParams != undefined && jsondata.systemParams != null && Object.keys(jsondata.systemParams).length > 0) {
                                            var systemKeys = [];
                                            for (var k in jsondata.systemParams) {
                                                systemKeys.push(k);
                                            }

                                            for (var i = 0; i < systemKeys.length; i++) {
                                                randomKey += "," + systemKeys[i];
                                                if (jsondata.tableName == "ambulance_master") {
                                                    // randomKey = ",amb_id"
                                                    ambulanceId = commonMethod.getRandomUserAccessId();
                                                    randomValue += "," + ambulanceId;
                                                } else
                                                    randomValue += "," + commonMethod.getRandomUserAccessId();
                                            }


                                        }
                                        if (jsondata.tableName == statusConstant.TABLE_APPS) {
                                            randomKey = ",api_key",
                                                randomValue = ",'" + rand.generate(64) + "'";
                                        }
                                        for (var k in jsondata.params) {
                                            keys.push(k);
                                        }
                                        for (var i = 0; i < keys.length; i++) {
                                            if (i == 0)
                                                query += "file_url," + keys[i]
                                            else
                                                query += keys[i]

                                            if (i == keys.length - 1) {
                                                if (jsondata.params[keys[j]] != null && typeof jsondata.params[keys[j]] == "string" && jsondata.params[keys[j]].indexOf("'") > -1) {
                                                    jsondata.params[keys[j]] = jsondata.params[keys[j]].replace("'", "''");
                                                }
                                                query += randomKey + ") values(";
                                                for (var j = 0; j < keys.length; j++) {
                                                    if (j == 0)
                                                        query += "'" + image_url + "','" + jsondata.params[keys[j]] + "'"
                                                    else
                                                        query += "'" + jsondata.params[keys[j]] + "'"

                                                    if (j == keys.length - 1)
                                                        query += randomValue + ")";
                                                    else
                                                        query += ",";
                                                }
                                            } else
                                                query += ",";
                                        }

                                        console.log("Query in Insert Recored ", query);
                                        if (jsondata.serviceParams != undefined && Object.keys(jsondata.serviceParams).length > 0) {
                                            var serviceId = { serviceId: jsondata.serviceParams.serviceId }
                                            var service = {
                                                serviceId: jsondata.serviceParams.serviceId,
                                                tableName: jsondata.tableName
                                            }
                                            serviceDao.connectToMysql(serviceId, function(dd) {
                                                connection = dd;
                                                connection.query(query, function(err, data) {
                                                    if (err) {
                                                        fn(null, err);
                                                    } else {
                                                        fn(data, "");

                                                    }
                                                });
                                            });
                                        } else {
                                            if (jsondata.tableName == statusConstant.TABLE_SERVICE_FIELD_VALUES) {
                                                var q = "select * from " + statusConstant.TABLE_SERVICES + " where id='" + jsondata.params.serviceId + "'";
                                                connection.query(q, function(err, res) {
                                                    if (!err) {
                                                        var serviceType = res[0].service_type_id;
                                                        var serviceName = res[0].name;
                                                        var defType = "";

                                                        if (serviceType == "24" && jsondata.params.serviceFieldsId == "44") {
                                                            connection.query("select fieldValue from " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " where serviceId ='" + jsondata.params.serviceId + "' and serviceFieldsId = '43'", function(err, res) {
                                                                if (!err) {
                                                                    defType = res[0].fieldValue;
                                                                    console.log("DEFTYPE  ", defType);
                                                                    //yaml to json

                                                                    var file = './api/customApi/' + 'http_' + jsondata.params.serviceId + '.json';
                                                                    var obj = jsondata.params.fieldValue;
                                                                    if (defType == "YAML") {
                                                                        var YAML = require('yamljs');
                                                                        var nativeObject = YAML.parse(jsondata.params.fieldValue);
                                                                        obj = JSON.stringify(nativeObject, null, 4);
                                                                        console.log("NATIVE OBJECT ", obj);
                                                                    }
                                                                    fs.writeFile(file, obj, function(err) {
                                                                        if (!err) {
                                                                            connection.query(query, function(err, data) {
                                                                                if (err) {
                                                                                    if (err.code == 1062) {
                                                                                        fn(statusConstant.s121, err);
                                                                                    } else
                                                                                        fn(null, err);
                                                                                } else {
                                                                                    file = commonConstants.serverUrl + commonConstants.serverPort + "/api/customApi/http_" + jsondata.params.serviceId + ".json";
                                                                                    connection.query("update " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " set fieldValue = '" + file + "' where serviceId ='" + jsondata.params.serviceId + "' and serviceFieldsId = '" + jsondata.params.serviceFieldsId + "'", function(err, obj) {
                                                                                        if (!err) {
                                                                                            fn(data, "");
                                                                                        } else {
                                                                                            fn(null, err);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        } else {
                                                                            console.error(err);
                                                                            fn(null, err);
                                                                        }
                                                                    })

                                                                }
                                                            });

                                                        }

                                                    }
                                                });
                                            } else {
						 if(jsondata.tableName == "ambulance_master"){
                                             		connection.query("select count(*) from ambulance_master where group_id="+jsondata.params.group_id,function(err,res){
                                             			if(!err){
                                             				connection.query("select group_limit from group_master where group_id="+jsondata.params.group_id,function(erG,resG){
                                             					if(!erG){
                                             						if(resG!=undefined){
                                             							if(resG.length>0){
console.log("GROUP LIMIT ",resG[0].group_limit, "AMB",res[0]['count(*)']);
                                             								if((parseInt(resG[0].group_limit) < res[0]['count(*)']) || (parseInt(resG[0].group_limit) == res[0]['count(*)'])){
                                             									fn("exceeded");
                                             								}else{
                                             									 connection.query(query, function(err, data) {
                                                                                     if (err) {
                                 console.log("ERR ",err);
                                                                                         if (err.code == 1062 ) {
                                                                                             fn('DUP', err);
                                                                                         } else
                                                                                             fn(null, err);
                                                                                     } else {
                                                                                         if (jsondata.tableName == "ambulance_master") {
                                                                                             console.log(ambulanceId, " hhhhhhhhhhhhhhh ambulanceId")
                                                                                             var ambEmerg = {
                                                                                                 ambulance_id: ambulanceId,
                                                                                                 emergencyAttributes: emergencyTypes
                                                                                             }
                                                                                             generalDao.insertAmbulanceEmergency(ambEmerg, function(err1, res1) {
                                                                                                 if (err1 == "success") {
                                                                                                     fn(data, "");
                                                                                                 } else {
                                                                                                     console.log("ERROR R *** ", err1);
                                                                                                     fn("err", obj);
                                                                                                 }
                                                                                             })
                                                                                         } else
                                                                                             fn(data, "");
                                                                                     }
                                                                                 });
                                             								}
                                             							}
                                             						}
                                             					}
                                             				})
                                             			}
                                             		})
                                             	}else{
                                                connection.query(query, function(err, data) {
                                                    if (err) {
console.log("ERR ",err);
                                                        if (err.code == 1062 ) {
                                                            fn('DUP', err);
                                                        } else
                                                            fn(null, err);
                                                    } else {
                                                        if (jsondata.tableName == "ambulance_master") {
                                                            console.log(ambulanceId, " hhhhhhhhhhhhhhh ambulanceId")
                                                            var ambEmerg = {
                                                                ambulance_id: ambulanceId,
                                                                emergencyAttributes: emergencyTypes
                                                            }
                                                            generalDao.insertAmbulanceEmergency(ambEmerg, function(err1, res1) {
                                                                if (err1 == "success") {
                                                                    fn(data, "");
                                                                } else {
                                                                    console.log("ERROR R *** ", err1);
                                                                    fn("err", obj);
                                                                }
                                                            })
                                                        } else
                                                            fn(data, "");
                                                    }
                                                });
						}
                                            }
                                        }
                                    }



                                });

                            });
                        });




                    } else {
                        var keys = [];
                        var query = "insert into " + req.tableName + "(";

                        var randomValue = "";
                        var randomKey = "";
                        var bookingId = 0;
			if(req.tableName=="emergency_booking")
                                bookingId= parseInt(req.params['booking_id'].toString());
                        console.log("BOOKING ID ",bookingId);

                        if (req.systemParams != undefined && req.systemParams != null && Object.keys(req.systemParams).length > 0) {
                            var systemKeys = [];
                            for (var k in req.systemParams) {
                                systemKeys.push(k);
                            }

                            for (var i = 0; i < systemKeys.length; i++) {
                                console.log("YSSYY ", systemKeys[i]);

                                /*if (systemKeys[i] == "booking_id") {
                                    randomKey += "," + systemKeys[i];
                                    //  var dateMonth = datetime.create().format('dm');
                                    //  console.log("DATE MONTH ", dateMonth)
                                    bookingId = commonMethod.getRandomBookingId();
                                    randomValue += "," + bookingId;

                                    // console.log("RANDONF ", randomValue)
                                } else {*/
                                    randomKey += "," + systemKeys[i];
                                    randomValue += "," + commonMethod.getRandomUserAccessId();

                                //}
                            }


                        }
                        if (req.tableName == statusConstant.TABLE_APPS) {
                            randomKey = ",api_key",
                                randomValue = ",'" + rand.generate(64) + "'";
                        }
                        for (var k in req.params) {
                            keys.push(k);
                        }
                        for (var i = 0; i < keys.length; i++) {
                            query += keys[i]
                            if (i == keys.length - 1) {
                                if (req.params[keys[j]] != null && typeof req.params[keys[j]] == "string" && req.params[keys[j]].indexOf("'") > -1) {
                                    req.params[keys[j]] = req.params[keys[j]].replace("'", "''");
                                }
                                query += randomKey + ") values(";
                                for (var j = 0; j < keys.length; j++) {
                                    query += "'" + req.params[keys[j]] + "'"
                                    if (j == keys.length - 1)
                                        query += randomValue + ")";
                                    else
                                        query += ",";
                                }
                            } else
                                query += ",";
                        }

                        console.log("Query in Insert Recored ", query);
                        if (req.serviceParams != undefined && Object.keys(req.serviceParams).length > 0) {
                            var serviceId = { serviceId: req.serviceParams.serviceId }
                            var service = {
                                serviceId: req.serviceParams.serviceId,
                                tableName: req.tableName
                            }
                            serviceDao.connectToMysql(serviceId, function(dd) {
                                connection = dd;
                                connection.query(query, function(err, data) {
                                    if (err) {
                                        fn(null, err);
                                    } else {
                                        fn(data, "");

                                    }
                                });
                            });
                        } else {
                            if (req.tableName == statusConstant.TABLE_SERVICE_FIELD_VALUES) {
                                var q = "select * from " + statusConstant.TABLE_SERVICES + " where id='" + req.params.serviceId + "'";
                                connection.query(q, function(err, res) {
                                    if (!err) {
                                        var serviceType = res[0].service_type_id;
                                        var serviceName = res[0].name;
                                        var defType = "";

                                        if (serviceType == "24" && req.params.serviceFieldsId == "44") {
                                            connection.query("select fieldValue from " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " where serviceId ='" + req.params.serviceId + "' and serviceFieldsId = '43'", function(err, res) {
                                                if (!err) {
                                                    defType = res[0].fieldValue;
                                                    console.log("DEFTYPE  ", defType);
                                                    //yaml to json

                                                    var file = './api/customApi/' + 'http_' + req.params.serviceId + '.json';
                                                    var obj = req.params.fieldValue;
                                                    if (defType == "YAML") {
                                                        var YAML = require('yamljs');
                                                        var nativeObject = YAML.parse(req.params.fieldValue);
                                                        obj = JSON.stringify(nativeObject, null, 4);
                                                        console.log("NATIVE OBJECT ", obj);
                                                    }
                                                    fs.writeFile(file, obj, function(err) {
                                                        if (!err) {
                                                            connection.query(query, function(err, data) {
                                                                if (err) {
		console.log("POPO ",err);
                                                                    if (err.code == 1062) {
                                                                        fn(statusConstant.s121, err);
                                                                    } else
                                                                        fn(null, err);
                                                                } else {
                                                                    file = commonConstants.serverUrl + commonConstants.serverPort + "/api/customApi/http_" + req.params.serviceId + ".json";
                                                                    connection.query("update " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " set fieldValue = '" + file + "' where serviceId ='" + req.params.serviceId + "' and serviceFieldsId = '" + req.params.serviceFieldsId + "'", function(err, obj) {
                                                                        if (!err) {
                                                                            fn(data, "");
                                                                        } else {
                                                                            fn(null, err);
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            console.error(err);
                                                            fn(null, err);
                                                        }
                                                    })

                                                }
                                            });

                                        }

                                    }
                                });
                            } else {
				logger.info("QUERY BOOKING ",query);
                                connection.query(query, function(err, data) {
console.log("POPO ",err);
                                    if (err) {
					logger.info("ERROR CREATE BOOKING ",err);
                                        if (err.code == 1062 ) {
                                            fn('DUP', err);
                                        } else
                                            fn(null, err);
                                    }else{
                                    	if(req.tableName == "group_master"){
											var emergencyTypes = "";
											emergencyTypes = req.params.emergency_type;
											console.log("EMERGENCY IN GROUP ",emergencyTypes,"INSERT ID ",data.info['insertId'],data)     ;
											var groupEmerges = { 
													group_id: data.info['insertId'],
													emergencyAttributes: emergencyTypes
											}
											generalDao.insertGroupEmergency(groupEmerges, function(err1, res1) {
												if (err1 == "success") {
													fn(data, "");
												} else {
													console.log("ERROR R *** ", err1);
													fn("err", obj);
												}
											});
										}else if(req.tableName == "faq" || req.tableName== "emergency_types" || req.tableName == "terms_conditions" || req.tableName== "privacy_policy" || req.tableName== "come_to_know_by" || req.tableName == "country_list"){
											fn(data, "");
											var topic = (req.tableName=="faq")?(req.params.app_id==0?"faq-amb":req.tableName):req.tableName;
logger.info("TOPIC NAME",topic);
											var data = topic+" has been updated";
											var message = "update the "+topic+" list";
											sendPNDao.sendPNToTopic(topic,data,message);
										}else{
											data.bookingId = bookingId;
											fn(data, "");
										}
                                    }
                                    
									
                                });
                            }
                        }
                    }
                }

            } else if (data == statusConstant.s108) {
                fn(statusConstant.s106);
            } else if (data == statusConstant.s104 || data == statusConstant.s105) {
                fn(data, obj);
            } else if (data == statusConstant.s129) {
                fn(data, obj);
            }
        });

    } catch (err) {
        console.log(err);
    }

}


exports.getDataByField = function(req, fn) {
    var connection = connections.connection;
//    console.log("REQJKJK ", req.body)
  //  console.log(req.method + "    method name");
    var reqMethod = req.method;
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
        tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
    }
    var req = req.body;
    if (req.fromApp != undefined) {
        tokReq['fromApp'] = req.fromApp;
    }

    commonMethod.verifyUser(tokReq, function(data, obj) {
        if (data == statusConstant.s103 && obj.length > 0) {
            var query = "select * from " + req.tableName;
            var paramsExists = false;
            if (req.params != undefined && req.params != null && req.params != "" && Object.keys(req.params).length > 0) {
                paramsExists = true;
                query += " where ";
                var keys = [];
                for (var k in req.params) {
                    keys.push(k);
                }
                for (var i = 0; i < keys.length; i++) {
                    query += keys[i] + " = '" + req.params[keys[i]] + "'";

                    if (i == keys.length - 1) {

                        query += "";
                    } else
                        query += " and ";
                }
            } else {
                query += "";
            }
            if (req.dateFilter != undefined && req.dateFilter != null && req.dateFilter != "" && Object.keys(req.dateFilter).length > 0) {
                var dateFilter = req.dateFilter;
                if (paramsExists) {
                    query += " and ";
                } else {
                    query += " where ";
                }
                if (dateFilter.startDate != undefined && dateFilter.startDate != "" && dateFilter.endDate != undefined && dateFilter.endDate != "") {
                    query += dateFilter['fieldName'] + " between '" + dateFilter.startDate + "' and '" + dateFilter.endDate + "'";
                } else if (dateFilter.startDate != undefined && dateFilter.startDate != "")
                    query += dateFilter['fieldName'] + " like '" + dateFilter.startDate + "%' ";
            } else {
                query += "";
            }

            if (req.groupByParams != undefined && req.groupByParams != null && Object.keys(req.groupByParams).length > 0) {
                query += " group by ";
                var keys = [];
                for (var k in req.groupByParams) {
                    keys.push(k);
                }
                for (var i = 0; i < keys.length; i++) {
                    query += keys[i]

                    if (i == keys.length - 1) {
                        query += "";
                    } else
                        query += ",";
                }
            } else {
                query += "";
            }

            if (req.orderByParams != undefined && req.orderByParams != null && Object.keys(req.orderByParams).length > 0) {
                query += " order by ";
                var keys = [];
                for (var k in req.orderByParams) {
                    keys.push(k);
                }
                for (var i = 0; i < keys.length; i++) {
                    query += keys[i];

                    if (i == keys.length - 1) {
                        query += " desc";
                    } else
                        query += ",";
                }
            } else {
                query += "";
            }
            if (req.limit != undefined && req.limit != null && req.limit != "") {
                query += " limit " + req.limit;
            }
            console.log("QUERY ", query);
            if (req.serviceParams != undefined && Object.keys(req.serviceParams).length > 0) {
                var serviceId = { serviceId: req.serviceParams.serviceId }
                var service = {
                        serviceId: req.serviceParams.serviceId,
                        tableName: req.tableName
                    }
                    //logsDao.insertServiceTable(service);

                serviceDao.connectToMysql(serviceId, function(dd) {
    //                console.log("KHHHJ ", dd);
                    connection = dd;

                    console.log("DDDDDDDDDDDDDDDDDDDDDDD" + dd);
      //              console.log("query" + query)
                    connection.query(query, function(err, data) {
                        if (err) {
                            fn(statusConstant.s122, err);
                        } else {
                            connection.query("desc " + service.tableName, function(err, res) {
                                if (!err) {
                                    var response = {
                                        'tableDesc': res,
                                        'data': data
                                    }
                            //        console.log("not errrr")
                                    fn(statusConstant.s103, response);
                                } else {
                              //      console.log("nwwwwwwwwwwwwwwwwwwwwwwwwrrrr")
                                    fn(statusConstant.s122, err);
                                }
                            })
                        }
                    });
                });
            } else {
                connection.query(query, function(err, data) {
                    if (err) {
                        fn(statusConstant.s122, err);
                    } else {

                        fn(statusConstant.s103, data);
                    }
                });
            }
        } else if (data == statusConstant.s108) {
            fn(statusConstant.s106, obj);
        } else if (data == statusConstant.s104 || data == statusConstant.s105) {
            fn(data, obj);
        } else if (data == statusConstant.s129) {
            fn(data, obj);
        }
    });
}

exports.deleteData = function(req, fn) {
    var connection = connections.connection;
    var reqMethod = req.method;
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
        tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
    }
    var req = req.body;
    if (req.fromApp != undefined) {
        tokReq['fromApp'] = req.fromApp;
    }
    commonMethod.verifyUser(tokReq, function(data, obj) {
        if (data == statusConstant.s103) {

            var query = "delete from " + req.tableName
            if (req.params != undefined && req.params != null && Object.keys(req.params).length > 0) {

                query += " where ";
                var keys = [];
                for (var k in req.params) {
                    keys.push(k);
                }
                for (var i = 0; i < keys.length; i++) {
                    query += keys[i] + " = '" + req.params[keys[i]] + "'";

                    if (i == keys.length - 1) {
                        query += "";
                    } else
                        query += " and ";
                }
            } else {
                query += "";
            }
            if (req.serviceParams != undefined && Object.keys(req.serviceParams).length > 0) {
                var serviceId = { serviceId: req.serviceParams.serviceId }
                var service = {
                    serviceId: req.serviceParams.serviceId,
                    tableName: req.tableName
                }


                serviceDao.connectToMysql(serviceId, function(dd) {
                    connection = dd;
                    connection.query(query, function(err, data) {
                        if (err) {
                            fn(null, err);
                        } else {
                            fn(data, "");
                        }
                    });
                });
            } else {
               
                    if (req.tableName == "ambulance_master") {
                        console.log(" hhhhhhhhhhhhhhh ambulanceId",req, req.emergency_type)
                        var emergencyTypes = req.emergency_type;
                        var ambEmerg = {
                            ambulance_id: req.params.amb_id,
                            emergencyAttributes: emergencyTypes
                        }
                        generalDao.deleteAmbulanceEmergency(ambEmerg, function(err1, res1) {
                            console.log("ERRRR ", err1)
                            if (err1 == "success") {
                                deleteD();
                            } else {
                                console.log("ERROR R *** ", err1);
                                fn("err", obj);
                            }
                        })
                    }else if (req.tableName == "group_master") {
                        var emergencyTypes = req.emergency_type;
                        var groupEmerg = {
                            group_id: req.params.group_id,
                            emergencyAttributes: emergencyTypes
                        }
                        generalDao.deleteAmbulanceEmergency(groupEmerg, function(err1, res1) {
                            console.log("ERRRR ", err1)
                            if (err1 == "success") {
                                deleteD();
                            } else {
                                console.log("ERROR R *** ", err1);
                                fn("err", obj);
                           }
                        })
                    }  else
                        deleteD();
                

                function deleteD() {
                    connection.query(query, function(err, data) {
                        if (err) {
                            fn(null, err);
                        } else {
                            if (req.params != undefined && Object.keys(req.params).length > 0 && req.tableName == "Apps" && req.params.appId != undefined) {
                                logsDao.deleteAppUserRolesByAppId(req.params.appId);
                            }

                            if (req.tableName == statusConstant.TABLE_BACKEND_USERS) {
                                logsDao.deleteAppUserRolesByUserId(req.params.userId);
                            }

                            if (req.tableName == statusConstant.TABLE_ROLES) {
                                logsDao.deleteAccessRolesByRoleId(req.params.roleId);
                            }
                            if (req.tableName == statusConstant.TABLE_SERVICES) {
                                console.log("IN DELTETING ", req.params);
                                if (req.params != undefined && Object.keys(req.params).length > 0) {
                                    console.log("IN DELTETING ");
                                    var servicejsonFile = "./api/customApi/http_" + req.params.id + ".json";
                                    if (fs.existsSync(servicejsonFile)) {
                                        var testFiles = fs.unlinkSync(servicejsonFile, "utf8");
                                    }
                                }
                                serviceDao.deleteServiceFields(req.params.id);

                            }else if(req.tableName == "faq" || req.tableName== "emergency_types" || req.tableName == "terms_conditions" || req.tableName== "privacy_policy" || req.tableName== "come_to_know_by" || req.tableName == "country_list"){
								fn(data, "");
								var topic = (req.tableName=="faq")?(req.params.app_id==0?"faq-amb":req.tableName):req.tableName;
								var data = topic+" has been updated";
								var message = "update the "+topic+" list";
								sendPNDao.sendPNToTopic(topic,data,message);
							} else
                                fn(data, "");
                            /*if (req.tableName == "ambulance_master") {
                                console.log(" hhhhhhhhhhhhhhh ambulanceId", req.emergency_type)
                                var emergencyTypes = req.emergency_type;
                                var ambEmerg = {
                                    ambulance_id: req.params.amb_id,
                                    emergencyAttributes: emergencyTypes
                                }
                                generalDao.deleteAmbulanceEmergency(ambEmerg, function(err1, res1) {
                                    console.log("ERRRR ", err1)
                                    if (err1 == "success") {
                                        fn(data, "");
                                    } else {
                                        console.log("ERROR R *** ", err1);
                                        fn("err", obj);
                                    }
                                })
                            }*/
                        }
                    });
                }
            }
        } else if (data == statusConstant.s108) {
            fn(statusConstant.s106, obj);
        } else if (data == statusConstant.s104 || data == statusConstant.s105) {
            fn(data, obj);
        } else if (data == statusConstant.s129) {
            fn(data, obj);
        }
    });
}


exports.updateData = function(req, fn) {
    var connection = connections.connection;
    var reqMethod = req.method;
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
        tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
    }
    if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
        tokReq['ambulanceApp'] = req.body.ambulanceApp;
    }
    var request = req;
    var req = null;
    if (request.body.jsondata != undefined) {
        req = JSON.parse(request.body.jsondata);
    } else {
        req = request.body;
    }

    if (req.fromApp != undefined) {
        tokReq['fromApp'] = req.fromApp;
    }
    commonMethod.verifyUser(tokReq, function(data, obj) {
        if (data == statusConstant.s103) {
            if (req.tableName == statusConstant.TABLE_USER_APP_ROLES) {
                if (req.userRole != undefined && req.userRole.length > 0) {
                    logsDao.deleteAppUserRolesByUserId(req.conditionalParams.userId);
                    for (var i = 0; i < req.userRole.length; i++) {
                        var obj = req.userRole[i];
                        obj.userId = req.conditionalParams.userId;
                        logsDao.insertAppUserRoles(obj);
                    }
                    fn(statusConstant.SAVE_SUCCESS, "");
                }
            } else if (req.tableName == statusConstant.TABLE_ACCESS_ROLES) {
                if (req.accessRole != undefined && req.accessRole.length > 0) {
                    logsDao.deleteAccessRolesByRoleId(req.conditionalParams.roleId);
                    for (var i = 0; i < req.accessRole.length; i++) {
                        var obj = req.accessRole[i];
                        obj.roleId = req.conditionalParams.roleId;
                        logsDao.insertAccessRoles(obj);
                    }
                }
                fn(statusConstant.SAVE_SUCCESS, "");
            } else {
                /** unlinking files **/
                console.log("KLKLK ", request.files);
                if (request.files != undefined && request.files.file != undefined) {
                    var jsondata = request.body.jsondata;
                    jsondata = JSON.parse(jsondata);
                    console.log("IIIIIIIIIIII ", jsondata);
                    var removeFile = "";
                    var filePath = "";
                    if (jsondata['tableName'] == "ambulance_master") {
                        removeFile = jsondata.params.file_url.split("/ambulances/")[1];
                        filePath = commonConstants.ambulanceImageUploadPath + "ambulance-1474547132423.png"

                    } else if (jsondata['tableName'] == "driver_master") {
                        if (jsondata.params.file_url) {
                            removeFile = jsondata.params.file_url.split("/drivers/")[1];
                            filePath = commonConstants.driverImageUploadPath + removeFile
                        }
                    } else if (jsondata['tableName'] == "customer_master") {
                        if (jsondata.params.file_url) {
                            removeFile = jsondata.params.file_url.split("/patients/")[1];
                            filePath = commonConstants.patientImageUploadPath + removeFile

                        }
                    }
                    console.log("FIEL ", "/home" + filePath);
                    //fs.unlinkSync(filePath);
                    // fs.unlink(filePath, function(err) {
                    //   console.log("II ",err);
                    //  if (!err) {
                    upload(request, fn, function(err) {
                        fs.readFile(request.files.file.path, function(err, data) {
                            var dirname = "";
                            var fileName = "";
                            if (jsondata['tableName'] == "ambulance_master") {
                                dirname = commonConstants.ambulanceImageUploadPath;
                                fileName = "ambulance-" + Date.now();
                            } else if (jsondata['tableName'] == "driver_master") {
                                dirname = commonConstants.driverImageUploadPath;
                                fileName = "driver-" + Date.now();
                            } else if (jsondata['tableName'] == "customer_master") {
                                dirname = commonConstants.patientImageUploadPath;
                                fileName = "customer" + Date.now();
                            }

                            var newPath = dirname + fileName;
                            if (request.files.file.type) {
                                newPath += "." + request.files.file.type.split("image/")[1];
                                fileName += "." + request.files.file.type.split("image/")[1];
                            }
                            var image_url = "";
                            if (jsondata['tableName'] == "ambulance_master") {
                                image_url = commonConstants.ambulanceFilePath + fileName;
                            } else if (jsondata['tableName'] == "driver_master") {
                                image_url = commonConstants.driverFilePath + fileName;
                            } else if (jsondata['tableName'] == "customer_master") {
                                image_url = commonConstants.patientFilePath + fileName;
                            }
                            jsondata['params']['file_url'] = image_url;

                            fs.writeFile(newPath, data, function(err) {
                                if (err) {
                                    fn("err", err);
                                } else {
                                    var keys = [];
                                    var randomValue = "";
                                    var randomKey = "";
                                    var query = "update " + jsondata.tableName + " set ";

                                    for (var k in jsondata.params) {
                                        keys.push(k);
                                    }
                                    for (var i = 0; i < keys.length; i++) {
                                        if (keys[i] == 'api_key') {
                                            query += "api_key = '" + rand.generate(64) + "'";
                                        } else {
                                            if (jsondata.params[keys[i]] != null && typeof jsondata.params[keys[i]] == "string" && jsondata.params[keys[i]].indexOf("'") > -1) {
                                                jsondata.params[keys[i]] = jsondata.params[keys[i]].replace("'", "''");
                                            }
                                            query += keys[i] + "='" + jsondata.params[keys[i]] + "'";
                                        }

                                        if (i == keys.length - 1) {
                                            query += "";
                                        } else {
                                            query += ",";

                                        }

                                    }
                                    /*}*/
                                    if (jsondata.conditionalParams != undefined && jsondata.conditionalParams != null && Object.keys(jsondata.conditionalParams).length > 0) {
                                        query += " where ";
                                        var conditionalKeys = [];
                                        for (var j in jsondata.conditionalParams) {
                                            conditionalKeys.push(j);
                                        }
                                        for (var l = 0; l < conditionalKeys.length; l++) {
                                            query += conditionalKeys[l] + "='" + jsondata.conditionalParams[conditionalKeys[l]] + "'";
                                            if (l == conditionalKeys.length - 1) {
                                                query += "";
                                            } else {
                                                query += "and "
                                            }
                                        }
                                    }
                                    if (jsondata.serviceParams != undefined && Object.keys(jsondata.serviceParams).length > 0) {
                                        var serviceId = { serviceId: jsondata.serviceParams.serviceId }
                                        var service = {
                                            serviceId: jsondata.serviceParams.serviceId,
                                            tableName: jsondata.tableName
                                        }
                                        serviceDao.connectToMysql(serviceId, function(dd) {
                                            connection = dd;
                                            connection.query(query, function(err, data) {
                                                if (err) {
                                                    fn("null", err);
                                                } else {
                                                    fn(data, "");
                                                }
                                            });
                                        });
                                    } else {
                                        if (jsondata.tableName == statusConstant.TABLE_SERVICE_FIELD_VALUES) {
                                            var q = "select * from " + statusConstant.TABLE_SERVICES + " where id='" + jsondata.params.serviceId + "'";
                                            connection.query(q, function(err, res) {
                                                if (!err) {
                                                    var serviceType = res[0].service_type_id;
                                                    var serviceName = res[0].name;
                                                    var defType = "";

                                                    if (serviceType == "24" && jsondata.params.serviceFieldsId == "44") {
                                                        connection.query("select fieldValue from " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " where serviceId ='" + jsondata.params.serviceId + "' and serviceFieldsId = '43'", function(err, res) {
                                                            if (!err) {
                                                                defType = res[0].fieldValue;
                                                                //yaml to json
                                                                var fs = require('fs');

                                                                var file = "./api/customApi/http_" + jsondata.params.serviceId + ".json";
                                                                console.log("FILE ", file);
                                                                var obj = req.params.fieldValue;
                                                                if (defType == "YAML") {
                                                                    var YAML = require('yamljs');
                                                                    var nativeObject = YAML.parse(jsondata.params.fieldValue);
                                                                    obj = JSON.stringify(nativeObject, null, 4);
                                                                    console.log("NATIVE OBJECT ", obj);
                                                                }
                                                                /*if(fs.existsSync(file)){
                                                                    
                                                                }*/
                                                                fs.writeFile(file, obj, function(err) {
                                                                    if (!err) {
                                                                        connection.query(query, function(err, data) {
console.log("POPO ",err);
                                                                            if (err) {
                                                                                if (err.code == 1062) {
                                                                                    fn('DUP', err);
                                                                                } else
                                                                                    fn(null, err);
                                                                            } else {
                                                                                file = commonConstants.serverUrl + commonConstants.serverPort + "/api/customApi/http_" + req.params.serviceId + ".json";
                                                                                connection.query("update " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " set fieldValue = '" + file + "' where serviceId ='" + req.params.serviceId + "' and serviceFieldsId = '" + req.params.serviceFieldsId + "'", function(err, obj) {
                                                                                    if (!err) {
                                                                                        fn(data, "");
                                                                                    } else {
                                                                                        fn(null, err);
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    } else {
                                                                        console.error(err);
                                                                        fn(null, err);
                                                                    }
                                                                });


                                                            } else {
                                                                fn(null, err);
                                                            }
                                                        });

                                                    } else {
                                                        connection.query(query, function(err, data) {
                                                            console.log("IN ELSE ", data);
console.log("POPO ",err);
                                                            if (err) {
                                                                if (err.code == 1062) {
                                                                    fn('DUP', err);
                                                                } else
                                                                    fn(null, err);
                                                            } else {
                                                                //fn(data, "");
                                                                 if (jsondata.tableName == "app_user") {
                                                                    logger.info("IN INACTIVATING ", jsondata.params.mobile);
                                                                    fn(data, "");
                                                                    if (jsondata.params.userStatus != 'Active') {
                                                                        var data = "Your account has been blocked";
                                                                        var message = "logout the app";
                                                                        sendPNDao.getDeviceTokensByEvdIMEI(jsondata.params.mobile, jsondata.params.deviceIMEI, function(devicetoken) {
                                                                            if (devicetoken != null) {
                                                                                sendPNDao.sendPN(devicetoken, data, message, "logout", jsondata.params.mobile);
                                                                            }
                                                                        })
                                                                    }
                                                                } else
                                                                    fn(data, "");
                                                            }
                                                        });
                                                    }

                                                }
                                            });
                                        } else {
                                            connection.query(query, function(err, data) {
                                                if (err) {
                                                   // fn(null, err);
						    if (err.code == 1062) {
                                                                    fn('DUP', err);
                                                                } else
                                                                    fn(null, err);

                                                } else {
                                                	if (jsondata.tableName == "driver_master") {
                                                        if (jsondata.params.is_active == 'InActive') {
                                                            logger.info("IN IN ACTIVE ");
                                                            logger.info("IN IN ACTIVE 2 ", jsondata.conditionalParams.amb_id);

                                                            sendPNDao.getDeviceTokensByAmbulanceId(jsondata.conditionalParams.amb_id, function(devicetoken, ambNumber) {
                                                                var data = "logout the driver";
                                                                var message = "logout the driver";
                                                                var type = "driver_logout";
                                                                //                              var deviceToken = [logRes[0].device_token];
                                                                var sendObj = {
                                                                    user_type: 'Ambulance',
                                                                    send_to: ambNumber
                                                                }
                                                               if (devicetoken != null) {
                                                                    var obj = {
                                                                        message: message,
                                                                        send_to: ambNumber,
                                                                        pn_type: type,
                                                                        user_type: "Ambulance"
                                                                    }
                                                                    sendPNDao.sendPN(devicetoken, data, message, type, obj);
                                                                }
                                                            });
                                                        }else {
                                                            var data = "update profile";
                                                            var message = "update profile ";
                                                            sendPNDao.getDeviceTokensByEvdIMEI(jsondata.conditionalParams.amb_id, function(devicetoken, ambNumber) {
                                                                if (devicetoken != null) {
                                                                    var obj = {
                                                                        message: "",
                                                                        send_to: ambNumber,
                                                                        pn_type: "update_ambulance_profile",
                                                                        user_type: "Ambulance"
                                                                    }
                                                                    sendPNDao.sendPN(devicetoken, data, message, "update_ambulance_profile", obj);
                                                                }
                                                            });
                                                        }
                                                    }
                                                    if (jsondata.tableName == "ambulance_master") {
                                                        //console.log(ambulanceId, " hhhhhhhhhhhhhhh ambulanceId")
                                                    	var data = "update profile";
                                                    	var message = "update profile ";
                                                    	sendPNDao.getDeviceTokensByEvdIMEI(jsondata.conditionalParams.amb_id,  function(devicetoken,ambNumber) {
                                                    		if (devicetoken != null) {
                                                    			var obj={
                                                    					message:message,
                                                    					send_to:ambNumber,
                                                    					pn_type:"update_ambulance_profile",
                                                    					user_type:"Ambulance"
                                                    			}
                                                    			sendPNDao.sendPN(devicetoken, data, message, "update_ambulance_profile", obj);
                                                    		}
                                                    	});

                                                    	if(req.params.emergency_type!=undefined){
                                                        var emergencyTypes = jsondata.params.emergency_type;
                                                        var ambEmerg = {
                                                            ambulance_id: jsondata.conditionalParams.amb_id,
                                                            emergencyAttributes: emergencyTypes
                                                        }
                                                        generalDao.deleteAmbulanceEmergency(ambEmerg, function(err1, res1) {
                                                            if (err1 == "success") {
                                                                generalDao.insertAmbulanceEmergency(ambEmerg, function(err2, res2) {
                                                                    if (err2 == "success") {
                                                                        fn(data, "");
                                                                    } else {
                                                                        console.log("ERROR R *** ", err2);
                                                                        fn("err", obj);
                                                                    }
                                                                })
                                                            } else {
                                                                console.log("ERROR R *** ", err1);
                                                                fn("err", obj);
                                                            }
                                                        })
							}else
                                            fn(data, "");
                                                    } else
                                                        fn(data, "");
                                                }
                                            });
                                        }
                                    }
                                }



                            });
                        });
                    });
                    /*} else {
                        console.log("ERRR ",err);
                        fn("err", err);
                    }*/
                    //});

                } else {
                    /** End **/
                    /*var req = req;
                    if(request.body.jsondata!=undefined){
                        req = JSON.parse(request.body.jsondata);
                    }*/
                    console.log("REQ ", req);
                    var keys = [];
                    var randomValue = "";
                    var randomKey = "";
                    console.log("BODY ", req);
                    var query = "update " + req.tableName + " set ";
                    //Resetting apiKey functionality
                    /*if (req.tableName == "Apps" && req.params.api_key!=undefined) {
                        query += "api_key = '" + rand.generate(64) + "'";
                    }else{*/
                    for (var k in req.params) {
                        keys.push(k);
                    }
                    for (var i = 0; i < keys.length; i++) {
                        if (keys[i] == 'api_key') {
                            query += "api_key = '" + rand.generate(64) + "'";
                        } else {
                            if (req.params[keys[i]] != null && typeof req.params[keys[i]] == "string" && req.params[keys[i]].indexOf("'") > -1) {
                                req.params[keys[i]] = req.params[keys[i]].replace("'", "''");
                            }
                            query += keys[i] + "='" + req.params[keys[i]] + "'";
                        }

                        if (i == keys.length - 1) {
                            query += "";
                        } else {
                            query += ",";

                        }

                    }
                    /*}*/
                    if (req.conditionalParams != undefined && req.conditionalParams != null && Object.keys(req.conditionalParams).length > 0) {
                        query += " where ";
                        var conditionalKeys = [];
                        for (var j in req.conditionalParams) {
                            conditionalKeys.push(j);
                        }
                        for (var l = 0; l < conditionalKeys.length; l++) {
                            query += conditionalKeys[l] + "='" + req.conditionalParams[conditionalKeys[l]] + "'";
                            if (l == conditionalKeys.length - 1) {
                                query += "";
                            } else {
                                query += "and "
                            }
                        }
                    }
                    if (req.serviceParams != undefined && Object.keys(req.serviceParams).length > 0) {
                        var serviceId = { serviceId: req.serviceParams.serviceId }
                        var service = {
                            serviceId: req.serviceParams.serviceId,
                            tableName: req.tableName
                        }
                        serviceDao.connectToMysql(serviceId, function(dd) {
                            connection = dd;
                            connection.query(query, function(err, data) {
                                if (err) {
                                    fn("null", err);
                                } else {
                                    fn(data, "");
                                }
                            });
                        });
                    } else {
                        if (req.tableName == statusConstant.TABLE_SERVICE_FIELD_VALUES) {
                            var q = "select * from " + statusConstant.TABLE_SERVICES + " where id='" + req.params.serviceId + "'";
                            connection.query(q, function(err, res) {
                                if (!err) {
                                    var serviceType = res[0].service_type_id;
                                    var serviceName = res[0].name;
                                    var defType = "";

                                    if (serviceType == "24" && req.params.serviceFieldsId == "44") {
                                        connection.query("select fieldValue from " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " where serviceId ='" + req.params.serviceId + "' and serviceFieldsId = '43'", function(err, res) {
                                            if (!err) {
                                                defType = res[0].fieldValue;
                                                //yaml to json
                                                var fs = require('fs');

                                                var file = "./api/customApi/http_" + req.params.serviceId + ".json";
                                                console.log("FILE ", file);
                                                var obj = req.params.fieldValue;
                                                if (defType == "YAML") {
                                                    var YAML = require('yamljs');
                                                    var nativeObject = YAML.parse(req.params.fieldValue);
                                                    obj = JSON.stringify(nativeObject, null, 4);
                                                    console.log("NATIVE OBJECT ", obj);
                                                }
                                                /*if(fs.existsSync(file)){
                                                    
                                                }*/
                                                fs.writeFile(file, obj, function(err) {
                                                    if (!err) {
                                                        connection.query(query, function(err, data) {
console.log("POPO ",err);
                                                            if (err) {
                                                                if (err.code== 1062 ) {
                                                                    fn('DUP', err);
                                                                } else
                                                                    fn(null, err);
                                                            } else {
                                                                file = commonConstants.serverUrl + commonConstants.serverPort + "/api/customApi/http_" + req.params.serviceId + ".json";
                                                                connection.query("update " + statusConstant.TABLE_SERVICE_FIELD_VALUES + " set fieldValue = '" + file + "' where serviceId ='" + req.params.serviceId + "' and serviceFieldsId = '" + req.params.serviceFieldsId + "'", function(err, obj) {
                                                                    if (!err) {
                                                                        fn(data, "");
                                                                    } else {
                                                                        fn(null, err);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        console.error(err);
                                                        fn(null, err);
                                                    }
                                                });
                                            } else {
                                                fn(null, err);
                                            }
                                        });

                                    } else {
                                        connection.query(query, function(err, data) {
                                            console.log("IN ELSE ", data);
console.log("POPO ",err);
                                            if (err) {
                                                if (err.code == 1062 ) {
                                                    fn('DUP', err);
                                                } else
                                                    fn(null, err);
                                            } else {
                                            	if (req.tableName == "driver_master") {
                                                    if (req.params.is_active == 'InActive') {
                                                        logger.info("IN IN ACTIVE ");
                                                        logger.info("IN IN ACTIVE 3", jsondata.conditionalParams.amb_id);

                                                        sendPNDao.getDeviceTokensByAmbulanceId(req.conditionalParams.amb_id, function(devicetoken, ambNumber) {
                                                            var data = "logout the driver";
                                                            var message = "logout the driver";
                                                            var type = "driver_logout";
                                                            //                              var deviceToken = [logRes[0].device_token];
                                                            var sendObj = {
                                                                user_type: 'Ambulance',
                                                                send_to: ambNumber
                                                            }
                                                            if (devicetoken != null) {
                                                                var obj = {
                                                                    message: message,
                                                                    send_to: ambNumber,
                                                                    pn_type: type,
                                                                    user_type: "Ambulance"
                                                                }
                                                                sendPNDao.sendPN(devicetoken, data, message, type, obj);
                                                            }
                                                        });
                                                    }
                                                }else if (req.tableName == "ambulance_master") {
                                                    var emergency_type = {
                                                        ambulance_id: req.body.conditionalParams.amb_id,
                                                        emergencyAttributes: req.body.params.emergency_type,
                                                        deletion: true
                                                    }
                                                    generalDao.insertAmbulanceEmergency(emergency_type, function(stat) {
                                                        if (stat == "success") {
                                                            fn(data, "");
                                                            if (req.body.params.is_active == 'InActive') {
                                                                logger.info("IN IN ACTIVE II");
                                                                sendPNDao.getDeviceTokensByEvdIMEI(req.body.conditionalParams.amb_id, function(devicetoken, ambNumber) {
                                                                    var data = "logout the driver";
                                                                    var message = "logout the driver";
                                                                    var type = "driver_logout";
                                                                    //                                                  var deviceToken = [logRes[0].device_token];
                                                                    var sendObj = {
                                                                        user_type: 'Ambulance',
                                                                        send_to: ambNumber
                                                                    }
                                                                    if (devicetoken != null) {
                                                                        var obj = {
                                                                            message: message,
                                                                            send_to: ambNumber,
                                                                            pn_type: type,
                                                                            user_type: "Ambulance"
                                                                        }
                                                                        sendPNDao.sendPN(devicetoken, data, message, type, obj);
                                                                    }
                                                                });
                                                            }else{
                                                            	var data = "update profile";
                                                                var message = "update profile ";
                                                                sendPNDao.getDeviceTokensByEvdIMEI(req.body.conditionalParams.amb_id, function(devicetoken, ambNumber) {
                                                                    if (devicetoken != null) {
                                                                        var obj = {
                                                                            message: "",
                                                                            send_to: ambNumber,
                                                                            pn_type: "update_ambulance_profile",
                                                                            user_type: "Ambulance"
                                                                        }

                                                                        sendPNDao.sendPN(devicetoken, data, message, "update_ambulance_profile", obj);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    })
                                                } else
                                                    fn(data, "");
                                            }
                                        });
                                    }

                                }
                            });
                        } else {
                        	if (req.tableName == "emergency_types" && req.params.status == 'InActive') {
//                        	    logger.info("QUERY 1641", query);

                        	    inActivateEmergencyType(req, function(data, status) {
                        	        logger.info("QUERY 1644", data+ status);
                        	        if (data == "noAmbGroup") {
                        	            console.log("noAmbGroup 1647");
					   var q = "update emergency_types set status='InActive'  where id=" + req.params.id;
logger.info("EMRGENCY_TYPES", q);
                        	            connection.query("update emergency_types set status='InActive'  where id=" + req.params.id, function(err, emeResp) {
                        	                if (err) {
                        	                    fn("err", {})

                        	                } else {
                        	                    fn("Successfully Saved", emeResp)
                        	                    var topic = req.tableName;
                        	                    var data = topic + " has been updated";
                        	                    var message = "update the " + topic + " list";
                        	                    console.log("topic--data--message");
                        	                    sendPNDao.sendPNToTopic(topic, data, message);
                        	                }
                        	            })

                        	        } else {
                        	            fn("bookingIsInProgress")
                        	        }

                        	    });

                        	} else {

                            console.log("QUERY ", query);
                            connection.query(query, function(err, data) {
                                if (err) {
                            //        fn(null, err);
					if (err.code == 1062) {
                                                                    fn('DUP', err);
                                                                } else
                                                                    fn(null, err);

                                } else {
                                    if (req.tableName == "emergency_booking") {
                                        data.updation = true;
                                    }
                                    if (req.tableName == "driver_master") {
                                        if (req.params.is_active == 'InActive') {
                                            logger.info("IN IN ACTIVE ");
                                            logger.info("IN IN ACTIVE 4 ", req);

                                            var reqJson = {};
                                            reqJson['id'] = req.conditionalParams.driver_id;
                                            reqJson['is_driver'] = true;
                                            console.log(reqJson, "TYPE OF ", typeof req);

                                            sendPNDao.getDeviceTokensByAmbulanceId(reqJson, function(devicetoken, ambNumber) {
                                                var data = "logout the driver";
                                                var message = "logout the driver";
                                                var type = "driver_logout";
                                                //                              var deviceToken = [logRes[0].device_token];
                                                var sendObj = {
                                                    user_type: 'Ambulance',
                                                    send_to: ambNumber
                                                }
                                                if (devicetoken != null) {
                                                    var obj = {
                                                        message: message,
                                                        send_to: ambNumber,
                                                        pn_type: type,
                                                        user_type: "Ambulance"
                                                    }
                                                    sendPNDao.sendPN(devicetoken, data, message, type, obj);
                                                }
                                            });
                                        }else{
                                        	
                                        }
                                    }else if (req.tableName == "ambulance_master") {
                                    	if (req.params.is_active == 'InActive') {
                                            logger.info("IN IN ACTIVE ");
					     if(req.params.emergency_type!=undefined){
                                                        var emergencyTypes = req.params.emergency_type;
                                                        var ambEmerg = {
                                                            ambulance_id: req.conditionalParams.amb_id,
                                                            emergencyAttributes: emergencyTypes
                                                        }
                                                        generalDao.deleteAmbulanceEmergency(ambEmerg, function(err1, res1) {
                                                            if (err1 == "success") {
                                                                generalDao.insertAmbulanceEmergency(ambEmerg, function(err2, res2) {
                                                                    if (err2 == "success") {
                                                                        fn(data, "");
                                                                    } else {
                                                                        console.log("ERROR R *** ", err2);
                                                                        fn("err", obj);
                                                                    }
                                                                })
                                                            } else {
                                                                console.log("ERROR R *** ", err1);
                                                                fn("err", obj);
                                                            }
                                                        })
                                                        }else
                                            fn(data, "");

                                            var reqAmb = {};
                                            reqAmb.amb_id = req.conditionalParams.amb_id;
                                            reqAmb.is_amb = true;
                                            sendPNDao.getDeviceTokensByAmbulanceId(reqAmb, function(devicetoken, ambNumber) {
                                                console.log("RESPONSE ", devicetoken);
                                                var data = "logout the ambulance";
                                                var message = "logout the ambulance";
                                                var type = "ambulance_logout";
                                                var sendObj = {
                                                    user_type: 'Ambulance',
                                                    send_to: ambNumber
                                                }
                                                if (devicetoken != null) {
                                                    var obj = {
                                                        message: message,
                                                        send_to: ambNumber,
                                                        pn_type: type,
                                                        user_type: "Ambulance"
                                                    }
                                                    sendPNDao.sendPN(devicetoken, data, message, type, obj);
                                                }
                                            });



                                        }else{
                                        console.log(" hhhhhhhhhhhhhhh ambulanceId", req.params.emergency_type)
                                        var data = "update profile";
                                        var message = "update profile ";
                                        sendPNDao.getDeviceTokensByEvdIMEI(req.conditionalParams.amb_id,  function(devicetoken,ambNumber) {
                                        	if (devicetoken != null) {
                                        		var obj={
                                        				message:message,
                                        				send_to:ambNumber,
                                        				pn_type:"update_ambulance_profile",
                                        				user_type:"Ambulance"
                                        		}

                                        		sendPNDao.sendPN(devicetoken, data, message, "update_ambulance_profile",obj);
                                        	}
                                        });
                                        

                                        if(req.params.emergency_type!=undefined){
                                        var emergencyTypes = req.params.emergency_type;
                                        var ambEmerg = {
                                            ambulance_id: req.conditionalParams.amb_id,
                                            emergencyAttributes: emergencyTypes
                                        }
                                        generalDao.deleteAmbulanceEmergency(ambEmerg, function(err1, res1) {
                                            console.log("ERRRR ", err1)
                                            if (err1 == "success") {
                                                generalDao.insertAmbulanceEmergency(ambEmerg, function(err2, res2) {
                                                    if (err2 == "success") {
                                                        fn(data, "");
                                                    } else {
                                                        console.log("ERROR R *** ", err2);
                                                        fn("err", obj);
                                                    }
                                                })
                                            } else {
                                                console.log("ERROR R *** ", err1);
                                                fn("err", obj);
                                            }
                                        })
					}else
						fn(data,"");
                                    }
                                        
                                    }else if(req.tableName == "group_master"){
										console.log(" hhhhhhhhhhhhhhh gropuId", req.params.emergency_type)
										if(req.params.emergency_type!=undefined){
											var emergencyTypes = req.params.emergency_type;
											var gropuEmerg = {
													group_id: req.conditionalParams.group_id,
													emergencyAttributes: emergencyTypes
											}
											generalDao.deleteGroupEmergency(gropuEmerg, function(err1, res1) {
												console.log("ERRRR ", err1)
												if (err1 == "success") {
													generalDao.insertGroupEmergency(gropuEmerg, function(err2, res2) {
														if (err2 == "success") {
															fn(data, "");
														} else {
															console.log("ERROR R *** ", err2);
															fn("err", obj);
														}
													})
												} else {
													console.log("ERROR R *** ", err1);
													fn("err", obj);
												}
											})
										}else
											fn(data,"");
									} else if(req.tableName == "faq" || req.tableName== "emergency_types" || req.tableName == "terms_conditions" || req.tableName== "privacy_policy" || req.tableName== "come_to_know_by" || req.tableName == "country_list"){
										fn(data, "");
										var topic = (req.tableName=="faq")?(req.params.app_id==0?"faq-amb":req.tableName):req.tableName;
										var data = topic+" has been updated";
										var message = "update the "+topic+" list";
										sendPNDao.sendPNToTopic(topic,data,message);
									}else
                                        fn(data, "");

                                }
                            });
                        }
                        }
                    }
                }
                /* end of new else*/
            }
        } else if (data == statusConstant.s108) {
            fn(statusConstant.s106);
        } else if (data == statusConstant.s104 || data == statusConstant.s105) {
            fn(data, obj);
        } else if (data == statusConstant.s129) {
            fn(data, obj);
        }

    });
}

exports.changePasswordOfAppUser = function(req, fn) {
    var connection = connections.connection;
    console.log("REQJKJK ", req.body)
    console.log(req.method + "    method name");
    var reqMethod = req.method;
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    var req = req.body;
    /* var tokReq = {
         token: req.token,
         userId: req.userId,
         method: reqMethod
     }*/
    if (req.fromApp != undefined) {
        tokReq['fromApp'] = req.fromApp;
    }

    commonMethod.verifyUser(tokReq, function(data, obj) {
        if (data == statusConstant.s103) {
            req.conditionalParams
            connection.query("select * from " + statusConstant.TABLE_APP_USER + " where generated_userID='" + req.userId + "'", function(err, res) {
                if (!err) {
                    if (res != undefined && res.length > 0) {
                        if (res[0].password == req.oldPassword) {
                            connection.query("update " + statusConstant.TABLE_APP_USER + " set password = '" + req.newPassword + "' where generated_UserID='" + req.userID + "'", function(err, data) {
                                if (!err) {
                                    fn(statusConstant.s103, data);
                                } else {
                                    fn(statusConstant.s122, err);
                                }
                            });
                        } else {
                            fn(statusConstant.s123, {});
                        }
                    } else {
                        fn(statusConstant.s124, {});
                    }
                } else {
                    fn(statusConstant.s122, err);
                }
            })
        } else if (data == statusConstant.s129) {
            fn(data, obj);
        }
    });
}

exports.changePassword = function(req, fn) {
	var connection = connections.connection;
	console.log("REQJKJK ", req.body)
	console.log(req.method + "    method name");
	var reqMethod = req.method;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	var req = req.body;
	if (req.fromApp != undefined) {
		tokReq['fromApp'] = req.fromApp;
	}

	commonMethod.verifyUser(tokReq, function(data, obj) {
		console.log(JSON.stringify(req));
		if (data == statusConstant.s103) {
			// req.conditionalParams
			console.log("select * from system_users where userId =" + tokReq.userId);
			connection.query("select * from system_users where userId =" + tokReq.userId , function(err, res) {
				console.log(res)
				if (!err) {
					if (res != undefined && res.length > 0) {
						console.log("update system_users set password = '" + req.newPassword + "' where userId='" + tokReq.userId + "'")
						if (res[0].password == req.oldPassword) {
							connection.query("update system_users set password = '" + req.newPassword + "' where userId=" + tokReq.userId, function(err, data) {
								console.log(err);
								console.log(data);
								if (!err) {
									fn(statusConstant.s103, data);
								} else {
									fn(statusConstant.s122, err);
								}
							});
						} else {
							fn(statusConstant.s123, {});
						}
					} else {
						fn(statusConstant.s124, {});
					}
				} else {
					fn(statusConstant.s122, err);
				}
			})
		} else if (data == statusConstant.s129) {
			fn(data, obj);
		}
	});
}

exports.forgotPassword = function(req,fn){
	var connection = connections.connection;
	var query = "select * from system_users where email = '"+req.body.email+"'";
	connection.query(query,function(err,res){
		if(!err){
			if(res!=undefined && res.length>0){
				//send email 
				/** send email**/
				var data= "",emailList=res[0].email,subject="Your forgotten Password",html="<p>Dear "+res[0].firstName+" "+res[0].lastName+",</p><p> As per request for password, here we are providing you the last password '"+res[0].password+"'. </p>";

				sendemail.sendEmail(data,emailList,subject,html);
				fn("success","mail sent to registered email id");
			}else{
				fn("noUser");
			}
		}else
			fn("err");
	})	

}

/*getGroupMasterList*/
exports.getGroupMasterList = function(req, fn) {
    var connection = connections.connection;
    console.log(req.method + "    method name", req.body, req.params);
    var reqMethod = req.method;
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    var req = req.body;

    if (req.fromApp != undefined) {
        tokReq['fromApp'] = req.fromApp;
    }

    commonMethod.verifyUser(tokReq, function(data, obj) {
        if (data == statusConstant.s103) {
            connection.query("select (select count(*) from ambulance_master where is_active='Active' and group_id =group_master.group_id)  as active_count, group_id,group_name,group_short_name,group_type,group_limit,group_longitude,group_address,group_city,pincode,group_state,is_active,emergency_contact,emergency_number,emergency_email,updated_date,created_date,group_latitude,geoHashCode,emergency_type from group_master  ", function(err, res) {
                if (!err) {
                    if (res != undefined && res.length > 0) {
                        fn("success", res)
                    } else {

                        fn(statusConstant.s124, {});
                    }
                } else {

                    fn(statusConstant.s122, err);
                }
            })
        } else if (data == statusConstant.s129) {
            fn(data, obj);
        }
    });
}




exports.commonLimitOrActiveCheck = function(req, fn) {
    try {
        var tokReq = {
            token: req.headers.token,
            userId: req.headers.userid,
            method: req.method
        }
        console.log("REQ BODY ", req.body);
        var req = req.body;
        commonMethod.verifyUser(tokReq, function(data, obj) {
            if (data == statusConstant.s103) {
                //  var req = req.body;
                if (req.type == "group_limit") {
                    var groupQuery = "select (select count(*) from ambulance_master where is_active='Active' and group_id = " + req.group_id + ") as count,  group_limit from group_master where group_id =" + req.group_id + ";"
                    connection.query(groupQuery, function(groupErr, groupRes) {
                        if (!groupErr) {
                            if (groupRes != undefined) {
                                if (groupRes.length > 0) {
                                    if (req.group_limit < groupRes[0].group_limit) {

                                        if (groupRes[0].count <= req.group_limit) {
                                            fn("success");
                                        } else {
                                            fn("GrouplimitExceeded")
                                        }
                                    } else {
                                        /*proceed to updation*/
                                        fn("success");
                                    }
                                }
                            }

                        } else {
                            fn("err");
                        }
                    })

                } else if (req.type == "driver") {
                    var query = "select is_assigned,count(*) from ambulance_driver where driver_id=" + req.driver_id + " and status='Online' order by created_date desc limit 1;";
                    connection.query(query, function(err, res) {
                        if (!err) {
                            if (res != undefined) {
                                if (res.length == 0) {
                                    fn("success");
                                } else {
                                    if (res[0].is_assigned == true) {
                                        fn("onTripDriver");
                                    } else {
                                        fn("success");
                                    }
                                }
                            }
                        }
                    })
                } else if (req.type == "ambulance") {
                    var logQuery = "select login_status,group_id,device_token,ambulance_mobile from ambulance_master where amb_id=" + req.ambulance_id;
                    connection.query(logQuery, function(logErr, logRes) {
                        if (!logErr) {
                            if (logRes.length == 0) {
                                fn("noAmbulance");
                            } else {
                                console.log("logRes[0]", logRes[0]);
                                if (logRes[0].login_status == 'LoggedIn') {
                                    if (req.is_active == "InActive") {
                                        var ambQuery = "select driver_id,status,is_assigned from ambulance_driver where ambulance_id=" + req.ambulance_id + " and status='Online' order by created_date desc;"
                                        console.log("QUERY ", ambQuery);
                                        connection.query(ambQuery, function(ambErr, ambRes) {
                                            console.log("ERROR ", ambErr, "ambRes", ambRes);
                                            if (!ambErr) {
                                                if (ambRes.length > 0) {
                                                    if (ambRes[0].is_assigned == true) {
                                                        fn("onTrip");
                                                    } else {
                                                        console.log(ambRes[0]);
                                                        if (ambRes[0].driver_id != null) {
                                                            fn("success"); /* logout the driver with pn*/
                                                            /*                                                          var pnData = "logout the driver";
                                                                                                                        var message = "logout the driver";
                                                                                                                        var type = "driver_logout";
                                                                                                                        var deviceToken = [logRes[0].device_token];
                                                                                                                        var sendObj = {
                                                                                                                                user_type: 'Ambulance',
                                                                                                                                send_to: logRes[0].ambulance_mobile
                                                                                                                        }
                                                                                                                        sendPNDao.sendPN(deviceToken, pnData, message, type, sendObj);*/
                                                        }
                                                    }
                                                } else {
                                                    fn("success");
                                                }

                                            }
                                        })
                                    } else if (req.is_active == 'Active') {
                                        /*check for group limit of this ambulance */
                                        var groupQuery = "select (select count(*) from ambulance_master where is_active='Active' and group_id = " + logRes[0].group_id + ") as count,  group_limit from group_master where group_id =" + logRes[0].group_id + ";"
                                        connection.query(groupQuery, function(groupErr, groupRes) {
                                            if (!groupErr) {
                                                if (groupRes != undefined) {
                                                    if (groupRes.length > 0) {
                                                        if (groupRes[0].count >= groupRes[0].group_limit) {
                                                            fn("limitExceeded");
                                                        } else {
                                                            fn("success");
                                                        }
                                                    }
                                                }

                                            } else {
                                                fn("err");
                                            }
                                        })

                                    }
                                } else {
                                  /*  fn("success");*/
					if (req.is_active == "InActive") {
                                        var ambQuery = "select driver_id,status,is_assigned from ambulance_driver where ambulance_id=" + req.ambulance_id + " and status='Online' order by created_date desc;"
                                        connection.query(ambQuery, function(ambErr, ambRes) {
                                            if (!ambErr) {
                                                if (ambRes.length > 0) {
                                                    if (ambRes[0].is_assigned == true) {
                                                        fn("onTrip");
                                                    } else {
                                                        console.log(ambRes[0]);
                                                        if (ambRes[0].driver_id != null) {
                                                            fn("success"); /* logout the driver with pn*/

                                                        }
                                                    }
                                                } else {
                                                    fn("success");
                                                }

                                            }
                                        })
                                    } else if (req.is_active == 'Active') {
                                        /*check for group limit of this ambulance */
                                        var groupQuery = "select (select count(*) from ambulance_master where is_active='Active' and group_id = " + logRes[0].group_id + ") as count,  group_limit from group_master where group_id =" + logRes[0].group_id + ";"
                                        connection.query(groupQuery, function(groupErr, groupRes) {
                                            if (!groupErr) {
                                                console.log(groupRes[0].count >= groupRes[0].group_limit, "groupRes[0].count >= groupRes[0].group_limit");
                                                if (groupRes != undefined) {
                                                    if (groupRes.length > 0) {
                                                        if (groupRes[0].count >= groupRes[0].group_limit) {
                                                            console.log("limit exceeded");

                                                            fn("limitExceeded");
                                                        } else {
                                                            fn("success");
                                                        }
                                                    }
                                                }

                                            } else {
                                                fn("err");
                                            }
                                        })

                                    }
                                }
                            }

                        } else {
                            fn("err");
                        }
                    })
                }
            } else if (data == statusConstant.s108) {
                fn(statusConstant.s108);
            } else if (data == statusConstant.s104 || data == statusConstant.s105) {
                fn(data, obj);
            } else if (data == statusConstant.s129) {
                fn(data, obj);
            } else fn(data, obj)

        });
    } catch (e) {
        logger.info("Exception ", e);
        fn("err");
    }
}

exports.getFeedbackList = function(req, fn) {
	var connection = connections.connection;
	console.log(req.method + "    method name", req.body, req.params,req);
	var reqMethod = req.method;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	var req = req.body;

	if (req.fromApp != undefined) {
		tokReq['fromApp'] = req.fromApp;
	}

	var query="select c.customer_mobile_number as customer_number,f.booking_id,f.customer_id,f.reason,f.created_date,f.points from customer_master c ,feedback f where f.customer_id=c.customer_id";
	if (req.dateFilter != undefined && req.dateFilter != null && req.dateFilter != "" && Object.keys(req.dateFilter).length > 0) {
		var dateFilter = req.dateFilter;

		if (dateFilter.startDate != undefined && dateFilter.startDate != "" && dateFilter.endDate != undefined && dateFilter.endDate != "") {
			query +=" and f."+ dateFilter['fieldName'] + " between '" + dateFilter.startDate + "' and '" + dateFilter.endDate + "'";
		} else if (dateFilter.startDate != undefined && dateFilter.startDate != ""){
			query += " and f."+dateFilter['fieldName'] + " like '" + dateFilter.startDate + "%' ";
		}
	} else {
		query += " order by created_date desc";
	}
	console.log("Ssssssssssssssss -query",query);

	commonMethod.verifyUser(tokReq, function(data, obj) {
		if (data == statusConstant.s103) {
			connection.query(query, function(err, res) {
				if (!err) {
					if (res != undefined && res.length > 0) {
						fn("success", res)
					} else {

						fn(statusConstant.s124, {});
					}
				} else {

					fn(statusConstant.s122, err);
				}
			})

		} else if (data == statusConstant.s129) {
			fn(data, obj);
		}
	});
}

function inActivateEmergencyType(req, fn) {
    var connection = connections.connection;
    console.log(req.method + "    method name", req);
    console.log(req.method + "    method id", req.id);
    console.log("  method params", req.params.id);

    var reqMethod = req.method;

    var query = "select  id,type_name,created_date,updated_date,status from emergency_types where id=" + req.params.id;
    connection.query(query, function(err, res) {
        if (!err) {
            console.log(res, err, "2312", query)
            if (res != undefined && res.length > 0) {
                var q = "select * from emergency_booking where emergency_type_id=" + req.params.id + " and booking_status not in ('TC','CAN','CANCFAKE') ";
                connection.query(q, function(err, res1) {
                    console.log(res1, err, "2316------------------------", q)

                    if (!err) {
			if(res1!=undefined){
                        if (res1.length > 0) {
                            console.log("res1", res1);
                            fn("bookingProgress");
                        } else {
                            /*     fn("success",res1)*/
                            var groupQuery = "select group_id from group_e_attributes where emergency_type_id=" + req.params.id;
                            connection.query(groupQuery, function(err, groups) {
                                console.log(groups, err, "2326", groupQuery);

                                if (err) {
                                    logger.info("GROUP EMERGENCY TYPE ERROR ", err);
                                    fn("err", err);
                                } else {
                                    var i = 0;
                                    if (groups != undefined) {
                                        if (groups.length > 0) {
                                            connection.query("delete from group_e_attributes where emergency_type_id=" + req.params.id, function(err1, res1) {
                                                if (err1) {
                                                    console.log("err err1", err1)
                                                } else {
                                                    console.log("ressss res1", res1)
                                                    iterateGroup();


                                                    function iterateGroup() {
                                                        if (i < groups.length) {
                                                            var gQuery = "select *from group_master where group_id=" + groups[i].group_id;
                                                            console.log(gQuery, "query 2341 values i", i)
                                                            var j = 0;
                                                            connection.query(gQuery, function(err, grp) {
                                                                if (err) {
                                                                    console(err, "errrrrrrr 2344")
                                                                } else {
                                                                    var e_type = grp[0].emergency_type;
                                                                    console.log(e_type + "2347", grp[0])
                                                                    var e_types = e_type.indexOf(',')>-1?e_type.split(','):[e_type];
                                                                    console.log(e_types + " e_types 2349" + typeof e_types, req.params.id)
                                                                    if (e_types.indexOf(req.params.id) > -1) {
                                                                        var postn = e_types.indexOf(req.params.id);
                                                                        var k = e_types.indexOf(req.params.id);
                                                                        if (k != -1) {
                                                                            e_types.splice(k, 1);
                                                                        }
                                                                        console.log("emeType", e_types)
                                                                    }
                                                                    var emeType = e_types.join();
                                                                    console.log("ssssssss 2359", emeType)
								    emeType = (emeType!=""&& emeType!=" ")?emeType:null;
//                                                                    var updateGrpQuery = "update group_master set emergency_type ='" + emeType + "' where group_id =" + groups[i].group_id;
								   var updateGrpQuery = "";
                                                                    if(emeType!=null)
                                                                         updateGrpQuery = "update group_master set emergency_type ='" + emeType + "' where group_id =" + groups[i].group_id;
                                                                else
                                                                        updateGrpQuery = "update group_master set emergency_type =null where group_id =" + groups[i].group_id;
                                                                    connection.query(updateGrpQuery, function(err, updateresp) {
                                                                        if (!err) {
                                                                            if (updateresp != undefined) {
                                                                                if (updateresp.info.affectedRows > 0) {
                                                                                    if (i == groups.length - 1) {
                                                                                        req.group_id = groups[i].group_id;
                                                                                        deleteAmbEmergencyType(req, function(amb, status) {
                                                                                            if (amb == "noAmbulance") {
                                                                                                fn("noAmbGroup", {});
                                                                                            } else {
                                                                                                fn("can't delete", {});
                                                                                            }
                                                                                        });
                                                                                    }

                                                                                    i++;
                                                                                    iterateGroup();
                                                                                } else {
                                                                                    if (i == groups.length - 1) {
                                                                                        req.group_id = groups[i].group_id;
                                                                                        deleteAmbEmergencyType(req, function(amb, status) {
                                                                                            if (amb == "noAmbulance") {
                                                                                                fn("noAmbGroup", {});
                                                                                            } else {
                                                                                                fn("can't delete", {});
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    i++;
                                                                                    iterateGroup();
                                                                                }

                                                                            }
                                                                            console.log("qqqqqqqqqqq 2370", updateresp)

                                                                        } else {
                                                                            fn("err", {})

                                                                        }
                                                                    })
                                                                    console.log("2385")

                                                                }
                                                            })
                                                        }

                                                    }
                                                }
                                            })
                                        } else {
                                            deleteAmbEmergencyType(req, function(amb, status) {
                                                if (amb == "noAmbulance") {
                                                    fn("noAmbGroup", {});
                                                } else if (amb = "Ambulance not found") {
                                                    console.log("ssssewewee323")
                                                    fn("noAmbGroup", {});
                                                } else {
                                                    fn("can't delete", {});

                                                }
                                            });
                                            console.log('ssssssssss')
                                        }
                                    } else {
                                        console.log(groups, err, "2338 length is 0");
                                    }

                                }
                            });

                        }
			}
                    }

                })
            } else {
                console.log(res, err, "2342", query)

                fn('Emergency Type doesn\'t exist');
            }
        } else {
            console.log(err, "2347", query)

        }

    });

}

function deleteAmbEmergencyType(req, fn) {
    var ambQuery = "select ambulance_id from ambulance_e_attributes where emergency_type_id=" + req.params.id;
    connection.query(ambQuery, function(err, amb) {

        if (err) {
            logger.info("GROUP EMERGENCY TYPE ERROR ", err);
            fn("err", err);
        } else {
            var i = 0;
            if (amb != undefined) {
                if (amb.length > 0) {
                    connection.query("delete from ambulance_e_attributes where emergency_type_id=" + req.params.id, function(err1, res1) {
                        if (err1) {
                            fn("err");
                        } else {
                            console.log("12341545")
                            iterateAmb();
                        }
                    })

                    function iterateAmb() {
                        if (i < amb.length) {


                            var aq = "select *from ambulance_master where amb_id=" + amb[i].ambulance_id;
                            connection.query(aq, function(err, ambulance) {
                                if (err) {
                                    fn("err")
                                } else {

                                    var e_type = ambulance[0].emergency_type;
//                                    var e_types = e_type.split(',');
				    var e_types = e_type.indexOf(',')>-1?e_type.split(','):[e_type];
                                    if (e_types.indexOf(req.params.id) > -1) {
                                        var postn = e_types.indexOf(req.params.id);
                                        var k = e_types.indexOf(req.params.id);
                                        if (k != -1) {
                                            e_types.splice(k, 1);
                                        }
                                    }
                                    var emeType = e_types.join();
				    emeType = (emeType!=""&& emeType!=" ")?emeType:null;
//                                    var updateAmbQuery = "update ambulance_master set emergency_type ='" + emeType + "' where amb_id =" + amb[i].ambulance_id;
				   var updateAmbQuery = "";
                                    if(emeType!=null)
                                      updateAmbQuery = "update ambulance_master set emergency_type ='" + emeType + "' where amb_id =" + amb[i].ambulance_id;
                                   else
                                        updateAmbQuery = "update ambulance_master set emergency_type =null where amb_id =" + amb[i].ambulance_id;
logger.info("UPDATE QUERY ", updateAmbQuery);

                                    connection.query(updateAmbQuery, function(err, updateresp) {
                                        if (!err) {
                                            if (updateresp != undefined) {
                                                if (updateresp.info.affectedRows > 0) {
                                                    if (i == amb.length - 1) {
                                                        fn("noAmbulance", {})
                                                    }
                                                    i++;
                                                    iterateAmb();
                                                } else {
                                                    if (i == amb.length - 1) {
                                                        fn("noAmbulance", {})
                                                    }
                                                    i++;
                                                    iterateAmb();
                                                }

                                            }

                                        } else {
                                            fn("err", {})

                                        }
                                    })

                                }
                            })

                        } else {
                            fn("err")
                        }
                    }

                } else {
                    console.log("Sasasawqw")
                    fn("noAmbulance", {})
                }
            } else {
                fn("noAmbulance")
            }
        }
    });
}
exports.getFeedbackList = function(req, fn) {
    var connection = connections.connection;
    console.log(req.method + "    method name", req.body, req.params,req);
    var reqMethod = req.method;
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    var req = req.body;

    if (req.fromApp != undefined) {
        tokReq['fromApp'] = req.fromApp;
    }


	var query="select c.customer_mobile_number as customer_number,f.booking_id,f.customer_id,f.reason,f.created_date,f.points from customer_master c ,feedback f where f.customer_id=c.customer_id";
	if (req.dateFilter != undefined && req.dateFilter != null && req.dateFilter != "" && Object.keys(req.dateFilter).length > 0) {
	            var dateFilter = req.dateFilter;
	
	            if (dateFilter.startDate != undefined && dateFilter.startDate != "" && dateFilter.endDate != undefined && dateFilter.endDate != "") {
	                query +=" and f."+ dateFilter['fieldName'] + " between '" + dateFilter.startDate + "' and '" + dateFilter.endDate + "'";
			query += " order by created_date desc";
	            } else if (dateFilter.startDate != undefined && dateFilter.startDate != ""){
	                query += " and f."+dateFilter['fieldName'] + " like '" + dateFilter.startDate + "%' ";
			query += " order by created_date desc";
	            }
	} else {
	   query += " order by created_date desc";
	}
	    logger.info("Ssssssssssssssss -query",query);
	
	commonMethod.verifyUser(tokReq, function(data, obj) {
	    if (data == statusConstant.s103) {
	        connection.query(query, function(err, res) {
	            if (!err) {
	                if (res != undefined && res.length > 0) {
	                    fn("success", res)
	                } else {
	
	                    fn(statusConstant.s124, {});
	                }
	            } else {
	
	                fn(statusConstant.s122, err);
	            }
	        })
	
	    } else if (data == statusConstant.s129) {
	        fn(data, obj);
	    }
	});
	}
