var connections = require("../Util/dbConnection.js"),
commonConstants = require("../Util/commonConstants.js"),
commonMethod = require("../Util/commonMethod.js");
var multer = require('multer');
var fs = require("fs");
var datetime = require("node-datetime");
var sendPNDao = require("../Dao/sendPNDao.js");
var logger = require('../logging/logger.js');
var encryptionMob = require("../Util/encryptionMob.js");
var statusConstant = require("../Util/statusConstants.json");
var generalDao = require("../Dao/generalDao.js");
var storage = multer.diskStorage({
	destination: function(req, file, callback) {
		callback(null, './banners');
	},
	filename: function(req, file, callback) {
		callback(null, file.fieldname + '-' + Date.now());
	}
});

var upload = multer({ storage: storage }).single('banner');

exports.uploadBanner = function(req,fn){

	if(req.headers.token!=undefined && req.headers.userid!=undefined && req.files!=undefined){

		var connection = connections.connection;

		var tokReq = {

				token: req.headers.token,

				userId: req.headers.userid,

				method: req.method

		}

		commonMethod.verifyUser(tokReq, function(data, obj) {

			if (data == statusConstant.s103) {

				if(req.files!=null && req.files!=""){

					var JSONDATA = req.body;

					var jsondata = JSONDATA.jsondata

					jsondata = (typeof jsondata == "string" ? JSON.parse(jsondata) : jsondata);

					logger.info("JSON DATA", jsondata);

					var title = jsondata['title'];

					var description = jsondata['description'];

					var start_date =jsondata['start_date'];

					var end_date = jsondata['end_date'];

					var is_active =jsondata['status'];

					upload(req, fn, function(err) {

						fs.readFile(req.files.banner.path, function(err, data) {

							var dirname = "";

							var fileName = "";

							var image_url = "";

							dirname = commonConstants.promoBannerUploadDir;

							fileName = "banner-" + Date.now();

							var newPath = dirname + fileName;

							if (req.files.banner.type) {

								newPath += "." + req.files.banner.type.split("image/")[1];

								fileName += "." + req.files.banner.type.split("image/")[1];

								image_url = commonConstants.promoBannerPath + fileName;

								fs.writeFile(newPath, data, function(err) {

									if (err) {

										logger.info("ERR wrting file",err);

										fn("err", err);

									} else {

										var insertQuery = "insert into promo_banners(banner_path,title,description,start_date,end_date,status) values('"+image_url+"','"+title+"','"+description+"','" + start_date+"','"+end_date+"','"+is_active+"')";

										connection.query(insertQuery,function(error,result){

											if(!error){
												if(is_active=='Active'){
												/*send pn */

												generalDao.getCustomerDeviceTokens(function(status,customers){

													if(status == "err"){

														fn("errPn");

													}else if(status == "noCustomers"){

														fn("noCustomerPN");

													}else if(status == "success"){

														fn("success");

														//var i = 0;

														var deviceTokens =[];

														var message ={bannerPath:image_url,

title:title,

description:description

}; 

														var pn_type = "new_banner";

														var data = message.title;

														var pnObj = {

																send_to:"All logged-in Customers",

user_type:"Customer"

														};
													if(customers.length >= commonConstants.customerSize){
															logger.info("IN MORETHAN CHUCNK SIZE ",customers.length);
														var customer_list = customers;
var k = 0;
iterateChunkCustomers();

function iterateChunkCustomers() {
    if (customer_list.length != 0) {
        logger.info("CUSTOME LIST ", customer_list.length);
        var devicetokens = [];
        var chunk_list = customer_list.splice(0, commonConstants.customerSize);
        logger.info("CHUNK CUSTOMERS ", customer_list.length);
        var j = 0;
	var ios_tokens = [];
        iterateCustomerList();

        function iterateCustomerList() {

            if (j < chunk_list.length) {
		if(chunk_list[j].device_type == "Android")
                devicetokens.push(chunk_list[j].device_token);
		else
		ios_tokens.push(chunk_list[j].device_token);

                if (j == chunk_list.length - 1) {
			logger.info("LENGTH ", devicetokens.length);
			logger.info("IOS LENGTH ", ios_tokens.length);
		    if(devicetokens.length>0 && ios_tokens.length>0){
                    sendPNDao.sendPN(devicetokens, data, message, pn_type, pnObj);
			sendPNDao.sendPN(ios_tokens, data, message, pn_type, pnObj,true);
			k++;
                    iterateChunkCustomers();
			}
		   else if(ios_tokens.length>0){
			sendPNDao.sendPN(ios_tokens, data, message, pn_type, pnObj,true);
			k++;
                    iterateChunkCustomers();
			}
		    else if(devicetokens.length>0){
			sendPNDao.sendPN(devicetokens, data, message, pn_type, pnObj);
				k++;
                    iterateChunkCustomers();
			}
                   /* k++;
                    iterateChunkCustomers();*/
                } else {

                    j++;

                    iterateCustomerList();

                }

            }
        }
    }
}

} else {

														var i=0;
														var iosTokens=[];
														iterateCustomers();

														function iterateCustomers(){

															if(i<customers.length){																       if(customers[i].device_type == "Android")
																deviceTokens.push(customers[i].device_token);
																else
																	iosTokens.push(customers[i].device_token);

																if(i == customers.length-1){
if(deviceTokens.length>0)
																	sendPNDao.sendPN(deviceTokens, data, message, pn_type, pnObj);
if(iosTokens.length>0)
sendPNDao.sendPN(iosTokens, data, message, pn_type, pnObj,true);

																}else{

																	i++;

																	iterateCustomers();

																}



															}

														}
													}

													}

												})
											}else{
												fn("success");
											}

											}else{

												fn("err");

											}

										});



									}

								});

							}else{

								fn("invalidFile");

							}



						})

					});

				}else{

					fn("noFile");

				}

			}

		});

	}else{

		fn("invalidInput");

	}



}
exports.getPromos =function (req,fn){
var query= "select offerid,banner_path,title,description,start_date,end_date from promo_banners where DATE(start_date) <= DATE(NOW()) and DATE(end_date) >= DATE(NOW()) and status= 'Active' order by created_date desc;";
var connection= connections.connection;

		 var tokReq = {
                                token: req.headers.token,
                                userId: req.headers.userid,
                                method: req.method
	                      }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
            tokReq['customerApp'] = req.body.customerApp;
            //      tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
console.log("------------------",req.body.iOS,req.body.customer_mobile,(req.body.iOS != undefined && (req.body.iOS == true || req.body.iOS == "true")), encryptionMob.decrypt(req.body.customer_mobile));

            tokReq['customerMobile'] = (req.body.iOS != undefined && (req.body.iOS == true || req.body.iOS == "true")) ? encryptionIos.decrypt(req.body.customer_mobile) : encryptionMob.decrypt(req.body.customer_mobile);
console.log("------------------",tokReq['customerMobile']);
        }

                commonMethod.verifyUser(tokReq, function(data, obj) {
	//	console.log("ssssssssssssssssss",data,obj);
              	        if (data == statusConstant.s103) {
				connection.query(query,function(err,res){
				if(!err){
					if(res != undefined &&  res.length>0){
						fn("success",res)
				
					}else {
	 			                        fn("success", res);
					}
				} else {
	
		                    fn(statusConstant.s122, err);
        		        }

				})
			} else if (data == statusConstant.s129) {
		            fn(data, obj);
        		}
		});


}

exports.updatePromoBanner = function(req, fn) {
    var connection = connections.connection;
    var obj= "";
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;

        tokReq['customerMobile'] = (req.body.iOS != undefined && (req.body.iOS == true || req.body.iOS == "true")) ? encryptionIos.decrypt(req.body.customer_mobile) : encryptionMob.decrypt(req.body.customer_mobile);
    }
        req =req.body;
        var query="update promo_banners set start_date='"+req.params.start_date+"',end_date='"+req.params.end_date+"',status='"+req.params.status+"' where offerid="+req.params.offerid;
    commonMethod.verifyUser(tokReq, function(data, obj) {
        console.log("ssssssssssssssssss", data, obj);
        if (data == statusConstant.s103) {
            connection.query(query, function(err, res) {
                console.log(err,"err",res,"res",res.length);
                if (!err) {
                    if (res != undefined ) {
                        fn("success", res)
                    } else {
                        fn("No rows updated", {});
                    }
                } else {
                    fn(statusConstant.s122, err);
                }
            })
        } else if (data == statusConstant.s129) {
            fn(data, obj);
        } else if (data == statusConstant.s124) {
            fn(data, obj);
        }
    });
}
