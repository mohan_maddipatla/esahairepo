var connections = require("../Util/dbConnection.js");
var	commonMethod = require("../Util/commonMethod.js");
var serviceDao = require("../Dao/serviceDao.js");
var logsDao = require("../Dao/logsDao.js");
exports.createTable = function(req,fn){


var reqMethod = req.method;
var tokReq = {
		token: req.headers.token,
		userId: req.headers.userid,
		method: req.method
}
		var req = req.body;
		/*var tokReq = {
				token: req.token,
				userId: req.userId,
				method: reqMethod
		}*/
		var fields = req.fields;
		var connection  = connections.connection;
		/*else{
			connection = connections.connection;
		}*/
commonMethod.verifyUser(tokReq, function(data,obj){
	if(data == "success" && obj.length >0 ){
		var query = "create table "+req.tableName+"(";
			var i=0;
			//if(fieldsfields.length >0)
			iterateFields();
			function iterateFields(){
				console.log("I ",i);
				query+= fields[i].fieldName+" "+fields[i].fieldType+" "+fields[i].nullable;
				if(req.autoIncrement!=null && req.autoIncrement != ""){
					if(req.autoIncrement == fields[i].fieldName){
						query += " auto_increment"
					}
				}
				if(i == fields.length-1){                                                                      
					if(req.primaryKey==null || req.primaryKey=="")
						query+=""
					else
						query+=","
				}else{
					query+=","
					i++;
					iterateFields();
				}
			}
			if(req.primaryKey!=null && req.primaryKey!=""){
				query+=" PRIMARY KEY ("+req.primaryKey+")";
				if(req.foreignKey!=undefined && req.foreignKey!="" && req.foreignKey!=null)
					query+=",";
				else 
					query+="";
			}
			if(req.foreignKey!=undefined && req.foreignKey!="" && req.foreignKey!=null){
				if(req.foreignKey.column!="" && req.foreignKey.column!=null && req.foreignKey.referenceTable!=null && req.foreignKey.referenceTable!="" && req.foreignKey.referenceColumn!=null && req.foreignKey.referenceColumn!=""){
					query+="FOREIGN KEY ("+req.foreignKey.column+")"+" REFERENCES "+req.foreignKey.referenceTable+"("+req.foreignKey.referenceColumn+")"
				}
			}
		query+=");";
	console.log("QUEry in Create Table ",query);
	if(req.serviceParams!=undefined && Object.keys(req.serviceParams).length>0){
			var serviceId = {serviceId:req.serviceParams.serviceId}
			var service = {
				serviceId:req.serviceParams.serviceId,
				tableName:req.tableName
			}
			
			serviceDao.connectToMysql(serviceId,function(dd){
				//console.log("KHHHJ ", dd);
				connection = dd;
				connection.query(query,function(err,res){
				if(err){
					console.log("ERROR ",err);
					fn(null,err);
				}else{
					console.log("Table created successfully");
					logsDao.insertServiceTable(service);
					fn(res,"");
				}
		});
			});
	}else{
		connection.query(query,function(err,res){
			if(err){
				console.log("ERROR ",err);
				fn(null,err);
			}else{
				console.log("Table created successfully");
				connection.query("insert into appTables(tableName) values('"+req.tableName+"')")
				fn(res,"");
			}
		});
	}
	}else if(data=="err"){
		 fn("Unauthorized");
	}else{
		fn("Invalid token",obj);
	}
});
};

exports.modifyField = function(req,fn){
	var connection = connections.connection;
	var reqMethod = req.method;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
		var req = req.body;
		/*var tokReq = {
				token: req.token,
				userId: req.userId,
				method: reqMethod
		}*/
	commonMethod.verifyUser(tokReq, function(data,obj){
		if(data == "success" && obj.length >0 ){
			var query = "ALTER TABLE "+req.tableName+" MODIFY COLUMN "+req.columnName+" "+req.columnType+"";
			if(req.serviceParams!=undefined && Object.keys(req.serviceParams).length>0){
				var serviceId = {serviceId:req.serviceParams.serviceId}
				var service = {
					serviceId:req.serviceParams.serviceId,
					tableName:req.tableName
				}
				//logsDao.insertServiceTable(service);
				serviceDao.connectToMysql(serviceId,function(dd){
					connection = dd;
					connection.query(query,function(err,res){
					if(err){
						console.log("Error occured ",err);
						fn(null,err)
					}else{
						console.log("Modified successfully");
						fn(res,"");
					}
					})
				});
			}else{
				connection.query(query,function(err,res){
						if(err){
							console.log("Error occured ",err);
							fn(null,err)
						}else{
							console.log("Modified successfully");
							fn(res,"");
						}
				})
			}
			
		}else if(data=="err"){
		 fn("Unauthorized");
	}else{
			fn("Invalid token",obj);
		}
	});
}

exports.addField = function(req,fn){
	var connection = connections.connection;
	var reqMethod = req.method;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
		var req = req.body;
		/*var tokReq = {
				token: req.token,
				userId: req.userId,
				method: reqMethod
		}*/
	commonMethod.verifyUser(tokReq, function(data,obj){
		if(data == "success" && obj.length >0 ){
			var query = "ALTER TABLE "+req.tableName+" ADD "+req.columnName+" "+req.columnType+"";
			if(req.serviceParams!=undefined && Object.keys(req.serviceParams).length>0){
				var serviceId = {serviceId:req.serviceParams.serviceId}
				var service = {
					serviceId:req.serviceParams.serviceId,
					tableName:req.tableName
				}
				//logsDao.insertServiceTable(service);
				serviceDao.connectToMysql(serviceId,function(dd){
					connection = dd;
					connection.query(query,function(err,res){
						if(err){
							console.log("Error occured ",err);
							fn(null);
							
						}else{
							console.log("added successfully");
							fn(res);
						}
					})
				});
			}else{
				connection.query(query,function(err,res){
					if(err){
						console.log("Error occured ",err);
						fn(null);
						
					}else{
						console.log("added successfully");
						fn(res);
					}
				})
			}
		}else if(data=="err"){
		 fn("Unauthorized");
	}else{
			fn("Invalid token",obj);
		}
	});
}

exports.dropField = function(req,fn){
	var connection = connections.connection;
	var reqMethod = req.method;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
		var req = req.body;
		/*var tokReq = {
				token: req.token,
				userId: req.userId,
				method: reqMethod
		}*/
	commonMethod.verifyUser(tokReq, function(data,obj){
		if(data == "success" && obj.length >0 ){
			var query = "ALTER TABLE "+req.tableName+" DROP COLUMN "+req.columnName+"";
			if(req.serviceParams!=undefined && Object.keys(req.serviceParams).length>0){
				var serviceId = {serviceId:req.serviceParams.serviceId}
				var service = {
					serviceId:req.serviceParams.serviceId,
					tableName:req.tableName
				}
				//logsDao.insertServiceTable(service);
				serviceDao.connectToMysql(serviceId,function(dd){
					connection = dd;
					connection.query(query,function(err,res){
						if(err){
							console.log("Error occured ",err);
							fn(null,err)
						}else{
							console.log("dropped successfully");
							fn(res,"");
						}
					})
				});
			}else{
				connection.query(query,function(err,res){
					if(err){
						console.log("Error occured ",err);
						fn(null,err)
					}else{
						console.log("dropped successfully");
						fn(res,"");
					}
				})
			}
		}else if(data=="err"){
		 fn("Unauthorized");
	}else{
			fn("Invalid token",obj);
		}
	});
}

exports.dropTable = function(req,fn){
	var connection = connections.connection;
	var reqMethod = req.method;
	var tableName=req.param('tableName');
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
		var req = req.body;
		/*var tokReq = {
				token: req.token,
				userId: req.userId,
				method: reqMethod
		}*/
	commonMethod.verifyUser(tokReq, function(data,obj){
		if(data == "success" && obj.length >0 ){
			var query = "drop table "+tableName; 
			if(req.serviceParams!=undefined && Object.keys(req.serviceParams).length>0){
				var serviceId = {serviceId:req.serviceParams.serviceId}
				var service = {
					serviceId:req.serviceParams.serviceId,
					tableName:req.tableName
				}
				//logsDao.insertServiceTable(service);
				connection.query("delete from serviceTables where tableName = '"+tableName+"' and serviceId = '"+req.serviceParams.serviceId+"'",function(err,res){
					if(!err){
						fn(res,"");
					}else{
						console.log("ERROR IN serviceTables DATA", err);
						fn(null,err);
					}
				})
				if(tableName=='services'){
					connection.query("delete from serviceFieldValues where serviceId ="+req.serviceParams.serviceId+"",function(err,res){
						if(!err){
							fn(res,"");
						}else{
							console.log("ERROR IN serviceTables DATA", err);
							fn(null,err);
						}
					})
				}
				
				serviceDao.connectToMysql(serviceId,function(dd){
					connection = dd;
					connection.query(query,function(err,res){
						if(err){
							console.log("error ",err)
							fn(null,err);
						}else{
							console.log("dropped successfully");
						}
					})
				});
			}else{
				connection.query(query,function(err,res){
					if(err){
						console.log("error ",err)
						fn(null,err);
					}else{
						console.log("dropped successfully");
						connection.query("delete from tableData where tableName = '"+tableName+"'",function(err,data){
							if(!err){
								fn(res,"");
							}else{
								console.log("ERROR IN TABLE DATA", err);
								fn(null,err);
							}
						})
						
					}
				})
			}
		}else if(data=="err"){
		 fn("Unauthorized");
	}else{
				fn("Invalid token",obj);
		}
	});
}