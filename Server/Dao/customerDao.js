var connections = require("../Util/dbConnection.js");
var logsDao = require("../Dao/logsDao.js");
var commonConstants = require("../Util/commonConstants.js"),
    commonMethod = require("../Util/commonMethod.js");
var statusConstant = require("../Util/statusConstants.json");
var multer = require('multer');
var serviceDao = require("../Dao/serviceDao.js");
var datetime = require("node-datetime");
var encryptionMob = require("../Util/encryptionMob.js");
var logger = require('../logging/logger.js');
var fs = require("fs");
exports.getSystemAttributes = function(req, fn) {
    var connection = connections.connection;
    var query = "select * from customer_e_types";
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined) {
                fn("success", res);
            }
        } else {
            fn(null, err);
        }
    })
}

exports.addCustomerAttributes = function(req, fn) {
    var connection = connections.connection;
    var query = "insert into customer_attributes(attribute_name,attribute_details,customer_id,customer_e_type_id) values('" +
        req.attribute_name + "','" + req.attribute_details + "'," + req.customer_id + "," + req.customer_e_type_id + ")";
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined) {
                fn("success", res);
            }
        } else {
            fn(null, err);
        }
    })
}

exports.updateCustomerAttributes = function(req, fn) {
    var connection = connections.connection;
    var query = "update customer_attributes set attribute_name = '" + req.attribute_name + "',attribute_details = '" + req.attribute_details + "',customer_id=" + req.customer_id + ",customer_e_type_id=" + req.customer_e_type_id + " where id=" + req.id;
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined) {
                fn("success", res);
            }
        } else {
            fn(null, err);
        }
    });
};

exports.getCustomerAttributes = function(req, fn) {
    var connection = connections.connection;
    var query = "select * from customer_attributes where customer_id = " + req.customer_id;
    console.log("QUERY ",query);
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined) {
                    fn("success", res);
            }
        } else {
            fn(null, err);
        }
    });
}

exports.getRateCard = function(req, fn) {
    var connection = connections.connection;
    var query = "select * from rate_card where status='Active'";
     var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    console.log(JSON.stringify(req.body),"Headers ",req.headers)
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
    }
    commonMethod.verifyUser(tokReq, function(status, data) {
console.log("OPOPO ",status);
        if (status == "success") {
            connection.query(query, function(err, res) {
                if (!err) {
                    if (res != undefined) {
			console.log("RES ",res);
                        fn("success", res);
                    }
                } else {
                    fn(null, err);
                }
            });
        } else if (status == "err") {
            fn("Unauthorized");
        } else if (status == "Invalid token" || status == 'No User') {
            fn(status, data);
        }
    })

}

var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './');
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now());
    }
});

var upload = multer({ storage: storage }).single('file');

exports.updateProfile = function(req, fn) {
    var connection = connections.connection;
    var reqMethod = req.method;
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    var request = req;

    var req = null;
var removeFile = "";
                    var filePath = "";

    if (request.body.jsondata != undefined) {
        req = JSON.parse(request.body.jsondata);
    } else {
        req = request.body;
    }
    if (req.customerApp == true || req.customerApp == "true") {
        req.id = request.headers.userid;
        req.tableName = 'customer_master';
        req.param = 'customer_id'; console.log("MOBILE ", req);
           tokReq['customerApp'] = true;
          tokReq['customerMobile'] = encryptionMob.decrypt(req.customer_mobile);
    } else {
        req.id = request.body.driverId;
        req.tableName = 'driver_master';
        req.param = 'driver_id';
        tokReq['ambApp'] = "true";
    }

   console.log("OIOI ",tokReq); 
    commonMethod.verifyUser(tokReq, function(data, obj) {
        console.log("Data   **** " + data)
        if (data == statusConstant.s103) {
		var q = "select file_url from " + req.tableName + " where " + req.param + "=" + req.id;
	  console.log("DATAT ",q);
            connection.query("select file_url from " + req.tableName + " where " + req.param + "=" + req.id, function(err, res) {
	//		console.log("ERRO ",err);
//console.log("OPOPO ",res);
                    if (!err) {
                        if (res != undefined && res.length > 0) {
                            //console.log("link"+res[0].file_url)
                            if (res[0]!=undefined && res[0].file_url != undefined) {
				var removeFile = "";
                    var filePath = ""; console.log("FIEL ",res[0].file_url);
                if(res[0].file_url!=null){ 
		   if (req['tableName'] == "driver_master") {
                        removeFile = res[0].file_url.split("/drivers/")[1];
                        filePath = commonConstants.driverImageUploadPath +removeFile
		   }else{
				removeFile = res[0].file_url.split("/patients/")[1];
                        filePath = commonConstants.patientImageUploadPath +removeFile;
			}
			//	console.log("POPOPOPO ");
				//if(filePath!=undefined && filePath!="" && filePath!=null)
				if(res[0].file_url!="" && res[0].file_url!=undefined && res[0].file_url!=null)
                                fs.unlinkSync(filePath);
		          //     console.log("IUIUIU ");
                            }}
                            upload(request, fn, function(err) {
                                fs.readFile(request.files.file.path, function(err, data) {
                                    var dirname = "";
                                    var fileName = ""; console.log("DATER ", Date.now());
				    var nPath = "";
                                    if (req.customerApp == true || req.customerApp == "true") {
                                        dirname = commonConstants.patientImageUploadPath;
			//		console.log("IOIOI ",commonConstants.patientImageUploadPath);
                                        fileName = "customer-" + Date.now();
				        nPath = dirname + fileName;
                                    } else {
                                        dirname = commonConstants.driverImageUploadPath;
                                        fileName = "driver-" + Date.now();
				        nPath = dirname + fileName;
                                    }

                                    var newPath = nPath;
			//		console.log("TYPE ",request.files.file);
                                    if (request.files.file.type) {
			//		console.log("TYEP ",request.files.file.type.split("image/"));
                                        newPath += "." + request.files.file.type.split("image/")[1];
                                        fileName += "." + request.files.file.type.split("image/")[1];
                                    }
                                    var image_url = "";
                                    if (req.customerApp == true  || req.customerApp == "true") {
                                        image_url = commonConstants.patientFilePath + fileName;
                                    } else {
                                        image_url = commonConstants.driverFilePath + fileName;
                                    }
			//		console.log("LLLLLLLLLLLLLLLLLL ",image_url);
                                    fs.writeFile(newPath, data, function(err) {
                                        if (err) {
                                            fn("err", err);
                                        } else {
                                            connection.query("update "+req.tableName+" set file_url='" + image_url + "' where "+req.param+"=" + req.id, function(error, res) {
                                                if(!error){
                                                    fn("success", image_url);
                                                }else{
							console.log("ERROR ",error);
                                                    fn(error, '');
                                                }
                                            })


                                        }
                                    });
                                });
                            });
                        } else {
                            fn(statusConstant.s124, {});
                        }
                    } else {
                        fn(statusConstant.s122, err);
                    }
                })

        } else if (data == statusConstant.s108) {
            fn(statusConstant.s106);
        } else if (data == statusConstant.s104 || data == statusConstant.s105) {
            fn(data, obj);
        } else if (data == statusConstant.s129) {
            fn(data, obj);
        }

    });
}

exports.addfeedback = function(req, fn) {
    var connection = connections.connection;
    var query = "insert into feedback(points,booking_id,customer_id,reason) values ("+req.body.points+
        ","+req.body.booking_id+","+req.body.customer_id+",'"+req.body.comment+"')";
    console.log("BODY ",req.body);
    console.log("QERY ",query);
     var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
        tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
    }
    commonMethod.verifyUser(tokReq, function(status, data) {
	console.log("OPOO ",status);
        if (status == "success") {
		console.log("IOIO ");
            connection.query(query, function(err, res) {
	   console.log("OPOP ",err,res);
                if (!err) {
                    if (res != undefined) {
                        fn("success", res);
                    }
                } else {
                    fn(null, err);
                }
            });
        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    })

}



exports.addPatientInfo = function(req,fn){
     var connection = connections.connection;
console.log("REQUEST ",req.body);
    var query = "insert into patient_master(patient_name,contact_number,patient_condition,gender,age,booking_id) values ('"+req.body.patient_name+"','"+req.body.contact_number+"','"+req.body.patient_condition+"','"+req.body.gender+"',"+req.body.age+","+req.body.booking_id+")"; console.log("QIERY" ,query);
     var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
    }
   if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
        tokReq['ambApp'] = req.body.ambulanceApp;
    }

    console.log("USER ",tokReq);
    commonMethod.verifyUser(tokReq, function(status, data) {
	console.log("STUTU ",status);
        if (status == "success") {
            connection.query(query, function(err, res) {
	//	console.log("ERR ",err);
	  //      console.log("ERES ",res);
                if (!err) {
                    if (res != undefined) { console.log("POPOP ",res);
                        fn("success", res);
                    }
                } else {
		  if(err.code == 1062){
			var query = "update patient_master set patient_name = '"+req.body.patient_name+"',contact_number='"+req.body.contact_number+"', patient_condition ='"+req.body.patient_condition+"',gender='"+req.body.gender+"',age="+req.body.age+" where booking_id="+req.body.booking_id;
			connection.query(query, function(err, res) {
                if (!err) {
                    if (res != undefined) { console.log("POPOP ",res);
                        fn("success", res);
                    }
                } else {
                    fn("err", err);
                }
            });

		  }else
                    fn(null, err);
                }
            });
        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    })
}

exports.getPatientInfo = function(req,fn){
     var connection = connections.connection;
    var query = "select * from patient_master where booking_id ="+req.body.booking_id;
     var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
    }
    if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
        tokReq['ambApp'] = req.body.ambulanceApp;
    }
    commonMethod.verifyUser(tokReq, function(status, data) {
        if (status == "success") {
            connection.query(query, function(err, res) {
		logger.info("PATIEINT MASTER RESULT ", res);
                if (!err) {
                    if (res != undefined) {
                        if(res.length >0)
                            fn("success", res);
                        else{
                           // fn("noCustomer");
	//			var bookquery = "select patient_number,customer_mobile,customer_name,is_self from emergency_booking where booking_id="+req.body.booking_id;
var bookquery= "select eb.customer_id,eb.patient_number,eb.customer_mobile,eb.customer_name,eb.is_self,cm.age,cm.gender from customer_master cm, emergency_booking eb where eb.customer_id=cm.customer_id and eb.booking_id="+req.body.booking_id;
logger.info("BOOK QUERY ", bookquery);
                        	connection.query(bookquery,function(error,result){
                        		if(!error){
				logger.info("UIUIUIU IU ",result);
                        			if(result.length>0){
                                                        result[0]['contact_number'] = result[0].is_self==true?result[0].customer_mobile:result[0].patient_number;
                                                        result[0]['age']=result[0].is_self==true?result[0].age:'';
                                                        result[0]['patient_name']=result[0].is_self==true?result[0].customer_name:'';
                                                        result[0]['patient_condition']=' ';
                                                        result[0]['gender']=result[0].is_self==true?result[0].gender:"";
                        				fn("success",result);
                        			}else{
                        				fn("noCustomer");
						}
                        		}else{
                        			fn(null, err);
                        		}
                        	});
			}
                    }
                } else {
                    fn(null, err);
                }
            });
        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    })
}

exports.updatePatientInfo = function(req,fn){
     var connection = connections.connection;
    var query = "update patient_master set patient_name = '"+req.body.patient_name+"',contact_number='"+req.body.contact_number+"', patient_condition ='"+req.body.patient_condition+"',gender='"+req.body.gender+"',age="+req.body.age+" where customer_id="+req.headers.userid+" and booking_id="+req.body.booking_id;
    console.log("QUERY ",query);
     var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
        tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
    }
    if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
        tokReq['ambApp'] = req.body.ambulanceApp;
    }
    commonMethod.verifyUser(tokReq, function(status, data) {
        if (status == "success") {
            connection.query(query, function(err, res) {
                if (!err) {
                    if (res != undefined) {
                        fn("success", res);
                    }
                } else {
                    fn("err", err);
                }
            });
        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    })
}

/** Manual Booking **/
exports.checkCustomerExistsOrNot = function(req,fn){
    var connection = connections.connection;
    var query = "select * from customer_master where customer_mobile_number = "+req.body.customerMobile+" order by created_date desc limit 1";
     var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    commonMethod.verifyUser(tokReq, function(data,obj) {
        if (data == "success") {
            connection.query(query, function(err, res) {
                if (!err) {
                    if (res != undefined) {
                        if(res.length >0){
			    var cusQuery = "select  * from emergency_booking where customer_id ="+res[0].customer_id+" and created_datetime in (select * from (select Max(created_datetime) from emergency_booking where customer_id="+res[0].customer_id+") As p);"
			    connection.query(cusQuery,function(error,result){
				if(!error){
				   if(result!=undefined){
					if(result.length>0){
					//	if(result[0].booking_status in ("Trip Closed","Cancelled","Cancelled (Fake)")){
						if(result[0].booking_status == commonConstants.TripClosed || result[0].booking_status ==commonConstants.Cancelled||result[0].booking_status == commonConstants.FakeCancel){
							fn("success", res[0]);
						}else
							fn("onTrip");
					}else{
						fn("success", res[0]);
					}
				  }
				}else{
				}
				});
}
//                            fn("success", res[0]);
		//	}
                        else
                            fn("noCustomer");
                    }
                } else {
                    fn("err", err);
                }
            });
        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    })
}




exports.getAppVersion = function(req,fn){
	var connection = connections.connection;
	var query = "select * from app_version";
	if(req.query.appType!=undefined)
		query += " where app_type='"+req.query.appType+"'"; console.log("ADJFLDSJ ",query);
			connection.query(query, function(err,res){
				if(!err){
					fn("success", res[0]);
				}else{
					fn("err", err);
				}
			});
}

exports.getTermsAndConditions = function(req, fn){
    var connection = connections.connection;
     var query = "select * from terms_conditions;";
     connection.query(query, function(err,res){
            if(!err){
                    if(res!=undefined)
                            fn("success",res);
            }else{
                    fn("err",err)
            }
     })
}
exports.getComeToKnowBy = function(req, fn){
    var connection = connections.connection;
     var query = "select id,know_by,status from come_to_know_by where status = 'Active';";
     connection.query(query, function(err,res){
            if(!err){
                    if(res!=undefined)
                            fn("success",res);
            }else{
                    fn("err",err)
            }
     })
}
exports.getCommandCentreNUmber = function(req, fn){
    var connection = connections.connection;
     var query = "select value from app_settings where fieldName='centre_number' order by created_date desc limit 1";
     connection.query(query, function(err,res){
            if(!err){
                    if(res!=undefined)
                            fn("success",res[0].value);
            }else{
                    fn("err",err)
            }
     })
}
exports.getPrivacyPolicy = function(req, fn){
    var connection = connections.connection;
     var query = "select privacy_and_policy from privacy_policy order by created_datetime desc limit 1;";
     connection.query(query, function(err,res){
            if(!err){
                    if(res!=undefined)
                            fn("success",res[0]);
            }else{
                    fn("err",err)
            }
     })
}

exports.getFaqList = function(req,fn){
    var connection = connections.connection;
     var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
     var query = "select question,answer from faq ";
     if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
    	 tokReq['customerApp'] = req.body.customerApp;
    	 tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
    	 query += "where app_id=1";
     }
     if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
    	 tokReq['ambApp'] = req.body.ambulanceApp;
    	 query += "where app_id=0";
     }
    commonMethod.verifyUser(tokReq, function(data,status) {
        if (data == "success") {
        	connection.query(query, function(err,res){
        		if(!err){
        			if(res!=undefined)
        				fn("success",res);
        		}else{
        			fn("err",err)
        		}
        	})
        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    })
}

exports.getCountryList= function(req, fn){
    var connection = connections.connection;
     var query = "select country,code from country_list order by created_datetime desc";
     connection.query(query, function(err,res){
            if(!err){
                    if(res!=undefined)
                            fn("success",res);
            }else{
                    fn("err",err)
            }
     })
}
