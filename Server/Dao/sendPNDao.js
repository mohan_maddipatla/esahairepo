var connections = require("../Util/dbConnection.js");
var sendPn = require("../Util/sendPN.js");
var logger = require("../logging/logger.js");
exports.getDeviceTokensByEvd = function(evd,fn){
	var connection = connections.connection;
        logger.info("INPUTS ", evd);
	connection.query("select gcmToken from app_user where mobile = '"+evd+"'",function(err,res){
		logger.info("res ",res); logger.info("err",err);
		if(!err){
			if(res!=undefined && res.length>0){
				fn(res);
			}
		}else{
			fn(err);
		}
	});
}
exports.getDeviceTokensByEvdIMEI = function(id,fn){
	var connection = connections.connection;
	connection.query("select device_token,ambulance_mobile from ambulance_master where amb_id = "+id+" and is_active = 'Active'",function(err,res){
        if(!err){
                if(res!=undefined && res.length>0){
                        var device = [res[0].device_token];
                        fn(device,res[0].ambulance_mobile);
                }
		}else{
			fn(err);
		}
	});
}

exports.getDeviceTokensByAmbulanceId = function(req,fn){
	var connection = connections.connection;
	if(req.is_driver != null && req.is_driver != undefined && req.is_driver == true){
		var query = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver where status='Online' and driver_id=" + req.id+ ")";

		var ambulance_id= "";
		connection.query(query,function(err,res){

			if(!err){
				if(res!=undefined){
					if(res.length>0){

						ambulance_id=res[0].ambulance_id;
						ambulance_master_id =res[0].id

						var query = "select device_token,ambulance_mobile from ambulance_master where amb_id = "+ambulance_id;
						connection.query(query,function(err,res){

							if(!err){
								if(res!=undefined){
									if(res.length>0){


										var device = [res[0].device_token];
										var query = "update ambulance_driver set status='Offline' WHERE driver_id=" + req.id+ " and id= '"+ambulance_master_id+"' and status='Online'";

										connection.query(query,function(err,res){
											if(!err){
												if(res!=undefined){	
													if(res.length>0){
														fn(device,res[0].ambulance_mobile);

													}
												}
											}else{
												fn(err);
											}
										});


									}
								}
							}else{
								fn(err);
							}
						});

					}
				}
			}else{
				fn(err);
			}
		});
	} else if(req.is_amb == true) {
		var query = "select device_token,ambulance_mobile from ambulance_master where amb_id = "+req.amb_id;

		connection.query(query,function(err,res){
			if(!err){
				if(res!=undefined){
					if(res.length>0){

						var device = [res[0].device_token];

						var query = "update ambulance_driver set status='Offline' WHERE ambulance_id=" + req.amb_id+ " and status='Online'";

						connection.query(query,function(err1,res1){
							if(!err1){
								if(res1!=undefined){
									fn(device,res[0].ambulance_mobile);
								}
							}else{
								fn(err1);
							}
						});
					}
				}
			}else{
				fn(err);
			}
		});


	}
	else{
		var query = "select device_token,ambulance_mobile from ambulance_master where amb_id = "+req;
		connection.query(query,function(err,res){
			if(!err){
				if(res!=undefined){
					if(res.length>0){

						var device = [res[0].device_token];
						fn(device,res[0].ambulance_mobile);
					}
				}
			}else{
				fn(err);
			}
		});
	}
}
exports.sendPN = function(deviceToken,data,message,type,pnObj,ios){
	sendPn.sendPNToAndroidDevices(deviceToken,data,message,type,pnObj,ios);
}

exports.sendPNToTopic = function(topic,data,message){
	var connection = connections.connection;
	connection.query("select topic_name,gcm_certificate from gcm_topics where topic_short_name = '"+topic+"'",function(err,res){
		if(!err){
			if(res!=undefined && res.length>0){
				sendPn.sendGeneralPNToTopics(res[0].topic_name,res[0].gcm_certificate,data,message);
			}
			
		}
	})
}
