var connections = require("../Util/dbConnection.js");
var logger = require("../logging/logger.js");

/*exports.insertAmbulanceEmergency = function(req,fn){
	var connection = connections.connection;
	req.emergencyAttributes = "1,2,3";
	var attributes = req.emergencyAttributes.split(",");
	for(var i=0;i<attributes.length;i++){
		var query = "insert into ambulance_e_attributes(emergency_type_id,ambulance_id) values(";
			query+= attributes[i]+","+req.ambulanceId+")";
		connection.query(query,function(err,res){
			if(!err){
				if(i==attributes.length-1){
					fn("success",res);
				}
			}
		})
	}
}*/

exports.insertAmbulanceEmergency = function(req, fn) {
    var connection = connections.connection;
    //req.emergencyAttributes = "1,2,3";
    console.log("PPPPPPPPPPPPPppp insertAmbulanceEmergency ", req)
    var attributes = req.emergencyAttributes.split(",");
    console.log("OIOI ", attributes);
    var i = 0;
    iterateAttributes();

    function iterateAttributes() {
        if (i < attributes.length) {
            var query = "insert into ambulance_e_attributes(emergency_type_id,ambulance_id) values(";
            query += attributes[i] + "," + req.ambulance_id + ")";
            connection.query(query, function(err, res) {
                console.log("I ", i, "len ", attributes.length)
                if (!err) {
                    if (i == attributes.length - 1) {
                        fn("success", res);
                    };
                    i++;
                    iterateAttributes();
                } else {
                    i++;
                    iterateAttributes();
                }
            })
        }
    }
}
exports.insertGroupEmergency = function(req, fn) {
    var connection = connections.connection;
    var attributes = req.emergencyAttributes.split(",");
    var i = 0;
    iterateAttributes();

    function iterateAttributes() {
        if (i < attributes.length) {
            var query = "insert into group_e_attributes(emergency_type_id,group_id) values(";
            query += attributes[i] + "," + req.group_id + ")";
            connection.query(query, function(err, res) {
                if (!err) {
                    if (i == attributes.length - 1) {
                        fn("success", res);
                    };
                    i++;
                    iterateAttributes();
                } else {
                    i++;
                    iterateAttributes();
                }
            })
        }
    }
}
//insert into ambulance_last status

exports.insertIntoTracking = function(req,fn){
	var connection = connections.connection;
	var query = "insert into tracking(ambulance_id,driver_id,speed,longitude,latitude,geoHashCode,is_booked,booking_id,booking_status,end_longitude,end_latitude,end_geoHashCode,estimated_time_arrival,emergency_latitude,emergency_longitude) values(";
		query += req.ambulanceId+","+req.driverId+",'"
		query += req.speed+"','"+req.longitude+"','"+req.latitude+"','"+req.geoHashCode+"',"
		query += (req.is_booked!=undefined ? req.is_booked:false)+","
		query += (req.booking_id!=undefined?req.booking_id:0)+",'"
		query += (req.booking_status!=undefined?req.booking_status:'')+"','"
		query += (req.end_longitude!=undefined?req.end_longitude:'')+"','"
		query += (req.end_latitude!=undefined?req.end_latitude:'')+"','"
		query += (req.end_geoHashCode!=undefined?req.end_geoHashCode:'')+"','"
		query += (req.estimated_time_arrival!=undefined?req.estimated_time_arrival:'')+"','"
		query += req.emergency_latitude+"','"+req.emergency_longitude+"'"
		query+=  ")";
		console.log("QUEUR ",query);
		connection.query(query,function(err,res){
			if(!err){
				fn("success",res);
			}else{
				console.log("OPOPO ",err)
				fn("err",err);
			}
		})
}
exports.getStatusList = function(req,fn){
    var connection = connections.connection;
    try{
        var query = "select status,short_code from status_list";
        connection.query(query,function(err,res){
            if(!err){
                fn("success",res);
            }else{
                fn(null);
            }
        })
    } catch(e){
        fn(null);
    }
}
exports.insertCustomerEmergencyContacts = function(req, fn) {
    var connection = connections.connection;
    var query = "insert into customer_e_contacts(customer_id,contact_person,relation,mobile_number,is_notify) values(";
    query += req.customer_id + ", '" + req.contact_person + "','" + req.relation + "','" + req.mobile_number + "',"+req.is_notify+")";

    console.log("QUEUR ", query);
    connection.query(query, function(err, res) {
        if (!err) {
            fn("success", res);
        } else {
            console.log("OPOPO ", err)
            fn("err", err);
        }
    })
}

exports.getCustomerEmergencyContacts = function(customer_id,fn){
	 var connection = connections.connection;
	    var query = "select mobile_number from customer_e_contacts where is_notify=true and customer_id="+customer_id;
	    console.log("QUEUR ", query);
	    connection.query(query, function(err, res) {
	        if (!err) {
	           // fn("success", res);
	        	if(res!=undefined){
	        		if(res.length>0){
	        			var contacts = "";
	        			var i =0;
	        			iterateContacts();
	        			function iterateContacts(){
	        				if(i<res.length){
	        					contacts += res[i].mobile_number;
	        					if(i == res.length-1){
	        						contacts += "";
	        						fn("success",contacts);
	        					}else{
	        						contacts+=",";
	        						i++;
	        						iterateContacts();
	        					}
	        				}
	        			}
	        		}else{
	        			fn("noContacts");
	        		}
	        	}
	        } else {
		  logger.info("ERRR",err);
	            fn("err", err);
	        }
	    })
}

exports.updateCustomerEmergencyContacts = function(req, fn) {
    var connection = connections.connection;
    var query = "update customer_e_contacts set contact_person= '"+ req.contact_person +"',relation= '"+ req.relation +"',mobile_number='"+ req.mobile_number+"',is_notify="+req.is_notify;;
    query += " where id=" + req.id;
    console.log("QUEUR ", query);
    connection.query(query, function(err, res) {
        if (!err) {
            fn("success", res);
        } else {
            fn("err", err);
        }
    })
}

exports.insertRejectedAmbulances = function(req,fn){
    var connection = connections.connection;
    var query = "insert into rejected_ambulances(ambulance_id,booking_id,reason) values("+req.ambulance_id+","+req.booking_id+",'"+req.reason+"')";
   connection.query(query, function(err, res) {
        if (!err) {
            fn("success", res);
        } else {
            fn("err", err);
        }
    })
}

exports.getRejectedAmbsByBookingId = function(req,fn){
      var connection = connections.connection;
    var query = "select ambulance_id from rejected_ambulances where booking_id = "+req.booking_id;
   connection.query(query, function(err, res) {
        if (!err) {
            if(res.length>0){
                var rejAmbs = [];
                var i =0;
                iterateAmbs();
                function iterateAmbs(){
                    if(i<res.length){
                        rejAmbs.push(res[i].ambulance_id);
                        if(i == res.length-1){
                            fn("success",rejAmbs);
                        }
                        i++;
                        iterateAmbs();
                    }
                }
            }else
            fn("success", res[0].ambulance_id);
        } else {
            fn("err", err);
        }
    })
}




exports.deleteAmbulanceEmergency = function(req, fn) {
    var connection = connections.connection;
    //req.emergencyAttributes = "1,2,3";
        //var attributes = req.emergencyAttributes.split(",");
    var query = "select * from ambulance_e_attributes where ambulance_id = " + req.ambulance_id;
    connection.query(query, function(err, res) {
            if (!err) {
                //console.log("Result" + JSON.stringify(res));
		if(res.length ==0)
		fn("success", res);
                var i = 0;
                iterateAttributes();

                function iterateAttributes() {
                    if (i < res.length) {
                        var query = "delete from ambulance_e_attributes where id =" + res[i].id; 
                        connection.query(query, function(err, res1) {
                            console.log("I ", i, "len ", res.length)
                            if (!err) {
                                if (i == res.length - 1) {
                                    fn("success", res);
                                };
                                i++;
                                iterateAttributes();
                            } else {
                                i++;
                                iterateAttributes();
                            }
                        })
                    }
                }
            }
        })
}
exports.insertSMSLOG = function(req){
	var connection = connections.connection;
	var query = "insert into sms_log(mobile,message) values('"+req.mobile+"','"+req.message+"')";
	logger.info("insert query in sms log ",query);
	connection.query(query,function(err,res){
		if(!err){
			logger.info("Inserted successfully");
		}else{
			logger.info("Error SMS Log",err);
		}
	});
}


exports.insertPNLOG = function(req){
	var connection = connections.connection;
	var query = "insert into pn_log(message,send_to,pn_type,user_type,status) values('"+req.message+"','"+req.send_to+"','"+req.pn_type+"','";
	query += req.user_type+"','"+req.status+"')";
	logger.info("insert query in pn log ",query);
	connection.query(query,function(err,res){
		if(!err){
			logger.info("Inserted successfully");
		}else{
			logger.info("Error PN Log",err);
		}
	});
}
exports.deleteGroupEmergency = function(req, fn) {
    var connection = connections.connection;
    var query = "select * from group_e_attributes where group_id = " + req.group_id;
    connection.query(query, function(err, res) {
            if (!err) {
                                if(res.length ==0)
                                fn("success", res);
                var i = 0;
                iterateAttributes();

                function iterateAttributes() {
                    if (i < res.length) {
                        var query = "delete from group_e_attributes where id =" + res[i].id;
                        connection.query(query, function(err, res1) {
                            console.log("I ", i, "len ", res.length)
                            if (!err) {
                                if (i == res.length - 1) {
                                    fn("success", res);
                                };
                                i++;
                                iterateAttributes();
                            } else {
                                i++;
                                iterateAttributes();
                            }
                        })
                    }
                }
            }
        })
}

exports.getGroupAttributesById = function(id,fn){
        var connection = connections.connection;
        var query = "select id,attribute_name,attribute_details from group_attributes where group_id="+id;
        connection.query(query, function(err, res) {
        if (!err) {
                if(res.length ==0)
                        fn("noAttr");
                else
                        fn("success", res);
        } else {
          fn("err");
        }
    });
}

exports.getEmergencyContactsById = function(id, fn) {
    var connection = connections.connection;
    var query = "select is_notify from customer_e_contacts where id = "+id;
    connection.query(query, function(err, res) {
        if (!err) {
                if(res.length >0)
                        fn("success", res[0].is_notify);
                else
                        fn("noData");
        } else {
            fn("err", err);
        }
    })
}

exports.getCustomerDeviceTokens = function(fn){
        var connection = connections.connection;
        var query = "select device_type,device_token,customer_mobile_number,customer_id from customer_master where is_active in ('Active','Yes') and loginStatus='loggedIn'";
        connection.query(query,function(err,res){
                if(!err){
                        if(res!=undefined){
                                if(res.length>0){
                                        fn("success",res);
                                }else{
                                        fn("noCustomers");
                                }
                        }
                }else{
                        fn("err");
                }
        })
}

