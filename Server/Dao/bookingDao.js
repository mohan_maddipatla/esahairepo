var connections = require("../Util/dbConnection.js");
var appUserDao = require("../Dao/appUserDao.js");
var statusConstant = require("../Util/statusConstants.json");
var logsDao = require("../Dao/logsDao.js");
var commonConstants = require("../Util/commonConstants.js"),
commonMethod = require("../Util/commonMethod.js");
var statusConstant = require("../Util/statusConstants.json");
var serviceDao = require("../Dao/serviceDao.js");
var multer = require('multer');
var rand = require("random-key");
var fs = require("fs");
var datetime = require("node-datetime");
var sendPNDao = require("../Dao/sendPNDao.js");
var encryptionMob = require("../Util/encryptionMob.js");
var logger = require('../logging/logger.js');
var bookingDao = require("../Dao/bookingDao.js");
var generalDao = require("../Dao/generalDao.js");
var customerDao = require("../Dao/customerDao.js");
var loginDao = require("../Dao/loginDao.js");
var sendemail = require("../Util/sendEmail.js");

var getBookingID = function(fn){
	var query = "select booking_id from booking_id;";
	connection.query(query,function(err,res){
		if(!err){
			if(res!=undefined){
				if(res.length>0){
					var booking = res[0].booking_id.toString();
					if(booking.substring(0,8) == datetime.create().format('Ymd')){
						var splitId = booking.substring(0,8)+pad((parseInt(booking.substring(9,12))+1).toString());
						var bookingId = parseInt(splitId);
						updateBookingId(bookingId,function(sts){
							if(sts=="success")
								fn("bookingId",bookingId);
						});
					}else{

						var  bookingId = datetime.create().format('Ymd').toString()+"0001";						
						updateBookingId(bookingId,function(sts){
							if(sts=="success")
								fn("bookingId",bookingId);
						});
					}
				}else{
					var  bookingId = datetime.create().format('Ymd').toString()+"0001";			
					connection.query("insert into booking_id(booking_id) values("+parseInt(bookingId.toString())+")",function(err,res){
						if(!err){
							fn("bookingId",bookingId);
						}else{
							fn("Err");
						}
					})
				}
			}
		}else
			fn("Err");
	});
}

var updateBookingId = function(booking_id,fn) {
	connection.query("update booking_id set booking_id=" + booking_id + ";", function(err, res) {
		if (!err) {
			logger.info("booking id updated");
			fn("success");
		} else {
			logger.info("Error in updating booking id");
			fn("Err");
		}
	})
}


exports.createBooking = function(req, fn) {
	req.body.tableName = "emergency_booking";
	console.log("TTTTTTTTTT ", req.body, req.body.params.customer_mobile)
	if (req.body.params.booked_by != undefined && req.body.params.booked_by != "Customer") {
		req.body.params.customer_mobile = encryptionMob.encrypt(req.body.params.customer_mobile);
	}else
		req.body.customerApp = true;
	req.body.params.customer_mobile = encryptionMob.decrypt(req.body.params.customer_mobile);
	req.body.params.booking_status = commonConstants.Pending;
	req.body.params.eme_address = req.body.address;
	req.body.params.group_type_id = req.body.group_type_id;
	console.log("OPOPPPPPPPPPPPPPPPPPPPPPPPPP ",req.body.group_id);
	req.body.params.group_id = req.body.group_id;
	req.body.params.emergency_type_id = req.body.emergency_type;
	/*req.body.systemParams = {
			"booking_id": ""
	}*/
//	req.body.params['hospital_latitude']=req.body.hospital_latitude;
	//                     req.body.params['hospital_longitude']=req.body.hospital_longitude;
	//  message['hospital_geoHashCode']=req.body.hospital_geoHashCode;
	if(req.body.address!=null && typeof req.body.address == "string" && req.body.address.indexOf("'")>-1){
		req.body.params.eme_address = req.body.address.replace("'","''");
	}
	if(req.body.params.hospital!=null && typeof req.body.params.hospital == "string" && req.body.params.hospital.indexOf("'")>-1){
                req.body.params.hospital = req.body.params.hospital.replace("'","''");
        }

	/** bookingID **/
	/*   var querySel = "select booking_id,DATE(created_datetime) from emergency_booking where created_datetime in (select * from (select max(created_datetime) from emergency_booking) As p);";
var bookingId;
console.log("QUERY ",querySel);
connection.query(querySel,function(bookErr,bookRes){
console.log("ERR ===========",bookErr);
 if(!bookErr){
    if(bookRes!=undefined){
        logger.info("R0ESUTL T^^^^^^^^^^^^^^^^^^^^^^ ",bookRes);
if(bookRes.length>0){
        if(bookRes[0]['DATE(created_datetime)'] == datetime.create().format('Y-m-d')){
//            var splitId = bookRes[0].booking_id.toString().split(datetime.create().format('Ymd').toString());
        var booking = bookRes[0].booking_id.toString();
        var splitId = booking.substring(0,8)+pad((parseInt(booking.substring(9,12))+1).toString());
                logger.info("SPLIT ID ", splitId);
//            bookingId = splitId[0] + (parseInt(splitId[1].toString()+"000")+1).toString();
                bookingId = splitId;
            req.body.params.booking_id = bookingId;
            insertData()
        }else{
            bookingId = datetime.create().format('Ymd').toString()+"0001";
            req.body.params.booking_id = parseInt(bookingId.toString());
            insertData()
        }
}else{
bookingId = datetime.create().format('Ymd').toString()+"0001";
            req.body.params.booking_id = parseInt(bookingId.toString());
            insertData()

}

   }
 }else{

 }
});
function insertData(){*/
	/** restricting bookings from same customer while is on trip*/
	var cusQuery = "select  * from emergency_booking where customer_id ="+req.body.params.customer_id+" and created_datetime in (select * from (select Max(created_datetime) from emergency_booking where customer_id="+req.body.params.customer_id+") As p);"
	connection.query(cusQuery,function(error,result){
		if(!error){
			if(result!=undefined){
				if(result.length>0){
					//      if(result[0].booking_status in ("Trip Closed","Cancelled","Cancelled (Fake)")){
					if(result[0].booking_status == commonConstants.TripClosed || result[0].booking_status ==commonConstants.Cancelled ||result[0].booking_status == commonConstants.FakeCancel){
						//fn("success", res[0]); 
						booking(function(checkSt,checkData){
							fn(checkSt,checkData);
						});
					}else
						fn("onTrip",result[0].booking_id);
				}else{
//					fn("success", res[0]);
					booking(function(checkSt,checkData){
						fn(checkSt,checkData);
					});

				}
			}
		}else{
		}
	})
	function booking(fn){
		getBookingID(function(idStatus,idNum){
			logger.info("ID IN CREATE ",idStatus+" "+idNum);
			if(idStatus == "bookingId"){
				req.body.params['booking_id'] = idNum;

				appUserDao.insertData(req, function(stat, obj) {
					console.log("OPOPO ",stat,obj);
					if (stat != undefined && stat.bookingId != undefined && stat.bookingId != 0) {
						connection.query("select * from emergency_booking where booking_id = " + stat.bookingId, function(err, res) {
							if (!err) {
								res['customer_deviceToken'] = req.body.gcmToken;
								getCentreNumber(function(numStat,number){
									if(numStat == "success"){
										res['commandCentreNumber'] = number;
										fn("success", res);
										/** send email**/
										var data= "",emailList=commonConstants.To,subject="Booking_id:"+stat.bookingId,html="<p>Customer ("+req.body.params.customer_name+"-"+req.body.params.customer_mobile+") requested for emergency, Booking id is "+stat.bookingId+". </p>";

										sendemail.sendEmail(data,emailList,subject,html);

										//send pn to near by ambulances
										var reqheaders = req.headers;
										var reqbody = {
												"emergencyGeoHash": req.body.params.emergency_geoHashCode,
												"latitude": req.body.params.emergency_latitude,
												"longitude": req.body.params.emergency_longitude,
												"customerApp": req.body.customerApp
										}
										var reqAmb = {
												headers: reqheaders,
												body: reqbody
										}
										var deviceTokens = [];
										var message = {
												booking_id: stat.bookingId,
												latitude: reqbody.latitude,
												longitude: reqbody.longitude,
												customerMobile: (req.body.params.is_self == true)?encryptionMob.decrypt(req.body.customer_mobile):req.body.params.patient_number,
														customerName: req.body.params.customer_name,
														age:req.body.age,
														address:req.body.address,
														emergencyType:req.body.params.emergency_type,
														gcmToken:req.body.gcmToken,
														hospital:req.body.params.hospital,
is_medical_taxi:(req.body.params.emergency_type_id != commonConstants.medicalTaxiId)?false:true
										}
console.log("HOSPITAL ", req.body);
										if(req.body.hospital_latitude!=undefined && req.body.hospital_longitude!=undefined){
//message['drop_location']=false;
											message['hospital_latitude']=req.body.hospital_latitude;
											message['hospital_longitude']=req.body.hospital_longitude;
											message['hospital_geoHashCode']=req.body.hospital_geoHashCode;
										}else{
											message['drop_location']=true;
											message['hospital_latitude']=req.body.params.drop_latitude;
											message['hospital_longitude']=req.body.params.drop_longitude;
											message['hospital_geoHashCode']=req.body.params.drop_geoHashCode;
										}
										console.log("MESSAGTE IN PN ",req.body.group_id,req.body.emergency_type);
										if(req.body.group_id!=undefined && req.body.emergency_type!=undefined){
											console.log("POPOP  request specific ");
											reqAmb.body['group_id'] = req.body.group_id;
											reqAmb.body['emergency_type'] = req.body.emergency_type;
											reqAmb.body['group_type_id'] = req.body.group_type_id;	
											reqAmb.body['customer_mobile'] = (req.body.customer_mobile);
											bookingDao.getNearByAmbulanceListSpecificType(reqAmb, function(status, ambulanceList) {
												logger.info("######################## ", status, ambulanceList);
logger.info("LENGTH___________________ ",ambulanceList.length);
												if (status == "success") {
													if (ambulanceList.length > 0) {
														var ambulanceIds = "";
														var ambulanceNumbers = "";
														var i = 0;
														iterateAmbulances();

														function iterateAmbulances() {
															if (i < ambulanceList.length) {
logger.info("I ",i);
logger.info("AMB ID ", ambulanceList[i].ambulance_id);
if (ambulanceList[i].ambulance_id != undefined) {
var deviceTOKEN = [];
																	var distanceMessage = {};
																	distanceMessage['distance']=ambulanceList[i].distance;
																	distanceMessage['duration']=ambulanceList[i].duration;
logger.info("DISCTANE ", distanceMessage);
connection.query("select device_token,ambulance_mobile from ambulance_master where amb_id=" + ambulanceList[i].ambulance_id, function(err, res) {
logger.info("ERR ",err);
logger.info("RES ",res);
//i++;
//iterateAmbulances();													
					if (!err) {
					if (res != undefined && res.length > 0) {
						deviceTokens.push(res[0].device_token);
																				deviceTOKEN = [res[0].device_token];
																				ambulanceIds += ambulanceList[i].ambulance_id;
																				ambulanceIds += (i==ambulanceList.length-1)?"":",";
																				console.log("AMBKSDFL ",ambulanceIds);
																				ambulanceNumbers += ambulanceList[i].ambulance_number;
																				ambulanceNumbers += (i<ambulanceList.length)?"":",";
				var data = "new emergency is waiting";
			
				logger.info("DATAAA ",i);
				var sendObj = {
						                                                        		"user_type":"Ambulance",
						                                                        		"send_to": res[0].ambulance_mobile
						                                                        }
if(deviceTOKEN.length>0){
getAgeByCustomerId(req.body.params.customer_id,function(statusAge,age){
logger.info("AGE ",statusAge);
if(statusAge == "age"){
logger.info("IN AGE ",age);
message['age'] = age.age ;
message['device_type'] = age.device_type;
                                   			message['distance']=distanceMessage['distance'];
                                       			message['duration']=distanceMessage['duration'];
						                                                        			
						                                                        			sendPNDao.sendPN(deviceTOKEN, data, message, "new_booking",sendObj);
if (i == ambulanceList.length - 1) {
																					
																					var data = "new emergency is waiting";
																				
																					if(req.body.params.emergency_type_id != commonConstants.medicalTaxiId){
																					generalDao.getCustomerEmergencyContacts(req.body.params.customer_id,function(emeStat,emeObj){
																						if(emeStat=="success"){ 
																							var sendObj = {mobilenumber:emeObj,message:req.body.params.customer_name+"("+req.body.params.customer_mobile+") is in emergency at "+req.body.address+". Please call "+commonConstants.commandCentreNumber+" to know more."};
																						loginDao.sendSMS(sendObj,function(smsStat,obj){
																							if(smsStat == "success")
																								logger.info("SMS sent to "+emeObj);
																							else logger.info("Failed to send sms");
																						});

																						}else logger.info("Contsacts ", emeStat);
																					});
																					}
																					
																					var amObj = {
																							ambulance_ids:ambulanceIds,
																							ambulance_numbers:ambulanceNumbers,
																							booking_id:stat.bookingId
																					}
																					logsDao.addAmbulanceListPerBooking(amObj);

																				}else{
		if (i == ambulanceList.length - 1) {
																					
																					var data = "new emergency is waiting";
																				
																					if(req.body.params.emergency_type_id != commonConstants.medicalTaxiId){
																					generalDao.getCustomerEmergencyContacts(req.body.params.customer_id,function(emeStat,emeObj){
																						if(emeStat=="success"){ 
																							var sendObj = {mobilenumber:emeObj,message:req.body.params.customer_name+"("+req.body.params.customer_mobile+") is in emergency at "+req.body.address+". Please call "+commonConstants.commandCentreNumber+" to know more."};
																						loginDao.sendSMS(sendObj,function(smsStat,obj){
																							if(smsStat == "success")
																								logger.info("SMS sent to "+emeObj);
																							else logger.info("Failed to send sms");
																						});

																						}else logger.info("Contsacts ", emeStat);
																					});
																					}
																					
																					var amObj = {
																							ambulance_ids:ambulanceIds,
																							ambulance_numbers:ambulanceNumbers,
																							booking_id:stat.bookingId
																					}
																					logsDao.addAmbulanceListPerBooking(amObj);

																				}else{
				i++;
																				iterateAmbulances();
}
}
}else{
						                                                        			sendPNDao.sendPN(deviceTOKEN, data, message, "new_booking",sendObj);
i++;
iterateAmbulances();
}
});
}
}else{
i++;

                 iterateAmbulances();
}
}else{
i++;

                 iterateAmbulances();
}
});
}
															} /*else if (i == ambulanceList.length - 1) {
																//send pn 
																console.log("OPOPOPOPOPPPPPPPPPPPPPP DEVICE TOKENS ", deviceTokens)
																var data = "new emergency is waiting";
																var sendObj = {
																		"user_type":"Ambulance",
																		"send_to": res[0].ambulance_mobile
																}
																if(deviceTokens.length>0){
			                                                        getAgeByCustomerId(req.body.params.customer_id,function(statusAge,age){
			                                                                if(statusAge == "age"){
				                                                                message['age'] = age;
				                                                                sendPNDao.sendPN(deviceTokens, data, message, "new_booking",sendObj);
			                                                                }else
			                                                                        sendPNDao.sendPN(deviceTokens, data, message, "new_booking",sendObj);
			                                                        });
			                                                       }


																generalDao.getCustomerEmergencyContacts(req.body.params.customer_id,function(emeStat,emeObj){
																	if(emeStat=="success"){ 
																		var sendObj = {mobilenumber:emeObj,message:req.body.params.customer_name+"("+req.body.params.customer_mobile+") is in emergency at "+req.body.address};
																	loginDao.sendSMS(sendObj,function(smsStat,obj){
																		if(smsStat == "success")
																			logger.info("SMS sent to "+emeObj);
																		else logger.info("Failed to send sms");
																	});

																	}else logger.info("Contsacts ", emeStat);
																});
															}*/
														}
													}
												}
											})	


										}// });
									}else{
										fn("centreNumberFail",obj);
									}
								});
							} else {
								fn("err", err)
							}
						})
					}else fn(stat, obj);

				});
			}else{
				logger.info("Error while getting booking id");
			}
		});
	}

//	}
};
var getAgeByCustomerId = function(id,fn){
	var query = "select age,device_type from customer_master where customer_id="+id;
       logger.info("getAgeByCustomerId ", query);
	connection.query(query,function(err,res){
		if(!err){
			if(res!=undefined && res.length>0){
				fn("age",res[0]);
			}else
				fn("err");
		}else
			fn("err");
	});
}

/*exports.assignBooking = function(req, fn) {
   console.log("REQ IN ASSIGN " , req.body);
    req.body.tableName = "emergency_booking";
    req.body.params.booking_status = "Confirmed";
    appUserDao.updateData(req, function(stat, obj) {
	console.log("UUUUUUUUUUUUUUU ",stat, "OBJJ ",obj)
	 if(stat!=undefined && stat.updation==true){
             fn(stat, obj);
            var message = {
                'distance':req.body.params.distance,
                'duration':req.body.params.duration,
                'cost':req.body.params.cost,
                'driverName':req.body.driverName,
                'ambulanceNumber':req.body.vehicleNumber,
                'driverMobileNumber':req.body.driverMobileNumber,
                'driverProfilePic':req.body.driverProfilePic,
                'emergencyLatitude':req.body.emergencyLatitude,
                'emergencyLongitude': req.body.emergencyLongitude,
                'ambulanceHospital':req.body.ambulanceHospital,
		'ambulance_longitude':req.body.params.ambulance_start_longitude,
                'ambulance_latitude':req.body.params.ambulance_start_latitude
            };
            var deviceTokens = []; 
	    deviceTokens = [req.body.deviceToken];
	    console.log("PN TO CUSTOMER ","ztoo ",req.body.deviceToken,deviceTokens, req.body);
            var data = "Your booking is confirmed";
            sendPNDao.sendPN(deviceTokens, data, message, "booking_confirmed");
        }else
       	 fn(stat, obj);
    });
};*/

exports.assignBooking = function(req, fn) {
	req.body.tableName = "emergency_booking";
	req.body.params.booking_status = commonConstants.Accepted;
	var selQuery = "select * from " + req.body.tableName + " where booking_id=" + req.body.conditionalParams.booking_id;
	connection.query(selQuery, function(error, result) {
		if (!error) {
			if (result != undefined) {
				if (result.length > 0) {
					var emergencyObj = result[0];
					console.log("OOOO STATUS ",result[0].booking_status);
					if(result[0]['booking_status'] == commonConstants.Cancelled || result[0]['booking_status'] == commonConstants.FakeCancel){
						fn("cancelled",result);
					}else if(result[0]['booking_status'] == commonConstants.TripClosed || result[0]['booking_status'] == commonConstants.ReachedDestination){
                        fn("closed",result);
                    } 
					else if (result[0]['booking_status'] != commonConstants.Pending && result[0]['booking_status'] != commonConstants.NoOneAccepted && result[0]['booking_status']!=commonConstants.TransferredNoResponse) {
						fn("alreadyBooked", result);
					}     
					else {
						appUserDao.updateData(req, function(stat, obj) {
							console.log("STAT ",stat,"OBJJJ ",obj);
							if (stat != undefined && stat.updation == true) {

								var query = "UPDATE ambulance_driver SET is_assigned=true WHERE ambulance_id=" + req.body.params.ambulance_id + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.params.ambulance_id + ") AS p )";
								connection.query(query, function(err, res) {
									if (!err) {
										var track = {
												'distance': req.body.params.distance,
												'estimated_time_arrival': req.body.params.duration,
												'cost': req.body.params.cost,
												'driverId':req.body.driverId,
												'booking_status':req.body.params.booking_status,
												'ambulanceId': req.body.params.ambulance_id,
												'speed':'',
												'emergency_latitude': req.body.emergencyLatitude,
												'emergency_longitude': req.body.emergencyLongitude,
												'longitude': req.body.params.ambulance_start_longitude,
												'latitude': req.body.params.ambulance_start_latitude,
												'geoHashCode':req.body.params.geoHashCode,
												'is_booked':true,
												'booking_id':req.body.conditionalParams.booking_id,
												'end_latitude':req.body.hospital_latitude,
												'end_longitude':req.body.hospital_longitude
										}; console.log("TRACK ",track);
										generalDao.insertIntoTracking(track, function(stat, data) {
											console.log("INININI TRACIKING ", stat, "KKK ", data);
											if (stat == "success") {
												// fn(stat, obj);
												/** insert into accepted_ambulances**/
												var acceptedQuery = "insert into accepted_ambulances(ambulance_id,booking_id) values("+req.body.params.ambulance_id+","+req.body.conditionalParams.booking_id+")"
												connection.query(acceptedQuery,function(acceErr,acceObj){
													if(!acceErr){
														//	fn(stat, obj);
														getCentreNumber(function(numStat,number){
															if(numStat == "success"){
																fn(stat,number);

																var message = {
																		'distance': req.body.params.distance,
																		'duration': req.body.params.duration,
																		'cost': req.body.params.cost,
																		'driverName': req.body.driverName,
																		'ambulanceNumber': req.body.vehicleNumber,
																		'driverMobileNumber': req.body.driverMobileNumber,
																		'driverProfilePic': req.body.driverProfilePic,
																		'emergencyLatitude': req.body.emergencyLatitude,
																		'emergencyLongitude': req.body.emergencyLongitude,
																		'ambulanceHospital': req.body.ambulanceHospital,
																		'ambulance_longitude': req.body.params.ambulance_start_longitude,
																		'ambulance_latitude': req.body.params.ambulance_start_latitude,
'is_medical_taxi':(emergencyObj.emergency_type_id != commonConstants.medicalTaxiId)?false:true

																};
																var deviceTokens = [];
																deviceTokens = [req.body.deviceToken];

																var data = "Your booking is confirmed";
																var type = "booking_confirmed";
																if(req.body.pn_type == "ambulance_transferred_booking")
																	type = "amb_transferred_booking_confirmed";
																if((req.body.ambulanceApp==undefined || (req.body.ambulanceApp==false || req.body.ambulanceApp=="false")))
																	type ="transferred_booking_confirmed";
																var sendObj = {
																		"user_type":"Customer",
																		"send_to": emergencyObj.customer_mobile
																}
																//sendPNDao.sendPN(deviceTokens, data, message, type,sendObj);
if (req.body.device_type == "Android")
    sendPNDao.sendPN(deviceTokens, data, message, type, sendObj);
else
    sendPNDao.sendPN(deviceTokens, data, message, type, sendObj, true);

																//sendPNDao.sendPN(deviceTokens, data, message, type);
																//send cancel pn to other ambulances
																/** ***/
																var reqheaders = req.headers;
																var reqbody = {
																		"emergencyGeoHash": emergencyObj.emergency_geoHashCode,
																		"latitude": emergencyObj.emergency_latitude,
																		"longitude": emergencyObj.emergency_longitude,
																		"ambulanceApp": req.body.ambulanceApp
																}
																var reqAmb = {
																		headers: reqheaders,
																		body: reqbody
																}
																reqAmb.body['group_id'] = emergencyObj.group_id;
																reqAmb.body['emergency_type'] = emergencyObj.emergency_type_id;
																reqAmb.body['group_type_id'] = emergencyObj.group_type_id;
																reqAmb.body['customer_mobile'] = (req.body.customer_mobile);
																console.log("EROIEOSIURSUERS ",reqAmb);													bookingDao.getNearByAmbulanceListSpecificType(reqAmb, function(status, ambulanceList) {
																	var nearDeviceTokens = [];
																	console.log("######################## ", status,ambulanceList.length);
																	if (status == "success") {
																		if (ambulanceList.length > 0) {
																			var i = 0;
																			iterateAmbulances();
																			function iterateAmbulances() {
																				if (i < ambulanceList.length) {
																					if (ambulanceList[i].ambulance_id != undefined) {
																						connection.query("select * from accepted_ambulances where booking_id="+req.body.conditionalParams.booking_id,function(bookErr,bookObj){
																							console.log("EWRERRR Accepted ",bookErr, bookObj);
																							if(!bookErr){
																								if(bookObj!=undefined){
																									if(bookObj.length >0){
																										console.log("KKKKKKKKKKKKKKKKKKK ");
																										if(ambulanceList[i].ambulance_id!=bookObj[0].ambulance_id){
																											console.log("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
																											var mobile = "";
																											connection.query("select ambulance_mobile,device_token from ambulance_master where amb_id=" + ambulanceList[i].ambulance_id, function(err, res) {
																												if (!err) {
																													// console.log("%%%%%% ", res[0].device_token);
																													if (res != undefined && res.length > 0) {
																														nearDeviceTokens.push(res[0].device_token);
																														 mobile += res[0].ambulance_mobile;
mobile += (i == ambulanceList.length - 1)?"":",";

																														if (i == ambulanceList.length - 1) {
																															//send pn 
																															// console.log("OPOPOPOPOPPPPPPPPPPPPPP ", message)
																															var dataT = "other ambulance has accepted";
																															var messageT = {
																																	booking_id:req.body.conditionalParams.booking_id
																															};
																															console.log("NEAEDEVICETOKENS ",nearDeviceTokens);																		
																															var sendObj = {
																																	"user_type":"Ambulance",
																																	"send_to": mobile
																															}

																															if (nearDeviceTokens.length > 0)
																																sendPNDao.sendPN(nearDeviceTokens, dataT, messageT, "other_accepted",sendObj);

																														}
																														i++;
																														iterateAmbulances();
																													} else {
																														i++;
																														iterateAmbulances();
																													}
																												} else {
																													i++;
																													iterateAmbulances();
																												}
																											})
																										}else{
																											i++;
																											iterateAmbulances();
																										}
																									}
																								}
																							}
																						})

																					} else {
																						i++;
																						iterateAmbulances();
																					}
																				} /*else if (i == ambulanceList.length - 1) {
																					//send pn 
																					//          console.log("OPOPOPOPOPPPPPPPPPPPPPP ", message)
																					var dataT = "other ambulance has accepted";
																					var messageT = {
																							booking_id:req.body.conditionalParams.booking_id
																					};
																					var sendObj = {
																							"user_type":"Ambulance",
																							"send_to": mobile
																					}
																					if (nearDeviceTokens.length > 0)
																						sendPNDao.sendPN(nearDeviceTokens, dataT, messageT, "other_accepted",sendObj);

																				}*/
																			}
																		}
																	}
																})
															}else{
																fn("centreNumberFail",obj);
															}
														});
													}else{
														fn("assignedButUpdateFail",data);
													}
												});
											}else{
												fn("assignedButUpdateFail",data);
											}
										})
									} else {
										fn("err", err);
									}
								})



							} else
								fn(stat, obj);
						});
					}
				}
			}
		}
	})

};
var getHospitalDetailsByName = function(req,fn){
	var connection = connections.connection;
	var query = "select * from group_master where group_name = "+req.hospital+"";
	connection.query(query,function(err,res){
		if(!err){
			if(res.length>0)
				fn("success",res);
			else
				fn(null);
		}else{
			fn(null)
		}
	})
}

exports.updateBooking = function(req, fn) {
	req.body.tableName = "emergency_booking";
	if (req.body.params.booked_by != undefined && req.body.params.booked_by != "Customer") {
		req.body.params.customer_mobile = encryptionMob.encrypt(req.body.params.customer_mobile);
	}
	appUserDao.updateData(req, function(stat, obj) {
		fn(stat, obj);
	});
};

exports.getAmbulanceList = function(req, fn) {
	req.body.tableName = "ambulance_driver";
	appUserDao.getDataByField(req, function(stat, obj) {
		fn(stat, obj);
	});
};

/* Near By Ambulance List By Hospital and EmergencyType*/
exports.getNearByAmbulanceListSpecific = function(req, fn) {
	var connection = connections.connection;
	try {
		if (req.body.latitude != undefined && req.body.longitude != undefined && req.body.emergencyGeoHash) {
			var tokReq = {
					token: req.headers.token,
					userId: req.headers.userid,
					method: req.method
			}
			if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
				tokReq['ambApp'] = req.body.ambulanceApp;
			}
			if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
				tokReq['customerApp'] = req.body.customerApp;
			}
			/***emergency TYPes**/
			var emergency_type = null;
			if (req.body.emergency_type != undefined && req.body.emergency_type != null) {
				emergency_type = req.body.emergency_type;
			}
			var groupFilter = null;
			if (req.body.group_id != undefined && req.body.group_id != null) {
				groupFilter = req.body.group_id;
			}
			console.log("SPECIFC ",req.body);
			commonMethod.verifyUser(tokReq, function(data, obj) {
				data = "success";
				if (data == "success") {
					var eme_geoHash = req.body.emergencyGeoHash;
					eme_geoHash = eme_geoHash.toString().substring(0, 3);
					console.log("OPOP GEO HASH ", eme_geoHash);
					// var query = "select * from ambulance_driver";// where geoHashCode like '"+eme_geoHash+"%'
					//  var query = "select * from ambulance_driver where geoHashCode like '" + eme_geoHash + "%' order by created_date desc "
					var query = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver where geoHashCode like '" + eme_geoHash + "%' and status ='Online' and is_assigned = false  group by ambulance_id order by created_date desc )";
					console.log("QUERY ", query);
					var nearestAmbulances = [];
					connection.query(query, function(err, res) {
						console.log("ERROR ", err);
						console.log("RES ", res.length);
						if (!err) {
							if (res != undefined) {
								if (res.length > 0) {
									console.log("********************************************")
									var origin_latlong = req.body.latitude + "," + req.body.longitude;
									var origins = [];
									origins.push(origin_latlong);
									console.log("ORIGINI ", origins);
									var i = 0;
									var count = 0;
									iterateAmbulances();

									function iterateAmbulances() {

										if (i < res.length) {
											getGroupByAmbulanceId(res[i].ambulance_id, function(stat, details) {
												if (stat == "success") {
													console.log("DETAILS ", details);
													if (details.group_id == req.body.group_id) {
														count = count + 1;
														getEmergencyTypePerAmbulance(res[i].ambulance_id, req.body.emergency_type, "ambulance", function(status, response) {
															console.log("EMRGENCY ", response, status);
															if (status == "success") {
																if (res[i].latitude != undefined && res[i].longitude != undefined) {
																	latlong = res[i].latitude + "," + res[i].longitude;
																	var destinations = [];
																	destinations.push(latlong);
																	console.log("ORIGINIS ", origins);
																	console.log("destinations ", destinations)
																	// destinations = ['17.4444,78.3861'];

																	commonMethod.calculateDistance(origins, destinations, function(status, distanceDurationArray) {
																		console.log("DIDJI ", distanceDurationArray);
																		if (status == "distance") {
																			var maxDistance = commonConstants.distanceLimit;
																			for (var j = 0; j < distanceDurationArray.length; j++) {

																				if (distanceDurationArray[j].distance <= maxDistance) {
																					res[i]['distance'] = distanceDurationArray[j].distance.toFixed(1);
																					res[i]['duration'] = distanceDurationArray[j].duration;
																					getAmbNumberById(res[i].ambulance_id,function(stat){
																						res[i]['ambulance_number'] = stat;
																						nearestAmbulances.push(res[i]);
																						if (i == res.length - 1) {
																							fn("success", nearestAmbulances);
																						}
																						i++;
																						iterateAmbulances();
																					});
																				} else {
																					getAmbNumberById(res[i].ambulance_id,function(stat){
																						res[i]['ambulance_number'] = stat;
																						if (i == res.length - 1) {
																							fn("success", nearestAmbulances);
																						}
																						i++;
																						iterateAmbulances();
																					});
																				}

																			}
																		} else {
																			getAmbNumberById(res[i].ambulance_id,function(stat){
																				res[i]['ambulance_number'] = stat;
																				if (i == res.length - 1) {
																					fn("success", nearestAmbulances);
																				}
																				i++;
																				iterateAmbulances();
																			});
																		}
																	});

																} else {
																	getAmbNumberById(res[i].ambulance_id,function(stat){
																		res[i]['ambulance_number'] = stat;
																		if (i == res.length - 1) {
																			fn("success", nearestAmbulances);
																		}
																		i++;
																		iterateAmbulances();
																	});
																}
															} else if (status == "noAttr") {
																console.log("PPPP (((((((((((((((((( ");
																if (i == res.length - 1) {
																	//if (count == 0) {
																	fn("noAmbulances");
																	// }
																}
																i++;
																iterateAmbulances();
															} else {
																if (i == res.length - 1) {
																	// if (count == 0) {
																	fn("noAmbulances");
																	//  }
																}
																i++;
																iterateAmbulances();
															}
														})
													} else {

														if (i == res.length - 1) {
															console.log("PPPOOO ",count);
															if (count == 0) {
																fn("noAmbulances");
															}else{
//																fn("success", nearestAmbulances);
																//getAmbNumberById(res[i].ambulance_id,function(stat){
																//		console.log("STAT ",stat, "&&&&& %%% ",res[i]);
																//						res[i]['ambulance_number'] = stat;
																fn("success", nearestAmbulances);


																//						});
															}
														}
														i++;
														iterateAmbulances();

													}

												} else if (stat == "noAmb") {
													fn("noAmbulances");
												} else {
													fn("err", details);
												}
											});

										}

									}
								} else {
									fn("noAmbulances", res);
								}
							}
						} else {
							fn("err", err)
						}
					})
				} else if (data == "err") {
					fn("Unauthorized");
				} else if (data == "Invalid token" || data == 'No User') {
					fn(data, obj);
				}
			});
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}

}
var getEmergencyTypePerAmbulance = function(ambId, emeTypes, type, fn) {
	var query = null;
	if (type == "ambulance")
		query = "select emergency_type_id from ambulance_e_attributes where ambulance_id = " + ambId;
	else
		query = "select emergency_type_id from group_e_attributes where ambulance_id = " + ambId;
	console.log("QUERY ", query)
	connection.query(query, function(err, res) {
		console.log("IOIO ", res);
		if (!err) {
			if (res != undefined) {
				if (res.length == 0) {
					fn("noAttr", 0);
				} else {

					var eTypes = res;
					var i = 0;
					var count = 0;
					iterateTypes();
					console.log("EME TYPES ",emeTypes);

					function iterateTypes() {
						if (i < eTypes.length) {
							//        console.log("III ",eTypes[i].emergency_type_id,emeTypes.indexOf(eTypes[i].emergency_type_id))
							//if (emeTypes.indexOf(eTypes[i].emergency_type_id)>-1) {
							if(emeTypes == eTypes[i].emergency_type_id){
								count += 1;
								if (i == eTypes.length - 1) {
									fn("success");
								}
								i++;
								iterateTypes();
							} else {
								if (i == eTypes.length - 1) {
									if(count > 0)
										fn("success");
									else
										fn("noAttr")
								}
								i++;
								iterateTypes();
							}
						}
					}
				}
			}
		} else {
			console.log("ERRROR ", err)
			fn("err");
		}
	})
}
var getGroupByAmbulanceId = function(ambId, fn) {
	var query = "select * from ambulance_master where amb_id = " + ambId+" and login_status ='LoggedIn'";
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				if (res.length == 0) {
					fn("noAmb", 0);
				} else {
					fn("success", res[0]);
				}
			}
		} else {
			fn(null);
		}
	})
}

exports.getNearByAmbulanceList = function(req, fn) {
	var connection = connections.connection;
	try {
		if (req.body.latitude != undefined && req.body.longitude != undefined && req.body.emergencyGeoHash) {
			var tokReq = {
					token: req.headers.token,
					userId: req.headers.userid,
					method: req.method
			}
			if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
				tokReq['ambApp'] = req.body.ambulanceApp;
			}
			if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
				tokReq['customerApp'] = req.body.customerApp;
			}
			commonMethod.verifyUser(tokReq, function(data, obj) {
				data = "success";
				if (data == "success") {
					var eme_geoHash = req.body.emergencyGeoHash;
					eme_geoHash = eme_geoHash.toString().substring(0, 3);
					//            console.log("OPOP GEO HASH ", eme_geoHash);
					// var query = "select * from ambulance_driver";// where geoHashCode like '"+eme_geoHash+"%'
					//            var query = "select * from ambulance_driver where geoHashCode like '" + eme_geoHash + "%' order by created_date desc"
					var query = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver where geoHashCode like '" + eme_geoHash + "%' and status ='Online' and is_assigned = false group by ambulance_id order by created_date desc )";
					var nearestAmbulances = [];
					connection.query(query, function(err, res) {
						//  console.log("ERROR ", err);
						// console.log("RES ", res.length);
						if (!err) {
							if (res != undefined) {
								if (res.length > 0) {
									console.log("********************************************")
									var origin_latlong = req.body.latitude + "," + req.body.longitude;
									var origins = [];
									origins.push(origin_latlong);
									console.log("ORIGINI ", origins);
									var i = 0;
									iterateAmbulances();

									function iterateAmbulances() {
										if (i < res.length) {
											if (res[i].latitude != undefined && res[i].longitude != undefined) {
												latlong = res[i].latitude + "," + res[i].longitude;
												var destinations = [];
												destinations.push(latlong);
												commonMethod.calculateDistance(origins, destinations, function(status, distanceDurationArray) {
													console.log("DIDJI ", distanceDurationArray);
													if (status == "distance") {
														var maxDistance = commonConstants.distanceLimit;
														for (var j = 0; j < distanceDurationArray.length; j++) {

															if (distanceDurationArray[j].distance <= maxDistance) {
																res[i]['distance'] = distanceDurationArray[j].distance.toFixed(1);
																res[i]['duration'] = distanceDurationArray[j].duration;
																getAmbNumberById(res[i].ambulance_id,function(stat){
																	res[i]['ambulance_number'] = stat;
																	nearestAmbulances.push(res[i]);
																	if (i == res.length - 1) {
																		fn("success", nearestAmbulances);
																	}
																	i++;
																	iterateAmbulances();
																});
															} else {
																getAmbNumberById(res[i].ambulance_id,function(stat){
																	res[i]['ambulance_number'] = stat;
																	if (i == res.length - 1) {
																		fn("success", nearestAmbulances);
																	}
																	i++;
																	iterateAmbulances();
																});

															}
														}
													} else {
														getAmbNumberById(res[i].ambulance_id,function(stat){
															res[i]['ambulance_number'] = stat;
															if (i == res.length - 1) {
																fn("success", nearestAmbulances);
															}
															i++;
															iterateAmbulances();
														});


													}
												});

											} else {
												getAmbNumberById(res[i].ambulance_id,function(stat){
													res[i]['ambulance_number'] = stat;
													if (i == res.length - 1) {
														fn("success", nearestAmbulances);
													}
													i++;
													iterateAmbulances();
												});

											}


										}
									}
								} else {
									fn("noAmbulances", res);
								}
							}
						} else {
							fn("err", err)
						}
					})
				} else if (data == "err") {
					fn("Unauthorized");
				} else if (data == "Invalid token" || data == 'No User') {
					fn(data, obj);
				}
			});
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}

}


/*Near By Hospitals List*/
exports.getNearByHospitalList = function(req, fn) {
	var connection = connections.connection;
	try {
		if (req.body.latitude != undefined && req.body.longitude != undefined && req.body.emergencyGeoHash) {
			var tokReq = {
					token: req.headers.token,
					userId: req.headers.userid,
					method: req.method
			}
			if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
				tokReq['ambApp'] = req.body.ambulanceApp;
			}
			if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
				tokReq['customerApp'] = req.body.customerApp;
				tokReq['customerMobile']=encryptionMob.decrypt(req.body.customer_mobile);
			}
			var group_type = req.body.groupType;
			commonMethod.verifyUser(tokReq, function(data, obj) {
				data = "success";
				if (data == "success") {
					var eme_geoHash = req.body.emergencyGeoHash;
					eme_geoHash = eme_geoHash.toString().substring(0, 3);
					console.log("OPOP GEO HASH ", eme_geoHash);
					
					//var query = "select * from group_master where is_active='Active' and  group_type="+commonConstants.hospitalGroupTypeId+" and geoHashCode like '" + eme_geoHash + "%' order by geoHashCode+"+0;
					var query = "select a.*,b.emergency_type_id from group_master a , group_e_attributes b where a.group_id=b.group_id and is_active='Active' and group_type="+commonConstants.hospitalGroupTypeId+" and geoHashCode like '" + eme_geoHash + "%' group by a.group_id order by geoHashCode+"+0;
                    if(req.body.emergency_type!=undefined)
                      query = "select a.*,b.emergency_type_id from group_master a , group_e_attributes b where a.group_id=b.group_id and b.emergency_type_id="+req.body.emergency_type+" and is_active='Active' and group_type="+commonConstants.hospitalGroupTypeId+" and geoHashCode like '" + eme_geoHash + "%' order by geoHashCode+"+0; 
					logger.info("Hospital list query ::  ",query);
					var nearestAmbulances = [];
					connection.query(query, function(err, res) {
						console.log("ERROR ", err);
						console.log("RES ", res.length);
						if (!err) {
							if (res != undefined) {
								if (res.length > 0) {
									console.log("********************************************")
									var origin_latlong = req.body.latitude + "," + req.body.longitude;
									var origins = [];
									origins.push(origin_latlong);
									console.log("ORIGINI ", origins);
									var i = 0;
									iterateHospitals();

									function iterateHospitals() {
										if (i < res.length) {
											console.log("RES I ",res[i]);
											if (res[i].group_latitude != undefined && res[i].group_longitude != undefined) {
												latlong = res[i].group_latitude + "," + res[i].group_longitude;
												var destinations = [];
												destinations.push(latlong);
												console.log("DESTINATIONS ",destinations);
												commonMethod.calculateDistanceForNearByHospitals(origins, destinations, function(status, distanceDurationArray) {
													console.log("DISTANCE ",status, distanceDurationArray);
													if (status == "distance") {
//														var maxDistance = commonConstants.distanceLimit;
														for (var j = 0; j < distanceDurationArray.length; j++) {

//															if (distanceDurationArray[j].distance <= maxDistance) {
																res[i]['distance'] = distanceDurationArray[j].distance.toFixed(1);
//																res[i]['distance'] = res[i]['distance'];
																res[i]['duration'] = distanceDurationArray[j].duration;
generalDao.getGroupAttributesById(res[i].group_id,function(stat,attributes){

 if(stat == "success"){

         res[i]['attributes'] = attributes;

         nearestAmbulances.push(res[i]);

 }else if(stat == "noAttr"){

//         res[i]['attributes'] = "No Attributes";
	res[i]['attributes'] = [];

         nearestAmbulances.push(res[i]);

 }else{

         logger.info("Error occured while getting attributes of group id ",res[i].group_id);

         res[i]['attributes'] = [];
        res[i]['attributeErr'] = true;
                                                                                                                                                        nearestAmbulances.push(res[i]);

 }
                                                                                                                                                

 if (i == res.length - 1) {

         fn("success", nearestAmbulances.sort());


 }

 i++;

 iterateHospitals();
                                                                                                                                        });

													/*			nearestAmbulances.push(res[i]);
																if (i == res.length - 1) {
																	fn("success", nearestAmbulances.sort());

																}
																i++;
																iterateHospitals();*/
															/*} else {
																if (i == res.length - 1) {
																	fn("success", nearestAmbulances.sort());
																}
																i++;
																iterateHospitals();
															}*/
														}
													} else {
														if (i == res.length - 1) {
															fn("success", nearestAmbulances.sort());

														}
														i++;
														iterateHospitals();

													}
												});

											} else {
												if (i == res.length - 1) {
													console.log("NEART ", nearestAmbulances);
													fn("success", nearestAmbulances.sort());
												}
												i++;
												iterateHospitals();
											}


										}
									}
								} else {
									fn("noHospitals", res);
								}
							}
						} else {
							fn("err", err)
						}
					})
				} else if (data == "err") {
					fn("Unauthorized");
				} else if (data == "Invalid token" || data == 'No User') {
					fn(data, obj);
				}
			});
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}

}

exports.getEmergencyContactsForCustomer = function(req, fn) {
	var connection = connections.connection;
	try {
		if (req.body.customerId != undefined && req.body.customerId != "") {
			req.body.tableName = "customer_e_contacts";
			req.body.params = {
					customer_id: req.body.customerId
			}
	/*		appUserDao.getDataByField(req, function(stat, obj) {
				fn(stat, obj);
			});*/
			var tokReq = {
                                        token: req.headers.token,
                                        userId: req.headers.userid,
                                        method: req.method
                        }

                        if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
                                tokReq['customerApp'] = req.body.customerApp;
                                tokReq['customerMobile']= encryptionMob.decrypt(req.body.customer_mobile);
                        }
                        commonMethod.verifyUser(tokReq, function(data, obj) {
                                if (data == "success") {
                                        var query = "select id,contact_person,mobile_number,is_notify,relation from customer_e_contacts where customer_id="+req.body.customerId;
                                        connection.query(query,function(err,res){
                                                if(!err){
                                                        if(res!=undefined){
                                                                if(res.length>0){
                                                                        fn("success",res);
                                                                }else{
                                                                        fn("success",res);
                                                                }
                                                        }
                                                }else{
                                                        fn("err");
                                                }
                                        })
                                } else if (data == "err") {
                                        fn("err");
                                } else if (data == "Invalid token" || data == 'No User') {
                                        fn(data, obj);
                                }else if (data == "Unauthorized") {
                                        fn("Unauthorized");
                                } else{
                                        fn(data,obj);
                                }
                        });

		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}
}

/*exports.updateCustomerProfile = function(req, fn) {
    var connection = connections.connection;
    try {
        if (req.body.customerId != undefined && req.body.customerName != undefined && req.body.gender != undefined && req.body.age != undefined) {
            req.body.tableName = "customer_master";
            req.body.params = {
                customer_name: req.body.customerName,
                gender: req.body.gender,
                age: req.body.age
            }
            req.body.conditionalParams = {
                customer_id: req.body.customerId
            }
            appUserDao.updateData(req, function(stat, obj) {
                console.log("UOUUOO ",stat, "OBJ ",obj);
                fn(stat, obj);
            });
        } else {
            fn("invalidInput");
        }
    } catch (e) {
        fn("err", e);
    }
}*/

exports.updateCustomerProfile = function(req, fn) {
	var connection = connections.connection;
	try {
		if (req.body.customerId != undefined && req.body.customerName != undefined && req.body.gender != undefined && req.body.age != undefined) {
			req.body.tableName = "customer_master";
			console.log("DONEONE ",req.body.donating_blood);
			req.body.params = {
					customer_name: req.body.customerName,
					gender: req.body.gender,
					age: req.body.age,
					blood_group:req.body.blood_group,
					donating_blood:req.body.donating_blood,
					come_to_know_by: req.body.come_to_know_by,
					city: req.body.city,
					state:req.body.state,
					address:req.body.address,
					customer_email:req.body.email,
					pincode:req.body.pincode
			}
			req.body.conditionalParams = {
					customer_id: req.body.customerId
			}
			console.log("OPOP OBDY ", req.body);
			appUserDao.updateData(req, function(stat, obj) {
				// fn(stat, obj);
				if (obj == "") {
					//delete existing ones
					var cusObj = {
							customer_id: req.body.customerId
					}
					customerDao.getCustomerAttributes(cusObj, function(check, response) {
						console.log("PPPPPPPPPPPPPPPPPPPPPP ",check);
						if (check == "success") {
							if (response.length > 0) {
								if (req.body.Attributes!=undefined && req.body.Attributes.length > 0) {
									var i = 0;
									iterateAttributes();

									function iterateAttributes() {
										if (i < req.body.Attributes.length) {
											if (req.body.Attributes[i].id != undefined && req.body.Attributes[i].id != null && req.body.Attributes[i].id != "") {
												//update
												var updateObj = {
														attribute_name: req.body.Attributes[i].attribute_name,
														attribute_details: req.body.Attributes[i].attribute_details,
														customer_id: req.body.customerId,
														customer_e_type_id: req.body.Attributes[i].customer_e_type_id,
														id: req.body.Attributes[i].id
												}
												customerDao.updateCustomerAttributes(updateObj, function(status, result) {
													if (status == "success") {
														if (i == req.body.Attributes.length - 1)
															fn(stat, obj);
														else {
															i++;
															iterateAttributes();
														}
													} else {

														if (i == req.body.Attributes.length - 1)
															fn("err", result);
														else {
															i++;
															iterateAttributes();
														}
													}
												})
											} else {
												//insert
												var insertObj = {
														attribute_name: req.body.Attributes[i].attribute_name,
														attribute_details: req.body.Attributes[i].attribute_details,
														customer_id: req.body.customerId,
														customer_e_type_id: req.body.Attributes[i].customer_e_type_id,
												}
												customerDao.addCustomerAttributes(insertObj, function(status, result) {
													if (status == "success") {
														if (i == req.body.Attributes.length - 1)
															fn(stat, obj);
														else {
															i++;
															iterateAttributes();
														}
													} else {

														if (i == req.body.Attributes.length - 1)
															fn("err", result);
														else {
															i++;
															iterateAttributes();
														}
													}
												})
											}
										}
									}
								} else {
									fn(stat, obj);
								}
							} else {
								/**/
								if (req.body.Attributes!=undefined && req.body.Attributes.length > 0) {
									var i = 0;
									iterateAttributes();

									function iterateAttributes() {
										if (i < req.body.Attributes.length) {
											var insertObj = {
													attribute_name: req.body.Attributes[i].attribute_name,
													attribute_details: req.body.Attributes[i].attribute_details,
													customer_id: req.body.customerId,
													customer_e_type_id: req.body.Attributes[i].customer_e_type_id,
											}
											customerDao.addCustomerAttributes(insertObj, function(status, result) {
												if (status == "success") {
													if (i == req.body.Attributes.length - 1)
														fn(stat, obj);
													else {
														i++;
														iterateAttributes();
													}
												} else {

													if (i == req.body.Attributes.length - 1)
														fn("err", result);
													else {
														i++;
														iterateAttributes();
													}
												}
											})

										}
									}
								} else {
									fn(stat, obj);
								}
							}
						}
					})
				}
			});
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}
}

/*exports.saveEmergencyContactsByCustomerId = function(req,fn){
    var connection = connections.connection;
}*/

var getAmbNumberById = function(id,fn){
	var connection = connections.connection;
	var query = "select ambulance_number from ambulance_master where amb_id="+id;
	console.log("AMBULANCE NUBER ",query);
	connection.query(query,function(err,res){
		if(!err){
			if(res!=undefined){ console.log("NUMBER ",res);
			if(res.length>0)
				fn(res[0].ambulance_number);
			}
		}else
			fn(null);
	});
}


exports.cancelBooking = function(req, fn) {
	var connection = connections.connection;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	};
	//var reason = "";
	var reason = commonConstants.Cancelled;
	var user_type = "Admin";
	if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
		tokReq['ambApp'] = req.body.ambulanceApp;
		//reason = " (Fake)";
		reason = commonConstants.FakeCancel;
		user_type="Ambulance";
	}
	if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
		tokReq['customerApp'] = req.body.customerApp;
		tokReq['customerMobile']=encryptionMob.decrypt(req.body.customer_mobile);
		user_type = "Customer";
	}
	try {
		if (req.body.booking_id != undefined && req.body.booking_id != null) {
			var pnData = "Booking(" + req.body.booking_id + ") is cancelled";
			//    var message = "Booking cancelled";
			var message = {
					booking_id:req.body.booking_id
			}
			var type = "cancel_booking";
			if((req.body.ambulanceApp==undefined && req.body.ambulanceApp!=true && req.body.ambulanceApp!="true") && (req.body.customerApp == undefined && req.body.customerApp != true && req.body.customerApp != "true")){
                type="command_centre_cancelled_booking";
        }
			var selQuery = "select * from emergency_booking where booking_id = " + req.body.booking_id;
			commonMethod.verifyUser(tokReq, function(data, obj) {
				// data = "success";
				if (data == "success") {
					connection.query(selQuery, function(error, result) {
						if (!error) {
							if (result != undefined) {
								var emergeObj = result[0];
								if (result[0].booking_status == commonConstants.Pending  || result[0].booking_status == commonConstants.TransferredNoResponse || result[0].booking_status == commonConstants.NoOneAccepted || result[0].booking_status == commonConstants.NoAmbulanceFound) {
console.log("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH ");
									/*update cancel and send pn to customer*/
									var bStatus = reason;
									updateEmergencyBooking(req.body.booking_id,bStatus, function(status, record) {
	console.log("UPDATE EMERGENCY BOOKING ", status,record);
										if (status == "success" || status == "noTrack") {
											//fn("success", record);
											/*send pn to customer*/
											var obj = {
													tableName: "customer_master",
													customer_id: req.body.customer_id==undefined?result[0].customer_id:req.body.customer_id
											}
											if (req.body.customerApp==undefined || (req.body.customerApp != undefined && (req.body.customerApp == false || req.body.customerApp == "false"))) {
console.log("KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK ");
												getDeviceTokenById(obj, function(status, data) {
													if (status == "success") {
														var deviceToken = [];
														deviceToken = [data.device_token];
														var sendObj = {
																user_type:user_type,
																send_to:emergeObj.customer_mobile
														}
	     											//		sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj);
														if(data.device_type == "Android")
                                                    sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj);
                                                else
                                                    sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj,true);
													} else {
														fn("errPN", data);
													}
												})
											}
											/** send pn to ambualnces as emergency is cancelled**/
											var reqheaders = req.headers;
											var reqbody = {
													"emergencyGeoHash": emergeObj.emergency_geoHashCode,
													"latitude": emergeObj.emergency_latitude,
													"longitude": emergeObj.emergency_longitude,
													"customerApp": req.body.customerApp,
													"ambulanceApp":req.body.ambulanceApp
											}
											var reqAmb = {
													headers: reqheaders,
													body: reqbody
											}
											reqAmb.body['group_id'] = emergeObj.group_id;
											reqAmb.body['emergency_type'] = emergeObj.emergency_type_id;
											reqAmb.body['group_type_id'] = emergeObj.group_type_id;
											if(req.body.customerApp == true || req.body.customerApp == "true")
								                reqAmb.body['customer_mobile'] = (req.body.customer_mobile);

											console.log("REQ AMB ",reqAmb);
											var deviceTokens = [];
											var mobile = "";
											fn("success", record) ;
											bookingDao.getNearByAmbulanceListSpecificType(reqAmb, function(status, ambulanceList) {
										//		fn("success", record) ; console.log("LENGTH ",ambulanceList.length,"  LLLL ",status);
												if (status == "success") {
													if (ambulanceList.length > 0) {
														var i = 0;
														iterateAmbulances();
														function iterateAmbulances() {
															if (i < ambulanceList.length) {
																if (ambulanceList[i].ambulance_id != undefined) {
																	connection.query("select device_token,ambulance_mobile from ambulance_master where amb_id=" + ambulanceList[i].ambulance_id, function(err, res) {
																		if (!err) {
																			if (res != undefined && res.length > 0) {
																				deviceTokens.push(res[0].device_token);
																				mobile += ""+res[0].ambulance_mobile;
																				mobile+= (i == ambulanceList.length-1)?"":",";
																				if (i == ambulanceList.length - 1) {
																					//send pn 																					
																					var sendObj = { user_type:user_type,send_to:mobile};                                                                                                                  
																					if (deviceTokens.length > 0)
																						sendPNDao.sendPN(deviceTokens, pnData, message, type,sendObj);

																				}
																				i++;
																				iterateAmbulances();
																			} else {
																				i++;
																				iterateAmbulances();
																			}
																		} else {
																			i++;
																			iterateAmbulances();
																		}
																	})
																} else {
																	i++;
																	iterateAmbulances();
																}
															} else if (i == ambulanceList.length - 1) {
																var sendObj = {user_type:user_type,send_to:mobile};     
																if (deviceTokens.length > 0)
                                                                    sendPNDao.sendPN(deviceTokens, pnData, message, type,sendObj);
															}
														}
													}else{
														fn(status,record);
													}
												}
											});
										}else if(status == "noTrack"){
											fn(status,record);
										} else { console.log("OPOPOPO ",status, record);
										fn("err", record);
										}
									})
								} else if(result[0].booking_status == commonConstants.Cancelled || result[0].booking_status == commonConstants.FakeCancel){
                                        fn("alreadyCancelled",result);
                                }else if(result[0].booking_status == commonConstants.TripClosed || result[0].booking_status == commonConstants.ReachedDestination){
                                        fn("closed",result);
                                }else {
									/*update cancel and send pn to custoimer and ambulance*/
									var bStatus = reason;
									updateEmergencyBooking(req.body.booking_id, bStatus,function(status, record) {
										if (status == "success") {
											var mobile = " Customer-";
											/*send pn to customer and ambulance*/
											FreeAmbulanceFromBooking(result[0].ambulance_id, function(stat, response) {
												if (stat == "success") {
													var obj = {
															tableName: "customer_master",
															customer_id: req.body.customer_id==undefined?result[0].customer_id:req.body.customer_id
													}
													getDeviceTokenById(obj, function(status, data) {
														if (status == "success") {
															var obj1 = {
																	tableName: "ambulance_master",
																	ambulance_id: result[0].ambulance_id
															}
															var deviceToken = [];
																deviceToken.push(data.device_token);
 var device_type = data.device_type;
																mobile += result[0].customer_mobile;
															console.log("DEVICE TOKEN ", deviceToken);
															getDeviceTokenById1(obj1, function(status1, data1) {
																if (status1 == "success") {
																	fn("success", data);
																	if(req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")){

																	}else
																		deviceToken.push(data1.device_token);
                                                                    	mobile += ": Ambulance-"+data1.ambulance_mobile;
                                                                    	var sendObj = {user_type:user_type,send_to:mobile}
                                                                    	//sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj);
									if(user_type=="Customer" && device_type =="Android")
                                                                    sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj);
                                                                    else
                                                                        sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj,true);

																} else {
																	fn("errPN", data);
																}
															})
														} else {
															fn("errPN", data);
														}
													})
												} else {
													fn("errAmbFree", response);
												}
											})

										} else { console.log("IOIOII ",record);
										fn("err", record);
										}
									})
								}
							}
						} else { console.log("LLLLL ",err);
						fn("err", err);
						}
					})
				} else if (data == "err") {
					fn("Unauthorized");
				} else if (data == "Invalid token" || data == 'No User') {
					fn(data, obj);
				}
			});
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		console.log("POPOLPOP ",e)
		fn("err", e);
	}
}
var getDeviceTokenById1 = function(data, fn) {
    var connection = connections.connection;
    var query = "select * from " + data.tableName;
    if (data.tableName == "customer_master") {
            query += " where customer_id=" + data.customer_id;
    } else if (data.tableName == "ambulance_master") {
            query += " where amb_id=" + data.ambulance_id;
    }
    console.log("QUERY in DEVICE TOEN ", query);
    connection.query(query, function(err, res) {
            if (!err) {
                    if (res != undefined) {
                            fn("success", res[0]);
                    }
            } else {
                    fn(null);
            }
    });
}

/*var updateEmergencyBooking = function(id, fn) {
    var query = "update emergency_booking set booking_status = 'Cancelled' where booking_id=" + id;
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined) {
                fn("success", res);
            }
        } else {
            fn("err", err);
        }
    })
}*/

var updateEmergencyBooking = function(id,status, fn) {
	var query = "update emergency_booking set booking_status = '"+status+"' where booking_id=" + id;
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				var qry = "SELECT * from tracking where booking_id=" + id + " and created_date IN (   SELECT * FROM (SELECT Max(created_date) FROM tracking where booking_id=" + id + ") AS p );"
				connection.query(qry, function(err1, res1) {
					if (!err1) {
						if (res1 != undefined) {
							if (res1.length > 0) {
								var obj = res1[0];
								obj.booking_status = status;
								console.log("OBJECT" ,obj);
								var track = {
										'distance': obj.distance,
										'estimated_time_arrival': obj.duration,
										'cost': obj.cost,
										'driverId':obj.driver_id,
										'booking_status':obj.booking_status,
										'ambulanceId': obj.ambulance_id,
										'speed':obj.speed,
										'emergency_latitude': obj.emergency_latitude,
										'emergency_longitude': obj.emergency_longitude,
										'longitude': obj.longitude,
										'latitude': obj.latitude,
										'geoHashCode':obj.geoHashCode,
										'is_booked':obj.is_booked,
										'booking_id':obj.booking_id,
										'end_latitude':obj.end_latitude,
										'end_longitude':obj.end_longitude
								}
								console.log("OPOPOPOP POBJECT In TRADKC ", track)
								generalDao.insertIntoTracking(track, function(stat, data) {
									console.log("INININI TRACIKING ", stat, "KKK ", data);
									if (stat == "success") {
										fn("success", res);
									}
								})
							} else {
								fn("noTrack", res1);
							}
						}
					} else {
						fn("err", err);
					}
				});

			}
		} else {
			fn("err", err);
		}
	})
}

var getDeviceTokenById = function(data, fn) {
	var connection = connections.connection;
	var query = "select device_token,device_type from " + data.tableName;
	if (data.tableName == "customer_master") {
		query += " where customer_id=" + data.customer_id;
	} else if (data.tableName == "ambulance_master") {
		query += " where amb_id=" + data.ambulance_id;
	}
	console.log("QUERY in DEVICE TOEN ", query);
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				//fn("success", res[0].device_token);
				if(data.tableName == "customer_master")
                fn("success", res[0]!=undefined?res[0]:"");
                else
                fn("success", res[0]!=undefined?res[0].device_token:"");
			}
		} else {
			fn(null);
		}
	});
}

var FreeAmbulanceFromBooking = function(id, fn) {
	var query = "UPDATE ambulance_driver SET is_assigned=false WHERE ambulance_id=" + id + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + id + ") AS p )";
	connection.query(query, function(err, res) {
		if (!err) {
			fn("success");
		} else {
			fn(null);
		}

	})
}


/**Trip list**/

exports.getTripList = function(req, fn) {
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		/* var query = "select * from ";
        if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
            tokReq['ambApp'] = req.body.ambulanceApp;
            query += "tracking where ambulance_id = " + req.body.ambulance_id + " and driver_id = "+req.body.driver_id+" and created_date between '" + req.body.startDate + "' and '" + req.body.endDate + "' group by booking_id order by created_date desc";

        }*/
		// var query ="SELECT * from tracking where ambulance_id = " + req.body.ambulance_id + " and driver_id = "+req.body.driver_id+" and created_date IN (   SELECT * FROM (SELECT Max(created_date) FROM tracking group by booking_id) AS p ) group by booking_id";
		var query ="SELECT * from tracking where ambulance_id = " + req.body.ambulance_id + " and driver_id = "+req.body.driver_id+" and created_date IN (   SELECT * FROM (SELECT Max(created_date) FROM tracking where created_date between '" + req.body.startDate + "' and '" + req.body.endDate+"' group by booking_id) AS p ) group by booking_id order by created_date desc";


		console.log("QUERY ",query);
		if (req.body.ambulanceApp == true) {
			if (req.body.startDate != undefined && req.body.startDate != "" && req.body.endDate != undefined && req.body.endDate != "" && req.body.ambulance_id != undefined && req.body.ambulance_id != null) {
				getTrip(query,function(stat,data){
					if (stat == "success") {
						if (data.length > 0) {
							var j = 0;
							iterateTrip();

							function iterateTrip() {
								if (j < data.length) {
									var qu = "select * from driver_master where driver_id = " + data[j].driver_id;
									connection.query(qu, function(err1, res2) {
										if (!err1) {
											if (res2.length > 0) {
												data[j]['driver_profile'] = res2[0].file_url;
												connection.query("select * from emergency_booking where booking_id="+data[j].booking_id,function(error,result1){
													if(!error){
														data[j]['emergency_location']=result1[0].eme_address;
														data[j]['hospital_location']=result1[0].hospital;
														data[j]['distance']=result1[0].distance;
														data[j]['duration']=result1[0].duration;
														data[j]['cost']=result1[0].cost;
														if(j == data.length-1){
															fn(stat, data);
														}
														j++;
														iterateTrip();

													}else{
														data[j]['emergency_location']=0;
														data[j]['hospital_location']=0;
														if(j == data.length-1){
															fn(stat, data);
														}
														j++;
														iterateTrip();


													}
												});
												// fn(stat, data);
												/*	 if(j == data.length-1){
                                                	fn(stat, data);
                                                }
                                                j++;
                                                iterateTrip();*/
											} else {
												data[j]['driver_profile'] = "no driver found";
												connection.query("select * from emergency_booking where booking_id="+data[j].booking_id,function(error,result1){
													if(!error){
														data[j]['emergency_location']=result1[0].eme_address;
														data[j]['hospital_location']=result1[0].hospital;
														if(j == data.length-1){
															fn(stat, data);
														}
														j++;
														iterateTrip();

													}else{
														data[j]['emergency_location']=0;
														data[j]['hospital_location']=0;
														if(j == data.length-1){
															fn(stat, data);
														}
														j++;
														iterateTrip();


													}
												});

											}


										}

									})
								}
							}

						} else {
							fn(stat, data);
						}
					} else {
						fn(stat, data);
					}
				})
			} else {
				fn("invalidInput");
			}
		} 

	} catch (e) {
		console.log("ERR ",e)
		fn("err", e);
	}
}
var getTrip = function(query,fn) {
	console.log("QUERY Y ",query);
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				if (res.length > 0) {
					fn("success", res);
				} else {
					fn("noData", res);
				}
			}
		} else {
			console.log("ERR ",err)
			fn("err", err);
		}
	})
}

exports.getCustomerprofile = function(req, fn) {
	var connection = connections.connection;
	if (req.body.customer_id != undefined && req.body.customer_id != null) {
		try {
			var tokReq = {
					token: req.headers.token,
					userId: req.headers.userid,
					method: req.method
			}
			if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
				tokReq['customerApp'] = req.body.customerApp;
				tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
			}
			commonMethod.verifyUser(tokReq, function(data, obj) {
				if (data == "success") {
					var query = "select * from customer_master where customer_id=" + req.body.customer_id;
					connection.query(query, function(err, res) {
						if (!err) {
							if (res != undefined) {
								if (res.length > 0) {
									//  fn("success", res[0]);
									var customerObj = {
											customer_id:req.body.customer_id
									}
									customerDao.getCustomerAttributes(customerObj,function(status,result){
										if(status!=null){
											res[0]['Attributes'] = result;
											//n("success", res[0]);
											customerDao.getSystemAttributes(customerObj,function(stat,response){
												if(stat!=null){
													res[0]['systemAttributes'] =response
													fn("success", res[0]);
												}else{
													fn("errAttr", res[0]);
												}
											});
										}else{
											fn("errAttr",res[0]);
										}
									})
								} else {
									fn("noData", res);
								}
							}
						} else {
							fn("err", err);
						}
					})
				} else if (data == "err") {
					fn("Unauthorized");
				} else if (data == "Invalid token" || data == 'No User') {
					fn(data, obj);
				}
			});

		} catch (e) {
			fn("err", e);
		}
	} else {
		fn("invalidInput"); 
	}

}
exports.getTripDetails = function(req, fn) {
	var connection = connections.connection;
	var query = "SELECT * from tracking where booking_id="+req.body.booking_id+" and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM tracking where booking_id="+req.body.booking_id+") AS p );"
	if (req.body.booking_id != undefined && req.body.booking_id != null) {
		try {
			var tokReq = {
					token: req.headers.token,
					userId: req.headers.userid,
					method: req.method
			}
			if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
				tokReq['customerApp'] = req.body.customerApp;
			}
			commonMethod.verifyUser(tokReq, function(data, obj) {
				data = "success";
				if (data == "success") {
					connection.query(query,function(err,res){
						if(!err){
							if(res!=undefined){
								if(res.length>0){
									fn("success",res);
								}else{
									fn("noData",res);
								}
							}
						}else{
							fn("err",err);
						}
					})
				} else if (data == "err") {
					fn("Unauthorized");
				} else if (data == "Invalid token" || data == 'No User') {
					fn(data, obj);
				}
			});

		} catch (e) {
			fn("err", e);
		}
	} else {
		fn("invalidInput"); 
	}
}
 exports.addEmergencyContactForCustomer = function(req, fn) {
          // if (req.body.customer_id != undefined && req.body.customer_id != null) {
          var tokReq = {
              token: req.headers.token,
              userId: req.headers.userid,
              method: req.method
          }
          if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
              tokReq['customerApp'] = req.body.customerApp;
              //          tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
              tokReq['customerMobile'] = (req.body.iOS == true || req.body.iOS == "true") ? encryptionIos.decrypt(req.body.customer_mobile) : encryptionMob.decrypt(req.body.customer_mobile);

          }
          commonMethod.verifyUser(tokReq, function(data, obj) {
              if (data == "success") {
                  var contacts = req.body.emergecy_contacts;
                  var to_be_notified_contacts = "";
                  var i = 0;
                  iterateContacts();

                  function iterateContacts() {
                      var requestObj = contacts[i];
                      if (requestObj.customer_id != undefined && requestObj.customer_id != null) {
                          to_be_notified_contacts += requestObj.is_notify == true ? requestObj.mobile_number : "";
                          generalDao.insertCustomerEmergencyContacts(requestObj, function(stat, obj) {
                              if (i == contacts.length - 1) {
                                  fn(stat, obj);
                                  to_be_notified_contacts += "";
                                  /* send sms to all contacts */
                                  var custQuery = "select customer_name from customer_master where customer_id=" + requestObj.customer_id;
                                  connection.query(custQuery, function(error, result) {
                                      if (!error) {
                                          if (result != undefined) {
                                              if (result.length > 0) {
                                                  //      var message = "Person "+result[0].customer_name+"("+encryptionMob.decrypt(req.body.customer_mobile)+") has added you as an emergency contact in eSahai. You will be notified in case any emergency request is raised";
                                                  var message = "";

                                                  if (result[0].customer_name != null && result[0].customer_name != "")
                                                      message = "" + result[0].customer_name + "(" + encryptionMob.decrypt(req.body.customer_mobile) + ") has added you as an emergency contact in eSahai App(bit.ly/2izuR7v). You will be notified in case any emergency request is raised. Thank You";
                                                  else
                                                      message = "Person with " + encryptionMob.decrypt(req.body.customer_mobile) + "as his/her mobile number has added you as an emergency contact in eSahai App(bit.ly/2izuR7v). You will be notified in case any emergency request is raised. Thank You";
                                                  if (to_be_notified_contacts != "" && to_be_notified_contacts.length > 0) {
                                                      var param = {
                                                          mobilenumber: to_be_notified_contacts,
                                                          message: message
                                                      }
                                                      loginDao.sendSMS(param, function(stat) {

                                                      })
                                                  }

                                              }
                                          }
                                      }
                                  })
                              } else {

                                  to_be_notified_contacts += ",";
                                  i++;
                                  iterateContacts();
                              }
                          })
                      } else {
                          fn("invalidInput");
                      }
                  }
              } else if (data == "err") {
                  fn("err");
              } else if (data == "Invalid token" || data == 'No User') {
                  fn(data, obj);
              } else fn(data, obj);
          });
         
      }
/*exports.addEmergencyContactForCustomer = function(req, fn) {
	// if (req.body.customer_id != undefined && req.body.customer_id != null) {
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
		tokReq['customerApp'] = req.body.customerApp;
		tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
	}
	commonMethod.verifyUser(tokReq, function(data, obj) {
		data = "success";
		if (data == "success") {
			var contacts = req.body.emergecy_contacts;
			var i=0;
			iterateContacts();
			function iterateContacts(){
				var requestObj = contacts[i];
				if (requestObj.customer_id != undefined && requestObj.customer_id != null) {
					generalDao.insertCustomerEmergencyContacts(requestObj, function(stat, obj) {
						if(i ==  contacts.length-1)
							fn(stat, obj);
						else{
							i++;
							iterateContacts();
						}
					})
				} else {
					fn("invalidInput");
				}
			}
		} else if (data == "err") {
			fn("Unauthorized");
		} else if (data == "Invalid token" || data == 'No User') {
			fn(data, obj);
		}
	});
}*/
/*   var reqbody = {
                                                    "emergencyGeoHash": res[0].emergency_geoHashCode,
                                                    "latitude": res[0].emergency_latitude,
                                                    "longitude": res[0].emergency_longitude,
                                                    "customerApp": false
                                                }
                                                var reqAmb = {
                                                    headers: reqheaders,
                                                    body: reqbody
                                                }
                                                var deviceTokens = [];

                    var message = {
                        booking_id: req.body.booking_id,
                        latitude: reqbody.latitude,
                        longitude: reqbody.longitude,
                        customerMobile: res[0].customer_mobile,
                        customerName: res[0].customer_name,
                        address: res[0].eme_address,
                        emergencyType: res[0].emergency_type,
                        gcmToken: req.body.gcmToken,
                        hospital: res[0].hospital
                    }
                    message['hospital_latitude'] = trackObj[0].end_latitude;
                    message['hospital_longitude'] = trackObj[0].end_longitude;
                    message['hospital_geoHashCode'] =trackObj[0].end_geoHash; console.log("MESSAGE EMREGENCY ",reqAmb);
                                               					console.log("UIUIPPP ",message);
						 var rejObj = {
                                 booking_id:req.body.booking_id
                             }
                             generalDao.getRejectedAmbsByBookingId(rejObj,function(rejStat,data){
                                 console.log("in getRejectedAmbsByBookingId ",rejStat, data);
                                 if(rejStat=="success"){
                                     var ambulanceIds = data;
                                     if(ambulanceIds!=undefined && ambulanceIds!=null){
                                    	 bookingDao.getNearByAmbulanceList(reqAmb, function(status, ambulanceList) {
                                             console.log("######################## ", status, ambulanceList);
                                            if (status == "success") {
						if(ambulanceList.length == 0){
							 fn("noAmb",data);
						}
                                                if (ambulanceList.length > 0) {
                                                    var i = 0;
                                                    iterateAmbulances();

                                                    function iterateAmbulances() {
                                                        if (i < ambulanceList.length) {
                                                            if (ambulanceList[i].ambulance_id != undefined) {
						console.log("PPPPPPPPPPP ",ambulanceList[i].ambulance_id, "  ",res[0].ambulance_id);
							//if(ambulanceList[i].ambulance_id != res[0].ambulance_id){
						if(ambulanceIds.indexOf(ambulanceList[i].ambulance_id)== -1){
                                                                message['distance'] = ambulanceList[i].distance;
                                                                message['duration'] = ambulanceList[i].duration;
                                                                connection.query("select device_token from ambulance_master where amb_id=" + ambulanceList[i].ambulance_id, function(err, res) {
                                                    if (!err) {*/
exports.transferBooking = function(req, fn) {
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {

			tokReq['ambApp'] = req.body.ambulanceApp;
		}
		if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
			tokReq['customerApp'] = req.body.customerApp;
		}
		if (req.body.booking_id != undefined && req.body.booking_id != null) {
			var query = "select * from emergency_booking where booking_id = " + req.body.booking_id;
			var query1 ="SELECT * from tracking where booking_id="+req.body.booking_id+" and created_date IN (   SELECT * FROM (SELECT Max(created_date) FROM tracking where booking_id="+req.body.booking_id+") AS p )" 
			connection.query(query, function(err, res) {
				if (!err) {
					if (res != undefined) {
						if (res.length > 0) {
							connection.query(query1,function(trackerr,trackObj){
								if(!trackerr){
									if (res[0].booking_status == commonConstants.TripClosed || res[0].booking_status == commonConstants.ReachedDestination ){
								        fn("closed");
								}else if (res[0].booking_status == commonConstants.Cancelled || res[0].booking_status == commonConstants.FakeCancel ){
								        fn("cancelled");
								}else if (res[0].booking_status != commonConstants.Pending) {
										FreeAmbulanceFromBooking(res[0].ambulance_id, function(stat, response) {
											if (stat == "success") {
//												fn(stat,response);
												var q = "update emergency_booking set booking_status='"+commonConstants.Pending+"',ambulance_id=0,ambulance_start_longitude='',ambulance_start_latitude='',ambulance_start_geoHashCode='' where booking_id=" + req.body.booking_id;
												connection.query(q, function(error, result) {
													if (!error) {
														//       fn(stat, response);
														var q1 = "update tracking set booking_status = '"+commonConstants.Pending+"' where booking_id=" + req.body.booking_id+" and created_date in ( SELECT * FROM (SELECT Max(created_date) FROM tracking where booking_id=" + req.body.booking_id+") AS p )"
														console.log("QUERY ",q1);
														connection.query(q1, function(error1, result1) {
															console.log("PPP ERRRO R ",error1);
															if (!error1) {
																var insertRej = {
																		ambulance_id:res[0].ambulance_id,
																		booking_id:req.body.booking_id,
reason:req.body.reason
																}
																console.log("INSERT OBJ ",insertRej)
																generalDao.insertRejectedAmbulances(insertRej,function(inStatus,rejResponse){
																	console.log("IOIOI STATU S ",inStatus,rejResponse);
																	if(inStatus == "success"){

																		/* }else{
                                                		 fn("errButAmbFree",rejResponse);
                                                	 }
                                                 })   */                      
																		var obj1 = {
																				tableName: "ambulance_master",
																				ambulance_id: res[0].ambulance_id
																		}
																		var pnData = "Booking Id " + req.body.booking_id + " is transferd to other";
																		var message = "Booking transfered";
																		var type = "transfer_booking";
																		var deviceToken = [];
																		getDeviceTokenById1(obj1, function(status1, data1) {
																			console.log("AMBULANCE STASTUS ",status1, data1)
																			if (status1 == "success") {
																				var deviceToken = [];
																				deviceToken = [data1.device_token];
																				var sendObj = {user_type:'Ambulance',send_to:data1.ambulance_mobile}
																				if (req.body.ambulanceApp == undefined || (req.body.ambulanceApp == false || req.body.ambulanceApp == "false"))
																					sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj);

																				/*** Notifying other ambulances ***/
																				var reqheaders = req.headers;
																				var reqbody = {
																						"emergencyGeoHash": res[0].emergency_geoHashCode,
																						"latitude": res[0].emergency_latitude,
																						"longitude": res[0].emergency_longitude,
																						"customerApp": false,
																						"ambulanceApp":req.body.ambulanceApp
																				}
																				var reqAmb = {
																						headers: reqheaders,
																						body: reqbody
																				}
																				var deviceTokens = [];

																				var message = {
																						booking_id: req.body.booking_id,
																						latitude: reqbody.latitude,
																						longitude: reqbody.longitude,
																						customerMobile: res[0].is_self==true?res[0].customer_mobile:res[0].patient_number,
																								customerName: res[0].customer_name,
																								age:req.body.age,
																								address: res[0].eme_address,
																								emergencyType: res[0].emergency_type,
																								gcmToken: req.body.gcmToken,
																								hospital: res[0].hospital
																				}
																				message['hospital_latitude'] = trackObj[0].end_latitude;
																				message['hospital_longitude'] = trackObj[0].end_longitude;
																				message['hospital_geoHashCode'] =trackObj[0].end_geoHash; console.log("MESSAGE EMREGENCY ",reqAmb);
																				/** PNS TO ALL AMBULANCES except above one*/
																				console.log("UIUIPPP ",message);
																				var rejObj = {
																						booking_id:req.body.booking_id
																				}
																				reqAmb.body['group_id'] = res[0].group_id;
																				reqAmb.body['emergency_type'] = res[0].emergency_type_id;
																				reqAmb.body['group_type_id'] = res[0].group_type_id;

																				generalDao.getRejectedAmbsByBookingId(rejObj,function(rejStat,data){
																					console.log("in getRejectedAmbsByBookingId ",rejStat, data);
																					if(rejStat=="success"){
																						var ambulanceIds = data;
																						if(ambulanceIds!=undefined && ambulanceIds!=null){
																							console.log("REQAMB ",reqAmb);
																							bookingDao.getNearByAmbulanceListSpecificType(reqAmb, function(status, ambulanceList) {
																								console.log("######################## ", status, ambulanceList);
																								if (status == "success") {
																									if (ambulanceList.length > 0) {
																										var i = 0;
																										var mobile="";
																										iterateAmbulances();

																										function iterateAmbulances() {
																											if (i < ambulanceList.length) {
																												if (ambulanceList[i].ambulance_id != undefined) {
																													console.log("PPPPPPPPPPP ",ambulanceList[i].ambulance_id, "  ",res[0].ambulance_id);
																													//if(ambulanceList[i].ambulance_id != res[0].ambulance_id){
																													if(ambulanceIds.indexOf(ambulanceList[i].ambulance_id)== -1){
																														message['distance'] = ambulanceList[i].distance;
																														message['duration'] = ambulanceList[i].duration;
																														connection.query("select device_token,ambulance_mobile from ambulance_master where amb_id=" + ambulanceList[i].ambulance_id, function(err, res) {
																															if (!err) {
																																console.log("% ", res[0].device_token);

																																if (res != undefined && res.length > 0) {
																																	console.log("IOIOI ",res);
																																	deviceTokens.push(res[0].device_token);
																																	console.log("DEVICE BEFORE",deviceTokens);
																																	var data = "new emergency is waiting";
																																	var sendObj = {user_type:'Ambulance',send_to:res[0].ambulance_mobile};
																																	/*if (deviceTokens.length > 0)
																																		sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking",sendObj);*/
																							                                           if(deviceTokens.length>0){
																				                                                        	getAgeByCustomerId(res[0].customer_id,function(statusAge,age){
																				                                                        		if(statusAge == "age"){
																				                                                        	//		message['age'] = age;
												message['age'] = age.age;
						                                                message['device_type'] = age.device_type;
																				                                                        			sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking",sendObj);

																				                                                        		}else
																				                                                        			sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking",sendObj);
																				                                                        	});
																				                                                        }
																																	if (i == ambulanceList.length - 1) {
																																		//send pn 


																																		// console.log("OPOPOPOPOPPPPPPPPPPPPPP ", message)
																																		var data = "new emergency is waiting";
																																		console.log("DEVIKCE TOKENENS", deviceTokens);
																																		fn(stat,response);
																																		/*if (deviceTokens.length > 0)
																																			sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking");*/
																																	}
																																	i++;
																																	iterateAmbulances();
																																} else {
																																	i++;
																																	iterateAmbulances();
																																}
																															} else {
																																i++;
																																iterateAmbulances();
																															}
																														})
																													}else{
																														if(i == ambulanceList.length - 1){
																															var data = "new emergency is waiting";
																															var sendObj = {user_type:'Ambulance',send_to:res[0].ambulance_mobile};
											                                                                                fn(stat,response);
											                                                                                /*if (deviceTokens.length > 0)
											                                                                                    sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking",sendObj);*/
																					                                           if(deviceTokens.length>0){
																		                                                        	getAgeByCustomerId(res[0].customer_id,function(statusAge,age){
																		                                                        		if(statusAge == "age"){
																		                                                        		//	message['age'] = age;
										message['age'] = age.age;
                                                                                message['device_type'] = age.device_type;
																		                                                        			sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking",sendObj);

																		                                                        		}else
																		                                                        			sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking",sendObj);
																		                                                        	});
																		                                                        }


																														}
																														i++;
																														iterateAmbulances();

																													}
																												} else {
																													i++;
																													iterateAmbulances();
																												}
																											}/* else if (i == ambulanceList.length - 1) {
																												//send pn 
																												// console.log("OPOPOPOPOPPPPPPPPPPPPPP ", message)
																												var data = "new emergency is waiting";
																												var sendObj = {user_type:'Ambulance',send_to:res[0].ambulance_mobile};

																												if (deviceTokens.length > 0)
																													sendPNDao.sendPN(deviceTokens, data, message, "ambulance_transferred_booking",sendObj);

																											}*/
																										}
																									}
																								}
																							})
																						}

																					}else{
																						fn("err",data);
																					}
																				})

																			} else {
																				fn("errPN", data);
																			}
																		})
																		/***/
																	}else{
																		fn("errButAmbFree",rejResponse);
																	}
																})   
															}
														});
													}
												});
											} else {
												fn("errAmbFree", response);
											}
										});
									} else {
										fn("cantTransfer", res[0].booking_status);
									}
								}else{
									fn("err", err);
								}
							});
						} else {
							fn("noBooking", res);
						}
					}
				} else {
					fn("err", err);
				}
			})
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}
}
/*exports.transferBooking = function(req, fn) {
    var connection = connections.connection;
    try {
	var tokReq = {
                token: req.headers.token,
                userId: req.headers.userid,
                method: req.method
            }
        if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
            tokReq['ambApp'] = req.body.ambulanceApp;
        }
        if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
            tokReq['customerApp'] = req.body.customerApp;
        }
        if (req.body.booking_id != undefined && req.body.booking_id != null) {
            var query = "select * from emergency_booking where booking_id = " + req.body.booking_id;
            connection.query(query, function(err, res) {
                if (!err) {
                    if (res != undefined) {
                        if (res.length > 0) {
                            if (res[0].booking_status != "Pending") {
                                FreeAmbulanceFromBooking(res[0].ambulance_id, function(stat, response) {
                                    if (stat == "success") {
//					fn(stat,response);
					 var q = "update emergency_booking set booking_status='Pending',ambulance_id=0,ambulance_start_longitude='',ambulance_start_latitude='',ambulance_start_geoHashCode='' where booking_id=" + req.body.booking_id;
                                        connection.query(q, function(error, result) {
                                            if (!error) {
                                         //       fn(stat, response);
					var q1 = "update tracking set booking_status = 'Pending' where booking_id=" + req.body.booking_id+" and created_date in ( SELECT * FROM (SELECT Max(created_date) FROM tracking where booking_id=" + req.body.booking_id+") AS p )"
console.log("QUERY ",q1);
                                                 connection.query(q1, function(error1, result1) {
						console.log("PPP ERRRO R ",error1);
                                            if (!error1) {

                                        var obj1 = {
                                            tableName: "ambulance_master",
                                            ambulance_id: res[0].ambulance_id
                                        }
                                        var pnData = "Booking Id " + req.body.booking_id + " is transferd to other";
                                        var message = "Booking transfered";
                                        var type = "transfer_booking";
                                        var deviceToken = [];
                                        getDeviceTokenById(obj1, function(status1, data1) {
						console.log("AMBULANCE STASTUS ",status1, data1)
                                            if (status1 == "success") {
						console.log("DATATTA TOKEN ",data1);
						var deviceToken = [];
                                                deviceToken = [data1];
                                               // deviceToken = deviceToken[data1];
					//	deviceToken = deviceToken.push(data1);
                                                console.log("OPOPOP CANCEL DEVICE TOKEN ", deviceToken);
						                                                        if (req.body.ambulanceApp == undefined || (req.body.ambulanceApp == false || req.body.ambulanceApp == "false"))

                                                sendPNDao.sendPN(deviceToken, pnData, message, type);
                                                var reqheaders = req.headers;
                                                var reqbody = {
                                                    "emergencyGeoHash": res[0].emergency_geoHashCode,
                                                    "latitude": res[0].emergency_latitude,
                                                    "longitude": res[0].emergency_longitude,
                                                    "customerApp": false
                                                }
                                                var reqAmb = {
                                                    headers: reqheaders,
                                                    body: reqbody
                                                }
                                                var deviceTokens = [];

                    var message = {
                        booking_id: req.body.booking_id,
                        latitude: reqbody.latitude,
                        longitude: reqbody.longitude,
                        customerMobile: res[0].customer_mobile,
                        customerName: res[0].customer_name,
                        address: req.body.address,
                        emergencyType: res[0].emergency_type,
                        gcmToken: req.body.gcmToken,
                        hospital: res[0].hospital
                    }
                    message['hospital_latitude'] = res[0].hospital_latitude;
                    message['hospital_longitude'] = res[0].hospital_longitude;
                    message['hospital_geoHashCode'] = res[0].hospital_geoHashCode; console.log("MESSAGE EMREGENCY ",reqAmb);
                                                // PNS TO ALL AMBULANCES except above one
						console.log("UIUIPPP ",message);
                                                bookingDao.getNearByAmbulanceList(reqAmb, function(status, ambulanceList) {
                                                     console.log("######################## ", status, ambulanceList);
                                                    if (status == "success") {
                                                        if (ambulanceList.length > 0) {
                                                            var i = 0;
                                                            iterateAmbulances();

                                                            function iterateAmbulances() {
                                                                if (i < ambulanceList.length) {
                                                                    if (ambulanceList[i].ambulance_id != undefined) {
								console.log("PPPPPPPPPPP ",ambulanceList[i].ambulance_id, "  ",res[0].ambulance_id);
									if(ambulanceList[i].ambulance_id != res[0].ambulance_id){
                                                                        message['distance'] = ambulanceList[i].distance;
                                                                        message['duration'] = ambulanceList[i].duration;
                                                                        connection.query("select device_token from ambulance_master where amb_id=" + ambulanceList[i].ambulance_id, function(err, res) {
                                                                            if (!err) {
                                                                                console.log("% ", res[0].device_token);

                                                                                if (res != undefined && res.length > 0) {
										console.log("IOIOI ",res);
                                                                                    deviceTokens.push(res[0].device_token);
										console.log("DEVICE BEFORE",deviceTokens);
                                                                                    if (i == ambulanceList.length - 1) {
                                                                                        //send pn 


                                                                                        // console.log("OPOPOPOPOPPPPPPPPPPPPPP ", message)
                                                                                        var data = "new emergency is waiting";
console.log("DEVIKCE TOKENENS", deviceTokens);
											fn(stat,response);
                                                                                        if (deviceTokens.length > 0)
                                                                                            sendPNDao.sendPN(deviceTokens, data, message, "new_booking");
                                                                                    }
                                                                                    i++;
                                                                                    iterateAmbulances();
                                                                                } else {
                                                                                    i++;
                                                                                    iterateAmbulances();
                                                                                }
                                                                            } else {
                                                                                i++;
                                                                                iterateAmbulances();
                                                                            }
                                                                        })
									}else{
										if(i == ambulanceList.length - 1){
										var data = "new emergency is waiting";
console.log("DEVIKCE TOKENENS", deviceTokens);
                                                                                        fn(stat,response);
                                                                                        if (deviceTokens.length > 0)
                                                                                            sendPNDao.sendPN(deviceTokens, data, message, "new_booking");

										}
									  i++;
                                                                                iterateAmbulances();

									}
                                                                    } else {
                                                                        i++;
                                                                        iterateAmbulances();
                                                                    }
                                                                } else if (i == ambulanceList.length - 1) {
                                                                    //send pn 
                                                                    // console.log("OPOPOPOPOPPPPPPPPPPPPPP ", message)
                                                                    var data = "new emergency is waiting";
                                                                    if (deviceTokens.length > 0)
                                                                        sendPNDao.sendPN(deviceTokens, data, message, "new_booking");
                                                                }
                                                            }
                                                        }
                                                    }
                                                })
                                            } else {
                                                fn("errPN", data);
                                            }
                                        })
					}
					});
					}
                                        });
                                    } else {
                                        fn("errAmbFree", response);
                                    }
                                });
                            } else {
                                fn("cantTransfer", res[0].booking_status);
                            }
                        } else {
                            fn("noBooking", res);
                        }
                    }
                } else {
                    fn("err", err);
                }
            })
        } else {
            fn("invalidInput");
        }
    } catch (e) {
        fn("err", e);
    }
}*/
exports.updateEmergencyContactForCustomer = function(req, fn) {

          var tokReq = {
              token: req.headers.token,
              userId: req.headers.userid,
              method: req.method
          }
          console.log(JSON.stringify(req.body))
          if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
              tokReq['customerApp'] = req.body.customerApp;
              //              tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);i
              tokReq['customerMobile'] = (req.body.iOS == true || req.body.iOS == "true") ? encryptionIos.decrypt(req.body.customer_mobile) : encryptionMob.decrypt(req.body.customer_mobile);

          }
          commonMethod.verifyUser(tokReq, function(data, obj) {
              if (data == "success") {
                  var contacts = req.body.emergecy_contacts;
                  var to_be_notified_contacts = "";
                  var shd_not_notified_contacts = "";
                  var i = 0;
                  iterateContacts();

                  function iterateContacts() {
                      var requestObj = contacts[i];
                      if (requestObj.id != undefined && requestObj.id != null) {
                          generalDao.getEmergencyContactsById(requestObj.id, function(status, isNotify) {
                              console.log("WEMERR ", status, isNotify);
                              if (status == "success") {
                                  if (isNotify == true) {
                                      console.log("LLLLLLLLLLLLLLLLLlll ");
                                      if (requestObj.is_notify == false) {
                                          /** send sms to contact with unregistered meesage*/
                                          shd_not_notified_contacts += requestObj.is_notify == false ? requestObj.mobile_number : "";
                                          updateContacts(requestObj, function(status, result) {
                                              fn(status, result);
                                          })
                                      } else {
                                          updateContacts(requestObj, function(status, result) {
                                              fn(status, result);
                                          })

                                      }
                                  } else if (isNotify == false) {
                                      console.log("LLLLLLLLLLLLLLLLLlll KKKKK  ");

                                      if (requestObj.is_notify == true) {
                                          /** send sms to contact with registered meesage*/
                                          to_be_notified_contacts += requestObj.is_notify == true ? requestObj.mobile_number : "";
                                          updateContacts(requestObj, function(status, result) {
                                              fn(status, result);
                                          })
                                      } else {
                                          updateContacts(requestObj, function(status, result) {
                                              fn(status, result);
                                          })

                                      }
                                  }
                                  /*      updateContacts(requestObj,function(status,result){
                                                  fn(status,result);
                                          })*/
                              } else {
                                  updateContacts(requestObj, function(status, result) {
                                      fn(status, result);
                                  })

                              }
                          });

                          function updateContacts(requestObj, fn) {
                              generalDao.updateCustomerEmergencyContacts(requestObj, function(stat, obj) {
                                  if (i == contacts.length - 1) {
                                      fn(stat, obj);
                                      to_be_notified_contacts += "";
                                      shd_not_notified_contacts += "";
                                      /* send sms to all contacts */
                                      var custQuery = "select customer_name from customer_master where customer_id=" + requestObj.customer_id;
                                      console.log("RRRRRRRRRRRRRRRRRRRRRRRR  ", custQuery);
                                      connection.query(custQuery, function(error, result) {
                                          if (!error) {
                                              if (result != undefined) {
                                                  if (result.length > 0) {
                                                      var message = "";
                                                      var unnotify_message = "";
                                                      if (result[0].customer_name != null && result[0].customer_name != "") {
                                                          message = "" + result[0].customer_name + "(" + encryptionMob.decrypt(req.body.customer_mobile) + ") has added you as an emergency contact in eSahai App(bit.ly/2izuR7v). You will be notified in case any emergency request is raised. Thank You";
                                                          unnotify_message = "Person " + result[0].customer_name + "(" + encryptionMob.decrypt(req.body.customer_mobile) + ") has removed you from his/her emergency contact list. Thank you eSahai!!!";
                                                      } else {
                                                          message = "Person with " + encryptionMob.decrypt(req.body.customer_mobile) + "as his/her mobile number has added you as an emergency contact in eSahai App(bit.ly/2izuR7v). You will be notified in case any emergency request is raised. Thank You";
                                                          unnotify_message = "Person with " + encryptionMob.decrypt(req.body.customer_mobile) + " has removed you from his/her emergency contact list. Thank you eSahai!!!";
                                                      }
                                                      console.log("NOTIFIED USERS ", to_be_notified_contacts);
                                                      if (to_be_notified_contacts != "" && to_be_notified_contacts.length > 0) {
                                                          console.log("IOOOOOOOOOOOOOO ");
                                                          var param = {
                                                              mobilenumber: to_be_notified_contacts,
                                                              message: message
                                                          }
                                                          loginDao.sendSMS(param, function(stat) {

                                                          })
                                                      }
                                                      console.log("IOOOO ", shd_not_notified_contacts);
                                                      if (shd_not_notified_contacts != "" && shd_not_notified_contacts.length > 0) {
                                                          console.log("PALLAOO ");
                                                          var param = {
                                                              mobilenumber: shd_not_notified_contacts,
                                                              message: unnotify_message
                                                          }
                                                          loginDao.sendSMS(param, function(stat) {

                                                          })
                                                      }
                                                  }
                                              }
                                          }
                                      })
                                  } else {
                                      to_be_notified_contacts += (to_be_notified_contacts.length > 0) ? "," : "";
                                      shd_not_notified_contacts += (shd_not_notified_contacts.length > 0) ? "," : "";
                                      i++;
                                      iterateContacts();
                                  }
                              });
                          }

                      } else {
                          fn("invalidInput");
                      }
                  }
              } else if (data == "err") {
                  fn("err");
              } else if (data == "Invalid token" || data == 'No User') {
                  fn(data, obj);
              } else fn(data, obj);
          });

      }
/*exports.updateEmergencyContactForCustomer = function(req, fn) {

	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	console.log(JSON.stringify(req.body))
	if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
		tokReq['customerApp'] = req.body.customerApp;
		tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
	}
	commonMethod.verifyUser(tokReq, function(data, obj) {
		if (data == "success") {
			var contacts = req.body.emergecy_contacts;
			var i = 0;
			iterateContacts();

			function iterateContacts() {
				var requestObj = contacts[i];
				if (requestObj.id != undefined && requestObj.id != null) {
					generalDao.updateCustomerEmergencyContacts(requestObj, function(stat, obj) {
						if (i == contacts.length - 1)
							fn(stat, obj);
						else {
							i++;
							iterateContacts();
						}
					})
				} else {
					fn("invalidInput");
				}
			}
		} else if (data == "err") {
			fn("Unauthorized");
		} else if (data == "Invalid token" || data == 'No User') {
			fn(data, obj);
		}
	});

}*/
exports.deleteEmergencyContactForCustomer = function(req, fn) {

	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
		tokReq['customerApp'] = req.body.customerApp;
		tokReq['customerMiobile'] = encryptionMob.decrypt(req.body.customer_mobile);
	}
	commonMethod.verifyUser(tokReq, function(data, obj) {
		if (data == "success") {
			var contacts = req.body.emergecy_contacts;
			var i = contacts.length - 1;
			var connection = connections.connection;
			var query = "delete from customer_e_contacts where id in (";
			while (i > -1) {
				if (i != 0) {
					query += "'" + contacts[i].id + "',";
				} else {
					query += "'" + contacts[i].id + "'";
				}
				i--;
			}
			query += ")";
			connection.query(query, function(err, res) {
				if (!err) {
					console.log("success" + query)
					fn("success", res);
				} else {
					console.log("sfailure" + query)
					fn("err", err);
				}
			})
		} else if (data == "err") {
			fn("Unauthorized");
		} else if (data == "Invalid token" || data == 'No User') {
			fn(data, obj);
		}
	});

}

/*exports.insertBloodRequest = function(req, fn) {
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
        tokReq['customerApp'] = req.body.customerApp;
    }

    commonMethod.verifyUser(tokReq, function(data, obj) {
        data = "success";
        if (data == "success") {
            if (req.body.customer_id != undefined && req.body.customer_id != null) {
                var connection = connections.connection;
                var query = "insert into blood_request(customer_id, name, contact, blood_group, quantity, location, time, latitude, longitude, geohash) value(";
                query += "'"+req.body.customer_id + "','" + req.body.name + "','" + req.body.contact + "','" + req.body.blood_group + "','" + req.body.quantity 
                query += "','" + req.body.location + "','" + req.body.time + "','" + req.body.latitude + "','" + req.body.longitude + "','" + req.body.geohash + "')";
                connection.query(query, function(err, res) {
                    if (!err) {
                        console.log("success" + query)
                        fn("success", res);
                    } else {
                        console.log("sfailure" + query)
                        fn("err", err);
                    }
                })
            } else {
                fn("invalidInput");
            }
        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    });
}*/

exports.insertBloodRequest = function(req, fn) {
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
		tokReq['customerApp'] = req.body.customerApp;
		tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
	}

	commonMethod.verifyUser(tokReq, function(data, obj) {
		if (data == "success") {
			if (req.body.customer_id != undefined && req.body.customer_id != null) {
				var connection = connections.connection;
				var query = "insert into blood_request(customer_id, name, contact, blood_group, quantity, location, time, latitude, longitude, geohash) value(";
				query += "'" + req.body.customer_id + "','" + req.body.name + "','" + req.body.contact + "','" + req.body.blood_group + "','" + req.body.quantity
				query += "','" + req.body.location + "','" + req.body.time + "','" + req.body.latitude + "','" + req.body.longitude + "','" + req.body.geohash + "')";

				connection.query(query, function(err, res) {
					console.log(JSON.stringify(err))
					if (!err) {
						console.log("success" + query);
						//send pn to this blood group
						getCustomersByBloodGroup(req.body.blood_group,function(stats, object){
							console.log("UIUI ",stats);
							if(stats == "success"){
								console.log("SUCCESS BLOOD ",object);
								var q = "select customer_id,device_token,customer_mobile_number,device_type from customer_master where customer_id in ("+object+")";
								connection.query(q,function(error,result){
									console.log("query and result ",error,result);
									if(!error){
										if(result!=undefined){
											if(result.length>0){
												var devicetokens = [];
												var message ="Urgent "+req.body.blood_group+" blood required for customer "+req.body.name+"("+req.body.contact+")";
												var data = message;
												var type = "blood_request";
												var i = 0;
												var mobile="";
												var iosTokens = [];
												iterateDeviceTokens();
												function iterateDeviceTokens(){
													if(i<result.length){
														if(result[i].customer_id != req.body.customer_id)
														//	devicetokens.push(result[i].device_token);
															if(result[i].device_type == "Android")
                                                                 devicetokens.push(result[i].device_token);
                                                            else
                                                                iosTokens.push(result[i].device_token);
															mobile += result[i].customer_mobile_number;
	                                                        mobile += (i==result.length-1)?"":",";
														if(i == result.length-1){
															console.log("DEVIEC TOKENS ",devicetokens);
															fn("success",res);
															var sendObj = {user_type:"Customer",send_to:mobile};
                                                           // sendPNDao.sendPN(devicetokens, data, message, type,sendObj);
								if(iosTokens.length>0)
                                                                sendPNDao.sendPN(iosTokens, data, message, type,sendObj,true);
                                                            if(devicetokens.length>0)
                                                                sendPNDao.sendPN(devicetokens, data, message, type,sendObj);

														}
														i++;
														iterateDeviceTokens();
													}
												}
											}else{
												fn("noCustomers",result);
											}
										}
										//fn("success", res);
									}else{
										fn("errPn",err);
									}
								});
							}else{
								console.log("ERROR ",err);
								fn(stats,err);
							}
						})


					} else {
						console.log("sfailure" + query)
						fn("err", err);
					}
				})
			} else {
				fn("invalidInput");
			}
		} else if (data == "err") {
			fn("Unauthorized");
		} else if (data == "Invalid token" || data == 'No User') {
			fn(data, obj);
		}
	});
}


var getCustomersByBloodGroup = function(bloodGroup,fn){
	var connection = connections.connection;
//	var query = "select customer_id from customer_attributes where customer_e_type_id=3 and attribute_details='"+bloodGroup+"'";
	var query = "select customer_id from customer_master where blood_group='"+bloodGroup+"' and donating_blood = 1";
	console.log("BOLOG ",query);
	connection.query(query,function(err,res){
		console.log("ERROR ",err);
		console.log("RESULT",res);
		if(!err){
			if(res!=undefined){
				if(res.length>0){
					var customerIds = "";
					var i=0;
					iterateCustomers();
					function iterateCustomers(){
						if(i<res.length){
							customerIds = customerIds+res[i].customer_id;
							if(i == res.length-1){
								customerIds += ""; console.log("PPP",customerIds);
								fn("success",customerIds);
							}
							else{
								customerIds += ","
									i++;
								iterateCustomers();
							}
						}
					}
				}else{
					fn("noCustomers");
				}
			}
		}else 
			fn(null);
	})
}



exports.transferByCommandCentre = function(req, fn) {
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
			tokReq['ambApp'] = req.body.ambulanceApp;
		}
		if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
			tokReq['customerApp'] = req.body.customerApp;
			tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
		}
		if (req.body.conditionalParams.booking_id != undefined && req.body.conditionalParams.booking_id != null) {
			var query = "select * from emergency_booking where booking_id = " + req.body.conditionalParams.booking_id;
			var query1 ="SELECT * from tracking where booking_id="+req.body.conditionalParams.booking_id+" and created_date IN (   SELECT * FROM (SELECT Max(created_date) FROM tracking where booking_id="+req.body.conditionalParams.booking_id+") AS p )"

			connection.query(query, function(err, res) {
				if (!err) {
					if (res != undefined) {
						if (res.length > 0) {
							var PerObj = res[0];
							var customerId = res[0].customer_id;
							connection.query(query1,function(trackerr,trackObj){
								if(!trackerr){
									if(res[0].booking_status == commonConstants.TripClosed || res[0].booking_status == commonConstants.ReachedDestination){
                                        fn("closed");
                                }else if(res[0].booking_status == commonConstants.Cancelled || res[0].booking_status == commonConstants.FakeCancel){
                                    fn("cancelled");
                                }else if (res[0].booking_status != commonConstants.Pending && res[0].booking_status!=commonConstants.TripClosed && res[0].booking_status!=commonConstants.ReachedDestination) {
logger.info("REQ PARAM ", req.body.params.ambulance_id);
logger.info("RES ", trackObj[0].ambulance_id);
  var checkBookingQuery = "select count(*) as count from rejected_ambulances where booking_id = "+req.body.conditionalParams.booking_id+" and ambulance_id="+req.body.params.ambulance_id;
logger.info("CHECK BOOOKING ", checkBookingQuery);
                                	    connection.query(checkBookingQuery,function(bookErr,bookObj){
logger.info("ERROR ", bookErr);
logger.info("BOOKING O", bookObj);
                                	    	if(!bookErr){
                                	    		if(bookObj!=undefined){
                                	    			if(bookObj[0].count >0){
                                	    				fn("chooseOther");
                                	    			}
										/*if(req.body.params.ambulance_id == trackObj[0].ambulance_id){
										logger.info("IN IF ");
											fn("chooseOther");
										}*/else {
										FreeAmbulanceFromBooking(res[0].ambulance_id, function(stat, response) {
											if (stat == "success") {
												var q = "update emergency_booking set booking_status='"+commonConstants.Pending+"',ambulance_id=0,ambulance_start_longitude='',ambulance_start_latitude='',ambulance_start_geoHashCode='' where booking_id=" + req.body.conditionalParams.booking_id;
												connection.query(q, function(error, result) {
													console.log("EROTTTTTTTTTTTTTTR ",error);
													console.log("RESUTL TDFSDHF ",result);
													if (!error) {
														// fn(stat, response);
														var obj1 = {
																tableName: "ambulance_master",
																ambulance_id: trackObj[0].ambulance_id
														}
														console.log("UIUI ",res[0].ambulance_id);
														var pnData = "Booking Id " + req.body.conditionalParams.booking_id + " is transferd to other";
														var message = "Booking transfered";
														var type = "transfer_booking";
														var deviceToken = [];
														getDeviceTokenById1(obj1, function(status1, data1) {
															console.log("AMBULANCE STASTUS ", status1, data1)
															if (status1 == "success") {
																console.log("DATATTA TOKEN ", data1);
																var deviceToken=[];
																deviceToken = [data1.device_token];
																var mobile = data1.ambulance_mobile;
		                                                        var sendObj = {user_type:"Ambulance",send_to:mobile};
																console.log("OPOPOP CANCEL DEVICE TOKEN ", deviceToken,deviceToken[data1]);
																if (req.body.ambulanceApp == undefined || (req.body.ambulanceApp == false || req.body.ambulanceApp == "false"))
																	sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj);
																//if from admin no need to searching nearest ambs



																/** PNS TO ALL AMBULANCES except above one*/
																var rejObj = {
																		booking_id: req.body.conditionalParams.booking_id
																}
																/// generalDao.getRejectedAmbsByBookingId(rejObj, function(rejStat, data) {
																//  if (rejStat == "success") {
																//    var ambulanceIds = data;
																//  if (ambulanceIds != undefined && ambulanceIds != null) {
																/**/
																req.body.tableName = "emergency_booking";
																//     req.body.params.booking_status = "Accepted";
																var selQuery = "select booking_status from " + req.body.tableName + " where booking_id=" + req.body.conditionalParams.booking_id;
																connection.query(selQuery, function(error1, result1) {
																	if (!error1) {
																		if (result1 != undefined) {
																			if (result1.length > 0) {
																				console.log("OOOO STATUS ", result1[0].booking_status);
																				if(result1[0].booking_status == commonConstants.TripClosed || result1[0].booking_status == commonConstants.ReachedDestination){
											                                        fn("closed");
																				}else if (result1[0]['booking_status'] == commonConstants.Cancelled || result1[0]['booking_status'] == commonConstants.FakeCancel) {
																					fn("cancelled", result1);
		}else if( result1[0]['booking_status'] == commonConstants.Pending || result1[0]['booking_status'] == 'No Response on transfer' ||result1[0]['booking_status'] == commonConstants.Accepted || result1[0].booking_status == commonConstants.ToEmergencyLocation || result1[0]['booking_status'] == commonConstants.AtEmergencyLocation || result1[0]['booking_status'] == commonConstants.WayToDestination){ 
																					req.body.params.booking_status = commonConstants.Accepted;
                         req.body.params.ambulance_number = req.body.ambulanceNumber; 
                         req.body.params.ambulance_id=  req.body.params.ambulance_id;	
																			appUserDao.updateData(req, function(stat, obj) {
																					if (stat != undefined && stat.updation == true) {

																						var query = "UPDATE ambulance_driver SET is_assigned=true WHERE ambulance_id=" + req.body.params.ambulance_id + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.params.ambulance_id + ") AS p )";
																						connection.query(query, function(err, res) {
																							if (!err) {
																								var track = {
																										'distance': req.body.params.distance,
																										'estimated_time_arrival': req.body.params.duration,
																										'cost': req.body.params.cost,
																										'driverId': req.body.driverId,
																										'booking_status': req.body.params.booking_status,
																										'ambulanceId': req.body.params.ambulance_id,
																										'speed': '',
																										'emergency_latitude': req.body.emergencyLatitude,
																										'emergency_longitude': req.body.emergencyLongitude,
																										'longitude': req.body.params.ambulance_start_longitude,
																										'latitude': req.body.params.ambulance_start_latitude,
																										'geoHashCode': req.body.params.geoHashCode,
																										'is_booked': true,
																										'booking_id': req.body.conditionalParams.booking_id,
																										'end_latitude': req.body.hospital_latitude,
																										'end_longitude': req.body.hospital_longitude
																								};
																								console.log("TRACK ", track);
																								generalDao.insertIntoTracking(track, function(stat, data) {
																									console.log("INININI TRACIKING ", stat, "KKK ", data);
																									if (stat == "success") {
																										fn(stat, obj);

																										var message = {
																												'distance': req.body.params.distance,
																												'duration': req.body.params.duration,
																												'cost': req.body.params.cost,
																												'driverName': req.body.driverName,
																												'ambulanceNumber': req.body.ambulanceNumber,
																												'driverMobileNumber': req.body.driverMobileNumber,
																												'driverProfilePic': req.body.driverProfilePic,
																												'emergencyLatitude': req.body.emergencyLatitude,
																												'emergencyLongitude': req.body.emergencyLongitude,
																												'ambulanceHospital': req.body.ambulanceHospital,
																												'ambulance_longitude': req.body.params.ambulance_start_longitude,
																												'ambulance_latitude': req.body.params.ambulance_start_latitude
																										};
																										console.log("OPOPOP ERRR ", PerObj);
																										var message1 = {
																												distance: req.body.params.distance,
																												duration: req.body.params.duration,
																												booking_id: PerObj.booking_id,
																												latitude: PerObj.emergency_latitude,
																												longitude: PerObj.emergency_longitude,
																												customerMobile: PerObj.is_self==true?PerObj.customer_mobile:PerObj.patient_number,
																														customerName: PerObj.customer_name,
																														age:req.body.customer_age,
																														address: PerObj.eme_address,
																														emergencyType: PerObj.emergency_type,
																														hospital: PerObj.hospital,
																														hospital_latitude: PerObj.emergency_latitude,
																														hospital_longitude:PerObj.emergency_longitude,
																														hospital_geoHashCode: PerObj.emergency_geoHashCode
																														/* 'hospital_latitude': trackObj[0].hospital_latitude,
    'hospital_longitude': trackObj[0].hospital_longitude,
    'hospital_geoHashCode': trackObj[0].hospital_geoHashCode*/
																										}
																										var deviceTokens = [];
																										var cusObj = {
																												tableName: "customer_master",
																												customer_id: customerId
																										}
																										getDeviceTokenById1(cusObj, function(status1, data1) {
																											if (status1 == "success") {
																												console.log("DATATTA TOKEN ", data1);
																												message1['gcmToken'] = data1.device_token;
																										        deviceTokens = [data1.device_token];
																										        var mobile = data1.customer_mobile_number;
																										        var data = "Your booking is confirmed";
																										        var sendObj = {user_type:"Customer",send_to:mobile};
																										        sendPNDao.sendPN(deviceTokens, data, message, "booking_transferred",sendObj);
																											} else {
																												fn("errPn", data1);
																											}
																										});

																										var deviceTokens1 = [];
																										var AmbObj = {
																												tableName: "ambulance_master",
																												ambulance_id: req.body.params.ambulance_id
																										}
																										getDeviceTokenById1(AmbObj, function(status2, data2) {
																											if (status2 == "success") {
																												console.log("DATATTA TOKEN ", data2);
																												var deviceTokens1 = [data2];
																												var data = "transferred emergency is waiting";
//																												sendPNDao.sendPN(deviceTokens1, data, message1, "transferred_new_booking");
																												/** new **/
																												var deviceTokens2 = [];
																												var cusObj1 = {
																														tableName: "customer_master",
																														customer_id: customerId
																												}
																												getDeviceTokenById(cusObj1, function(status1, data1) {
																													if (status1 == "success") {

																														message1['gcmToken'] = data1.device_token;
												 message1['device_type']=data1.device_type;
																														var deviceTokens1 = [data2.device_token];
																												        var sendObj = {user_type:"Ambulance",send_to:data2.ambulance_mobile};
																												        var data = "transferred emergency is waiting";
																												        sendPNDao.sendPN(deviceTokens1, data, message1, "transferred_new_booking",sendObj);

																													} else {
																														fn("errPn", data1);
																													}
																												});
																											} else {
																												fn("errPn", data1);
																											}
																										})
																									}
																									else {
																										fn("assignedButUpdateFail", data);
																									}
																								})
																							} else {
																								fn("err", err);
																							}
																						})
																					} else
																						fn(stat, obj);
																				});
																				}
																			}
																		}
																	}
																})

															}
															else {
																fn("errPN", data);
															}
														})
													}
												});
											} else {
												fn("errAmbFree", response);
											}
										});
									   }
/**start */
}
                                	    	}
                                	    });
/** end */

									} else {
										fn("cantTransfer", res[0].booking_status);
									}
								} else {
									fn("err", err);
								}
							});
						} else {
							fn("noBooking", res);
						}
					}
				} else {
					fn("err", err);
				}
			})
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}
}

/**** NEw API ****/
exports.getNearByAmbulanceListSpecificType = function(req, fn) {
	var connection = connections.connection;
	try {
		if (req.body.latitude != undefined && req.body.longitude != undefined && req.body.emergencyGeoHash && req.body.group_type_id!=undefined && req.body.group_type_id!=null) {
			var tokReq = {
					token: req.headers.token,
					userId: req.headers.userid,
					method: req.method
			}
			console.log("Request *** ",req.body);
			if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
				tokReq['ambApp'] = req.body.ambulanceApp;
			}
			if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
				tokReq['customerApp'] = req.body.customerApp;
				tokReq['customerMobile'] =encryptionMob.decrypt(req.body.customer_mobile);
			}
			/***emergency TYPes**/
			var emergency_type = null;
			if (req.body.emergency_type != undefined && req.body.emergency_type != null) {
				emergency_type = req.body.emergency_type;
			}

			var groupFilter = null;
			if (req.body.group_id != undefined && req.body.group_id != null) {
				groupFilter = req.body.group_id;
			}
			commonMethod.verifyUser(tokReq, function(data, obj) {
				data = "success";
				if (data == "success") {
					var eme_geoHash = req.body.emergencyGeoHash;
					eme_geoHash = eme_geoHash.toString().substring(0, 3);
					var query = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver where geoHashCode like '" + eme_geoHash + "%' and status ='Online' and is_assigned = false  group by ambulance_id order by created_date desc )";
					console.log("QUERY ", query);
					var nearestAmbulances = [];
					connection.query(query, function(err, res) {
						console.log("ERROR ", err);
						console.log("RES ******* ", res);
						if (!err) {
							if (res != undefined) {
								if (res.length > 0) {
									console.log("********************************************")
									var origin_latlong = req.body.latitude + "," + req.body.longitude;
									var origins = [];
									origins.push(origin_latlong);
									//	console.log("ORIGINI ", origins);
									var i = 0;
									var count = 0;
									iterateAmbulances();

									function iterateAmbulances() {

										if (i < res.length) {
console.log("^^^^^^^^^ I = ",i);
											getGroupByAmbulanceId(res[i].ambulance_id, function(stat, details) {
												console.log("GROUPBy Amb Id  ",res[i].ambulance_id, stat);
												logger.info("GROUP AMB ID ", res[i].ambulance_id);
												logger.info("STATUS ",stat);
												if (stat == "success") {
													//		console.log("DETAILS ", details);
													if(req.body.group_type_id == commonConstants.anyGroupTypeId){
														if(groupFilter == "Any"){
															/*all ambulances*/
															emergecyAMb(function(emerS,emerObj){
																fn(emerS,emerObj);
															});
														}else{
															/*particular hospital*/
															GroupSpecific(details,req,function(groupS,groupList){
																fn(groupS,groupList);
															});
														}
													}else if(req.body.group_type_id == commonConstants.hospitalGroupTypeId){
														if(groupFilter == "Any"){
															/*all ambulances*/
															//if(details.g)--> check for group type with group id
															connection.query("select group_type from group_master where group_id="+details.group_id,function(errRes,groupRes){
																if(!errRes){
																	if(groupRes[0].group_type != commonConstants.govtGroupTypeId && groupRes[0].group_type != commonConstants.privateGroupTypeId){
																		emergecyAMb(function(emerS,emerObj){
																			fn(emerS,emerObj);
																		});
																	}
																}else{
																	console.log("IOIOI ",errRes);
																	fn("err");
																}
															})


														}else{
															/*particular hospital*/
															GroupSpecific(details,req,function(groupS,groupList){
																fn(groupS,groupList);
															});
														}
													}else if(req.body.group_type_id == commonConstants.privateGroupTypeId){
														/*	if(groupFilter == "Any"){
															all ambulances
															emergecyAMb(function(emerS,emerObj){
																fn(emerS,emerObj);
															});
														}else{
															particular hospital
															GroupSpecific(details,req,function(groupS,groupList){
																fn(groupS,groupList);
															});
														}*/

														/*connection.query("select group_type from group_master where group_id="+details.group_id,function(errRes,groupRes){
															if(!errRes){
																if(groupRes[0].group_type == commonConstants.privateGroupTypeId){
																	emergecyAMb(function(emerS,emerObj){
																		fn(emerS,emerObj);
																	});
																}else{
																	if (i == res.length - 1) {
																	
																		if (nearestAmbulances.length == 0) {
																			fn("noAmbulances");
																		}else{
																			fn("success", nearestAmbulances);

																		}
																	}
																	i++;
																	iterateAmbulances();
																}
															}else{
																console.log("IOIOI ",errRes);
																fn("err");
															}
														})*/
														connection.query("select group_type from group_master where group_id="+details.group_id,function(errRes,groupRes){
															if(!errRes){
																if(groupRes[0].group_type == commonConstants.privateGroupTypeId){
																	emergecyAMb(function(emerS,emerObj){

																		fn(emerS,emerObj);
																	});
																}else{
																	if (i == res.length - 1) {


																		if (nearestAmbulances.length == 0) {

																			fn("noAmbulances");

																		}else{

																			fn("success", nearestAmbulances);

																		}
																	}
																	i++;
																	iterateAmbulances();
																}
															}else{
																console.log("IOIOI ",errRes);
																fn("err");
															}
														})
													}else if(req.body.group_type_id == commonConstants.govtGroupTypeId){
														if(groupFilter == "Any"){
															/*all ambulances*/
															emergecyAMb(function(emerS,emerObj){
																fn(emerS,emerObj);
															});
														}else{
															/*particular hospital*/
															GroupSpecific(details,req,function(groupS,groupList){
																fn(groupS,groupList);
															});
														}
													}

													function GroupSpecific(details,req,fn){
														if(req.body.group_type_id == commonConstants.anyGroupTypeId && groupFilter !="Any"){
															connection.query("select group_type from group_master where group_id="+details.group_id,function(errRes,groupRes){
																if(!errRes){
																	if(groupRes[0].group_type == commonConstants.privateGroupTypeId || (groupRes[0].group_type==commonConstants.hospitalGroupTypeId && details.group_id == req.body.group_id)){
	count = count + 1;
	emergecyAMb(function(emerS,emerObj){
                                                                                                                                                                fn(emerS,emerObj);
                                                                                                                                                        });
}else {

															if (i == res.length - 1) {
																console.log("PPPOOO ",count);
																if (count == 0) {
																	fn("noAmbulances");
																}else{

																	fn("success", nearestAmbulances);

																}
															}
															i++;
															iterateAmbulances();

														}
																}else{
																	console.log("IOIOI ",errRes);
																	fn("err");
																}
															})
														}else{
														if (details.group_id == req.body.group_id) {
															count = count + 1;
															emergecyAMb(function(emerS,emerObj){
																fn(emerS,emerObj);
															});

														} else {

															if (i == res.length - 1) {
																console.log("PPPOOO ",count);
																if (count == 0) {
																	fn("noAmbulances");
																}else{

																	fn("success", nearestAmbulances);

																}
															}
															i++;
															iterateAmbulances();

														} }
													}
													function emergecyAMb (fn){
console.log("******************************** amb id ",res[i].ambulance_id);
														getEmergencyTypePerAmbulance(res[i].ambulance_id, req.body.emergency_type, "ambulance", function(status, response) {
															console.log("EMRGENCY ============================================    ", response, status);
															logger.info("EMREGE CHEK ",response);
															logger.info("ERMERGY STATUS ",status);
															if (status == "success") {
																if (res[i].latitude != undefined && res[i].longitude != undefined) {
																	latlong = res[i].latitude + "," + res[i].longitude;
																	var destinations = [];
																	destinations.push(latlong);
																	console.log("ORIGINIS ", origins);
																	console.log("destinations ", destinations)


																	commonMethod.calculateDistance(origins, destinations, function(status, distanceDurationArray) {
																		//															console.log("DIDJI ", distanceDurationArray);
																		if (status == "distance") {
																			var maxDistance = commonConstants.distanceLimit;
																			for (var j = 0; j < distanceDurationArray.length; j++) {
																				console.log("POPOPO Distance ",distanceDurationArray[j]);
																				if (distanceDurationArray[j].distance <= maxDistance) {
																					res[i]['distance'] = distanceDurationArray[j].distance.toFixed(1);
																					res[i]['duration'] = distanceDurationArray[j].duration;
																					getAmbNumberById1(res[i].ambulance_id,res[i].driver_id,function(stat){
																						//			res[i]['ambulance_number'] = stat;
																						res[i]['ambulance_number'] = stat.ambulance_number;
																						res[i]['group_name']=stat.group_name;
																						res[i]['ambulance_mobile']=stat.ambulance_mobile;
																						res[i]['vehicleTypeName']=stat.vehicle_type_name;
																						res[i]['driverDetails'] = stat.driverDetails;
																						console.log("AMB KJLJ ",res[i]);																			
																						nearestAmbulances.push(res[i]);
																						if (i == res.length - 1) {
																							console.log("NEATTT ",nearestAmbulances);												fn("success", nearestAmbulances);
																						}
																						i++;
																						iterateAmbulances();
																					});
																				} else {
																					getAmbNumberById1(res[i].ambulance_id,res[i].driver_id,function(stat){
																						//			res[i]['ambulance_number'] = stat;
																						res[i]['ambulance_number'] = stat.ambulance_number;
																						res[i]['group_name']=stat.group_name;
																						res[i]['ambulance_mobile']=stat.ambulance_mobile;
																						res[i]['vehicleTypeName']=stat.vehicle_type_name;
																						res[i]['driverDetails'] = stat.driverDetails;
																						if (i == res.length - 1) {
																							console.log("NEATTT $$ ",nearestAmbulances);	
																							fn("success", nearestAmbulances);
																						}
																						i++;
																						iterateAmbulances();
																					});
																				}

																			}
																		} else {
																			getAmbNumberById1(res[i].ambulance_id,res[i].driver_id,function(stat){
																				//	res[i]['ambulance_number'] = stat;
																				res[i]['ambulance_number'] = stat.ambulance_number;
																				res[i]['group_name']=stat.group_name;
																				res[i]['ambulance_mobile']=stat.ambulance_mobile;
																				res[i]['vehicleTypeName']=stat.vehicle_type_name;
																				res[i]['driverDetails'] = stat.driverDetails;															if (i == res.length - 1) {
																					console.log("NEATTT ^^^^^^^ ",nearestAmbulances);
																					fn("success", nearestAmbulances);
																				}
																				i++;
																				iterateAmbulances();
																			});
																		}
																	});

																} else {
																	getAmbNumberById1(res[i].ambulance_id,res[i].driver_id,function(stat){
//																		res[i]['ambulance_number'] = stat;
																		res[i]['ambulance_number'] = stat.ambulance_number;
																		res[i]['group_name']=stat.group_name;
																		res[i]['ambulance_mobile']=stat.ambulance_mobile;
																		res[i]['vehicleTypeName']=stat.vehicle_type_name;
																		res[i]['driverDetails'] = stat.driverDetails;
																		if (i == res.length - 1) {
																			fn("success", nearestAmbulances);
																		}
																		i++;
																		iterateAmbulances();
																	});
																}
															} else if (status == "noAttr") {
																console.log("PPPP (((((((((((((((((( ");
																if (i == res.length - 1) {
																	if(nearestAmbulances.length > 0)
																		fn("success", nearestAmbulances);
																	else
																		fn("noAmbulances");

																}
																i++;
																iterateAmbulances();
															} else {
																if (i == res.length - 1) {
																	if(nearestAmbulances.length > 0)
																		fn("success", nearestAmbulances);
																	else
																		fn("noAmbulances");
																}
																i++;
																iterateAmbulances();
															}
														})
													}
												} else if (stat == "noAmb") {
													if (i == res.length - 1) {
														if(nearestAmbulances.length > 0)
															fn("success", nearestAmbulances);
														else
															fn("noAmbulances");
													}
													i++;
													iterateAmbulances();
												} else {
													fn("err", details);
												}
											});

										}

									}
								} else {
									fn("noAmbulances", res);
								}
							}
						} else {
							fn("err", err)
						}
					})
				} else if (data == "err") {
					fn("Unauthorized");
				} else if (data == "Invalid token" || data == 'No User') {
					fn(data, obj);
				}
			});
		} else {
			fn("invalidInput");
		}
	} catch (e) {
		fn("err", e);
	}

}
var getCentreNumber = function(fn){
	var connection = connections.connection;
	var query = "select * from app_settings where fieldName = 'centre_number'";
	connection.query(query,function(err,res){
		if(!err){
			if(res.length>0)
				fn("success",res[0].value);
			else
				fn(null);
		}else{
			fn(null)
		}
	})
}



var getAmbNumberById1 = function(id,driverId,fn){
	var connection = connections.connection;
	var query = "select * from ambulance_master where amb_id="+id;
	console.log("AMBULANCE NUBER ",query);
	connection.query(query,function(err,res){
		if(!err){
			if(res!=undefined){ //console.log("NUMBER ",res);
				if(res.length>0){
					var q = "select type_name from vehicle_type where vehicle_id=" + res[0].vehicle_type;
					connection.query(q,function(error,result){
						if(!error){
							res[0]['vehicle_type_name'] = result[0].type_name;
							var qu = "select group_name from group_master where group_id="+res[0].group_id;
							connection.query(qu,function(erro1,resul1){
								if(!erro1){
									res[0]['group_name'] = resul1[0].group_name;
//									fn(res[0]);
									connection.query("select * from driver_master where driver_id="+driverId,function(err2,res2){
										if(!err2){
											res[0]['driverDetails'] = res2[0];
											fn(res[0]);
										}
									})
								}
							})
						}else{
							fn(null);
						}

					})

				}
			}
		}else
			fn(null);
	});
}
/** Manual Booking **/
exports.createBookingByCommandCentre = function(req, fn) {
	req.body.tableName = "emergency_booking";
	req.body.params.booking_status = commonConstants.Pending;
	logger.info("TTTTTTTTTT ", req.body)
	if (req.body.booked_by != undefined && req.body.booked_by != "Customer") {
		req.body.customer_mobile = encryptionMob.encrypt(req.body.customer_mobile);
	}
	/*	req.body.params.customer_name = req.body.customer_name;
	req.body.params.customer_id = req.body.customer_id;
	req.body.params.emergency_latitude = req.body.emergency_latitude;
	req.body.params.emergency_longitude = req.body.emergency_longitude;
	req.body.params.emergency_geoHashCode = req.body.emergency_geoHashCode;
	req.body.params.customer_mobile = encryptionMob.decrypt(req.body.customer_mobile);
	req.body.params.booking_status = "Pending";
	req.body.params.eme_address = req.body.address;
	req.body.params.group_type_id = req.body.group_type_id;
	req.body.params.group_id = req.body.group_id;
	req.body.params.emergency_type_id = req.body.emergency_type_id;
	req.body.params.emergency_type = req.body.emergency_type;*/

/*	req.body.systemParams = {
			"booking_id": ""
	}*/

	if(req.body.params.eme_address!=null && typeof req.body.params.eme_address == "string" && req.body.params.eme_address.indexOf("'")>-1){
		req.body.params.eme_address = req.body.params.eme_address.replace("'","''");
	}
	/** bookingID **/
	/*  var querySel = "select booking_id,DATE(created_datetime) from emergency_booking where created_datetime in (select * from (select max(created_datetime) from emergency_booking) As p);";
var bookingId;
connection.query(querySel,function(bookErr,bookRes){
 if(!bookErr){
    if(bookRes!=undefined ){
if(bookRes.length>0){
        logger.info("RESUTL T^^^^^^^^^^^^^^^^^^^^^^ ",bookRes);
        if(bookRes[0]['DATE(created_datetime)'] == datetime.create().format('Y-m-d')){
//            var splitId = bookRes[0].booking_id.toString().split(datetime.create().format('Ymd').toString());
        var booking = bookRes[0].booking_id.toString();
        var splitId = booking.substring(0,8)+pad((parseInt(booking.substring(9,12))+1).toString());
                logger.info("SPLIT ID ", splitId);
//            bookingId = splitId[0] + (parseInt(splitId[1].toString()+"000")+1).toString();
                bookingId = splitId;
            req.body.params.booking_id = bookingId;
            insertData()
        }else{
            bookingId = datetime.create().format('Ymd').toString()+"0001";
            req.body.params.booking_id = parseInt(bookingId.toString());
            insertData()
        }
}else{
 bookingId = datetime.create().format('Ymd').toString()+"0001";
            req.body.params.booking_id = parseInt(bookingId.toString());
            insertData()

}

   }
 }else{

 }
});
function insertData(){*/
	getBookingID(function(idStatus,idNum){
		logger.info("ID IN CREATE ",req.body.patient_number);
		if(idStatus == "bookingId"){
			req.body.params['booking_id'] = idNum;

			appUserDao.insertData(req, function(stat, obj) {
				console.log("OPOPO ",stat,obj);
				if (stat != undefined && stat.bookingId != undefined && stat.bookingId != 0) {
					var query  = "select * from emergency_booking where booking_id="+stat.bookingId;
					connection.query(query,function(err,result){
						console.log("ERRR ",err);
						if(!err){
							if(result!=undefined){
								result[0]['is_drop_location']=(result[0]['drop_latitude']==null && result[0]['drop_longitude']==null)?false:true;
								fn("success", result[0]);
								var sendObj = {
										mobilenumber:(req.body.params.patient_number!=undefined||req.body.params.patient_number!=null)?req.body.params.patient_number:req.body.params.customer_mobile,
//                                                        customer_name:req.body.params.customer_name,
  //                                                      bookingId:stat.bookingId,
                                                        message:"Dear Customer, you have raised request for an ambulance. We are processing your request, booking id is "+stat.bookingId+". Use this booking id for any further information. Esahai!!  "};
						logger.info("Message", sendObj);
                                                        loginDao.sendSMS(sendObj,function(smsStat,obj){
                                                                if(smsStat == "success")
                                                                 logger.info("SMS sent to "+sendObj.mobilenumber);
                                                                else logger.info("Failed to send sms");
                                                        });

								/** send email**/
								var data= "",emailList=commonConstants.To,subject="Booking_id:"+stat.bookingId,html="<p>Customer ("+req.body.params.customer_name+"-"+req.body.params.customer_mobile+") requested for emergency, Booking id is "+stat.bookingId+". </p>";

								sendemail.sendEmail(data,emailList,subject,html);
							}
						}else{
							fn("err",err);
						}
					})			
				}else fn(stat, obj);

			});
		}else{
			logger.info("Error while getting booking id");
		}
	});

//	};
};
exports.assignBookingByCommandCentre = function(req, fn) {
	req.body.tableName = "emergency_booking";
	req.body.params.booking_status = commonConstants.Accepted;
	var selQuery = "select * from " + req.body.tableName + " where booking_id=" + req.body.conditionalParams.booking_id;
	connection.query(selQuery, function(error, result) {
		if (!error) {
			if (result != undefined) {
				if (result.length > 0) {
					var emergencyObj = result[0];
					console.log("OOOO STATUS ",result[0].booking_status);
					if(result[0]['booking_status'] == commonConstants.Cancelled || result[0]['booking_status'] == commonConstants.FakeCancel){
						fn("cancelled",result);
					} else if (result[0]['booking_status'] == commonConstants.TripClosed || result[0]['booking_status'] == commonConstants.ReachedDestination) {
                        fn("closed", result);
                    }
					else if (result[0]['booking_status'] != commonConstants.Pending && result[0]['booking_status'] != commonConstants.NoOneAccepted && result[0]['booking_status'] !=commonConstants.TransferredNoResponse && result[0]['booking_status'] !="No Ambulance") {
						fn("alreadyBooked", result);
					}     
					else {
						/** check whether amb is online or not **/
						var ambQuery = "select * from ambulance_driver WHERE ambulance_id=" + req.body.params.ambulance_id + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.params.ambulance_id + ") AS p )";
						connection.query(ambQuery,function(ambErr,ambObj){
							if(ambErr){
								fn("err");
							}else{
								if(ambObj!=undefined){
									if(ambObj.length == 0){
										fn("noAmbulanceFound");
									}else{
										if(ambObj[0].status == 'Offline'){
											fn("Offline");
										}else{
											if(ambObj[0].is_assigned == true){
                                                fn("onTrip");
	                                        }else{
		                                        assignAmb(function(assignStat,assignObj){
		                                                fn(assignStat,assignObj);
		                                        });
	                                        }
										}
									}
								}
							}
						});
						function assignAmb(fn){
						appUserDao.updateData(req, function(stat, obj) {
							console.log("OPOP ",stat, obj);
							if (stat != undefined && stat.updation == true) {
								var query = "UPDATE ambulance_driver SET is_assigned=true WHERE ambulance_id=" + req.body.params.ambulance_id + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.params.ambulance_id + ") AS p )";
								connection.query(query, function(err, res) {
									if (!err) {
										var track = {
												'distance': req.body.params.distance,
												'estimated_time_arrival': req.body.params.duration,
												'cost': req.body.params.cost,
												'driverId':req.body.driverId,
												'booking_status':req.body.params.booking_status,
												'ambulanceId': req.body.params.ambulance_id,
												'speed':'',
												'emergency_latitude': emergencyObj.emergency_latitude,
												'emergency_longitude': emergencyObj.emergency_longitude,
												'longitude': req.body.params.ambulance_start_longitude,
												'latitude': req.body.params.ambulance_start_latitude,
												'geoHashCode':req.body.params.geoHashCode,
												'is_booked':true,
												'booking_id':req.body.conditionalParams.booking_id,
												'end_latitude':(req.body.hospital_latitude!=undefined)?req.body.hospital_latitude:emergencyObj.drop_latitude,
												'end_longitude':(req.body.hospital_longitude!=undefined)?req.body.hospital_longitude:emergencyObj.drop_longitude
										}; 
										console.log("TRACK ",track);
										generalDao.insertIntoTracking(track, function(stat, data) {
											if (stat == "success") {
												// fn(stat, obj);
												/** insert into accepted_ambulances**/
												var acceptedQuery = "insert into accepted_ambulances(ambulance_id,booking_id) values("+req.body.params.ambulance_id+","+req.body.conditionalParams.booking_id+")"
												connection.query(acceptedQuery,function(acceErr,acceObj){
													if(!acceErr){
														getCentreNumber(function(numStat,number){
															if(numStat == "success"){
																fn(stat,number);
																var message = {
																		distance: req.body.params.distance,
																		duration: req.body.params.duration,
																		booking_id: emergencyObj.booking_id,
																		latitude: emergencyObj.emergency_latitude,
																		longitude: emergencyObj.emergency_longitude,
																		customerMobile: (emergencyObj.is_self==true)?emergencyObj.customer_mobile:emergencyObj.patient_number,
																				customerName: emergencyObj.customer_name,
																				age: req.body.customer_age,
																				address: emergencyObj.eme_address,
																				emergencyType: emergencyObj.emergency_type,
																				hospital: emergencyObj.hospital,
																				hospital_latitude: (req.body.hospital_latitude!=undefined)?req.body.hospital_latitude:emergencyObj.drop_latitude,
																				hospital_longitude:(req.body.hospital_longitude!=undefined)?req.body.hospital_longitude:emergencyObj.drop_longitude,
																				hospital_geoHashCode:(req.body.hospital_geoHashCode!=undefined)? req.body.hospital_geoHashCode:emergencyObj.drop_geoHashCode,
																				is_medical_taxi:(emergencyObj.emergency_type_id != commonConstants.medicalTaxiId)?false:true
																}
																var deviceTokens = [];
																var obj = {
																		ambulance_id : req.body.params.ambulance_id,
																		tableName:"ambulance_master"
																}
																getDeviceTokenById1(obj, function(status, data) {
																	if (status == "success") {
//																		var deviceToken = [];
																		deviceTokens = [data.device_token];
																		console.log("DEVICE TOEK ",deviceTokens);	
																		var sendObj = {user_type:"Ambulance",send_to:data.ambulance_mobile};
																		var pnData = "command centre has assigned an emergency for you, please pick up the customer";
																			var type = "booking_from_commandCentre";
//																		sendPNDao.sendPN(deviceTokens, pnData, message, type);
																		var cusObj1 = {
																				tableName: "customer_master",
																				customer_id: emergencyObj.customer_id
																		}
																		getDeviceTokenById(cusObj1, function(status1, data1) {
																			if (status1 == "success") {
																				//message['gcmToken'] = data1;
		 message['gcmToken'] = data1.device_token;
                 message['device_type']=data1.device_type;
																				logger.info("MESAGE ******************** ",message);
																				sendPNDao.sendPN(deviceTokens, pnData, message, type,sendObj);

																			} else {
																				fn("errPn", data1);
																			}
																		});
																	} else {
																		fn("errPN", data);
																	}
																})
															}else{
																fn("centreNumberFail",obj);
															}
														});
													}else{
														fn("assignedButUpdateFail",data);
													}
												});
											}else{
												fn("assignedButUpdateFail",data);
											}
										})
									} else {
										fn("err", err);
									}
								})



							} else
								fn(stat, obj);
						});
						}
					}
				}
			}
		}
	})
}
//};


exports.getEmergencies = function(req,fn){
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		var query = "select * from emergency_booking";
		if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
			tokReq['customerApp'] = req.body.customerApp;
			tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
			query += " where customer_id = " + req.body.customer_id + " order by created_datetime desc limit 25";
		}
		console.log("ERQEW ",query);
		connection.query(query,function(err,res){
			console.log("ERRR ",err);
			console.log("REs ",res);
			if(!err){
				if(res!=undefined){
					if(res.length>0){
						var j = 0;
						iterateTrip();
						function iterateTrip() {
							if (j < res.length) {
								var q = "select * from tracking where booking_id="+res[j].booking_id+" order by created_date desc ";
								console.log("QEURO ",q);			connection.query(q,function(error,result){
									console.log("OPOOOOOOOOOOOOOOOOOOO ",result);

									if(!error){
										if(result!=undefined){
											if(result.length>0){
												res[j]['hospital_longitude']=result[0].end_longitude;
												res[j]['hospital_latitude']=result[0].end_latitude;
												res[j]['hospital_geoHashCode']=result[0].end_geoHashCode;
												res[j]['speed'] = result[0].speed;
												var qu = "select * from driver_master where driver_id="+result[0].driver_id;
												console.log("QERUIU ",qu); 					connection.query(qu,function(errq,resq){
													if(!errq){
														res[j]['driver_profile'] = resq[0].file_url;
														res[j]['driver_name']=resq[0].driver_name;
														/*		if(j == res.length-1){
															fn("success", res);
														}
														j++;
														iterateTrip();*/
														var que = "select id,points,reason from feedback where customer_id="+req.headers.userid+" and booking_id="+res[j].booking_id;
														connection.query(que,function(errF,resF){
															if(!errF){
																if(resF!=undefined && resF.length>0){
																	res[j]['feedback']=resF[0];
																	if(j == res.length-1){
																		fn("success", res);
																	}
																	j++;
																	iterateTrip();
																}else{
																	if(j == res.length-1){
																		fn("success", res);
																	}
																	j++;
																	iterateTrip();
																}
															}else{
																if(j == res.length-1){
																	fn("success", res);
																}
																j++;
																iterateTrip();
															}
														})
													}else{
														console.log("OPOOOOOOOOOOOOOOOOOOO ",errq);

														res[j]['driver_profile'] = "no driver found";
														if(j == res.length-1){
															fn("success", res);
														}
														j++;
														iterateTrip();
													}
												})
											}else{
												if(j == res.length-1){
													fn("success", res);
												}
												j++;
												iterateTrip();
											}
										}
									}else{
										if(j == res.length-1){
											fn("success", res);
										}
										j++;
										iterateTrip();
									}
								})
							}
						}
					}else{
						fn("noData",res);
					}
				}
			}else{
				console.log("OPOOOOOOOOOOOOOOOOOOO ",err);
				fn("err", err);
			}
		})
	} catch (e) {
		console.log("ERR ",e)
		fn("err", e);
	}
}
var pad = function pad(str){
	str = str.toString();
	return str.length < 4 ? pad('0'+str,4) : str;
}



exports.checkMyBookingStatus = function(req,fn){
	logger.info("==== checkMyBookingStatus =====");
	logger.info("Request :: ",req.body);
	logger.info("Headers :: ",req.headers);
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		var query = "select * from emergency_booking where booking_id="+req.body.booking_id;
		if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
			tokReq['customerApp'] = req.body.customerApp;
			tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
		}
		commonMethod.verifyUser(tokReq, function(data, obj) {
			if (data == "success") {
				connection.query(query,function(err,res){
					if(!err){
						if(res!=undefined){
							if(res.length == 0){
								fn("inValidBookingId");
							}else{
								if(res[0].booking_status == commonConstants.Pending || res[0].booking_status == commonConstants.NoOneAccepted || res[0].booking_status == commonConstants.TransferredNoResponse || res[0].booking_status == "No Ambulance"){
									fn("noOneAccepted");
									/** update status as no one responded*/
									var updateObj = {
											booking_id:req.body.booking_id,
											status:(req.body.transferred==true)?commonConstants.TransferredNoResponse:commonConstants.NoOneAccepted
									}
									updateBookingStatusToNoOneAccepted(updateObj);

								}else if(res[0].booking_status == commonConstants.Cancelled || res[0].booking_status == commonConstants.FakeCancel){
									fn("cancelled");
								}else{
									var message = {
											'distance': res[0].distance,
											'duration': res[0].duration,
											'cost': res[0].cost,
											'ambulanceNumber': res[0].ambulance_number,
											'emergencyLatitude': res[0].emergency_latitude,
											'emergencyLongitude': res[0].emergency_longitude,
											'ambulanceHospital': res[0].hospital,
											'booking_status': res[0].booking_status,
											is_medical_taxi:(res[0].emergency_type_id != commonConstants.medicalTaxiId)?false:true

									};
									var q = "select * from tracking where booking_id="+req.body.booking_id+" and created_date in(select * from (select Max(created_date) from tracking where booking_id="+req.body.booking_id+") as p);";
									connection.query(q,function(error,result){
										if(!error){
											if(result!=undefined){
												if(result.length > 0){
													message['ambulance_longitude']= result[0].longitude;
													message['ambulance_latitude']= result[0].latitude;
													message['hospital_longitude']=result[0].end_longitude;
													message['hospital_latitude']=result[0].end_latitude;
													var driverQ = "select * from driver_master where driver_id="+result[0].driver_id;
													connection.query(driverQ,function(errDriver,resDriver){
														if(!errDriver){
															if(resDriver.length==0){
																fn("noDriverDetails");     										
															}else{
																message['driverName'] = resDriver[0].driver_name;
																message['driverMobileNumber'] = resDriver[0].driver_mobile_number;
																message['driverProfilePic'] = resDriver[0].file_url
																fn("success",message);
															}
														}else{

															logger.info("Error in getting Driver details",errDriver);
															fn("errDriver");
														}
													})

												}else{
													fn("noTrackDetails")
												}
											}
										}else{
											logger.info("Error in getting Tracking details",error);

											fn("errTrack");
										}

									})

								}
							}
						}
					}else{
						logger.info("Error in getting booking details",err);
						fn("err");
					}

				})


			} else if (data == "err") {
				fn("err");
			} else if (data == "Invalid token" || data == 'No User') {
				fn(data, obj);
			}else if(data == "Unauthorized"){
				fn("Unauthorized");
			}else{
				fn(data,obj);
			}
		});

	} catch (e) {
		logger.info("Exception  ",e)
		fn("err", e);
	}
}

var updateBookingStatusToNoAmbulanceFound = function(req){
	logger.info("==  updateBookingStatusToNoAmbulanceFound ==",req);
	var query = "update emergency_booking set booking_status = '"+commonConstants.NoAmbulanceFound+"' where booking_id="+req.booking_id;
	logger.info(" Query ",query);
	connection.query(query,function(err,res){
		if(!err){
			if(res!=undefined){
				logger.info("UPDATED ");
//				fn("success");
			}
		}else{
			logger.info("Error ",err);
//			fn("updateErr");
		}
	})

}

var updateBookingStatusToNoOneAccepted = function(req,fn){
    logger.info("==  updateBookingStatusToNoOneAccepted ==",req);
    var query = "update emergency_booking set booking_status = '"+req.status+"' where booking_id="+req.booking_id;
    logger.info(" Query ",query);
    connection.query(query,function(err,res){
            if(!err){
                    if(res!=undefined){
            //              fn("success");
logger.info("UPDATED ");

                    }
            }else{
                    logger.info("Error ",err);
//                  fn("updateErr");
            }
    })

}



exports.getActiveBooking = function(req,fn){
	logger.info("==== getActiveBooking =====");
	logger.info("Request :: ",req.body);
	logger.info("Headers :: ",req.headers);
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		var query = "select * from emergency_booking ";
		if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
			tokReq['customerApp'] = req.body.customerApp;
			tokReq['customerMobile'] = encryptionMob.decrypt(req.body.customer_mobile);
			query += "where customer_id="+req.headers.userid;
		}
		if(req.body.ambulanceApp!=undefined  && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true") ){
			query += "where ambulance_id="+req.headers.userid;
		}
		query += " order by created_datetime desc limit 1";
		commonMethod.verifyUser(tokReq, function(data, obj) { data = "success";
		if (data == "success") {
			connection.query(query,function(err,res){
				if(!err){
					if(res!=undefined){
						if(res.length == 0){
							fn("noActiveBookings");
						}else{
							if(res[0].booking_status == commonConstants.Pending){
								fn("noOneAccepted");
							}else if(res[0].booking_status ==commonConstants.NoAmbulanceFound){
								fn("noOneAccepted");
							}else if(res[0].booking_status == commonConstants.NoOneAccepted || res[0].booking_status == commonConstants.TransferredNoResponse){
								fn("noOneAccepted");
							}else if(res[0].booking_status == commonConstants.TripClosed){
								fn("noActiveBookings");
							}else if(res[0].booking_status == commonConstants.Cancelled || res[0].booking_status == commonConstants.FakeCancel){
								fn("noActiveBookings");
							}else{
								var message = {
										'distance': res[0].distance,
										'duration': res[0].duration,
										'cost': res[0].cost,
										'ambulanceNumber': res[0].ambulance_number,
										'emergencyLatitude': res[0].emergency_latitude,
										'emergencyLongitude': res[0].emergency_longitude,
										'ambulanceHospital': res[0].hospital,
										'is_self':res[0].is_self,
										is_medical_taxi:(res[0].emergency_type_id != commonConstants.medicalTaxiId)?false:true

										//										'booking_status': res[0].booking_status,
										//										'booking_id':res[0].booking_id
								};
								message["booking_id"]= res[0].booking_id;
								message["booking_status"]=res[0].booking_status;
								/*message.booking_id = res[0].booking_id;
message.booking_status = res[0].booking_status;*/
								console.log("MESSAGE ",message);
								var q = "select * from tracking where booking_id="+res[0].booking_id+" and created_date in(select * from (select Max(created_date) from tracking where booking_id="+res[0].booking_id+") as p);";
								connection.query(q,function(error,result){
									if(!error){
										if(result!=undefined){
											if(result.length > 0){
												message['ambulance_longitude']= result[0].longitude;
												message['ambulance_latitude']= result[0].latitude;
												message['hospital_longitude']=result[0].end_longitude;
												message['hospital_latitude']=result[0].end_latitude;
												var driverQ = "select * from driver_master where driver_id="+result[0].driver_id;
												connection.query(driverQ,function(errDriver,resDriver){
													if(!errDriver){
														if(resDriver.length==0){
															fn("noDriverDetails");     										
														}else{
															message['driverName'] = resDriver[0].driver_name;
															message['driverMobileNumber'] = resDriver[0].driver_mobile_number;
															message['driverProfilePic'] = resDriver[0].file_url
															fn("success",message);
														}
													}else{

														logger.info("Error in getting Driver details",errDriver);
														fn("errDriver");
													}
												})

											}else{
												fn("noTrackDetails")
											}
										}
									}else{
										logger.info("Error in getting Tracking details",error);

										fn("errTrack");
									}

								})

							}
						}
					}
				}else{
					logger.info("Error in getting booking details",err);
					fn("err");
				}

			})
		} else if (data == "err") {
			fn("err");
		} else if (data == "Invalid token" || data == 'No User') {
			fn(data, obj);
		}else if(data == "Unauthorized"){
			fn("Unauthorized");
		}else{
			fn(data,obj);
		}
		});

	} catch (e) {
		logger.info("Exception  ",e)
		fn("err", e);
	}
}
