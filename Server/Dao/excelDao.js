var datetime = require('node-datetime');
var formidable = require('formidable'),
    http = require('http'),
    util = require('util'),
    fs = require('fs-extra');
xlsj = require("xls-to-json");
var XLSX = require('xlsx');
var commonMethod = require("../Util/commonMethod.js");
var sendPNDao = require("../Dao/sendPNDao.js");
var logger = require("../logging/logger.js");
var encryptionMob = require("../Util/encryptionMob.js");
var connections = require("../Util/dbConnection.js");
var statusConstant = require("../Util/statusConstants.json");
var connections = require("../Util/dbConnection.js");
exports.uploadExcel = function(req, fn) {
    var tokReq = {
        token: req.headers.token,
        userId: req.headers.userid,
        method: req.method
    }
    var jsondata = req.body.jsondata;
    jsondata = JSON.parse(jsondata);
    if (jsondata.userType != undefined && jsondata.userType != null) {
        tokReq['userType'] = jsondata.userType;
    }

    commonMethod.verifyUser(tokReq, function(data, obj) {
        if (data == "success") {
            if (req.files.ambulance_master != undefined) {

                uploadFile(req.files.ambulance_master, req.files.ambulance_master.fieldName, function(status, data) {
                    if (status == "err") {
                        fn("failure")
                    } else {
                        var incomeKeys = ["Operation", "Ambulance Number", "Mobile", "Group", "Emergency Type"];
                        var keys = Object.keys(data[0]);
                        campareArrays(incomeKeys.sort(), keys.sort(), function(data2) {
                            if (data2 == true) {

                                CreateAndInsertIntoTable(data, req.files.ambulance_master.fieldName, function(err, count) {
                                    if (err == undefined) {
                                        fn("fail");
                                    } else if (err == 'upload not completed succesfully') {
                                        console.log("sa")
                                        fn("upload not completed succesfully", count);
                                    } else {
                                        fn(err);
                                    }
                                });
                            } else {
                                fn("failure")
                            }

                        });

                    }

                });
            } else if (req.files.group_master != undefined) {

                uploadFile(req.files.group_master, req.files.group_master.fieldName, function(status, data) {
                    if (status == "err") {
                        fn("failure")
                    } else {
                        var incomeKeys = ["Operation", "Group Name", "Short Name", "Group Type", "Limit", "Latitude", "Longitude", "Address", "City", "Pincode", "State", "Emergency Contact", "Emergency Email", "Emergency Number", "Status"];
                        var keys = Object.keys(data[0]);
                        campareArrays(incomeKeys.sort(), keys.sort(), function(data2) {
                            if (data2 == true) {

                                CreateAndInsertIntoTable(data, req.files.group_master.fieldName, function(err, count) {
                                    console.log("CreateAndInsertIntoTable", err, count)

                                    if (err == undefined) {
                                        fn("fail");
                                    } else if (err == 'upload not completed succesfully') {
                                        console.log("sa")
                                        fn("upload not completed succesfully", count);
                                    } else {
                                        fn(err);
                                    }
                                });
                            } else {
                                fn("failure")
                            }

                        });

                    }

                });
            } else if (req.files.driver_master != undefined) {

                uploadFile(req.files.driver_master, req.files.driver_master.fieldName, function(status, data) {
                    if (status == "err") {
                        fn("failure")
                    } else {

                        var incomeKeys = ["Operation", "Group", "Driver Name", "Mobile Number", "License Number"];
                        var keys = Object.keys(data[0]);
                        campareArrays(incomeKeys.sort(), keys.sort(), function(data2) {
                            if (data2 == true) {

                                CreateAndInsertIntoTable(data, req.files.driver_master.fieldName, function(err, count) {
                                    if (err == undefined) {
                                        fn("fail");
                                    } else if (err == 'upload not completed succesfully') {
                                        console.log("sa")
                                        fn("upload not completed succesfully", count);
                                    } else {
                                        fn(err);
                                    }
                                });
                            } else {
                                fn("failure")
                            }

                        });

                    }

                });
            } else if (req.files.customer_master != undefined) {

                uploadFile(req.files.customer_master, req.files.customer_master.fieldName, function(status, data) {
                    if (status == "err") {
                        fn("failure")
                    } else {

                        var incomeKeys = ["Operation", "Name", "Email", "Age", "City", "State", "Mobile", "Status"];
                        var keys = Object.keys(data[0]);
                        campareArrays(incomeKeys.sort(), keys.sort(), function(data2) {
                            if (data2 == true) {

                                CreateAndInsertIntoTable(data, req.files.customer_master.fieldName, function(err, count) {
                                    if (err == undefined) {
                                        fn("fail");
                                    } else if (err == 'upload not completed succesfully') {
                                        console.log("sa")
                                        fn("upload not completed succesfully", count);
                                    } else {
                                        fn(err);
                                    }
                                });
                            } else {
                                fn("failure")
                            }

                        });

                    }

                });
            } else {
                fn(null);
            }

        } else if (data == "err") {
            fn("Unauthorized");
        } else if (data == "Invalid token" || data == 'No User') {
            fn(data, obj);
        }
    });
}

function CreateAndInsertIntoTable(data1, tableName, fn) {
    logger.info("Calling CreateAndInsertIntoTable");
    var data = data1;

    var i = 0,
        j = 0;
    var topic = "";
    iterateApp();

    function iterateApp() {
        logger.info("Calling length", data1.length, i);

        if (i < data1.length) {
            var json = {};
            json = data1[i];

            if (tableName == "ambulance_master") {
                topic = "ambulance_master";

                logger.info("Calling iterateApp");
                var sql = "";
                getEmergencyTypeIdByType(json['Emergency Type'], function(emergency_type) {
                    getGroupIdByGroupName(json["Group"], function(group_id) {
                        json["Operation"] = json["Operation"].toUpperCase();

                        if (json["Operation"] == "ADD") {

                            sql = "insert into " + tableName + " (ambulance_number,ambulance_mobile,group_id,emergency_type) values ('" +
                                json["Ambulance Number"] + "','" + json["Mobile"] + "','" + group_id + "','" + emergency_type + "')";
                        } else if (json["Operation"] == "EDIT") {

                            sql = "update " + tableName + " set group_id ='" + group_id + "',  ambulance_mobile='" + json['Mobile'] + "' ,emergency_type ='" + emergency_type + "' where ambulance_number = '" + json['Ambulance Number'] + "'";

                        }/* else if (json["Operation"] == "DELETE") {
                            sql = "update " + tableName + "set is_active ='InActive' where ambulance_number = '" + json['Ambulance Number'] + "' and  group_id ='" + group_id + "' and  ambulance_mobile='" + json['Mobile'] + "'  and emergency_type ='" + emergency_type + "'";
                        } */else if (json['Operation'] == '') {
                            j++;
                            console.log("I is ", i, "J is ", j)
                        }
                        logger.info("query TO BE PERFORMED ", sql);
                        connection.query(sql, function(err, result) {
                            if (err) {
                                console.log('err', err);
                                if (err.code == '1062') {
                                    if (typeof err == 'object') {
                                        var res = JSON.stringify(err);
                                    }

                                    var resp = res.indexOf('ambulance_mobile_uni') ? json['Mobile'] : json['Ambulance Number'];
                                    updateAmbulanceDetails();

                                    function updateAmbulanceDetails() {
                                        if (resp == json['Ambulance Number']) {
                                            sql = "update " + tableName + " set group_id ='" + group_id + "',  ambulance_mobile='" + json['Mobile'] + "' ,emergency_type ='" + emergency_type + "' where ambulance_number = '" + json['Ambulance Number'] + "'";
                                            connection.query(sql, function(err, result2) {
                                                if (!err) {
                                                    logger.info("query  PERFORMED ", sql);
                                                }
                                            })
                                        } else {

                                            sql = "update " + tableName + " set group_id ='" + group_id + "',  emergency_type ='" + emergency_type + "' , ambulance_number = '" + json['Ambulance Number'] + "' where ambulance_mobile='" + json['Mobile'] + "'";
                                            connection.query(sql, function(err, result3) {
                                                if (!err) {
                                                    console.log(result3, ":::::::::result3")
                                                    logger.info("query  PERFORMED ", sql);
                                                }
                                            })
                                        }
                                    }
                                }
                                groupResp(topic);

                                i++;
                                iterateApp();
                            } else if (result != undefined) {
                                groupResp(topic);

                                i++;
                                iterateApp();
                            } else {
                                groupResp(topic);

                                i++;
                                iterateApp();
                            }


                        });
                    });
                });

            } else if (tableName == "group_master") {
                topic = "group_master";
                logger.info("Calling iterateApp");

                var sql = "";
                getGroupTypeByGroupName(json["Group Type"], function(group_type) {

                    json["Operation"] = json["Operation"].toUpperCase();
                    if (json["Operation"] == "ADD") {

                        sql = "insert into " + tableName + " (group_name,group_short_name,group_type,group_limit,group_latitude,group_longitude,group_address,group_city,pincode,group_state,emergency_contact,emergency_email,emergency_number,is_active) values ('" +
                            json["Group Name"] + "','" + json['Short Name'] + "','" + group_type + "','" + json['Limit'] + "','" + json['Latitude'] + "','" + json['Longitude'] + "','" + json['Address'] + "','" + json['City'] + "','" + json["Pincode"] + "','" + json["State"] + "','" + json["Emergency Contact"] + "','" + json["Emergency Email"] + "','" + json["Emergency Number"] + "','" + json["Status"] + "')";
                    } else if (json["Operation"] == "EDIT") {

                        sql = "update " + tableName + " set  group_short_name='" + json['Short Name'] + "', group_type='" + group_type + "' ,group_limit ='" + json['Limit'] + "',group_latitude ='" + json['Latitude'] + "',group_longitude ='" + json['Longitude'] + "',group_address='" + json['Address'] + "',group_city='" + json['City'] + "',pincode='" + json['Pincode'] + "',group_state='" + json['State'] + "',emergency_contact='" + json['Emergency Contact'] + "',emergency_email='" + json['Emergency Email'] + "',emergency_number='" + json['Emergency Number'] + "',is_active ='" + json['Status'] + "' where group_name = '" + json["Group Name"] + "'";

                    } /*else if (json["Operation"] == "DELETE") {
                        sql = "update " + tableName + " set is_active='InActive' where  group_short_name='" + json['Short Name'] + "' and  group_type='" + group_type + "'  and group_limit ='" + json['Limit'] + "' and group_latitude ='" + json['Latitude'] + "' and group_longitude ='" + json['Longitude'] + "' and group_address='" + json['Address'] + "' and group_city='" + json['City'] + "' and pincode='" + json['Pincode'] + "' and group_state='" + json['State'] + "' and emergency_contact='" + json['Emergency Contact'] + "' and emergency_email='" + json['Emergency Email'] + "' and emergency_number='" + json['Emergency Number'] + "' and group_name = '" + json["Group Name"] + "'";
                    } */else if (json['Operation'] == '') {
                        j++;
                        console.log("I is ", i, "J is ", j)
                    }
                    // logger.info("query TO BE PERFORMED ", sql);
                    connection.query(sql, function(err, result) {
                        if (err) {
                            groupResp(topic);
                            i++;
                            iterateApp();
                        } else if (result != undefined) {
                            groupResp(topic);
                            i++;
                            iterateApp();
                        } else {
                            groupResp(topic);
                            i++;
                            iterateApp();
                        }



                    });
                });




            } else if (tableName == "driver_master") {
                logger.info("Calling iterateApp");
                topic = "driver_master";

                var sql = "";
                getGroupIdByGroupName(json["Group"], function(group_id) {
                    json["Operation"] = json["Operation"].toUpperCase();

                    if (json["Operation"] == "ADD") {
                        var driver_pin = commonMethod.getRandomDriverPin();

                        sql = "insert into " + tableName + " (group_id,driver_name,driver_mobile_number,license_number,driver_pin) values ('" +
                            group_id + "','" + json['Driver Name'] + "','" + json['Mobile Number'] + "','" + json['License Number'] + "','" + driver_pin + "')";

                    } else if (json["Operation"] == "EDIT") {

                        sql = "update " + tableName + " set  group_id='" + group_id + "' ,driver_mobile_number='" + json['Mobile Number'] + "',license_number ='" + json['License Number'] + "' where  driver_name='" + json['Driver Name'] + "'";

                    }/* else if (json["Operation"] == "DELETE") {
                        sql = "update " + tableName + " set is_active= 'InActive'  where group_id='" + group_id + "' and driver_mobile_number='" + json['Mobile Number'] + "' and license_number ='" + json['License Number'] + "'  and  driver_name='" + json['Driver Name'] + "'";
                    } */else if (json['Operation'] == '') {
                        j++;
                        console.log("I is ", i, "J is ", j)
                    }
                    // logger.info("query TO BE PERFORMED ", sql);
                    connection.query(sql, function(err, result) {
                        if (err) {
                            groupResp(topic);
                            i++;
                            iterateApp();
                        } else if (result != undefined) {
                            groupResp(topic);
                            i++;
                            iterateApp();
                        } else {
                            groupResp(topic);
                            i++;
                            iterateApp();
                        }


                    });
                });

            } else if (tableName == "customer_master") {
                topic = "customer_master";

                logger.info("Calling iterateApp");

                var sql = "";
                json["Operation"] = json["Operation"].toUpperCase();

                if (json["Operation"] == "ADD") {

                    sql = "insert into " + tableName + " (customer_name ,customer_email,age,city,state,customer_mobile_number,is_active) values ('" +
                        json['Name'] + "','" + json['Email'] + "','" + json['Age'] + "','" + json['City'] + "','" + json['State'] + "','" + json["Mobile"] + "','" + json['Status'] + "')";
                } else if (json["Operation"] == "EDIT") {

                    sql = "update " + tableName + " set  customer_email='" + json['Email'] + "' ,age='" + json['Age'] + "',city ='" + json['City'] + "',state ='" + json['State'] + "', customer_name='" + json['Name'] + "', is_active='" + json['Status'] + "' where  customer_mobile_number='" + json['Mobile'] + "'";

                } /*else if (json["Operation"] == "DELETE") {
                    sql = "update " + tableName + " set is_active='InActive' where  customer_email='" + json['Email'] + "' and age='" + json['Age'] + "' and city ='" + json['City'] + "' and state ='" + json['State'] + "' and customer_mobile_number='" + json['Mobile'] + "' and device_imei='" + json['Device IMEI'] + "' and device_type='" + json['Device Type'] + "' and device_os='" + json['Device OS'] + " ' and is_active='" + json['Status'] + "' and  customer_name='" + json['Name'] + "'";

                } */else if (json['Operation'] == '') {
                    j++;
                    console.log("I is ", i, "J is ", j)
                }
                logger.info("query TO BE PERFORMED ", sql);
                connection.query(sql, function(err, result) {
                    if (err) {

                        if (err.code == '1062') {
                            if (typeof err == 'object') {
                                var res = JSON.stringify(err);
                            }

                            updateAmbulanceDetails();

                            function updateAmbulanceDetails() {

                                sql = "update " + tableName + " set  customer_email='" + json['Email'] + "' ,age='" + json['Age'] + "',city ='" + json['City'] + "',state ='" + json['State'] + "', customer_name='" + json['Name'] + "',device_imei='" + json['Device IMEI'] + "', device_type='" + json['Device Type'] + "',device_os='" + json['Device OS'] + " ', is_active='" + json['Status'] + "' where  customer_mobile_number ='" + json["Mobile"] + "'";
                                connection.query(sql, function(err, result2) {
                                    if (!err) {
                                        logger.info("_____________query  PERFORMED ", sql);
                                    } else {}

                                })
                            }
                        }
                        groupResp(topic);
                        i++;
                        iterateApp();
                    } else if (result != undefined) {
                        groupResp(topic);
                        i++;
                        iterateApp();
                    } else {
                        groupResp(topic);
                        i++;
                        iterateApp();
                    }



                });


            }

        } else {
            fn(undefined)
        }

        function groupResp(topic) {
            if (i == data1.length - 1) {
                console.log("count of I is=" + i + " and j is =" + data1.length + "customer_master topic ====" + topic)
                if (j == 0) {
                    fn("success");
                } else {
                    fn("upload not completed succesfully", j)
                }
            }
            var data = tableName + " has been updated";
            var message = "update the " + tableName + " list";
            sendPNDao.sendPNToTopic(topic, data, message);


        }


    }
}

function campareArrays(array1, array2, res) {
    var is_same = (array1.length == array2.length) && array1.every(function(element, index) {
        return element === array2[index];
    });
    res(is_same);
}

function uploadFile(uploadPath, name, cb) {
    console.log("name" + name, "path ", uploadPath.path);

    fs.readFile(uploadPath.path, function(err, data) {
        fs.writeFile('./excels/' + name + '.xls', data, function(err) {
            if (err) {
                cb("err", err);
            }
            var input = './excels/' + name + '.xls';
            var output = './excels/' + name + '.json';
            xlsj({
                input: input,
                output: output
            }, function(err, result) {
                if (err) {
                    cb("err", err);
                } else {
                    cb("success", result);

                }
            });
        });
    });
}
/*getGroupTypeByGroupName*/
var getGroupTypeByGroupName = function(group_type, cb) {
        var connection = connections.connection;
        if (group_type != null) {
            var group_type_id = "";
            var i = 0;
            iterateCircle();

            function iterateCircle() {


                var query = "select id from group_types where type_name='" + group_type + "';";
                connection.query(query, function(err, res1) {
                    if (!err) {
                        if (res1 != undefined && res1.length > 0) {
                            group_type_id += "" + res1[0].id;

                            if (res1.length - 1 == i) {

                                group_type_id += "";
                                cb(group_type_id);
                            } else {
                                group_type_id += ",";
                                i++;
                                iterateCircle();
                            }
                        } else {
                            cb(null);
                        }

                    } else {
                        cb(null);
                    }
                })

            }
        }
    }
    /* getGroupIdByGroupName*/
var getGroupIdByGroupName = function(group_name, cb) {
        var connection = connections.connection;
        if (group_name != null) {
            var group_id = "";
            var i = 0;
            iterateCircle();

            function iterateCircle() {


                var query = "select group_id from group_master where group_name='" + group_name + "';";
                connection.query(query, function(err, res1) {
                    if (!err) {
                        if (res1 != undefined && res1.length > 0) {
                            group_id += "" + res1[0].group_id;

                            if (res1.length - 1 == i) {
                                group_id += "";
                                cb(group_id);
                            } else {
                                group_id += ",";
                                i++;
                                iterateCircle();
                            }
                        } else {
                            cb(null);
                        }

                    } else {
                        cb(null);
                    }
                })

            }
        }
    }
    /* getEmergencyTypeId from Emergency Type Name*/
var getEmergencyTypeIdByType = function(emergency_type, fn) {
    console.log("emergency_type", emergency_type);
    var connection = connections.connection;
    if (emergency_type != null) {
        var res = (emergency_type.indexOf(",") ? emergency_type.split(',') : emergency_type);
        var emergency_type_id = "";
        if (typeof res != "string") {
            var i = 0;
            iterateCircle();

            function iterateCircle() {

                var query = "select id from emergency_types where type_name='" + res[i] + "';";
                connection.query(query, function(err, res1) {
                    if (!err) {
                        if (res1 != undefined && res1.length > 0) {

                            emergency_type_id += "" + res1[0].id;

                            if (res.length - 1 == i) {

                                emergency_type_id += "";
                                fn(emergency_type_id);
                            } else {
                                emergency_type_id += ",";
                                i++;
                                iterateCircle();
                            }
                        } else {
                            fn(null);
                        }

                    } else {
                        fn(null);
                    }
                })

            }

        } else {
            var emergency_type_id = "";

            var query = "select id from emergency_types where type_name='" + emergency_type + "';";

            connection.query(query, function(err, res1) {
                if (!err) {
                    if (res1 != undefined && res1.length > 0) {
                        emergency_type_id = res1;
                        fn(res1[0].id);
                    } else {
                        fn(null);
                    }

                } else {
                    fn(null);
                }


            })

        }
    } else {
        fn(null);
    }

}

