var connections = require("../Util/dbConnection.js"),
commonConstants = require("../Util/commonConstants.js"),
commonMethod = require("../Util/commonMethod.js");
var statusConstant = require("../Util/statusConstants.json");
var logsDao = require("../Dao/logsDao.js");
var logger = require("../logging/logger.js");
var generalDao = require("../Dao/generalDao.js");
var sendPNDao = require("../Dao/sendPNDao.js");
var loginDao = require("../Dao/loginDao.js");
var customerDao =require("../Dao/customerDao.js");
exports.getDriversListByAmbulanceId = function(req, fn) {
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
			tokReq['ambApp'] = req.body.ambulanceApp;
		}
		var query = "select * from ambulance_master where amb_id=" + parseInt(req.body.ambulanceId);
		commonMethod.verifyUser(tokReq, function(data, obj) {

			if (data == "success") {
				connection.query(query, function(err, res) {
					if (!err) {
						if (res != undefined) {
							if (res.length > 0) {
								var q = "select driver_id,driver_name,driver_mobile_number,driver_email,file_url from driver_master where group_id=" + parseInt(res[0].group_id) + " and is_active='Active'"
								connection.query(q, function(error, result) {
									if (!error) {
										fn("success", result);
									} else {
										console.log("ERROR ", error)
										fn("error", error);
									}
								})
							} else {
								fn("noAmbulance", res);
							}
						}
					} else {
						console.log("ERROR ", err)
						fn("error", err);
					}
				})
			} else if (data == "err") {
				fn("Unauthorized");
			} else if (data == "Invalid token" || data == 'No User') {
				fn(data, obj);
			}

		});
	} catch (e) {
		console.log("ERROR ", e)
		fn("exception", e);
	}
}

exports.updateAmbulanceStatus = function(req, fn) {
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
		tokReq['ambApp'] = req.body.ambulanceApp;
	}
	console.log("OPOPOPO ",req.body);
	commonMethod.verifyUser(tokReq, function(data, obj) {
		req.body.estimated_time_arrival = req.body.duration;
		if (data == "success") {
			generalDao.insertIntoTracking(req.body, function(stat, obj) {
				if (stat == "success") {
					if (req.body.booking_status == commonConstants.TripClosed) {
						var query = "UPDATE ambulance_driver SET is_assigned=false WHERE ambulance_id=" + req.body.ambulanceId + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.ambulanceId + ") AS p )";
						connection.query(query, function(err, res) {
							if (!err) { console.log("OOOOOOOOOOOO ",req.body.is_location_update);
							if (req.body.is_location_update == undefined) {
								var emerge = {
										booking_status: req.body.booking_status,
										booking_id: req.body.booking_id,
										cost: req.body.cost,
										duration: req.body.duration,
										distance: req.body.distance
								}
								updateEmergencyStatus(emerge, function(status) {
									if (status == "success") {
										fn("success", obj);
										/*send sms to customer*/
										var q = "select is_self,customer_id,customer_mobile,customer_name,patient_number from emergency_booking where booking_id="+req.body.booking_id;
										connection.query(q,function(error,result){
											if(!error){
												if(result!=undefined && result.length>0){
													var message = "eSahai! Your bill for "+req.body.booking_id;
													message += "is Rs."+req.body.cost+"(Incl of all taxes)."
													//		message += "Distance:"+req.body.distance+".Ride Time:"+req.body.duration+"." ;
													var param = {
															// mobilenumber:result[0].customer_mobile,
															message:message
													}; 
													logger.info("IS SELF ", result[0].is_self);
													if (result[0].is_self == true || result[0].is_self == "true" ) {
														param['mobilenumber'] = result[0].customer_mobile
														loginDao.sendSMS(param,function(stat){
															// fn("success", obj);
														})
													} else {
														param['mobilenumber']=(result[0].patient_number!=undefined && result[0].patient_number!=null && result[0].patient_number!="")?commonConstants.indiaCode+result[0].patient_number:result[0].customer_mobile;
														loginDao.sendSMS(param,function(stat){
															// fn("success", obj);
														})
													}
													//console.log("PARAMS ",param);
													/*loginDao.sendSMS(param,function(stat){
														// fn("success", obj);
													})*/
												}
											}
										});
									} else {
										fn(status);
									}
								});
							} else {
								fn("success", obj);
							}

							} else {
								fn("err");
							}
						});

					} else {
						if (req.body.is_location_update == undefined) {
							var emerge = {
									booking_status: req.body.booking_status,
									booking_id: req.body.booking_id,
									cost: req.body.cost,
									duration: req.body.duration,
									distance: req.body.distance
							}
							if(req.body.emergency_type_id!=undefined)
								emerge['emergency_type_id']=req.body.emergency_type_id;
							if(req.body.emergency_type!=undefined) 
								emerge['emergency_type']=req.body.emergency_type;
							if(req.body.hospital!=undefined) 
								emerge['hospital']=req.body.hospital;
							if(req.body.group_id!=undefined)
								emerge['group_id']=req.body.group_id;
							updateEmergencyStatus(emerge, function(status) {
								if (status == "success") {
									if(req.body.booking_status == commonConstants.WayToDestination){
										console.log("MATCHING LLL ");
										var patientDet = {
												headers:req.headers,
												body:{
													patient_name:req.body.patient_name,
													contact_number:req.body.contact_number,
													age:req.body.age,
													gender:req.body.gender,
													patient_condition:req.body.patient_condition,
													booking_id:req.body.booking_id,
													ambulanceApp:req.body.ambulanceApp
												}
										}
										customerDao.addPatientInfo(patientDet,function(patientStat,patientRes) { console.log("OPOPOP ",patientStat,patientRes);
										if(patientStat == "success"){
											sendSmsToHospital(req,function(status,smsResult){
												fn(status,smsResult);
											})
										}else{
											sendSmsToHospital(req,function(status,smsResult){
												fn(status,smsResult);
											})
										}
										})
										/*send sms to hospital saying that patient is coming*/
										function sendSmsToHospital(req,fn){
											getGroupByBookingId(req.body.booking_id,function(groupStat,groupBody){
												console.log("GRUP ST ",groupStat);
												if(groupStat == "success"){
													connection.query("select * from emergency_booking where booking_id="+req.body.booking_id,function(bookErr,bookObj){ console.log("OPOPO ",bookErr);
													if(!bookErr){
														if(bookObj!=undefined){
															if(bookObj.length>0){
																var param = {
																	//	mobilenumber:groupBody,
mobilenumber:commonConstants.indiaCode+groupBody,
																		message:"Patient with the following details is coming to your hospital.  Details:"+bookObj[0].customer_name+"("+bookObj[0].customer_mobile+") for "+bookObj[0].emergency_type
																}; console.log("PARAMS ",param);
																fn("success", obj);
																loginDao.sendSMS(param,function(stat){
																	//     if(stat=="success")
																	
																})
															}
														}
													}
													})

												}else
													fn("success", obj);
											});
										}

									}else
										fn("success", obj);
								} else {
									fn(status);
								}
							});
						} else {
							fn("success", obj);
						}
						// fn("success", obj);
					}
				} else {
					fn("err");
				}

			})
		} else if (data == "err") {
			fn("Unauthorized");
		} else if (data == "Invalid token" || data == 'No User') {
			fn(data, obj);
		}
	});
}
var updateEmergencyStatus = function(req, fn) {
	var connection = connections.connection;
//	var query = "update emergency_booking set booking_status ='" + req.booking_status + "',distance='" + req.distance + "',duration='" + req.duration + "',cost='" + req.cost + "' where booking_id=" + req.booking_id;
	/*var query = "update emergency_booking set booking_status ='" + req.booking_status + "',distance='" + req.distance + "',duration='" + req.duration + "',cost='" + req.cost + "'";
	if(req.emergency_type_id!=undefined)
		query += ",emergency_type_id="+req.emergency_type_id
	if(req.emergency_type!=undefined) 
		query += ",emergency_type='"+req.emergency_type+"'";
	if(req.hospital!=undefined) 
		query += ",hospital='"+req.hospital+"'";
	if(req.group_id!=undefined)
		query += ",group_id="+req.group_id

	query += " where booking_id=" + req.booking_id;
    connection.query(query, function(err, res) {
        if (!err) {
            fn("success");
        } else {
            fn("err");
        }
    });*/

	var query1 = "select booking_status from emergency_booking where booking_id="+req.booking_id;
	connection.query(query1,function(error,result){
		if(!error){
			if(result!=undefined && result.length>0){
				if(result[0].booking_status == commonConstants.Cancelled || result[0].booking_status == commonConstants.FakeCancel){
					fn("cancelled");
				}else{
					var query = "update emergency_booking set booking_status ='" + req.booking_status + "',distance='" + req.distance + "',duration='" + req.duration + "',cost='" + req.cost + "'";
					if(req.emergency_type_id!=undefined)
						query += ",emergency_type_id="+req.emergency_type_id
						if(req.emergency_type!=undefined)
							query += ",emergency_type='"+req.emergency_type+"'";
					if(req.hospital!=undefined)
						query += ",hospital='"+req.hospital+"'";
					if(req.group_id!=undefined)
						query += ",group_id="+req.group_id

						query += " where booking_id=" + req.booking_id;
					connection.query(query, function(err, res) {
						if (!err) {
							fn("success");
						} else {
							fn("err");
						}
					});
				}
			}
		}else{
			fn("err");
		}
	})
}

var getGroupByBookingId = function(bookingId,fn){
	var connection = connections.connection;
	var query = "select group_id from emergency_booking where booking_id = "+bookingId;
	connection.query(query, function(err, res) {
		if (!err) {
			if(res!=undefined){
				if(res.length>0){
					//fn("success",res[0].group_id);
					if(res[0].group_id!="Any"){
						var q = "select emergency_number from group_master where group_id="+res[0].group_id;
						connection.query(q,function(errEme,emeRes){
							//		console.log("KLKLKLK ",errEme);
							if(!errEme){
								if(emeRes!=undefined){
									if(emeRes.length>0){
										fn("success",emeRes[0].emergency_number);
									}else{
										fn("noBooking");
									}
								}
							}else {
								fn("err");
							}
						})
					}else{
						fn("AnyGroup");
					}
				}else{
					fn("noBooking");
				}
			}	            
		} else { console.log("ERRRR ",err);
		fn("err");
		}
	});
}
exports.updateDeviceToken = function(req, fn) {
	var connection = connections.connection;
	var query = "";


	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.customerApp != undefined && (req.body.customerApp == true || req.body.customerApp == "true")) {
		query += " update customer_master set device_token='" + req.body.deviceToken+ 
		"' where customer_id = '" + req.headers.userid + "'"
		tokReq['customerApp'] = req.body.customerApp;
	} else if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
		query += " update ambulance_master set device_token='" + req.body.deviceToken +
		"' where amb_id = '" + req.headers.userid + "'"
		tokReq['ambApp'] = req.body.ambulanceApp;
	}
	commonMethod.verifyUser(tokReq, function(data, obj) {
		if (data == "success") {
			connection.query(query, function(err, res) {
				if (!err) {
					fn("success");
				} else {
					fn("err");
				}
			});
		} else if (data == "err") {
			fn("Unauthorized");
		} else if (data == "Invalid token" || data == 'No User') {
			fn(data, obj);
		}
	});
}


exports.getAmbulanceTypeList = function(req, fn) {
	var connection = connections.connection;
	var query = "select * from emergency_types";
	connection.query(query, function(err, res) {
		if (!err) {
			fn("success", res);
		} else {
			fn("err", err);
		}
	})
}
exports.getEmergencyTypeList = function(req, fn) {
	var connection = connections.connection;
	var query = "select id,type_name,created_date,updated_date from emergency_types where status='Active'";
	connection.query(query, function(err, res) {
		if (!err) {
			fn("success", res);
		} else {
			fn("err", err);
		}
	})
}

exports.getAmbulanceDriverStatusList = function(req, fn) {
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		commonMethod.verifyUser(tokReq, function(data, obj) {

			if (data == "success") {
				var q = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver group by ambulance_id)";
				if(req.body.ambulance_id!=undefined && req.body.driver_id!=undefined){
					//                    q = "SELECT * FROM ambulance_driver WHERE ambulance_id="+req.body.ambulance_id+" and created_date in (SELECT MAX(created_date) FROM ambulance_driver where ambulance_id="+req.body.ambulance_id+")";
					q = "SELECT * FROM ambulance_driver WHERE ambulance_id="+req.body.ambulance_id+" and driver_id = "+req.body.driver_id+" and created_date in (SELECT MAX(created_date) FROM ambulance_driver where ambulance_id="+req.body.ambulance_id+")";


				}
				var resArray=[];
				connection.query(q, function(err, res) {
					if (!err) {
						if (res != undefined) {
							if (res.length > 0) {
								var i = 0;
								iterateAmbulanceDrivers();

								function iterateAmbulanceDrivers() {
									if (i < res.length) {
										var query = "select * from ambulance_master where amb_id=" + res[i].ambulance_id;
										if(req.body.groupId!=undefined)
											query+=" and group_id="+req.body.groupId;
										console.log("QUERY ",query);
										connection.query(query, function(error, result) {
											if (!error) {
												if (result != undefined && result.length>0) {

													var qry = "select type_name from vehicle_type where vehicle_id=" + result[0].vehicle_type;
													connection.query(qry, function(error2, result2) {
														if (!error2) {
															result[0]['vehicleTypeName'] = result2[0].type_name;
															res[i]['ambulanceDetails'] = result[0];

															var query1 = "select * from driver_master where driver_id=" + res[i].driver_id;
															connection.query(query1, function(err1, res1) {
																if (!err1) {
																	if (res1 != undefined) {
																		res[i]['driverDetails'] = res1[0];
																		var query2 = "SELECT * FROM emergency_booking WHERE created_datetime in (SELECT MAX(created_datetime) FROM emergency_booking where ambulance_id = " + res[i].ambulance_id + ") limit 1";
																		if(req.body.booking_id!=undefined){
																			query2 = "SELECT * FROM emergency_booking WHERE booking_id="+req.body.booking_id;
																		}
																		connection.query(query2, function(err4, res4) {
																			if (!err4) {
																				if (res4 != undefined) {
																					/** GROUP **/
																					var query3 = "select group_name from group_master where group_id="+result[0].group_id; 
																					connection.query(query3,function(error5,result5){
																						if(!error5){
																							if(result5!=undefined){
																								if(result.length>0){
																									res[i]['ambulanceDetails']['group']=result5[0].group_name;
																									if(req.body.ambulance_id!=undefined)
																										res[i]['bookingDetails'] = res4[0];

																									if (res4.length == 0) {
																										res[i]['is_booked'] = 'No';
																										res[i]['booking_status'] = 'Not Booked Yet';
																										res[i]['created_datetime'] = '00';
																										res[i]['booking_id'] = 0;
																										resArray.push(res[i]);
																										if (i == res.length - 1) {
																											fn("success", resArray)
																										}
																										i++;
																										iterateAmbulanceDrivers();
																									} else {
																										if (res[i]['is_assigned']==false || (res4[0]['booking_status'] == commonConstants.Pending || res4[0]['booking_status'] == commonConstants.Cancelled || res4[0]['booking_status'] == commonConstants.FakeCancel || res4[0]['booking_status'] ==commonConstants.TripClosed )) {
																											res[i]['is_booked'] = 'No';
																											res[i]['booking_status'] = 'Not Booked Yet';
																											res[i]['created_datetime'] = '00';
																											res[i]['booking_id'] = 0;
																											resArray.push(res[i]);
																											//                                                                                                           res[i]['ambulanceDetails']['group']=res4[0].hospital;
																										} else {
																											res[i]['is_booked'] = 'Yes';
																											res[i]['booking_status'] = res4[0].booking_status;
																											res[i]['created_datetime'] = res4[0].created_datetime;
																											res[i]['booking_id'] = res4[0].booking_id;
																											resArray.push(res[i]);
																											//                                                                                                         res[i]['ambulanceDetails']['group']=res4[0].hospital;
																										}
																										if (i == res.length - 1) {
																											fn("success", resArray)
																										}
																										i++;
																										iterateAmbulanceDrivers();
																									}
																								}
																							}
																						}else{
																							if(req.body.ambulance_id!=undefined)
																								res[i]['bookingDetails'] = res4[0];

																							if (res4.length == 0) {
																								res[i]['is_booked'] = 'No';
																								res[i]['booking_status'] = 'Not Booked Yet';
																								res[i]['created_datetime'] = '00';
																								res[i]['booking_id'] = 0;
																								resArray.push(res[i]);
																								if (i == res.length - 1) {
																									fn("success", resArray)
																								}
																								i++;
																								iterateAmbulanceDrivers();
																							} else {
																								if (res4[0]['booking_status'] == commonConstants.Pending || res4[0]['booking_status'] == commonConstants.Cancelled || res4[0]['booking_status'] == commonConstants.TripClosed) {
																									res[i]['is_booked'] = 'No';
																									res[i]['booking_status'] = 'Not Booked Yet';
																									res[i]['created_datetime'] = '00';
																									res[i]['booking_id'] = res4[0].booking_id;
																									resArray.push(res[i]);
																									//                                                                                               res[i]['ambulanceDetails']['group']=res4[0].hospital;
																								} else {
																									res[i]['is_booked'] = 'Yes';
																									res[i]['booking_status'] = res4[0].booking_status;
																									res[i]['created_datetime'] = res4[0].created_datetime;
																									res[i]['booking_id'] = res4[0].booking_id;
																									resArray.push(res[i]);
																									//                                                                                             res[i]['ambulanceDetails']['group']=res4[0].hospital;
																								}
																								if (i == res.length - 1) {
																									fn("success", resArray)
																								}
																								i++;
																								iterateAmbulanceDrivers();
																							}
																						}
																					});
																					/* if(req.body.ambulance_id!=undefined)
                                                                                                 res[i]['bookingDetails'] = res4[0];
                                                                                            if (res4.length == 0) {
                                                                                                res[i]['is_booked'] = 'No';
                                                                                                res[i]['booking_status'] = 'Not Booked Yet';
                                                                                                res[i]['created_datetime'] = '00';
                                                                                                res[i]['booking_id'] = 0;
                                                                                                if (i == res.length - 1) {
                                                                                                    fn("success", res)
                                                                                                }
                                                                                                i++;
                                                                                                iterateAmbulanceDrivers();
                                                                                            } else {
                                                                                            	if (res4[0]['booking_status'] == "Pending" || res4[0]['booking_status'] == "Cancelled" || res4[0]['booking_status'] == "Trip Closed") {
                                                                                                    res[i]['is_booked'] = 'No';
                                                                                                    res[i]['booking_status'] = 'Not Booked Yet';
                                                                                                    res[i]['created_datetime'] = '00';
                                                                                                    res[i]['booking_id'] = res4[0].booking_id;    												     res[i]['ambulanceDetails']['group']=res4[0].hospital;
                                                                                                } else {
                                                                                                    res[i]['is_booked'] = 'Yes';
                                                                                                    res[i]['booking_status'] = res4[0].booking_status;
                                                                                                    res[i]['created_datetime'] = res4[0].created_datetime;
                                                                                                    res[i]['booking_id'] = res4[0].booking_id;
												     res[i]['ambulanceDetails']['group']=res4[0].hospital;
                                                                                                }
                                                                                                if (i == res.length - 1) {
                                                                                                    fn("success", res)
                                                                                                }
                                                                                                i++;
                                                                                                iterateAmbulanceDrivers();
                                                                                            }*/
																				}

																			} else {
																				if (i == res.length - 1) {
																					fn("success", resArray)
																				}
																				i++;
																				iterateAmbulanceDrivers();
																			}
																		})

																	}
																} else {
																	if (i == res.length - 1) {
																		fn("success", resArray)
																	}
																	i++;
																	iterateAmbulanceDrivers();
																}
															})
														} else {
															i++;
															iterateAmbulanceDrivers();
														}
													})

												}else{
													if (i == res.length - 1) {
														fn("success", resArray)
													}
													i++;
													iterateAmbulanceDrivers();
												}
											} else {
												if (i == res.length - 1) {
													fn("success", resArray)
												}
												i++;
												iterateAmbulanceDrivers();
											}
										})
									} else if (i == res.length - 1) {
										fn("success", resArray)
									}
								}
							}
						}
					}
				})
			} else if (data == "err") {
				fn("Unauthorized");
			} else if (data == "Invalid token" || data == 'No User') {
				fn(data, obj);
			}
		});
	} catch (e) {
		fn("err", e);
	}
};

exports.trackAmbulance = function(req, fn) {
	var connection = connections.connection;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
		tokReq['ambApp'] = req.body.ambulanceApp;
	}
	try {
		var driver = req.body;
		console.log("BODY ", driver);
		//var query = "insert into ambulance_driver(ambulance_id,driver_id,longitude,latitude,geoHashCode,status) values(" + driver.ambulance_id + "," + driver.driver_id + ",'" + driver.longitude + "','" + driver.latitude + "','" + driver.geoHashCode + "','" + driver.status + "')";
		var query = "update ambulance_driver set longitude='"+driver.longitude+"',latitude='"+driver.latitude+"',geoHashCode='"+driver.geoHashCode+"' where ambulance_id="+driver.ambulance_id+" and driver_id ="+driver.driver_id;
		console.log("QEURY ", query);
		commonMethod.verifyUser(tokReq, function(data, obj) {
			data = "success";
			if (data == "success") {
				connection.query(query, function(err, res) {
					if (!err) {
						if (res != undefined) {
							fn("success");
						}
					} else {
						console.log("IOIOIOI ", err)
						fn("err", err);
					}
				});
			} else if (data == "err") {
				fn("Unauthorized");
			} else if (data == "Invalid token" || data == 'No User') {
				fn(data, obj);
			}
		});
	} catch (e) {
		fn("err", e);
	}
}

exports.getAmbulanceTrackDetails = function(req, fn) {
	var connection = connections.connection;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
		tokReq['ambApp'] = req.body.ambulanceApp;
	}
	var query = "SELECT * from ambulance_driver where ambulance_id=" + req.body.ambulance_id + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.ambulance_id + ") AS p )";
	console.log("QUERY ",query);
	try {
		commonMethod.verifyUser(tokReq, function(data, obj) {
			data = "success";
			if (data == "success") {
				connection.query(query, function(err, res) {
					if (!err) {
						if (res != undefined) {
							if (res.length == 0) {
								fn("noData", res)
							} else {
								fn("success", res);
							}
						}
					} else {
						fn("err", err);
					}
				});
			} else if (data == "err") {
				fn("Unauthorized");
			} else if (data == "Invalid token" || data == 'No User') {
				fn(data, obj);
			}
		});

	} catch (e) {
		fn("err", e);
	}

}
exports.updateAmbulanceDriverStatus = function(req, fn) {
	var connection = connections.connection;
	try {
		var tokReq = {
				token: req.headers.token,
				userId: req.headers.userid,
				method: req.method
		}
		var query = "update ambulance_driver set status = '" + req.body.status + "'";
		query += " WHERE ambulance_id=" + req.body.ambulanceId + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.ambulanceId + ") AS p )";

		var querySel = "SELECT * from ambulance_driver where ambulance_id=" + req.body.ambulanceId + " and created_date IN (SELECT * FROM (SELECT Max(created_date) FROM ambulance_driver where ambulance_id=" + req.body.ambulanceId + ") AS p )"
		commonMethod.verifyUser(tokReq, function(data, obj) {
			data = "success";
			if (data == "success") {
				connection.query(querySel, function(error, result) {
					if (!error) {
						if (result != undefined) {
							if (result.length > 0) {
								if (result[0].is_assigned == true) {
									fn("onTrip", result);
								} else {
									connection.query(query, function(err, res) {
										if (!err) {
											if (res != undefined) {

												//send pn to ambulance to call logout api of the driver
												var pnData = "logout the driver";
												var message = "logout the driver";
												var type = "driver_logout";
												var deviceToken = [];
												var obj1 = {
														tableName: "ambulance_master",
														ambulance_id: req.body.ambulanceId
												}
												getDeviceTokenById(obj1, function(status1, data1) {
													if (status1 == "success") {
														fn("success", res)
														
														 var  deviceToken = [data1.device_token];
                                                        console.log("OPOPOP CANCEL DEVICE TOKEN ", deviceToken);
                                                        var sendObj = {
                                                                        user_type:'Ambulance',
                                                                        send_to:data1.ambulance_mobile
                                                        }
                                                        sendPNDao.sendPN(deviceToken, pnData, message, type,sendObj);

													} else {
														fn("errPN", data);
													}
												})
											}
										} else {
											fn("err", err);
										}
									});
								}
							} else {
								fn("noData", result);
							}
						}
					} else {
						fn("err", err);
					}
				})

			} else if (data == "err") {
				fn("Unauthorized");
			} else if (data == "Invalid token" || data == 'No User') {
				fn(data, obj);
			}
		});
	} catch (e) {
		fn("err", e);
	}
}
var getDeviceTokenById = function(data, fn) {
	var connection = connections.connection;
	var query = "select * from " + data.tableName;
	if (data.tableName == "customer_master") {
		query += " where customer_id=" + data.customer_id;
	} else if (data.tableName == "ambulance_master") {
		query += " where amb_id=" + data.ambulance_id;
	}
	console.log("QUERY in DEVICE TOEN ", query);
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				fn("success", res[0]);
			}
		} else {
			fn(null);
		}
	});
}

exports.getAmbulanceTypes = function(req, fn) {
	var connection = connections.connection;
	var query = "select * from  group_types order by type_name";

			connection.query(query, function(err, res) {
				if (!err) {
					if (res != undefined) {
						if (res.length > 0) {
							fn("success", res);
						} else
							fn("noAmb", res);
					}
				} else {
					fn("err", err);
				}
			})
}
exports.getAmbulanceProfile = function(req,fn){
	var connection = connections.connection;
	var tokReq = {
			token: req.headers.token,
			userId: req.headers.userid,
			method: req.method
	}
	if (req.body.ambulanceApp != undefined && (req.body.ambulanceApp == true || req.body.ambulanceApp == "true")) {
		tokReq['ambApp'] = req.body.ambulanceApp;
	}
	var query = "select a.*,b.type_name,c.group_name,c.emergency_number from ambulance_master a , vehicle_type b,group_master c where a.amb_id="+req.headers.userid+" and b.vehicle_id=a.vehicle_type and c.group_id=a.group_id";
	logger.info("QUERY ",query);
	try {
		commonMethod.verifyUser(tokReq, function(data, obj) {
			if (data == "success") {
				connection.query(query, function(err, res) {
					logger.info("ERRROR ", err);
					if (!err) {
						if(res.length==0){
							fn("no ambulance found");
						}else{
							res[0]['vehicle_type'] = res[0]['type_name'];
							res[0]['group_mobile_number']=res[0]['emergency_number'];
							loginDao.getEmergencyTypeNameById(res[0].emergency_type, function(status,typeObj) {
								if (status != null) {
									res[0].emergency_type = status;
									res[0].emergency_types =typeObj;
									var qry = "select attribute_name,attribute_details from ambulance_attributes where ambulance_id="+req.headers.userid;
									connection.query(qry,function(error,result){
										if(!err){
											if(res.length>0){
												res[0]['ambulance_attributes'] = result;
												fn("success", res[0]);
											}else if(res.length==0){
												fn("success", res[0]);
											}
										}else{
											fn("err", err);
										}
									})
								}
							});
							
						}
					} else {
						fn("err", err);
					}
				});
			} else if (data == "err") {
				fn("Unauthorized");
			} else if (data == "Invalid token" || data == 'No User') {
				fn(data, obj);
			}
		});

	} catch (e) {
		fn("err", e);
	}
}


