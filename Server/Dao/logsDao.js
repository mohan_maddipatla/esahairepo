//logs the activity/errors
var connections = require("../Util/dbConnection.js");
var dateFormat = require('dateformat');

exports.insertAppUserRoles = function(req){
	try{
		var connection = connections.connection;
		var user_id=null;
		var query = "insert into user_app_roles(user_id, app_id, role_id)"
		+"values('"+req.userId+"','"+req.appId+"','"+req.roleId+"')";
		connection.query(query,function(err,suceess){
			if(err){
				console.log("ERROR ",err);
			}else{
				console.log("SUCCESS ",suceess);
			}
		});

	}catch(err){
		console.log(err);
	}
}
exports.deleteAppUserRolesByUserId = function(userId){
	var connection = connections.connection;
	var q = "delete from user_app_roles where user_id = '"+userId+"'";
	connection.query("delete from user_app_roles where user_id = '"+userId+"'",function(err,data){
		if(err){
			console.log("ERROR ",err);
		}else{
			console.log("SUCCESS ",data);
		}							
	})
}
exports.deleteAppUserRolesByAppId = function(appId){
	var connection = connections.connection;
	connection.query("delete from user_app_roles where app_id = '"+appId+"'",function(err,data){
		if(err){
			console.log("ERROR ",err);
		}else{
			console.log("SUCCESS ",suceess);
		}							
	})
}

exports.insertAccessRoles = function(req){
	try{
		var connection = connections.connection;
		var query = "insert into access_roles(service, component, access,requestor,roleId,filters)"
		+"values('"+req.service+"','"+req.component+"','"+req.access+"','"+req.requestor+"','"+req.roleId+"','"+req.filters+"')";
		connection.query(query,function(err,suceess){
			if(err){
				console.log("ERROR ",err);
			}else{
				console.log("SUCCESS ",suceess);
			}
		});

	}catch(err){
		console.log(err);
	}
}
exports.deleteAccessRolesByRoleId = function(roleId){
	var connection = connections.connection;
	var q = "delete from access_roles where roleId = '"+roleId+"'";
	connection.query(q,function(err,data){
		if(err){
			console.log("ERROR ",err);
		}else{
			console.log("SUCCESS ",data);
		}							
	})
}

exports.insertServiceTable= function(req){
	try{
		var connection = connections.connection;
		var query = "insert into serviceTables(serviceId, tableName)"
		+"values('"+req.serviceId+"','"+req.tableName+"')";
		connection.query(query,function(err,suceess){
			if(err){
				console.log("ERROR ",err);
			}else{
				console.log("SUCCESS ",suceess);
			}
		});

	}catch(err){
		console.log(err);
	}
}


exports.insertAPILog = function(req,fn) {
    try {
        var respJson = {};
        var connection = connections.connection;
        var query = "insert into api_log(api_path,request_time,response_time,result_code,user_id)" + "values('" + req.apiPath + "','" + req.reqTime + "','" + req.resTime + "','" + req.resultCode + "','" + req.userId + "')";
        connection.query(query, function(err, suceess) {
            if (err) {
              //   console.log("insertAPILog ---> ERROR  ", err);
            } else {
                console.log("insertAPILog ---> SUCCESS ", suceess);
            }
        });
    } catch (err) {
        console.log(err);
    }
}

exports.appMetaData = function(fn){
	try{
		var connection = connections.connection;
		var query = "select * from app_meta_data";
		connection.query(query,function(err,res){
			if(!err){
				if(res!=undefined && res.length>0){
					if(res[0].unique_login == "true"){
						fn("unique",1);
					}else if(res[0].unique_login == "false"){
						fn("nonUnique",res[0].max_logins);
					}
				}
			}else{
				fn("err",0);
			}
		});
	}catch(err){
		console.log(err);
	}
}

exports.addAmbulanceListPerBooking = function(req){
	var connection = connections.connection;
	var query  =  "insert into booking_ambulance_log(booking_id,ambulance_ids,ambulance_numbers) values("+req.booking_id+",'"+req.ambulance_ids+"','"+req.ambulance_numbers+"')";
console.log("QERUERIU ",query);
	connection.query(query,function(err,res){
		if(!err){
			//logger.info("Ambulance List Logged ");
			console.log("Ambulance List Logged ")
		}else{
			//logger.info("Failed to log the ambulance list");
			console.log("Failed to log the ambulance list",err)
		}
	})
	
}
