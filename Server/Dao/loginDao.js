var connections = require("../Util/dbConnection.js"),
appUserDao = require("../Dao/appUserDao.js"),
jwt = require("jsonwebtoken"),
encryption = require("../Util/encryption.js");
commonMethod = require("../Util/commonMethod.js");
commonConstants = require("../Util/commonConstants.js");
var statusConstants = require("../Util/statusConstants.json");
var request = require('request');
var logger = require("../logging/logger.js");
var datetime = require('node-datetime');
var encryptionMob = require("../Util/encryptionMob.js");
var generalDao = require("../Dao/generalDao.js");
var sendPNDao = require("../Dao/sendPNDao.js");
var loginDao = require("../Dao/loginDao.js");
exports.login = function(req, fn) {
	connection = connections.connection;
	var respJson = {};
	try {
		request = req;

		if (request.email != undefined && request.email != "" && request.password != undefined && request.password != "") {
			var q = "select * from system_users where email = '" + request.email + "'";


			//  function userLogin() {
			console.log("OOO PPP ", q);
			connection.query(q, function(err, res) {
				console.log("OOO ", err);
				console.log("P ", res)
				if (err) {
					respJson['statusCode'] = "500";
					respJson['statusMessage'] = err;
					fn(JSON.stringify(respJson));
				} else {

					if (res.length > 0 && res[0] != undefined) {
						if(res[0].isActive !='Active'){
							respJson['statusCode'] = "201";
                                        respJson['statusMessage'] = "You are not authorized to login, Please contact administrator for further information";
                                        fn(JSON.stringify(respJson));

						}else{
							if(res[0].password!=request.password){
								respJson['statusCode'] = "201";
                                respJson['statusMessage'] = "Invalid Password";
                                fn(JSON.stringify(respJson));
							}else{
						var token = jwt.sign({ userId: res[0].userId }, request.password);
						request['token'] = token;
						request['userId'] = res[0].userId;
						request['displayName'] = res[0].displayName;
						var tokenResp = null;
						var query = "update system_users set token = '" + token + "' where userId ='" + res[0].userId + "'";
						console.log("qwere ", query);
						connection.query(query, function(err, data) {
							if (!err) {

								var obj = {
										userId: res[0].userId,
										userType: res[0].userType
								}
								tokenResp = token;
								commonMethod.appRoleCheckForUser(obj, function(status, result) {
									console.log("NO ROLE  ", status);
									if (status == "No Roles Assigned") {
										getUserTypeByTypeId(res[0].userType, function(status, type) {
											if (status == "success") {
												respJson['statusCode'] = "302";
												respJson['statusMessage'] = "Sorry, it appears you have no active applications. Please contact your system administrator.";
												respJson['email'] = res[0].email;
												respJson['displayName'] = res[0].displayName;
												respJson['userId'] = res[0].userId;
												respJson['userTypeId'] = res[0].userType;
												respJson['userType'] = type;
												respJson['isActive'] = res[0].isActive;
												respJson['token'] = tokenResp;
												fn(JSON.stringify(respJson));
											}
										})
									} else {
										if (result != null && result.length > 0) {
											respJson['appsList'] = result;
										}
										getUserTypeByTypeId(res[0].userType, function(status, type) {
											if (status == "success") {
												respJson['statusCode'] = "300";
												respJson['email'] = res[0].email;
												respJson['displayName'] = res[0].displayName;
												respJson['userId'] = res[0].userId;
												respJson['isActive'] = res[0].isActive;
												respJson['userTypeId'] = res[0].userType;
												respJson['userType'] = type;
												respJson['groupId'] = res[0].group_id;
												respJson['token'] = tokenResp;
												fn(JSON.stringify(respJson));
											}
										})

									}
								});

							}
						});
						}
						}
					} else {
						respJson['statusCode'] = "201";
						respJson['statusMessage'] = "No user found";
						fn(JSON.stringify(respJson));
					}

				}
			})
			// }

			/*if (request.groupAdmin != undefined && groupAdmin) {
                connection.query("select * from group_master where group_id = " + request.groupAdmin, function(error, res1) {
                    if (!error) {
                        if (res1.length > 0) {
                            userLogin();
                        }
                    }
                })
            } else {
                console.log("OOOOOOOOOOOOOOOo")*/
			//userLogin();
			//}

		} else {
			respJson['statusCode'] = '400';
			respJson['statusMessage'] = "Invalid request";
			fn(JSON.stringify(respJson));
		}
	} catch (err) {
		console.log("error ", err);
		respJson['statusCode'] = '500';
		respJson['statusMessage'] = "Error occured";
		fn(JSON.stringify(respJson));
	}
};

exports.appUserLogin = function(req, fn) {
	console.log("in appu SEr long;");
	connection = connections.connection;
	var respJson = {};
	console.log("in appu SEr long;");
	try {

		if (((req.email != undefined && req.email != "") || (req.mobile != undefined && req.mobile != null) || (req.userId != undefined && req.userId != null)) && req.password != undefined && req.password != "") {

			var q = "select * from app_user where ";
			if (req.email != null && req.email != "") {
				q += "email = '" + req.email + "'";
			} else if (req.mobile != null && req.mobile != "") {
				q += "mobile = '" + req.mobile + "'";
			} else {
				q += "generated_UserID = '" + req.userId + "'";
			}

			q += " and password = '" + req.password + "'";
			console.log("LOGIN query ", q);
			connection.query(q, function(err, res) {
				if (err) {
					respJson['statusCode'] = "500";
					respJson['statusMessage'] = err;
					fn(JSON.stringify(respJson));
				} else {
					if (res.length > 0) {
						var token = jwt.sign({ userId: res[0].generated_UserID }, req.password);
						req['token'] = token;
						req['userId'] = res[0].generated_UserID;
						var tokenResp = null;
						var query = "update app_user set token = '" + token + "',loginStatus= 'true' where generated_UserID ='" + res[0].generated_UserID + "'";
						connection.query(query, function(err, data) {
							if (!err) {
								tokenResp = token;

								respJson['statusCode'] = "200";
								respJson['email'] = res[0].email;
								respJson['generatedUserId'] = res[0].generated_UserID;
								respJson['mobile'] = res[0].mobile;
								respJson['userStatus:'] = res[0].userStatus;
								respJson['token'] = tokenResp;
								respJson['appId'] = res[0].appId;
								fn(JSON.stringify(respJson));
							}
						});

					} else {
						respJson['statusCode'] = "201";
						respJson['statusMessage'] = "No user found";
						fn(JSON.stringify(respJson));
					}
				}
			})
		} else {
			respJson['statusCode'] = '400';
			respJson['statusMessage'] = "Invalid request";
			fn(JSON.stringify(respJson));
		}


	} catch (err) {
		console.log("error ", err);
		respJson['statusCode'] = '500';
		respJson['statusMessage'] = "Error occured";
		fn(JSON.stringify(respJson));
	}
};

exports.getDashboardDetails = function(req, fn) {
	connection = connections.connection;
	var respJson = {};
	var newUsersLength = 0;
	var activeUsersLength = 0;
	var androidUsers = 0;
	var iosUsers = 0;
	try {
		if ((req.token != undefined && req.token != "") && (req.userId != undefined && req.userId != null)) {
		   if(req.groupId!=undefined && req.userType=="GROUP ADMIN"){
					connection.query("select count(*) from ambulance_master where group_id="+req.groupId, function(err1, res1) {
						respJson[statusConstants.STATUS_CODE] = "300";
						respJson['Result'] = [];
						var params = {};
						params['ambulanceCount'] = res1[0]['count(*)'];
						connection.query("select count(*) from driver_master where group_id="+req.groupId, function(err2, res2) {
							params['driversCount'] = res2[0]['count(*)'];
							connection.query("select count(*) from emergency_booking where group_id="+req.groupId,function(err3,res3){
								params['noOfJobs'] = res3[0]['count(*)'];
								connection.query("select count(*) from emergency_booking where booking_status = '"+commonConstants.TripClosed+"' and group_id="+req.groupId,function(err4,res4){
									params['completed'] = res4[0]['count(*)'];
									connection.query("select count(*) from emergency_booking where booking_status not in ('"+commonConstants.TripClosed+"', '"+commonConstants.Cancelled+"' ,'"+commonConstants.FakeCancel+"') and group_id="+req.groupId,function(err5,res5){
										params['inprogress'] = res5[0]['count(*)'];
										connection.query("select count(*) from emergency_booking where booking_status in ('"+commonConstants.Cancelled+"' ,'"+commonConstants.FakeCancel+"') and group_id="+req.groupId,function(err6,res6){
											params['cancelled'] = res6[0]['count(*)'];
											respJson['Result'].push(params);
											fn(JSON.stringify(respJson));
										});
									})
								})
							})
						})
					})
			
			}else{
			connection.query("select count(*) from group_master", function(err, res) {
				respJson[statusConstants.STATUS_CODE] = "300";
				respJson['Result'] = [];
				var params = {};
				console.log("RES IN DASHBOARD ", res[0]['count(*)']);
				params['groupCount'] = res[0]['count(*)'];
				connection.query("select count(*) from ambulance_master", function(err1, res1) {
					params['ambulanceCount'] = res1[0]['count(*)'];
					connection.query("select count(*) from customer_master", function(err2, res2) {
						params['customers'] = res2[0]['count(*)'];
						connection.query("select count(*) from emergency_booking",function(err3,res3){
							params['noOfJobs'] = res3[0]['count(*)'];
							connection.query("select count(*) from emergency_booking where booking_status = '"+commonConstants.TripClosed+"'",function(err4,res4){
								params['completed'] = res4[0]['count(*)'];
								connection.query("select count(*) from emergency_booking where booking_status not in ('"+commonConstants.TripClosed+"', '"+commonConstants.Cancelled+"' ,'"+commonConstants.FakeCancel+"')",function(err5,res5){
									params['inprogress'] = res5[0]['count(*)'];
									connection.query("select count(*) from emergency_booking where booking_status in ('"+commonConstants.Cancelled+"' ,'"+commonConstants.FakeCancel+"')",function(err6,res6){
										params['cancelled'] = res6[0]['count(*)'];
										respJson['Result'].push(params);
										fn(JSON.stringify(respJson));
									});
								})
							})
						})

					})
				})
			});
		  }

		} else {
			respJson['statusCode'] = '400';
			respJson['statusMessage'] = "Invalid request";
			fn(JSON.stringify(respJson));
		}
	} catch (err) {
		console.log("error ", err);
		respJson['statusCode'] = '500';
		respJson['statusMessage'] = "Error occured";
		fn(JSON.stringify(respJson));
	}
}


/** generate otp for ambulance app **/
exports.generateOTPAmbulance = function(req, fn) {
	var connection = connections.connection;
	var response = {}
	try {
		console.log("PPP ", req);
		req.ambulanceMobile = encryptionMob.decrypt(req.ambulanceMobile);
		var query = "select * from ambulance_master where ambulance_mobile='" + req.ambulanceMobile + "'";
		console.log("QUERIEU ", query)
		connection.query(query, function(err, res) {
			if (!err) {
				if (res != undefined) {
					if (res.length == 0) {
						response.statusCode = "302";
						response.statusMessage = "no ambulance found with that number";
						fn(JSON.stringify(response));
					} else {
						if (res[0].is_active == "Active") {
							if(res[0].login_status=="LoggedIn" && res[0].device_imei != req.deviceIMEI){
								response.statusCode = "304";
								response.statusMessage = "This ambulance is already logged in other device";
								fn(JSON.stringify(response));
							}else{
							var otp = commonMethod.generateOTP();
							var q = "update ambulance_master set otp = '" + otp + "', device_imei = '" + req.deviceIMEI + "' where ambulance_mobile='" + req.ambulanceMobile + "'";
							console.log("qeyre ", q)
							connection.query(q, function(error, result) {
								if (!error) {
									if (result != undefined) {
									var sendObj = {
                                            mobilenumber:commonConstants.indiaCode+req.ambulanceMobile,
                                            message:otp+" is your one time password"
                                        }
										loginDao.sendSMS(sendObj, function(status) {
											console.log("OPOPO  sms",status);
											if (status == "success") {
												connection.query("update ambulance_master set is_verified='Pending',sms_status='Sent' where ambulance_mobile='" + req.ambulanceMobile + "'", function(err1, res1) {
													if (!err1) {
														response.statusCode = "300";
														response.statusMessage = "Your otp has been sent to registered mobile number";
														response.otp = otp;
														response.ambulanceMobile = req.ambulance_mobile;
														fn(JSON.stringify(response));
													} else {
														response.statusCode = "500";
														response.statusMessage = "Error occured while generating otp";
														fn(JSON.stringify(response));
													}
												})

											} else {
												response.statusCode = "333";
												response.statusMessage = "OTP generated, failed to send sms. Please contact administration";
//response.otp = otp;
												fn(JSON.stringify(response));
											}
										})
										/*	 } else {
                            response.statusCode = "511";
                            response.statusMessage = "Not Active Ambulance, Please contact administrator";
                            fn(JSON.stringify(response));
                        }*/

									}
								} else {
									console.log("UPDIDO ", error)
									response.statusCode = "500";
									response.statusMessage = "Error occured while generating otp";
									fn(JSON.stringify(response));
								}
							})
							}
						} else {
							response.statusCode = "511";
							response.statusMessage = "Not Active Ambulance, Please contact administrator";
							fn(JSON.stringify(response));
						}

					}
				}
			} else {
				console.log("ERROR ONNN   ", err)
				response.statusCode = "500";
				response.statusMessage = "Error occured while generating otp";
				fn(JSON.stringify(response));
			}
		})
	} catch (e) {
		console.log("CATCH  ", e)
		response.statusCode = "500";
		response.statusMessage = "Error occured while generating otp";
		fn(JSON.stringify(response));
	}
}

/*var sendSMS = function(param, fn) {
	var params = param;
	//console.log("PARAMA ",params);
        var respJson = {};
	fn("success");
	var request = require("request");
	getSMSDetails(param,function(stat,path){
		console.log("PATH ",path);
		if(path!=null){
			var smsObj = {
                    mobile:params.mobilenumber,
                    message:params.message
                 }
            generalDao.insertSMSLOG(smsObj);

			  request({
		            url: commonConstants.sendSmsPath+path,
		            method: "GET",
		            headers: { 'content-type': 'application/json'},
		            qs: params
		        }, function(error, response, data) {
		            if (error) {
				console.log("ERRR IN SMS ",error);
		               // fn("success");
		            } else {
		             //   console.log("OPOPOP ",response, data);
		                if(data.indexOf("OK:")>-1){
		                   // fn("success");
		                }else{
		                	//fn("success");
		                }
		            }
		        })
		}else{
			console.log("ERRROER ",stat);
		//	fn("success");
		}
	})
      
}*/

exports.sendSMS = function(param, fn) {
        var params = param;
        var respJson = {};
	fn("success");
        var request = require("request");
	   	getSMSDetails(param,function(stat,path){
    		console.log("PATH ",path);
    		if(path!=null){
    			var smsObj = {
                        mobile:params.mobilenumber,
                        message:params.message
                     }
                generalDao.insertSMSLOG(smsObj);

    			  request({
    		            url: commonConstants.sendSmsPath+path,
    		            method: "GET",
    		            headers: { 'content-type': 'application/json'},
    		            qs: params
    		        }, function(error, response, data) {
    		            if (error) {
    				console.log("ERRR IN SMS ",error);
    		            //    fn("success");
    		            } else {
    		                if(data.indexOf("OK:")>-1){
    		              //      fn("success");
    		                }else{
    		                //	fn("success");
    		                }
    		            }
    		        })
    		}else{
    			console.log("ERRROER ",stat);
		//	fn("success");
    		}
    	})
}
var getSMSDetails = function(req,fn){
	var connection = connections.connection;
	var query = "select * from sms_details";
	connection.query(query,function(err,res){
		if(!err){
			if(res!=undefined){
				if(res.length>0){
					var i=0;
					var smsPath = "";
					iterateSmsDetails();
					function iterateSmsDetails(){
						if(i<res.length){
							smsPath += res[i]['fieldName']+"="+res[i]['value'];
							if(i==res.length-1){
								smsPath += "";
								fn("success",smsPath);
							}
							else{
								smsPath += "&";
								i++;
								iterateSmsDetails();
							}
						}
					}
				}else{
					fn(null);
				}
			}
		}else 
			fn(null);
	})
}
exports.verifyAmbulanceOTP = function(req, fn) {
	var connection = connections.connection;
	var response = {};
	try {
		req.ambulanceMobile = encryptionMob.decrypt(req.ambulanceMobile);
		req.otp = encryptionMob.decrypt(req.otp);
//		console.log("OPOPO ", req.otp)
		var query = "select * from ambulance_master where ambulance_mobile='" + req.ambulanceMobile + "'";
//		console.log("OPOPOP LKL ",query);
		connection.query(query, function(err, res) {
//			console.log("ERRR ",err);
console.log("RES",res);
			if (!err) {
				if (res != undefined) {
					if (res.length == 0) {
						response.statusCode = "302";
						response.statusMessage = "no ambulance found with that number";
						fn(JSON.stringify(response));
					} else {
console.log("DJFLJDSF ",res[0].otp,req.otp,(res[0].otp==req.otp));
						if (res[0].otp == req.otp) {
							var token = jwt.sign({ userId: res[0].amb_id }, req.otp);
							// req['token'] = token;
							var tokenResp = null;
							// req['userId'] = res[0].amb_id;
							getVehicleNameById(res[0].vehicle_type, function(stat) {
								if (stat != null) {
									res[0].vehicle_type = stat;
									loginDao.getEmergencyTypeNameById(res[0].emergency_type, function(status,typeObj) {
										if (status != null) {
											res[0].emergency_type = status;
											res[0].emergency_types =typeObj;
											tokenResp = token;
											//update device details
											var q = "update ambulance_master set token='" + token + "',is_verified='Yes',device_imei='" + req.deviceIMEI + "'," +
											"device_os = '" + req.deviceOs + "'," +
											"device_type = '" + req.deviceType + "'," +
											"device_token='" + req.deviceToken + "',app_version='"+req.app_version+"',login_status='LoggedIn' where amb_id='" + res[0].amb_id + "'";

											connection.query(q, function(error, result) {
												if (!error) {
													
													getGroupNameById(res[0].group_id, function(grpName) {
														getAmbAttributes(res[0].amb_id, function(stat,attr){
															console.log("OOOOOOOOOOOOOO ",stat);
															console.log("data ",grpName);
															//if(stat!=null){
															res[0]['ambulance_attributes']=attr;
															//}else{
															//   res[0]['ambulance_attributes']=attr;
															// }
															res[0]['token'] = tokenResp;
															 res[0]['group_name'] = grpName.group_name;
															 res[0]['group_mobile_number'] = grpName.emergency_number;
															 res[0]['gcm_key']=encryptionMob.encrypt(commonConstants.googleAPIKEY);
															response.statusCode = "300";
															response.statusMessage = "Verified successfully";
															response.responseData = res[0];
															fn(JSON.stringify(response));
														})
													})
												} else {
													console.log("IOIO ", error)
													response.statusCode = "500";
													response.statusMessage = "Error occured while verifying otp";
													fn(JSON.stringify(response));
												}

											})
										}else{
											response.statusCode = "500";
											response.statusMessage = "No Emergency types associated with this ambulance,Please contact administrator.";
											fn(JSON.stringify(response));
										}
									})
								}
							})
						} else {
						//	console.log("IOIO ", error)
							response.statusCode = "303";
							response.statusMessage = "Invalid otp, try with valid otp";
							fn(JSON.stringify(response));
							
						}

					}
				} else {
					response.statusCode = "500";
					response.statusMessage = "otp verified but error occured while updating device details";
					fn(JSON.stringify(response));
				}


			} else {
				console.log("ERROR ONNN   ", err)
				response.statusCode = "500";
				response.statusMessage = "Error occured while verifying otp";
				fn(JSON.stringify(response));
			}
		});
	} catch (e) {
		console.log("CATCH  ", e)
		response.statusCode = "500";
		response.statusMessage = "Error occured while verifying otp";
		fn(JSON.stringify(response));
	}
}
var getAmbAttributes = function(req,fn){
	var connection = connections.connection;
	var query = "select attribute_name,attribute_details from ambulance_attributes where ambulance_id="+req;
	console.log("OPOPO query ", query);
	connection.query(query,function(err,res){
		console.log("RESULT ",res);
		if(!err){
			if(res!=undefined){
				if(res.length >0){
					fn("success",res);
				}else{
					fn("noAttrs",res);
				}
			}
		}else
			fn(null);
	})
}
/** Driver Login functionality */

exports.driverLogin = function(req, fn) {
	var connection = connections.connection;
	var response = {};
	try {
		req.driverPin = encryptionMob.decrypt(req.driverPin);
		var query = "select * from driver_master where driver_id=" + req.driverId + ";";
		connection.query(query, function(err, res) {
			if (!err) {
				if (res != undefined) {
					if (res.length == 0) {
						response.statusCode = "302";
						response.statusMessage = "no driver found with given id";
						fn(JSON.stringify(response));
					} else {
						if (res[0].is_active == "Active") {
							if (res[0].driver_pin == req.driverPin) {
								//		var qry = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver where driver_id=" + req.driverId + " and ambulance_id = "+req.ambulanceId+" and status = 'Online')";
	//							var qry = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver where status='Online' and driver_id=" + req.driverId + " and ambulance_id = "+req.ambulanceId+")";
 								var qry = "SELECT * FROM ambulance_driver WHERE created_date in (SELECT MAX(created_date) FROM ambulance_driver where status='Online' and driver_id=" + req.driverId + ")";

								connection.query(qry, function(error, result) {
									if (!error) {
										if (result != undefined) {
									//		result.length = 0; //for temporory
											if (result.length > 0) {
												response.statusCode = "510";
												response.statusMessage = "Driver is already logged in other ambulance.";
												fn(JSON.stringify(response));
											} else {
												var driver = {};
												driver.ambulanceId = req.ambulanceId;
												driver.driverId = req.driverId;
												driver.longitude = req.longitude;
												driver.latitude = req.latitude;
												driver.geoHashCode = req.geoHashCode;
												driver.speed = req.speed;
												driver.status = "online";
												insertAmbulanceDriverDetails(driver, function(status) {
													if (status != null) {
														/* generalDao.insertIntoTracking(driver, function(stat, obj) {
                                        console.log("ERROr ", stat)
                                        if (stat == "success") {*/
														response.statusCode = "300";
														response.statusMessage = "logged in successfully";
														response.responseData = res[0];
														fn(JSON.stringify(response));
														/*   } else {
                                            response.statusCode = "500";
                                            response.statusMessage = "Error occured while verifying driver";
                                            fn(JSON.stringify(response));
                                        }
                                    })*/

													} else {
														console.log("OOOOOOOOOO ", status)
														response.statusCode = "500";
														response.statusMessage = "Error occured while verifying driver";
														fn(JSON.stringify(response));
													}
												});
											}
										}
									} else {
										console.log("Error  ", err);
										response.statusCode = "500";
										response.statusMessage = "Error occured while verifying driver";
										fn(JSON.stringify(response));
									}
								})
							} else {
								response.statusCode = "303";
								response.statusMessage = "Invalid pin, try with valid pin";
								fn(JSON.stringify(response));
							}
						} else {
							response.statusCode = "511";
							response.statusMessage = "Not an Active Driver, Please contact administrator";
							fn(JSON.stringify(response));
						}
					}
				}
			} else {
				console.log("Error  ", err);
				response.statusCode = "500";
				response.statusMessage = "Error occured while verifying driver";
				fn(JSON.stringify(response));
			}
		});
	} catch (e) {
		console.log("CATCH  ", e)
		response.statusCode = "500";
		response.statusMessage = "Error occured while verifying driver";
		fn(JSON.stringify(response));
	}
}
exports.driverLogout = function(req, fn) {
	var connection = connections.connection;
	var response = {};
	try {
		var query = "select * from ambulance_driver where driver_id=" + req.driverId + " and ambulance_id=" + req.ambulanceId + ";";
		connection.query(query, function(err, res) {
			if (!err) {
				if (res != undefined) {
					// if (res[0].status == "Online") {
					var q = "update ambulance_driver set status = 'Offline',longitude='" + req.longitude + "', latitude='" + req.latitude + "' where driver_id=" + req.driverId + " and ambulance_id=" + req.ambulanceId + ";";
					connection.query(q, function(error, result) {
						if (!error) {
							response.statusCode = "300";
							response.statusMessage = "loggedout successfully";
							fn(JSON.stringify(response));
						} else {
							response.statusCode = "500";
							response.statusMessage = "Error occured while logging out";
							fn(JSON.stringify(response));
						}
					})

					/* } else {
                        response.statusCode = "444";
                        response.statusMessage = "already logged out,please contact administration";
                        fn(JSON.stringify(response));
                    }*/
				}
			} else {
				// console.log("CATCH  ", e);
				response.statusCode = "500";
				response.statusMessage = "Error occured while logging out";
				fn(JSON.stringify(response));
			}
		})
	} catch (e) {
		console.log("CATCH  ", e)
		response.statusCode = "500";
		response.statusMessage = "Error occured while logging out";
		fn(JSON.stringify(response));
	}
}
exports.logout = function(req, fn) {
	var connection = connections.connection;
	var respJson = {};
	var query = "select * from app_user where deviceIMEI='" + req.IMEI + "' and mobile='" + req.evd + "' and loginStatus ='true'";
	connection.query(query, function(err, res) {
		if (!err) {
			if (res.length > 0) {
				respJson['userId'] = res[0].generated_UserID;
				var query = "update app_user set token='null',loginStatus='loggedOut' where deviceIMEI='" + req.IMEI + "' and mobile='" + req.evd + "'";
				connection.query(query, function(error, result) {
					if (!error) {
						respJson['statusCode'] = "200";
						respJson["statusMessage"] = "logged out successfully";
						fn(JSON.stringify(respJson));
					}
				});
			} else {
				respJson['statusCode'] = "201";
				respJson["statusMessage"] = "no retailer found logged in with given details";
				fn(JSON.stringify(respJson));
			}
		} else {
			respJson['statusCode'] = '500';
			respJson['statusMessage'] = "Error occured";
			fn(JSON.stringify(respJson));
		}
	})
}


var getCircleIdByShortName = function(shortName, fn) {
	var connection = connections.connection;
	var query = "select circle_id from circle_master where short_name = '" + shortName + "'";
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined && res.length > 0) {
				fn(res[0].circle_id);
			} else {
				fn(null)
			}
		} else {
			fn(null);
		}
	})
}

var getUserTypeByTypeId = function(typeId, fn) {
	var connection = connections.connection;
	var query = "select type_name from user_types where id=" + typeId;
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined && res.length > 0)
				fn("success", res[0].type_name);
		}
	})
}

var getVehicleNameById = function(id, fn) {
	var connection = connections.connection;
	var query = "Select type_name from vehicle_type where vehicle_id=" + id;
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined && res.length > 0) {
				fn(res[0].type_name);
			} else {
				fn(null);
			}
		} else
			fn(null);
	})
}

var getGroupNameById = function(id, fn) {
	var connection = connections.connection;
	var query = "Select group_name,emergency_number from group_master where group_id=" + id;
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined && res.length > 0) {
				fn(res[0]);
			} else {
				fn(null);
			}
		} else
			fn(null);
	})
}

/*var getEmergencyTypeNameById = function(id, fn) {
    var connection = connections.connection;
    var query = "Select type_name from emergency_types where id=" + id;
    connection.query(query, function(err, res) {
        if (!err) {
            if (res != undefined && res.length > 0) {
                fn(res[0].type_name);
            } else {
                fn(null);
            }
        } else
            fn(null);
    })
}*/
exports.getEmergencyTypeNameById = function(id, fn) {
	var connection = connections.connection;
	var query = "Select type_name from emergency_types where id=" + id;
	if (id.indexOf(",")) {
		ids = id.split(",");
		var i = 0;
		var names = "";
		var Obj = [];
		iterateEmerges();

		function iterateEmerges() {
			if (i < ids.length) {
				query = "Select type_name from emergency_types where id=" + ids[i];
				connection.query(query, function(err, res) {
					if (!err) {
						if (res != undefined && res.length > 0) {
							names += res[0].type_name;
							var typeObj = {
								id:ids[i],
								type_name:res[0].type_name
							}
							Obj.push(typeObj);
							if (i == ids.length - 1) {
								names += "";
								fn(names,Obj);
							} else {
								names += ","
								i++;
								iterateEmerges();
							}
						} else {
							if (i == ids.length - 1) {
								if(Obj.length>0)fn(names,Obj);
                                else fn(null);
							} else {
								i++;
								iterateEmerges();
							}

						}
					} else
						if (i == ids.length - 1) {
							if(Obj.length>0)fn(names,Obj);
                            else fn(null);
						} else {
							i++;
							iterateEmerges();
						}
				})
			}
		}

	} else {
		connection.query(query, function(err, res) {
			if (!err) {
				if (res != undefined && res.length > 0) {
					fn(res[0].type_name);
				} else {
					fn(null);
				}
			} else
				fn(null);
		})
	}
}
var insertAmbulanceDriverDetails = function(driver, fn) {
	var connection = connections.connection;
	var query = "insert into ambulance_driver(ambulance_id,driver_id,longitude,latitude,geoHashCode,status) values(" + driver.ambulanceId + "," + driver.driverId + ",'" + driver.longitude + "','" + driver.latitude + "','"+ driver.geoHashCode +"','Online')";
	console.log("QEURY ", query);
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				fn("success");
			}
		} else {
			console.log("IOIOIOI ", err)
			fn(null);
		}
	})
}


/** Customer Login Services **/
/*exports.customerGenerateOTP = function(req, fn) {
	var connection = connections.connection;
	var response = {};
	try {
		console.log("REQUEST ", req.body);
		if (req.body.customerMobile != undefined && req.body.customerMobile != "" && req.body.deviceIMEI != undefined) {
			req.body.customerMobile = encryptionMob.decrypt(req.body.customerMobile);
			var query = "select * from customer_master where customer_mobile_number = '" + req.body.customerMobile + "' and device_imei='"+req.body.deviceIMEI+"'";
			connection.query(query, function(err, res) {
				console.log("ERROR in 1st check ", err)
				if (!err) {
					if (res != undefined) {
						console.log("RESULT ", res);
						if (res.length > 0) {
							for (var i = 0; i < res.length; i++) {
								if (res[i].device_imei == req.body.deviceIMEI) {
									//update otp
									var updateObj = {
											customerMobile: req.body.customerMobile,
											deviceIMEI: req.body.deviceIMEI
									}
									updateCustomerMaster(updateObj, function(status, otp) {
										if (status == "success") {
											//send sms
var smsObj = {'mobilenumber':req.body.customerMobile,'message':otp+' is your otp'}
											sendSMS(smsObj, function(status) {
												if (status == "success") {
													connection.query("update customer_master set is_verified='Pending',sms_status='Sent' where customer_mobile_number='" + req.body.customerMobile + "' and device_imei='"+req.body.deviceIMEI+"'", function(err1, res1) {
														if (!err1) {
															response.statusCode = "300";
															response.statusMessage = "Your otp has been sent to registered mobile number";
															response.otp = otp;
															response.customerMobile = req.body.customerMobile;
															fn(JSON.stringify(response));
														} else {
															response.statusCode = "500";
															response.statusMessage = "Error occured while generating otp";
															fn(JSON.stringify(response));
														}
													})

												} else {
													response.statusCode = "333";
													response.statusMessage = "OTP generated, failed to send sms. Please contact administration";
													fn(JSON.stringify(response));
												}
											})
										} else {
											//throw error and end response
											response.statusCode = "500";
											response.statusMessage = "Error occured while generating otp";
											fn(JSON.stringify(response));
										}
									})
								} else {
									//create row
									var smsObj = {'mobilenumber':req.body.customerMobile,'message':otp+' is your otp'}
									var insertObj = {
											customerMobile: req.body.customerMobile,
											deviceIMEI: req.body.deviceIMEI
									}
									insertCustomerMaster(insertObj, function(status, otp) {
										if (status == "success") {
											//send sms
											sendSMS(smsObj, function(status) {
												if (status == "success") {

													connection.query("update customer_master set is_verified='Pending',sms_status='Sent' where customer_mobile_number='" + req.body.customerMobile + "' and device_imei = '"+req.body.deviceIMEI+"'", function(err1, res1) {
														if (!err1) {
															response.statusCode = "300";
															response.statusMessage = "Your otp has been sent to registered mobile number";
															response.otp = otp;
															response.customerMobile = req.body.customerMobile;
															fn(JSON.stringify(response));
														} else {
															response.statusCode = "500";
															response.statusMessage = "Error occured while generating otp";
															fn(JSON.stringify(response));
														}
													})

												} else {
													response.statusCode = "333";
													response.statusMessage = "OTP generated, failed to send sms. Please contact administration";
													fn(JSON.stringify(response));
												}
											})
										} else {
											//throw error and end response
											response.statusCode = "500";
											response.statusMessage = "Error occured while generating otp";
											fn(JSON.stringify(response));
										}
									})
								}
							}
						} else {
							//create row
							console.log("OPOPOP ", res.length)
							var insertObj = {
								customerMobile: req.body.customerMobile,
								deviceIMEI: req.body.deviceIMEI
							}
							insertCustomerMaster(insertObj, function(status, otp) {
								if (status == "success") {
									//send sms
									var smsObj = {'mobilenumber':req.body.customerMobile,'message':otp+' is your otp'}
									sendSMS(smsObj, function(status) {
										if (status == "success") {
											connection.query("update customer_master set is_verified='Pending',sms_status='Sent' where customer_mobile_number='" + req.body.customerMobile + "' and device_imei = '"+req.body.deviceIMEI+"'", function(err1, res1) {
												if (!err1) {
													response.statusCode = "300";
													response.statusMessage = "Your otp has been sent to registered mobile number";
													response.otp = otp;
													response.customerMobile = req.body.customerMobile;
													fn(JSON.stringify(response));
												} else {
													response.statusCode = "500";
													response.statusMessage = "Error occured while generating otp";
													fn(JSON.stringify(response));
												}
											})

										} else {
											response.statusCode = "333";
											response.statusMessage = "OTP generated, failed to send sms. Please contact administration";
											fn(JSON.stringify(response));
										}
									})
								} else {
									//throw error and end response
									response.statusCode = "500";
									response.statusMessage = "Error occured while generating otp";
									fn(JSON.stringify(response));
								}
							})
						}
					}
				} else {
					response.statusCode = "500";
					response.statusMessage = "Error occured while generating otp";
					fn(JSON.stringify(response));
				}
			});
		}
	} catch (e) {
		response.statusCode = "500";
		response.statusMessage = "Error occured while generating otp";
		fn(JSON.stringify(response));
	}
}

var updateCustomerMaster = function(req, fn) {
	var otp = commonMethod.generateOTP();
	var query = "update customer_master set otp = '" + otp + "', device_imei = '" + req.deviceIMEI + "',new_user='no' where customer_mobile_number='" + req.customerMobile + "' and device_imei = '"+req.deviceIMEI+"'";
	connection.query(query, function(err, res) {
	console.log("QUery,", query,"err ",err);
		if (!err) {
			if (res != undefined) {
				fn("success", otp);
			}
		} else
			fn("error");
	})
}

var insertCustomerMaster = function(req, fn) {
	var otp = commonMethod.generateOTP();
	var query = "insert into customer_master(customer_id,customer_mobile_number,device_imei,otp,new_user) values(" + commonMethod.getRandomUserAccessId() + ",'" + req.customerMobile + "','" + req.deviceIMEI + "','" + otp + "','yes')";
	console.log("YYYY ",query);
	connection.query(query, function(err, res) {
	console.log("ERR ",err);
		if (!err) {
			if (res != undefined) {
				fn("success", otp);
			}
		} else
			fn("error");
	})
}


exports.verifyCustomerOTP = function(req, fn) {
	var connection = connections.connection;
	var response = {};
	try {
		console.log("IUOIIIIIIIIIi ",req,encryptionMob.decrypt(req.customerMobile))
		req.customerMobile = encryptionMob.decrypt(req.customerMobile);
		req.otp = encryptionMob.decrypt(req.otp);
		console.log("OPOPO ", req.otp)
		var query = "select * from customer_master where customer_mobile_number='" + req.customerMobile + "'";
		connection.query(query, function(err, res) {
			if (!err) {
				if (res != undefined) {
					if (res.length == 0) {
						response.statusCode = "302";
						response.statusMessage = "no customer found with given number";
						fn(JSON.stringify(response));
					} else {
						var i = 0;
						var count = 0;
						var otpMatched = false;
						var iterationNumber = 0;
						iterateCustomer();

						function iterateCustomer() {
							if (res[i].device_imei == req.deviceIMEI) {
								count += 1;
								if (res[i].otp == req.otp) {
									iterationNumber = i;
									otpMatched = true;
								}
							}

							if (i == res.length - 1) {
								if (count == 0) {
									response.statusCode = "302";
									response.statusMessage = "no customer found with given number";
									fn(JSON.stringify(response));
								} else if (count > 0 && otpMatched == true) {
									var token = jwt.sign({ userId: res[iterationNumber].customer_id }, req.otp);
									var tokenResp = null;
									tokenResp = token;
									//update device details
									var q = "update customer_master set token='" + tokenResp + "',is_verified='Yes',device_imei='" + req.deviceIMEI + "'," +
									"device_os = '" + req.deviceOs + "'," +
									"device_type = '" + req.deviceType + "'," +
									"is_active = 'Yes', loginStatus = 'loggedIn'," +
									"device_type = '" + req.deviceType + "'," +
									"device_type = '" + req.deviceType + "'," +
									"device_token='" + req.deviceToken + "',app_version='"+req.app_version+"' where customer_id='" + res[iterationNumber].customer_id + "'";
									console.log("IIIIIIIII ", req.token)

									connection.query(q, function(error, result) {
										if (!error) {
											connection.query("select * from customer_master where customer_id=" + res[iterationNumber].customer_id, function(error, result) {
												if (!error) {
													//response['token'] = tokenResp;
													response.statusCode = "300";
													response.statusMessage = "Verified successfully";
													response.responseData = result[0];
	console.log("veirfy otp ", result[0]);
													fn(JSON.stringify(response));
												} else {
													response.statusCode = "500";
													response.statusMessage = "Error occured while verifying otp";
													fn(JSON.stringify(response));
												}
											})

										} else {
											console.log("IOIO ", error)
											response.statusCode = "500";
											response.statusMessage = "Error occured while verifying otp";
											fn(JSON.stringify(response));
										}

									})


								} else if (count > 0 && otpMatched == false) {
									response.statusCode = "303";
									response.statusMessage = "Invalid otp, try with valid otp";
									fn(JSON.stringify(response));
								}
							} else {
								i++;
								iterateCustomer();
							}

						}


					}
				}


			} else {
				console.log("ERROR ONNN   ", err)
				response.statusCode = "500";
				response.statusMessage = "Error occured while verifying otp";
				fn(JSON.stringify(response));
			}
		});
	} catch (e) {
		console.log("CATCH  ", e)
		response.statusCode = "500";
		response.statusMessage = "Error occured while verifying otp";
		fn(JSON.stringify(response));
	}
}*/


exports.customerGenerateOTP = function(req, fn) {
	var connection = connections.connection;
	var response = {};
	try {
		console.log("REQUEST ", req.body);
		if (req.body.customerMobile != undefined && req.body.customerMobile != "" && req.body.deviceIMEI != undefined) {
				req.body.customerMobile = encryptionMob.decrypt(req.body.customerMobile);
			var query = "select * from customer_master where customer_mobile_number = '" + req.body.customerMobile +"' order by created_date desc limit 1";
			console.log("QUERY CUS GEN ",query);
			connection.query(query, function(err, res) {
				console.log("ERROR in 1st check ", err)
				if (!err) {
					if (res != undefined) {
						console.log("RESULT ", res);
						if (res.length > 0) {
							for (var i = 0; i < res.length; i++) {
								//if (res[i].device_imei == req.body.deviceIMEI){
								/*					if(res[0].loginStatus == "loggedIn"){
							console.log("JL IN LOGGED IN ")
							   if(res[0].device_imei != req.body.deviceIMEI){
								//send pn to old imei
							    var pnMsg = "logout the customer";	
							    var type = "customer_logout";
							    var deviceToken = [res[0].device_token];
							   sendPNDao.sendPN(deviceToken, "", pnMsg, type);
								}
							}*/
								/*		response.statusCode = "417";
								response.statusMessage = "You are already logged in other device";
								fn(JSON.stringify(response));*/

								//update otp
								var updateObj = {
										customerMobile: req.body.customerMobile,
										deviceIMEI: req.body.deviceIMEI
								}
								updateCustomerMaster(updateObj, function(status, otp) {
									if (status == "success") {
										//send sms
										var smsObj = {'mobilenumber':req.body.customerMobile,'message':otp+' is your otp'}
										loginDao.sendSMS(smsObj, function(status) {
											if (status == "success") {
												connection.query("update customer_master set is_verified='Pending',sms_status='Sent' where customer_mobile_number='" + req.body.customerMobile + "' order by created_date desc limit 1", function(err1, res1) {
													if (!err1) {
														response.statusCode = "300";
														response.statusMessage = "Your otp has been sent to registered mobile number";
														response.otp = otp;
														response.customerMobile = req.body.customerMobile;
														fn(JSON.stringify(response));
													} else {
														response.statusCode = "500";
														response.statusMessage = "Error occured while generating otp";
														fn(JSON.stringify(response));
													}
												})

											} else {
												response.statusCode = "333";
												response.statusMessage = "OTP generated, failed to send sms. Please contact administration";
												fn(JSON.stringify(response));
											}
										})
									} else {
										//throw error and end response
										response.statusCode = "500";
										response.statusMessage = "Error occured while generating otp";
										fn(JSON.stringify(response));
									}
								})
								/*	} else {
									//create row
									var smsObj = {'mobilenumber':req.body.customerMobile,'message':otp+' is your otp'}
									var insertObj = {
											customerMobile: req.body.customerMobile,
											deviceIMEI: req.body.deviceIMEI
									}
									insertCustomerMaster(insertObj, function(status, otp) {
										if (status == "success") {
											//send sms
											sendSMS(smsObj, function(status) {
												if (status == "success") {

													connection.query("update customer_master set is_verified='Pending',sms_status='Sent' where customer_mobile_number='" + req.body.customerMobile + "' and device_imei = '"+req.body.deviceIMEI+"'", function(err1, res1) {
														if (!err1) {
															response.statusCode = "300";
															response.statusMessage = "Your otp has been sent to registered mobile number";
															response.otp = otp;
															response.customerMobile = req.body.customerMobile;
															fn(JSON.stringify(response));
														} else {
															response.statusCode = "500";
															response.statusMessage = "Error occured while generating otp";
															fn(JSON.stringify(response));
														}
													})

												} else {
													response.statusCode = "333";
													response.statusMessage = "OTP generated, failed to send sms. Please contact administration";
													fn(JSON.stringify(response));
												}
											})
										} else {
											//throw error and end response
											response.statusCode = "500";
											response.statusMessage = "Error occured while generating otp";
											fn(JSON.stringify(response));
										}
									})
								}*/
							}
						} else {
							//create row
							console.log("OPOPOP ", res.length)
							var insertObj = {
								customerMobile: req.body.customerMobile,
								deviceIMEI: req.body.deviceIMEI
							}
							insertCustomerMaster(insertObj, function(status, otp) {
								if (status == "success") {
									//send sms
									var smsObj = {'mobilenumber':req.body.customerMobile,'message':otp+' is your otp'}
									loginDao.sendSMS(smsObj, function(status) {
										if (status == "success") {
											connection.query("update customer_master set is_verified='Pending',sms_status='Sent' where customer_mobile_number='" + req.body.customerMobile + "' order by created_date desc limit 1", function(err1, res1) {
												if (!err1) {
													response.statusCode = "300";
													response.statusMessage = "Your otp has been sent to registered mobile number";
													response.otp = otp;
													response.customerMobile = req.body.customerMobile;
													fn(JSON.stringify(response));
												} else {
													response.statusCode = "500";
													response.statusMessage = "Error occured while generating otp";
													fn(JSON.stringify(response));
												}
											})

										} else {
											response.statusCode = "333";
											response.statusMessage = "OTP generated, failed to send sms. Please contact administration";
											fn(JSON.stringify(response));
										}
									})
								} else {
									//throw error and end response
									response.statusCode = "500";
									response.statusMessage = "Error occured while generating otp";
									fn(JSON.stringify(response));
								}
							})
						}
					}
				} else {
					response.statusCode = "500";
					response.statusMessage = "Error occured while generating otp";
					fn(JSON.stringify(response));
				}
			});
		}
	} catch (e) {
		response.statusCode = "500";
		response.statusMessage = "Error occured while generating otp";
		fn(JSON.stringify(response));
	}
}

var updateCustomerMaster = function(req, fn) {
	var otp = commonMethod.generateOTP();
	var query = "update customer_master set otp = '" + otp + "',new_user='no' where customer_mobile_number='" + req.customerMobile + "' order by created_date desc limit 1";
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				fn("success", otp);
			}
		} else
			fn("error");
	})
}

var insertCustomerMaster = function(req, fn) {
	var otp = commonMethod.generateOTP();
	var query = "insert into customer_master(customer_id,customer_mobile_number,device_imei,otp,new_user) values(" + commonMethod.getRandomUserAccessId() + ",'" + req.customerMobile + "','" + req.deviceIMEI + "','" + otp + "','yes')";
	connection.query(query, function(err, res) {
		if (!err) {
			if (res != undefined) {
				fn("success", otp);
			}
		} else
			fn("error");
	})
}


exports.verifyCustomerOTP = function(req, fn) {
	var connection = connections.connection;
	var response = {};
	try {
		console.log("SDLFJDSLKJ ",req);
		//	console.log("IUOIIIIIIIIIi ",req,encryptionMob.decrypt(req.customerMobile))
			req.customerMobile = encryptionMob.decrypt(req.customerMobile);
			req.otp = encryptionMob.decrypt(req.otp);
		

		//req.customerMobile = encryptionMob.decrypt(req.customerMobile);
//		req.otp = encryptionMob.decrypt(req.otp);
		console.log("OPOPO ", req.otp)
		var query = "select * from customer_master where customer_mobile_number='" + req.customerMobile + "' order by created_date desc limit 1";
		console.log("LLLLL ",query);
		connection.query(query, function(err, res) {
			if (!err) {
				if (res != undefined) {
					if (res.length == 0) {
						response.statusCode = "302";
						response.statusMessage = "no customer found with given number";
						fn(JSON.stringify(response));
					} else {
	var loggedIMEI ="";
						if(res[0]!=undefined && res[0].loginStatus == "loggedIn"){
							console.log("JL IN LOGGED IN ")
							if(res[0].device_imei != req.deviceIMEI){
								//send pn to old imei
							/*	var pnMsg = "logout the customer";
								var type = "customer_logout";
								var deviceToken = [res[0].device_token];
                                var sendObj = {user_type:"Customer", send_to:res[0].customer_mobile_number};
                               sendPNDao.sendPN(deviceToken, "", pnMsg, type,sendObj);*/
								loggedIMEI = res[0].device_imei;

							}
						}
						var i = 0;
						var count = 0;
						var otpMatched = false;
						var iterationNumber = 0;
						iterateCustomer();

						function iterateCustomer() {
							if (res[i].device_imei == req.deviceIMEI || res[i].device_imei != req.deviceIMEI) {
								count += 1;
								if (res[i].otp == req.otp) {
									iterationNumber = i;
									otpMatched = true;
								}
							}

							if (i == res.length - 1) {
								if (count == 0) {
									response.statusCode = "302";
									response.statusMessage = "no customer found with given number";
									fn(JSON.stringify(response));
								} else if (count > 0 && otpMatched == true) {
									var token = jwt.sign({ userId: res[iterationNumber].customer_id }, req.otp);
									var tokenResp = null;
									tokenResp = token;
									//update device details
									var q = "update customer_master set token='" + tokenResp + "',is_verified='Yes',device_imei='" + req.deviceIMEI + "'," +
									"device_os = '" + req.deviceOs + "'," +
									"device_type = '" + req.deviceType + "'," +
									"is_active = 'Yes', loginStatus = 'loggedIn'," +
									"device_type = '" + req.deviceType + "'," +
									"device_type = '" + req.deviceType + "'," +
									"device_token='" + req.deviceToken + "',app_version='"+req.app_version+"' where customer_id='" + res[iterationNumber].customer_id + "'";
									console.log("IIIIIIIII ", req.token)

									connection.query(q, function(error, result) {
										if (!error) {
											connection.query("select * from customer_master where customer_id=" + res[iterationNumber].customer_id, function(error, result) {
												if (!error) {
													//response['token'] = tokenResp;
													response.statusCode = "300";
													response.statusMessage = "Verified successfully";
													response.responseData = result[0];
													console.log("veirfy otp ", result[0]);
													fn(JSON.stringify(response));
													if (res[0].device_imei != req.deviceIMEI) {
                                                            var pnMsg = "logout the customer";
                                                            var type = "customer_logout";
                                                            var deviceToken = [res[0].device_token];
                                                            var sendObj = { user_type: "Customer", send_to: res[0].customer_mobile_number };
                                                            sendPNDao.sendPN(deviceToken, "", pnMsg, type, sendObj);
                                                        }
												} else {
													response.statusCode = "500";
													response.statusMessage = "Error occured while verifying otp";
													fn(JSON.stringify(response));
												}
											})

										} else {
											console.log("IOIO ", error)
											response.statusCode = "500";
											response.statusMessage = "Error occured while verifying otp";
											fn(JSON.stringify(response));
										}

									})


								} else if (count > 0 && otpMatched == false) {
									response.statusCode = "303";
									response.statusMessage = "Invalid otp, try with valid otp";
									fn(JSON.stringify(response));
								}
							} else {
								i++;
								iterateCustomer();
							}

						}
						//}

					}
				}


			} else {
				console.log("ERROR ONNN   ", err)
				response.statusCode = "500";
				response.statusMessage = "Error occured while verifying otp";
				fn(JSON.stringify(response));
			}
		});
	} catch (e) {
		console.log("CATCH  ", e)
		response.statusCode = "500";
		response.statusMessage = "Error occured while verifying otp";
		fn(JSON.stringify(response));
	}
}

