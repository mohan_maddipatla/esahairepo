'use strict';
var tableOperationsDao = require("../Dao/tableOperationsDao.js");
var appUserDao = require("../Dao/appUserDao.js");
var logsDao = require("../Dao/logsDao.js");
var serviceDao = require("../Dao/serviceDao.js");
var connections = require("../Util/dbConnection.js");
var constants = require("../Util/commonConstants.js");
var logsDao = require("../Dao/logsDao.js");
var statusConstant = require("../Util/statusConstants.json");
var datetime = require('node-datetime');
var logger = require('../logging/logger.js');
exports.createTable = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'createTable';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    console.log(serviceName + "  serviceName");
    serviceDao.getServiceId(serviceName, function(data) {
        console.log("data  " + data);
        if (data != null) {
            var serviceparams = {};
            serviceparams.serviceId = data;
            req.body.serviceParams = serviceparams;

        }
        console.log("createTable- started");
        var tableName = req.body.tableName,
            fields = req.body.fields; //gets array of fields
        console.log(fields + "  fields");
        tableOperationsDao.createTable(req, function(data, status) {
            console.log("Data ", data)
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.CREATE_SUCCESS;
                res.end(JSON.stringify(response));

            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstantsSTATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        })
    })
}
exports.alterTable = function(req, res, next) {
    console.log("alterTable- started");

    var apiReq = {}; //api-log
    apiReq.apiPath = 'alterTable';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            var serviceparams = {};
            serviceparams.serviceId = data;
            req.body.serviceParams = serviceparams;

        }
        appUserDao.updateData(req, function(data, status) {
            if (data === statusConstant.s103) {
                console.log("data ", data);
                if (data.info.affectedRows !=undefined) {
                    response[statusConstant.STATUS_CODE] = statusConstant.OK;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.UPDATE_SUCCESS;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                    response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s106) {

                    response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else {
                    response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.s110;
                    res.end(JSON.stringify(response));
                }

            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE ] = status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        });
    });
}
exports.deleteTable = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'deleteTable';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    console.log(serviceName + "asjdadkasda");
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            var serviceparams = {};
            serviceparams.serviceId = data;
            req.body.serviceParams = serviceparams;

        }
        console.log("deleteTable- started " + req.params);
        var name = req.param('tableName');
        console.log(name + " table name   ");
        tableOperationsDao.dropTable(req, function(data, status) {
            console.log("Data ", data)
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] =  statusConstant.DELETE_SUCCESS;
                res.end(JSON.stringify(response));

            } else if (data == statusConstant.s106 ) {

                response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        });
    });
}
exports.insertRecord = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'insertRecord';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            console.log(constants.appDeviceServiceId);
            if (data == constants.appUserServiceId) {
                req.body.tableName = statusConstant.TABLE_APP_USER;
                req.body.systemParams = {};
                req.body.systemParams.generated_userID = "";
            } else if (data == constants.appDeviceServiceId) {
                req.body.tableName = 'deviceTable';
            } else {
                var serviceparams = {};
                serviceparams.serviceId = data;
                req.body.serviceParams = serviceparams;
            }


        }
        console.log("insertRecord- started");
        appUserDao.insertData(req, function(data, status) {
            logger.info("Data!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ", data);
	//    console.log("OOOOP ",typeof status, status.toString().split("{ Error:"));
            if (data != null) {
                if (data == statusConstant.s121) {
	//		console.log("LKDLFKLDK ",JSON.parse(status));
                    response[statusConstant.STATUS_CODE] = statusConstant.DUPLICATE;
                    response[statusConstant.STATUS_MESSAGE] = status.toString().split("{ Error:")[0];
	//		console.log("DLFJDSKJFLKDSJ F",response,JSON.stringify(response));
                    res.end(JSON.stringify(response));
                 }else if(data == "exceeded"){
            		  response[statusConstant.STATUS_CODE] = "500";
                      response[statusConstant.STATUS_MESSAGE] = "Group limit is exceeding, please increase the group limit";
                      res.end(JSON.stringify(response));
            	}
		else if (data == statusConstant.SAVE_SUCCESS || data.info.affectedRows != undefined ) {
                    response[statusConstant.STATUS_CODE] = statusConstant.OK;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.INSERT_SUCCESS;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                    response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.SAVE_SUCCESS) {
                    response[statusConstant.STATUS_CODE] = statusConstant.OK;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } /*else if (data == statusConstant.s121) {
                    response[statusConstant.STATUS_CODE] = statusConstant.DUPLICATE;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.s113;
                    res.end(JSON.stringify(response));
                }*/ else if (data == statusConstant.s106) {
                    response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else {
                    response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.s114;
                    res.end(JSON.stringify(response));
                }
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s116 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        });
    });
}
exports.updateRecord = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'updateRecord';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;


    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            if (data == constants.appUserServiceId) {
                req.body.tableName = statusConstant.TABLE_APP_USER;
            } else if (data == constants.appDeviceServiceId) {
                req.body.tableName = 'deviceTable';
            } else {
                var serviceparams = {};
                serviceparams.serviceId = data;
                req.body.serviceParams = serviceparams;
            }


        }
        console.log("updateRecord- started");
        appUserDao.updateData(req, function(data, status) {
            console.log("Data ", data)
            if (data != null) {
                if (data == statusConstant.s121) {
                    response[statusConstant.STATUS_CODE] = statusConstant.DUPLICATE;
                    response[statusConstant.STATUS_MESSAGE] = /*statusConstant.s113*/status.toString().split("{ Error:")[0];
                    res.end(JSON.stringify(response));
                }else if (status== "" || data == "Successfully Saved" || (data.info && data.info.affectedRows !=undefined || data=="")) {
                    response[statusConstant.STATUS_CODE] = statusConstant.OK;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.UPDATE_SUCCESS;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                    response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s106) {
                    response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.SAVE_SUCCESS) {
                    response[statusConstant.STATUS_CODE] = statusConstant.OK;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                }else if(data == "bookingIsInProgress"){
	        	response[statusConstant.STATUS_CODE] = "412";
                    response[statusConstant.STATUS_MESSAGE] = "Booking Is In Progress,Cannot delete Emergency Type";
                    res.end(JSON.stringify(response));
		} else {
                    response[statusConstant.STATUS_MESSAGE] =statusConstant.s115;
                    res.end(JSON.stringify(response));
                }
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s117 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        })
    });
}
exports.deleteRecord = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'deleteRecord';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            if (data == constants.appUserServiceId) {
                req.body.tableName = statusConstant.TABLE_APP_USER;
            } else if (data == constants.appDeviceServiceId) {
                req.body.tableName = 'deviceTable';
            } else {
                var serviceparams = {};
                serviceparams.serviceId = data;
                req.body.serviceParams = serviceparams;
            }


        }
        console.log("deleteRecord- started");
        appUserDao.deleteData(req, function(data, status) {
            if (data != null) {
             if (data == "circleExists"){
                	response[statusConstant.STATUS_CODE] = "555";
                    response[statusConstant.STATUS_MESSAGE] = "This circle associated with users, banners etc, can't delete";
                    res.end(JSON.stringify(response));
                }else if (data.info.affectedRows !=undefined) {
                    response[statusConstant.STATUS_CODE] =  "300";
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.DELETE_SUCCESS;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                    response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s106) {

                    response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else {
                    response[statusConstant.STATUS_CODE] = statusConstant.OK;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.s110;
                    res.end(JSON.stringify(response));
                }
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.BAD_REQUEST;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s118 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        });
    });
}
exports.addColumn = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'addColumn';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            var serviceparams = {};
            serviceparams.serviceId = data;
            req.body.serviceParams = serviceparams;

        }
        console.log("addColumn- started");
        tableOperationsDao.addField(req, function(data, status) {
            var response = {};
            if (data == statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s100;
                res.end(JSON.stringify(response));

            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s119 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        })
    });

}
exports.deleteColumn = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'deleteColumn';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            var serviceparams = {};
            serviceparams.serviceId = data;
            req.body.serviceParams = serviceparams;

        }
        console.log("deleteColumn- started");
        tableOperationsDao.dropField(req, function(data, status) {
            var response = {};
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.STATUS_CODE;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s101;
                res.end(JSON.stringify(response));

            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s120 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        })
    });

}
exports.updateColumn = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'updateColumn';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;


    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            var serviceparams = {};
            serviceparams.serviceId = data;
            req.body.serviceParams = serviceparams;

        }
        console.log("updateColumn- started");
        tableOperationsDao.modifyField(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.UPDATE_SUCCESS;
                res.end(JSON.stringify(response));

            } else if (data == statusConstant.s106) {
                response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s117 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        });
    });
}
exports.getDataByField = function(req, res, next) {


    var apiReq = {}; //api-log
    apiReq.apiPath = 'Fetch-'+req.body.tableName;
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;

    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            if (data == constants.appUserServiceId) {
                req.body.tableName = statusConstant.TABLE_APP_USER;
            } else if (data == constants.appDeviceServiceId) {
                req.body.tableName = 'deviceTable';
            } else {
                var serviceparams = {};
                serviceparams.serviceId = data;
                req.body.serviceParams = serviceparams;
            }


        }

        appUserDao.getDataByField(req, function(data, status) {
            if (data == statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.TABLE_NAME] = req.body.tableName;
                response[statusConstant.RESULT] = status;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104 || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            }else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s107 + data;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        })
    });
}




exports.getTableSchema = function(req, res, next) {

    var apiReq = {}; //api-log
    apiReq.apiPath = 'getTableSchema';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;


    var response = {};
    var serviceNameArr = req.path.split("/");
    var serviceName = serviceNameArr[2];
    serviceDao.getServiceId(serviceName, function(data) {
        if (data != null) {
            var serviceparams = {};
            serviceparams.serviceId = data;
            req.body.serviceParams = serviceparams;

        }
        console.log("getTableSchema- started", req.method);
        appUserDao.getTableSchema(req, function(data, status) {
            console.log("dbOperations"+JSON.stringify(data))
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                if (status == "tableDesc") {
                    response[statusConstant.RESULT] = data;
                } else
                    response[statusConstant.RESULT] = data;
                res.end(JSON.stringify(response));
            }else if(status == 'tableDesc'){
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.RESULT] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104 || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s107 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        })
    });
}

exports.changePassword = function(req, res, next) {

    var apiReq = {}; //api-log
    var response = {};
    apiReq.apiPath = 'changePassword';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;    
        console.log("changePassword- started");
        appUserDao.changePassword(req, function(data, status) {
            if (data != null) {
             if (data == statusConstant.s103 || data.info && data.info.affectedRows !=undefined || data == statusConstant.s110) {
                    response[statusConstant.STATUS_CODE] =  "300";
                    response[statusConstant.STATUS_MESSAGE] = "Password updated successfully";
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                    response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                }else if(data == statusConstant.s123){			
                    response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                    response[statusConstant.STATUS_MESSAGE] = 'Current Password is Incorrect';
                    res.end(JSON.stringify(response));
                } else if (data == statusConstant.s106) {

                    response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                    response[statusConstant.STATUS_MESSAGE] = data;
                    res.end(JSON.stringify(response));
                } else {
                    response[statusConstant.STATUS_CODE] = statusConstant.OK;
                    response[statusConstant.STATUS_MESSAGE] = statusConstant.s110;
                    res.end(JSON.stringify(response));
                }
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.BAD_REQUEST;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s118 + status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        });
   
}
exports.forgotPassword = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'forgotPassword';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    var response = {};
        appUserDao.forgotPassword(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {
                response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if(data == "inActive"){
            	response['statusCode'] = "410";
            	response['statusMessage'] = statusConstant.s130;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s117 + status;
                res.end(JSON.stringify(response));
            }
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        });
}

exports.commonLimitOrActiveCheck = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'commonLimitOrActiveCheck';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.headers.userid;
    console.log("REQ ", req.body);
    appUserDao.commonLimitOrActiveCheck(req, function(data, status) {
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "success";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "You are unauthorized user";
            res.end(JSON.stringify(respJson));
        } else if (data == "GrouplimitExceeded"){
        	respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Group limit can't be less than older limit as it is associated with active ambulances";
            res.end(JSON.stringify(respJson));
        }else if (data == "onTripDriver"){
        	respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Can't make InActive as this driver is associated with active trip";
            res.end(JSON.stringify(respJson));
        }else if (data == "noAmbulance"){
        	respJson['statusCode'] = "401";
            respJson['statusMessage'] = "No Ambulance found";
            res.end(JSON.stringify(respJson));
        }else if (data == "onTrip"){
        	respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Can't make InActive as this ambulance is associated with active trip";
            res.end(JSON.stringify(respJson));
        }else if (data == "limitExceeded"){
        	respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Can't make Active as the group limit is exceeding";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

/*getGroupMasterList*/
exports.getGroupMasterList = function(req, res, next) {
    var response = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getGroupMasterList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    console.log("getGroupMasterList- started");
    appUserDao.getGroupMasterList(req, function(data, status) {
        console.log("sSSSSSSSSSSSSSSSSSSSSS",data,status)
        if (data != null) {
            if (data == "success" || data.info.affectedRows != undefined) {
                response[statusConstant.STATUS_CODE] = "300";
                response[statusConstant.RESULT] = status;

                response[statusConstant.STATUS_MESSAGE] = "Fetched Active Count Succesfully";
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104 || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = statusConstant.s110;
                res.end(JSON.stringify(response));
            }
        } else {
            response[statusConstant.STATUS_CODE] = statusConstant.BAD_REQUEST;
            response[statusConstant.STATUS_MESSAGE] = statusConstant.s118 + status;
            res.end(JSON.stringify(response));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = response[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);

    });

}
exports.getFeedbackList = function(req, res, next) {
    var response = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getFeedbackList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = req.body.userId;
	console.log("getFeedbackList- started");
	appUserDao.getFeedbackList(req, function(data, status) {
	console.log("sSSSSSSSSSSSSSSSSSSSSS",data,status)
	    if (data != null) {
	        if (data == "success" ||(data.info !=undefined && data.info.affectedRows != undefined)) {
	            response[statusConstant.STATUS_CODE] = "300";
	            response[statusConstant.RESULT] = status;
	
	            response[statusConstant.STATUS_MESSAGE] = "Fetched FeedbackList Succesfully";
	            res.end(JSON.stringify(response));
	        } else if (data == statusConstant.s104 || data == statusConstant.s105) {
	            response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
	            response[statusConstant.STATUS_MESSAGE] = data;
	            res.end(JSON.stringify(response));
	        } else if (data == statusConstant.s106) {
	
	            response[statusConstant.STATUS_CODE] = statusConstant.UNAUTHORIZED;
	            response[statusConstant.STATUS_MESSAGE] = data;
	            res.end(JSON.stringify(response));
	        } else {
	            response[statusConstant.STATUS_CODE] = statusConstant.OK;
	            response[statusConstant.STATUS_MESSAGE] = statusConstant.s110;
	            res.end(JSON.stringify(response));
	        }
	    } else {
	        response[statusConstant.STATUS_CODE] = statusConstant.BAD_REQUEST;
	        response[statusConstant.STATUS_MESSAGE] = statusConstant.s118 + status;
	        res.end(JSON.stringify(response));
	    }
	
	    apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
	    apiReq.resultCode = response[statusConstant.STATUS_CODE];
	    logsDao.insertAPILog(apiReq);
	
	});
	
	}
