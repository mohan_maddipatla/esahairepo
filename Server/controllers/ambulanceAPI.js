var url = require('url');
var ambulanceAPIService = require('./ambulanceAPIService');
var logger = require("../logging/logger.js");
module.exports.getDriversListByAmbulanceId = function getDriversListByAmbulanceId (req, res, next) {
	console.log("getDriversListByAmbulanceId service started");
  ambulanceAPIService.getDriversListByAmbulanceId(req, res, next);
  logger.info("getDriversListByAmbulanceId service ended");
};
module.exports.updateAmbulanceStatus = function updateAmbulanceStatus (req, res, next) {
	console.log("updateAmbulanceStatus service started");
  ambulanceAPIService.updateAmbulanceStatus(req, res, next);
  logger.info("updateAmbulanceStatus service ended");
};

module.exports.updateDeviceToken = function updateDeviceToken (req, res, next) {
	console.log("updateDeviceToken service started");
  ambulanceAPIService.updateDeviceToken(req, res, next);
  logger.info("updateDeviceToken service ended");
};
module.exports.getEmergencyTypeList = function getEmergencyTypeList (req, res, next) {
	console.log("getEmergencyTypeList service started");
  ambulanceAPIService.getEmergencyTypeList(req, res, next);
  logger.info("getEmergencyTypeList service ended");
};

module.exports.getAmbulanceDriverStatusList = function getAmbulanceDriverStatusList (req, res, next) {
	console.log("getAmbulanceDriverStatusList service started");
  ambulanceAPIService.getAmbulanceDriverStatusList(req, res, next);
  logger.info("getAmbulanceDriverStatusList service ended");
};

module.exports.getStatusList = function getStatusList (req, res, next) {
  console.log("getStatusList service started");
  ambulanceAPIService.getStatusList(req, res, next);
  logger.info("getStatusList service ended");
};
module.exports.trackAmbulance = function trackAmbulance (req, res, next) {
  console.log("trackAmbulance service started");
  ambulanceAPIService.trackAmbulance(req, res, next);
  logger.info("trackAmbulance service ended");
};

module.exports.getAmbulanceTrackDetails = function getAmbulanceTrackDetails (req, res, next) {
  console.log("getAmbulanceTrackDetails service started");
  ambulanceAPIService.getAmbulanceTrackDetails(req, res, next);
  logger.info("getAmbulanceTrackDetails service ended");
};

module.exports.updateAmbulanceDriverStatus = function updateAmbulanceDriverStatus (req, res, next) {
	  logger.info("updateAmbulanceDriverStatus service started");
	  ambulanceAPIService.updateAmbulanceDriverStatus(req, res, next);
	  logger.info("updateAmbulanceDriverStatus service ended");
	};

module.exports.getAmbulanceTypes = function getAmbulanceTypes (req, res, next) {
  logger.info("getAmbulanceTypes service started");
  ambulanceAPIService.getAmbulanceTypes(req, res, next);
  logger.info("getAmbulanceTypes service ended");
};
module.exports.getAmbulanceProfile = function getAmbulanceProfile (req, res, next) {
    logger.info("getAmbulanceProfile service started");
    ambulanceAPIService.getAmbulanceProfile(req, res, next);
    logger.info("getAmbulanceProfile service ended");
  };
