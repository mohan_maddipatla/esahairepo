var url = require('url');
var logger = require("../logging/logger.js");
var promoService = require("./promoBannerService.js");

module.exports.uploadBanner = function uploadBanner (req, res, next) {
        logger.info("uploadBanner service started");
        promoService.uploadBanner(req, res, next);
        logger.info("uploadBanner service ended");
};
module.exports.getPromos = function getPromos (req, res, next) {
        logger.info("getPromos service started");
        promoService.getPromos(req, res, next);
        logger.info("getPromos service ended");
};


module.exports.updateBanner = function updateBanner (req, res, next) {
        logger.info("updateBanner service started");
        promoService.updateBanner(req, res, next);
        logger.info("updateBanner service ended");
}

