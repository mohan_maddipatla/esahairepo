var url = require('url');
var excel = require('./excelService');
var logger = require("../logging/logger.js");

module.exports.uploadExcel = function uploadExcel(req, res, next) {
    logger.info("uploadExcel service started");
    excel.uploadExcel(req, res, next);
    logger.info("createBooking service ended");
};

