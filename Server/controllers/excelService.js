'use strict';
var tableOperationsDao = require("../Dao/tableOperationsDao.js");
var logsDao = require("../Dao/logsDao.js");
var connections = require("../Util/dbConnection.js");
var constants = require("../Util/commonConstants.js");
var logsDao = require("../Dao/logsDao.js");
var statusConstant = require("../Util/statusConstants.json");
var logger = require('../logging/logger.js');
var excelDao = require('../Dao/excelDao.js')


exports.uploadExcel = function(req, res) {
    res.setTimeout(0);
    var respJson = {};
    excelDao.uploadExcel(req, function(data, status) {

        if (data == "success") {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "File Uploaded successfully";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "noAmbulance") {
            respJson['statusCode'] = "444";
            respJson['statusMessage'] = "No Ambulances Found";
            res.end(JSON.stringify(respJson));
        }else if (data == 'upload not completed succesfully' && status != undefined && status != null) {

            respJson['statusCode'] = "446";

            respJson['statusMessage'] = "File uploaded but " + status + " record(s) were not inserted because there is no operation specified";

            res.end(JSON.stringify(respJson));

        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

    })
}

