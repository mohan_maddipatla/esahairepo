'use strict';
var loginDao = require("../Dao/loginDao.js");
var constants = require("../Util/commonConstants.js");
var appUserDao = require("../Dao/appUserDao.js");
var logsDao = require("../Dao/logsDao.js");
var serverPort = 3001;
var serverUrl = "http://localhost:";
var fs = require("fs");
var commonMethod = require("../Util/commonMethod.js");
var swaggerFilePathRead = "./api/swagger.json";
var swaggerFilePathWrite = "./api/swaggerAPI.json";
var swaggernewFile = "./api/newService.json";
var mysqlSeriveJson = "./api/mysqlService.json";
var httpSeriveJson = "./api/httpService.json";
var mobileApiJson= "./api/mobileApi.json";
var appUserJson = "./api/appUser.json";
var apiDocJson = "./api/apiDoc.json";
var systemServiceJson = "./api/system.json";
var definitionJson = "./api/definitions.json";
var datetime = require('node-datetime');
var util = require('util');
var logger = require("../logging/logger.js");
exports.appUserLogin = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'appUserLogin';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	loginDao.appUserLogin(request, function(data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		var response = data;
		apiReq.resultCode = response.statusCode;
		logsDao.insertAPILog(apiReq);
		res.send(response);
	})
};

exports.generateOTPAmbulance = function(req, res) {
    var respJson = {};
    var path = req.path.split("/");
   var  servicePath = path[path.length-1];
    var request = req.body;
    request.servicePath = servicePath;
    var apiReq = {};
    apiReq.apiPath = 'generateOTPAmbulance';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = request.userId;
    apiReq.evd = request.EVD;
    loginDao.generateOTPAmbulance(request, function(data) {
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        var response = JSON.parse(data);
        apiReq.resultCode = response.statusCode;
        logsDao.insertAPILog(apiReq);
        res.send(response);
    })
}

exports.verifyAmbulanceOTP = function(req, res) {
    var respJson = {};
    var request = req.body;
    var apiReq = {};
    apiReq.apiPath = 'verifyAmbulanceOTP';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = request.userId;
    apiReq.evd = request.EVD;
    loginDao.verifyAmbulanceOTP(request, function(data) {
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        var response = JSON.parse(data);
        apiReq.resultCode = response.statusCode;
        logsDao.insertAPILog(apiReq);
	logger.info("RESONSE IN VERIFY OTP",response)
        res.send(response);
    })
}
exports.driverLogin = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'driverLogin'
		apiReq.reqTime = datetime.create().format('Y-m-d H:M:S')
		loginDao.driverLogin(request, function(data) {
			apiReq.resTime = datetime.create().format('Y-m-d H:M:S')
			var response = JSON.parse(data);
			apiReq.userId = response.userId;
			apiReq.resultCode = response.statusCode;
			if (response.userId != undefined) {
				logsDao.insertAPILog(apiReq);
			}
			res.send(response);

		})
};

exports.driverLogout = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'driverLogout'
		apiReq.reqTime = datetime.create().format('Y-m-d H:M:S')
		loginDao.driverLogout(request, function(data) {
			apiReq.resTime = datetime.create().format('Y-m-d H:M:S')
			var response = JSON.parse(data);
			apiReq.userId = response.userId;
			apiReq.resultCode = response.statusCode;
			if (response.userId != undefined) {
				logsDao.insertAPILog(apiReq);
			}
			res.send(response);

		})
};
exports.customerGenerateOTP = function(req, res) {
    var respJson = {};
    var path = req.path.split("/");
   var  servicePath = path[path.length-1];
    var request = req.body;
    request.servicePath = servicePath;
    var apiReq = {};
    apiReq.apiPath = 'customerGenerateOTP';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = request.userId;
    apiReq.evd = request.EVD;
    loginDao.customerGenerateOTP(req, function(data) {
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        var response = JSON.parse(data);
        apiReq.resultCode = response.statusCode;
        logsDao.insertAPILog(apiReq);
        res.send(response);
    })
}

exports.verifyCustomerOTP = function(req, res) {
    var respJson = {};
    var request = req.body;
    var apiReq = {};
    apiReq.apiPath = 'verifyCustomerOTP';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = request.userId;
    apiReq.evd = request.EVD;
    loginDao.verifyCustomerOTP(request, function(data) {
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        var response = JSON.parse(data);;
        apiReq.resultCode = response.statusCode;
        logsDao.insertAPILog(apiReq);
	logger.info("RESONSE IN VERIFY OTP",response)
        res.send(response);
    })
}
exports.login = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'login'
		apiReq.reqTime = datetime.create().format('Y-m-d H:M:S')
		loginDao.login(request, function(data) {
			apiReq.resTime = datetime.create().format('Y-m-d H:M:S')
			var response = JSON.parse(data);
			apiReq.userId = response.userId;
			apiReq.resultCode = response.statusCode;
			if (response.userId != undefined) {
				logsDao.insertAPILog(apiReq);
			}
			res.send(response);

		})
};
exports.logout = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'logout'
		apiReq.reqTime = datetime.create().format('Y-m-d H:M:S')
		loginDao.logout(request, function(data) {
			apiReq.resTime = datetime.create().format('Y-m-d H:M:S')
			var response = JSON.parse(data);
			apiReq.userId = response.userId;
			apiReq.resultCode = response.statusCode;
			if (response.userId != undefined) {
				logsDao.insertAPILog(apiReq);
			}
			res.send(response);

		})
};

exports.getDashboardDetails = function(req, res) {

    var respJson = {};
    var request = req.body;
    var apiReq = {};
    apiReq.apiPath = 'getDashboardDetails';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = request.userId;
    loginDao.getDashboardDetails(request, function(data) {
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        var response = JSON.parse(data);
        apiReq.resultCode = response.statusCode;
        logsDao.insertAPILog(apiReq);
        res.end(JSON.stringify(response));

	})
};

exports.getApiDocs = function(req, res, next) {
	var fs = require("fs");
	var response = {};
	var data = fs.readFileSync(swaggerFilePathRead, "utf8");
	var tags = [];
	var tagObjLogin = {};
	tagObjLogin.name = "Login";
	tagObjLogin.description = "Service about Login";
	tags.push(tagObjLogin);
	var mysqlList = "";
	var emailList = "";
	var httpList = "";
	var appDefalutList = "";
	var appDeviceList = "";
	var apiDocList = "";
	var systemList = "";
	var remainingList = "";
	var difinitionList = "";
	var pathsList = "";
	var soapList = "";
	var callbackApiList = "";
	var mobileApiList = "";
	commonMethod.getSetServicesList(function(response) {


		if (response != null && response.length > 0) {
			var arrayRespone = response;
			for (var i = 0; i < arrayRespone.length; i++) {
				var obj = arrayRespone[i].name;
				obj = obj.replace(/ /g, '');


				var tagObj = {};
				 if (arrayRespone[i].service_type_id == constants.httpServiceTypeId) {
					console.log("In service ID " + arrayRespone[i].service_type_id);
					var name = arrayRespone[i].name.trim();
					console.log("UIUOUOU ", name)
					var servicejsonFile = "./api/customApi/" + "http_" + arrayRespone[i].id + ".json";
					if (fs.existsSync(servicejsonFile)) {
						console.log("OPOOOOOOOOOO ",name)
						var testFiles = fs.readFileSync(servicejsonFile, "utf8");
						if (testFiles != undefined && testFiles.length > 0) {
							console.log("OIOIOI ", testFiles)
							if (testFiles.indexOf("paths") != -1) {
								console.log("PPPPPPPPPP ")
								var pathsJson = JSON.parse(testFiles).paths;
								var definitionsDyna = JSON.parse(testFiles).definitions;
								if (testFiles.indexOf("definitions") != -1) {
									if (definitionsDyna != undefined) {
										for (var defName in definitionsDyna) {
											if (defName.indexOf("{") == -1 || defName.indexOf("}")) {
												var def = '"' + defName + '":' + JSON.stringify(definitionsDyna[defName]);
												difinitionList = difinitionList + def + ",";
											}
										}
									}
								}
								if (pathsJson != undefined) {
									console.log("PPPPPPPPPPPPP ", pathsJson)
									for (var attributename in pathsJson) {
										if (attributename.indexOf("{") == -1 || attributename.indexOf("}")) {
											var httpSerive = fs.readFileSync(httpSeriveJson, "utf8");
											httpSerive = httpSerive.replace(/{port}/g, commonConstants.portName);
											httpSerive = httpSerive.replace(/{path}/g, obj.toString());
											httpSerive = httpSerive.replace(/{url}/g, attributename);
											httpSerive = httpSerive.replace(/{tag}/g, obj.toString());
											var methodValue = pathsJson[attributename];
											if (methodValue != undefined) {
												for (var methodName in methodValue) {
													httpSerive = httpSerive.replace(/{method}/g, methodName.toString());
													var methodNameValue = methodValue[methodName];
													if (methodNameValue != undefined && methodNameValue["parameters"] != undefined) {
														var params =	[{

															"in": "header",
															"name": "token",
															"description": "your http service summary",
															"required": true,
															"type": "string"},{ "in": "header",
																"name": "userId",
																"description": "your http service summary",
																"required": true,
																"type": "string"}];
														if(methodNameValue["parameters"] instanceof Array){
															console.log("lenthg  ",methodNameValue["parameters"].length)
															for(var pr=0;pr<methodNameValue["parameters"].length;){
																params.push(methodNameValue["parameters"][pr]);
																pr++;
															}
														}else{
															params.push(methodNameValue["parameters"]);
														}
														/*for(var i=0;i<methodNameValue["parameters"].length;i++){
															params.push(methodNameValue["parameters"][i]);
														}*/
														//params.push(methodNameValue["parameters"]);
														httpSerive = httpSerive.replace(/{params}/g, JSON.stringify(params));
														console.log("HEOIHRH ", httpSerive);
													}
												}
											}
											pathsList = pathsList + httpSerive;
										}
									}
								}
							}
						}
					}
				}else if (arrayRespone[i].service_type_id == constants.mobileApiSeriveTypeId) {
					var mobileService = fs.readFileSync(mobileApiJson, "utf8");
					mobileService = mobileService.replace(/{port}/g, commonConstants.portName);
									mobileService = mobileService.replace(/{tag}/g, obj.toString());
														mobileApiList = mobileApiList + mobileService;
																		}  else if (arrayRespone[i].isDefault == 'true') {
					if (arrayRespone[i].name == 'app_user') {
						var appUser = fs.readFileSync(appUserJson, "utf8");
						appUser = appUser.replace(/{port}/g, commonConstants.portName);
						appUser = appUser.replace(/{path}/g, obj.toString());
						appUser = appUser.replace(/{tag}/g, obj.toString());
						appDefalutList = appDefalutList + appUser;
					}
					
					if (arrayRespone[i].name == 'api_docs') {
						var apiDoc = fs.readFileSync(apiDocJson, "utf8");
						
						apiDoc = apiDoc.replace(/{path}/g, obj.toString());
						apiDoc = apiDoc.replace(/{tag}/g, obj.toString());
						apiDocList = apiDocList + apiDoc;
					}
					if (arrayRespone[i].name == 'system') {
						var systemDoc = fs.readFileSync(systemServiceJson, "utf8");
						systemDoc = systemDoc.replace(/{port}/g, commonConstants.portName);
						systemDoc = systemDoc.replace(/{tag}/g, obj.toString());
						systemList = systemList + systemDoc;
					}

				} else {
					var newService = fs.readFileSync(swaggernewFile, "utf8");
					newService = newService.replace(/{port}/g, commonConstants.portName);
					newService = newService.replace(/{path}/g, obj.toString());
					newService = newService.replace(/{tag}/g, obj.toString());
					remainingList = remainingList + newService;
				}
				tagObj.name = obj.toString();
				tagObj.description = " Service about " + arrayRespone[i].type_name;
				tags.push(tagObj);
			}
		} else {

		}

		setTimeout(function() {
			var definitions = fs.readFileSync(definitionJson, "utf8");
			difinitionList = difinitionList + definitions;
			var result = data.replace(/{tags}/g, JSON.stringify(tags));
			result = result.replace(/{definitions}/g, difinitionList.toString());
			result = result.replace(/{mysqlService}/g, mysqlList.toString());
			result = result.replace(/{soapService}/g, soapList.toString());
			result = result.replace(/{emailService}/g, emailList.toString());
			result = result.replace(/{newService}/g, remainingList.toString());
			result = result.replace(/{httpService}/g, pathsList.toString());
			result = result.replace(/{defaultService}/g, appDefalutList.toString());
			result = result.replace(/{apiDoc}/g, apiDocList.toString());
			result = result.replace(/{callbackService}/g, callbackApiList.toString());
			result = result.replace(/{mobileService}/g, mobileApiList.toString());
			result = result.replace(/{systemService}/g, systemList.toString());

			var write = fs.writeFileSync(swaggerFilePathWrite, result, "utf8");
			res.end(result);
		}, 10000);

	});
};

var wsdlParsing = function(service, fn) {
	//console.log("service id ",serviceId)
	var wsdlPathFunction = commonMethod.syncDatasoap(service.id, function(path) {
		//      console.log("WSDL PATH ",wsdlPath)
		var wsdlPath = path[0].fieldValue;
	logger.info("WSDL PATH",wsdlPath);
		var urls = [];
		//var parser = require('xml2json');
		var parser = require('xml2js').Parser();
		var http = require('http');
		var xml = null;
		var server = null;
		var request = require('request');
		request({
			url: wsdlPath,
			method: 'GET',
		}, function(error, response, body) {
			if (error) {
				console.log("ERROR ", error);
				//  fn(error);
			} else {
			//	logger.info("BOYD ", body);
				xml = body;
				parser.parseString(xml, function(err, json) {
			//	logger.info("ERRRRERER ", err);
			//	logger.info("JOSN ", json);
		          //  console.log('Complete result:');
		          //  console.log(util.inspect(json, {depth: null})); //Work
		           // console.log('Try to access element:');
		          //  console.log(json.smil.body); //Work
		          //  console.log(json.smil.body.update); //Undefined
				//var json = parser.toJson(xml);
			//	json = JSON.parse(json);
				/* server = http.createServer(function(request,response) {
                          response.end("404: Not Found: " + request.url);
                      });
                      server.listen(3333);*/
		var json = json;
		//	logger.info("json", json);
				var def = json["wsdl:definitions"];
				//console.log("HHJHJHJ ",def)
				var parameters = "";
				if (def != undefined) {
					var bindings = def['wsdl:binding'];
					if (bindings.length > 0) {
						for (var i in bindings) {
							var bind = bindings[i];
							var bindName = bind['$'];
							//console.log("I ",i ,"Name ",bind['$']);
						//	if (bindName.name != undefined && (bindName.name.indexOf("HttpGet") > -1 || bindName.name.indexOf("HttpPost") > -1)) {
					if (bindName.name != undefined && (bindName.name.indexOf("HttpPost") > -1)) {	
								if (bind['wsdl:operation'] != undefined) {
									var op = [];
									var ops = bind['wsdl:operation'];
									//ops=ops['$'];
									if (ops != undefined && ops instanceof Array) {
										op = ops;
									} else if (ops != undefined && !(ops instanceof Array)) {
										op.push(ops);
									}
									//console.log("OPERATION ", op);

									if (op.length > 0) {
										for (var j in op) {
											var operation = op[j];
											operation = operation['$'];
											var urlToAppend = {};
											urlToAppend['url'] = operation.name;
										/*	if (bindName.name.indexOf("HttpGet") > -1) {
												urlToAppend['method'] = "get";
											} else*/ if (bindName.name.indexOf("HttpPost") > -1) {
												urlToAppend['method'] = "post";
											}
										//	console.log("URL TO APPEND ", urlToAppend)
											//for input parameters
											
											if (def["wsdl:types"] != undefined) {
												var types = def["wsdl:types"];
												for(var type in types){
													var schema = types[type]["s:schema"];
													//console.log("in types ", schema.targetNamespace);
												
													for(var sch in schema){
														
														var targetUrl = schema[sch]['$'].targetNamespace;
													
												
												
												// targetUrl += "/"+endPath;
												//  console.log("targetUrl ",targetUrl);
												// console.log("schema ",schema["s:element"])
												if (schema[sch]["s:element"] != undefined) {

													console.log("ELEMENTS ", elements);
													var elements = [];
													var element = schema[sch]["s:element"];
													if (elements != undefined && elements instanceof Array) {
														elements = element;
													} else if (element != undefined && !(element instanceof Array)) {
														elements.push(element);
													}

													for (var i in elements) {
														var elmnt = elements[i];
														if (operation.name == elmnt['$'].name && elmnt['s:complexType'] != undefined) {
															var compType = elmnt['s:complexType'];
															for(var cmtype in compType){
																var comp = compType[cmtype]['s:sequence'];
																//seq = compType['s:sequence']
															
															if (comp != undefined) {
																var seq = comp;
																var parameters = "";
																for (var j in seq) {
																	var seqs = seq[j]['s:element'];

																	if (seqs != undefined && seqs.length > 0) {
																		for (var k in seqs) {
																			parameters += seqs[k]['$'].name;
																			if (k == seqs.length - 1) {
																				parameters += ""
																			} else {
																				parameters += ","
																			}
																		}
																	} else {
																		parameters += seqs['$'].name;
																	}

																}
																urlToAppend['parameters'] = parameters;
																//  console.log("url append Paramaerts ",urlToAppend)
															}
														}
														}
													}

												}
											}
												}
											}

											urls.push(urlToAppend);
											//console.log("URL S ",urls)

										}
									} else {

									}
								}
							}
						}
						// console.log("URL S ",urls)
						fn(urls, service);
					}
				}
				});
			}

		});

	});

}
