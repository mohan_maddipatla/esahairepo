'use strict';
var serviceDao = require("../Dao/serviceDao.js");

exports.connectToMysql = function(req, res, next) {
  var response ={};
  console.log("connectToMysql- started");
		serviceDao.connectToMysql(req,function(data,status){
			console.log("Data ",data)
			if(data!=null){
					response['statusCode'] = "200";
 					response['statusMessage'] = "successfully created";
	 				res.end(JSON.stringify(response));

			}else if(data == "Invalid token"){
					response['statusCode'] = "500";
					response['statusMessage'] = data;
	 				res.end(JSON.stringify(response));
				}else if(data == "Unauthorized"){

					response['statusCode'] = "401";
					response['statusMessage'] = data;
	 				res.end(JSON.stringify(response));
			}else{
				response['statusCode'] = "500";
				response['statusMessage'] = status;
 				res.end(JSON.stringify(response));
			}
				
		})
}
