var bookingDao = require("../Dao/bookingDao.js");
var datetime = require('node-datetime');
var logger = require('../logging/logger.js');
var statusConstant = require("../Util/statusConstants.json");
var logsDao = require("../Dao/logsDao.js");
var commonConstants = require("../Util/commonConstants.js");
exports.createBooking = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'createBooking';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.createBooking(req, function(data, status) {
        logger.info("createBooking ", data);
logger.info("Response",status);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "your request saved successfully";
            respJson['responseData'] = status[0];
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "You are unauthorized user";
            res.end(JSON.stringify(respJson));
        } else if (data == "onTrip") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "You previous booking request '"+status+"' is inprogress,  Please contact command centre for any help";
             console.log("ON TRIP 4111  ",respJson);
            res.end(JSON.stringify(respJson));
        }else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.assignBooking = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'assignBooking';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.assignBooking(req, function(data, status) {
        logger.info("assignBooking ", data);
	console.log("assignBooking ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Booking assigned successfully";
	    respJson['centreNumber'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "You are unauthorized user";
            res.end(JSON.stringify(respJson));
        }else if (data == "alreadyBooked") {
            respJson['statusMessage'] = "Other Ambulance has already accepted";
            respJson['statusCode'] = "415";
            res.end(JSON.stringify(respJson));
        }else if(data == "closed"){
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "You can't assign as the trip is completed";
            res.end(JSON.stringify(respJson));
        }else if(data == "cancelled"){
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "This Emergency has been cancelled";
            res.end(JSON.stringify(respJson));
        }else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.updateBooking = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'updateBooking';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.updateBooking(req, function(data, status) {
        logger.info("updateBooking ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Booking updated successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.getAmbulanceList = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getAmbulanceList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getAmbulanceList(req, function(data, status) {
        logger.info("getAmbulanceList ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Ambulance List fetched successfully";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.getNearByAmbulanceListByHospitalAndEmergeType = function(req, res) {
	  //  console.log("PPPPPPPPPPPPP ",req);
	    var respJson = {};
	    var apiReq = {}; //api-log
	    apiReq.apiPath = 'getNearByAmbulanceListByHospitalAndEmergeType';
	    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	    apiReq.userId = req.body.userId;
	    bookingDao.getNearByAmbulanceListSpecific(req, function(data, status) {
	        logger.info("getNearByAmbulanceListByHospitalAndEmergeType ", data);
	        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
	        console.log("LIST SIZE ",status.length);
		    respJson['statusCode'] = "300";
	            respJson['statusMessage'] = "Ambulance List fetched successfully";
	            respJson['responseData'] = status;
	            res.end(JSON.stringify(respJson));
	        } else if (data == "Invalid token" || data == "No User") {
	            respJson['statusCode'] = "204";
	            respJson['statusMessage'] = data;
	            res.end(JSON.stringify(respJson));
	        } else if (data == "Unauthorized") {
	            respJson['statusCode'] = "401";
	            respJson['statusMessage'] = data;
	            res.end(JSON.stringify(respJson));
	        } else if(data == "noAmbulances"){
	             respJson['statusCode'] = "303";
	            respJson['statusMessage'] = "No ambulances found";
	            res.end(JSON.stringify(respJson));
	        }else {
	            console.log("PPPPPPPPPPP ", status);
	            respJson['statusCode'] = "500";
	            respJson['statusMessage'] = status;
	            res.end(JSON.stringify(respJson));
	        }

	        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
	        apiReq.resultCode = res[statusConstant.STATUS_CODE];
	        logsDao.insertAPILog(apiReq);
	    });
	}
exports.getNearByAmbulanceList = function(req, res) {
  //  console.log("PPPPPPPPPPPPP ",req);
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getNearByAmbulanceList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getNearByAmbulanceList(req, function(data, status) {
        logger.info("getNearByAmbulanceList ", data);
	console.log("OIOI ",data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
        console.log("LIST SIZE ",status.length);
	    respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Ambulance List fetched successfully";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if(data == "noAmbulances"){
             respJson['statusCode'] = "303";
            respJson['statusMessage'] = "No ambulances found";
            res.end(JSON.stringify(respJson));
        }else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.getNearByHospitalList = function(req, res) {
  //  console.log("PPPPPPPPPPPPP ",req);
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getNearByHospitalList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getNearByHospitalList(req, function(data, status) {
        //logger.info("getNearByHospitalList ", status);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Hospital List fetched successfully";
            respJson['responseData'] = status.sort(function(a, b) {
//    return parseInt(a.distance) > parseInt(b.distance) ? 1 : (parseInt(a.distance) < parseInt(b.distance) ? -1 : 0);
return parseFloat(a.distance) > parseFloat(b.distance) ? 1 : (parseFloat(a.distance) < parseFloat(b.distance) ? -1 : 0);
}).slice(0,commonConstants.numberOfNearByHospitals);
	   // logger.info("HOSPITAL LIST ",respJson['responseData']);
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        }else if(data == "noHospitals"){
             respJson['statusCode'] = "303";
            respJson['statusMessage'] = "No hospitals found";
            res.end(JSON.stringify(respJson));
        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.getEmergencyContactsForCustomer = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getEmergencyContactsForCustomer';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getEmergencyContactsForCustomer(req, function(data, status) {
        logger.info("getEmergencyContactsForCustomer ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Emergency Contacts fetched successfully";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.updateCustomerProfile = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'updateCustomerProfile';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.updateCustomerProfile(req, function(data, status) {
        logger.info("updateCustomerProfile ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "updated successfully";
           // respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.cancelBooking = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'cancelBooking';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.cancelBooking(req, function(data, status) {
        logger.info("cancelBooking ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "booking cancelled";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "errPN") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "cancelled but failed to send pn";
            res.end(JSON.stringify(respJson));
        }else if (data == "errAmbFree") {
            respJson['statusCode'] = "412";
            respJson['statusMessage'] = "cancelled but failed to free ambulance "+status;
            res.end(JSON.stringify(respJson));
        }else if(data == "noTrack"){
            respJson['statusCode'] = "413";
            respJson['statusMessage'] = "cancelled but no details found to send pn"+status;
            res.end(JSON.stringify(respJson));
        }else if (data == "closed") {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "You can't cancel as the trip completed";
            res.end(JSON.stringify(respJson));
        }else if(data == "alreadyCancelled"){
        	respJson['statusCode'] = "413";
            respJson['statusMessage'] = "Your booking is already cancelled";
            res.end(JSON.stringify(respJson));

	}else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.getTripList = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getTripList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getTripList(req, function(data, status) {
        logger.info("getTripList ",status);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.getCustomerprofile = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getCustomerprofile';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getCustomerprofile(req, function(data, status) {
        logger.info("getCustomerprofile ", status);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.getTripDetails  = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getTripDetails';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getTripDetails(req, function(data, status) {
        logger.info("getTripDetails ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
} 
exports.addEmergencyContactForCustomer  = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'addEmergencyContactForCustomer';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.addEmergencyContactForCustomer(req, function(data, status) {
        logger.info("addEmergencyContactForCustomer ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "added successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.transferBooking = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'transferBooking';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.transferBooking(req, function(data, status) {
        logger.info("transferBooking ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Booking transfered successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] ="You are unauthorized user";
            res.end(JSON.stringify(respJson));
        }else if(data=="cantTransfer"){
            respJson['statusCode'] = "402";
            respJson['statusMessage'] = "Can not reject, Either customer has cancelled or already transfered to others";
            res.end(JSON.stringify(respJson));
        }else if(data=="closed"){
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "You can't transfer the completed trip";
            res.end(JSON.stringify(respJson));
        }else if(data=="cancelled"){
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "This emergency has been cancelled";
            res.end(JSON.stringify(respJson));
        }    else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.updateEmergencyContactForCustomer  = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'updateEmergencyContactForCustomer';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.updateEmergencyContactForCustomer(req, function(data, status) {
        logger.info("updateEmergencyContactForCustomer ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Updated successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.deleteEmergencyContactForCustomer  = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'deleteEmergencyContactForCustomer';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.deleteEmergencyContactForCustomer(req, function(data, status) {
        logger.info("deleteEmergencyContactForCustomer ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Deleted successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.insertBloodRequest = function(req, res){
    var respJson = {};
    var apiReq = {};
    apiReq.apiPath = 'insertBloodRequest';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.insertBloodRequest(req, function(data, status) {
        logger.info("insertBloodRequest ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Request submitted successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else if (data == "noCustomers") {
            respJson['statusCode'] = "412";
            respJson['statusMessage'] = "No customers found with required blood group";
            res.end(JSON.stringify(respJson));
        }else if (data == "errPn") {
            respJson['statusCode'] = "413";
            respJson['statusMessage'] = "Error occured while sending pn to customers";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.transferByCommandCentre = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'transferByCommandCentre';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.transferByCommandCentre(req, function(data, status) {
        logger.info("transferByCommandCentre ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Booking transfered successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        }else if(data=="cantTransfer"){
            respJson['statusCode'] = "402";
            respJson['statusMessage'] = "Can not reject, Either customer has cancelled or already transfered to others";
            res.end(JSON.stringify(respJson));
        }else if (data == "alreadyBooked") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "Other Ambulance has already accepted";
            res.end(JSON.stringify(respJson));
        }else if(data == "cancelled"){
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "This Emergency has been cancelled";
            res.end(JSON.stringify(respJson));
        }else if(data == "closed"){
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "You can't transfer as the trip is closed";
            res.end(JSON.stringify(respJson));
        } else if(data == "chooseOther"){
            respJson['statusCode'] = "412";
            respJson['statusMessage'] = "Current ambulance is already associated with this emergency,Please choose other";
            res.end(JSON.stringify(respJson));
        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.getNearByAmbulanceListSpecificType = function(req, res) {
	  //  console.log("PPPPPPPPPPPPP ",req);
	    var respJson = {};
	    var apiReq = {}; //api-log
	    apiReq.apiPath = 'getNearByAmbulanceListSpecificType';
	    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	    apiReq.userId = req.body.userId;
	    bookingDao.getNearByAmbulanceListSpecificType(req, function(data, status) {
	        logger.info("getNearByAmbulanceListSpecificType ", data);
	        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
	        console.log("LIST SIZE ",status.length);
		    respJson['statusCode'] = "300";
	            respJson['statusMessage'] = "Ambulance List fetched successfully";
	            respJson['responseData'] = status.sort(function(a, b) {
	            	return parseFloat(a.distance) > parseFloat(b.distance) ? 1 : (parseFloat(a.distance) < parseFloat(b.distance) ? -1 : 0);
	            });
	            res.end(JSON.stringify(respJson));
	        } else if (data == "Invalid token" || data == "No User") {
	            respJson['statusCode'] = "204";
	            respJson['statusMessage'] = data;
	            res.end(JSON.stringify(respJson));
	        } else if (data == "Unauthorized") {
	            respJson['statusCode'] = "401";
	            respJson['statusMessage'] = "You are unauthorized user";
	            res.end(JSON.stringify(respJson));
	        } else if(data == "noAmbulances"){
	             respJson['statusCode'] = "303";
	            respJson['statusMessage'] = "No ambulances found";
	            res.end(JSON.stringify(respJson));
	        }else {
	            console.log("PPPPPPPPPPP ", status);
	            respJson['statusCode'] = "500";
	            respJson['statusMessage'] = status;
	            res.end(JSON.stringify(respJson));
	        }

	        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
	        apiReq.resultCode = res[statusConstant.STATUS_CODE];
	        logsDao.insertAPILog(apiReq);
	    });
	}

exports.createBookingByCommandCentre = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'createBookingByCommandCentre';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.createBookingByCommandCentre(req, function(data, status) {
        logger.info("createBookingByCommandCentre ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "your request saved successfully";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else {
            console.log("PPPPPPPPPPP ", status);
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.assignBookingByCommandCentre = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'assignBookingByCommandCentre';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.assignBookingByCommandCentre(req, function(data, status) {
        logger.info("assignBookingByCommandCentre ", data);
        console.log("assignBooking ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Booking assigned successfully";
	  //  respJson['centreNumber'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        }else if(data == "closed"){
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "You can't assign as the trip is completed";
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        }else if (data == "alreadyBooked") {
            respJson['statusMessage'] = "Other Ambulance has already accepted";
            respJson['statusCode'] = "415";
            res.end(JSON.stringify(respJson));
        }else if(data == "cancelled"){
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "This Emergency has been cancelled";
            res.end(JSON.stringify(respJson));
        }else if(data == "Offline"){
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "This Ambulance is offline, you can't assign";
            res.end(JSON.stringify(respJson));
        }else if(data == "onTrip"){
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = "Selected Ambulane is already on Trip, Please choose other";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.getEmergencies = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getEmergencies';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getEmergencies(req, function(data, status) {
        logger.info("getTripList ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "Unauthorized";
            res.end(JSON.stringify(respJson));
        } else if (data == "invalidInput") {
            respJson['statusCode'] = "410";
            respJson['statusMessage'] = "Missing mandotory fields in your request";
            res.end(JSON.stringify(respJson));
        } else if (data == "noData") {
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "no data found";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}


exports.checkMyBookingStatus = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'checkMyBookingStatus';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.checkMyBookingStatus(req, function(data, status) {
        logger.info("Response :: checkMyBookingStatus ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Booking Data fetched successfully";
            respJson['responseData'] = status;
            logger.info("Response Data :: ",status);
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        }else if (data == "noOneAccepted") {
            respJson['statusMessage'] = "Oops!! No Ambulance accepted your request";
            respJson['statusCode'] = "413";
            res.end(JSON.stringify(respJson));
        }else if(data == "cancelled"){
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "Your request has cancelled due to some problem";
            res.end(JSON.stringify(respJson));
        }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.getActiveBooking = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getActiveBooking';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    bookingDao.getActiveBooking(req, function(data, status) {
        logger.info("Response :: getActiveBooking ", data);
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Booking Data fetched successfully";
            respJson['responseData'] = status;
            logger.info("Response Data :: ",status);
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token" || data == "No User") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        }else if (data == "noOneAccepted") {
            respJson['statusMessage'] = "Oops!! No Ambulance accepted your request";
            respJson['statusCode'] = "413";
            res.end(JSON.stringify(respJson));
        }else if(data == "cancelled"){
            respJson['statusCode'] = "411";
            respJson['statusMessage'] = "Your request has cancelled due to some problem";
            res.end(JSON.stringify(respJson));
        }else if(data == "noActiveBookings"){
            respJson['statusCode'] = "412";
        respJson['statusMessage'] = "No Actvie bookings";
        res.end(JSON.stringify(respJson));
    }else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
