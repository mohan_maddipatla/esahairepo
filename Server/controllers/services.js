'use strict';

var url = require('url');
var serviceOperations = require('./ServiceOperations');


module.exports.connectToMysql = function connectToMysql (req, res, next) {
	console.log("connectToMysql service started");
  serviceOperations.connectToMysql(req, res, next);
};
module.exports.connectToMongo = function connectToMongo (req, res, next) {
	console.log("connectToMongo service started");
  serviceOperations.connectToMongo(req, res, next);
};
module.exports.createCollection = function createCollection (req, res, next) {
	console.log("createCollection service started");
  serviceOperations.createCollection(req, res, next);
};
module.exports.httpService = function httpService (req, res, next) {
	console.log("httpService service started");
  serviceOperations.httpService(req, res, next);
};