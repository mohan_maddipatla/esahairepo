var promoBannerDao = require("../Dao/promoBannerDao.js");
var datetime = require('node-datetime');
var logger = require('../logging/logger.js');
var statusConstant = require("../Util/statusConstants.json");
var logsDao = require("../Dao/logsDao.js");
exports.uploadBanner = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'uploadBanner';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    promoBannerDao.uploadBanner(req, function(data, status) {
        logger.info("uploadBanner ", data);
        if (data == "success") {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Banner uploaded successfully";
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "You are unauthorized user";
            res.end(JSON.stringify(respJson));
        } else if (data == "errPn") {
            respJson['statusCode'] = "402";
            respJson['statusMessage'] = "Error occured while sending pn to customers";
            res.end(JSON.stringify(respJson));
        }  else if (data == "noCustomerPN") {
            respJson['statusCode'] = "403";
            respJson['statusMessage'] = "No Customer Found to send pn";
            res.end(JSON.stringify(respJson));
        }  else if (data == "invalidFile") {
            respJson['statusCode'] = "404";
            respJson['statusMessage'] = "Invalid Banner";
            res.end(JSON.stringify(respJson));
        }  else if (data == "noFile") {
            respJson['statusCode'] = "405";
            respJson['statusMessage'] = "No Banner Found"; 
            res.end(JSON.stringify(respJson));
        }  else if (data == "invalidInput") {
            respJson['statusCode'] = "406";
            respJson['statusMessage'] = "Missing Mandotory field in request"; 
            res.end(JSON.stringify(respJson));
        } else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.getPromos = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getPromos';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    promoBannerDao.getPromos(req, function(data, status) {
        //logger.info("getPromos ", data,status);
//console.log("get data",data,status);
        if (data == "success") {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Promo Banners fetched successfully";
            respJson['Result'] = status;

            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "You are unauthorized user";
            res.end(JSON.stringify(respJson));
        } else if (data == "errPn") {
            respJson['statusCode'] = "402";
            respJson['statusMessage'] = "Error occured while sending pn to customers";
            res.end(JSON.stringify(respJson));
        }  else if (data == "noCustomerPN") {
            respJson['statusCode'] = "403";
            respJson['statusMessage'] = "No Customer Found to send pn";
            res.end(JSON.stringify(respJson));
        }  else if (data == "invalidFile") {
            respJson['statusCode'] = "404";
            respJson['statusMessage'] = "Invalid Banner";
            res.end(JSON.stringify(respJson));
        }  else if (data == "noFile") {
            respJson['statusCode'] = "405";
            respJson['statusMessage'] = "No Banner Found"; 
            res.end(JSON.stringify(respJson));
        }  else if (data == "invalidInput") {
            respJson['statusCode'] = "406";
            respJson['statusMessage'] = "Missing Mandotory field in request"; 
            res.end(JSON.stringify(respJson));
        } else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
exports.updateBanner = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'updateBanner';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    promoBannerDao.updatePromoBanner(req, function(data, status) {
        logger.info("updatePromoBanner", data,status);
	console.log("get updatePromoBanner",data,status);
        if (data == "success") {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Promo Banner updated successfully";
            respJson['Result'] = status;

            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = "You are unauthorized user";
            res.end(JSON.stringify(respJson));
        } else if (data == "errPn") {
            respJson['statusCode'] = "402";
            respJson['statusMessage'] = "Error occured while sending pn to customers";
            res.end(JSON.stringify(respJson));
        }  else if (data == "noCustomerPN") {
            respJson['statusCode'] = "403";
            respJson['statusMessage'] = "No Customer Found to send pn";
            res.end(JSON.stringify(respJson));
        }  else if (data == "invalidFile") {
            respJson['statusCode'] = "404";
            respJson['statusMessage'] = "Invalid Banner";
            res.end(JSON.stringify(respJson));
        }  else if (data == "noFile") {
            respJson['statusCode'] = "405";
            respJson['statusMessage'] = "No Banner Found";
            res.end(JSON.stringify(respJson));
        }  else if (data == "invalidInput") {
            respJson['statusCode'] = "406";
            respJson['statusMessage'] = "Missing Mandotory field in request";
            res.end(JSON.stringify(respJson));
        } else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}
