var customerDao = require("../Dao/customerDao.js");
var datetime = require('node-datetime');
var logger = require('../logging/logger.js');
var statusConstant = require("../Util/statusConstants.json");
var logsDao = require("../Dao/logsDao.js");
exports.getRateCard = function(req, res) {
    var respJson = {};
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getRateCard';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    customerDao.getRateCard(req, function(data, status) {
        logger.info("getRateCard ", data);
	console.log("OPOPO ",data)
        if (data == "success" || (data.info!=undefined && data.info.affectedRows!=undefined)) {
            respJson['statusCode'] = "300";
            respJson['statusMessage'] = "Rate card fetched successfully";
            respJson['responseData'] = status;
            res.end(JSON.stringify(respJson));
        } else if (data == "Invalid token") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (data == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = status;
            res.end(JSON.stringify(respJson));
        }

        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        apiReq.resultCode = res[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    });
}

exports.updateProfile = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'updateProfile';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.body.userId;
    var response = {};
        customerDao.updateProfile(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = 'Profile Updated Successfully';
		response['imageUrl'] = status;
                res.end(JSON.stringify(response));

            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstantsSTATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }

            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        })
}

exports.addfeedback = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'addfeedback';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.headers.userid;
    var response = {};
        customerDao.addfeedback(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = 'feedback Updated Successfully';
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstantsSTATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        })
}


exports.addPatientInfo = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'addPatientInfo';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.headers.userid;
    var response = {};
        customerDao.addPatientInfo(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = 'Saved successfully';
                res.end(JSON.stringify(response));
            }else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {
                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstantsSTATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        })
}



exports.getPatientInfo = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getPatientInfo';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.headers.userid;
    var response = {};
        customerDao.getPatientInfo(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response['responseData'] = status;
                res.end(JSON.stringify(response));
            }else if(data == "noCustomer"){
                response[statusConstant.STATUS_CODE] = "411";
                response[statusConstant.STATUS_MESSAGE] = 'No data found';
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstantsSTATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        })
}

exports.updatePatientInfo = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'updatePatientInfo';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.headers.userid;
    var response = {};
        customerDao.updatePatientInfo(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = 'updated successfully';
                res.end(JSON.stringify(response));
            }else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {
                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstantsSTATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        })
}


exports.checkCustomerExistsOrNot = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'checkCustomerExistsOrNot';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.headers.userid;
    var response = {};
        customerDao.checkCustomerExistsOrNot(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response[statusConstant.STATUS_MESSAGE] = 'Customer Exists';
                response['responseData'] = status;
                res.end(JSON.stringify(response));
            }else if(data == "noCustomer"){
                response[statusConstant.STATUS_CODE] = "411";
                response[statusConstant.STATUS_MESSAGE] = 'No customer exists with this mobile number';
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            }else if (data == "onTrip") {
                response[statusConstant.STATUS_CODE] ="411";
                response[statusConstant.STATUS_MESSAGE] = "This customer is already on trip";
                res.end(JSON.stringify(response));
            }  else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
        })
}


exports.getAppVersion = function(req, res) {
	var response = {};
        var respJson = {}
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'getAppVersion';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	customerDao.getAppVersion(req, function(flag,data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			response[statusConstant.STATUS_CODE] = "300";
			response['responseData'] = data;
			res.end(JSON.stringify(response));
		} else if (flag == "err") {
			response[statusConstant.STATUS_CODE] = "304";
			response[statusConstant.STATUS_MESSAGE] = data;
			res.end(JSON.stringify(response));
		}else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = response[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.getTermsAndConditions = function(req,res){
    var respJson = {};
    var apiReq = {};
    apiReq.apiPath = 'getTermsAndConditions';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = "";
    apiReq.evd ="";
    customerDao.getTermsAndConditions(req, function(flag,data) {
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            if (flag === "success") {
                    respJson[statusConstant.STATUS_CODE] = "300";
                    respJson[statusConstant.STATUS_MESSAGE] = "terms and conditions fetched successfully";
                    respJson["responseData"] = data;
                    res.end(JSON.stringify(respJson));
            } else if (flag == "err") {
                    respJson[statusConstant.STATUS_CODE] = "304";
                    respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching";
                    res.end(JSON.stringify(respJson));
            }else {
                    respJson['statusCode'] = "500";
                    respJson['statusMessage'] = data;
                    res.end(JSON.stringify(respJson));
            }
            apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
    })
}
exports.getComeToKnowBy= function(req,res){
    var respJson = {};
    var apiReq = {};
    apiReq.apiPath = 'getComeToKnowBy';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = "";
    apiReq.evd ="";
    customerDao.getComeToKnowBy(req, function(flag,data) {
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            if (flag === "success") {
                    respJson[statusConstant.STATUS_CODE] = "300";
                    respJson["responseData"] = data;
                    res.end(JSON.stringify(respJson));
            } else if (flag == "err") {
                    respJson[statusConstant.STATUS_CODE] = "304";
                    respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching";
                    res.end(JSON.stringify(respJson));
            }else {
                    respJson['statusCode'] = "500";
                    respJson['statusMessage'] = data;
                    res.end(JSON.stringify(respJson));
            }
            apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
});
}
exports.getCommandCentreNUmber= function(req,res){
    var respJson = {};
    var apiReq = {};
    apiReq.apiPath = 'getCommandCentreNUmber';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = "";
    apiReq.evd ="";
    customerDao.getCommandCentreNUmber(req, function(flag,data) {
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            if (flag === "success") {
                    respJson[statusConstant.STATUS_CODE] = "300";
                    respJson["responseData"] = data;
                    res.end(JSON.stringify(respJson));
            } else if (flag == "err") {
                    respJson[statusConstant.STATUS_CODE] = "304";
                    respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching";
                    res.end(JSON.stringify(respJson));
            }else {
                    respJson['statusCode'] = "500";
                    respJson['statusMessage'] = data;
                    res.end(JSON.stringify(respJson));
            }
            apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
});
}

exports.getPrivacyPolicy= function(req,res){
    var respJson = {};
    var apiReq = {};
    apiReq.apiPath = 'getPrivacyPolicy';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = "";
    apiReq.evd ="";
    customerDao.getPrivacyPolicy(req, function(flag,data) {
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            if (flag === "success") {
                    respJson[statusConstant.STATUS_CODE] = "300";
                    respJson["responseData"] = data;
                    res.end(JSON.stringify(respJson));
            } else if (flag == "err") {
                    respJson[statusConstant.STATUS_CODE] = "304";
                    respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching";
                    res.end(JSON.stringify(respJson));
            }else {
                    respJson['statusCode'] = "500";
                    respJson['statusMessage'] = data;
                    res.end(JSON.stringify(respJson));
            }
            apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
});
}

exports.getFaqList = function(req, res, next) {
    var apiReq = {}; //api-log
    apiReq.apiPath = 'getFaqList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = req.headers.userid;
    var response = {};
        customerDao.getFaqList(req, function(data, status) {
            if (data === statusConstant.s103) {
                response[statusConstant.STATUS_CODE] = statusConstant.OK;
                response['responseData'] = status;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s104  || data == statusConstant.s105) {
                response[statusConstant.STATUS_CODE] = statusConstant.INVALID_TOKEN;
                response[statusConstant.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else if (data == statusConstant.s106) {

                response[statusConstant.STATUS_CODE] =statusConstant.UNAUTHORIZED;
                response[statusConstants.STATUS_MESSAGE] = data;
                res.end(JSON.stringify(response));
            } else {
                response[statusConstant.STATUS_CODE] = statusConstant.INTERNAL_ERROR;
                response[statusConstant.STATUS_MESSAGE] = status;
                res.end(JSON.stringify(response));
            }
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            apiReq.resultCode = response[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);

        })
}

exports.getCountryList= function(req,res){
    var respJson = {};
    var apiReq = {};
    apiReq.apiPath = 'getCountryList';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = "";
    apiReq.evd ="";
    customerDao.getCountryList(req, function(flag,data) {
            apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
            if (flag === "success") {
                    respJson[statusConstant.STATUS_CODE] = "300";
                    respJson["responseData"] = data;
                    res.end(JSON.stringify(respJson));
            } else if (flag == "err") {
                    respJson[statusConstant.STATUS_CODE] = "304";
                    respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching";
                    res.end(JSON.stringify(respJson));
            }else {
                    respJson['statusCode'] = "500";
                    respJson['statusMessage'] = data;
                    res.end(JSON.stringify(respJson));
            }
            apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
            logsDao.insertAPILog(apiReq);
});
}
