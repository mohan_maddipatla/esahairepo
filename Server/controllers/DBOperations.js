'use strict';

var url = require('url');


var DBOperationsService = require('./DBOperationsService');


module.exports.createTable = function createTable (req, res, next) {
	console.log("createTable service started");
  DBOperationsService.createTable(req, res, next);
  logger.info("createTable service ended");
};
module.exports.alterTable = function alterTable (req, res, next) {
	console.log("alterTable service started");
  DBOperationsService.alterTable(req, res, next);
};
module.exports.deleteTable = function deleteTable (req, res, next) {
	console.log("deleteTable service started");
  DBOperationsService.deleteTable(req, res, next);
};
module.exports.insertRecord = function insertRecord (req, res, next) {
	console.log("insertRecord service started");
  DBOperationsService.insertRecord(req, res, next);
};
module.exports.updateRecord = function updateRecord (req, res, next) {
	console.log("updateRecord service started");
  DBOperationsService.updateRecord(req, res, next);
};
module.exports.deleteRecord = function deleteRecord (req, res, next) {
	console.log("deleteRecord service started");
  DBOperationsService.deleteRecord(req, res, next);
};
module.exports.addColumn = function addColumn (req, res, next) {
	console.log("addColumn service started");
  DBOperationsService.addColumn(req, res, next);
};
module.exports.deleteColumn = function deleteColumn (req, res, next) {
	console.log("deleteColumn service started");
  DBOperationsService.deleteColumn(req, res, next);
};
module.exports.updateColumn = function updateColumn (req, res, next) {
	console.log("updateColumn service started");
  DBOperationsService.updateColumn(req, res, next);
};
module.exports.getDataByField = function getDataByField (req, res, next) {
	console.log("getDataByField service started");
  DBOperationsService.getDataByField(req, res, next);
}
module.exports.getTableSchema = function getDataByField (req, res, next) {
	console.log("getTableSchema service started");
	  DBOperationsService.getTableSchema(req, res, next);
	}
module.exports.changePasswordOfAppUser = function getDataByField (req, res, next) {
	console.log("changePasswordOfAppUser service started");
	  DBOperationsService.changePasswordOfAppUser(req, res, next);
	}
module.exports.verifyRetailer = function getDataByField (req, res, next) {
	console.log("verifyRetailer service started");
	  DBOperationsService.verifyRetailer(req, res, next);
	}
module.exports.changePassword =  function changePassword (req, res, next) {
	console.log("changePassword service started");
	  DBOperationsService.changePassword(req, res, next);
	}
module.exports.forgotPassword = function forgotPassword (req, res, next) {
	console.log("forgotPassword service started");
	DBOperationsService.forgotPassword(req, res, next);
}
module.exports.commonLimitOrActiveCheck = function commonLimitOrActiveCheck (req, res, next) {
        console.log("commonLimitOrActiveCheck service started");
        DBOperationsService.commonLimitOrActiveCheck(req, res, next);
}

module.exports.getGroupMasterList = function getGroupMasterList (req, res, next) {
        console.log("getGroupMasterList service started");
        DBOperationsService.getGroupMasterList(req, res, next);
}
module.exports.getFeedbackList = function getFeedbackList(req, res, next) {
    console.log("getFeedbackList service started");
    DBOperationsService.getFeedbackList(req, res, next);
}

