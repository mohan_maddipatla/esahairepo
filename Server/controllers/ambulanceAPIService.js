var ambulanceDao = require("../Dao/ambulanceAPIDao.js");
var datetime = require('node-datetime');
var logger = require("../logging/logger.js");
var logsDao = require("../Dao/logsDao.js");
var generalDao = require("../Dao/generalDao.js");
var statusConstant = require("../Util/statusConstants.json");
exports.getDriversListByAmbulanceId = function(req,res){
	var respJson = {};
	var apiReq = {}; //api-log
	apiReq.apiPath = 'getDriversListByAmbulanceId';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = req.body.userId;
	ambulanceDao.getDriversListByAmbulanceId(req, function(data, status) {
		logger.info("getDriversListByAmbulanceId ",data);
		if(data=="success"){
			respJson['statusCode'] = "300";
			respJson['statusMessage']= "getDriversListByAmbulanceId fetched successfully";
			respJson['responseData'] = status;
			res.end(JSON.stringify(respJson));
		}else if (data == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (data == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		}else if(data == "noAmbulance"){
			respJson['statusCode'] = "444";
			respJson['statusMessage'] = "No Ambulances Found";
			res.end(JSON.stringify(respJson));
		} else {
			console.log("PPPPPPPPPPP ",status);
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = status;
			res.end(JSON.stringify(respJson));
		}

		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		apiReq.resultCode = res[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	});
}

exports.updateAmbulanceStatus = function(req,res){
	var respJson = {};
	var apiReq = {}; //api-log
	apiReq.apiPath = 'updateAmbulanceStatus';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = req.body.userId;
	ambulanceDao.updateAmbulanceStatus(req, function(data, status) {
		logger.info("updateAmbulanceStatus ",data);
		if(data=="success"){
			respJson['statusCode'] = "300";
			respJson['statusMessage']= "updated successfully";
			res.end(JSON.stringify(respJson));
		}else if (data == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (data == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (data == "cancelled") {
			respJson['statusCode'] = "411";
			respJson['statusMessage'] = "booking was cancelled, you can't update the status";
			res.end(JSON.stringify(respJson));
		} else {
			console.log("PPPPPPPPPPP ",status);
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = status;
			res.end(JSON.stringify(respJson));
		}

		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		apiReq.resultCode = res[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	});
}

exports.updateDeviceToken = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'updateDeviceToken';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	ambulanceDao.updateDeviceToken(req, function(flag,data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";
			respJson[statusConstant.STATUS_MESSAGE] = "device token updated successfully";
			res.end(JSON.stringify(respJson));
		} else if (flag == "err") {
			respJson[statusConstant.STATUS_CODE] = "304";
			respJson[statusConstant.STATUS_MESSAGE] = "Error occured while updating";
			res.end(JSON.stringify(respJson));
		}else if (flag == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		}  else if(data == "inActive"){
			respJson['statusCode'] = "410";
			respJson['statusMessage'] = statusConstant.s130;
			res.end(JSON.stringify(respJson));
		}else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.getEmergencyTypeList = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'getEmergencyTypeList';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	ambulanceDao.getEmergencyTypeList(req, function(flag,data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";

			respJson['responseData'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "err") {
			respJson[statusConstant.STATUS_CODE] = "304";
			respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching emergency types";
			res.end(JSON.stringify(respJson));
		}else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.getAmbulanceDriverStatusList = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'getAmbulanceDriverStatusList';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	ambulanceDao.getAmbulanceDriverStatusList(req, function(flag,data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";
			respJson['responseData'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "err") {
			respJson[statusConstant.STATUS_CODE] = "304";
			respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching";
			res.end(JSON.stringify(respJson));
		}else if (flag == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		}  else if(data == "inActive"){
			respJson['statusCode'] = "410";
			respJson['statusMessage'] = statusConstant.s130;
			res.end(JSON.stringify(respJson));
		}else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.getStatusList = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'getStatusList';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	generalDao.getStatusList(req, function(flag, data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";
			respJson['responseData'] = data;
			res.end(JSON.stringify(respJson));
		} else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.trackAmbulance = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'trackAmbulance';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	ambulanceDao.trackAmbulance(req, function(flag, data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";
			respJson[statusConstant.STATUS_MESSAGE] = "saved successfully";
			res.end(JSON.stringify(respJson));
		} else if (flag == "err") {
			respJson[statusConstant.STATUS_CODE] = "304";
			respJson[statusConstant.STATUS_MESSAGE] = "Error occured while updating";
			res.end(JSON.stringify(respJson));
		} else if (flag == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (data == "inActive") {
			respJson['statusCode'] = "410";
			respJson['statusMessage'] = statusConstant.s130;
			res.end(JSON.stringify(respJson));
		} else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.getAmbulanceTrackDetails = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'getAmbulanceTrackDetails';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	ambulanceDao.getAmbulanceTrackDetails(req, function(flag, data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";
			respJson['responseData'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "err") {
			respJson[statusConstant.STATUS_CODE] = "304";
			respJson[statusConstant.STATUS_MESSAGE] = "Error occured while fetching";
			res.end(JSON.stringify(respJson));
		} else if (flag == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (data == "inActive") {
			respJson['statusCode'] = "410";
			respJson['statusMessage'] = statusConstant.s130;
			res.end(JSON.stringify(respJson));
		} else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.updateAmbulanceDriverStatus = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'updateAmbulanceDriverStatus';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	ambulanceDao.updateAmbulanceDriverStatus(req, function(flag, data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";
			respJson[statusConstant.STATUS_MESSAGE] = "updated successfully";
			res.end(JSON.stringify(respJson));
		} else if (flag == "err") {
			respJson[statusConstant.STATUS_CODE] = "304";
			respJson[statusConstant.STATUS_MESSAGE] = "Error occured while updating";
			res.end(JSON.stringify(respJson));
		} else if (flag == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (data == "inActive") {
			respJson['statusCode'] = "410";
			respJson['statusMessage'] = statusConstant.s130;
			res.end(JSON.stringify(respJson));
		} else if(data == "onTrip"){
			respJson['statusCode'] = "411";
			respJson['statusMessage'] = "Ambulance is on trip, can't modify the status";
			res.end(JSON.stringify(respJson));
		}else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};

exports.getAmbulanceTypes = function(req, res) {
	var respJson = {};
	var request = req.body;
	var apiReq = {};
	apiReq.apiPath = 'getAmbulanceTypes';
	apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
	apiReq.userId = request.userId;
	apiReq.evd = request.EVD;
	ambulanceDao.getAmbulanceTypes(req, function(flag, data) {
		apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
		if (flag === "success") {
			respJson[statusConstant.STATUS_CODE] = "300";
			respJson[statusConstant.STATUS_MESSAGE] = "ambulance types fetched successfully";
			respJson['responseData'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "err") {
			respJson[statusConstant.STATUS_CODE] = "304";
			respJson[statusConstant.STATUS_MESSAGE] = "Error occured while updating";
			res.end(JSON.stringify(respJson));
		} else if (flag == "Invalid token") {
			respJson['statusCode'] = "204";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (flag == "Unauthorized") {
			respJson['statusCode'] = "401";
			respJson['statusMessage'] = data;
			res.end(JSON.stringify(respJson));
		} else if (data == "inActive") {
			respJson['statusCode'] = "410";
			respJson['statusMessage'] = statusConstant.s130;
			res.end(JSON.stringify(respJson));
		} else if(data == "noAmbs"){
			respJson['statusCode'] = "411";
			respJson['statusMessage'] = "No ambulance types found";
			res.end(JSON.stringify(respJson));
		}else {
			respJson['statusCode'] = "500";
			respJson['statusMessage'] = flag;
			res.end(JSON.stringify(respJson));
		}
		apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
		logsDao.insertAPILog(apiReq);
	})
};
exports.getAmbulanceProfile = function(req, res) {
    var respJson = {};
    var request = req.body;
    var apiReq = {};
    apiReq.apiPath = 'getAmbulanceProfile';
    apiReq.reqTime = datetime.create().format('Y-m-d H:M:S');
    apiReq.userId = request.userId;
    apiReq.evd = request.EVD;
    ambulanceDao.getAmbulanceProfile(req, function(flag, data) {
        apiReq.resTime = datetime.create().format('Y-m-d H:M:S');
        if (flag === "success") {
            respJson[statusConstant.STATUS_CODE] = "300";
            respJson['responseData'] = data;
            res.end(JSON.stringify(respJson));
        } else if (flag == "err") {
            respJson[statusConstant.STATUS_CODE] = "304";
            respJson[statusConstant.STATUS_MESSAGE] = "Error occured while updating";
            res.end(JSON.stringify(respJson));
        } else if (flag == "Invalid token") {
            respJson['statusCode'] = "204";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else if (flag == "Unauthorized") {
            respJson['statusCode'] = "401";
            respJson['statusMessage'] = data;
            res.end(JSON.stringify(respJson));
        } else {
            respJson['statusCode'] = "500";
            respJson['statusMessage'] = flag;
            res.end(JSON.stringify(respJson));
        }
        apiReq.resultCode = respJson[statusConstant.STATUS_CODE];
        logsDao.insertAPILog(apiReq);
    })
};
