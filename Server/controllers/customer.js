var url = require('url');
var customerService = require('./customerService');
var logger = require("../logging/logger.js");

module.exports.getRateCard = function getRateCard (req, res, next) {
	logger.info("getRateCard service started");
  customerService.getRateCard(req, res, next);
  logger.info("getRateCard service ended");
};

module.exports.updateProfile = function updateProfile (req, res, next) {
	console.log("updateProfile service started");
	  customerService.updateProfile(req, res, next);
}

module.exports.addfeedback = function addfeedback (req, res, next) {
	logger.info("addfeedback service started");
  customerService.addfeedback(req, res, next);
  logger.info("addfeedback service ended");
};

module.exports.addPatientInfo = function addPatientInfo (req, res, next) {
	logger.info("addPatientInfo service started");
  customerService.addPatientInfo(req, res, next);
  logger.info("addPatientInfo service ended");
};

module.exports.getPatientInfo = function getPatientInfo (req, res, next) {
	logger.info("getPatientInfo service started");
  customerService.getPatientInfo(req, res, next);
  logger.info("getPatientInfo service ended");
};

module.exports.updatePatientInfo = function updatePatientInfo (req, res, next) {
	logger.info("updatePatientInfo service started");
  customerService.updatePatientInfo(req, res, next);
  logger.info("updatePatientInfo service ended");
};

module.exports.checkCustomerExistsOrNot = function checkCustomerExistsOrNot (req, res, next) {
	logger.info("checkCustomerExistsOrNot service started");
  customerService.checkCustomerExistsOrNot(req, res, next);
  logger.info("checkCustomerExistsOrNot service ended");
};
module.exports.getAppVersion = function getAppVersion (req, res, next) {
	logger.debug("getAppVersion service started");
	customerService.getAppVersion(req, res, next);
	logger.debug("getAppVersion service ended");
};

module.exports.getTermsAndConditions = function getTermsAndConditions (req, res, next) {
    logger.debug("getTermsAndConditions service started");
    customerService.getTermsAndConditions(req, res, next);
    logger.debug("getTermsAndConditions service ended");
};
module.exports.getComeToKnowBy = function getComeToKnowBy(req, res, next) {
    logger.debug("getComeToKnowBy service started");
    customerService.getComeToKnowBy(req, res, next);
    logger.debug("getComeToKnowBy service ended");
};
module.exports.getCommandCentreNUmber = function getCommandCentreNUmber(req, res, next) {
    logger.debug("getCommandCentreNUmber service started");
    customerService.getCommandCentreNUmber(req, res, next);
    logger.debug("getCommandCentreNUmber service ended");
};
module.exports.getPrivacyPolicy = function getPrivacyPolicy(req, res, next) {
    logger.debug("getPrivacyPolicy service started");
    customerService.getPrivacyPolicy(req, res, next);
    logger.debug("getPrivacyPolicy service ended");
};

module.exports.getFaqList = function getFaqList(req, res, next) {
    logger.debug("getFaqList service started");
    customerService.getFaqList(req, res, next);
    logger.debug("getFaqList service ended");
};
module.exports.getCountryList = function getCountryList(req, res, next) {
    logger.info("getCountryList service started");
    customerService.getCountryList(req, res, next);
    logger.info("getCountryList service ended");
};