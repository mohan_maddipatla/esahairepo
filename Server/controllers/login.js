'use strict';

var url = require('url');


var loginService = require('./loginService');

module.exports.appUserLogin = function appUserLogin (req, res, next) {
	console.log("appUserLogin service started");
  loginService.appUserLogin(req, res, next);
};
module.exports.login = function appUserLogin (req, res, next) {
	console.log("login service started");
  loginService.login(req, res, next);
};
module.exports.getApiDocs = function getApiDocs (req, res, next) {
	console.log("getApiDocs service started");
  loginService.getApiDocs(req, res, next);
};
module.exports.getDashboardDetails = function getDashboardDetails (req, res) {
	console.log("getDashboardDetails service started");
  loginService.getDashboardDetails(req, res);
};
module.exports.generateOTPAmbulance = function generateOTPAmbulance(req, res) {
	console.log("generateOTPAmbulance service started");
  loginService.generateOTPAmbulance(req, res);
};
module.exports.verifyAmbulanceOTP = function verifyAmbulanceOTP (req, res) {
	console.log("verifyAmbulanceOTP service started");
  loginService.verifyAmbulanceOTP(req, res);
};
module.exports.logout = function verifyOTP (req, res) {
	console.log("logout service started");
  loginService.logout(req, res);
};
module.exports.driverLogin = function driverLogin (req, res) {
	console.log("driverLogin service started");
  loginService.driverLogin(req, res);
};
module.exports.driverLogout = function driverLogout (req, res) {
	console.log("driverLogout service started");
  loginService.driverLogout(req, res);
};

module.exports.customerGenerateOTP = function customerGenerateOTP(req, res) {
	console.log("customerGenerateOTP service started");
  loginService.customerGenerateOTP(req, res);
};
module.exports.verifyCustomerOTP = function verifyCustomerOTP (req, res) {
	console.log("verifyCustomerOTP service started");
  loginService.verifyCustomerOTP(req, res);
};