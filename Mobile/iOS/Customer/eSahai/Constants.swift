//
//  Constants.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import Foundation

//Dev Server Url
//let kServerUrl = "http://54.200.233.193/esahai/db/"
//http://dev.esahai.in/esahai-ios/swagger/
//let kServerUrl = "http://dev.esahai.in/esahai/db/"
//let kServerUrl = "http://dev.esahai.in/esahai-ios/db/"

// QA Server Url
//let kServerUrl = "http://qa.esahai.in/esahai/db/"

// Prod Server Url
let kServerUrl = "https://prod.esahai.in/esahai/db/"

let	kAPI_Key = "AIzaSyAkqngmnSY19mAY3Qj5yU63N7f48_16GOU"
//"AIzaSyBILQohUbnLLLvNH-Y-rnLo-3bkeZHoJSs"

//Menu Keys
let kHome = "Home"
let kEmergencyRequests = "Emergency Requests"
let kBloodRequest = "Blood Request"
let kEmergencyContacts = "Emergency Contacts"
let kMyProfile = "My Profile"
let kSettings = "Settings"
let kRateCard = "Rate Card"
let kFAQs = "FAQs"


//Crittercism
let kcrittercismAppID = "64b8ca68725f481291651d436ad2fce400555300"
let kcrittercismAppKey = "2c8829e39f06e6558ee1c6943e3e5872"

let kCustomerCare = "+914047911911"
let kDriverNum = "driverMobileNumber"

//Google Analytics Screen Names
let kAnalyticsEventName = "Button_Action"
let  kCallAmbulanceButtonName = "CAll AMBULANCE"
let  kBookNowMedicalTaxiButtonName = "BOOK MEDICAL TAXI"
let  kScheduleMedicalTaxiButtonName = "SCHEDULE MEDICAL TAXI"
let  kBloodRequestButtonName = "BLOOD REQUEST"
let  kNearestHospitalButtonName = "NEAREST HOSPITAL"
let  kOffersButtonName = "Offers"

//User Default Keys
let kISSIGNEDIN = "ISSIGNEDIN"
let kMobileNumber = "MobileNumber"
let kMobile = "Mobile"
let kEncryptedMobileNumber = "EncryptedMobileNumber"
let kToken = "token"
let kCustomer_id = "customer_id"
let kGeoCoordinate = "GeoCoordinate"
let kLatitude = "latitude"
let kLongitude = "longitude"
let kAddress = "address"
let kIMEI = "deviceIMEI"
let kDeviceToken = "DeviceToken"
let kCountryNames = "Country Names"
let kCountryList = "Country List"

let kCancelCode = "CAN" 
let kFakeCancelCode = "CANCFAKE"

let ktripDetails = "TripBookingDetails"




let  kUserPackage = "UserPackage"

let kToastDuration  = 3.0
let kNetworkStatusMessage = "Please check your internet connection!" 
let kRequestTimedOutMessage = "The request timed out."


let kEmergencyList = "arrEmergencyTypeList"
let kAmbulanceList = "arrAmbulanceTypes"
let kHowDoYouKnowList = "arrDictHowDoYouKnow"
let kTNC = "TNC"
let kPP = "PrivacyPolicyList"
let kFAQ = "FAQ"

