//
//  MenuViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//


import UIKit
//
// MARK: - Section Data Structure
//
struct Section {
    var name: String!
    var items: [String]!
    var collapsed: Bool!
    var menuIcon : String!
    
    init(name: String, items: [String], collapsed: Bool = true , menuIcon : String ) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
        self.menuIcon = menuIcon
    }
}

//
// MARK: - View Controller
//
class MenuViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate  {
    
    @IBOutlet var lblMobileNumberTopConstraint: NSLayoutConstraint!
    @IBOutlet var lblMobileNumber: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet weak var menyTblView: UITableView!
    var sections = [Section]()
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Initialize the sections array
        // Here we have three sections: Mac, iPad, iPhone
        self.view.backgroundColor = UIColor(hex: "#195F92")
        menyTblView.backgroundColor = UIColor(hex: "#195F92")
        
        sections = [
            Section(name: "Home", items: [], menuIcon : "Side Menu Bar-01.png"),
            Section(name: "My Account", items: ["Profile" , "My Trips"],menuIcon : "Side Menu Bar-02.png"),
            Section(name: "Services", items: ["Ambulance","Medical Taxi","Blood Request","Nearest Hospital"],menuIcon : "Side Menu Bar-03.png"),
            Section(name: "Emergency Contacts", items: [],menuIcon : "Side Menu Bar-04.png"),
            Section(name: "Rate Card", items: [], menuIcon : "Side Menu Bar-05.png"),
            Section(name: "Offers", items: [], menuIcon : "Side Menu Bar-06.png"),
            Section(name: "Support", items: ["Request Call back","Call Customer Care", "Email"],menuIcon : "Side Menu Bar-07.png"),
            Section(name: "Help", items: ["FAQ"],menuIcon : "Side Menu Bar-08.png"),
            Section(name: "About", items: ["About Us", "Privacy Policy","Terms & Conditions"],menuIcon : "Side Menu Bar-09.png"),
        ]
        
        //For Rounded Image View
        imgProfile.layer.borderWidth = 1
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.borderColor = UIColor.white.cgColor
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2.0
        imgProfile.clipsToBounds = true
       
        menyTblView.separatorColor = UIColor.clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillAppear(_:)), name: NSNotification.Name(rawValue: "ReloadMenuTableView"), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Menu Screen")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        self.reloadTableView()
        
         lblMobileNumber.text = "+\(UserDefaults.standard.value(forKey: kMobileNumber)as! String)"
        
        if UserDefaults.standard.value(forKey: "customer_name") != nil {
            lblName.text = UserDefaults.standard.value(forKey: "customer_name") as? String
            lblMobileNumberTopConstraint.constant = 40
        }
        else{
            lblMobileNumberTopConstraint.constant = 16
            lblName.text = nil
        }
        if UserDefaults.standard.value(forKey: "ProfileImage") != nil
        {
            if(self.appDelegate.checkInternetConnectivity()){
            let profileData = UserDefaults.standard.value(forKey: "ProfileImage") as! NSDictionary
            
            DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                
                let url = NSURL(string:(profileData["imageUrl"] as? String)!)
                let data = NSData(contentsOf:url as! URL)
                let image = UIImage(data:data! as Data)
                
                DispatchQueue.main.async(execute: {() -> Void in
                    // Update the UI
                    self.imgProfile.image = image
                })
            })
        }
    }
        
    }
}

//
// MARK: - Table View Controller DataSource and Delegate
//
extension MenuViewController {
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    // Cell
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell? ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        cell.textLabel?.text = "        \(sections[indexPath.section].items[indexPath.row])"
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        cell.textLabel?.textColor = UIColor.white
        cell.backgroundColor = UIColor(hex: "#195F92")
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].collapsed! ? 0 : 44.0
    }
    
    // Header
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = sections[section].name
        if(sections[section].items.count == 0){
            header.arrowLabel.text = ""
        }else{
            header.arrowLabel.text = ">"
        }
        if(sections[section].menuIcon.characters.count > 0){
            header.iconImage.image = UIImage(named: sections[section].menuIcon)
        }
        
        header.setCollapsed(sections[section].collapsed)
        header.section = section
        header.delegate = self
        
        return header
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    
        if(sections[indexPath.section].items[indexPath.row] == "Profile"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        
        if(sections[indexPath.section].items[indexPath.row] == "My Trips"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "TripsViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
       
        if(sections[indexPath.section].items[indexPath.row] == "Ambulance"){
             let vc = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController")
             SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }

        if(sections[indexPath.section].items[indexPath.row] == "Medical Taxi"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "MedicalTaxiViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }

       if(sections[indexPath.section].items[indexPath.row] == "Blood Request"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "BloodRequestViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
       }

       if(sections[indexPath.section].items[indexPath.row] == "Nearest Hospital"){
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "HospitalsMapRoutesViewController")
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        
       }
        
        
        
       if(sections[indexPath.section].items[indexPath.row] == "Choose Language"){
       }

       if(sections[indexPath.section].items[indexPath.row] == "Clear Cache"){
        }
        
        if(sections[indexPath.section].items[indexPath.row] == "Request Call back"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            vc.isComingFrom = "Request Call back"
         //   vc.viewWillAppear(false)
           SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        
            }
        if(sections[indexPath.section].items[indexPath.row] == "Call Customer Care"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
             vc.isComingFrom = "Call Customer Care"
          //  vc.viewWillAppear(false)
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        if(sections[indexPath.section].items[indexPath.row] == "Email"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            vc.isComingFrom = "Email"
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        if(sections[indexPath.section].items[indexPath.row] == "FAQ"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "FAQViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        if(sections[indexPath.section].items[indexPath.row] == "About Us"){
            
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AboutUsViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        if(sections[indexPath.section].items[indexPath.row] == "Privacy Policy"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicy")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        if(sections[indexPath.section].items[indexPath.row] == "Terms & Conditions"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "Terms_ConditionsViewController") as! Terms_ConditionsViewController
            vc.canShowSlideMenu = true
            
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
    }
    
    

}

//
// MARK: - Section Header Delegate
//
extension MenuViewController: CollapsibleTableViewHeaderDelegate {
    
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        
        for i in 0 ..< sections.count {
            
            if section != i {
                sections[i].collapsed = true
               // header.setCollapsed(collapsed)
                
            }
        }
        
        if(sections[section].name == "Home"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        
        if(sections[section].name == "Emergency Contacts"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "EmergencyContactsViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        if(sections[section].name == "Rate Card"){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "RateCardViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }
        
        if(sections[section].name == "Offers"){
            
            guard let tracker = GAI.sharedInstance().defaultTracker else { return }
            guard let builder = GAIDictionaryBuilder.createEvent(withCategory: kAnalyticsEventName, action: kOffersButtonName, label:"Offers", value: nil)else { return }
            tracker.send(builder.build() as [NSObject : AnyObject])
            
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "PromosViewController")
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
            
        }

        
        let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        // Adjust the height of the rows inside the section
       /* menyTblView.beginUpdates()
        for i in 0 ..< sections[section].items.count {
            menyTblView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        menyTblView.endUpdates()*/
        menyTblView.reloadData()
        if(!collapsed){
           self.tableViewScrollToBottom(section: section)
        }
    }
    
    func reloadTableView(){
        
        for i in 0 ..< sections.count {
                sections[i].collapsed = true
        }
        menyTblView.reloadData()
        menyTblView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func tableViewScrollToBottom(section: Int) {
        
        //let numberOfSections = self.menyTblView.numberOfSections
        let numberOfRows = self.menyTblView.numberOfRows(inSection: section)
        
        if numberOfRows > 0 {
            let indexPath = IndexPath(row: numberOfRows-1, section: section)
            self.menyTblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}

