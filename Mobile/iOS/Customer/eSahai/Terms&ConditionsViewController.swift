//
//  Terms&ConditionsViewController.swift
//  eSahai
//
//  Created by PINKY on 01/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

class Terms_ConditionsViewController: BaseViewController {
    
    var canShowSlideMenu = Bool()
    

    @IBOutlet weak var webViewTC: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Terms & Conditions"
        self.navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor(hex: "#195F92")
        navigationController?.navigationBar.tintColor = UIColor.white
        //navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        

        webViewTC.scrollView.isScrollEnabled = true

        // Do any additional setup after loading the view.
        self.getTCDetails()
    }
    
    func getTCDetails() {
        
        let  strUrl = String(format :"%@getTermsAndConditions",kServerUrl) as NSString
        
        let url : NSURL = NSURL(string: strUrl as String)!
        
        sharedController.requestGETURL(strURL:url, success:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        
                        let htmlString : String = ((result["responseData"] as! NSArray).firstObject as! NSDictionary).value(forKey: "terms_and_conditions") as! String
                        self.webViewTC.loadHTMLString(htmlString,
                                                      baseURL: nil)
                    }
                    else
                    {
                        self.appDelegate.window?.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        return
                    }
                    
            }
            
        }, failure:  {(error) in
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Slide Navigation
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        
        if(canShowSlideMenu){
            return true
        }
        return false
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
