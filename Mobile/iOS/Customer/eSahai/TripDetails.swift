//
//  BookingConfirmationViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/30/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit


class TripDetails: BaseViewController,UIPopoverPresentationControllerDelegate,delegatePopOver {
    
    @IBOutlet weak var lblTotalDistance: UITextField!

    @IBOutlet weak var lblTotalTime: UITextField!
    
    @IBOutlet weak var lblAvgSpeed: UITextField!
    
    @IBOutlet weak var lblTripCost: UITextField!
    
    var data = NSDictionary()
   // var BookID = NSString()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.bookingString = ""
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Trip Details"
        navigationController?.navigationBar.barTintColor = UIColor(hex: "#195F92")
        navigationController!.navigationBar.tintColor = UIColor.white
        //navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        let newBackButton = UIBarButtonItem(title: "<", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TripDetails.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
     
        self.setUpView()
    }
    
    func setUpView() {
        
        print(data)
        
        if var totalDistance : Float = Float((data.value(forKey: "distance")as? String)!){
            totalDistance = totalDistance/1000.0
            lblTotalDistance.text = String(format : "%.2f Kms",totalDistance)
        }
        
       // lblTotalDistance.text = ("\(data.value(forKey:"distance") as! String) Kms")
        
        let tripDuration : TimeInterval = TimeInterval(data.value(forKey: "duration") as! String)!
        
        lblTotalTime.text = self.stringFromTimeInterval(interval: tripDuration) as String
        
        lblAvgSpeed.text = data.value(forKey: "speed")
            as? String
           
        lblTripCost.text = ("₹\(data.value(forKey: "cost") as! String)")
    }
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        var ti = NSInteger(interval)
        ti = ti/1000
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return NSString(format:"%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }

    @IBAction func DoneAction(_ sender: Any) {
   
        let popoverContent = (self.storyboard?.instantiateViewController(withIdentifier: "RatingPopOverViewController")) as! RatingPopOverViewController
        popoverContent.bookingID = appDelegate.bookingID as String
        popoverContent.del = self
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nav.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width:320, height:130)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.sourceRect = CGRect(x: UIScreen.main.bounds.width * 0.5 - 160, y: UIScreen.main.bounds.height * 0.5 - 65, width: 0, height: 0)
       // popover?.sourceRect = CGRect(x: self.view., y: 200, width: 0, height: 0)
        self.present(nav, animated: true, completion: nil)
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }

    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }

    func back(sender: UIBarButtonItem) {
        self.goToDashBoard()
    }
    
    func goToDashBoard()
    {
        for VC: UIViewController in self.navigationController!.viewControllers {
            if (VC is DashboardViewController) {
                self.navigationController!.popToViewController(VC, animated: true)
                return
            }
        }
        
        let signIn = self.storyboard?.instantiateViewController(withIdentifier:"DashboardViewController") as! DashboardViewController
        self.navigationController?.pushViewController(signIn, animated: false)
        
        return
    }
    
    func updateTable(){
        self.goToDashBoard()
    }

    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.bookingID = ""
    }

}
