//
//  PopUpViewController.swift
//  eSahai
//
//  Created by PINKY on 06/02/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

class PopUpViewController: BaseViewController {

    @IBOutlet weak var lblInternetConnection: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        appDelegate.isAtDashBoard = false
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        if !(appDelegate.checkInternetConnectivity()){
            lblInternetConnection.isHidden = false
        }else{
            lblInternetConnection.isHidden = true
        }
        
        if(UserDefaults.standard.value(forKey: "isfirstTime") == nil){
            if(!appDelegate.checkInternetConnectivity()){
                
                let alertController = UIAlertController(title: "", message: "No internet Connection! Do you want to connect to Customer service for booking?", preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                }
                let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    
                    guard let number = URL(string: "telprompt://\(kCustomerCare)")
                        else { return }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(number, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        if let url = URL(string: "tel://\(kCustomerCare)") {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }else{
                UserDefaults.standard.set(true, forKey: "isfirstTime")
                self.getMissingValues()
            }
        }
        else{
            self.getMissingValues()
        }
    }
    
    func getMissingValues(){
        if(appDelegate.checkInternetConnectivity()){
            self.UpdateVersion()
        }else{
            if (UserDefaults.standard.value(forKey:kCountryList) != nil) && (UserDefaults.standard.value(forKey: kEmergencyList) != nil) && (UserDefaults.standard.value(forKey: kAmbulanceList) != nil){
                 self.appDelegate.window?.makeToast("No internet connection..!", duration:kToastDuration, position:CSToastPositionBottom)
                self.movetosignUpPage()
            }else{
                self.appDelegate.window?.makeToast("The Internet connection appears to be offline. Please connect to the internet", duration:kToastDuration, position:CSToastPositionBottom)
                return
            }
        }
    }
    
    // Force Update Sys call
    func UpdateVersion(){
        
        let  strUrl = String(format :"%@getAppVersion?appType=Customer_Ios",kServerUrl) as NSString
        
        let url : NSURL = NSURL(string: strUrl as String)!
        
        sharedController.requestGETURL(strURL:url, success:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        let dict = result["responseData"] as? NSDictionary
                        
                        if (dict?.value(forKey:"force_upgrade") as! String) == "1" && ((dict?.value(forKey:"version_no")) as! String != self.appDelegate.strAppVersion)
                        {
                            
                            let alertController = UIAlertController(title: "", message: "This app is an old version. Please update the app", preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                                return
                            }
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                                
                                print ("forceupgrade")
                                UIApplication.shared.openURL(URL(string: "itms://itunes.apple.com/app/id" + "id1230376202")!)
                            }
                            alertController.addAction(cancelAction)
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                           
                            
                        } else if ((dict?.value(forKey:"force_upgrade") as! String) == "0") && ((dict?.value(forKey:"version_no")) as! String != self.appDelegate.strAppVersion) {
                            
                            let alertController = UIAlertController(title: "", message: "Please update app to get new features", preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                                
                                self.getCustomerCareNumber()
                                //self.getCountryCodes()
                                //self.getAmbulanceType()
                                //self.getEmergencyTypeList()
                            }
                            
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }else {
                            self.getCustomerCareNumber()
                           // self.getCountryCodes()
                            //self.getAmbulanceType()
                            //self.getEmergencyTypeList()
                        }
                    }
                    else
                    {
                        self.appDelegate.window?.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        return
                    }
                        
                }
            
        }, failure:  {(error) in
        })
    }

    
    func getCountryCodes(){
        
        let  strUrl = String(format :"%@getCountryList",kServerUrl) as NSString
        
        let url : NSURL = NSURL(string: strUrl as String)!
        
        sharedController.requestGETURL(strURL:url, success:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        let countryNames = NSMutableArray()
                        let countryLists = result["responseData"] as! NSMutableArray
                        for dict in countryLists{
                            let dic = dict as! NSDictionary
                            countryNames.add(dic["country"]!)
                        }
                        
                        UserDefaults.standard.set(countryNames, forKey:kCountryNames )
                        UserDefaults.standard.set(countryLists, forKey:kCountryList )
                        
                        for i in 0..<countryLists.count {
                            
                            if ( ((countryLists.object(at: i) as! NSDictionary ).value(forKey: "country")as! String)  == "India") {
                                self.appDelegate.row = i
                            }
                        }
                       self.getAmbulanceType()
                    }
                    else if(strStatusCode != "500")
                    {
                        self.appDelegate.window?.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        return
                    }
            }
            
        }, failure:  {(error) in
        })
    }

    
    func getAmbulanceType() {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getAmbulanceTypes") as NSString
        
        let dictParams = [:] as NSDictionary
        
        let dictHeaders = [:] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        let arrAmbulanceTypes =  result["responseData"] as! NSMutableArray
                        
                        for i in 0..<arrAmbulanceTypes.count{
                            let tempDict = arrAmbulanceTypes.object(at: i) as! NSMutableDictionary
                            
                            for (key, value) in tempDict {
                                print("value --> \(value)" )
                                if value is NSNull{
                                    tempDict[key] = ""
                                    arrAmbulanceTypes.replaceObject(at: i, with: tempDict)
                                }
                            }
                        }
                        UserDefaults.standard.set(arrAmbulanceTypes, forKey :kAmbulanceList)
                        
                       self.getEmergencyTypeList()
                    }
                    else if(strStatusCode != "500"){
                       
                        self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
            }
        }, failureHandler: {(error) in
        })
    }
    
    
    func getEmergencyTypeList() {
        
        UserDefaults.standard.removeObject(forKey:kEmergencyList )
        let  strUrl = String(format : "%@%@",kServerUrl,"getEmergencyTypeList") as NSString
        
        let dictParams = ["":""] as NSDictionary
        let dictHeaders = ["":""] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        let arrEmergencyTypeList =  result["responseData"] as! NSMutableArray
                        
                        for i in 0..<arrEmergencyTypeList.count{
                            let tempDict = arrEmergencyTypeList.object(at: i) as! NSMutableDictionary
                            
                            for (key, value) in tempDict {
                                print("value --> \(value)" )
                                if value is NSNull{
                                    tempDict[key] = ""
                                    arrEmergencyTypeList.replaceObject(at: i, with: tempDict)
                                }
                            }
                        }
                        UserDefaults.standard.set(arrEmergencyTypeList,forKey : kEmergencyList)
                        
                        
                        self.movetosignUpPage()
                    }
                    else if(strStatusCode != "500"){
                        self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
            }
        }, failureHandler: {(error) in
        })
    }
    
    func getCustomerCareNumber() {
        UserDefaults.standard.removeObject(forKey:kEmergencyList )
        let  strUrl = String(format : "%@%@",kServerUrl,"getCommandCentreNUmber") as NSString
        
        let dictParams = ["":""] as NSDictionary
        let dictHeaders = ["":""] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        self.getCountryCodes()
                        print("result --> \(result)")
                      UserDefaults.standard.set(result["responseData"] as! String, forKey: "CustomerCare")
                    }
            }
        }, failureHandler: {(error) in
        })

    }
    
    
    func movetosignUpPage() {
        let signIn = self.storyboard?.instantiateViewController(withIdentifier:"SignInViewController") as! SignInViewController
        self.navigationController?.pushViewController(signIn, animated: false)
        return
    }
}
