//
//  DetailedEmergiencyInfoViewController.swift
//  eSahai
//
//  Created by UshaRao on 12/7/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit

class TripDetailViewController: BaseViewController {

    var dictDetails = NSDictionary()
    
    @IBOutlet var lblTripCost: UILabel!
    @IBOutlet var lblAvgSpeed: UILabel!
    @IBOutlet var lblTripTime: UILabel!
    @IBOutlet var lblTotalDistance: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if var totalDistance : Float = Float((dictDetails.value(forKey: "distance")as? String)!){
            totalDistance = totalDistance/1000.0
            lblTotalDistance.text = String(format : "%.2f kms",totalDistance)
        }
        
         let tripDuration : TimeInterval = TimeInterval((dictDetails.value(forKey: "duration") as? String)!)!
        lblTripTime.text = self.stringFromTimeInterval(interval: tripDuration) as String
        
        lblAvgSpeed.text = dictDetails.value(forKey: "speed") as? String
        lblTripCost.text = String(format: "Rs %@",(dictDetails.value(forKey: "cost") as? String)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        let ti = NSInteger(interval)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return NSString(format:"%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }
    @IBAction func btnDone(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
