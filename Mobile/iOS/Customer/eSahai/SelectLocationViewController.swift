//
//  SelectLocationViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/14/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class SelectLocationViewController: BaseViewController,MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var coordinates = CLLocationCoordinate2D()
    @IBOutlet var mapViewObj: MKMapView!
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    var arrPredictions = NSMutableArray()
    var userLocationFromSearch = CLLocationCoordinate2D()
    let annotationPoint = MKPointAnnotation()

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var locationsTableView: UITableView!
    var myTimer = Timer()
    
    var isComingFrom = NSString()
    var delegate : LocationDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.getGPSCoordinates()
        coordinates = appDelegate.AppCurrentCoordinates
        mapViewObj.delegate = self
        searchBar.delegate = self
        annotationPoint.coordinate = coordinates
        mapViewObj.addAnnotation(annotationPoint)
        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinates, 500, 500)
        mapViewObj.setRegion(viewRegion, animated: true)
        self.addToolBar(searchBar)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func addToolBar(_ searchBar: UISearchBar) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.resignKeyboard))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        searchBar.inputAccessoryView = toolBar
    }
    func resignKeyboard() {
        searchBar.resignFirstResponder()
    }

    // MARK: - Select Button Clicked
    @IBAction func btnSelectLocationClicked(_ sender: AnyObject) {
        
        if(self.searchBar.text == ""){
            self.view.makeToast("Please select the location", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
     let locationDetails = ["lat": coordinates.latitude,"long": coordinates.longitude ,"address": self.searchBar.text! ,"geohash": (Geohash.encode(latitude: coordinates.latitude , longitude: coordinates.longitude))] as NSDictionary
        
        if(isComingFrom == "Current Location")
        {
            print(locationDetails)
            delegate?.getUserLocation(dict: locationDetails as NSDictionary)
        }else{
            delegate?.getHospitalLocation(dict: locationDetails as NSDictionary)
        }
      self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - MapView Delegate Methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
            let annotationIdentifier = "AnnotationIdentifier"
            
            var annotationView: MKAnnotationView?
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
            }
            else {
                let av = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                annotationView = av
            }
            if let annotationView = annotationView {
                // Configure your annotation view here
                annotationView.canShowCallout = true
            }
            
            return annotationView
        }
    
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool){
        
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool)
    {
        MBProgressHUD.hide(for: appDelegate.window, animated: true)
        MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
         coordinates = self.mapViewObj.centerCoordinate
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        
        geocoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in
            if error != nil {
                print("Error getting location2 : \(error)")
                MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
            }else {
                let placeArray = placemarks as [CLPlacemark]!
                var placeMark: CLPlacemark!
                placeMark = placeArray?[0]
                
            
                    let strAddress = String(format: "%@%@,%@,%@,%@,%@,%@,\n%@",(placeMark.name) != nil ? placeMark.name!: "",(placeMark.thoroughfare) != nil ? placeMark.thoroughfare!: "",(placeMark.locality) != nil ? placeMark.locality!:"",(placeMark.subLocality) != nil ? placeMark.subLocality!:"",(placeMark.subAdministrativeArea) != nil ? placeMark.subAdministrativeArea!:"",(placeMark.administrativeArea) != nil ? placeMark.administrativeArea!:"",(placeMark.country) != nil ? placeMark.country!:"",(placeMark.postalCode) != nil ? placeMark.postalCode!:"")
                if strAddress != ",,,,,,\n" {
                    
                    self.searchBar.text = strAddress
                  //  UserDefaults.standard.value(forKey:kAddress)] as NSDictionary
                  //  UserDefaults.standard.set(strAddress, forKey: kAddress)
                    
                }
                MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
            }
            
            MBProgressHUD.hideAllHUDs(for: self.appDelegate.window, animated: true)
            //MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
        }
    }
    
    // MARK: - Text Field Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if appDelegate.checkInternetConnectivity() == false && searchText == self.searchBar.text {
            
            self.view.makeToast(kNetworkStatusMessage, duration:kToastDuration, position:CSToastPositionTop)
            locationsTableView.isHidden = true
            return
        }
        self.getLocationsBasedOnAddress()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        return true
    }
   
    // MARK: - tableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print(self.arrPredictions)
        return self.arrPredictions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UITableViewCell = UITableViewCell(style:.default, reuseIdentifier: "identifier")
        let tempDict = self.arrPredictions.object(at: indexPath.row) as! NSDictionary
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.text = tempDict.value(forKey: "description") as! String?
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        locationsTableView.isHidden = true
        let tempDict = self.arrPredictions.object(at: indexPath.row) as! NSDictionary
        self.searchBar.text = tempDict.value(forKey: "description") as! String?
        self.searchBar.resignFirstResponder()
       // MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
        self.getCoordinateForAddress(address: self.searchBar.text!)
    }
    // MARK: - Address Methods
    
    func getLocationsBasedOnAddress() {
        
        if ((self.searchBar.text?.length)! > 1) {
            let strAddress = self.searchBar.text?.replacingOccurrences(of:" ", with: "%22", options: .literal, range: nil)
            
            //let  strUrl = String(format :"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=%@",strAddress!,kAPI_Key) as NSString
            
            let strUrl =  "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(kAPI_Key)&input=\(strAddress!)"
            
            
            let url : NSURL = NSURL(string: strUrl as String)!
            
            sharedController.requestGETURL(strURL:url, success:{(result) in
                DispatchQueue.main.async()
                    {
                        self.arrPredictions.removeAllObjects()
                        self.locationsTableView.isHidden = false
                        self.arrPredictions = result.value(forKey: "predictions") as! NSMutableArray
                        self.locationsTableView.reloadData()
                }
            }, failure:  {(error) in
            })
        }
        else
        {
            locationsTableView.isHidden = true
        }
    }
    

    
    func getCoordinateForAddress(address : String) {
        
        print(address)
       
            if ((address.length) > 1) {
                let strAddress = self.searchBar.text?.replacingOccurrences(of:" ", with: "%22", options: .literal, range: nil)
                let strUrl = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=\(strAddress!)&key=\(kAPI_Key)"
                let url : NSURL = NSURL(string: strUrl)!
                
                sharedController.requestGETURL(strURL:url, success:{(result) in
                    DispatchQueue.main.async()
                        {
                            let status = result["status"] as! String
                            
                            if( status == "ZERO_RESULTS" ||
                                status == "REQUEST_DENIED" ||
                                status == "INVALID_REQUEST" ||
                                status == "INVALID_REQUEST" ){
                                
                                //self.Test(address: self.searchBar.text! )
                                self.searchBar.text = ""
                               self.view.makeToast("Geo Location not found", duration:kToastDuration, position:CSToastPositionTop)
                                
                            }else if(status == "OK") {
                                if let result = result["results"] as? NSArray{
                                    
                                    let geometry = (result[0] as! NSDictionary).value(forKey:"geometry") as! NSDictionary
                                    let loco = geometry.value(forKey: "location") as! NSDictionary
                                    let adddressLatitude = loco.value(forKey: "lat")
                                    let addressLongitude = loco.value(forKey: "lng")
                                    
                                    let addressCoordinates = CLLocationCoordinate2DMake(adddressLatitude as! CLLocationDegrees, addressLongitude as! CLLocationDegrees)
                                    
                                    self.annotationPoint.coordinate = addressCoordinates
                                    
                                    //self.mapViewObj.addAnnotation(annotationPoint)
                                    
                                    let viewRegion = MKCoordinateRegionMakeWithDistance(self.annotationPoint.coordinate, 500, 500)
                                    self.mapViewObj.centerCoordinate = self.annotationPoint.coordinate
                                    self.mapViewObj.setRegion(viewRegion, animated: true)
                                }
                            }
                        
            
                    }
                }, failure:  {(error) in
                })
            }
    }
    
    // MARK: - Current Location Button
    @IBAction func CurrentLocation(_ sender: Any) {
        appDelegate.getGPSCoordinates()
        coordinates = appDelegate.AppCurrentCoordinates
        annotationPoint.coordinate = coordinates
        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinates, 500, 500)
        mapViewObj.setRegion(viewRegion, animated: true)
    }
}
