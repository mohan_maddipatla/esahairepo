//
//  BloodRequestViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit

class BloodRequestViewController: BaseViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var customPickerView : UIView! = UIView()
    var pickerView : UIPickerView = UIPickerView()
    var intTag =  NSInteger()
    var arrBloodGroups = NSMutableArray()
    var arrBloodSize = NSMutableArray()
    
    @IBOutlet var txtLocationName: UITextField!
    @IBOutlet var txtRequiredBloodSize: UITextField!
    @IBOutlet var txtBloodGroup: UITextField!
    @IBOutlet var txtContactNumber: UITextField!
    @IBOutlet var txtContactName: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Blood Request"
        self.addToolBar(txtContactNumber)
        txtBloodGroup.text = "A+"
        txtRequiredBloodSize.text = "50 ml"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Blood Request Screen")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    // MARK: - Slide Navigation
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
     // MARK: - Submit Button Clicked
    @IBAction func btnSubmitClicked(_ sender: AnyObject) {
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        guard let builder = GAIDictionaryBuilder.createEvent(withCategory: kAnalyticsEventName, action: kBloodRequestButtonName, label:"Submit", value: nil)else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        
        if (txtContactName.text?.isEmpty)! || (txtContactNumber.text?.isEmpty)! || (txtLocationName.text?.isEmpty)!
        {
            self.view.makeToast("Enter all the details", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
        if((txtContactNumber.text?.length)! < 10){
            
            self.view.makeToast("Please Check the Mobile Number !", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
        let  strUrl = String(format : "%@%@",kServerUrl,"addBloodRequest") as NSString
        let currentDate = NSDate()
        let strDate = String(format:"%@",currentDate)
        
        let dictParams = ["geohash":Geohash.encode(latitude: appDelegate.AppCurrentCoordinates.latitude, longitude: appDelegate.AppCurrentCoordinates.longitude),kLatitude: appDelegate.AppCurrentCoordinates.latitude ,"longitude": appDelegate.AppCurrentCoordinates.longitude,"customerApp": "true","ambulanceApp": "false",kCustomer_id:UserDefaults.standard.value(forKey: kCustomer_id)as! String,"name":txtContactName.text! as String,"contact":txtContactNumber.text! as String,"blood_group":txtBloodGroup.text! as String,"quantity":txtRequiredBloodSize.text! as String,"location":txtLocationName.text! as String,"time":strDate,"customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String] as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async(){
            let strStatusCode = result["statusCode"] as? String
            
            if strStatusCode == nil
            {
                return
            }
            
            if strStatusCode == "300"
            {
              self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                let DashCon  = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                self.navigationController?.pushViewController(DashCon, animated: true)
            }else if strStatusCode == "204"{
                self.logout()
            }
            else if(strStatusCode != "500"){
            
                self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
                }
            }, failureHandler: {(error) in
        })
    }
    @IBAction func btnSelectBloodGroop(_ sender: AnyObject) {
        
        self.resignKeyboard()
        intTag = 333
        arrBloodGroups = ["A+","A-","B+","B-","AB+","AB-","O+","O-"]
        self.openPickerView()
        txtBloodGroup.text = "A+"
    }
    @IBAction func btnSelectBloodsize(_ sender: AnyObject) {
        
      //  scrollView.contentOffset = CGPoint(x: 0, y: 100)
        self.resignKeyboard()
        intTag = 444
        arrBloodSize = ["50 ml","100 ml","200 ml","300 ml","400 ml","500 ml"]
        self.openPickerView()
        txtRequiredBloodSize.text = "50 ml"
    }
    // MARK: - Text field delegate code
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        //self.dismissPicker()
        return textField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        let iOSDeviceScreenSize = UIScreen.main.bounds.size
        if iOSDeviceScreenSize.height == 480 {
            
            if textField.tag == 111 {
                self.dismissPicker()
            }
            if textField.tag == 222 {
                self.dismissPicker()
            }
            if textField.tag == 333 {
               // scrollView.contentOffset = CGPoint(x: 0, y: 50)
                self.resignKeyboard()
                intTag = 333
                arrBloodGroups = ["A+","A-","B+","B-","O+","O-"]
                self.openPickerView()
                txtBloodGroup.text = "A+"
                return false
            }
            if textField.tag == 444 {
               // scrollView.contentOffset = CGPoint(x: 0, y: 50)
                self.resignKeyboard()
                intTag = 444
               arrBloodSize = ["50 ml","100 ml","200 ml","300 ml","400 ml","500 ml"]
                self.openPickerView()
                txtRequiredBloodSize.text = "50 ml"
                return false
            }
            else if textField.tag == 555{
              //  scrollView.contentOffset = CGPoint(x: 0, y: 100)
                
            }
            return true
        }
        else
        {
            
        if textField.tag == 111 {
            self.dismissPicker()
        }
        if textField.tag == 222 {
            self.dismissPicker()
        }
        if textField.tag == 333 {
            
            self.resignKeyboard()
            intTag = 333
            arrBloodGroups = ["A+","A-","B+","B-","O+","O-"]
            self.openPickerView()
            txtBloodGroup.text = "A+"
            return false
        }
        if textField.tag == 444 {
         //   scrollView.contentOffset = CGPoint(x: 0, y: 50)
            self.resignKeyboard()
            intTag = 444
            arrBloodSize = ["50 ml","100 ml","200 ml","300 ml","400 ml","500 ml"]
            self.openPickerView()
            txtRequiredBloodSize.text = "50 ml"
            return false
        }
        else if textField.tag == 555{
           // scrollView.contentOffset = CGPoint(x: 0, y: 80)
        }
        return true
        
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtContactNumber {
            let oldLength = textField.text?.characters.count
            let replacementLength = string.characters.count
            let rangeLength = range.length
            let newLength = oldLength! - rangeLength + replacementLength
            // var returnKey = (string as NSString).rangeOf("\n").location != NSNotFound
            return newLength <= 10
        }
        else {
            return true
        }
    }

    func addToolBar(_ textField: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.resignKeyboard))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    
    // MARK: - Picker View
    
    func openPickerView() {
        
        customPickerView.center = CGPoint(x: 160, y: 1100)
        customPickerView.backgroundColor = UIColor.lightGray
        if customPickerView != nil {
            customPickerView.removeFromSuperview()
            customPickerView = nil
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.commitAnimations()
        let iOSDeviceScreenSize = UIScreen.main.bounds.size
        if iOSDeviceScreenSize.height == 480 {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        else {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        self.view.addSubview(customPickerView)
        //Adding toolbar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: iOSDeviceScreenSize.width, height: 44))
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        customPickerView.addSubview(toolBar)
        if iOSDeviceScreenSize.height == 480 {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        else {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        pickerView.showsSelectionIndicator = true
        customPickerView.addSubview(pickerView)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.commitAnimations()
        self.view.bringSubview(toFront: customPickerView)
    }
    func dismissPicker() {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        customPickerView.center = CGPoint(x: 160, y: 3000)
        UIView.commitAnimations()
    }
    func resignKeyboard() {
        self.view.endEditing(true)
    }
    // MARK: - Picker Delegate Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if intTag == 333 {
            return arrBloodGroups.count
        }
        else if intTag == 444 {
            return arrBloodSize.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if intTag == 333 {
            return arrBloodGroups.object(at: row) as? String
        }
        else if intTag == 444 {
            return arrBloodSize.object(at: row) as? String
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if intTag == 333 {
            txtBloodGroup.text = arrBloodGroups.object(at: row) as? String
        }
        else if intTag == 444 {
            txtRequiredBloodSize.text = arrBloodSize.object(at: row) as? String
        }
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let sectionWidth = 200
        return CGFloat(sectionWidth)
    }
    
 }
