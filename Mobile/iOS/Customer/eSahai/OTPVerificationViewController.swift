//
//  OTPVerificationViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class OTPVerificationViewController: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet var constraintMobileNumberTop: NSLayoutConstraint!
    @IBOutlet var txtMobileNumber: UITextField!
    @IBOutlet var txtOTP: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

    //    btnSubmit.isEnabled = true
        self.navigationController?.navigationBar.isHidden = true
        let iOSDeviceScreenSize = UIScreen.main.bounds.size
        if iOSDeviceScreenSize.height == 480 {
            constraintMobileNumberTop.constant = 15
        }
        txtMobileNumber.text = String(format: "%@%@","+",UserDefaults.standard.value(forKey:kMobileNumber)as! String)
        self.addToolBar(txtOTP)
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Otp Verification Screen")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        appDelegate.isAtDashBoard = false
    }
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Submit Clicked Code
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if !((txtOTP.text?.isEmpty)!)
        {
                let strEncryptedOtp = self.encryptMobileNummber(strOtp: txtOTP.text! as String)
                
                let  strUrl = String(format : "%@%@",kServerUrl,"app_user/verifyCustomerOTP") as NSString
            
          let token = FIRInstanceID.instanceID().token()!
            
            print("Token --> \(token)")
            print("Apns Token --> \(appDelegate.deviceTokenString)")
               // let dictParams = ["iOS":"true","otp": strEncryptedOtp,"customerMobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,kIMEI:UserDefaults.standard.value(forKey: kIMEI)as! String,"deviceType": "iPhone","deviceOs": "iOS",kDeviceToken:UserDefaults.standard.value(forKey: kDeviceToken)as! String,"fromApp":"true"] as NSDictionary
            
            let dictParams = ["otp": strEncryptedOtp,
                              "customerMobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,kIMEI:UserDefaults.standard.value(forKey: kIMEI)as! String,
                                "deviceType": "iOS",
                                "deviceOs":appDelegate.strDeviceOSVersion,
                                "deviceToken": token,
                                "app_version" : appDelegate.strAppVersion,
                                "fromApp":"true",
                                "iOS":"true"] as NSDictionary
            
            print("dic params \(dictParams)")
            
                let dictHeaders = ["":"","":""] as NSDictionary
            
                sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                    DispatchQueue.main.async()
                    {
                    let strStatusCode = result.value(forKey: "statusCode") as! String
                    
                    if strStatusCode == "300"
                    {
                        FBSDKAppEvents.logEvent("Complete Registration")
                        
                           let dictResult =   result.value(forKey: "responseData") as! NSDictionary
                            
                                UserDefaults.standard.setValue(dictResult.value(forKey: kToken) as! String, forKey: kToken)
                            UserDefaults.standard.setValue(dictResult.value(forKey: kCustomer_id) as! String, forKey: kCustomer_id)
                            UserDefaults.standard.set(true, forKey: kISSIGNEDIN)
                            UserDefaults.standard.synchronize()
                        //"new_user" = no;
                        if ((dictResult.value(forKey: "new_user") as! NSString) == "yes"){
                            UserDefaults.standard.set(true, forKey: "isFirstTime")
                        }else{
                            UserDefaults.standard.set(false, forKey: "isFirstTime")
                        }
                        
                        self.MovetoViewController()
                            //self.signIntoFirebaseWithToken(firebaseToken: result.value(forKey:"firebaseToken") as! String)
                    }
                    else if strStatusCode == "204"{
                        self.logout()
                    }
                    else
                    {
                       // self.view.makeToast(result.value(forKey:"statusMessage") as! String, duration:kToastDuration, position:CSToastPositionCenter)
                     
                        let alertController = UIAlertController(title: "", message: result.value(forKey:"statusMessage") as! String , preferredStyle: UIAlertControllerStyle.alert)
                        
                        let DestructiveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                        }
                        alertController.addAction(DestructiveAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                        }
                    }
                    }, failureHandler: {(error) in
                })
            }
        else{
            
            
           // self.view.makeToast("OTP should not empty!", duration:kToastDuration, position:CSToastPositionCenter)
            
            
                // self.view.makeToast(result.value(forKey:"statusMessage") as! String, duration:kToastDuration, position:CSToastPositionCenter)
                
                let alertController = UIAlertController(title: "", message: "OTP should not empty!", preferredStyle: UIAlertControllerStyle.alert)
                
                let DestructiveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                }
                alertController.addAction(DestructiveAction)
                self.present(alertController, animated: true, completion: nil)
        }
    }
    func encryptMobileNummber(strOtp : String) -> String
    {
        let key = "@*%$!eSaHai" as String
        let plain : NSData = strOtp.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) as NSData!
        let encryptedData = plain.aes256Encrypt(withKey: key).base64EncodedString(options: [])
        //print(encryptedData)
        return encryptedData
    }

    // MARK: - Text field delegate code
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtOTP {
            let oldLength = textField.text?.characters.count
            let replacementLength = string.characters.count
            let rangeLength = range.length
            let newLength = oldLength! - rangeLength + replacementLength
            
           /* if(newLength==6){
                btnSubmit.isEnabled = true

            }else{
                btnSubmit.isEnabled = false
            }*/
            return newLength <= 6
        }
        else {
            return true
        }
    }
    func addToolBar(_ textField: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    func doneClicked(){
        
        txtOTP.resignFirstResponder()
    }
    func MovetoViewController() {
        
       /* FIRAuth.auth()?.signIn(withCustomToken: firebaseToken, completion: {(user:FIRUser?,error)in
            
            if error != nil {
                 self.view.makeToast(error!.localizedDescription, duration:kToastDuration, position:CSToastPositionCenter)
                return
            }*/
        
        if(UserDefaults.standard.value(forKey:"isFirstTime") as! Bool)
        {
            let profileController  = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
            self.navigationController?.pushViewController(profileController, animated: true)
        }else{
          //  let homeViewCon  = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
           // self.navigationController?.pushViewController(homeViewCon, animated: true)
            
            let DashCon = self.storyboard?.instantiateViewController(withIdentifier:"DashboardViewController") as! DashboardViewController
            self.navigationController?.pushViewController(DashCon, animated: true)
        }
    }
}
