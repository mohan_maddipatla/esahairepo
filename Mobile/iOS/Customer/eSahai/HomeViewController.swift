//
//  HomeViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import CoreLocation
//import DLRadioButton
import FirebaseInstanceID
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class HomeViewController: BaseViewController,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,LocationDelegate,UIPopoverControllerDelegate {
    
    var alertController = UIAlertController()
    
    var coordinates = CLLocationCoordinate2D()
    let iOSDeviceScreenSize = UIScreen.main.bounds.size

    var customPickerView : UIView! = UIView()
    var pickerView : UIPickerView = UIPickerView()
   
    var intTag =  NSInteger()
    var arrayHospitals = NSMutableArray()
   
    // var arrAmbulances = NSMutableArray()
    //var arrForWhoom = NSMutableArray()
    var arrEmergencies = NSMutableArray()
    var arrHospitals = NSMutableArray()
    var arrHospitalList = NSMutableArray()
    
    
    var hospitalLatitude = Double()
    var hospitalLongitude = Double()
    var hospitalGroupId = String()
    var hospitalName = String()
    //var bookingDetailsString = String()
    var emergencyID = String()
    
    var ambulanceTypeID = NSInteger()
    
    var dictCurrentLocation = NSDictionary()
    var dictHospitalLocation = NSDictionary()
    var hospitalLocationDict = NSDictionary()
    
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtEmergency: UITextField!
    @IBOutlet weak var btnCurrentLocation: UIButton!
    @IBOutlet weak var btnHospitalLocation: UIButton!
    
    //1st Set of Radio
    @IBOutlet weak var anyAmbulanceRadio:UIButton!
    
    @IBOutlet weak var anyAmbulanceRadio2: UIButton!
    
    @IBOutlet weak var anyAmbulanceRadio3: UIButton!
    
    //2nd Set of Radio
    @IBOutlet weak var ambulanceForRadio: UIButton!
     @IBOutlet weak var ambulanceForRadio2: UIButton!
    
    // 3rd Set of Radio
    @IBOutlet weak var locationHospitalRadio: UIButton!
    @IBOutlet weak var locationHospitalRadio2: UIButton!
    
    
    @IBOutlet var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txtEmergency.delegate = self
        txtContact.isHidden = true
        topConstraint.constant = 30
        self.addToolBar(txtContact)
        self.setUpHomeView()
        
        let notificationName = Notification.Name("getActiveBooking")
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.getBooking), name: notificationName, object: nil)
        appDelegate.getGPSCoordinates()
    }

    func BookingConfirmed(notification:NSNotification) -> Void {
       // print("booking_Confirmed")
        
      
         let vc = self.storyboard?.instantiateViewController(withIdentifier:"BookingViewController")as! BookingViewController
        vc.timer.invalidate()
        vc.dismiss(animated: true, completion: nil)
        
       // vc.dismiss(animated: false) {
           /* let bookingTrackingController = self.storyboard?.instantiateViewController(withIdentifier: "BookingTrackingViewController") as! BookingTrackingViewController
          //  bookingTrackingController.delete = self
            self.navigationController?.pushViewController(bookingTrackingController, animated: true)*/
     //   }
      //  }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Home Screen")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Ambulance"
        navigationController?.navigationBar.barTintColor = UIColor(hex: "#195F92")
        navigationController!.navigationBar.tintColor = UIColor.white
        //navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        SlideNavigationController.sharedInstance().portraitSlideOffset = self.view.frame.size.width - 220
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    func setUpHomeView() {
        
        appDelegate.getGPSCoordinates()
        btnCurrentLocation.setTitle(UserDefaults.standard.value(forKey:kAddress) as? String , for: .normal)
        
        anyAmbulanceRadio.isSelected = true
        ambulanceForRadio.isSelected = true
        locationHospitalRadio.isSelected = true
        
        ambulanceTypeID = 8
        
        self.setTypeofEmergencyPicker()
        
    }
    func setTypeofEmergencyPicker() {
       
        let emergencyTypes  = UserDefaults.standard.value(forKey: "arrEmergencyTypeList") as! NSArray

        for dict in emergencyTypes {
             for (key,value) in dict as! NSDictionary {
                if (key as! String == "type_name") {
                    if !(value as! String == "Medical Taxi"){
                            arrEmergencies.add(value)
                    }
                }
            }
        }
        
        txtEmergency.text = arrEmergencies.object(at: 0) as? String
        
        let dic = emergencyTypes.object(at:0) as! NSDictionary
        emergencyID = dic["id"] as! String
        self.getNearbyHospitalsList()
        
    }
    
    // MARK: - Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.dismissPicker()
        return textField.resignFirstResponder()
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        
        if textField == txtEmergency {
            self.openPickerView()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtContact {
            let oldLength = textField.text?.characters.count
            let replacementLength = string.characters.count
            let rangeLength = range.length
            let newLength = oldLength! - rangeLength + replacementLength
            // var returnKey = (string as NSString).rangeOf("\n").location != NSNotFound
            return newLength <= 10
        }
        else {
            return true
        }
    }
    
    func addToolBar(_ textField: UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    
    // MARK: - Picker View
    func openPickerView() {
        customPickerView.center = CGPoint(x: 160, y: 1100)
        customPickerView.backgroundColor = UIColor.lightGray
        if customPickerView != nil {
            customPickerView.removeFromSuperview()
            customPickerView = nil
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.commitAnimations()
        if iOSDeviceScreenSize.height == 480 {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        else {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        self.view.addSubview(customPickerView)
        //Adding toolbar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: iOSDeviceScreenSize.width, height: 44))
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        customPickerView.addSubview(toolBar)
        if iOSDeviceScreenSize.height == 480 {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        else {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        pickerView.showsSelectionIndicator = true
        customPickerView.addSubview(pickerView)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.commitAnimations()
        self.view.bringSubview(toFront: customPickerView)
    }
    
    func dismissPicker() {
        
        if intTag != 111 && iOSDeviceScreenSize.height == 480 {
        // scrollView.setContentOffset(CGPoint(x: 0, y: 60), animated: true)
        }
        else
        {
        // scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        customPickerView.center = CGPoint(x: 160, y: 3000)
        UIView.commitAnimations()
    }
    
    // MARK: - Picker Delegate Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrEmergencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return arrEmergencies.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtEmergency.text = arrEmergencies.object(at: row) as? String
        self.getNearbyHospitalsList()
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let sectionWidth = 200
        return CGFloat(sectionWidth)
    }
    
     // MARK: - Book Ambulance
    @IBAction func btnSubmitClicked(_ sender: AnyObject) {
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        guard let builder = GAIDictionaryBuilder.createEvent(withCategory: kAnalyticsEventName, action: kCallAmbulanceButtonName, label:"CallAmbulance", value: nil)else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        if !(appDelegate.checkInternetConnectivity()){
            
            
                    self.alertController = UIAlertController(title: "", message: "No internet connection! Do you want to connect to Customer Service for booking?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                        return
                    }
                    let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        
                        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
                            else { return }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(number, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                                UIApplication.shared.openURL(url)
                            }
                        }
                        
                    }
                    self.alertController.addAction(DestructiveAction)
                    self.alertController.addAction(okAction)
                    self.present(self.alertController, animated: true, completion: nil)
            return
        }
        
        

        if(locationHospitalRadio.isSelected){
            if(btnHospitalLocation.titleLabel?.text == "No Nearby Hospitals found"){
                self.view.makeToast("Please select Valid Inputs", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }
        
        if(!ambulanceForRadio.isSelected){
            if(txtContact.text?.length == 0){
                self.view.makeToast("Please enter Contact Number", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
            
            if((txtContact.text?.length)! < 10){
                self.view.makeToast("Please enter valid Contact Number", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
            
        }
        
        if(!locationHospitalRadio.isSelected){
            if(dictHospitalLocation.count == 0){
                self.view.makeToast("Please set the Destination Location", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }
        //Comparing Current location and Hospital location to show alert
        if btnCurrentLocation.titleLabel?.text == btnHospitalLocation.titleLabel?.text
        {
            self.view.makeToast("Both locations are same", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
        
        if !(self.locationHospitalRadio.isSelected){
            var strCurLat = String()
            var strCurLon = String()

            if(dictCurrentLocation.count>0)
            {
                strCurLat = String(format: "%.3f", (dictCurrentLocation.value(forKey: "lat") as! Double))
                strCurLon = String(format: "%.3f", (dictCurrentLocation.value(forKey: "long") as! Double))
            }
            else
            {
                 strCurLat = String(format: "%.3f", (appDelegate.AppCurrentCoordinates.latitude as? Double)!)
                 strCurLon = String(format: "%.3f", (appDelegate.AppCurrentCoordinates.longitude as? Double)!)
            }
            
            
            let strHosLat = String(format: "%.3f", (dictHospitalLocation.value(forKey: "lat") as? Double)!)
            let strHosLon = String(format: "%.3f", (dictHospitalLocation.value(forKey: "long") as? Double)!)
            
            if strCurLat == strHosLat && strCurLon == strHosLon
            {
                self.view.makeToast("Both locations are same", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }
        
      let  strUrl = String(format : "%@%@",kServerUrl,"createBooking") as NSString
        var groupID = NSString()
        
        let emergencyTypes  = UserDefaults.standard.value(forKey: "arrEmergencyTypeList") as! NSArray
        for emergency in emergencyTypes {
            let emer = emergency as! NSDictionary
            if(emer["type_name"] as? String == txtEmergency.text){
                emergencyID = emer["id"] as! String
            }
        }
        
        
        if(locationHospitalRadio.isSelected){
            if(hospitalLocationDict.count == 0){
                hospitalLatitude = 0.00
                hospitalLongitude = 0.00
                hospitalName = "Any nearby Hospital"
                groupID = "Any"
            }else{
            hospitalLatitude = hospitalLocationDict.value(forKey: "latitude") as! Double
            hospitalLongitude = hospitalLocationDict.value(forKey: "longitude") as! Double
            hospitalName = hospitalLocationDict.value(forKey: "Hospital Name") as! String
            groupID = hospitalLocationDict.value(forKey:"Group_id") as! NSString
            }
        }else{
            hospitalLatitude = dictHospitalLocation.value(forKey: "lat") as! Double
                //Double(dictHospitalLocation.value(forKey: "lat") as! String)!
            hospitalLongitude = dictHospitalLocation.value(forKey: "long") as! Double
                //Double(dictHospitalLocation.value(forKey: "long") as! String)!
            hospitalName = dictHospitalLocation.value(forKey: "address") as! String
            groupID = "Any"
        }
    
        var forSelf = NSString()
        if(ambulanceForRadio.isSelected){
            forSelf = "1"
        }else{
            forSelf = "0"
        }
        
        var patientContact = String()
        if(forSelf).boolValue{
            patientContact = ""
        }else{
            patientContact = txtContact.text!
        }
        
        let hospitalGeohash = Geohash.encode(latitude:hospitalLatitude , longitude:hospitalLongitude) as String
        
        var name = NSString()
        if((UserDefaults.standard.value(forKey: "customer_name")) != nil){
            name = UserDefaults.standard.value(forKey: "customer_name")as! NSString
        }else{
            name = "No Name"
        }
        
        var pickUpCoordinates = CLLocationCoordinate2D()
        
        if(dictCurrentLocation.count>0){
            pickUpCoordinates.latitude = dictCurrentLocation.value(forKey: "lat") as! Double
            pickUpCoordinates.longitude = dictCurrentLocation.value(forKey: "long") as! Double
        }else{
            pickUpCoordinates = appDelegate.AppCurrentCoordinates
        }
        
        let personalDetails = [
            "customer_name": name ,
            "customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
            "patient_number" : patientContact,
            "booked_by": "Customer",
            "emergency_type": txtEmergency.text ?? "",
            "emergency_longitude": pickUpCoordinates.longitude ,
            "emergency_latitude": pickUpCoordinates.latitude ,
            "emergency_geoHashCode":Geohash.encode(latitude: pickUpCoordinates.latitude, longitude: pickUpCoordinates.longitude),
            "hospital": hospitalName,
            "customer_id": UserDefaults.standard.value(forKey: kCustomer_id)as! String ,
            "is_self": forSelf] as NSMutableDictionary
        
        if(!locationHospitalRadio.isSelected){
           personalDetails.setValue(hospitalLatitude, forKey: "drop_latitude")
            personalDetails.setValue(hospitalLongitude, forKey: "drop_longitude")
            personalDetails.setValue(hospitalGeohash, forKey: "drop_geoHashCode")
        }
        
        
        let token = FIRInstanceID.instanceID().token()
         let dictParams = [
            "params":personalDetails,
            "customerApp": "true",
            "customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
             "group_type_id": ambulanceTypeID ,
             "group_id": groupID,
             "emergency_type": emergencyID,
             "gcmToken": token ?? "",
             "hospital_latitude": hospitalLatitude,
             "hospital_longitude": hospitalLongitude,
             "hospital_geoHashCode":hospitalGeohash,
            "address" : self.btnCurrentLocation.titleLabel?.text
               // UserDefaults.standard.value(forKey:kAddress)
            ] as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            
            let strStatusCode = result["statusCode"] as? String
            if strStatusCode == nil
            {
                
            }
            else
            {
                if strStatusCode == "300"
                {
                     FBSDKAppEvents.logEvent(kGAIPAPurchase)
                    
                    let bookingLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
                    
                    self.appDelegate.bookingID = (result["responseData"] as! NSDictionary).value(forKey: "booking_id") as! NSString
                    
                    UserDefaults.standard.set( (result["responseData"] as! NSDictionary).value(forKey: "booking_id") as! NSString, forKey: "bookingid")
                    
                    bookingLocationCon.iscoming = "Ambulance"
                    
                    //print(result["responseData"] as! NSDictionary)
                    self.navigationController?.pushViewController(bookingLocationCon, animated: true)
                }else if strStatusCode == "204"{
                    self.logout()
                }
                else if(strStatusCode == "411"){
                     self.alertController = UIAlertController(title: "", message: result["statusMessage"] as! String, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                        
                    }
                    let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        
                        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
                            else { return }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(number, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                                UIApplication.shared.openURL(url)
                            }
                        }
                        
                    }
                    self.alertController.addAction(DestructiveAction)
                    self.alertController.addAction(okAction)
                    self.present(self.alertController, animated: true, completion: nil)
                }
                else
                {
                    DispatchQueue.main.async(){
                       // self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
                }
            }
        }, failureHandler: {(error) in
        })
    }
    
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController!) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .none
    }
    
    // MARK: - Location setup Actions
    @IBAction func btnCurrentLocationAction(_ sender: Any) {
        let selectLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectLocationCon.delegate = self
        selectLocationCon.isComingFrom = "Current Location"
        self.navigationController?.pushViewController(selectLocationCon, animated: true)
    }
    
    @IBAction func btnHospitalLocationAction(_ sender: Any) {
        
        if(locationHospitalRadio.isSelected){
            if(btnHospitalLocation.titleLabel?.text == "No Nearby Hospitals found"){
                self.view.makeToast("Please select Valid Inputs", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }
        
        if(locationHospitalRadio.isSelected){
            
            let emergencyTypes  = UserDefaults.standard.value(forKey: "arrEmergencyTypeList") as! NSArray
            for emergency in emergencyTypes {
                let emer = emergency as! NSDictionary
                if(emer["type_name"] as? String == txtEmergency.text){
                    emergencyID = emer["id"] as! String
                }
            }
            
            
            
            
            let nearestLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "NearestListMapViewController") as! NearestListMapViewController
            
            if(dictCurrentLocation.count>0)
            {
                nearestLocationCon.coordinates = CLLocationCoordinate2D(latitude: (dictCurrentLocation.value(forKey: "lat") as! Double), longitude: (dictCurrentLocation.value(forKey: "long") as! Double))
            }
            else
            {
                nearestLocationCon.coordinates = appDelegate.AppCurrentCoordinates
            }
            
            
            nearestLocationCon.delegate = self
            nearestLocationCon.IDemergency = emergencyID
            self.navigationController?.pushViewController(nearestLocationCon, animated: true)
        }else{
            let selectLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
            selectLocationCon.delegate = self
            selectLocationCon.isComingFrom = "Hospital Location"
            self.navigationController?.pushViewController(selectLocationCon, animated: true)
        }
    }
    
    func doneClicked(){
        
        txtContact.resignFirstResponder()
    }
    
    // MARK: - Radio Button code
    @IBAction fileprivate func ambulanceType( sender : UIButton) {
        
        if (sender.tag == 101) {
             ambulanceTypeID = 8
            self.anyAmbulanceRadio.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.anyAmbulanceRadio2.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.anyAmbulanceRadio3.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)

        }else if(sender.tag == 102){
            ambulanceTypeID = 9
            self.anyAmbulanceRadio.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.anyAmbulanceRadio2.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.anyAmbulanceRadio3.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            
        }else if(sender.tag == 103) {
            ambulanceTypeID = 10
            self.anyAmbulanceRadio.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.anyAmbulanceRadio2.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.anyAmbulanceRadio3.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
        }
    }
    
    @IBAction fileprivate func ambulanceFor( _sender : UIButton){
        
        if (_sender.tag == 201) {
            txtContact.isHidden = true
            topConstraint.constant = 30
            
            self.ambulanceForRadio.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.ambulanceForRadio2.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.ambulanceForRadio.isSelected = true
        }else if(_sender.tag == 202){
           txtContact.isHidden = false
            topConstraint.constant = 67
            self.ambulanceForRadio.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.ambulanceForRadio2.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.ambulanceForRadio.isSelected = false
        }
    }
    
    @IBAction fileprivate func whereTo( sender : UIButton){
        
        if (sender.tag == 301) {
            
            self.locationHospitalRadio.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.locationHospitalRadio2.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.locationHospitalRadio.isSelected = true
            
            if !(self.arrayHospitals.count > 0 ){
                btnHospitalLocation.setTitle("No Nearby Hospitals found", for: .normal)
                return
            }
            if (hospitalLocationDict.count > 0) {
                btnHospitalLocation.setTitle(hospitalLocationDict.value(forKey:"Hospital Name") as! String?, for: .normal)
            }else {
                btnHospitalLocation.setTitle(" Any Near Hospital or Select a Hospital", for: .normal)
            }
        }else if(sender.tag == 302){
            self.locationHospitalRadio.isSelected = false
            self.locationHospitalRadio.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.locationHospitalRadio2.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            
            if (dictHospitalLocation.count > 0) {
                btnHospitalLocation.setTitle(dictHospitalLocation.value(forKey:"address") as! String?, for: .normal)
            }else {
               btnHospitalLocation.setTitle("Select drop location", for: .normal)
            }
        }
    }
    
    // MARK: - Delegate Call backs
    func getUserLocation(dict : NSDictionary){
        dictCurrentLocation = dict
        btnCurrentLocation.setTitle(dict.value(forKey:"address") as! String?, for: .normal)
        self.getNearbyHospitalsList()
    }
    func getHospitalLocation(dict : NSDictionary){
     
        dictHospitalLocation = dict
        //print(dict)
        btnHospitalLocation.setTitle(dict.value(forKey:"address") as! String?, for: .normal)
    }
    
    func getHospitalLocationDict(dict : NSDictionary){
        hospitalLocationDict = dict
       // print(dict)
        btnHospitalLocation.setTitle(dict.value(forKey:"Hospital Name") as! String?, for: .normal)
    }
    
    
    func getBooking(){
        
        
        let  strUrl = String(format : "%@%@",kServerUrl,"checkMyBookingStatus") as NSString
        
        let dictParams = ["iOS":"true",
                          "customerApp": "true",
                          "ambulanceApp": "false",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                          "booking_id" :UserDefaults.standard.value(forKey: "bookingid") as! String]
            as NSDictionary
        
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
     //   print(dictParams)
       // print(dictHeaders)
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                
                if strStatusCode == "413"{
                    print("No Ambulances Found")
                    
                     self.alertController = UIAlertController(title: "", message: "No Ambulance found for the given criteria.. Connect to customer Service?" , preferredStyle: UIAlertControllerStyle.alert)
                    
                    let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                        
                    }
                    let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        
                        
                        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
                            else { return }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(number, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                                UIApplication.shared.openURL(url)
                            }
                        }
                        
                    }
                    
                    self.alertController.addAction(DestructiveAction)
                    self.alertController.addAction(okAction)
                    self.present(self.alertController, animated: true, completion: nil)
                    
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
                else
                {
                    //self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
    
    
    // MARK: - get Nearest Hospital
    
    func getNearbyHospitalsList() {
        
        let emergencyTypes  = UserDefaults.standard.value(forKey: "arrEmergencyTypeList") as! NSArray
        for emergency in emergencyTypes {
            let emer = emergency as! NSDictionary
            if(emer["type_name"] as? String == txtEmergency.text){
                emergencyID = emer["id"] as! String
            }
        }
        var HomeCoordinates = CLLocationCoordinate2D()
        if(dictCurrentLocation.count>0){
            HomeCoordinates.latitude = dictCurrentLocation.value(forKey: "lat") as! Double
            HomeCoordinates.longitude = dictCurrentLocation.value(forKey: "long") as! Double
        }else{
            HomeCoordinates = appDelegate.AppCurrentCoordinates
        }
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getNearByHospitalList") as NSString
        let dictParams = ["iOS":"true",
                          "emergencyGeoHash": Geohash.encode(latitude: HomeCoordinates.latitude, longitude: HomeCoordinates.longitude ) ,
                          kLatitude:HomeCoordinates.latitude ,
                          "longitude": HomeCoordinates.longitude,
                          "customerApp": "true",
                          "ambulanceApp": "false",
                          "emergency_type" : emergencyID,
                          "groupType":"2",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300"
                {
                    self.arrayHospitals = result["responseData"] as! NSMutableArray
                    if !(self.arrayHospitals.count>0){
                        if (self.locationHospitalRadio.isSelected){
                            self.btnHospitalLocation.setTitle("No Nearby Hospitals found", for: .normal)
                        }
                    }else{
                
                        //if !(self.arrayHospitals.count>0){
                            if (self.locationHospitalRadio.isSelected){
                                self.btnHospitalLocation.setTitle("  Any Near Hospital or Select a Hospital", for: .normal)
                            }
                        //}
                    }
                    
                }else if strStatusCode == "303"
                {
                    if (self.locationHospitalRadio.isSelected){
                        self.btnHospitalLocation.setTitle("No Nearby Hospitals found", for: .normal)
                    }

                }
                
            }
        }, failureHandler: {(error) in})
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.alertController.dismiss(animated: true, completion: nil)
    }
}
