//
//  NearestListMapViewController.swift
//  eSahai
//
//  Created by PINKY on 17/02/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class NearestHospitalsAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var id: String!
    var name: String!
    var attributes = NSArray()
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}

class AnnotationView: MKAnnotationView{
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if (hitView != nil)
        {
            self.superview?.bringSubview(toFront: self)
        }
        return hitView
    }
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds;
        var isInside: Bool = rect.contains(point);
        if(!isInside)
        {
            for view in self.subviews
            {
                isInside = view.frame.contains(point);
                if isInside
                {
                    break;
                }
            }
        }
        return isInside;
    }
}

class NearestListMapViewController: BaseViewController,MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    var coordinates = CLLocationCoordinate2D()

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet var mapViewObj: MKMapView!
   
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    var arrPredictions = NSMutableArray()
    var userLocationFromSearch = CLLocationCoordinate2D()
    var arrHospitals = NSMutableArray()
    var HospitalsArr = NSMutableArray()
    var IDemergency = String()

    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var locationsTableView: UITableView!
    
    var myTimer = Timer()
    var delegate : LocationDelegate?
    
    var circle:MKCircle!

    @IBOutlet weak var distanceTableView: UITableView!
    var arrDistances = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpMapView()
        searchBar.delegate = self
        distanceTableView.isHidden = true
        self.addToolBar(searchBar)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.HospitalsArr.removeAllObjects()
        self.arrDistances.removeAllObjects()
    }
    func addToolBar(_ searchBar: UISearchBar) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.resignKeyboard))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        searchBar.inputAccessoryView = toolBar
    }
    func resignKeyboard() {
        searchBar.resignFirstResponder()
    }
    // MARK: - MapView Set Up
    func setUpMapView(){
      
        //appDelegate.getGPSCoordinates()
        //coordinates = appDelegate.AppCurrentCoordinates
        
        mapViewObj.delegate = self
        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinates, 20000, 20000)
        mapViewObj.setRegion(viewRegion, animated: true)
    }

    // MARK: - Circle Overlay
    func loadOverlayForRegionWithLatitude(_ latitude: Double, andLongitude longitude: Double) {
        let Coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        circle = MKCircle(center: Coordinates, radius: 5000)
        mapViewObj.removeOverlays(mapViewObj.overlays)
        mapViewObj.add(circle)
        self.getNearbyHospitalsList(latitude, andLongitude: longitude)
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.fillColor = UIColor.blue.withAlphaComponent(0.1)
        circleRenderer.strokeColor = UIColor.blue
        circleRenderer.lineWidth = 1
        return circleRenderer
    }

    // MARK: - Annotations
    func setAnnotationMarkers() {
        for i in 0...(self.HospitalsArr.count - 1)
        {
            let tempdict = self.HospitalsArr.object(at: i) as! NSMutableDictionary
            
            let xyz =  CLLocationCoordinate2D(latitude: (tempdict.value(forKey: "latitude") as! CLLocationDegrees ), longitude: (tempdict.value(forKey: "longitude") as! CLLocationDegrees))
            
            let point = NearestHospitalsAnnotation(coordinate: xyz)
            
           // point.title = tempdict.value(forKey: "name") as! String!
            point.name = tempdict.value(forKey: "name") as! String!
            point.id = tempdict.value(forKey: "id") as! String!
            
            let attrArray = tempdict.value(forKey: "attributes") as? NSArray
            if((attrArray?.count)! > 0){
                 point.attributes = tempdict.value(forKey: "attributes") as! NSArray
            }else{
                point.attributes = []
            }

           
            self.mapViewObj.addAnnotation(point)
        }
        MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
    }
    
    // MARK: - MapView Annotation Delegate Methods
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
           // annotationView?.canShowCallout = false
        }
        
       //  let btn = UIButton(type: .detailDisclosure)
       // annotationView?.rightCalloutAccessoryView = btn
        annotationView?.image = UIImage(named: "Location Icon-01.png")
        return annotationView
    }
    
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        
        
        let customAnnotation = view.annotation as! NearestHospitalsAnnotation
        let views = Bundle.main.loadNibNamed("CustomCallOut", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCallOut
        
        calloutView.HospitalName.text = customAnnotation.name
        calloutView.Attr1.isHidden = false
        calloutView.Attr2.isHidden = false
        calloutView.Attr3.isHidden = false
        
        switch customAnnotation.attributes.count {
        case 0:
            calloutView.Attr1.isHidden = true
            calloutView.Attr2.isHidden = true
            calloutView.Attr3.isHidden = true
        case 1:
            calloutView.Attr1.text = "\((customAnnotation.attributes.object(at: 0) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:0) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr2.isHidden = true
            calloutView.Attr3.isHidden = true

        case 2:
            calloutView.Attr1.text = "\((customAnnotation.attributes.object(at: 0) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:0) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr2.text = "\((customAnnotation.attributes.object(at: 1) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:1) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr3.isHidden = true
            
        case 3:
            calloutView.Attr1.text = "\((customAnnotation.attributes.object(at: 0) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:0) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr2.text = "\((customAnnotation.attributes.object(at: 1) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:1) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr3.text = "\((customAnnotation.attributes.object(at: 2) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:2) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"

        default:
            break
        }
        
       // calloutView.attributeDetails = customAnnotation.attributes
       // calloutView.attrTblView.reloadData()
      //  calloutView.attrTblView.sizeToFit()
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        
        calloutView.btnClickable.tag = Int(customAnnotation.id)!
        
        calloutView.btnClickable.addTarget(self, action: #selector(self.selectHospital(sender:)), for: .touchUpInside)
        
        
        
        view.addSubview(calloutView)
    }
    
    func selectHospital(sender: UIButton)
    {
        
        print(sender.tag)
        
        for i in 0...(self.HospitalsArr.count - 1)
        {
            let tempdict = self.HospitalsArr.object(at: i) as! NSMutableDictionary
            
            if(sender.tag == Int(tempdict.value(forKey: "id") as! String!)){
                let nearestHospitalDict = [
                    "Hospital Name": tempdict.value(forKey: "name") as! String!,
                    "Group_id":tempdict.value(forKey: "id") as! String!,
                    "latitude": (tempdict.value(forKey: "latitude") as! CLLocationDegrees ),
                    "longitude" : (tempdict.value(forKey: "longitude") as! CLLocationDegrees)] as NSDictionary
                
                delegate?.getHospitalLocationDict(dict:nearestHospitalDict)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                
                subview.removeFromSuperview()
            }
        }
    }
    
    
   /* func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if view.annotation is MKUserLocation
        {
            return
        }
        let testAnnotation = view.annotation as! NearestHospitalsAnnotation
        let nearestHospitalDict = ["Hospital Name": testAnnotation.name,"Group_id":testAnnotation.id,"latitude":testAnnotation.coordinate.latitude , "longitude" : testAnnotation.coordinate.longitude] as NSDictionary
        
        delegate?.getHospitalLocationDict(dict:nearestHospitalDict as NSDictionary)
        self.navigationController?.popViewController(animated: true)
    }*/
    
    // MARK: - MapView  Delegate Methods

    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool){
        
        MBProgressHUD.hide(for: appDelegate.window, animated: true)
        MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool)
    {
        coordinates = self.mapViewObj.centerCoordinate
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        if !(appDelegate.checkInternetConnectivity()){
            MBProgressHUD.hideAllHUDs(for: appDelegate.window, animated: true)
            appDelegate.window?.makeToast(kNetworkStatusMessage, duration: kToastDuration, position: CSToastPositionBottom)
            return
        }
        
        geocoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in
            if error != nil {
                print("Error getting location2 : \(error)")
            }else {
                let placeArray = placemarks as [CLPlacemark]!
                var placeMark: CLPlacemark!
                placeMark = placeArray?[0]
                
                
                let strAddress = String(format: "%@,%@,%@,%@,%@,%@,%@,\n%@",(placeMark.name) != nil ? placeMark.name!: "",(placeMark.thoroughfare) != nil ? placeMark.thoroughfare!: "",(placeMark.locality) != nil ? placeMark.locality!:"",(placeMark.subLocality) != nil ? placeMark.subLocality!:"",(placeMark.subAdministrativeArea) != nil ? placeMark.subAdministrativeArea!:"",(placeMark.administrativeArea) != nil ? placeMark.administrativeArea!:"",(placeMark.country) != nil ? placeMark.country!:"",(placeMark.postalCode) != nil ? placeMark.postalCode!:"")
                if strAddress != ",,,,,,\n" {
                    
                    self.searchBar.text = strAddress
                    self.loadOverlayForRegionWithLatitude(self.coordinates.latitude, andLongitude:self.coordinates.longitude )
                }
            }
        }
    }
    
    // MARK: - Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if appDelegate.checkInternetConnectivity() == false && searchText == self.searchBar.text {
            
            self.view.makeToast(kNetworkStatusMessage, duration:kToastDuration, position:CSToastPositionTop)
            locationsTableView.isHidden = true
            return
        }
        self.getLocationsBasedOnAddress()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        return true
    }
    
    
    // MARK: - TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == distanceTableView {
        
           return self.arrDistances.count
        }
            return 1
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if tableView == distanceTableView {
            if let headerView = view as? UITableViewHeaderFooterView {
                let abc : Int = (((arrDistances.object(at: section) as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "multiplier") as! Int) * 5
                
                let aStr = String(format: "Hospitals from (%d-%d)Kms", abc-5, abc)
                
                headerView.textLabel?.text = aStr
                headerView.textLabel?.font = headerView.textLabel?.font.withSize(16)
                headerView.textLabel?.textColor = UIColor.white
                headerView.textLabel?.textAlignment = .center
                headerView.contentView.backgroundColor = UIColor(hex :"#1C73A4")
                }
            }else{
                if let headerView = view as? UITableViewHeaderFooterView {
                headerView.frame = CGRect.zero
            }
        }
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (tableView == distanceTableView){
            return 40.0
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (tableView == distanceTableView){
            return (self.arrDistances[section] as! NSArray).count
        }else if (tableView == locationsTableView){
            return self.arrPredictions.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (tableView == distanceTableView){
            let cell1 : UITableViewCell = UITableViewCell(style:.default, reuseIdentifier: "identifier")
            let tempArray = self.arrDistances.object(at: indexPath.section) as! NSArray
            cell1.backgroundColor = UIColor.clear
            cell1.textLabel?.textAlignment = .center
            cell1.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            cell1.textLabel?.textColor = UIColor.black
            cell1.textLabel?.text = (tempArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "name") as! String?
            return cell1
        } else {
            let cell : UITableViewCell = UITableViewCell(style:.default, reuseIdentifier: "identifier")
            let tempDict = self.arrPredictions.object(at: indexPath.row) as! NSDictionary
            cell.backgroundColor = UIColor.clear
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
            cell.textLabel?.numberOfLines = 2
            cell.textLabel?.textColor = UIColor.black
            cell.textLabel?.text = tempDict.value(forKey: "description") as! String?
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == distanceTableView){
            
            let testDict = (arrDistances.object(at: indexPath.section) as! NSArray).object(at: indexPath.row) as! NSDictionary
        
            let nearestHospitalDict = ["Hospital Name": testDict.value(forKey: "name")! ,"Group_id": testDict.value(forKey: "id")!,"latitude":testDict.value(forKey: "latitude")! , "longitude" :testDict.value(forKey: "longitude")! ] as NSDictionary
            delegate?.getHospitalLocationDict(dict:nearestHospitalDict as NSDictionary)
            self.navigationController?.popViewController(animated: true)
            
        }else{
            locationsTableView.isHidden = true
            let tempDict = self.arrPredictions.object(at: indexPath.row) as! NSDictionary
            self.searchBar.text = tempDict.value(forKey: "description") as! String?
            self.searchBar.resignFirstResponder()
            MBProgressHUD.hide(for: appDelegate.window, animated: true)
            MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
            self.getCoordinateForAddress(address: self.searchBar.text!)
        }
    }
    
    // MARK: - Search Field Methods
    
    func getLocationsBasedOnAddress() {
        
        if ((self.searchBar.text?.length)! > 1) {
            let strAddress = self.searchBar.text?.replacingOccurrences(of:" ", with: "%22", options: .literal, range: nil)
            
          //  let  strUrl = String(format :"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(kAPI_Key)&types=geocode&key=%@",strAddress!,) as NSString
            
        
            
            let strUrl =  "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(kAPI_Key)&input=\(strAddress!)"
            
            
            let url : NSURL = NSURL(string: strUrl as String)!
            
            sharedController.requestGETURL(strURL:url, success:{(result) in
                DispatchQueue.main.async()
                    {
                        self.arrPredictions.removeAllObjects()
                        self.locationsTableView.isHidden = false
                        self.arrPredictions = result.value(forKey: "predictions") as! NSMutableArray
                        self.locationsTableView.reloadData()
                //        heightConstraint = self.locationsTableView.contentSize.height
                }
            }, failure:  {(error) in
            })
        }
        else
        {
            locationsTableView.isHidden = true
        }
    }
    
    
     func getCoordinateForAddress(address : String) {
        
        if ((address.length) > 1) {
            let strAddress = self.searchBar.text?.replacingOccurrences(of:" ", with: "%22", options: .literal, range: nil)
            let strUrl = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=\(strAddress!)&key=\(kAPI_Key)"
            let url : NSURL = NSURL(string: strUrl)!
            
            sharedController.requestGETURL(strURL:url, success:{(result) in
                DispatchQueue.main.async()
                    {
                        let status = result["status"] as! String
                        
                        if( status == "ZERO_RESULTS" ||
                            status == "REQUEST_DENIED" ||
                            status == "INVALID_REQUEST" ||
                            status == "INVALID_REQUEST" ){
                            
                            //self.Test(address: self.searchBar.text! )
                            self.searchBar.text = ""

                            self.view.makeToast("Geo Location not found", duration:kToastDuration, position:CSToastPositionTop)
                            
                        }else if(status == "OK") {
                        if let result = result["results"] as? NSArray {
                            let geometry = (result[0] as? NSDictionary)?.value(forKey:"geometry") as? NSDictionary
                            let loco = geometry?.value(forKey: "location") as! NSDictionary
                            let adddressLatitude = loco.value(forKey: "lat")
                            let addressLongitude = loco.value(forKey: "lng")
                            let addressCoordinates = CLLocationCoordinate2DMake(adddressLatitude as! CLLocationDegrees, addressLongitude as! CLLocationDegrees)
                           
                            let annotationPoint = MKPointAnnotation()
                            annotationPoint.coordinate = addressCoordinates
                            
                            self.mapViewObj.addAnnotation(annotationPoint)
                            let viewRegion = MKCoordinateRegionMakeWithDistance(annotationPoint.coordinate, 20000, 20000)
                            self.mapViewObj.setRegion(viewRegion, animated: true)
                             MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
                        }
                        }
                }
            }, failure:  {(error) in
            })
        }
    }
    
    // MARK: - Get Hospitals
    
    func getNearbyHospitalsList(_ latitude: Double, andLongitude longitude: Double) {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getNearByHospitalList") as NSString
        
        let dictParams = ["iOS":"true",
                          "emergencyGeoHash": Geohash.encode(latitude:latitude , longitude: longitude) ,
                          kLatitude: latitude,
                          "longitude": longitude,
                          "customerApp": "true",
                          "ambulanceApp": "false",
                          "emergency_type" : IDemergency,
                          "groupType" : "2",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        self.HospitalsArr.removeAllObjects()
        self.arrDistances.removeAllObjects()
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300" {
                    self.arrHospitals =  result["responseData"] as! NSMutableArray
                    
                     let tempArray  = NSMutableArray()
                     var tempxyz = Int()
                    for i in 0..<self.arrHospitals.count{
                       
                        let tempDict = self.arrHospitals.object(at: i) as! NSDictionary
                        let tempdict1 = NSMutableDictionary()
                        
                        tempdict1["name"] = tempDict["group_name"]!
                        tempdict1["latitude"] = Double(tempDict.value(forKey:"group_latitude") as! String)
                        tempdict1["longitude"] = Double(tempDict.value(forKey:"group_longitude") as! String)
                        tempdict1["id"] = tempDict["group_id"]!
                        tempdict1["attributes"] = tempDict["attributes"]
                        self.HospitalsArr.add(tempdict1)
                        
                        let distance = Double(tempDict["distance"] as! String)
                        let dividet : Double = (distance!/Double(5))
                        let xyz : Int = Int(dividet)
                        
                        tempdict1["distance"] = tempDict["distance"]!
                        tempdict1["multiplier"] = xyz + 1
                        
                        if(xyz != tempxyz){
                            let abc = NSMutableArray()
                            if(tempArray.count > 0){
                                for i in 0..<tempArray.count {
                                    abc.add(tempArray.object(at: i))
                                }
                                self.arrDistances.add(abc)
                                tempArray.removeAllObjects()
                            }
                        }
                        tempArray.add(tempdict1)
                        tempxyz = xyz
                        
                        if( i == (self.arrHospitals.count - 1) ){
                            if(tempArray.count > 0){
                                let abc = NSMutableArray()
                                for i in 0..<tempArray.count {
                                    abc.add(tempArray.object(at: i))
                                }
                                self.arrDistances.add(abc)
                                tempArray.removeAllObjects()
                            }
                        }
                    }
                     self.setAnnotationMarkers()
                }
                else if strStatusCode == "204"{
                    MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
                    self.arrHospitals.removeAllObjects()
                    self.arrDistances.removeAllObjects()
                    self.logout()
                }
                else
                {
                    MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
                    self.arrHospitals.removeAllObjects()
                    self.arrDistances.removeAllObjects()
                   // self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }

    // MARK: - Button Actions
    
    @IBAction func MapViewAction(_ sender: Any) {
        distanceTableView.isHidden = true
        self.setUpMapView()
    }
    
    @IBAction func ListViewAction(_ sender: Any) {
        distanceTableView.reloadData()
        distanceTableView.isHidden = false
    }
    
    @IBAction func CurrentLocationAction(_ sender: Any) {
        appDelegate.getGPSCoordinates()
        coordinates = appDelegate.AppCurrentCoordinates
        loadOverlayForRegionWithLatitude(coordinates.latitude, andLongitude: coordinates.longitude)
    }
}
