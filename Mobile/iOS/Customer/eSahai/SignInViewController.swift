//
//  SignInViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import FirebaseInstanceID

class SignInViewController: BaseViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
  
    var arrCountryNames = NSArray()
    var arrCountryList = NSArray()
    var customPickerView : UIView! = UIView()
    var pickerView : UIPickerView = UIPickerView()
    let iOSDeviceScreenSize = UIScreen.main.bounds.size
    
    
    @IBOutlet weak var txtCountryCode: UITextField!

    @IBOutlet var btnSelectTermsAndConditions_Outlet: UIButton!
    @IBOutlet var txtMobieNumber: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
  
    @IBOutlet weak var CountryNames: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        SlideNavigationController.sharedInstance().viewControllers.remove(at: 0)
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Sign In Screen")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        if UserDefaults.standard.bool(forKey:kISSIGNEDIN) {
            let DashCon = self.storyboard?.instantiateViewController(withIdentifier:"DashboardViewController") as! DashboardViewController
            DashCon.isComingFrom = "SignIn"
            self.navigationController?.pushViewController(DashCon, animated: false)
            
            return
        }
        
        let iOSDeviceScreenSize = UIScreen.main.bounds.size
        if iOSDeviceScreenSize.height == 480 {
           // constraintMobileNumberTop.constant = 44
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.addToolBar(txtMobieNumber)
        
         self.arrCountryNames = UserDefaults.standard.value(forKey: kCountryNames) as! NSArray
        self.arrCountryList = UserDefaults.standard.value(forKey: kCountryList) as! NSArray
        
        for i in 0..<self.arrCountryList.count {
            
            if ( ((self.arrCountryList.object(at: i) as! NSDictionary ).value(forKey: "country")as! String)  == "India") {
                self.appDelegate.row = i
            }
        }
        
        
        txtCountryCode.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "code")as! String)
        
        CountryNames.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "country")as! String)
        appDelegate.isAtDashBoard = false
    }
    

    // MARK: - Picker View
    func openPickerView() {
        
        customPickerView.center = CGPoint(x: 160, y: 1100)
        customPickerView.backgroundColor = UIColor.lightGray
        if customPickerView != nil {
            customPickerView.removeFromSuperview()
            customPickerView = nil
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.commitAnimations()
        if iOSDeviceScreenSize.height == 480 {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        else {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        self.view.addSubview(customPickerView)
        //Adding toolbar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: iOSDeviceScreenSize.width, height: 44))
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        customPickerView.addSubview(toolBar)
        if iOSDeviceScreenSize.height == 480 {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        else {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        pickerView.showsSelectionIndicator = true
        customPickerView.addSubview(pickerView)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.commitAnimations()
        self.view.bringSubview(toFront: customPickerView)
    }
    
    func dismissPicker() {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        customPickerView.center = CGPoint(x: 160, y: 3000)
        UIView.commitAnimations()
    }
    
    // MARK: - Picker Delegate Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountryNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCountryNames.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        appDelegate.row = row
        CountryNames.text = arrCountryNames.object(at: row) as? String
        txtCountryCode.text = ((arrCountryList.object(at: row) as! NSDictionary ).value(forKey: "code")as! String)
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let sectionWidth = 200
        return CGFloat(sectionWidth)
    }
    
    
    // MARK: - Submit Clicked Code
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        
        if((txtMobieNumber.text?.length)! < 5){
            
            self.view.makeToast("Please Check the mobile Number !", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
        if !((txtMobieNumber.text?.isEmpty)!)
        {
            if btnSelectTermsAndConditions_Outlet.isSelected
            {                
                var code = txtCountryCode.text! as String
                code = code.replacingOccurrences(of: "+", with: "")

                code = code + (txtMobieNumber.text! as String)
                let strEncryptedMobileNumber = self.encryptMobileNummber(strMobileNumber: code)
                
                UserDefaults.standard.set(txtMobieNumber.text, forKey:kMobile)
                UserDefaults.standard.set(code, forKey:kMobileNumber)
                UserDefaults.standard.set(strEncryptedMobileNumber as String, forKey: kEncryptedMobileNumber)
                UserDefaults.standard.setValue(nil, forKey: "ProfileImage")
                UserDefaults.standard.synchronize()
                
                let  strUrl = String(format : "%@%@",kServerUrl,"app_user/customerGenerateOTP") as NSString
                
                let dictParams = ["customerMobile": strEncryptedMobileNumber,kIMEI:UserDefaults.standard.value(forKey: kIMEI) as! String,"iOS":"true"] as NSDictionary
                print(dictParams)
                let dictHeaders = ["":"","":""] as NSDictionary
                
               sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                   DispatchQueue.main.async()
                    {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        
                        let otpVerificationCon  = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationViewController") as! OTPVerificationViewController
                        self.navigationController?.pushViewController(otpVerificationCon, animated: true)
                    }
                    else if(strStatusCode != "500"){
                        self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        }
                    }
                    }, failureHandler: {(error) in
                        
                })
            }
            else{
                self.view.makeToast("Please check Terms and Conditions", duration:kToastDuration, position:CSToastPositionCenter)
            }
        }
        else{
            self.view.makeToast("Mobile number should not empty!", duration:kToastDuration, position:CSToastPositionCenter)
        }
    }
    
    func encryptMobileNummber(strMobileNumber : String) -> String
    {
        let key = "@*%$!eSaHai" as String
        let plain : NSData = strMobileNumber.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) as NSData!
        let encryptedData = plain.aes256Encrypt(withKey: key).base64EncodedString(options: [])
        print(encryptedData)
        return encryptedData
    }
    
        // MARK: - Text field delegate code
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.dismissPicker()
        return textField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        
        if textField == CountryNames {
            txtMobieNumber.resignFirstResponder()
            self.openPickerView()
            pickerView.selectRow(appDelegate.row, inComponent: 0, animated: false)
            return false
        }
        else if textField == txtMobieNumber {
            self.dismissPicker()
            return true
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
            if textField == txtMobieNumber {
                let oldLength = textField.text?.characters.count
                let replacementLength = string.characters.count
                let rangeLength = range.length
                let newLength = oldLength! - rangeLength + replacementLength
              /*  if(newLength == 10){
                  //  btnSignIn.isEnabled = true
                }else{
                    //btnSignIn.isEnabled = false
                }*/
                return newLength <= 15
            }
            else {
                return true
            }
        }
    func addToolBar(_ textField: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    func doneClicked(){
        
        txtMobieNumber.resignFirstResponder()
    }
    @IBAction func btnClickTermsAndConditions(_ sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionsViewController")
         self.navigationController?.pushViewController(vc!, animated: false)

    }
    @IBAction func btnSelectTermsAndConditions(_ sender: AnyObject) {
        
        if btnSelectTermsAndConditions_Outlet.isSelected {
            let image = UIImage(named: "unselected.png")! as UIImage
            btnSelectTermsAndConditions_Outlet.setBackgroundImage(image, for: UIControlState.normal)
            btnSelectTermsAndConditions_Outlet.isSelected = false
        }else{
            let image = UIImage(named: "selected.png")! as UIImage
            btnSelectTermsAndConditions_Outlet.setBackgroundImage(image, for: UIControlState.normal)
            btnSelectTermsAndConditions_Outlet.isSelected = true
        }

    }
    
    
    @IBAction func CountryDropDown(_ sender: Any) {
        txtMobieNumber.resignFirstResponder()
        self.openPickerView()
    pickerView.selectRow(appDelegate.row, inComponent: 0, animated: false)
    }
}
