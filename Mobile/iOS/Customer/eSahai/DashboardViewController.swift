//
//  DashboardViewController.swift
//  eSahai
//
//  Created by PINKY on 14/02/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
import MessageUI


class DashboardViewController: BaseViewController,MFMailComposeViewControllerDelegate {
    
    @IBOutlet var btnNearestHospital_Outlet: UIButton!
    @IBOutlet var btnBloodBank_Outlet: UIButton!
    @IBOutlet var btnMedicalTaxi_Outlet: UIButton!
    @IBOutlet var btnAmbulance_Outlet: UIButton!
    @IBOutlet var childViewWidth: NSLayoutConstraint!
    @IBOutlet var childViewHeight: NSLayoutConstraint!
    var responseDict = NSMutableDictionary()
    var isComingFrom : String = ""
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        btnAmbulance_Outlet.layer.cornerRadius = btnAmbulance_Outlet.frame.height/2.0
        btnAmbulance_Outlet.layer.borderWidth = 2.0
        btnAmbulance_Outlet.layer.borderColor = UIColor(hex: "#EDA220").cgColor
        
        btnMedicalTaxi_Outlet.layer.cornerRadius = btnMedicalTaxi_Outlet.frame.height/2.0
        btnMedicalTaxi_Outlet.layer.borderWidth = 2.0
        btnMedicalTaxi_Outlet.layer.borderColor = UIColor(hex: "#EDA220").cgColor
        
        btnBloodBank_Outlet.layer.cornerRadius = btnBloodBank_Outlet.frame.height/2.0
        btnBloodBank_Outlet.layer.borderWidth = 2.0
        btnBloodBank_Outlet.layer.borderColor = UIColor(hex: "#EDA220").cgColor
        
        btnNearestHospital_Outlet.layer.cornerRadius = btnNearestHospital_Outlet.frame.height/2.0
        btnNearestHospital_Outlet.layer.borderWidth = 2.0
        btnNearestHospital_Outlet.layer.borderColor = UIColor(hex: "#EDA220").cgColor
        
         //self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 25)]
        //navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        let screenSize = UIScreen.main.bounds
        if screenSize.height == 480
        {
            childViewHeight.constant = 280
            childViewWidth.constant = 270
        }else{
             childViewWidth.constant = 270
        }
        
        SlideNavigationController.sharedInstance().portraitSlideOffset = self.view.frame.size.width - 220
        
        if(self.isComingFrom == "Request Call back"){
            self.requestCallBack()
        }else if(self.isComingFrom == "Call Customer Care"){
            self.callCustomerSupport()
        }else if(self.isComingFrom == "Email"){
            self.emailCustomerSupport()
        }else {//if(self.isComingFrom == "SignIn"){
            self.getActiveBooking()
        }
        getProfile()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Ambulance Booking  Screen")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        appDelegate.isAtDashBoard = true
        
        UserDefaults.standard.set(false, forKey: "isFirstTime")
        self.navigationController?.navigationBar.isHidden = false
        self.title = "eSahai"
        navigationController?.navigationBar.barTintColor = UIColor(hex: "#195F92")
        navigationController?.navigationBar.tintColor = UIColor.white
        
    }

    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }

    @IBAction func ambulanceView(_ sender: AnyObject){
        let HomeCon = self.storyboard?.instantiateViewController(withIdentifier:"HomeViewController")
                self.navigationController?.pushViewController(HomeCon!, animated: true)
        return
        
    }
    
    @IBAction func medicalTaxi(_ sender: AnyObject){
        let MediCon = self.storyboard?.instantiateViewController(withIdentifier:"MedicalTaxiViewController")
        self.navigationController?.pushViewController(MediCon!, animated: true)
        return
    }
    
    @IBAction func bloodbank(_ sender: AnyObject){
        let BloodCon = self.storyboard?.instantiateViewController(withIdentifier:"BloodRequestViewController")
        self.navigationController?.pushViewController(BloodCon!, animated: true)
        return
    }
    
    @IBAction func nearesthospital(_ sender: AnyObject){
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        guard let builder = GAIDictionaryBuilder.createEvent(withCategory: kAnalyticsEventName, action: kNearestHospitalButtonName, label:"Nearest Hospital", value: nil)else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        let HospCon = self.storyboard?.instantiateViewController(withIdentifier:"HospitalsMapRoutesViewController")
        self.navigationController?.pushViewController(HospCon!, animated: true)
        return
    }
    
    func requestCallBack(){
        
      let cusNum = UserDefaults.standard.value(forKey: kMobile) as! String
        
        var cusCareNum = UserDefaults.standard.value(forKey: "CustomerCare") as! String
        if(cusCareNum.hasPrefix("+91")){
            cusCareNum = cusCareNum.substring(5)
            print(cusCareNum)
        }
        
        let  strUrl = "http://call.esahai.in/ConVox3.0/user_conference.php?conf_no_list=\(cusNum),\(cusCareNum)"
        
        print("strurl --> \(strUrl)")
        
        let url : NSURL = NSURL(string: strUrl as String)!
        
        sharedController.requestGETURL(strURL:url, success:{(result) in
            self.view.makeToast("Request call back successful!", duration:kToastDuration, position:CSToastPositionBottom)
            DispatchQueue.main.async()
                {
                    print("result \(result)")
                       self.view.makeToast("Request call back successful!", duration:kToastDuration, position:CSToastPositionBottom)
            }
        }, failure:  {(error) in
            self.view.makeToast("Request call back successful!", duration:kToastDuration, position:CSToastPositionBottom)
        })
        
        self.view.makeToast("Request call back successful!", duration:kToastDuration, position:CSToastPositionBottom)
    }
    
    func callCustomerSupport(){
        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
            else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func emailCustomerSupport(){
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["info@esahai.in"])
        mailComposerVC.setSubject("eSahai Customer - Feedback")
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let strCurrentDate = formatter.string(from: date)
        
        let strBody : String = String(format :"\n\n\n\n\n\n\n-----------------------------------The data below will be used for complaint resolution. Please do not delete any information.\n\nMobile Number: %@.\nHandset Make: %@.\nModel: %@.\nOS Version: %@.\nDate & Time: %@.\n-----------------------------------",UserDefaults.standard.value(forKey: kMobileNumber) as! String,"Apple",appDelegate.getModel(),appDelegate.strDeviceOSVersion,strCurrentDate) as String
        
        mailComposerVC.setMessageBody(strBody, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        controller.dismiss(animated: true, completion: nil)
    }
    
    func getActiveBooking() {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getActiveBooking") as NSString
        
        // let dictParams = [:] as NSDictionary
        
        //let dictHeaders = [:] as NSDictionary
        
        
        let dictParams = ["iOS":"true",
                          "ambulanceApp": "false",
                          "customerApp": "true",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                          "customer_id":UserDefaults.standard.value(forKey: kCustomer_id)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    print("Result --? \(result)")
                    if strStatusCode == "300"
                    {
                        //  UserDefaults.standard.set( result["responseData"] as! NSDictionary , forKey: ktripDetails)
                        let trackingCon = self.storyboard?.instantiateViewController(withIdentifier:"BookingTrackingViewController") as! BookingTrackingViewController
                        
                        if !((result["responseData"] as! NSDictionary).value(forKey: "is_medical_taxi") as! Bool) {
                            trackingCon.isComingFrom = "Ambulance"
                                
                        }else {
                            trackingCon.isComingFrom = "Medical Taxi"
                        }
                        
                        UserDefaults.standard.set( (result["responseData"] as! NSDictionary).value(forKey: "booking_id") as! NSString, forKey: "bookingid")
                        
                        self.appDelegate.bookingID = ((result["responseData"] as! NSDictionary).value(forKey:"booking_id")) as! NSString
                        
                        self.navigationController?.pushViewController(trackingCon, animated: false)
                        return
                        
                    } else if strStatusCode == "204"{
                        self.logout()
                    }
                        
                    else if strStatusCode == "412"
                    {
                       // self.getCountryCodes()
                    }
                    else if strStatusCode == "413"
                    {
                        //self.getCountryCodes()
                    }else if strStatusCode == "411"
                    {
                        //self.getCountryCodes()
                    }
                    else
                    {
                       // self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
            }
        }, failureHandler: {(error) in
        })
    }
    
    func getProfile() {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getCustomerProfile") as NSString
        
        let dictParams = ["iOS":"true","customer_id": UserDefaults.standard.value(forKey: kCustomer_id)as! String,"customerApp": true,"customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String] as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async(){
                let strStatusCode = result["statusCode"] as? String
                
                if strStatusCode == nil
                {
                    return
                }
                
                if strStatusCode == "300"
                {
                    let dict = NSMutableDictionary()
                    self.responseDict = (result["responseData"] as? NSMutableDictionary)!
                    
                    for (key, value) in  self.responseDict {
                        print("value --> \(value)" )
                        if value is NSNull{
                            self.responseDict[key] = ""
                           // arrEmergencyTypeList.replaceObject(at: i, with: tempDict)
                        }
                    }
                    
                    
                    
                    if self.responseDict.value(forKey: "file_url") as! String != ""
                    {
                        dict.setValue(self.responseDict.value(forKey: "file_url"), forKey: "imageUrl")
                        UserDefaults.standard.setValue(dict, forKey: "ProfileImage")
                    }
                    else{
                        UserDefaults.standard.setValue(nil, forKey: "ProfileImage")
                    }
                    
                    if !( self.responseDict.value(forKey: "customer_name") as? String == "")
                    {
                        UserDefaults.standard.setValue(self.responseDict["customer_name"] as? String, forKey: "customer_name")
                    }
                    else{
                        UserDefaults.standard.setValue(nil, forKey: "customer_name")
                    }
                    
                }else if strStatusCode == "204"{
                   self.logout()
                }
                else
                {
                    print(result["statusCode"])
                    
                   // self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in
        })
    }
    
}
