//
//  HospitalsMapRoutesViewController.swift
//  eSahai
//
//  Created by PINKY on 08/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation
import Polyline

class HospitalsMapRoutesViewController: BaseViewController,MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    var coordinates = CLLocationCoordinate2D()
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet var mapViewObj: MKMapView!
    
    @IBOutlet weak var btnCurrentLocation: UIButton!
    @IBOutlet weak var centerObject: UIImageView!
    @IBOutlet var searchBar: UISearchBar!
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    var arrPredictions = NSMutableArray()
    var userLocationFromSearch = CLLocationCoordinate2D()
    var arrHospitals = NSMutableArray()
    var HospitalsArr = NSMutableArray()
    var isDirectionsOn = Bool()
    
    @IBOutlet var locationsTableView: UITableView!
    
    var myTimer = Timer()
    var delegate : LocationDelegate?
    
    var circle:MKCircle!
    
    @IBOutlet weak var distanceTableView: UITableView!
    var arrDistances = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Nearest Hospitals"
        self.mapViewObj.delegate = self
        searchBar.delegate = self
        distanceTableView.isHidden = true
        self.setUpMapView()
        self.addToolBar(searchBar)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.HospitalsArr.removeAllObjects()
        self.arrDistances.removeAllObjects()
    }
    func addToolBar(_ searchBar: UISearchBar) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.resignKeyboard))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        searchBar.inputAccessoryView = toolBar
    }
    func resignKeyboard() {
        searchBar.resignFirstResponder()
    }
    func setUpMapView(){
        self.isDirectionsOn = false
        appDelegate.getGPSCoordinates()
        coordinates = appDelegate.AppCurrentCoordinates
        self.centerObject.isHidden = false
        self.searchBar.isHidden = false
        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinates, 20000, 20000)
        mapViewObj.setRegion(viewRegion, animated: true)
    }

    // MARK: - Circle Overlay
    func loadOverlayForRegionWithLatitude(_ latitude: Double, andLongitude longitude: Double) {
        let Coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        circle = MKCircle(center: Coordinates, radius: 5000)
        isDirectionsOn = false
        mapViewObj.removeOverlays(mapViewObj.overlays)
        mapViewObj.add(circle)
        self.getNearbyHospitalsList()
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.red
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }else{
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.fillColor = UIColor.blue.withAlphaComponent(0.1)
            circleRenderer.strokeColor = UIColor.blue
            circleRenderer.lineWidth = 1
            return circleRenderer
        }
    }
    
    // MARK: - Annotations
    func setAnnotationMarkers() {
        DispatchQueue.main.async {
            self.mapViewObj.removeAnnotations(self.mapViewObj.annotations)
            self.mapViewObj.reloadInputViews()
        
            for i in 0...(self.HospitalsArr.count - 1)
            {
                let tempdict = self.HospitalsArr.object(at: i) as! NSMutableDictionary
                let xyz =  CLLocationCoordinate2D(latitude: (tempdict.value(forKey: "latitude") as! CLLocationDegrees ), longitude: (tempdict.value(forKey: "longitude") as! CLLocationDegrees))
                let point = NearestHospitalsAnnotation(coordinate: xyz)
              //  point.title = tempdict.value(forKey: "name") as! String!
                point.name = tempdict.value(forKey: "name") as! String!
                point.id = tempdict.value(forKey: "id") as! String!
                
                let attrArray = tempdict.value(forKey: "attributes") as? NSArray
                if((attrArray?.count)! > 0){
                    point.attributes = tempdict.value(forKey: "attributes") as! NSArray
                }else{
                    point.attributes = []
                }

                
                self.mapViewObj.addAnnotation(point)
            }
        }
        MBProgressHUD.hideAllHUDs(for: self.appDelegate.window, animated: true)
         //MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
    }
    
    // MARK: - MapView Delegate Methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        
        if annotationView == nil{
            annotationView?.canShowCallout = false
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
        }else{
            annotationView?.annotation = annotation
        }
        
        let customAnnotation = annotation as! NearestHospitalsAnnotation
        
        if ((customAnnotation.name) != "MyLocation"){
            annotationView?.canShowCallout = false
          //  let btn = UIButton(type: .detailDisclosure)
           // annotationView?.rightCalloutAccessoryView = btn
            annotationView?.image = UIImage(named: "Location Icon-01.png")
        }else{
          //  annotationView?.canShowCallout = false
            annotationView?.image = UIImage(named: "LocationCone-0.png")
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let customAnnotation = view.annotation as! NearestHospitalsAnnotation
        
        if customAnnotation.name == "MyLocation" {
            return
        }
        
        let views = Bundle.main.loadNibNamed("CustomCallOut", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCallOut
        calloutView.HospitalName.text = customAnnotation.name
        calloutView.Attr1.isHidden = false
        calloutView.Attr2.isHidden = false
        calloutView.Attr3.isHidden = false
        
        if(self.isDirectionsOn){
            calloutView.selectLbl.isHidden = true
        }else {
            calloutView.selectLbl.isHidden = false
        }
        
        switch customAnnotation.attributes.count {
        case 0:
            calloutView.Attr1.isHidden = true
            calloutView.Attr2.isHidden = true
            calloutView.Attr3.isHidden = true
        case 1:
            calloutView.Attr1.text = "\((customAnnotation.attributes.object(at: 0) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:0) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr2.isHidden = true
            calloutView.Attr3.isHidden = true
            
        case 2:
            calloutView.Attr1.text = "\((customAnnotation.attributes.object(at: 0) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:0) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr2.text = "\((customAnnotation.attributes.object(at: 1) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:1) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr3.isHidden = true
            
        case 3:
            calloutView.Attr1.text = "\((customAnnotation.attributes.object(at: 0) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:0) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr2.text = "\((customAnnotation.attributes.object(at: 1) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:1) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            calloutView.Attr3.text = "\((customAnnotation.attributes.object(at: 2) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((customAnnotation.attributes.object(at:2) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
            
        default:
            break
        }
        
        // calloutView.attributeDetails = customAnnotation.attributes
        // calloutView.attrTblView.reloadData()
        //  calloutView.attrTblView.sizeToFit()
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        
        calloutView.btnClickable.tag = Int(customAnnotation.id)!
        
        calloutView.btnClickable.addTarget(self, action: #selector(self.DrawRoute(sender:)), for: .touchUpInside)
        
        
        
        view.addSubview(calloutView)
    }
    
    func DrawRoute(sender: UIButton)
    {
     MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
        print(sender.tag)
        
        for i in 0...(self.HospitalsArr.count - 1)
        {
            let tempdict = self.HospitalsArr.object(at: i) as! NSMutableDictionary
            
            if(sender.tag == Int(tempdict.value(forKey: "id") as! String!)){
                let testDict = NSMutableDictionary()
                testDict.setValue(CLLocationCoordinate2D(latitude: tempdict.value(forKey: "latitude")! as! CLLocationDegrees  ,longitude: tempdict.value(forKey: "longitude")! as! CLLocationDegrees), forKey: "coordinates")
                testDict.setValue(tempdict.value(forKey: "name") as! String! , forKey: "name")
                testDict.setValue(tempdict.value(forKey: "id") as! String! , forKey: "id")
                testDict.setValue(tempdict.value(forKey: "attributes") as! NSArray, forKey: "attributes")
                
                self.getDirections( source: self.coordinates, destinationDict: testDict)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                
                subview.removeFromSuperview()
            }
        }
    }
    
    
   /* func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if view.annotation is MKUserLocation
        {
            return
        }
        let testAnnotation = view.annotation as! NearestHospitalsAnnotation
       
        let testDict = NSMutableDictionary()
        testDict.setValue(testAnnotation.coordinate, forKey: "coordinates")
        testDict.setValue(testAnnotation.name, forKey: "name")
        testDict.setValue(testAnnotation.id, forKey: "id")
       // testDict.setValue(testAnnotation.title, forKey: "title")
        self.getDirections( source: self.coordinates, destinationDict: testDict)
    }*/
    
    func getDirections( source : CLLocationCoordinate2D, destinationDict : NSMutableDictionary){
        let anno = NearestHospitalsAnnotation(coordinate: destinationDict.value(forKey: "coordinates") as! CLLocationCoordinate2D)
      //  anno.title = destinationDict.value(forKey: "title") as! String
        anno.id =  destinationDict.value(forKey: "id") as! String
        anno.name  = destinationDict.value(forKey: "name") as! String
        anno.attributes = destinationDict.value(forKey: "attributes") as! NSArray
        
        var directionURL = "\("https://maps.googleapis.com/maps/api/directions/json")?origin=\(source.latitude),\(source.longitude)&destination=\(anno.coordinate.latitude),\(anno.coordinate.longitude)&sensor=true&key=\(kAPI_Key)"
        
       directionURL = directionURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
        let request = URLRequest(url: URL(string:directionURL)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
            if error == nil {
                let object = try? JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                let routes = object?["routes"] as! [NSDictionary]
                for route in routes {
                    let overviewPolyline = route["overview_polyline"] as! NSDictionary
                    let points = overviewPolyline["points"] as! String
                    
                    let xyz: [CLLocationCoordinate2D]? = decodePolyline(points)
                    let mkPolyline = MKPolyline(coordinates: xyz!, count: (xyz?.count)!)
    
                    self.mapViewObj.removeAnnotations(self.mapViewObj.annotations)
                    
                    
                    self.mapViewObj.removeOverlays(self.mapViewObj.overlays)
                    self.centerObject.isHidden = true
                    self.searchBar.isHidden = true
                    self.mapViewObj.reloadInputViews()
                    self.mapViewObj.add(mkPolyline)
                    self.isDirectionsOn = true
                    self.mapViewObj.setVisibleMapRect(mkPolyline.boundingMapRect, edgePadding: UIEdgeInsetsMake(30, 30, 30, 30), animated: true)
                    self.mapViewObj.addAnnotation(anno)
                    let currentPoint = NearestHospitalsAnnotation(coordinate: source)
                  //  currentPoint.title = "MyLocation"
                    currentPoint.name = "MyLocation"
                    currentPoint.id = "abc"
                    self.mapViewObj.addAnnotation(currentPoint)
                }
                MBProgressHUD.hideAllHUDs(for: self.appDelegate.window, animated: true)
            }
            else {
                print(error!)
            }
            }
        })
        task.resume()
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool){
        
       
    }

 
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool)
    {
        MBProgressHUD.hide(for: appDelegate.window, animated: true)
        MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
        if !(self.centerObject.isHidden){
            coordinates = self.mapViewObj.centerCoordinate
            let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
            
            
            if !(appDelegate.checkInternetConnectivity()){
                MBProgressHUD.hideAllHUDs(for: appDelegate.window, animated: true)
                appDelegate.window?.makeToast(kNetworkStatusMessage, duration: kToastDuration, position: CSToastPositionBottom)
                return
            }
        
            geocoder.reverseGeocodeLocation(location) {
                (placemarks, error) -> Void in
                if error != nil {
                    print("Error getting location2 : \(error)")
                }else {
                    let placeArray = placemarks as [CLPlacemark]!
                    var placeMark: CLPlacemark!
                    placeMark = placeArray?[0]
                    
                    let strAddress = String(format: "%@,%@,%@,%@,%@,%@,%@,\n%@",(placeMark.name) != nil ? placeMark.name!: "",(placeMark.thoroughfare) != nil ? placeMark.thoroughfare!: "",(placeMark.locality) != nil ? placeMark.locality!:"",(placeMark.subLocality) != nil ? placeMark.subLocality!:"",(placeMark.subAdministrativeArea) != nil ? placeMark.subAdministrativeArea!:"",(placeMark.administrativeArea) != nil ? placeMark.administrativeArea!:"",(placeMark.country) != nil ? placeMark.country!:"",(placeMark.postalCode) != nil ? placeMark.postalCode!:"")
                    if strAddress != ",,,,,,\n" {
                        self.searchBar.text = strAddress
                        self.loadOverlayForRegionWithLatitude(self.coordinates.latitude, andLongitude:self.coordinates.longitude )
                    }
                }
            }
        }else {
            MBProgressHUD.hide(for: self.appDelegate.window, animated: true)
        }
    }
    
    
    
    // MARK: - Textfield Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if appDelegate.checkInternetConnectivity() == false && searchText == self.searchBar.text {
            
            self.view.makeToast(kNetworkStatusMessage, duration:kToastDuration, position:CSToastPositionTop)
            locationsTableView.isHidden = true
            return
        }
        self.getLocationsBasedOnAddress()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        return true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == distanceTableView {
            return self.arrDistances.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if tableView == distanceTableView {
            if let headerView = view as? UITableViewHeaderFooterView {
                let abc : Int = (((arrDistances.object(at: section) as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "multiplier") as! Int) * 5
                let aStr = String(format: "Hospitals from (%d-%d)Kms", abc-5, abc)
                
                headerView.textLabel?.text = aStr
                headerView.textLabel?.font = headerView.textLabel?.font.withSize(16)
                headerView.textLabel?.textColor = UIColor.white
                headerView.textLabel?.textAlignment = .center
                headerView.contentView.backgroundColor = UIColor(hex :"#1C73A4")
            }
        }else{
            if let headerView = view as? UITableViewHeaderFooterView {
                headerView.frame = CGRect.zero
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (tableView == distanceTableView){
            return 40.0
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (tableView == distanceTableView){
            return (self.arrDistances[section] as! NSArray).count
        }else if (tableView == locationsTableView){
            return self.arrPredictions.count
        }
        return 0
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (tableView == distanceTableView){
            let cell1 : UITableViewCell = UITableViewCell(style:.default, reuseIdentifier: "identifier")
            let tempArray = self.arrDistances.object(at: indexPath.section) as! NSArray
            cell1.backgroundColor = UIColor.clear
            cell1.textLabel?.textAlignment = .center
            cell1.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            cell1.textLabel?.textColor = UIColor.black
            cell1.textLabel?.text = (tempArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "name") as! String?
            return cell1
        } else {
            let cell : UITableViewCell = UITableViewCell(style:.default, reuseIdentifier: "identifier")
            let tempDict = self.arrPredictions.object(at: indexPath.row) as! NSDictionary
            cell.backgroundColor = UIColor.clear
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
            cell.textLabel?.numberOfLines = 2
            cell.textLabel?.textColor = UIColor.black
            cell.textLabel?.text = tempDict.value(forKey: "description") as! String?
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == distanceTableView){
            let currentCoord = coordinates
            let testDict = (arrDistances.object(at: indexPath.section) as! NSArray).object(at: indexPath.row) as! NSDictionary
            let testCood = CLLocationCoordinate2D(latitude: testDict.value(forKey: "latitude")! as! CLLocationDegrees  ,longitude: testDict.value(forKey: "longitude")! as! CLLocationDegrees)
            
            let nearestHospitalDict = ["name": testDict.value(forKey: "name")! ,"id": testDict.value(forKey: "id")!, "coordinates" :testCood , "attributes": testDict.value(forKey: "attributes") as! NSArray ] as NSMutableDictionary
            
            
            self.distanceTableView.isHidden = true
            self.getDirections(source: currentCoord , destinationDict:nearestHospitalDict )
        }else{
            locationsTableView.isHidden = true
            let tempDict = self.arrPredictions.object(at: indexPath.row) as! NSDictionary
            self.searchBar.text = tempDict.value(forKey: "description") as! String?
            self.searchBar.resignFirstResponder()
            MBProgressHUD.hide(for: appDelegate.window, animated: true)
            MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
            self.getCoordinateForAddress(address: self.searchBar.text!)
        }
    }
    
    // MARK: - Get Address Methods
    
    func getLocationsBasedOnAddress() {
        
        if ((self.searchBar.text?.length)! > 1) {
            let strAddress = self.searchBar.text?.replacingOccurrences(of:" ", with: "%22", options: .literal, range: nil)
            
           // let  strUrl = String(format :"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=%@",strAddress!,kAPI_Key) as NSString
            
            let strUrl =  "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(kAPI_Key)&input=\(strAddress!)"
            
            let url : NSURL = NSURL(string: strUrl as String)!
            
            sharedController.requestGETURL(strURL:url, success:{(result) in
                DispatchQueue.main.async()
                    {
                        self.arrPredictions.removeAllObjects()
                        self.locationsTableView.isHidden = false
                        self.arrPredictions = result.value(forKey: "predictions") as! NSMutableArray
                        self.locationsTableView.reloadData()
                }
            }, failure:  {(error) in
            })
        }
        else
        {
            locationsTableView.isHidden = true
        }
    }
    
    
    func getCoordinateForAddress(address : String) {
        
        MBProgressHUD.hide(for: appDelegate.window, animated: true)
        MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
        if ((address.length) > 1) {
            
            let strAddress = self.searchBar.text?.replacingOccurrences(of:" ", with: "%22", options: .literal, range: nil)
            let strUrl = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=\(strAddress!)&key=\(kAPI_Key)"
            
            let url : NSURL = NSURL(string: strUrl)!
            
            sharedController.requestGETURL(strURL:url, success:{(result) in
                DispatchQueue.main.async()
                    {
                        let status = result["status"] as! String
                        
                        if( status == "ZERO_RESULTS" ||
                            status == "REQUEST_DENIED" ||
                            status == "INVALID_REQUEST" ||
                            status == "INVALID_REQUEST" ){
                            
                            //self.Test(address: self.searchBar.text! )
                            self.searchBar.text = ""
                            self.view.makeToast("Geo Location not found", duration:kToastDuration, position:CSToastPositionTop)
                        }else if(status == "OK") {
                        print(result["results"] ?? "  No data")
                        if let result = result["results"] as? NSArray {
    
                            let geometry = (result[0] as! NSDictionary).value(forKey:"geometry") as! NSDictionary
                            let loco = geometry.value(forKey: "location") as! NSDictionary
                            let adddressLatitude = loco.value(forKey: "lat")
                            let addressLongitude = loco.value(forKey: "lng")
                            
                            let addressCoordinates = CLLocationCoordinate2DMake(adddressLatitude as! CLLocationDegrees, addressLongitude as! CLLocationDegrees)
                            
                           // let annotationPoint = MKPointAnnotation()
                           // annotationPoint.coordinate = addressCoordinates
                            
                            self.coordinates = addressCoordinates
                           
                            let annotationPoint = NearestHospitalsAnnotation(coordinate: addressCoordinates)
                            annotationPoint.name = ""
                            annotationPoint.id = ""
                            
                         
                            self.mapViewObj.addAnnotation(annotationPoint)
                            
                            let viewRegion = MKCoordinateRegionMakeWithDistance(annotationPoint.coordinate, 20000, 20000)
                            
                            self.mapViewObj.setRegion(viewRegion, animated: true)
                        }
                        }
                        MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
                }
            }, failure:  {(error) in
            })
        }
    }
    
    // MARK: - Get Nearest Hospital
    
    func getNearbyHospitalsList() {
        let  strUrl = String(format : "%@%@",kServerUrl,"getNearByHospitalList") as NSString
        
        let dictParams = ["iOS":"true",
                          "emergencyGeoHash": Geohash.encode(latitude:coordinates.latitude , longitude: coordinates.longitude) ,
                          kLatitude: coordinates.latitude,
                          "longitude": coordinates.longitude, "customerApp": "true",
                          "ambulanceApp": "false",
                          "groupType":"2",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        self.HospitalsArr.removeAllObjects()
        self.arrDistances.removeAllObjects()
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300" {
                    self.arrHospitals =  result["responseData"] as! NSMutableArray
                    print("hospitals Arr -- > \(self.arrHospitals)")
                    
                    let tempArray  = NSMutableArray()
                    var tempxyz = Int()
                    
                    
                    for i in 0..<self.arrHospitals.count{
                        let tempDict = self.arrHospitals.object(at: i) as! NSDictionary
                        let tempdict1 = NSMutableDictionary()
                        
                        tempdict1["name"] = tempDict["group_name"]!
                        tempdict1["latitude"] = Double(tempDict.value(forKey:"group_latitude") as! String)
                        tempdict1["longitude"] = Double(tempDict.value(forKey:"group_longitude") as! String)
                        tempdict1["id"] = tempDict["group_id"]!
                        tempdict1["attributes"] = tempDict["attributes"]
                
                        self.HospitalsArr.add(tempdict1)
                        let distance = Double(tempDict["distance"] as! String)
                        let dividet : Double = (distance!/Double(5))
                        let xyz : Int = Int(dividet)
                        
                        tempdict1["distance"] = tempDict["distance"]!
                        tempdict1["multiplier"] = xyz + 1
                        
                        if(xyz != tempxyz){
                            let abc = NSMutableArray()
                            if(tempArray.count > 0){
                                for i in 0..<tempArray.count {
                                    abc.add(tempArray.object(at: i))
                                }
                                self.arrDistances.add(abc)
                                tempArray.removeAllObjects()
                            }
                        }
                        tempArray.add(tempdict1)
                        tempxyz = xyz
                        if( i == (self.arrHospitals.count - 1) ){
                            if(tempArray.count > 0){
                                let abc = NSMutableArray()
                                for i in 0..<tempArray.count {
                                    abc.add(tempArray.object(at: i))
                                }
                                self.arrDistances.add(abc)
                                tempArray.removeAllObjects()
                            }
                        }
                    }
                    self.setAnnotationMarkers()
                }
                else if strStatusCode == "204"{
                    MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
                    self.arrHospitals.removeAllObjects()
                    self.arrDistances.removeAllObjects()
                    self.logout()
                }
                else
                {
                    MBProgressHUD.hideAllHUDs(for: self.appDelegate.window, animated: true)
                   // MBProgressHUD.hide(for:self.appDelegate.window, animated: true)
                    DispatchQueue.main.async {
                        self.mapViewObj.removeAnnotations(self.mapViewObj.annotations)
                        self.mapViewObj.reloadInputViews()
                    }
                    self.arrHospitals.removeAllObjects()
                    self.arrDistances.removeAllObjects()
                    self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
    
    // MARK: - Button Actions
    
    @IBAction func xyz(_ sender: Any) {
        distanceTableView.isHidden = true
        self.setUpMapView()
    }
    
    @IBAction func abc(_ sender: Any) {        
        distanceTableView.isHidden = false
        distanceTableView.reloadData()
    }
    
    @IBAction func btnCurrentLocationAction(_ sender: Any) {
        appDelegate.getGPSCoordinates()
        self.coordinates = appDelegate.AppCurrentCoordinates
        self.setUpMapView()
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
}
