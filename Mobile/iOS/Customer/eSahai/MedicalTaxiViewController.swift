//
//  MedicalTaxiViewController.swift
//  eSahai
//
//  Created by PINKY on 16/02/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
//import DLRadioButton
import FirebaseInstanceID
import CoreLocation

class MedicalTaxiViewController: BaseViewController,LocationDelegate,UITextFieldDelegate {

    var alertController = UIAlertController()
    
    
    //1st set Radio
    @IBOutlet weak var assitanceNoRadio: UIButton!
    @IBOutlet weak var assitanceNoRadio2: UIButton!
    
    //2nd set Radio
    @IBOutlet weak var myselfRadio: UIButton!
    @IBOutlet weak var myselfRadio2: UIButton!
    
    //3rd set Radio
    @IBOutlet weak var locationradio: UIButton!
    @IBOutlet weak var locationradio2: UIButton!
    
    @IBOutlet weak var btnHospitalLocation: UIButton!
    @IBOutlet weak var btnCurrentLocation: UIButton!
    @IBOutlet weak var txtContact: UITextField!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var dictCurrentLoc = NSDictionary()
    var dictHospitalLoc = NSDictionary()
    var hospitalLoc = NSDictionary()
    var emergencyID = String()
    var arrayHospitals = NSMutableArray()
    
    //var IDbooking =  NSString()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTaxiView()
        
        let notificationName = Notification.Name("getActiveBooking")
        NotificationCenter.default.addObserver(self, selector: #selector(MedicalTaxiViewController.getBooking), name: notificationName, object: nil)
        
        self.addToolBar(txtContact)
        self.getNearbyHospitalsList()
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        // Do any additional setup after loading the view.
    self.navigationController?.navigationBar.isHidden = false
    self.title = "MEDICAL TAXI"
    navigationController?.navigationBar.barTintColor = UIColor(hex: "#195F92")
    navigationController!.navigationBar.tintColor = UIColor.white
    //navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    
    
    SlideNavigationController.sharedInstance().portraitSlideOffset = self.view.frame.size.width - 220
    
}

    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    func addToolBar(_ textField: UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    func doneClicked(){
        
        txtContact.resignFirstResponder()
    }
    
    func setUpTaxiView() {
        
        txtContact.isHidden = true
        topConstraint.constant = 30
        
        appDelegate.getGPSCoordinates()
        btnCurrentLocation.setTitle(UserDefaults.standard.value(forKey:kAddress) as? String , for: .normal)
        
    
        btnHospitalLocation.titleLabel?.text = "Select drop location"
        btnCurrentLocation.titleLabel?.text = UserDefaults.standard.value(forKey: kAddress) as! String?
        
        myselfRadio.isSelected = true
        assitanceNoRadio.isSelected = true
        locationradio.isSelected = true
    }

    
    @IBAction func btnCurrentLocationAction(_ sender: Any) {
        let selectLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectLocationCon.delegate = self
        selectLocationCon.isComingFrom = "Current Location"
        self.navigationController?.pushViewController(selectLocationCon, animated: true)
    }

    @IBAction func btnanylocationAction(_ sender: Any) {
       
        if(!locationradio.isSelected){
            if(btnHospitalLocation.titleLabel?.text == "No Nearby Hospitals found"){
                self.view.makeToast("Please select Valid Inputs", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }
        
        
        if(locationradio.isSelected){
            let selectLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
            selectLocationCon.delegate = self
            selectLocationCon.isComingFrom = "Hospital Location"
            self.navigationController?.pushViewController(selectLocationCon, animated: true)
        }else{
         
            let emergencyTypes  = UserDefaults.standard.value(forKey: "arrEmergencyTypeList") as! NSArray
            for emergency in emergencyTypes {
                let emer = emergency as! NSDictionary
                if(emer["type_name"] as? String == "Medical Taxi"){
                    emergencyID = emer["id"] as! String
                }
            }
            
            
            
            let nearestLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "NearestListMapViewController") as! NearestListMapViewController
            
            
            if(dictCurrentLoc.count>0)
            {
                nearestLocationCon.coordinates = CLLocationCoordinate2D(latitude: (dictCurrentLoc.value(forKey: "lat") as! Double), longitude: (dictCurrentLoc.value(forKey: "long") as! Double))
            }
            else
            {
                nearestLocationCon.coordinates = appDelegate.AppCurrentCoordinates
            }
            nearestLocationCon.delegate = self
            nearestLocationCon.IDemergency = emergencyID
            self.navigationController?.pushViewController(nearestLocationCon, animated: true)
        }
    }
    
    @IBAction func scheduleBtnAction(_ sender: Any) {
         self.alertController = UIAlertController(title: "", message: "Please contact our Customer Service to schedule the service", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            
            guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
                else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        self.alertController.addAction(cancelAction)
        self.alertController.addAction(okAction)
        self.present(self.alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnTaxiBookingAction(_ sender: Any) {
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        guard let builder = GAIDictionaryBuilder.createEvent(withCategory: kAnalyticsEventName, action: kCallAmbulanceButtonName, label:"CallMedicalTaxi", value: nil)else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        
        if !(appDelegate.checkInternetConnectivity()){
            
            self.alertController = UIAlertController(title: "", message: "No internet connection! Do you want to connect to Customer Service for booking?", preferredStyle: UIAlertControllerStyle.alert)
            
            let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                return
            }
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
                    else { return }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(number, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                        UIApplication.shared.openURL(url)
                    }
                }
                
            }
            self.alertController.addAction(DestructiveAction)
            self.alertController.addAction(okAction)
            self.present(self.alertController, animated: true, completion: nil)
            return
        }
        
        
        if(!locationradio.isSelected){
            if(btnHospitalLocation.titleLabel?.text == "No Nearby Hospitals found"){
                self.view.makeToast("Please select Valid Inputs", duration:kToastDuration, position:CSToastPositionCenter)
                return

            }
        }
        
        if(!myselfRadio.isSelected){
            if(txtContact.text?.length == 0){
                self.view.makeToast("Please enter Contact Number", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
            
            if((txtContact.text?.length)! < 10){
                self.view.makeToast("Please enter valid Contact Number", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }
        
        if(locationradio.isSelected){
            if(dictHospitalLoc.count == 0){
                self.view.makeToast("Please set the Destination Location", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }
        
        var assistance = NSString()
        
        if(assitanceNoRadio.isSelected){
            assistance = "No"
        }else{
            assistance = "Yes"
        }
        //Comparing Current location and Hospital location to show alert
        if btnCurrentLocation.titleLabel?.text == btnHospitalLocation.titleLabel?.text
        {
            self.view.makeToast("Both locations are same", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
        if (self.locationradio.isSelected){
            var strCurLat = String()
            var strCurLon = String()
            
            if(dictCurrentLoc.count>0)
            {
                strCurLat = String(format: "%.3f", (dictCurrentLoc.value(forKey: "lat") as! Double))
                strCurLon = String(format: "%.3f", (dictCurrentLoc.value(forKey: "long") as! Double))
            }
            else
            {
                strCurLat = String(format: "%.3f", (appDelegate.AppCurrentCoordinates.latitude as? Double)!)
                strCurLon = String(format: "%.3f", (appDelegate.AppCurrentCoordinates.longitude as? Double)!)
            }
            let strHosLat = String(format: "%.3f", (dictHospitalLoc.value(forKey: "lat") as? Double)!)
            let strHosLon = String(format: "%.3f", (dictHospitalLoc.value(forKey: "long") as? Double)!)
            
            if strCurLat == strHosLat && strCurLon == strHosLon
            {
                self.view.makeToast("Both locations are same", duration:kToastDuration, position:CSToastPositionCenter)
                return
            }
        }

        
        let  strUrl = String(format : "%@%@",kServerUrl,"createBooking") as NSString
        var groupID = NSString()
        
        let emergencyTypes  = UserDefaults.standard.value(forKey: "arrEmergencyTypeList") as! NSArray
        for emergency in emergencyTypes {
            let emer = emergency as! NSDictionary
            if(emer["type_name"] as? String == "Medical Taxi"){
                emergencyID = emer["id"] as! String
            }
        }
        var hospitalLatitude = Double()
        var hospitalLongitude = Double()
        var hospitalName = String()
        
        if(!locationradio.isSelected){
            if(hospitalLoc.count == 0){
                hospitalLatitude = 0.00
                hospitalLongitude = 0.00
                hospitalName = "Any nearby Hospital"
                groupID = "Any"
            }else{
            hospitalLatitude = hospitalLoc.value(forKey: "latitude") as! Double
            hospitalLongitude = hospitalLoc.value(forKey: "longitude") as! Double
            hospitalName = hospitalLoc.value(forKey: "Hospital Name") as! String
            groupID = hospitalLoc.value(forKey:"Group_id") as! NSString
            }
        }else{
            hospitalLatitude = dictHospitalLoc.value(forKey: "lat") as! Double
                //Double(dictHospitalLoc.value(forKey: "lat") as! String)!
            hospitalLongitude = dictHospitalLoc.value(forKey: "long") as! Double
                //Double(dictHospitalLoc.value(forKey: "long") as! String)!
            hospitalName = dictHospitalLoc.value(forKey: "address") as! String
            groupID = "Any"
        }
        
        
        var forSelf = NSString()
        if(myselfRadio.isSelected){
            forSelf = "1"
        }else{
            forSelf = "0"
        }
        
        var patientContact = String()
        if(forSelf).boolValue{
            patientContact = ""
        }else{
            patientContact = txtContact.text!
        }
        
        let hospitalGeohash = Geohash.encode(latitude:hospitalLatitude , longitude:hospitalLongitude) as String
        
        var name = NSString()
        if((UserDefaults.standard.value(forKey: "customer_name")) != nil){
            name = UserDefaults.standard.value(forKey: "customer_name")as! NSString
        }else{
            name = "No Name"
        }
        
        var MediCoordinates = CLLocationCoordinate2D()
        
        if(dictCurrentLoc.count>0){
            MediCoordinates.latitude = dictCurrentLoc.value(forKey: "lat") as! CLLocationDegrees
            MediCoordinates.longitude = dictCurrentLoc.value(forKey: "long") as! CLLocationDegrees
        }else{
            MediCoordinates = appDelegate.AppCurrentCoordinates
        }
        
        let personalDetails = [
            "customer_name": name ,
            "customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
            "patient_number" : patientContact,
            "booked_by": "Customer",
            "emergency_type": "Medical Taxi",
            "emergency_longitude": MediCoordinates.longitude  ,
            "emergency_latitude": MediCoordinates.latitude ,
            "emergency_geoHashCode": Geohash.encode(latitude: MediCoordinates.latitude, longitude: MediCoordinates.longitude ),
            "hospital": hospitalName,
            "customer_id": UserDefaults.standard.value(forKey: kCustomer_id)as! String ,
            "is_self": forSelf,
            "assistance": assistance] as NSMutableDictionary
        
        if(locationradio.isSelected){
            personalDetails.setValue(hospitalLatitude, forKey: "drop_latitude")
            personalDetails.setValue(hospitalLongitude, forKey: "drop_longitude")
            personalDetails.setValue(hospitalGeohash, forKey: "drop_geoHashCode")
        }
        
        
        let token = FIRInstanceID.instanceID().token()
        
        let dictParams = [
            "params":personalDetails,
            "customerApp": "true",
            "customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
            "group_type_id": "8" ,
            "group_id":groupID,
            "emergency_type": emergencyID,
            "gcmToken": token ?? "",
            "hospital_latitude": hospitalLatitude,
            "hospital_longitude": hospitalLongitude,
            "hospital_geoHashCode":hospitalGeohash,
            "address" : btnCurrentLocation.titleLabel?.text
                //UserDefaults.standard.value(forKey:kAddress)
            ] as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            
            let strStatusCode = result["statusCode"] as? String
            if strStatusCode == nil
            {
                
            }
            else
            {
                if strStatusCode == "300"
                {
                    let bookingLocationCon = self.storyboard?.instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
                    
                    self.appDelegate.bookingID = (result["responseData"] as! NSDictionary).value(forKey: "booking_id") as! NSString
                    UserDefaults.standard.set( (result["responseData"] as! NSDictionary).value(forKey: "booking_id") as! NSString, forKey: "bookingid")
                    bookingLocationCon.iscoming = "Medical Taxi"
                    //UserDefaults.standard.set( (result["responseData"] as! NSDictionary).value(forKey: kDriverNum) as! NSString, forKey: kDriverNum)
                    
                    // bookingLocationCon.delegate = self
                    // selectLocationCon.isComingFrom = "Current Location"
                    self.navigationController?.pushViewController(bookingLocationCon, animated: true)
                }else if(strStatusCode == "411"){
                    self.alertController = UIAlertController(title: "", message: result["statusMessage"] as! String, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                        
                    }
                    let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        
                        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
                            else { return }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(number, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                                UIApplication.shared.openURL(url)
                            }
                        }
                        
                    }
                    self.alertController.addAction(DestructiveAction)
                    self.alertController.addAction(okAction)
                    self.present(self.alertController, animated: true, completion: nil)
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
                    
                else if(strStatusCode != "500"){
                
                    DispatchQueue.main.async(){
                        self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
                }
            }
        }, failureHandler: {(error) in
        })
    }
    
    // MARK: - Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtContact {
            let oldLength = textField.text?.characters.count
            let replacementLength = string.characters.count
            let rangeLength = range.length
            let newLength = oldLength! - rangeLength + replacementLength
            // var returnKey = (string as NSString).rangeOf("\n").location != NSNotFound
            return newLength <= 10
        }
        else {
            return true
        }
    }
    
    // MARK: - Radio Button code
    
    @IBAction fileprivate func Taxifor( _sender : UIButton) {
        
        if (_sender.tag == 101) {
            txtContact.isHidden = true
            topConstraint.constant = 30
            self.myselfRadio.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.myselfRadio2.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.myselfRadio.isSelected = true
        }else if(_sender.tag == 102){
            txtContact.isHidden = false
            topConstraint.constant = 82
            self.myselfRadio.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.myselfRadio2.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.myselfRadio.isSelected = false
        }
    }
    
    @IBAction fileprivate func needAssistance( _sender : UIButton){
        
        if (_sender.tag == 201) {
            self.assitanceNoRadio.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.assitanceNoRadio2.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.assitanceNoRadio.isSelected = true
            
        }else if(_sender.tag == 202){
            self.assitanceNoRadio.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.assitanceNoRadio2.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.assitanceNoRadio.isSelected = false
        }
    }
    
    @IBAction fileprivate func locationBtn( _sender : UIButton){
        
        if (_sender.tag == 301) {
            self.locationradio.isSelected = true
            self.locationradio.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            self.locationradio2.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            
            if (dictHospitalLoc.count > 0) {
                btnHospitalLocation.setTitle(dictHospitalLoc.value(forKey:"address") as! String?, for: .normal)
            }else {
                btnHospitalLocation.setTitle("Select drop location", for: .normal)
            }
        }else if(_sender.tag == 302){
            self.locationradio.isSelected = false
            self.locationradio.setImage(UIImage(named: "unselectedRadio.png"), for: UIControlState.normal)
            self.locationradio2.setImage(UIImage(named: "selectedRadio.png"), for: UIControlState.normal)
            
            if !(self.arrayHospitals.count > 0 ){
            btnHospitalLocation.setTitle("No Nearby Hospitals found", for: .normal)
                return
            }
            
            
            if (hospitalLoc.count > 0) {
                btnHospitalLocation.setTitle(hospitalLoc.value(forKey:"Hospital Name") as! String?, for: .normal)
            }else {
                btnHospitalLocation.setTitle(" Any Near Hospital or Select a Hospital", for: .normal)
            }
        }
    }
    
    // MARK: - Delegate Call backs
    
    func getUserLocation(dict : NSDictionary){
        
        dictCurrentLoc = dict
        btnCurrentLocation.setTitle(dict.value(forKey:"address") as! String?, for: .normal)
        self.getNearbyHospitalsList()
    }
    
    func getHospitalLocation(dict : NSDictionary){
        
        dictHospitalLoc = dict
        btnHospitalLocation.setTitle(dict.value(forKey:"address") as! String?, for: .normal)
    }
    func getHospitalLocationDict(dict : NSDictionary){
        
        hospitalLoc = dict
        btnHospitalLocation.setTitle(dict.value(forKey:"Hospital Name") as! String?, for: .normal)
    }
    
    /*func getBooking(){
        
        print("Booking-- >")
        let  strUrl = String(format : "%@%@",kServerUrl,"getActiveBooking") as NSString
        
        let dictParams = ["iOS":"true",
                          "ambulanceApp": "false",
                          "customerApp": "true",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                          "customer_id":UserDefaults.standard.value(forKey: kCustomer_id)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        let trackingCon = self.storyboard?.instantiateViewController(withIdentifier:"BookingTrackingViewController") as! BookingTrackingViewController
                        
                        UserDefaults.standard.set( (result["responseData"] as! NSDictionary).value(forKey: "booking_id") as! NSString, forKey: "bookingid")
                        
                        trackingCon.booking_id = (result["responseData"] as! NSDictionary).value(forKey: "booking_id") as! NSString
                        
                        self.navigationController?.pushViewController(trackingCon, animated: false)
                        return
                        
                    }else if strStatusCode == "204"{
                        self.logout()
                    }else if strStatusCode == "413"{
                        print("No Ambulances Found")
                        
                        let alertController = UIAlertController(title: "", message: "No Ambulance found for the given criteria.. Connect to customer Service?" , preferredStyle: UIAlertControllerStyle.alert)
                        
                        let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                            
                        }
                        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                            
                            
                            guard let number = URL(string: "telprompt://04047911911")
                                else { return }
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(number, options: [:], completionHandler: nil)
                            } else {
                                // Fallback on earlier versions
                                if let url = URL(string: "tel://04047911911") {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                            
                        }
                        
                        alertController.addAction(DestructiveAction)
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    }else
                    {
                        // self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
            }
        }, failureHandler: {(error) in
        })
    }*/
    
    
    func getBooking(){
        
        
        let  strUrl = String(format : "%@%@",kServerUrl,"checkMyBookingStatus") as NSString
        
        let dictParams = ["iOS":"true",
                          "customerApp": "true",
                          "ambulanceApp": "false",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                          "booking_id" :UserDefaults.standard.value(forKey: "bookingid") as! String]
            as NSDictionary
        
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        print(dictParams)
        print(dictHeaders)
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                
                if strStatusCode == "413"{
                    print("No Ambulances Found")
                    
                     self.alertController = UIAlertController(title: "", message: "No Ambulance found for the given criteria.. Connect to customer Service?" , preferredStyle: UIAlertControllerStyle.alert)
                    
                    let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                        
                    }
                    let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        
                        
                        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
                            else { return }
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(number, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                                UIApplication.shared.openURL(url)
                            }
                        }
                        
                    }
                    
                    self.alertController.addAction(DestructiveAction)
                    self.alertController.addAction(okAction)
                    self.present(self.alertController, animated: true, completion: nil)
                    
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
                else
                {
                    //self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
    
    // MARK: - get Nearest Hospital
    
    func getNearbyHospitalsList() {
        
        let emergencyTypes  = UserDefaults.standard.value(forKey: "arrEmergencyTypeList") as! NSArray
        for emergency in emergencyTypes {
            let emer = emergency as! NSDictionary
            if(emer["type_name"] as? String == "Medical Taxi"){
                emergencyID = emer["id"] as! String
            }
        }
        
        var MediCoordinates = CLLocationCoordinate2D()
        
        if(dictCurrentLoc.count>0){
            MediCoordinates.latitude = dictCurrentLoc.value(forKey: "lat") as! CLLocationDegrees
            MediCoordinates.longitude = dictCurrentLoc.value(forKey: "long") as! CLLocationDegrees
        }else{
            MediCoordinates = appDelegate.AppCurrentCoordinates
        }
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getNearByHospitalList") as NSString
        let dictParams = ["iOS":"true",
                          "emergencyGeoHash": Geohash.encode(latitude: MediCoordinates.latitude, longitude: MediCoordinates.longitude ) ,
                          kLatitude:MediCoordinates.latitude ,
                          "longitude": MediCoordinates.longitude,
                          "customerApp": "true",
                          "ambulanceApp": "false",
                          "emergency_type" : emergencyID,
                          "groupType":"2",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300"
                {
                    self.arrayHospitals = result["responseData"] as! NSMutableArray
                if !(self.arrayHospitals.count>0){
                    if !(self.locationradio.isSelected){
                        self.btnHospitalLocation.setTitle("No Nearby Hospitals found", for: .normal)
                    }
                }else {
                    
                    if !(self.locationradio.isSelected){
                        self.btnHospitalLocation.setTitle("Any Near Hospital or  Select a Hospital", for: .normal)
                    }
                    
                    }
                    
                }else if strStatusCode == "303"
                {
                    if !(self.locationradio.isSelected){
                        self.btnHospitalLocation.setTitle("No Nearby Hospitals found", for: .normal)
                    }
                }
            }
        }, failureHandler: {(error) in})
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.alertController.dismiss(animated: true, completion: nil)
    }

}
