 //
//  AppDelegate.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import SystemConfiguration
import CoreData
import CoreLocation
import Firebase
//import GoogleMaps
//import GooglePlaces
import CoreTelephony
import UserNotifications
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import FirebaseInstanceID
import FirebaseMessaging

 //import IQKeyboardManagerSwift



//import DeviceGuru


#if (arch(i386) || arch(x86_64)) && os(iOS)
let DEVICE_IS_SIMULATOR = true
#else
let DEVICE_IS_SIMULATOR = false
#endif

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager()
    var AppCurrentCoordinates = CLLocationCoordinate2D()
    let geocoder = CLGeocoder()
    var deviceTokenString = String()
  //  var isFromAddContact = Bool()
    let gcmMessageIDKey = "gcm.message_id"
    var ambulanceTypesArray = NSArray()
    var emergencyTypesArray = NSArray()
    var nearesthospital = NSDictionary()
    var strAppVersion = String()
    var strDeviceOSVersion = String()
    var row = Int()
    var isAtDashBoard = Bool()
    
    var bookingString : String = ""
    
    var topics = ["FAQ","TNC","PRIVACY-POLICY","EMERGENCY-TYPES","HOW-DO-YOU-KNOW","COUNTRY_LIST"]

    let sharedCon = SharedController()
    
    var bookingUpdateTimer = Timer()
    var bookingID = NSString()
    

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        FIRApp.configure()
    //    GMSServices.provideAPIKey(kAPI_Key)
      //  GMSPlacesClient.provideAPIKey(kAPI_Key)
      
        Crittercism.enable(withAppID: kcrittercismAppID)
       // IQKeyboardManager.sharedManager().enable = true
        if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
             print("didReceiveRemoteNotification0")
            print(launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as! NSDictionary)
        }

        
        
        // Google Analytics Code
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
      //  guard let gai = GAI.sharedInstance() else {
        //    assert(false, "Google Analytics not configured correctly")
        
        //}
       // gai.trackUncaughtExceptions = true  // report uncaught exceptions
       // gai.logger.logLevel = GAILogLevel.verbose  // remove before app release
        
        self.addLeftMenu()
        self.getGPSCoordinates()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
           strAppVersion = version
        }
        strDeviceOSVersion = String(format:"%@",UIDevice.current.systemVersion)
        print("AppVersion : %@",strAppVersion)
        print("DeviceOSVersion : %@",strDeviceOSVersion)
        
        UserDefaults.standard.set(UIDevice.current.identifierForVendor!.uuidString as String, forKey: kIMEI)
        UserDefaults.standard.synchronize()
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        application.registerForRemoteNotifications()
        return true
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        print("didReceiveRemoteNotification2")
        print(userInfo as NSDictionary)
        self.handlePushNotifications(dataDictionary: userInfo)
       
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(UIApplication.shared, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        // Add any custom logic here.
        return handled
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("didReceiveRemoteNotification1")
        print(userInfo as NSDictionary)
        self.handlePushNotifications(dataDictionary: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]

    // [START refresh_token]
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    
    // [START connect_to_fcm]
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
            
                for topic in self.topics{
                    FIRMessaging.messaging().subscribe(toTopic: "/topics/\(topic)")
                }

                print("Connected to FCM.")
            }
        }
    }
    // [END connect_to_fcm]


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        FIRMessaging.messaging().disconnect()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0

        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        
        if(isAtDashBoard == true){
            self.getLocationStatus()

            
        }else{
            //UserDefaults.standard.set(true, forKey: "isFirtTime")
        }
        connectToFcm()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "eSahai")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }

        } else
        {
            // Fallback on earlier versions
        }
    }
    // MARK: - For Device Token

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("APNs token retrieved: \(deviceToken)")

        for i in 0..<deviceToken.count {
            self.deviceTokenString = self.deviceTokenString + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("DeviceToken : \(self.deviceTokenString)")
        UserDefaults.standard.set(self.deviceTokenString, forKey: kDeviceToken)
        
        //Development Push Notifications
       // FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        
        //Prod Push Notifications
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)

        UserDefaults.standard.synchronize()
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")

        if (DEVICE_IS_SIMULATOR) {
            self.deviceTokenString = ""
        }
        UserDefaults.standard.set("", forKey:kDeviceToken)
        UserDefaults.standard.synchronize()
        
        // With swizzling disabled you must set the APNs token here.
      //   FIRInstanceID.instanceID().setAPNSToken(self.deviceTokenString, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    // MARK: - Add Left menu
    func addLeftMenu() {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let leftMenu = storyBoard.instantiateViewController(withIdentifier: "MenuViewController")
        SlideNavigationController.sharedInstance().leftMenu = leftMenu
        SlideNavigationController.sharedInstance().enableShadow = false
        SlideNavigationController.sharedInstance().menuRevealAnimationDuration = 0.18
    }
    // MARK: - Check Internet Connectivity
    
    func checkInternetConnectivity() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    // MARK: - Current Location
    
    func getGPSCoordinates() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
     //   locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    
    
    func getLocationStatus(){
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch(CLLocationManager.authorizationStatus()) {
            
            case .notDetermined, .restricted, .denied:
                
                
                print("No access")
                let alert = UIAlertController(title: "", message: "GPS not enabled", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Turn ON GPS", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                    print("")
                    //UIApplication.shared.openURL(NSURL(string:"prefs:root=Location")! as URL)
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(NSURL(string:"prefs:root=Location")! as URL)
                    }

                }))
                
                
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("Access")
            }
            
        }else{
            
            let alert = UIAlertController(title: "", message: "GPS not enabled", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Turn ON GPS", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                print("")
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(NSURL(string:"prefs:root=Location")! as URL)
                }
            }))
            
           
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            print("No access")

        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        
        locationManager.stopUpdatingLocation()
        
        let alert = UIAlertController(title: "", message: "GPS not enabled", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Turn ON GPS", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(NSURL(string:"prefs:root=Location")! as URL)
            }
        }))
        
        
        //  self.window?.present(alert, animated: false, completion: nil)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
 
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        self.AppCurrentCoordinates = locationObj.coordinate
        self.getCurrentLocationAddress(coordinate: coord)
        
        locationManager.stopUpdatingLocation()
        print("didUpdateLocations")
        print("latitude: \(coord.latitude)")
        print("longitude: \(coord.longitude)")
    }
    
    func getCurrentLocationAddress(coordinate : CLLocationCoordinate2D) {
        
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        geocoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in
            if error != nil {
                print("Error getting location2 : \(error)")
            }else {
                let placeArray = placemarks as [CLPlacemark]!
                var placeMark: CLPlacemark!
                placeMark = placeArray?[0]

                  let strAddress = String(format: "%@,%@,%@,%@,%@,%@,\n%@",(placeMark.thoroughfare) != nil ? placeMark.thoroughfare!: "",(placeMark.locality) != nil ? placeMark.locality!:"",(placeMark.subLocality) != nil ? placeMark.subLocality!:"",(placeMark.subAdministrativeArea) != nil ? placeMark.subAdministrativeArea!:"",(placeMark.administrativeArea) != nil ? placeMark.administrativeArea!:"",(placeMark.country) != nil ? placeMark.country!:"",(placeMark.postalCode) != nil ? placeMark.postalCode!:"")
               
                UserDefaults.standard.set(strAddress, forKey: kAddress)
               
                UserDefaults.standard.synchronize()

            }
        }
    }
    
    func logOut(){
     
        let countryLists = UserDefaults.standard.value(forKey: kCountryList) as! NSArray
        
        for i in 0..<countryLists.count {
            
            if ( ((countryLists.object(at: i) as! NSDictionary ).value(forKey: "country")as! String)  == "India") {
                self.row = i
            }
        }
       
       SlideNavigationController.sharedInstance().popToRootViewController(animated: true)

        let alert = UIAlertController(title: "", message: "Someone else has logged in with same number entered in another device", preferredStyle: UIAlertControllerStyle.alert)
         alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive, handler: { (alert: UIAlertAction!) in
         
         }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        //completionHandler([])
        completionHandler(UNNotificationPresentationOptions.alert)

    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
        
    }
    
}
// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        
        self.handlePushNotifications(dataDictionary: remoteMessage.appData)
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func cancelFromBookingTracking(){
        if let wd = self.window {
            var vc = wd.rootViewController
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).visibleViewController
            }
            
            if( vc is BookingTrackingViewController){
                
                let controller = vc as! BookingTrackingViewController
                
                print("DashBoard")
                controller.MovetoDashBoard()
            }else if(vc is BookingViewController){
                let controller = vc as! BookingViewController
                
                print("DashBoard")
                controller.goToDashBoard()
            }
        }
    }
    
    func startTimer(){
        
        if let wd = self.window {
            var vc = wd.rootViewController
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).visibleViewController
            }
            if( vc is BookingTrackingViewController){
                
                let controller = vc as! BookingTrackingViewController
                
                self.bookingUpdateTimer = Timer.scheduledTimer(timeInterval: 45.0, target: self, selector: #selector(controller.getBookingStatus), userInfo: nil, repeats: true)
            }
        }
    }
    
    func resetTimer(){
        self.bookingUpdateTimer.invalidate()
        self.startTimer()
    }
    
    
    func handlePushNotifications(dataDictionary : [AnyHashable : Any]){
        if UserDefaults.standard.bool(forKey:kISSIGNEDIN) {
        print(dataDictionary)
        if((dataDictionary["from"] != nil) && ((dataDictionary["from"] as? String)?.hasPrefix("/topics/"))! ){
            /*  if (dataDictionary["from"] as! String) == "/topics/FAQ"{
             
             let  strUrl = String(format : "%@%@",kServerUrl,"faqList") as NSString
             
             let dictParams = ["customerApp": "true",
             "ambulanceApp": "false",
             "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
             as NSDictionary
             
             let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
             
             sharedCon.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
             {
             let strStatusCode = result["statusCode"] as? String
             if strStatusCode == "300" {
             
             print(result)
             
             let FAQlist = result["responseData"] as! NSArray
             UserDefaults.standard.set(FAQlist, forKey:kFAQ )
             }
             
             }
             }, failureHandler: {(error) in})
             }
             else if (dataDictionary["from"] as! String) == "topics/PRIVACY-POLICY"{
             
             let  strUrl = String(format :"%@getPrivacyPolicy",kServerUrl) as NSString
             
             let url : NSURL = NSURL(string: strUrl as String)!
             
             sharedCon.requestGETURL(strURL:url, success:{(result) in
             DispatchQueue.main.async()
             {
             let strStatusCode = result["statusCode"] as? String
             
             if strStatusCode == "300"
             {
             
             let privacypolicy = (result["responseData"] as! NSDictionary).value(forKey: "privacy_and_policy") as! String
             UserDefaults.standard.set(privacypolicy, forKey: kPP)
             }
             }
             
             }, failure:  {(error) in
             })
             
             }else if (dataDictionary["from"] as! String) == "/topics/TNC"{
             
             let  strUrl = String(format :"%@getTermsAndConditions",kServerUrl) as NSString
             
             let url : NSURL = NSURL(string: strUrl as String)!
             
             sharedCon.requestGETURL(strURL:url, success:{(result) in
             DispatchQueue.main.async()
             {
             let strStatusCode = result["statusCode"] as? String
             
             if strStatusCode == "300"
             {
             
             let TNC = ((result["responseData"] as! NSArray).firstObject as! NSDictionary).value(forKey: "terms_and_conditions") as! String
             
             UserDefaults.standard.set(TNC, forKey: kTNC)
             }
             
             }
             
             }, failure:  {(error) in
             })
             }else if (dataDictionary["from"] as! String) == "/topics/HOW-DO-YOU-KNOW"{
             let  strUrl = String(format :"%@getComeToKnowBy",kServerUrl) as NSString
             
             let url : NSURL = NSURL(string: strUrl as String)!
             
             sharedCon.requestGETURL(strURL:url, success:{(result) in
             DispatchQueue.main.async()
             {
             let strStatusCode = result["statusCode"] as? String
             
             if strStatusCode == "300"
             {
             let arrDictHowDoYouKnow = (result["responseData"] as! NSMutableArray)
             
             UserDefaults.standard.set(arrDictHowDoYouKnow,forKey : kHowDoYouKnowList )
             
             }
             }
             
             }, failure:  {(error) in
             })
             }else*/
            if (dataDictionary["from"] as! String) == "/topics/EMERGENCY-TYPES"{
                
                let  strUrl = String(format : "%@%@",kServerUrl,"getEmergencyTypeList") as NSString
                
                let dictParams = ["":""] as NSDictionary
                let dictHeaders = ["":""] as NSDictionary
                
                sharedCon.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                    DispatchQueue.main.async()
                        {
                            let strStatusCode = result.value(forKey: "statusCode") as? String
                            
                            if strStatusCode == "300"
                            {
                                let arrEmergencyTypeList =  result.value(forKey: "responseData") as! NSMutableArray
                                
                                for i in 0..<arrEmergencyTypeList.count{
                                    let tempDict = arrEmergencyTypeList.object(at: i) as! NSMutableDictionary
                                    
                                    for (key, value) in tempDict {
                                        if value is NSNull{
                                            tempDict[key] = ""
                                            arrEmergencyTypeList.replaceObject(at: i, with: tempDict)
                                        }
                                    }
                                }
                                UserDefaults.standard.set(arrEmergencyTypeList,forKey : kEmergencyList)
                            }
                    }
                }, failureHandler: {(error) in
                })
            }
            
        }else{
            
            if( (dataDictionary["type"] as! String) == "booking_confirmed"){
                print("booking_confirmed")
                
                let xyz = dataDictionary[AnyHashable("title")]!
                let dict = convertToDictionary(text: xyz as! String)
                
                let notificationName = Notification.Name("booking_confirmed")
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: dict )
                
            }else if( (dataDictionary["type"] as! String) == "ambulanceUpdate"){
                print("ambulanceUpdate")
              //  self.resetTimer()
                let notificationName = Notification.Name("ambulanceUpdate")
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: dataDictionary )
                
            }else if( (dataDictionary["type"] as! String) == "bookingStatusUpdate"){
                print("bookingStatusUpdate")
                //self.resetTimer()
                let notificationName = Notification.Name("bookingStatusUpdate")
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: dataDictionary)
                
                
                if let wd = self.window {
                    var vc = wd.rootViewController
                    if(vc is UINavigationController){
                        vc = (vc as! UINavigationController).visibleViewController
                    }
                    
                    if !(vc is BookingTrackingViewController){
                        
                        self.activebookingStatus()
                        
                    }
                }
                
            }else if( (dataDictionary["type"] as! String) == "cancel_booking"){
                print("cancel_booking")
                
                //  let notificationName = Notification.Name("cancel_booking")
                // NotificationCenter.default.post(name: notificationName, object: nil, userInfo: dataDictionary )
                self.cancelFromBookingTracking()
                
            }else if( (dataDictionary["type"] as! String) == "cancelledBooking"){
                print("cancelledBooking")
                //  let notificationName = Notification.Name("cancel_booking")
                // NotificationCenter.default.post(name: notificationName, object: nil, userInfo: dataDictionary )
                self.cancelFromBookingTracking()
                
            }else if( (dataDictionary["type"] as! String) == "command_centre_cancelled_booking"){
                print("command_centre_cancelled_booking")
                
                //  let notificationName = Notification.Name("cancel_booking")
                // NotificationCenter.default.post(name: notificationName, object: nil, userInfo: dataDictionary )
                
                let alert = UIAlertController(title: "", message: "Command center has cancelled the booking", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive, handler: { (alert: UIAlertAction!) in
                    
                }))
                
                self.cancelFromBookingTracking()
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                
            }else if( (dataDictionary["type"] as! String) == "blood_request"){
                
                let alert = UIAlertController(title:dataDictionary["title"] as! String? , message: "", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive, handler: { (alert: UIAlertAction!) in
                    
                }))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                print("blood_request")
                
            }else if( (dataDictionary["type"] as! String) == "booking_transferred"){
                print("booking_transferred")
                if let wd = self.window {
                    var vc = wd.rootViewController
                    if(vc is UINavigationController){
                        vc = (vc as! UINavigationController).visibleViewController
                    }
                    
                    if( vc is BookingTrackingViewController){
                        
                        let controller = vc as! BookingTrackingViewController
                        
                        print("DashBoard")
                        controller.BookingTransferred()
                    }else if(vc is BookingViewController){
                        
                    }else {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let bookingCon = storyBoard.instantiateViewController(withIdentifier:"BookingViewController") as! BookingViewController
                    
                        vc?.navigationController?.pushViewController(bookingCon, animated: true)
                        return
                    }
                }
            }else if( (dataDictionary["type"] as! String) == "ambulanceTrasnferredTheBooking"){
                print("ambulanceTrasnferredTheBooking")
                if let wd = self.window {
                    var vc = wd.rootViewController
                    if(vc is UINavigationController){
                        vc = (vc as! UINavigationController).visibleViewController
                    }
                    
                    if( vc is BookingTrackingViewController){
                        
                        let controller = vc as! BookingTrackingViewController
                        print("DashBoard")
                        controller.BookingTransferred()
                    }else if(vc is BookingViewController){
                        
                    }else {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)

                        let bookingCon = storyBoard.instantiateViewController(withIdentifier:"BookingViewController") as! BookingViewController
                        vc?.navigationController?.pushViewController(bookingCon, animated: true)
                        return
                    }
                }
                
            }else if( (dataDictionary["type"] as! String) == "amb_transferred_booking_confirmed"){
                print("amb_transferred_booking_confirmed")
                
                let xyz = dataDictionary[AnyHashable("title")]!
                let dict = convertToDictionary(text: xyz as! String)
                
                let notificationName = Notification.Name("booking_confirmed")
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: dict )
                
            }else if( (dataDictionary["type"] as! String) == "customer_logout"){
                print("customer_logout")
                UserDefaults.standard.set(false, forKey: kISSIGNEDIN)
               // print(UserDefaults.value(forKey:kISSIGNEDIN))
                UserDefaults.standard.synchronize()
                
                self.logOut()
                
            }else if( (dataDictionary["type"] as! String) == "new_banner") {
                
                print("new_banner")
                
                if let wd = self.window {
                    var vc = wd.rootViewController
                    if(vc is UINavigationController){
                        vc = (vc as! UINavigationController).visibleViewController
                    }
                    
                    if (vc is PromosViewController){
                        let controller = vc as! PromosViewController
                        controller.getPromos()
                    }
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let xyz = dataDictionary[AnyHashable("title")]!
                    let dict = convertToDictionary(text: xyz as! String)
                    
                    let promoCon = storyBoard.instantiateViewController(withIdentifier:"PromoDetailViewController") as! PromoDetailViewController
                    
                    promoCon.promodetailsDict = dict as! NSDictionary
                    vc?.navigationController?.present(promoCon, animated: true, completion:nil)
            }
            }
        }
        }
    }
    
    func activebookingStatus(){
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getActiveBooking") as NSString
        
        
        let dictParams = ["iOS":"true",
        "ambulanceApp": "false",
        "customerApp": "true",
        "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
        "customer_id":UserDefaults.standard.value(forKey: kCustomer_id)as! String]
        as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        
        sharedCon.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
        DispatchQueue.main.async()
        {
        let strStatusCode = result.value(forKey: "statusCode") as! String
        print("Result --? \(result["statusCode"])")
        if strStatusCode == "300"
        {
         let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let trackingCon = storyBoard.instantiateViewController(withIdentifier:"BookingTrackingViewController") as! BookingTrackingViewController
        
        if !((result.value(forKey: "responseData") as! NSDictionary).value(forKey: "is_medical_taxi") as! Bool) {
            trackingCon.isComingFrom = "Ambulance"
        
        }else {
            trackingCon.isComingFrom = "Medical Taxi"
        }
        
        UserDefaults.standard.set( (result.value(forKey: "responseData") as! NSDictionary).value(forKey: "booking_id") as! NSString, forKey: "bookingid")
        
        appDelegate.bookingID = (result.value(forKey: "responseData") as! NSDictionary).value(forKey: "booking_id") as! NSString
        
            
            if let wd = self.window {
                var vc = wd.rootViewController
                if(vc is UINavigationController){
                    vc = (vc as! UINavigationController).visibleViewController
                }
                
                
                if !(vc is BookingTrackingViewController){
                 //   let storyBoard = UIStoryboard(name: "Main", bundle: nil)

                  // let bookingCon = storyBoard.instantiateViewController(withIdentifier:"BookingTrackingViewController") as! BookingTrackingViewController
                    vc?.navigationController?.pushViewController(trackingCon, animated: true)
                }
            }
            return
        }
        }
        }, failureHandler: {(error) in
        })
    }
    
    func getModel() -> String {
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        var strModel = String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
        let arr = strModel.components(separatedBy: ",") as NSArray
        strModel = arr.object(at: 0) as! String
     return strModel
    }
}

// [END ios_10_data_message_handling]



