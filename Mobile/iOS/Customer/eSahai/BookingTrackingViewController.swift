//
//  BookingTrackingViewController.swift
//  eSahai
//
//  Created by PINKY on 08/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Polyline


class BookingTrackingViewController: BaseViewController,MKMapViewDelegate,CLLocationManagerDelegate,delegateUpdate {
    
    @IBOutlet weak var ambulanceStatus: UILabel!
    @IBOutlet weak var BookingID: UILabel!
    @IBOutlet weak var labelETA: UILabel!
    
    @IBOutlet weak var trackingMapView: MKMapView!

    @IBOutlet weak var DriverImage: UIImageView!
    @IBOutlet weak var lblDriver: UILabel!
    @IBOutlet weak var lblDriverNumber: UILabel!
    
   // var booking_id = NSString()
    var timer = Timer()
    var dataDictionary = NSDictionary()
    var canBeCancelled : Bool = true
    var fromBooking : Bool = false
    var emergencyCoordinates = CLLocationCoordinate2D()
    var hospitalCoordinate = CLLocationCoordinate2D()
    var ambulanceCoordinate = CLLocationCoordinate2D()
    var isComingFrom : NSString = ""
    let TripDictionary : NSDictionary? = UserDefaults.standard.value(forKey: ktripDetails) as? NSDictionary
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print(self.navigationController?.viewControllers)
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Tracking Your \(isComingFrom)"
        navigationController?.navigationBar.barTintColor = UIColor(hex: "#195F92")
        navigationController!.navigationBar.tintColor = UIColor.white
        //navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        UserDefaults.standard.setValue(true, forKey:"isTrackingOn")
        //UserDefaults.standard.set( booking_id, forKey: "bookingid")
        timer = Timer.scheduledTimer(timeInterval: 45.0, target: self, selector: #selector(self.upDateInformation), userInfo: nil, repeats: true)
        ambulanceStatus.text = "\(isComingFrom) has accepted your request"


        let not = Notification.Name("bookingStatusUpdate")
        NotificationCenter.default.addObserver(self, selector: #selector(BookingTrackingViewController.bookingStatus(notification:)), name: not, object: nil)
       
        let not1 = Notification.Name("ambulanceUpdate")
        NotificationCenter.default.addObserver(self, selector: #selector(BookingTrackingViewController.ambulanceUpdate(notification:)), name: not1, object: nil)
        
      //  let not2 = Notification.Name("cancel_booking")
       // NotificationCenter.default.addObserver(self, selector: #selector(BookingTrackingViewController.MovetoDashBoard), name: not2, object: nil)
        
        DriverImage.layer.borderWidth = 1
        DriverImage.layer.masksToBounds = false
        DriverImage.layer.borderColor = UIColor.white.cgColor
        DriverImage.layer.cornerRadius = DriverImage.frame.size.height/2
        DriverImage.clipsToBounds = true
        
        if !(fromBooking){
            self.getBookingStatus()
            appDelegate.bookingID = UserDefaults.standard.value(forKey: "bookingid") as! NSString
            print(appDelegate.bookingID)
        }else{
            self.setUpMapView(dataDict:dataDictionary)
            self.setUpView(dataDict:dataDictionary)
            UserDefaults.standard.set( appDelegate.bookingID, forKey: "bookingid")
            
        }
        
        appDelegate.bookingUpdateTimer = Timer.scheduledTimer(timeInterval: 45.0, target: self, selector: #selector(self.getBookingStatus), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       /* guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Ambulance Tracking Screen")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])*/
        
      /*  booking_id = TripDictionary.value(forKey: "")
        ambulanceCoordinate = CLLocationCoordinate2D(latitude:, longitude:)
        hospitalCoordinate = CLLocationCoordinate2D(latitude:, longitude:)
        emergencyCoordinates = CLLocationCoordinate2D(latitude:, longitude:)*/
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.bookingUpdateTimer.invalidate()
    }
    
    func setUpView(dataDict : NSDictionary){
        self.lblDriver.text = "Driver : \(dataDict.value(forKey: "driverName") as! String)"
        self.lblDriverNumber.text = dataDict.value(forKey: "ambulanceNumber") as?  String
        self.BookingID.text = String(format : "Booking ID \(appDelegate.bookingID)")
       // let url = NSURL(string : (dataDict.value(forKey: "driverProfilePic") as! NSString) as String)
        
        LazyImage.show(imageView:DriverImage, url:dataDict.value(forKey: "driverProfilePic") as? String) {
            () in
            //Lazy loading complete. Do something..
        }
    }
    
    func bookingStatus(notification:NSNotification) -> Void {
        print(notification.userInfo!)
        
        self.fromBooking = true
        switch (notification.userInfo?[AnyHashable("booking_status")] as! String) {
        case "Started":
            print("Started")
            ambulanceStatus.text = "Your \(isComingFrom) is on the way"
        case "ACTD":
            print("ACTD")
            ambulanceStatus.text = ""
        case "WTEL":
            print("WTEL")
            ambulanceStatus.text = ""
        case "AEL":
            print("AEL")
            ambulanceStatus.text = "Your \(isComingFrom) is at Emergency Location"
        case "WBTD":
            print("WBTD")
            
            ambulanceStatus.text = "Your \(isComingFrom) is on the way to the hospital"
            
            DispatchQueue.main.async {
               
                //self.trackingMapView.removeAnnotations(self.trackingMapView.annotations)
                self.trackingMapView.reloadInputViews()
              
                self.hospitalCoordinate =  CLLocationCoordinate2D(latitude:Double(notification.userInfo?[AnyHashable("hospitalLat")] as! String)!  , longitude: Double(notification.userInfo?[AnyHashable("hospitalLong")] as! String)! )
                
                let HospitalAnnotation = NearestHospitalsAnnotation(coordinate: self.hospitalCoordinate)
               // HospitalAnnotation.title = "Hospital"
                HospitalAnnotation.name = "Hospital"
                HospitalAnnotation.id = ""
                self.trackingMapView.addAnnotation(HospitalAnnotation)
                
                let EmergencyAnnotation = NearestHospitalsAnnotation(coordinate:self.emergencyCoordinates)
                
               // EmergencyAnnotation.title = "EmergencyLocation"
                EmergencyAnnotation.name = "EmergencyLocation"
                EmergencyAnnotation.id = ""
                self.trackingMapView.addAnnotation(EmergencyAnnotation)
        
                self.getDirections(source: self.emergencyCoordinates , destination:self.hospitalCoordinate )
            }
            canBeCancelled = false
        case "RD":
            print("RD")
            ambulanceStatus.text = "Your \(isComingFrom) Reached Destination"
        case "TC":
            print("TC")
            
            let dict  = [
                    "distance" : notification.userInfo?[AnyHashable("totalDistance")] as! String ,
                         "speed" : notification.userInfo?[AnyHashable("speed")] as! String,
                         "cost":notification.userInfo?[AnyHashable("cost")] as! String ,
                         "duration": notification.userInfo?[AnyHashable("duration")] as! String]
        
            let tripDetailsCon = self.storyboard?.instantiateViewController(withIdentifier: "TripDetails") as! TripDetails
            tripDetailsCon.data = dict as NSDictionary
           // tripDetailsCon.BookID = booking_id
            timer.invalidate()
            UserDefaults.standard.setValue(false, forKey:"isTrackingOn")
            self.navigationController?.pushViewController(tripDetailsCon, animated: true)
            
        case "CAN":
            print("CAN")
            ambulanceStatus.text = ""
            self.MovetoDashBoard()
            
        default:
            return
        }
    }
    
    
    func ambulanceUpdate(notification:NSNotification) -> Void {
        print(notification.userInfo!)
        
        let sourceCoordinates = CLLocationCoordinate2D(latitude:Double(notification.userInfo?[AnyHashable("latitude")] as! String)!  , longitude: Double(notification.userInfo?[AnyHashable("longitude")] as! String)! )
       var destinationCoordinates = CLLocationCoordinate2D()
        
        if(canBeCancelled){
            destinationCoordinates = emergencyCoordinates
        }else{
            destinationCoordinates = self.hospitalCoordinate
        }
        
        let annoArray  = self.trackingMapView.annotations
        
        for annotation in annoArray{
            let anno = annotation as! NearestHospitalsAnnotation
            if (anno.name as String? == "Ambulance") {
               anno.coordinate = sourceCoordinates
            }
        }
        
        //self.trackingMapView.addAnnotation(AmbulanceAnnotation)
        
        
        self.getDirections(source: sourceCoordinates, destination: destinationCoordinates)
    }
    
    func cancelBooking(notification:NSNotification) -> Void {
        print(notification.userInfo!)
         self.MovetoDashBoard()
        }
    
    func setUpMapView(dataDict : NSDictionary) {
        print(dataDict)
      //  print(dataDict[AnyHashable("emergencyLatitude")]!)
        print(dataDict.value(forKey: "emergencyLongitude")!)
        
       // var ambulanceCoordinates = CLLocationCoordinate()
        
        if !(fromBooking){
            ambulanceCoordinate = CLLocationCoordinate2D(latitude: Double(dataDict.value(forKey:"ambulance_latitude") as! String)!   , longitude: Double(dataDict.value(forKey: "ambulance_longitude") as! String)!)
        }else{
            ambulanceCoordinate = CLLocationCoordinate2D(latitude: dataDict[AnyHashable("ambulance_latitude")]! as! CLLocationDegrees, longitude: dataDict[AnyHashable("ambulance_longitude")]! as! CLLocationDegrees)
        }
        
        let AmbulanceAnnotation = NearestHospitalsAnnotation(coordinate: ambulanceCoordinate )
        
        
        
      //  AmbulanceAnnotation.title = "Ambulance"
        AmbulanceAnnotation.name = "Ambulance"
        AmbulanceAnnotation.id = ""
        
        self.trackingMapView.addAnnotation(AmbulanceAnnotation)
        if !(fromBooking){
            emergencyCoordinates = CLLocationCoordinate2D(latitude: Double(dataDict.value(forKey:"emergencyLatitude") as! String)!  , longitude: Double(dataDict.value(forKey:"emergencyLongitude") as! String)!)
        }else{
           // emergencyCoordinates = CLLocationCoordinate2D(latitude: Double(dataDict.value(forKey:"emergencyLatitude") as! String)!  , longitude: Double(dataDict.value(forKey:"emergencyLongitude") as! String)!)
            
             emergencyCoordinates = CLLocationCoordinate2D(latitude: dataDict[AnyHashable("emergencyLatitude")]! as! CLLocationDegrees, longitude: dataDict[AnyHashable("emergencyLongitude")]! as! CLLocationDegrees)
        }
        
        let EmergencyAnnotation = NearestHospitalsAnnotation(coordinate:emergencyCoordinates )
        
      //  EmergencyAnnotation.title = "EmergencyLocation"
        EmergencyAnnotation.name = "EmergencyLocation"
        EmergencyAnnotation.id = ""
        self.trackingMapView.addAnnotation(EmergencyAnnotation)
        
        
        
       print (emergencyCoordinates)
        
        self.getDirections(source: emergencyCoordinates , destination:AmbulanceAnnotation.coordinate )
    }

    func upDateInformation(){
        
    }
    
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
        if(canBeCancelled) {
            
            let alertController = UIAlertController(title: "",message: "Are you sure you want to cancel the trip?", preferredStyle: UIAlertControllerStyle.alert)
            
            let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                
            }
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                let  strUrl = String(format : "%@%@",kServerUrl,"cancelBooking") as NSString
                
                let dictParams = ["iOS":"true",
                                  "ambulanceApp": "false",
                                  "customerApp": "true",
                                  "booking_id": self.appDelegate.bookingID,
                                  "customer_id": UserDefaults.standard.value(forKey: kCustomer_id)as! String,
                                  "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
                    as NSDictionary
                let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
                
                self.sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                    DispatchQueue.main.async()
                        {
                            let strStatusCode = result["statusCode"] as? String
                            
                            if strStatusCode == "300" {
                                   self.MovetoDashBoard()
                            }else if strStatusCode == "204"{
                                self.logout()
                            }
                            else if(strStatusCode != "500"){
                                self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                            }
                    }
                }, failureHandler: {(error) in
                })
                
                
            }
            alertController.addAction(DestructiveAction)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
            
        }else {
            let alertController = UIAlertController(title: "", message: "Booking Cannot be cancelled", preferredStyle: UIAlertControllerStyle.alert)
            
            let DestructiveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(DestructiveAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func MovetoDashBoard() {
        self.timer.invalidate()
        UserDefaults.standard.setValue(false, forKey:"isTrackingOn")
        
        print("\(self.navigationController?.viewControllers)")
        
      //  for VC: UIViewController in (self.navigationController?.viewControllers)!         
        for VC: UIViewController in self.navigationController!.viewControllers {
            if (VC is DashboardViewController) {
                self.navigationController!.popToViewController(VC, animated: true)
                return
            }
        }
        
        let signIn = self.storyboard?.instantiateViewController(withIdentifier:"DashboardViewController") as! DashboardViewController
        self.navigationController?.pushViewController(signIn, animated: false)
        
        return

    }
    
    func BookingTransferred() {
        
        appDelegate.bookingString = "Your booking has been cancelled by the ambulance.Hold on while we are assigning your booking to another ambulance."
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateInfoBtnAction(_ sender: Any) {
        let updateDetailsCon = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsViewController") as! UserDetailsViewController
       // updateDetailsCon.BookId = self.booking_id
        updateDetailsCon.testDelegate = self
        self.navigationController?.pushViewController(updateDetailsCon, animated: true)
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.red
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }else{
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.fillColor = UIColor.blue.withAlphaComponent(0.1)
            circleRenderer.strokeColor = UIColor.blue
            circleRenderer.lineWidth = 1
            return circleRenderer
        }
    }
    
    // MARK: - Annotations
    func setAnnotationMarkers(_ lat: Double, andLongitude long: Double , annotationDict : NSDictionary)  {
        
        DispatchQueue.main.async {
        //    self.trackingMapView.removeAnnotations(self.trackingMapView.annotations)
            self.trackingMapView.reloadInputViews()
                let xyz =  CLLocationCoordinate2D(latitude: lat , longitude: long)
                let point = NearestHospitalsAnnotation(coordinate: xyz)
            //    point.title = annotationDict.value(forKey: "name") as! String!
                point.name = annotationDict.value(forKey: "name") as! String!
                point.id = annotationDict.value(forKey: "id") as! String!
                
                self.trackingMapView.addAnnotation(point)
        }
    }
    
    // MARK: - MapView Delegate Methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
        }else{
            annotationView?.annotation = annotation
        }
        
        let customAnnotation = annotation as! NearestHospitalsAnnotation
        
       
        
        if (customAnnotation.name == "EmergencyLocation"){
            
            annotationView?.image = UIImage(named: "LocationCone-0.png")
            
        }else if (customAnnotation.name == "Ambulance"){
            
            annotationView?.image = UIImage(named: "AmbulanceMarker.png")
            
        }else {
            annotationView?.image = UIImage(named: "Location Icon-01.png")
          
        }
        print(customAnnotation.name)
        annotationView?.canShowCallout = false
        return annotationView
    }
    
    func getDirections( source : CLLocationCoordinate2D, destination : CLLocationCoordinate2D ){
        
         var directionURL1 = "\("https://maps.googleapis.com/maps/api/directions/json")?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&key=\(kAPI_Key)"
        print("directionURL -- > \(directionURL1)")
        
        directionURL1 = directionURL1.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
        let request = URLRequest(url: URL(string:directionURL1)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                if error == nil {
                    let object = try? JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    
                    let routes = object?["routes"] as! [NSDictionary]
                    for route in routes {
                        let overviewPolyline = route["overview_polyline"] as! NSDictionary
                        let ETAdict = (route["legs"] as! NSArray).firstObject as! NSDictionary
                        
                        let distance = (ETAdict.value(forKey: "distance") as! NSDictionary).value(forKey: "text") as! String
                        
                        let time = (ETAdict.value(forKey: "duration") as! NSDictionary).value(forKey: "text") as! String
                       
                        self.labelETA.text = String(format: "ETA is approximatly \(distance) away / \(time)" )
                        
                        let points = overviewPolyline["points"] as! String
                        
                    //    let xyz: [CLLocationCoordinate2D]? = 
                        let polylineCoordinates : [CLLocationCoordinate2D] = decodePolyline(points)!
                        
                        
                        print("polylineCoordinates \(polylineCoordinates)")
                        let mkPolyline = MKPolyline(coordinates: polylineCoordinates, count: (polylineCoordinates.count))
                      
                  //  self.trackingMapView.removeOverlays(self.trackingMapView.overlays)

                        self.trackingMapView.reloadInputViews()
                        self.trackingMapView.add(mkPolyline)
                      //  self.trackingMapView.setVisibleMapRect( mkPolyline.boundingMapRect, animated: true)
                        
                        self.trackingMapView.setVisibleMapRect( mkPolyline.boundingMapRect, edgePadding: UIEdgeInsetsMake(30, 30, 30, 30), animated: true)
                    }
                }
                else {
                    print(error!)
                }
            }
        })
        task.resume()
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }

    @IBOutlet weak var callingView: UIView!
    
    @IBAction func CallFunction(_ sender: Any) {
        
        if(callingView.isHidden){
            callingView.isHidden = false
        }else{
            callingView.isHidden = true
        }
    }
    
    
    @IBAction func CallCustomercare(_ sender: Any) {
        guard let number = URL(string: "telprompt://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)")
            else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            if let url = URL(string: "tel://\(UserDefaults.standard.value(forKey: "CustomerCare") as! String)") {
                UIApplication.shared.openURL(url)
            }
        }
        callingView.isHidden = true
    }
    
    @IBAction func callAmbulance(_ sender: Any) {
        
        let  strUrl = "http://call.esahai.in/ConVox3.0/user_conference.php?conf_no_list=\(UserDefaults.standard.value(forKey: kMobile) as! String),\(self.dataDictionary.value(forKey: kDriverNum)!)"
    
        
        print("strurl --> \(strUrl)")
        
        let url : NSURL = NSURL(string: strUrl as String)!
        
        sharedController.requestGETURL(strURL:url, success:{(result) in
            DispatchQueue.main.async()
                {
                    print("result \(result)")
            }
            
        }, failure:  {(error) in
        })
        callingView.isHidden = true
    }
    
    func wayBackToDestination(dict : NSDictionary){
        
        ambulanceStatus.text = "Your \(isComingFrom) is on the way to the hospital"
        
        DispatchQueue.main.async {
            
           // self.trackingMapView.removeAnnotations(self.trackingMapView.annotations)
            self.trackingMapView.reloadInputViews()
            
            self.hospitalCoordinate =  CLLocationCoordinate2D(latitude:Double(dict.value(forKey:"hospitalLat") as! String)!  , longitude: Double(dict.value(forKey:"hospitalLong") as! String)! )
            
            let HospitalAnnotation = NearestHospitalsAnnotation(coordinate: self.hospitalCoordinate)
          //  HospitalAnnotation.title = "Hospital"
            HospitalAnnotation.name = "Hospital"
            HospitalAnnotation.id = ""
            self.trackingMapView.addAnnotation(HospitalAnnotation)
            
            self.emergencyCoordinates =  CLLocationCoordinate2D(latitude:Double(dict.value(forKey:"emergencyLatitude") as! String)!  , longitude: Double(dict.value(forKey:"emergencyLongitude") as! String)! )
            
            let EmergencyAnnotation = NearestHospitalsAnnotation(coordinate:self.emergencyCoordinates)
            
         //   EmergencyAnnotation.title = "EmergencyLocation"
            EmergencyAnnotation.name = "EmergencyLocation"
            EmergencyAnnotation.id = ""
            self.trackingMapView.addAnnotation(EmergencyAnnotation)
            
            self.getDirections(source: self.emergencyCoordinates , destination:self.hospitalCoordinate )
            
        }
        canBeCancelled = false
    }
    
    func getBookingStatus(){
        
        DispatchQueue.main.async {
            self.trackingMapView.removeAnnotations(self.trackingMapView.annotations)
            
             self.trackingMapView.removeOverlays( self.trackingMapView.overlays)
        }
        self.fromBooking = false
        
        let  strUrl = String(format : "%@%@",kServerUrl,"checkMyBookingStatus") as NSString
        
        let dictParams = ["iOS":"true",
                          "customerApp": "true",
                          "ambulanceApp": "false",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                         "booking_id" :UserDefaults.standard.value(forKey: "bookingid") as! String]
            as NSDictionary
        
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
       
        print(dictParams)
        print(dictHeaders)
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                
                if strStatusCode == "300" {
                    let responseDict = result["responseData"] as? NSDictionary
                    self.dataDictionary = (result["responseData"] as! NSDictionary)
                    if(responseDict?.value(forKey: "booking_status") as! String == "ACTD"){
                        self.setUpView(dataDict:responseDict! )
                        self.setUpMapView(dataDict: responseDict!)
                        self.ambulanceStatus.text = "\(self.isComingFrom) has accepted your request"

                    }else if(responseDict?.value(forKey: "booking_status") as! String == "WTEL"){
                        self.setUpView(dataDict:responseDict! )
                        self.setUpMapView(dataDict: responseDict!)
                        self.ambulanceStatus.text = "Your \(self.isComingFrom) is on the way"
                    }else if(responseDict?.value(forKey: "booking_status") as! String == "AEL"){
                        self.setUpView(dataDict:responseDict! )
                        self.setUpMapView(dataDict: responseDict!)
                        self.ambulanceStatus.text = "Your \(self.isComingFrom) is at Emergency Location"
                    }else if(responseDict?.value(forKey: "booking_status") as! String == "WBTD"){
                        self.setUpView(dataDict:responseDict! )
                        self.setUpMapView(dataDict: responseDict!)
                        
                        self.ambulanceStatus.text = "Your \(self.isComingFrom) is on the way to the hospital"
                        
                        DispatchQueue.main.async {
                            
                            // self.trackingMapView.removeAnnotations(self.trackingMapView.annotations)
                            self.trackingMapView.reloadInputViews()
                            
                            self.hospitalCoordinate =  CLLocationCoordinate2D(latitude:Double(responseDict?.value(forKey:"hospital_latitude") as! String)!  , longitude: Double(responseDict?.value(forKey:"hospital_longitude") as! String)! )
                            
                            let HospitalAnnotation = NearestHospitalsAnnotation(coordinate: self.hospitalCoordinate)
                       //     HospitalAnnotation.title = "Hospital"
                            HospitalAnnotation.name = "Hospital"
                            HospitalAnnotation.id = ""
                            self.trackingMapView.addAnnotation(HospitalAnnotation)
                            
                            self.emergencyCoordinates =  CLLocationCoordinate2D(latitude:Double(responseDict?.value(forKey:"emergencyLatitude") as! String)!  , longitude: Double(responseDict?.value(forKey:"emergencyLongitude") as! String)! )
                            
                            let EmergencyAnnotation = NearestHospitalsAnnotation(coordinate:self.emergencyCoordinates)
                            
                           // EmergencyAnnotation.title = "EmergencyLocation"
                            EmergencyAnnotation.name = "EmergencyLocation"
                            EmergencyAnnotation.id = ""
                            self.trackingMapView.addAnnotation(EmergencyAnnotation)
                            
                            self.getDirections(source: self.emergencyCoordinates , destination:self.hospitalCoordinate )
                            
                        }
                        self.canBeCancelled = false
                        
                        
                    }else if(responseDict?.value(forKey: "booking_status") as! String == "RD"){
                        self.setUpView(dataDict:responseDict! )
                        self.setUpMapView(dataDict: responseDict!)
                        self.ambulanceStatus.text = "Your \(self.isComingFrom) Reached Destination"
                    }else if(responseDict?.value(forKey: "booking_status") as! String == "TC"){
                        
                        let tripDetailsCon = self.storyboard?.instantiateViewController(withIdentifier: "TripDetails") as! TripDetails
                        tripDetailsCon.data = responseDict!
                        //tripDetailsCon.BookID = self.booking_id
                        self.timer.invalidate()
                        UserDefaults.standard.setValue(false, forKey:"isTrackingOn")
                        self.navigationController?.pushViewController(tripDetailsCon, animated: true) 
                    }
                }else if strStatusCode == "411"{
                    self.MovetoDashBoard()
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
                else if(strStatusCode != "500"){
                    self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
    
    func updateData(){
        fromBooking = false
        self.getBookingStatus()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first!
        if touch.view != callingView {
            if !(callingView.isHidden){
                callingView.isHidden = true
            }
        }
    }
}



