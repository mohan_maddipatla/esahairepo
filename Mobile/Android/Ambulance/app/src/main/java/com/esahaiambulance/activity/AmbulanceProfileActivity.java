package com.esahaiambulance.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.esahaiambulance.R;
import com.esahaiambulance.adapters.AmbulanceAttributesAdapter;
import com.esahaiambulance.db.DatabaseHelper;
import com.esahaiambulance.models.AmbulanceAttributesModel;
import com.esahaiambulance.models.EmergencyType;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;

import java.util.ArrayList;

import eu.fiskur.chipcloud.ChipCloud;

public class AmbulanceProfileActivity extends AppCompatActivity {

    Button btnSubmit;
    EditText etRegistrationNumber, etVehicleType, etGroupName;
    //Spinner spinnerVehicleType;
    ChipCloud chipCloud;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance_profile);
        initView();
       /* ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
                .createFromResource(this, R.array.vehicleType,
                        R.layout.my_spinner_layout);
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/

        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);

        final ArrayList<EmergencyType> emergencyTypeArrayList = dbHelper.getEmergencyTypes();

        for(EmergencyType emer:emergencyTypeArrayList){
            chipCloud.addChip(emer.getTypeName());
        }

        LinearLayout llAttributesHeader = (LinearLayout)findViewById(R.id.llAttributesHeader);
        ArrayList<AmbulanceAttributesModel> list = dbHelper.getAmbulanceAttributes();

        AmbulanceAttributesAdapter adapter = new AmbulanceAttributesAdapter(this,list );
        llAttributesHeader.setVisibility((list.size()>0)?View.VISIBLE:View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        //spinnerVehicleType.setAdapter(spinnerAdapter);
        //  chipCloud.getS
    }

    private void initView() {

        etRegistrationNumber = (EditText) findViewById(R.id.etRegistrationNumber);
     //   etCapacity = (EditText) findViewById(R.id.etCapacity);
        etVehicleType = (EditText) findViewById(R.id.etVehicleType);
        etGroupName = (EditText) findViewById(R.id.etGroupName);
        //spinnerVehicleType = (Spinner) findViewById(R.id.spinnerVehicleType);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AmbulanceProfileActivity.this, DriverListActivity.class);
                startActivity(intent);
            }
        });
        chipCloud = (ChipCloud) findViewById(R.id.chip_cloud);

        etVehicleType.setText(Common.getStringPref(this, Constants.vehicleType, ""));
        etRegistrationNumber.setText(Common.getAmbulanceNumber(this));
        etGroupName.setText(Common.getStringPref(this, Constants.ambGroupNmae, ""));
    }
}
