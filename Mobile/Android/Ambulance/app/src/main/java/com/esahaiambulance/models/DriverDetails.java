package com.esahaiambulance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by prashanth on 19/9/16.
 */

public class DriverDetails implements Serializable {


    @SerializedName("driver_id")
    private String driverId;
    @SerializedName("driver_name")
    private String driverName;
    @SerializedName("driver_mobile_number")
    private String driverMobileNumber;
    @SerializedName("driver_email")
    private String driverEmail;
    @SerializedName("file_url")
    private String fileUrl;

    /**
     * @return The driverId
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * @param driverId The driver_id
     */
    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    /**
     * @return The driverName
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * @param driverName The driver_name
     */
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    /**
     * @return The driverMobileNumber
     */
    public String getDriverMobileNumber() {
        return driverMobileNumber;
    }

    /**
     * @param driverMobileNumber The driver_mobile_number
     */
    public void setDriverMobileNumber(String driverMobileNumber) {
        this.driverMobileNumber = driverMobileNumber;
    }

    /**
     * @return The driverEmail
     */
    public String getDriverEmail() {
        return driverEmail;
    }

    /**
     * @param driverEmail The driver_email
     */
    public void setDriverEmail(String driverEmail) {
        this.driverEmail = driverEmail;
    }

    /**
     * @return The fileUrl
     */
    public String getFileUrl() {
        return fileUrl;
    }

    /**
     * @param fileUrl The file_url
     */
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }


}
