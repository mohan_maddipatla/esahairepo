package com.esahaiambulance.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esahaiambulance.R;
import com.esahaiambulance.adapters.NavExpandableListAdapter;
import com.esahaiambulance.adapters.TripRejectionReasonAdapter;
import com.esahaiambulance.db.DatabaseHelper;
import com.esahaiambulance.firebase.MyFirebaseMessagingService;
import com.esahaiambulance.fragments.CustomMapFragment;
import com.esahaiambulance.models.BookingDetails;
import com.esahaiambulance.models.DirectionResults;
import com.esahaiambulance.models.EmergencyType;
import com.esahaiambulance.models.HospitalList;
import com.esahaiambulance.models.NavExpandedMenuModel;
import com.esahaiambulance.utils.AppObserver;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.utils.HeartbeatReceiver;
import com.esahaiambulance.utils.MapWrapperLayout;
import com.esahaiambulance.utils.MyAlertListener;
import com.esahaiambulance.utils.OnItemClickListener;
import com.esahaiambulance.utils.PermissionUtils;
import com.esahaiambulance.utils.RouteDecode;
import com.esahaiambulance.utils.SendPushNotification;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.esahaiambulance.webservices.CallbackWithRetry;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;

/**
 * Created by prashanth on 20/9/16.
 */

@SuppressWarnings({"MissingPermission"})
public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener, LocationListener, View.OnClickListener,
        AppObserver {

    private final int MY_LOCATION_PERMISSION_REQUEST_CODE = 1;

    private GoogleMap mMap = null;

    Context mContext;
    private UiSettings mUiSettings;
    private boolean mLocationPermissionGranted = false;


    private LocationManager locationManager;
    // private static final long MIN_TIME = 1000 * 5;
    //private static final float MIN_DISTANCE = 30;

    public static final long MIN_TIME = 1000 * 60;
    public static final float MIN_DISTANCE = 10;
    GoogleMap.OnMapClickListener mapClickListener;
    // Location fromPosition;

    CoordinatorLayout coordinatorLayout;
    LinearLayout llCurrentStatus, llLocationDetails;
    Button btnDutyStatus, btnReachedToEmergencyLocation, btnReachedToHospital, btnStartTrip;
    TextView txtCurrentStatus, txtDirection, txtDistanceAndTime, txtPatientName, txtLocation, txtBookingId;
    ImageView imgCallButton, imgCancelTrip, imgNavigate, imgFakeBooking;

    public static final int MY_TRIP_COMPLETED_REQUEST_CODE = 2;
    public static final int MY_EMERGENCY_LOCATION_REACHED_REQUEST_CODE = 3;

    public static final int MY_TRIP_REJECTED = 5;
    public static final int MY_TRIP_ACCEPT_REQUEST_CODE = 4;

    Handler handler = new Handler();


    /// timers to update location
    private Timer myTimerUpdateLocation;
    private TimerTask timerTaskUpdateLocation;

    Location currentLocation = null;
    BookingDetails bookingDetails = null;

    boolean isPathDrawedFromAmbulanceToEmergency = false;

    boolean isPathDrawedFromEmergencyToHospital = false;
    // this flag indicates whether to draw path from emergency to hosptil...
    boolean isDrawPathFromEmergencyToHospital = false;

    /// status updation constants
    private final int reachedHospital = 3;
    private final int reachedEmergencyLocation = 2;
    private final int wayBackToHospital = 5;
    private final int tripCompleted = 6;
    private final int startTrip = 1;
    private final int locationUpdate = -1;
    private final int cancelBooking = 4;

    private Timer timerSendLatLantToCustomer;
    private TimerTask timerTaskSendLatLantToCustomer;

    public boolean isBookingInProgressToDrawPath = false;

    boolean isSendForTwoMinutes = false;


    // flag to stop location update while sending steps update
    boolean isSendingStatusUpdate = false;

    //  private String currentBookingStatus;

    public Dialog dialogAtEmergency;
    public Dialog dialogTripCompleted;
    public Dialog dialogRejectionReason;
    public Dialog dialogWhomToCall;
    DatabaseHelper dbHelper;
    ArrayList<HospitalList.HospitalDetails> nearByHospitals = new ArrayList<HospitalList.HospitalDetails>();

    //  int countToAddMarker = 0;
    //   private FirebaseAnalytics mFirebaseAnalytics;

    private MediaPlayer ringTone = null;
    private MediaPlayer cancelRingTone = null;


    public static final int timeDifference = 1000 * 30;
    // public Location previousBestLocation = null;

    private boolean isMovingMapFirstTime = true;

    NavigationView navigationView;

    private Dialog simpleSingleButtonDialog = null;
    // private Dialog simpleTwoButtonDialog = null;

    private Location previousLocation = null;

    private BroadcastReceiver netWorkChangeListener;

    Marker markerAmbulanceStarted;

    float previousZoomLevel = 17.0f;

    boolean isZooming = false;
    ExpandableListView menuListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Log.d("oncreate", "oncreat dashboar");
        MyApplication.getApplication().addObserver(this);
        mContext = this;


        dbHelper = DatabaseHelper.getInstance(this);
        initViews();
        //Common.showAccRegNotification(mContext);
        //Defining toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name_esahai);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        //Defining drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                if(menuListView != null){
                    NavExpandableListAdapter adapter =    (NavExpandableListAdapter) menuListView.getExpandableListAdapter();
                    menuListView.collapseGroup(adapter.lastExpandedGroupPosition);
                    menuListView.smoothScrollToPosition(0);
                }

                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Defining navigationView
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_dashboard);

        //Menu menu = navigationView.getMenu();
        //menu.findItem(R.id.app_version).setTitle("Version: " + getAppVersion());

        CustomMapFragment mapFragment =
                (CustomMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!Common.checkLocationPermission(this)) {
            requestLocationPermission(MY_LOCATION_PERMISSION_REQUEST_CODE);
        } else {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
            mLocationPermissionGranted = true;

            // moveCameraToCurrentLocation(location);
        }

        Log.d("booking details"," getBookingDetails"+Common.getBookingDetails(this));
        if (getIntent().hasExtra(Constants.bookingDetails)) {

            bookingDetails = (BookingDetails) getIntent().getSerializableExtra("bookingDetails");
            Log.d("typeFromNewTripScreen", "typeFromNewTripScreen = " + getIntent().hasExtra("typeFromNewTripScreen"));
            Log.d("bookingDetails", "bookingDetails ::::" + getIntent().hasExtra("isFromService"));
            MyApplication.getApplication().setBookingInProgress(true);
            if (!getIntent().hasExtra("isFromService")
                    && !getIntent().hasExtra("typeFromNewTripScreen")) {
                if (mMap != null && !isPathDrawedFromAmbulanceToEmergency) {

                    drawPathFromAmbulanceToEmergency();
                }
                setStartTrip();
                if (bookingDetails.getBookingType().equalsIgnoreCase(MyFirebaseMessagingService.transferBooking) ||
                        bookingDetails.getBookingType().equalsIgnoreCase(MyFirebaseMessagingService.bookingFromCommandCentre)) {

                    imgCancelTrip.setVisibility(View.GONE);
                    imgFakeBooking.setVisibility(View.GONE);
                }
            } else if (!getIntent().hasExtra("isFromService")
                    && getIntent().hasExtra("typeFromNewTripScreen")) {

                Log.d("typeFromNewTripScreen", "typeFromNewTripScreen");
                update(getIntent().getIntExtra("typeFromNewTripScreen", -1), bookingDetails);
            } else {

                updateBasedOnStatusFromService();
            }

        } else if (getIntent().hasExtra("cancelBooking")) {

            setInitialOnlineStatus();
            startUpdateStatusTimer();
            showBookingCancelledOrTransferredDialog(getIntent().getStringExtra("cancelBooking"));
        } else if (Common.getBookingDetails(this) != null) {

            bookingDetails = Common.getBookingDetails(this);
            updateBasedOnStatusFromService();
        } else {
            setInitialOnlineStatus();
            startUpdateStatusTimer();
        }
        // showAtEmergencyLocationDialog();


        //String token = FirebaseInstanceId.getInstance().getToken();
        //Log.d("device token", "device token " + token);
        netWorkChangeListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


                ConnectivityManager connectivityManager = (ConnectivityManager)
                        context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                //boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();

                boolean isConnected = wifi != null && wifi.isConnectedOrConnecting() ||
                        mobile != null && mobile.isConnectedOrConnecting();
                if (isConnected)
                    Log.i("NET", "connecte" + isConnected);
                else {
                    Log.i("NET", "not connecte" + isConnected);

                    enableNetWork();
                }
            }
        };
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        // mIntentFilter.setPriority(2147483647);
        registerReceiver(netWorkChangeListener, mIntentFilter);


    }

    private void initViews() {

        llCurrentStatus = (LinearLayout) findViewById(R.id.llCurrentStatus);
        llLocationDetails = (LinearLayout) findViewById(R.id.llLocationDetails);

        btnDutyStatus = (Button) findViewById(R.id.btnDutyStatus);
        btnReachedToHospital = (Button) findViewById(R.id.btnReachedToHospital);
        btnReachedToEmergencyLocation = (Button) findViewById(R.id.btnReachedToEmergencyLocation);
        btnStartTrip = (Button) findViewById(R.id.btnStartTrip);

        txtCurrentStatus = (TextView) findViewById(R.id.txtCurrentStatus);

        txtDirection = (TextView) findViewById(R.id.txtDirection);
        txtDistanceAndTime = (TextView) findViewById(R.id.txtDistanceAndTime);
        txtPatientName = (TextView) findViewById(R.id.txtPatientName);
        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtBookingId = (TextView) findViewById(R.id.txtBookingId);

        imgCancelTrip = (ImageView) findViewById(R.id.imgCancelTrip);
        imgCallButton = (ImageView) findViewById(R.id.imgCallButton);
        imgNavigate = (ImageView) findViewById(R.id.imgNavigate);
        imgFakeBooking = (ImageView) findViewById(R.id.imgFakeBooking);

        btnDutyStatus.setOnClickListener(this);
        btnReachedToHospital.setOnClickListener(this);
        btnReachedToEmergencyLocation.setOnClickListener(this);
        btnStartTrip.setOnClickListener(this);

        imgCallButton.setOnClickListener(this);
        imgCancelTrip.setOnClickListener(this);
        imgNavigate.setOnClickListener(this);
        imgFakeBooking.setOnClickListener(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //  LinearLayout header_main = (LinearLayout) navigationView.getHeaderView(0);

        LinearLayout header_main = (LinearLayout) navigationView.findViewById(R.id.nav_header_dashboard);
        // driver details
        CircleImageView profilePic = (CircleImageView) header_main.findViewById(R.id.profilePic);
        TextView txtDriverName = (TextView) header_main.findViewById(R.id.txtDriverName);
        TextView txtDriverMobileNumber = (TextView) header_main.findViewById(R.id.txtDriverMobileNumber);
        TextView txtAmbulanceNumber = (TextView) header_main.findViewById(R.id.txtAmbulanceNumber);

        Picasso.with(this).load(Common.getStringPref(this, Constants.driverProfilePic, "")).into(profilePic);
        txtDriverName.setText(Common.getStringPref(this, Constants.driverName, ""));
        txtDriverMobileNumber.setText(Common.getStringPref(this, Constants.driverMobileNumber, ""));

        txtAmbulanceNumber.setText(Common.getAmbulanceNumber(this) + "\n" + Common.getStringPref(this, Constants.ambGroupNmae, ""));

        ViewTreeObserver vto = llCurrentStatus.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                if (mMap != null) {
                    Fragment fragment = (getSupportFragmentManager().findFragmentById(R.id.map));
                    ViewGroup v1 = (ViewGroup) fragment.getView();
                    ViewGroup v2 = (ViewGroup) v1.getChildAt(0);
                    ViewGroup v3 = (ViewGroup) v2.getChildAt(2);
                    final View position = (View) v3.getChildAt(0);

                    ViewTreeObserver obs = llCurrentStatus.getViewTreeObserver();
                    int positionWidth = position.getLayoutParams().width;
                    int positionHeight = position.getLayoutParams().height;

                    Log.d("width height", "wid hei root layout" + positionWidth + " " + positionHeight);

                    Log.d("width height", "total heigh root layout" + llCurrentStatus.getHeight());
                    //lay out position button
                    RelativeLayout.LayoutParams positionParams = new RelativeLayout.LayoutParams(positionWidth, positionHeight);
                    int margin = 40 + positionWidth / 5;
                    positionParams.setMargins(0, llCurrentStatus.getHeight(), 0, 0);
                    positionParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                    positionParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                    position.setLayoutParams(positionParams);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        obs.removeGlobalOnLayoutListener(this);
                    }
                }
            }

        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDriverProifle = new Intent(mContext, DriverProfileActivity.class);
                startActivity(intentDriverProifle);
            }
        });


//        "Version: " + getAppVersion()

        TextView txtVersion = (TextView) navigationView.findViewById(R.id.txtVersionCode);
        txtVersion.setText(getString(R.string.version) + ": " + getAppVersion());
        // Expandable listview
        menuListView = (ExpandableListView) navigationView.findViewById(R.id.navigationmenu);

        prepareNavigationMenuAndSet(menuListView);

        menuListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                /*final String selected = (String) mMenuAdapter.getGroup(
                        groupPosition);
*/
                Log.d("Navigation", "selected item_GROUP:::" + groupPosition);

                Intent intent;
                if (groupPosition == 0) {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    return true;
                } else if (groupPosition == 1) {
                    intent = new Intent(DashboardActivity.this, TripsListActivity.class);
                    startActivity(intent);
                    return true;
                } else if (groupPosition == 2) {

                    intent = new Intent(DashboardActivity.this, DriverProfileActivity.class);
                    startActivity(intent);
                    return true;
                } else if (groupPosition == 3) {

                    intent = new Intent(DashboardActivity.this, FaqsActivity.class);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });
        menuListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                NavExpandableListAdapter adapter = (NavExpandableListAdapter) menuListView.getExpandableListAdapter();
                final String selected = (String) adapter.getChild(
                        groupPosition, childPosition);

                Log.d("Navigation", "selected item_CHILD:::" + selected);

                // Switch case to open selected child element activity on child element selection.

                Intent intent;

                if (selected.equalsIgnoreCase(getResources().getString(R.string.call_customer_care))) {
                    //intent = new Intent(DashboardActivity.this, UpdateProfileActivity.class);
                    //  startActivity(intent);
                    String num = Common.getStringPref(mContext, Constants.commandCenterNumber, "");
                    if (num.isEmpty()) {
                        getCommandCenterNumber();
                    } else {
                        makeCallFromMobile(num);
                    }

                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.call_group_admin))) {

                    // from to
                    makeCallViaProxy(Common.getStringPref(mContext, Constants.driverMobileNumber, ""),
                            Common.getStringPref(mContext, Constants.ambHospitalNumber, ""));

                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.choose_language))) {

                    if (bookingDetails != null) {
                        Log.d("saveBookingDetails", "savinggggBookingDetails language");
                        Common.saveBookingDetails(mContext, bookingDetails);
                    }
                    MyApplication.getApplication().languageSwitcher.showChangeLanguageDialog(DashboardActivity.this);
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.clear_cache))) {

                    clearCacheConfirmation();
                }


                return true;
            }

        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle  view item clicks here.
        int id = item.getItemId();

        switch (id) {


            case R.id.nav_dashboard:
                break;

            case R.id.nav_my_trips:

                Intent mytrips = new Intent(this, TripsListActivity.class);
                startActivity(mytrips);
                break;

            case R.id.nav_my_account:
                Intent intentDriverProifle = new Intent(this, DriverProfileActivity.class);
                startActivity(intentDriverProifle);
                break;

            case R.id.nav_support:
                Intent faq = new Intent(this, FaqsActivity.class);
                startActivity(faq);
                break;
            case R.id.nav_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;

            default:
                // fragmentClass = DashboardFragment.class;
                break;
        }
        // Highlight the selected item has been done by NavigationView
        item.setChecked(true);


        return true;
    }

    private void prepareNavigationMenuAndSet(ExpandableListView expandableList) {


        HashMap<NavExpandedMenuModel, ArrayList<String>> listDataChild = new HashMap<>();

        ArrayList<NavExpandedMenuModel> listDataHeader = new ArrayList<NavExpandedMenuModel>();

        // adding headers
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.home), R.drawable.menu_home));

        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.my_trips), R.drawable.ambulance_marker));

        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.my_account), R.drawable.menu_account));

        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.faqs), R.drawable.menu_faqs));

        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.support), R.drawable.menu_support));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.settings), R.drawable.menu_settings));

        // childs

        // support 3
        ArrayList<String> supportHeaderList = new ArrayList<>();
        supportHeaderList.add(getResources().getString(R.string.call_customer_care));
        supportHeaderList.add(getResources().getString(R.string.call_group_admin));

        // settings 4
        ArrayList<String> settingsHeaderList = new ArrayList<>();
        settingsHeaderList.add(getResources().getString(R.string.choose_language));
        settingsHeaderList.add(getResources().getString(R.string.clear_cache));


        listDataChild.put(listDataHeader.get(0), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(1), new ArrayList<String>());// Header, Child data
        listDataChild.put(listDataHeader.get(2), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(3), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(4), supportHeaderList);
        listDataChild.put(listDataHeader.get(5), settingsHeaderList);

        NavExpandableListAdapter mMenuAdapter = new NavExpandableListAdapter(this, listDataHeader, listDataChild, expandableList);

        expandableList.setAdapter(mMenuAdapter);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mUiSettings = mMap.getUiSettings();
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);

        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (currentLocation == null) {
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        moveCameraToCurrentLocation(currentLocation);
        if (mLocationPermissionGranted) {
            mMap.setMyLocationEnabled(true);
            mUiSettings.setMyLocationButtonEnabled(true);
            // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
            //mMap.
        }
        //mMap.setOnMapClickListener(mapClickListener);
        if (bookingDetails != null) {
            if (!isPathDrawedFromAmbulanceToEmergency) {
                drawPathFromAmbulanceToEmergency();
            }
            if (isDrawPathFromEmergencyToHospital && !isPathDrawedFromEmergencyToHospital) {
                drawPathFromEmergencyToHospital();
            }
        }
        //  mMap.setPadding();
        resetMyPositionButton();
       /* LatLng lang = new LatLng(17.4442181, 78.3837159);
        LatLng des = new LatLng(17.4444509, 78.3839143);
        getRoute(lang, des, false);*/

    }

    @Override
    public void onCameraMoveStarted(int reason) {

        Log.d("onCameraMoveStarted", "onCameraMoveStarted = " + reason);
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            //  Toast.makeText(this, "The user gestured on the map.",
            //         Toast.LENGTH_SHORT).show();
            previousZoomLevel = mMap.getCameraPosition().zoom;
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            //  Toast.makeText(this, "The user tapped something on the map.",
            //        Toast.LENGTH_SHORT).show();
            previousZoomLevel = mMap.getCameraPosition().zoom;
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            // Toast.makeText(this, "The app moved the camera.",
            //         Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCameraIdle() {
        Log.d("onCameraIdle", "onCameraIdle = ");
        //  Toast.makeText(this, "The camera has stopped moving.",
        //         Toast.LENGTH_SHORT).show();
        previousZoomLevel = mMap.getCameraPosition().zoom;
    }

    private void resetMyPositionButton() {
        Fragment fragment = (getSupportFragmentManager().findFragmentById(R.id.map));
        ViewGroup v1 = (ViewGroup) fragment.getView();
        ViewGroup v2 = (ViewGroup) v1.getChildAt(0);
        ViewGroup v3 = (ViewGroup) v2.getChildAt(2);
        final View position = (View) v3.getChildAt(0);
/*        int positionWidth = position.getLayoutParams().width;
        int positionHeight = position.getLayoutParams().height;

        Log.d("width height","wid hei"+positionWidth+" "+positionHeight);

        Log.d("width height","total heigh"+llCurrentStatus.getHeight());
        //lay out position button
        RelativeLayout.LayoutParams positionParams = new RelativeLayout.LayoutParams(positionWidth, positionHeight);
        int margin = 40 + positionWidth / 5;
        positionParams.setMargins(0, llCurrentStatus.getHeight(), 0, 0);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        positionParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        position.setLayoutParams(positionParams);*/

        ViewTreeObserver vto = position.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = position.getViewTreeObserver();
                int positionWidth = position.getLayoutParams().width;
                int positionHeight = position.getLayoutParams().height;

                Log.d("width height", "wid hei" + positionWidth + " " + positionHeight);

                Log.d("width height", "total heigh" + llCurrentStatus.getHeight());
                //lay out position button
                RelativeLayout.LayoutParams positionParams = new RelativeLayout.LayoutParams(positionWidth, positionHeight);
                int margin = 40 + positionWidth / 5;
                positionParams.setMargins(0, llCurrentStatus.getHeight(), 0, 0);
                positionParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                positionParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                position.setLayoutParams(positionParams);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    obs.removeGlobalOnLayoutListener(this);
                }
            }

        });
    }

    private void drawPathFromAmbulanceToEmergency() {
        if (mMap == null) {
            return;
        }
        isPathDrawedFromAmbulanceToEmergency = true;
        Log.d("booking loca", "booking loca = " + bookingDetails.getLatitude() + " " + bookingDetails.getLongitude());
        LatLng destinationLatLng = new LatLng(bookingDetails.getLatitude(), bookingDetails.getLongitude());

        // LatLng destinationLatLng = new LatLng(17.451542,  78.381196);
        MarkerOptions options = new MarkerOptions();
        options.position(destinationLatLng);
        Marker destinationMarker = mMap.addMarker(options);

        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (currentLocation == null) {
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        Log.d("currentLocation", "currentLocation = " + currentLocation);
        if (currentLocation != null) {
            MarkerOptions fromOptions = new MarkerOptions();
            LatLng fromLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            fromOptions.position(fromLatLng);

            Bitmap ambulance_marker = BitmapFactory.decodeResource(getResources(), R.drawable.ambulance_marker);
            BitmapDescriptor ambulance = BitmapDescriptorFactory.fromBitmap(ambulance_marker);

            // fromOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            fromOptions.icon(ambulance);
            markerAmbulanceStarted = mMap.addMarker(fromOptions);

            getRoute(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destinationLatLng, false, bookingDetails.getBookingId());

            ArrayList<Marker> markers = new ArrayList<>();
            markers.add(destinationMarker);
            markers.add(markerAmbulanceStarted);
            // updateMapToShowAllMarkers(markers);
        } else {
            isPathDrawedFromAmbulanceToEmergency = false;
        }

    }

    private void drawPathFromEmergencyToHospital() {

        try {


            if (mMap == null) {
                return;
            }
            isPathDrawedFromEmergencyToHospital = true;
            Log.d("booking loca", "booking loca = " + bookingDetails.getHospitalLatitude() + " " + bookingDetails.getHospitalLongitude());
            LatLng destinationLatLng = new LatLng(Double.parseDouble(bookingDetails.getHospitalLatitude()), Double.parseDouble(bookingDetails.getHospitalLongitude()));

            // LatLng destinationLatLng = new LatLng(17.451542,  78.381196);
            MarkerOptions options = new MarkerOptions();
            options.position(destinationLatLng);

            Bitmap ambulance_marker = BitmapFactory.decodeResource(getResources(), R.drawable.hospital);
            BitmapDescriptor hospital = BitmapDescriptorFactory.fromBitmap(ambulance_marker);

            //  options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            options.icon(hospital);
            Marker destinationMarker = mMap.addMarker(options);

            currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (currentLocation == null) {
                currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            Log.d("currentLocation", "currentLocation = " + currentLocation);
            if (currentLocation != null) {
                MarkerOptions fromOptions = new MarkerOptions();
                LatLng fromLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                fromOptions.position(fromLatLng);
                fromOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                //  mMap.addMarker(fromOptions);

                getRoute(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destinationLatLng, true, bookingDetails.getBookingId());

                ArrayList<Marker> markers = new ArrayList<>();
                markers.add(destinationMarker);
                markers.add(markerAmbulanceStarted);
                //  updateMapToShowAllMarkers(markers);
            } else {
                isPathDrawedFromEmergencyToHospital = false;
            }
        } catch (Exception e) {
            isPathDrawedFromEmergencyToHospital = false;
            Log.d("null point", "drawPathFromEmergencyToHospital **** null pointer exception");
            e.printStackTrace();
        }

    }

    public void requestLocationPermission(int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog
                    .newInstance(requestCode, false).show(
                    getSupportFragmentManager(), "dialog");
        } else {
            // Location permission has not been granted yet, request it.
            PermissionUtils.requestPermission(this, requestCode,
                    Manifest.permission.ACCESS_FINE_LOCATION, false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_LOCATION_PERMISSION_REQUEST_CODE) {
            // Enable the My Location button if the permission has been granted.
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                mLocationPermissionGranted = true;
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                moveCameraToCurrentLocation(location);
                if (mMap != null) {
                    mUiSettings.setMyLocationButtonEnabled(true);
                    mMap.setMyLocationEnabled(true);
                }
                // mMyLocationButtonCheckbox.setChecked(true);
                Log.d("schedule", "schedule@@@@@@@@");
            } else {
                mLocationPermissionGranted = false;
            }

        } /*else if (requestCode == LOCATION_LAYER_PERMISSION_REQUEST_CODE) {
            // Enable the My Location layer if the permission has been granted.
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                mMap.setMyLocationEnabled(true);
                mMyLocationLayerCheckbox.setChecked(true);
            } else {
                mLocationPermissionDenied = true;
            }
        }*/
    }

    /**
     * Checks if the map is ready (which depends on whether the Google Play services APK is
     * available. This should be called prior to calling any methods on GoogleMap.
     */
    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "onLocationChanged" + location);
        // Toast.makeText(this, "location changes", Toast.LENGTH_SHORT).show();
        if (isMovingMapFirstTime) {
            moveCameraToCurrentLocation(location);
            isMovingMapFirstTime = false;
        }
        if (isBetterLocation(location, currentLocation)) {
            currentLocation = location;

            moveCameraToCurrentLocation(location);
            if (bookingDetails != null && !isPathDrawedFromAmbulanceToEmergency) {
                drawPathFromAmbulanceToEmergency();
            }
            if (bookingDetails != null) {

                calculateDistance();
            }
        }

      /*  if (isBookingInProgressToDrawPath && isPathDrawed) {

         *//*   if (countToAddMarker == 0) {
                Toast.makeText(this, "add marker", Toast.LENGTH_SHORT).show();
                MarkerOptions options = new MarkerOptions();
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                options.position(new LatLng(location.getLatitude(), location.getLongitude()));
                mMap.addMarker(options);
            } else if (countToAddMarker == 10) {
                countToAddMarker = 0;
            } else {
                countToAddMarker++;
            }*//*
        }*/

        // locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void moveCameraToCurrentLocation(Location location) {
        Log.d("moveCameraToCurrentL", "moveCameraToCurrentLocation");
        currentLocation = location;
        if (mMap == null || location == null) {
            return;
        }
        if (bookingDetails == null) {
            previousZoomLevel = 17.0f;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, previousZoomLevel);
        mMap.animateCamera(cameraUpdate);
    }

    private void getRoute(LatLng fromPosition, LatLng toPosition, final boolean isToHospital, final String bookingId) {

        Log.d("getRoute", "getRoute");
        String base_url = "https://maps.googleapis.com/";
        Log.d("getRoute", "getRoute" + currentLocation);
        if (currentLocation == null) {
            return;
        }

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().build();

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(base_url).
                        client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // Retrofit restAdapter = ApiClient.getClientGoogle();
        Log.d("retrofit", "retro = " + restAdapter.baseUrl());
        System.out.println("retrofit" + "retro = " + restAdapter.baseUrl());
        ApiInterface apiService =
                restAdapter.create(ApiInterface.class);
        System.out.println("retrofit" + "retro = " + apiService);

        Call<DirectionResults> call = apiService.getRoute(fromPosition.latitude + "," + fromPosition.longitude, toPosition.latitude + "," + toPosition.longitude, Constants.API_KEY);
        call.enqueue(new CallbackWithRetry<DirectionResults>(call) {
            @Override
            public void onResponse(Call<DirectionResults> call, Response<DirectionResults> response) {

                if (bookingDetails != null && bookingId.equalsIgnoreCase(bookingDetails.getBookingId()) && MyApplication.getApplication().getBookingInProgress()) {
                    //call.
                    DirectionResults directionResults = response.body();

                    ArrayList<LatLng> routelist = new ArrayList<LatLng>();
                    if (directionResults.getRoutes().size() > 0) {
                        ArrayList<LatLng> decodelist;
                        DirectionResults.Route routeA = directionResults.getRoutes().get(0);
                        Log.i("zacharia", "Legs length : " + routeA.getLegs().size());
                        if (routeA.getLegs().size() > 0) {
                            DirectionResults.Legs legs = routeA.getLegs().get(0);

                            Log.i("zacharia", "distance :" + legs.getDistance().getText());
                            Log.i("zacharia", "duration :" + legs.getDuration().getText());
                        /*Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "Distance = " + legs.getDistance().getText()
                                        + " Duration = " + legs.getDuration().getText(), Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(Color.RED);
                        snackbar.show();*/
                            if (isToHospital) {
                                bookingDetails.setGoogleDistanceFromEmergencyToHospital(legs.getDistance().getText());
                                bookingDetails.setEstimateTimeFromEmergencyToHospital(legs.getDuration().getText());

                                bookingDetails.setGoogleDistanceFromEmergencyToHospitalValue(legs.getDistance().getValue());

                                //bookingDetails.setEstimateTimeFromEmergencyToHospital(legs.getDuration().getText());
                                txtDistanceAndTime.setText(legs.getDistance().getText() + " /" + legs.getDuration().getText() + " away");

                            } else {
                                txtDistanceAndTime.setText(legs.getDistance().getText() + " /" + legs.getDuration().getText() + " away");
                                bookingDetails.setGoogleDistanceFromAmbToEmergency(legs.getDistance().getText());
                                bookingDetails.setEstimateTimeFromAmbulanceToEmergency(legs.getDuration().getText());

                                bookingDetails.setGoogleDistanceFromAmbToEmergencyValue(legs.getDistance().getValue());
                            }
                            List<DirectionResults.Steps> steps = legs.getSteps();
                            Log.i("zacharia", "Steps size :" + steps.size());
                            DirectionResults.Steps step;
                            DirectionResults.Location location;
                            String polyline;
                            for (int i = 0; i < steps.size(); i++) {
                                step = steps.get(i);
                                location = step.getStart_location();
                                routelist.add(new LatLng(location.getLat(), location.getLng()));
                                Log.i("zacharia", "Start Location :" + location.getLat() + ", " + location.getLng());
                                polyline = step.getPolyline().getPoints();
                                decodelist = RouteDecode.decodePoly(polyline);
                                routelist.addAll(decodelist);
                                location = step.getEnd_location();
                                routelist.add(new LatLng(location.getLat(), location.getLng()));
                                Log.i("zacharia", "End Location :" + location.getLat() + ", " + location.getLng());
                            }
                        }
                    }
                    Log.i("zacharia", "routelist size : " + routelist.size());
                    if (routelist.size() > 0) {
                        PolylineOptions rectLine = null;
                        if (isToHospital) {
                            rectLine = new PolylineOptions().width(10).color(
                                    Color.RED);
                        } else {
                            rectLine = new PolylineOptions().width(10).color(
                                    Color.RED);
                        }

                        for (int i = 0; i < routelist.size(); i++) {
                            rectLine.add(routelist.get(i));
                        }
                        // Adding route on the map
                        mMap.addPolyline(rectLine);

                        moveCameraToCurrentLocation(currentLocation);

                        // markerOptions.position(toPosition);
                        //   markerOptions.draggable(true);
                        //   mMap.addMarker(markerOptions);
                    }
                }

            }

            @Override
            public void onFailure(Call<DirectionResults> call, Throwable throwable) {


                Log.d("", "RetroFit2.0 :getRoute: " + "eroorrrrrrrrrrrr");
                Log.e("eroo getRoute", throwable.toString());
                System.out.print("eroooooooorr getRoute " + throwable.toString());
                Log.e(TAG, throwable.getLocalizedMessage());
                if (retryCount++ < TOTAL_RETRIES) {
                    Log.v(TAG, "Retrying... (" + retryCount + " out of " + TOTAL_RETRIES + ")");
                    retry();
                }

            }
        });

    }

    private void setInitialOnlineStatus() {
        MyApplication.getApplication().setBookingInProgress(false);
        txtCurrentStatus.setText(getString(R.string.OnDuty));
        txtDirection.setVisibility(View.GONE);
        btnDutyStatus.setVisibility(View.VISIBLE);
        llLocationDetails.setVisibility(View.GONE);
        llCurrentStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreen));
        imgCallButton.setVisibility(View.GONE);
        imgCancelTrip.setVisibility(View.GONE);
        imgNavigate.setVisibility(View.GONE);
        txtBookingId.setVisibility(View.GONE);
        imgFakeBooking.setVisibility(View.GONE);
    }

    private void setWayToEmergencyLocaiton() {

        txtDirection.setVisibility(View.VISIBLE);
        txtDirection.setText("( " + getString(R.string.way_to_emergency_location) + " )");
        llCurrentStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlue));

        txtCurrentStatus.setText(R.string.onTrip);
        btnDutyStatus.setVisibility(View.GONE);
        llLocationDetails.setVisibility(View.VISIBLE);
        btnReachedToEmergencyLocation.setVisibility(View.VISIBLE);
        btnReachedToHospital.setVisibility(View.GONE);
        btnStartTrip.setVisibility(View.GONE);
        imgCallButton.setVisibility(View.VISIBLE);
        imgCancelTrip.setVisibility(View.GONE);
        imgFakeBooking.setVisibility(View.GONE);

        imgNavigate.setVisibility(View.VISIBLE);
    }

    private void setWayToHospitalLocaiton() {

        txtDirection.setVisibility(View.VISIBLE);
        //   String direction = bookingDetails.getIsDropLocation()?getString(R.string.on_way_to_your_destination):getString(R.string.on_way_to_hospital);
        txtDirection.setText("( " + getString(R.string.on_way_to_your_destination) + " )");
        llCurrentStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlue));

        txtCurrentStatus.setText(R.string.onTrip);
        btnDutyStatus.setVisibility(View.GONE);
        llLocationDetails.setVisibility(View.VISIBLE);
        btnReachedToEmergencyLocation.setVisibility(View.GONE);
        btnStartTrip.setVisibility(View.GONE);
        btnReachedToHospital.setVisibility(View.VISIBLE);

        imgCallButton.setVisibility(View.VISIBLE);

        if(bookingDetails.getIsDropLocation()){
            btnReachedToHospital.setText(getString(R.string.reached_to_location));
        }
    }

    private void setReachedHospital() {

        txtDirection.setText("( " + getString(R.string.reached_to_location) + " )");
    }


    private void setStartTrip() {

        setBookingDetails();
        txtDirection.setVisibility(View.VISIBLE);
        txtDirection.setText("( " + getString(R.string.start_to_pickup_location) + " )");
        txtBookingId.setVisibility(View.VISIBLE);

        txtBookingId.setText(getString(R.string.booking_id) + ": " + bookingDetails.getBookingId());
        llCurrentStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlue));

        txtCurrentStatus.setText(R.string.onTrip);
        btnDutyStatus.setVisibility(View.GONE);
        llLocationDetails.setVisibility(View.VISIBLE);
        btnReachedToEmergencyLocation.setVisibility(View.GONE);
        btnReachedToHospital.setVisibility(View.GONE);
        btnStartTrip.setVisibility(View.VISIBLE);
        imgCallButton.setVisibility(View.VISIBLE);
        imgCancelTrip.setVisibility(View.VISIBLE);
        imgFakeBooking.setVisibility(View.VISIBLE);
    }

    private void setBookingDetails() {

        txtDistanceAndTime.setText(bookingDetails.getDistance() + " Km" + " /" + bookingDetails.getDuration() + " " + getString(R.string.away));
        if (bookingDetails.getCustomerName() == null || bookingDetails.getCustomerName().equalsIgnoreCase("null")
                || bookingDetails.getCustomerName().isEmpty()) {

            txtPatientName.setText(getString(R.string.patient) + ": " + "NA");
        } else {
            txtPatientName.setText(getString(R.string.patient) + ": " + bookingDetails.getCustomerName());
        }

        txtLocation.setText("@ " + bookingDetails.getAddress());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult", "onActivityResult@@@@");
        Log.d("onActivityResult", "onActivityResult@@@@" + (resultCode == RESULT_OK));

        Log.d("onActivityResult", "onActivityResult@@@@" + requestCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == MY_TRIP_COMPLETED_REQUEST_CODE) {
                setInitialOnlineStatus();
            } else if (requestCode == MY_EMERGENCY_LOCATION_REACHED_REQUEST_CODE) {
                setWayToHospitalLocaiton();

            } else if (requestCode == MY_TRIP_ACCEPT_REQUEST_CODE) {
                if (data.getBooleanExtra("status", true)) {
                    setStartTrip();
                } else {
                    setInitialOnlineStatus();
                }
            } else if (requestCode == MY_TRIP_REJECTED) {
                setInitialOnlineStatus();
                if (mMap != null) {
                    mMap.clear();
                }
            }
        }

    }

    @SuppressWarnings({})
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnReachedToHospital:

                // finish();
                bookingDetails.setHospitalReachedTime(Calendar.getInstance().getTimeInMillis());
                bookingDetails.setCurrentBookingStatus(Constants.REACHED_DESTINATION);
                HashMap<String, Object> mapHos = new HashMap<>();
                mapHos.put("booking_status", Constants.REACHED_DESTINATION);
                //  mapHos.put("distance", Long.parseLong(bookingDetails.getGoogleDistanceFromAmbToEmergencyValue()) + Long.parseLong(bookingDetails.getGoogleDistanceFromEmergencyToHospitalValue()));
                mapHos.put("duration", bookingDetails.getHospitalReachedTime() - bookingDetails.getAmbulanceStartTime());
                // starting the trip
                isBookingInProgressToDrawPath = false;
                updateDeviceStatus(true, mapHos, reachedHospital);

                MyApplication.getApplication().trackEvent("Dashboard", Constants.REACHED_DESTINATION, "" + bookingDetails.getBookingId(), false);

                break;
            case R.id.btnReachedToEmergencyLocation:

                btnReachedToEmergencyLocation.setEnabled(false);
                bookingDetails.setEmergencyReachedTime(Calendar.getInstance().getTimeInMillis());
                bookingDetails.setCurrentBookingStatus(Constants.AT_EMERGENCY_LOCATION);
                HashMap<String, Object> mapEmer = new HashMap<>();
                mapEmer.put("booking_status", Constants.AT_EMERGENCY_LOCATION);
                // mapEmer.put("distance", bookingDetails.getHospitalReachedTime() - bookingDetails.getAmbulanceStartTime());
                // starting the trip
                updateDeviceStatus(true, mapEmer, reachedEmergencyLocation);

                MyApplication.getApplication().trackEvent("Dashboard", Constants.AT_EMERGENCY_LOCATION, "" + bookingDetails.getBookingId(), false);
                break;
            case R.id.btnDutyStatus:
                logoutConfirmation();

                break;
            case R.id.imgCallButton:
                selectWhomToCall();
                // showDriverCallPopUp();

                break;
            case R.id.btnStartTrip:
                // stopping 15 min updation as we are sending every minute
                bookingDetails.setAmbulanceStartTime(Calendar.getInstance().getTimeInMillis());
                stopUpdateStatustimertask();
                bookingDetails.setCurrentBookingStatus(Constants.WAY_TO_EMERGENCY_LOCATION);

                HashMap<String, Object> map = new HashMap<>();
                map.put("booking_status", Constants.WAY_TO_EMERGENCY_LOCATION);
                // starting the trip
                isBookingInProgressToDrawPath = true;
                updateDeviceStatus(true, map, startTrip);

                MyApplication.getApplication().trackEvent("Dashboard", Constants.WAY_TO_EMERGENCY_LOCATION, "" + bookingDetails.getBookingId(), false);
                break;

            case R.id.imgCancelTrip:
                cancelBookingConfirmation();
                MyApplication.getApplication().trackEvent("Dashboard", "Cancel Trip", "" + bookingDetails.getBookingId(), false);
                break;
            case R.id.imgFakeBooking:
                fakeBookingConfirmation();
                MyApplication.getApplication().trackEvent("Dashboard", Constants.FAKE_BOOKING, "" + bookingDetails.getBookingId(), false);
                break;

            case R.id.imgNavigate:
                // Uri gmmIntentUri = Uri.parse("google.navigation:q=Taronga+Zoo,+Sydney+Australia");

                // currentBookingStatus = "Way back to Hospital";
                if (bookingDetails.getCurrentBookingStatus().equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)) {

                    Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?daddr=" + bookingDetails.getHospitalLatitude() + "," + bookingDetails.getHospitalLongitude());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);

                    MyApplication.getApplication().trackEvent("Dashboard", "Navigate to Maps", "Hospital Direction");
                } else {
                    Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?daddr=" + bookingDetails.getLatitude() + "," + bookingDetails.getLongitude());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                    MyApplication.getApplication().trackEvent("Dashboard", "Navigate to Maps", "Emergency Direction");
                }

                break;

        }

    }


    private void logoutConfirmation() {
        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
        simpleSingleButtonDialog = Common.simpleTwoButtonAlertDialog(this, getString(R.string.are_you_sure_you_want_to_logout), new MyAlertListener() {
            @Override
            public void buttonClicked(int type) {
                if (type == 0) {
                    MyApplication.getApplication().trackEvent("Dashboard", "Button", "Driver Log out");
                    driverLogOutService();
                } else {

                }
            }
        });
    }

    private void clearCacheConfirmation() {
        if (bookingDetails == null) {
            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
            simpleSingleButtonDialog = Common.simpleTwoButtonAlertDialog(this, getString(R.string.are_you_sure_you_want_to_clear_data), new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {
                    if (type == 0) {
                        MyApplication.getApplication().trackEvent("Dashboard", "Button", "clear cache");
                        clearCache();

                    } else {

                    }
                }
            });
        } else {
            simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(this, getString(R.string.you_can_not_clear_data), new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {

                }
            });
        }
    }

    private void cancelBookingConfirmation() {
        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
        simpleSingleButtonDialog =
                Common.simpleTwoButtonAlertDialog(this, getString(R.string.are_you_sure_you_want_to_cancel_the_booking), new MyAlertListener() {
                    @Override
                    public void buttonClicked(int type) {
                        if (type == 0) {

                   /* HashMap<String, Object> map = new HashMap<>();
                    currentBookingStatus = "Cancelled booking";
                    map.put("booking_status", "Cancelled booking");
                    // starting the trip
                    updateDeviceStatus(true, map, cancelBooking);*/

                            showRejectReasonDialog();

                            //    cancelBooking("");
                            MyApplication.getApplication().trackEvent("Dashboard", "Button", "Cancel Booking ");

                        } else {


                        }
                    }
                });
    }

    private void fakeBookingConfirmation() {
        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
        simpleSingleButtonDialog =
                Common.simpleTwoButtonAlertDialog(this, getString(R.string.are_you_sure_it_is_fake_booking), new MyAlertListener() {
                    @Override
                    public void buttonClicked(int type) {
                        if (type == 0) {

                   /* HashMap<String, Object> map = new HashMap<>();
                    currentBookingStatus = "Cancelled booking";
                    map.put("booking_status", "Cancelled booking");
                    // starting the trip
                    updateDeviceStatus(true, map, cancelBooking);*/
                            fakeBooking();
                            MyApplication.getApplication().trackEvent("Dashboard", " Fake Button", "fake Booking ");

                        } else {


                        }
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Common.showAccRegNotification(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApplication.getApplication().getBookingInProgress() && bookingDetails != null
                ) {
            if (!bookingDetails.getCurrentBookingStatus().equalsIgnoreCase(Constants.BOOKING_ACCEPTED)) {
                startSendLatLangTimer();
            }

            stopForeGroundService();
            requestLocationUpdates();
        }
        enableLocationServices();
        //  Common.increaseGcmHeartBeat(this);
    }

    @Override
    protected void onPause() {
        if (MyApplication.getApplication().getBookingInProgress() && bookingDetails != null) {
            locationManager.removeUpdates(this);
            stopSendLatLangtimertask();
            startForeGroundService();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {

        Log.d("onStop","onStop");
        // Common.cancelAccRegNotification(this);

        Log.d("saveBookingDetails", "bookingDetails onstop "+bookingDetails);
        if (bookingDetails != null) {
            Log.d("saveBookingDetails", "savinggggBookingDetails");
            Common.saveBookingDetails(this, bookingDetails);
        }
        stopEmergencySound();
        stopCancelSound();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        MyApplication.getApplication().deleteObserver(this);
        Common.cancelAccRegNotification(this);
        stopUpdateStatustimertask();
        stopSendLatLangtimertask();
        //  MyApplication.isBookingInProgress = false;
        if (bookingDetails == null) {
            //MyApplication.getApplication().setBookingInProgress(false);
        }
        try {
            unregisterReceiver(netWorkChangeListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    // updating ambulance location to server while not in booking
    public void startUpdateStatusTimer() {
        if (myTimerUpdateLocation != null) {
            myTimerUpdateLocation.cancel();
            myTimerUpdateLocation = null;
        }
        // set a new Timer
        myTimerUpdateLocation = new Timer();
        // initialize the TimerTask's job
        initializeTimerTask();
        // schedule the timer, after the first 5000ms the TimerTask will run
        // every 10000ms
        myTimerUpdateLocation.schedule(timerTaskUpdateLocation, 0, Constants.deviceLocationUpdateInterval); //
    }

    public void stopUpdateStatustimertask() {
        // stop the timer, if it's not already null
        if (myTimerUpdateLocation != null) {
            myTimerUpdateLocation.cancel();
            myTimerUpdateLocation = null;
        }
    }

    @SuppressLint("NewApi")
    public void initializeTimerTask() {
        timerTaskUpdateLocation = new TimerTask() {
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        HashMap<String, Object> map = new HashMap<>();
                        //updateDeviceStatus(false, map, locationUpdate);
                        updateAmbulanceLocation();
                    }
                });
            }
        };
    }

    private void updateDeviceStatus(final boolean isShowProgressDialog, HashMap<String, Object> map, final int type) {

        map.put(Constants.latitude, currentLocation.getLatitude());
        map.put(Constants.longitude, currentLocation.getLongitude());
        map.put(Constants.geoHashCode, Common.getGeoHashCode(currentLocation));
        map.put("ambulanceId", Common.getAmbulanceId(mContext));
        map.put(Constants.driverId, Common.getDriverId(mContext));
        map.put(Constants.ambulanceApp, true);
        if (bookingDetails != null) {
            map.put("booking_id", bookingDetails.getBookingId());
            map.put("booking_status", bookingDetails.getCurrentBookingStatus());

            if (bookingDetails.getCurrentBookingStatus().equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)) {

                map.put("distanceFromEmerToHospital", bookingDetails.getCalculatedDistanceFromEmergencyToHospitalValue());

            } else {
                map.put("distanceFromAmbToEmr", bookingDetails.getCalculatedDistanceFromAmbToEmergencyValue());

            }

        }

        if (bookingDetails != null && type != -1) {
            map.put("is_booked", true);
            map.put("booking_id", bookingDetails.getBookingId());

            map.put("end_latitude", bookingDetails.getHospitalLatitude());
            map.put("end_longitude", bookingDetails.getHospitalLongitude());
            map.put("end_geoHashCode", bookingDetails.getHospitalGeoHashCode());

            // emergency Location
            map.put("emergency_latitude", bookingDetails.getLatitude());
            map.put("emergency_longitude", bookingDetails.getLongitude());


        }

        Log.d("req", "req = " + Arrays.asList(map));
        if (type != -1) {
            isSendingStatusUpdate = true;
        }
        if (Common.isNetworkAvailable(mContext)) {

            if (isShowProgressDialog) {
                Common.displayProgress(mContext);
            }
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<JsonElement> call = apiService.updateAmbulanceStatus(map, Common.getUserId(mContext), Common.getAccessToken(mContext));

            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    if (isShowProgressDialog) {
                        Common.stopProgressDialog();
                    }

                    try {
                        Log.d("response", "response = 222 updateDeviceStatus" + serverResponse.body());
                        System.out.print("response = 22222 updateDeviceStatus" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());

                        isSendingStatusUpdate = false;

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            if (type == startTrip) {
                                // initial start step
                                setWayToEmergencyLocaiton();
                                startSendLatLangTimer();
                                sendStatusPns("Started");


                            } else if (type == reachedEmergencyLocation) {

                                btnReachedToEmergencyLocation.setEnabled(true);
                                Log.d("reachedEmergencyLocat", "reachedEmergencyLocation");
                                sendStatusPns(Constants.AT_EMERGENCY_LOCATION);
                                //reached to emergency
                                //Intent intent2 = new Intent(DashboardActivity.this, AtEmergencyLocationActivity.class);
                                // intent2.putExtra("bookingDetails", bookingDetails);
                                //  DashboardActivity.this.startActivityForResult(intent2, MY_EMERGENCY_LOCATION_REACHED_REQUEST_CODE);
                               /* if (!bookingDetails.getHospital().equalsIgnoreCase("any nearby hospital")) {
                                    showAtEmergencyLocationDialog(false);
                                } else {

                                    getNearByHospitals();
                                }*/

                                getPatientDetails();
                            } else if (type == reachedHospital) {
                                stopForeGroundService();
                                sendStatusPns(Constants.REACHED_DESTINATION);
                                // Intent intent = new Intent(DashboardActivity.this, TripDetailsActivity.class);
                                // DashboardActivity.this.startActivityForResult(intent, MY_TRIP_COMPLETED_REQUEST_CODE);
                                showTripCompletedDialog();
                                stopSendLatLangtimertask();
                                // startActivity(intent);
                            } else if (type == cancelBooking) {
                                Intent intent = new Intent(DashboardActivity.this, TripRejectionActivity.class);
                                startActivityForResult(intent, MY_TRIP_REJECTED);
                            } else if (type == wayBackToHospital) {

                                // clearing the nearbyhosptials
                                nearByHospitals.clear();

                                setWayToHospitalLocaiton();
                                sendStatusPns(Constants.WAY_BACK_TO_DESTINATION);
                            } else if (type == tripCompleted) {


                                sendStatusPns(Constants.TRIP_CLOSED);
                               /* MyApplication.isBookingInProgress = false;
                                isPathDrawed = false;
                                bookingDetails = null;
                                setInitialOnlineStatus();
                                if (mMap != null) {
                                    mMap.clear();
                                }
                                 startUpdateStatusTimer();*/
                                clearAllandSetInitialStates();

                            }
                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(mContext);

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseBookingCancelled)) {

                            clearAllandSetInitialStates();
                            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                            simpleSingleButtonDialog = Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        } else {
                            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                            simpleSingleButtonDialog = Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        if (type == reachedEmergencyLocation) {
                            btnReachedToEmergencyLocation.setEnabled(true);
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    if (type == reachedEmergencyLocation) {
                        btnReachedToEmergencyLocation.setEnabled(true);
                        bookingDetails.setCurrentBookingStatus(Constants.WAY_TO_EMERGENCY_LOCATION);
                    }
                    if (isShowProgressDialog) {
                        Common.stopProgressDialog();
                    }
                    Log.d("", "RetroFit2.0 :updateAmbulanceStatus: " + "eroorrrrrrrrrrrr");
                    Log.e("er teAmbulanceStatus", throwable.toString());
                    MyApplication.getApplication().trackEvent("Dashboard", getString(R.string.networkError), "update ambualnce status", false);
                    // Common.displayAlertDialog(mContext, mContext.getString(R.string.networkError));
                }
            });
        } else {

            if (type == reachedEmergencyLocation) {
                btnReachedToEmergencyLocation.setEnabled(true);
                bookingDetails.setCurrentBookingStatus(Constants.WAY_TO_EMERGENCY_LOCATION);
            }
            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
            simpleSingleButtonDialog = Common.displayAlertDialog(mContext, mContext.getString(R.string.offline));
        }
    }

    private void driverLogOutService() {

        if (Common.isNetworkAvailable(this)) {


            HashMap<String, Object> map = new HashMap<>();
            if (currentLocation == null) {
                map.put(Constants.latitude, 00000000);
                map.put(Constants.longitude, 00000000);
            } else {
                map.put(Constants.latitude, currentLocation.getLatitude());
                map.put(Constants.longitude, currentLocation.getLongitude());
            }

            map.put("ambulanceId", Common.getAmbulanceId(this));
            map.put("driverId", Common.getDriverId(this));

            Common.displayProgress(mContext);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<JsonElement> call = apiService.driverLogOut(map, Common.getUserId(mContext), Common.getAccessToken(mContext));
            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            Common.saveBooleanPref(mContext, Constants.isDriverLoggedIn, false);
                            Common.cancelAccRegNotification(mContext);

                            // cancelling the heartbeat
                            HeartbeatReceiver.cancelHeartbeatRequest(DashboardActivity.this);

                            MyApplication.getApplication().setBookingInProgress(false);
                            Intent intent1 = new Intent(mContext, DriverListActivity.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            finish();
                            startActivity(intent1);


                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(mContext);

                        } else {
                            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                            simpleSingleButtonDialog =
                                    Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :driverLogOut: " + "eroorrrrrrrrrrrr");
                    Log.e("ero driverLogOut", throwable.toString());
                    System.out.print("erro = driverLogOut" + throwable.toString());
                    Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                    simpleSingleButtonDialog =
                            Common.displayAlertDialog(mContext, getString(R.string.networkError));
                    MyApplication.getApplication().trackEvent("Dashboard", getString(R.string.networkError), "Log out ambualnce", false);
                }
            });
        } else {
            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
            simpleSingleButtonDialog =
                    Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Log.d("onNewIntent", "onNewIntent" + intent.getBooleanExtra("isFromService", false));
        if (!intent.hasExtra("isFromService") && intent.hasExtra(Constants.bookingDetails)) {

            Log.d("bookingDetails", "bookingDetails ::::");

            bookingDetails = (BookingDetails) intent.getSerializableExtra("bookingDetails");

            if (!intent.hasExtra("typeFromNewTripScreen")) {
                if (mMap != null && !isPathDrawedFromAmbulanceToEmergency) {

                    drawPathFromAmbulanceToEmergency();
                }

                setStartTrip();
            } else {
                update(getIntent().getIntExtra("typeFromNewTripScreen", -1), bookingDetails);
            }

        }
    }

    @Override
    public void update(final int event, final Object arg) {
        // booking
        Log.d("update", "main dashboard update started");
        // Log.d("booking","booking started");
        if (MyApplication.NEW_BOOKING == event) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    MyApplication.getApplication().setBookingInProgress(true);

                    MyApplication.getApplication().deleteObserver(DashboardActivity.this);
                    BookingDetails details = (BookingDetails) arg;
                    Intent intent = new Intent(DashboardActivity.this, NewTripRequestActivity.class);
                    intent.putExtra(Constants.bookingDetails, details);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                    finish();
                }
            });

        } else if (event == MyApplication.BOOKING_CANCELLED || event == MyApplication.BOOKING_CANCELLED_COMMAND_CENTER) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   /* startUpdateStatusTimer();
                    stopSendLatLangtimertask();
                    MyApplication.isBookingInProgress = false;
                    isBookingInProgressToDrawPath = false;
                    isPathDrawed = false;
                    bookingDetails = null;
                    if(dialogAtEmergency != null && dialogAtEmergency.isShowing()){
                        dialogAtEmergency.dismiss();
                    }
                    setInitialOnlineStatus();
                    if (mMap != null) {
                        mMap.clear();
                    }*/
                    stopEmergencySound();
                    if (bookingDetails != null) {
                        closeAllPopups();
                        clearAllandSetInitialStates();
                        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                        if (event == MyApplication.BOOKING_CANCELLED) {
                            //  simpleSingleButtonDialog =
                            //  Common.displayAlertDialog(mContext, getString(R.string.customer_has_cancelled_your_booking));
                            showBookingCancelledOrTransferredDialog(MyFirebaseMessagingService.cancelBooking);
                        } else if (event == MyApplication.BOOKING_CANCELLED_COMMAND_CENTER) {

                            showBookingCancelledOrTransferredDialog(MyFirebaseMessagingService.cancelBookingCommandCenter);
                            // simpleSingleButtonDialog =
                            //        Common.displayAlertDialog(mContext, getString(R.string.command_has_cancelled_your_booking));
                        }
                    }
                }
            });

        } else if (event == MyApplication.BOOKING_TRANSFERRED) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    closeAllPopups();
                    stopEmergencySound();
                    clearAllandSetInitialStates();
                    //  Common.displayAlertDialog(mContext, getString(R.string.transferred_your_booking));

                    Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                    showBookingCancelledOrTransferredDialog(MyFirebaseMessagingService.transferBooking);

                  /*  simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, getString(R.string.transferred_your_booking), new MyAlertListener() {
                        @Override
                        public void buttonClicked(int type) {

                        }
                    });*/
                }
            });
        } else if (event == MyApplication.TRANSFERRED_NEW_BOOKING || event == MyApplication.COMMAND_CENTER_NEW_BOOKING) {

            Log.d("TRANSFERRED_NEW_BOOKING", "TRANSFERRED_NEW_BOOKING COMMAND_CENTER_NEW_BOOKING" + event);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    if (arg != null) {


                        MyApplication.getApplication().setBookingInProgress(true);
                        bookingDetails = (BookingDetails) arg;
                        if (mMap != null && !isPathDrawedFromAmbulanceToEmergency) {

                            drawPathFromAmbulanceToEmergency();
                        }
                        bookingDetails.setCommandCenter(Common.getStringPref(mContext, Constants.commandCenterNumber, ""));
                        setStartTrip();
                        imgCancelTrip.setVisibility(View.GONE);
                        imgFakeBooking.setVisibility(View.GONE);
                        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                        playEmergencySound();
                        simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, getString(R.string.new_booking_from_center), new MyAlertListener() {
                            @Override
                            public void buttonClicked(int type) {

                                stopEmergencySound();
                            }
                        });
                    }
                }
            });
        } else if (event == MyApplication.AMBULANCE_PROFILE_UPDATE) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    LinearLayout header_main = (LinearLayout) navigationView.findViewById(R.id.nav_header_dashboard);
                    TextView txtAmbulanceNumber = (TextView) header_main.findViewById(R.id.txtAmbulanceNumber);

                    txtAmbulanceNumber.setText(Common.getAmbulanceNumber(mContext) + "\n" + Common.getStringPref(mContext, Constants.ambGroupNmae, ""));

                }
            });
        }

    }


    public void startSendLatLangTimer() {
        if (timerSendLatLantToCustomer != null) {
            timerSendLatLantToCustomer.cancel();
            timerSendLatLantToCustomer = null;
        }
        // set a new Timer
        timerSendLatLantToCustomer = new Timer();
        // initialize the TimerTask's job
        initializeSendLatLangTimerTask();
        // schedule the timer, after the first 5000ms the TimerTask will run
        // every 10000ms
        timerSendLatLantToCustomer.schedule(timerTaskSendLatLantToCustomer, 0, Constants.deviceLocationUpdateIntervalWhileBookingInProgress); //
    }

    public void stopSendLatLangtimertask() {
        // stop the timer, if it's not already null
        if (timerSendLatLantToCustomer != null) {
            timerSendLatLantToCustomer.cancel();
            timerSendLatLantToCustomer = null;
        }
    }

    @SuppressLint("NewApi")
    public void initializeSendLatLangTimerTask() {
        timerTaskSendLatLantToCustomer = new TimerTask() {
            public void run() {


                Log.d("currentLocation", "currentLocation in lat lang timer" + currentLocation);
                if (currentLocation == null || bookingDetails == null) {
                    return;
                }
                final HashMap<String, Object> mGcmData = new HashMap<>();
                HashMap<String, Object> mData = new HashMap<>();
                //updateDeviceStatus(false, map, locationUpdate);
                mGcmData.put("to", bookingDetails.getGcmToken());
                mData.put(Constants.latitude, currentLocation.getLatitude());
                mData.put(Constants.longitude, currentLocation.getLongitude());
                mData.put("type", "ambulanceUpdate");

                if (bookingDetails != null) {
                    mData.put("booking_id", bookingDetails.getBookingId());
                }


                mGcmData.put("data", mData);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SendPushNotification sendPushNotification = new SendPushNotification(mContext);

                        sendPushNotification.sendPn(mGcmData);
                    }
                });

                if (!isSendingStatusUpdate) {
                    final HashMap<String, Object> hashMapServerUpdate = new HashMap<>();
                    hashMapServerUpdate.put("end_latitude", bookingDetails.getHospitalLatitude());
                    hashMapServerUpdate.put("end_longitude", bookingDetails.getHospitalLongitude());
                    hashMapServerUpdate.put("is_location_update", true);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateDeviceStatus(false, hashMapServerUpdate, -1);
                        }
                    });
                }


            }


        };
    }

    private void sendStatusPns(String type) {

        if (bookingDetails == null) {
            return;
        }

        try {
            HashMap<String, Object> mGcmData = new HashMap<>();
            HashMap<String, Object> mData = new HashMap<>();
            //updateDeviceStatus(false, map, locationUpdate);
            mGcmData.put("to", bookingDetails.getGcmToken());
            mData.put("booking_status", type);
            mData.put("type", "bookingStatusUpdate");
            mData.put("booking_id", bookingDetails.getBookingId());

            Log.d("currentLocation", "currentLocation" + currentLocation);
            Log.d("currentLocation", "speedddddddd" + currentLocation.getSpeed());
            mData.put("speed", "speed" + currentLocation.getSpeed());
            if (currentLocation != null && currentLocation.getSpeed() != 0 &&
                    type.equalsIgnoreCase(Constants.TRIP_CLOSED) && type.equalsIgnoreCase(Constants.REACHED_DESTINATION)) {

                if (type.equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)) {

                    Location hospital = new Location("");
                    hospital.setLatitude(Double.parseDouble(bookingDetails.getHospitalLatitude()));
                    hospital.setLongitude(Double.parseDouble(bookingDetails.getHospitalLongitude()));
                    float distancInMeters = currentLocation.distanceTo(hospital);
                    float speedInMeterPerSecond = (currentLocation.getSpeed()) * 60;
                    float estimatedDriveTimeInMinutes = distancInMeters / speedInMeterPerSecond;
                    mData.put("estimateTime", estimatedDriveTimeInMinutes);
                    mData.put("distance", distancInMeters);

                    Log.d("distancInMeters", "distancInMeters" + distancInMeters);
                    Log.d("speedInMeterPerSecond", "speedInMeterPerSecond" + speedInMeterPerSecond);
                    Log.d("estimatedDriveTimeIn", "estimatedDriveTimeInMinutes" + estimatedDriveTimeInMinutes);

                } else {

                    Location emergency = new Location("");
                    emergency.setLatitude((bookingDetails.getLatitude()));
                    emergency.setLongitude((bookingDetails.getLongitude()));
                    float distancInMeters = currentLocation.distanceTo(emergency);
                    float speedInMeterPerSecond = (currentLocation.getSpeed()) * 60;
                    float estimatedDriveTimeInMinutes = distancInMeters / speedInMeterPerSecond;
                    mData.put("estimateTime", estimatedDriveTimeInMinutes);
                    mData.put("distance", distancInMeters);

                    Log.d("distancInMeters*1", "distancInMeters" + distancInMeters);
                    Log.d("speedInMeterPerSecond*1", "speedInMeterPerSecond" + speedInMeterPerSecond);
                    Log.d("estimatedDriveTimeIn*1", "estimatedDriveTimeInMinutes" + estimatedDriveTimeInMinutes);
                }
            }

            if (type.equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)) {
                mData.put("hospitalLat", bookingDetails.getHospitalLatitude());
                mData.put("hospitalLong", bookingDetails.getHospitalLongitude());
            } else if (type.equalsIgnoreCase(Constants.TRIP_CLOSED)) {


                mData.put("duration", bookingDetails.getHospitalReachedTime() - bookingDetails.getAmbulanceStartTime());
                //   mData.put("totalDistance", Long.parseLong(bookingDetails.getGoogleDistanceFromAmbToEmergencyValue()) + Long.parseLong(bookingDetails.getGoogleDistanceFromEmergencyToHospitalValue()));
                double distance = bookingDetails.getTotalCalculatedDistance();

                distance = distance * 1000;
                distance = Math.round(distance);
                distance = distance / 1000;
                mData.put("totalDistance", distance);
                mData.put("speed", bookingDetails.getSpeed() + " Kmph");
                mData.put("cost", bookingDetails.getTripCost());
            }
            mGcmData.put("data", mData);

            if(bookingDetails.getDeviceType().equalsIgnoreCase("ios")){
                mGcmData.put("content_available", true);
                HashMap<String, Object> notification = new HashMap();
                notification.put("body", getFullOfShortCode(type));
                mGcmData.put("notification", notification);
            }

            SendPushNotification sendPushNotification = new SendPushNotification(mContext);
            sendPushNotification.sendPn(mGcmData);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void showRejectReasonDialog() {
        if (dialogRejectionReason != null && dialogRejectionReason.isShowing()) {
            dialogRejectionReason.dismiss();
        }

        dialogRejectionReason = new Dialog(mContext);
        dialogRejectionReason.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater factory = LayoutInflater.from(this);
        final View rootView = factory.inflate(R.layout.dialog_trip_rejection,
                null);

        dialogRejectionReason.setCanceledOnTouchOutside(false);
        dialogRejectionReason.setCancelable(false);

        dialogRejectionReason.setContentView(rootView);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.reclyerView);
        Button btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);

        final EditText etReason = (EditText) rootView.findViewById(R.id.etReason);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        final String[] tripRejectList = getResources().getStringArray(R.array.tripRejectionType);
        final TripRejectionReasonAdapter tripRejectionReasonAdapter = new TripRejectionReasonAdapter(this, tripRejectList, new OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (position != tripRejectList.length - 1) {
                    etReason.setVisibility(View.GONE);
                } else {
                    etReason.setVisibility(View.VISIBLE);
                }
            }
        });
        recyclerView.setAdapter(tripRejectionReasonAdapter);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tripRejectionReasonAdapter.mCheckedPostion != -1) {

                    if (tripRejectionReasonAdapter.mCheckedPostion == tripRejectList.length - 1 && etReason.getText().toString().isEmpty()) {

                        Toast.makeText(mContext, getString(R.string.EnterReasonToReject), Toast.LENGTH_SHORT).show();
                    } else {
                        dialogRejectionReason.dismiss();
                        if (tripRejectionReasonAdapter.mCheckedPostion == tripRejectList.length - 1) {

                            cancelBooking(etReason.getText().toString());
                        } else {
                            cancelBooking(tripRejectList[tripRejectionReasonAdapter.mCheckedPostion]);
                        }

                    }


                } else {
                    Toast.makeText(mContext, getString(R.string.selectReasonToReject), Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogRejectionReason.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //  WindowManager.LayoutParams lp = campaignAlertDialog.getWindow().getAttributes();
        //lp.dimAmount=1.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        // lp.gravity = (Gravity.CENTER);
        // campaignAlertDialog.getWindow().setAttributes(lp);
        Window window = dialogRejectionReason.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        dialogRejectionReason.show();
    }

    private void showAtEmergencyLocationDialog(final boolean isShowHospital) {

        Log.d("showAtEmergency", "showAtEmergencyLocationDialog");

        if (dialogAtEmergency != null && dialogAtEmergency.isShowing()) {
            dialogAtEmergency.dismiss();
        }
        btnReachedToEmergencyLocation.setEnabled(false);
        dialogAtEmergency = new Dialog(mContext);
        dialogAtEmergency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater factory = LayoutInflater.from(this);
        final View rootView = factory.inflate(R.layout.dialog_layout_at_emergency_location,
                null);

        dialogAtEmergency.setCanceledOnTouchOutside(false);
        dialogAtEmergency.setCancelable(false);

        dialogAtEmergency.setContentView(rootView);
        final EditText etPatientName = (EditText) rootView.findViewById(R.id.etPatientName);

        //final EditText etPatientGender = (EditText) rootView.findViewById(R.id.etPatientGender);
        final EditText etPatientAge = (EditText) rootView.findViewById(R.id.etPatientAge);
        final EditText etPatientCondition = (EditText) rootView.findViewById(R.id.etPatientCondition);
        final EditText etContactNumber = (EditText) rootView.findViewById(R.id.etContactNumber);
        EditText etHospital = (EditText) rootView.findViewById(R.id.etHospital);
        TextInputLayout txtInputHospital = (TextInputLayout) rootView.findViewById(R.id.txtInputHospital);

        Button btnOnWayToHospitalDialog = (Button) rootView.findViewById(R.id.btnOnWayToHospital);
        final Spinner spinnerEmergencyType = (Spinner) rootView.findViewById(R.id.spinnerEmergencyType);
        final Spinner spinnerHospitalList = (Spinner) rootView.findViewById(R.id.spinnerHospitalList);
        final Spinner spinnerGender = (Spinner) rootView.findViewById(R.id.spinnerGender);


        if (isShowHospital) {
            spinnerHospitalList.setVisibility(View.VISIBLE);
            etHospital.setVisibility(View.GONE);
        } else {
            if(bookingDetails.getIsDropLocation()){
                etHospital.setText(bookingDetails.getHospital());
                etHospital.setHint(getString(R.string.location));
                txtInputHospital.setHint(getString(R.string.location));
            }
            etHospital.setVisibility(View.VISIBLE);
            spinnerHospitalList.setVisibility(View.GONE);
        }

        final ArrayList<EmergencyType> emergencyTypeArrayList = dbHelper.getEmergencyTypes();

        Log.d("size **", "size **" + emergencyTypeArrayList.size());

        String[] emergencyArray = new String[emergencyTypeArrayList.size()];


        int spinnerEmergencyMatched = 0;

        for (int i = 0; i < emergencyTypeArrayList.size(); i++) {
            emergencyArray[i] = emergencyTypeArrayList.get(i).getTypeName();
            if (emergencyArray[i].equalsIgnoreCase(bookingDetails.getEmergencyType())) {
                spinnerEmergencyMatched = i;
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(mContext, R.layout.my_spinner_layout, emergencyArray);
      /*  ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
                .createFromResource(this, emergencyArray,
                        R.layout.my_spinner_layout);*/
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmergencyType.setAdapter(spinnerArrayAdapter);

        final ArrayAdapter<CharSequence> genderAdapter = ArrayAdapter
                .createFromResource(this, R.array.gender,
                        R.layout.my_spinner_layout);
        // Specify the layout to use when the list of choices appears
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(genderAdapter);

        btnOnWayToHospitalDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent();
                intent.putExtra("MESSAGE", "");
                setResult(RESULT_OK, intent);
                finish();*/
              /*  if (etPatientName.getText().toString().isEmpty()) {
                    etPatientName.setError("Please enter name");
                    requestFocus(etPatientName);
                    return;
                }
                if (etContactNumber.getText().toString().isEmpty() || etContactNumber.getText().toString().length() != 10) {
                    etContactNumber.setError("Please enter valid number");
                    requestFocus(etContactNumber);
                    return;
                }

                if (etPatientAge.getText().toString().isEmpty()) {
                    etPatientAge.setError("Please enter valid age");
                    requestFocus(etPatientAge);
                    return;
                }*/
                dialogAtEmergency.dismiss();
                bookingDetails.setCurrentBookingStatus(Constants.WAY_BACK_TO_DESTINATION);
                HashMap<String, Object> map = new HashMap<>();
                map.put("booking_status", Constants.WAY_BACK_TO_DESTINATION);
                map.put("emergency_type_id", emergencyTypeArrayList.get(spinnerEmergencyType.getSelectedItemPosition()).getId());
                map.put("emergency_type", emergencyTypeArrayList.get(spinnerEmergencyType.getSelectedItemPosition()).getTypeName());

                map.put("patient_name", etPatientName.getText().toString().isEmpty() ? " " : etPatientName.getText().toString());
                map.put("age", etPatientAge.getText().toString().isEmpty() ? 0 : Integer.parseInt(etPatientAge.getText().toString()));
                map.put("patient_condition", etPatientCondition.getText().toString().isEmpty() ? " " : etPatientCondition.getText().toString());
                // adding 91 to the number
                map.put("contact_number", etContactNumber.getText().toString().isEmpty() ? "" : "91"+etContactNumber.getText().toString());
                map.put("gender", "" + genderAdapter.getItem(spinnerGender.getSelectedItemPosition()));

                String name = etPatientName.getText().toString();
                if (name == null || name.equalsIgnoreCase("null")
                        || name.isEmpty()) {

                    txtPatientName.setText(getString(R.string.patient)+": " + "NA");
                } else {
                    //txtPatientName.setText("Patient: " + bookingDetails.getCustomerName());
                    txtPatientName.setText(getString(R.string.patient)+": " + name);
                }
                if (isShowHospital) {

                    HospitalList.HospitalDetails hospital = nearByHospitals.get(spinnerHospitalList.getSelectedItemPosition());
                    bookingDetails.setHospitalLatitude(hospital.getGroup_latitude());
                    bookingDetails.setHospitalLongitude(hospital.getGroup_longitude());
                    bookingDetails.setHospital(hospital.getGroup_name());
                    bookingDetails.setHospitalNumber(hospital.getEmergency_number());

                    map.put("hospital", bookingDetails.getHospital());
                    map.put("group_id", hospital.getGroup_id());
                }

                txtLocation.setText(bookingDetails.getHospital());

                // starting the trip
                isDrawPathFromEmergencyToHospital = true;
                drawPathFromEmergencyToHospital();
                updateDeviceStatus(true, map, wayBackToHospital);


            }
        });

        spinnerEmergencyType.setSelection(spinnerEmergencyMatched);
        etHospital.setText(bookingDetails.getHospital());

        if(!bookingDetails.getPatientName().isEmpty()) {
            etPatientName.setText(bookingDetails.getPatientName());
        }

        if(!bookingDetails.getPatientContactNumber().isEmpty()) {
            etContactNumber.setText(removeIndianCountryCode(bookingDetails.getPatientContactNumber()));
        }

        if(!bookingDetails.getPatientAge().isEmpty()){
            etPatientAge.setText(bookingDetails.getPatientAge());
        }

        if(!bookingDetails.getPatientCondition().isEmpty()){
            etPatientCondition.setText(bookingDetails.getPatientCondition());
        }


        if(bookingDetails.getIsDropLocation()){
            btnOnWayToHospitalDialog.setText(getString(R.string.on_way_to_your_destination));
        }
        // etPatientGender.setText();
        if (bookingDetails.getPatientGender() == null || bookingDetails.getPatientGender().isEmpty() ||bookingDetails.getPatientGender().equalsIgnoreCase("male")) {
            spinnerGender.setSelection(0);
        } else {
            spinnerGender.setSelection(1);
        }


        if (isShowHospital) {

            String[] nearByHospitalsArray = new String[nearByHospitals.size()];
            for (int i = 0; i < nearByHospitals.size(); i++) {
                HospitalList.HospitalDetails hospital = nearByHospitals.get(i);
                nearByHospitalsArray[i] = hospital.getGroup_short_name() + " (" + hospital.getDistance() + " )" ;

            }
            ArrayAdapter<String> hospitalArrayAdapter = new ArrayAdapter<>(mContext, R.layout.my_spinner_layout, nearByHospitalsArray);
      /*  ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
                .createFromResource(this, emergencyArray,
                        R.layout.my_spinner_layout);*/
            hospitalArrayAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerHospitalList.setAdapter(hospitalArrayAdapter);

        }
        //etPatientGender.setText(bookingDetails.getg);


        dialogAtEmergency.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //  WindowManager.LayoutParams lp = campaignAlertDialog.getWindow().getAttributes();
        //lp.dimAmount=1.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        // lp.gravity = (Gravity.CENTER);
        // campaignAlertDialog.getWindow().setAttributes(lp);
        Window window = dialogAtEmergency.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        dialogAtEmergency.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Log.d("isShowHospital", "isShowHospital =" + isShowHospital);
                if (isShowHospital) {
                    spinnerHospitalList.performClick();
                }
            }
        });
        dialogAtEmergency.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                View view = DashboardActivity.this.getCurrentFocus();
                if (view != null) {
                    Common.closeKeyBoard(mContext, view);
                }

            }
        });
        dialogAtEmergency.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                View view = DashboardActivity.this.getCurrentFocus();
                if (view != null) {
                    Common.closeKeyBoard(mContext, view);
                }
            }
        });
        dialogAtEmergency.show();
        btnReachedToEmergencyLocation.setEnabled(true);

    }

    private void showTripCompletedDialog() {

        Log.d("showAtEmergency", "showAtEmergencyLocationDialog");

        if (dialogTripCompleted != null && dialogTripCompleted.isShowing()) {
            dialogTripCompleted.dismiss();
        }

        Button btnTripCompleted;
        TextView txtTotalDistance, txtTripTime, txtAvgSpeed, txtTripCost;
        final EditText etTripCost;

        dialogTripCompleted = new Dialog(mContext);
        dialogTripCompleted.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater factory = LayoutInflater.from(this);
        final View rootView = factory.inflate(R.layout.layout_dialog_trip_completed,
                null);


        dialogTripCompleted.setCancelable(false);
        dialogTripCompleted.setCanceledOnTouchOutside(false);

        dialogTripCompleted.setContentView(rootView);


        txtTotalDistance = (TextView) rootView.findViewById(R.id.text_total_distance);
        txtTripTime = (TextView) rootView.findViewById(R.id.text_trip_time);
        txtAvgSpeed = (TextView) rootView.findViewById(R.id.text_avg_speed);
        //txtTripCost = (TextView) rootView.findViewById(R.id.text_trip_cost);
        etTripCost = (EditText) rootView.findViewById(R.id.etTripCost);

        long totalTime = 0;
        //final long distance = Long.parseLong(bookingDetails.getGoogleDistanceFromAmbToEmergencyValue()) + Long.parseLong(bookingDetails.getGoogleDistanceFromEmergencyToHospitalValue());

        double distance = 0;
        if(bookingDetails.isMedicalTaxi()){

            distance =
                    bookingDetails.getCalculatedDistanceFromEmergencyToHospitalValue();
            totalTime =  bookingDetails.getHospitalReachedTime() - bookingDetails.getEmergencyReachedTime();
        }
        else{
            distance = bookingDetails.getCalculatedDistanceFromAmbToEmergencyValue()
                    + bookingDetails.getCalculatedDistanceFromEmergencyToHospitalValue();

            totalTime =  bookingDetails.getHospitalReachedTime() - bookingDetails.getAmbulanceStartTime();
        }


        bookingDetails.setTotalCalculatedDistance(distance);
        final double distanceInner = distance;
        final long timeInner = totalTime;

        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalTime),
                TimeUnit.MILLISECONDS.toMinutes(totalTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalTime)),
                TimeUnit.MILLISECONDS.toSeconds(totalTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime)));

        txtTripTime.setText("" + hms);
        bookingDetails.setSpeed(calculateSpeed(totalTime, Double.valueOf(distance).longValue()));

        txtAvgSpeed.setText(bookingDetails.getSpeed() + " Kmph");

        //  txtTotalDistance.setText(bookingDetails.getGoogleDistanceFromAmbToEmergency() + " " + bookingDetails.getGoogleDistanceFromEmergencyToHospital());

        Log.d("distance", "distance = " + distance);
        distance = (distance / 1000d);
        distance = distance * 1000;
        distance = Math.round(distance);
        distance = distance / 1000;
        txtTotalDistance.setText(distance + " Km");

        btnTripCompleted = (Button) rootView.findViewById(R.id.btnTripCompleted);

        btnTripCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etTripCost.getText().toString().isEmpty() || etTripCost.getText().toString().length() > 5) {
                    etTripCost.setError("Please enter the valid cost");
                    //requestFocus(etTripCost);
                    return;
                }
                bookingDetails.setCurrentBookingStatus(Constants.TRIP_CLOSED);
                HashMap<String, Object> map = new HashMap<>();
                map.put("booking_status", Constants.TRIP_CLOSED);
                map.put("emergencyReachedTime", bookingDetails.getEmergencyReachedTime());
                map.put("hospitalReachedTime", bookingDetails.getHospitalReachedTime());
                map.put("googleDistanceFromAmbToEmergency", bookingDetails.getGoogleDistanceFromAmbToEmergency());
                map.put("googleDistanceFromEmergencyToHospital", bookingDetails.getGoogleDistanceFromEmergencyToHospital());
                map.put("duration", timeInner);
                map.put("distance", distanceInner);
                map.put("speed", bookingDetails.getSpeed() + " Kmph");
                map.put("cost", etTripCost.getText().toString());
                bookingDetails.setTripCost(etTripCost.getText().toString());
                // starting the trip
                updateDeviceStatus(true, map, tripCompleted);
                dialogTripCompleted.dismiss();
            }
        });

        dialogTripCompleted.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //  WindowManager.LayoutParams lp = campaignAlertDialog.getWindow().getAttributes();
        //lp.dimAmount=1.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        // lp.gravity = (Gravity.CENTER);
        // campaignAlertDialog.getWindow().setAttributes(lp);
        Window window = dialogTripCompleted.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        //  window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        dialogTripCompleted.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                Log.d("setOnDismissListener", "dialogTripCompleted setOnDismissListener");
                View view = DashboardActivity.this.getCurrentFocus();
                if (view != null) {
                    Common.closeKeyBoard(mContext, view);
                }

            }
        });
        dialogTripCompleted.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("setOnCancelListener", "setOnCancelListener setOnCancelListener");
                View view = DashboardActivity.this.getCurrentFocus();
                if (view != null) {
                    Common.closeKeyBoard(mContext, view);
                }
            }
        });


        dialogTripCompleted.show();
    }

    private void getNearByHospitals() {
        HashMap<String, Object> locationDetails = new HashMap<>();

        Log.d("getNearByHospitals", "getNearByHospitals");

        locationDetails.put("emergencyGeoHash", Common.getGeoHashCode(currentLocation));
        locationDetails.put("latitude", currentLocation.getLatitude());
        locationDetails.put("longitude", currentLocation.getLongitude());
        locationDetails.put("groupType", "2");
        locationDetails.put("customerApp", false);
        locationDetails.put("ambulanceApp", true);

        Log.i("location details", Arrays.asList(locationDetails).toString());

        if (Common.isNetworkAvailable(this)) {

            Common.displayProgress(mContext);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<HospitalList> getAmbulancesList = apiService.getNearByHospitalsList(locationDetails, Common.getAmbulanceId(this), Common.getAccessToken(this));

            getAmbulancesList.enqueue(new Callback<HospitalList>() {
                @Override
                public void onResponse(Call<HospitalList> call, Response<HospitalList> response) {
                    try {
                        Common.stopProgressDialog();
                        Log.i("hospitals list", response.body().toString());


                        HospitalList hospitalList = (HospitalList) response.body();
                        nearByHospitals = hospitalList.getResponseData();
                        //Collections.sort(nearByHospitals);
                        Log.d("near by hospitals", "size  " + nearByHospitals.size());
                       /* for(int i = 0 ; i < hospitals.size() ; i++){
                            HOSPITAL_TYPE.add(hospitals.get(i).getGroup_short_name()+" (~"+hospitals.get(i).getDistance()+" Kms )");
                        }
                        hospital_adapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_dropdown_item_1line, HOSPITAL_TYPE);
                        spinner_hospital.setAdapter(hospital_adapter);

                        callNetworkMethodForAmbulances();
*/
                        showAtEmergencyLocationDialog(true);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<HospitalList> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getNearByHospitals: " + "eroorrrrrrrrrrrr");
                    Log.e("eroo getNearByHospitals", throwable.toString());
                    System.out.print("erro = getNearByHospitals" + throwable.toString());
                    Common.displayAlertDialog(mContext, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }

    }

    private void getPatientDetails() {
        HashMap<String, Object> bookingDetailsMap = new HashMap<>();


        bookingDetailsMap.put("ambulanceApp", true);
        bookingDetailsMap.put("ambulance_id", Common.getAmbulanceId(mContext));
        bookingDetailsMap.put("booking_id", bookingDetails.getBookingId());


        Log.i("location details", Arrays.asList(bookingDetails).toString());

        if (Common.isNetworkAvailable(this)) {

            Common.displayProgress(mContext);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<JsonElement> getAmbulancesList = apiService.getPatientInfo(bookingDetailsMap, Common.getAmbulanceId(this), Common.getAccessToken(this));

            getAmbulancesList.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {
                        Common.stopProgressDialog();
                        Log.i("Patient details", serverResponse.body().toString());

                        Log.d("response", "response = getPatientDetails" + serverResponse.body());
                        System.out.print("response = getPatientDetails" + serverResponse.body());


                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            // patient details
                            JSONArray responseData = response.getJSONArray("responseData");
                            Log.d("responseData", "array patient" + responseData);
                            JSONObject jsonPatientDetails = responseData.getJSONObject(0);
                            Log.d("responseData", "array patient details" + jsonPatientDetails);
                            if (jsonPatientDetails.getString("patient_name").equalsIgnoreCase("") ||
                                    jsonPatientDetails.getString("patient_name").equalsIgnoreCase("null")) {
                                bookingDetails.setPatientName("");
                            } else {
                                bookingDetails.setPatientName(jsonPatientDetails.getString("patient_name"));
                            }

                            // updating customer name as patient name
                            bookingDetails.setCustomerName(bookingDetails.getPatientName());

                            bookingDetails.setPatientContactNumber(jsonPatientDetails.getString("contact_number"));
                            bookingDetails.setPatientCondition(jsonPatientDetails.getString("patient_condition"));
                            bookingDetails.setPatientAge(jsonPatientDetails.getString("age"));
                            bookingDetails.setPatientGender(jsonPatientDetails.getString("gender"));

                            if (!bookingDetails.getHospital().equalsIgnoreCase("any nearby hospital")) {
                                showAtEmergencyLocationDialog(false);
                            } else {
                                getNearByHospitals();
                            }
                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(DashboardActivity.this);

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase("411")) {

                            if (bookingDetails.getCustomerName() == null || bookingDetails.getCustomerName().equalsIgnoreCase("null")
                                    ) {
                                bookingDetails.setPatientName("");
                            } else {
                                bookingDetails.setPatientName(bookingDetails.getCustomerName());
                            }

                            if (bookingDetails.getAge() == null || bookingDetails.getAge().isEmpty()) {

                                bookingDetails.setPatientAge("");
                            } else {

                                bookingDetails.setPatientAge(bookingDetails.getAge());
                            }


                            bookingDetails.setPatientContactNumber(bookingDetails.getCustomerMobile());
                            bookingDetails.setPatientGender("male");
                            if (!bookingDetails.getHospital().equalsIgnoreCase("any nearby hospital")) {
                                showAtEmergencyLocationDialog(false);
                            } else {
                                getNearByHospitals();
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getPatientInfo: " + "eroorrrrrrrrrrrr");
                    Log.e("eror PatientDetails", throwable.toString());
                    System.out.print("erro =getPatientInfo" + throwable.toString());
                    Common.displayAlertDialog(mContext, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }

    }

    private void updateAmbulanceLocation() {


        if (Common.isNetworkAvailable(this)) {
            if (currentLocation == null) {
                return;
            }
            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulance_id", Common.getAmbulanceId(mContext));
            map.put("driver_id", Common.getDriverId(mContext));
            map.put("ambulanceApp", true);
            map.put(Constants.geoHashCode, Common.getGeoHashCode(currentLocation));
            map.put(Constants.latitude, currentLocation.getLatitude());
            map.put(Constants.longitude, currentLocation.getLongitude());

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call<JsonElement> call2 = apiService.updateAmbulanceLocation(map, Common.getUserId(mContext), Common.getAccessToken(mContext));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {

                        Log.d("response", "response = updateAmbulanceLocation" + serverResponse.body());
                        System.out.print("response = updateAmbulanceLocation" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {


                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(DashboardActivity.this);

                        }else {

                            // logging out driver if location update fails....
                            driverLogOutService();
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    Log.d("", "RetroFit2.0 :updateAmbulanceLocation: " + "eroorrrrrrrrrrrr");
                    Log.e("eroupdateAmbulanceLocat", throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                }
            });
        }
    }

    private void cancelBooking(String reason) {

        Log.d("cancelBooking", "cancelBooking");
        if (Common.isNetworkAvailable(this)) {

            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceApp", true);
            map.put("booking_id", bookingDetails.getBookingId());
            map.put("gcmToken", bookingDetails.getGcmToken());
            map.put("age", bookingDetails.getAge());
            map.put("reason", reason);


            if (!bookingDetails.getHospital().equalsIgnoreCase("any nearby hospital")) {

                map.put("hospital_latitude", bookingDetails.getHospitalLatitude());
                map.put("hospital_longitude", bookingDetails.getHospitalLongitude());
                LatLng destinationLatLng = new LatLng(Double.parseDouble(bookingDetails.getHospitalLatitude()), Double.parseDouble(bookingDetails.getHospitalLongitude()));
                map.put("hospital_geohash", Common.getGeoHashCode(destinationLatLng));
            }


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Common.displayProgress(mContext);
            Call<JsonElement> call2 = apiService.transferBooking(map, Common.getUserId(mContext), Common.getAccessToken(mContext));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {
                        Common.stopProgressDialog();

                        Log.d("response", "response = cancelBooking" + serverResponse.body());
                        System.out.print("response = cancelBooking" + serverResponse.body());

                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {


                            HashMap<String, Object> mGcmData = new HashMap<>();
                            HashMap<String, Object> mData = new HashMap<>();
                            //updateDeviceStatus(false, map, locationUpdate);
                            mGcmData.put("to", bookingDetails.getGcmToken());
                            mData.put("type", "ambulanceTrasnferredTheBooking");

                            mGcmData.put("data", mData);

                            SendPushNotification sendPushNotification = new SendPushNotification(mContext);
                            sendPushNotification.sendPn(mGcmData);

                            Intent intent = new Intent(DashboardActivity.this, TripRejectionActivity.class);
                            intent.putExtra("bookingId", bookingDetails.getBookingId());
                            //startActivity(intent);
                            clearAllandSetInitialStates();


                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(DashboardActivity.this);

                        } else {
                            Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :transferBooking: " + "eroorrrrrrrrrrrr");
                    Log.e("erooo transferBooking", throwable.toString());
                    System.out.print("transferBooking =" + throwable.toString());
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }

    private void fakeBooking() {

        Log.d("fakeBooking", "fakeBooking");
        if (Common.isNetworkAvailable(this)) {

            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceApp", true);
            map.put("booking_id", bookingDetails.getBookingId());
            //map.put("customer_id",Common.get);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Common.displayProgress(mContext);
            Call<JsonElement> call2 = apiService.fakeBooking(map, Common.getUserId(mContext), Common.getAccessToken(mContext));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {
                        Common.stopProgressDialog();

                        Log.d("fakeBooking", "fakeBooking = 222" + serverResponse.body());
                        System.out.print("fakeBooking = 22222" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            clearAllandSetInitialStates();

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase("413")) {

                            clearAllandSetInitialStates();
                            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                            simpleSingleButtonDialog = Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(DashboardActivity.this);

                        } else {
                            Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    Common.stopProgressDialog();
                    Log.d("", "fakeBooking.0 :fakeBooking: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", "fakeBooking" + throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }

    private void clearAllandSetInitialStates() {

        MyApplication.getApplication().setBookingInProgress(false);
        startUpdateStatusTimer();
        stopSendLatLangtimertask();
        stopForeGroundService();
        //  MyApplication.isBookingInProgress = false;
        isBookingInProgressToDrawPath = false;
        isPathDrawedFromAmbulanceToEmergency = false;
        bookingDetails = null;
        if (dialogAtEmergency != null && dialogAtEmergency.isShowing()) {
            dialogAtEmergency.dismiss();
        }
        if (mMap != null) {
            mMap.clear();
        }
        previousLocation = null;
        setInitialOnlineStatus();

        // clearing data in shared pref
        Common.saveBookingDetails(this, null);
    }

    private void startForeGroundService() {

        Intent startIntent = new Intent(DashboardActivity.this, TripProgressService.class);
        startIntent.setAction(TripProgressService.STARTFOREGROUND_ACTION);
        startIntent.putExtra("bookingDetails", bookingDetails);
        TripProgressService.IS_SERVICE_RUNNING = true;
        startService(startIntent);
    }

    private void stopForeGroundService() {
        TripProgressService.IS_SERVICE_RUNNING = false;
        Intent stopIntent = new Intent(DashboardActivity.this, TripProgressService.class);
        stopIntent.setAction(TripProgressService.STOPFOREGROUND_ACTION);
        startService(stopIntent);
    }

    private void updateBasedOnStatusFromService() {

        Log.d("service ***", "from servie**  updateBasedOnStatusFromService ***");

        Log.d("service ***", "from servie**  updateBasedOnStatusFromService ***" + bookingDetails.getBookingType());

        if (mMap != null && !isPathDrawedFromAmbulanceToEmergency) {

            drawPathFromAmbulanceToEmergency();
        }

        MyApplication.getApplication().setBookingInProgress(true);
        setStartTrip();
        if (bookingDetails.getBookingType().equalsIgnoreCase(MyFirebaseMessagingService.transferBooking) ||
                bookingDetails.getBookingType().equalsIgnoreCase(MyFirebaseMessagingService.bookingFromCommandCentre)) {

            imgCancelTrip.setVisibility(View.GONE);
            imgFakeBooking.setVisibility(View.GONE);
        }
        Log.d("current status", "booking" + bookingDetails.getCurrentBookingStatus());
        switch (bookingDetails.getCurrentBookingStatus()) {


            case Constants.WAY_TO_EMERGENCY_LOCATION:
                // starting the timer
                startSendLatLangTimer();
                setWayToEmergencyLocaiton();
                // break;
            case Constants.AT_EMERGENCY_LOCATION:
                startSendLatLangTimer();
                setWayToEmergencyLocaiton();
                break;
            case Constants.WAY_BACK_TO_DESTINATION:

                startSendLatLangTimer();
                setWayToEmergencyLocaiton();
                isDrawPathFromEmergencyToHospital = true;
                drawPathFromEmergencyToHospital();
                setWayToHospitalLocaiton();
                break;
            case Constants.REACHED_DESTINATION:
                startSendLatLangTimer();
                setWayToEmergencyLocaiton();
                isDrawPathFromEmergencyToHospital = true;
                drawPathFromEmergencyToHospital();
                setWayToHospitalLocaiton();
                break;

            default:
                break;
        }
    }


    public void playEmergencySound() {

        Log.d("playEmergencySound", "playEmergencySound");
        try {
           /* Uri notification = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_RINGTONE);

            Log.d("notification", "uri path" + notification.getPath());*/
            stopEmergencySound();
            stopCancelSound();
            if (ringTone == null) {
                ringTone = MediaPlayer.create(this, R.raw.ambulance_siron);
                //  ringTone = MediaPlayer.create(this, R.raw.dialtone);
                // DialTone.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                ringTone.setLooping(true);
                //ringTone.setTIm
                ringTone.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopEmergencySound() {
        Log.d("stopEmergencySound", "stopEmergencySound");
        try {
            if (ringTone != null && ringTone.isPlaying()) {
                System.out.println("StopDialTone :: isPlaying");
                ringTone.stop();
                ringTone.release();
                ringTone = null;
            }
        } catch (IllegalStateException e) {
            System.out.println("StopDialTone exception");
            e.printStackTrace();

        }

    }

    public void playCancelSound() {

        try {

            stopCancelSound();
            stopEmergencySound();
            Log.d("playCancelSound", "playCancelSound");
            if (cancelRingTone == null) {
                cancelRingTone = MediaPlayer.create(this, R.raw.cancel_alert);
                //  ringTone = MediaPlayer.create(this, R.raw.dialtone);
                // DialTone.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                cancelRingTone.setLooping(false);
                //ringTone.setTIm
                cancelRingTone.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopCancelSound() {
        Log.d("stopCancelSound", "stopCancelSound");
        try {
            if (cancelRingTone != null && cancelRingTone.isPlaying()) {
                System.out.println("StopDialTone :: isPlaying");
                cancelRingTone.stop();
                cancelRingTone.release();
                cancelRingTone = null;
            }
        } catch (IllegalStateException e) {
            System.out.println("StopDialTone exception");
            e.printStackTrace();
            return;
        }

    }


    // checking is better location
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > timeDifference;
        boolean isSignificantlyOlder = timeDelta < -timeDifference;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private void selectWhomToCall() {
        final CharSequence[] items = {getString(R.string.customer), getString(R.string.command_center),
                getString(R.string.cancel)};
        System.out.println("selectWhomToCall");
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Call");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {

                    String mobile = null;
                    if (bookingDetails != null) {
                        mobile = bookingDetails.getCustomerMobile();

                        if (mobile == null) {
                            mobile = "1234567890";
                        }
                    } else {
                        mobile = "1234567890";
                    }

                    // makeCallFromMobile(mobile);
                    makeCallViaProxy(Common.getStringPref(mContext, Constants.driverMobileNumber, ""), mobile);

                } else if (item == 1) {

                    String mobile = null;
                    if (bookingDetails != null) {
                        mobile = bookingDetails.getCommandCenter();
                        if (mobile == null) {
                            mobile = "1234567890";
                        }
                    } else {
                        mobile = "1234567890";
                    }
                    makeCallFromMobile(mobile);
                } else if (item == 2) {
                    dialog.dismiss();
                }
            }
        });
        dialogWhomToCall = builder.create();
        dialogWhomToCall.show();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public String getAppVersion() {
        String versionCode = "1.0";
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return versionCode;
    }

    private void enableLocationServices() {
        if (mLocationPermissionGranted && !Common.isLocationServiceEnabled(locationManager)) {

            // Common.hidesimpleSingleButtonAlertDialog(simpleTwoButtonDialog, mContext);
            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
           /* simpleTwoButtonDialog = Common.simpleTwoButtonAlertDialog(this, getString(R.string.locationServiceEnable), new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {
                    if (type == 0) {
                        // positive
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(myIntent);
                    }

                }
            });*/
            simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(this, getString(R.string.locationServiceEnable), new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {

                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(myIntent);
                }
            });
        }
    }

    private void enableNetWork() {

        //Common.hidesimpleSingleButtonAlertDialog(simpleTwoButtonDialog, mContext);
        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
        simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, getString(R.string.internetServiceEnable), new MyAlertListener() {
            @Override
            public void buttonClicked(int type) {

            }
        });

    }

    // accepts disantce in meters , time millis
    private double calculateSpeed(long totalTime, long distance) {

        Log.d("totalTime", "totalTime = " + totalTime);
        Log.d("distance", "distance = " + distance);
        double speed = 0;
        // speed calculation
        try {
            double distanceInKm = distance / 1000d;
            double totalTimeInHrs = (totalTime / (1000d * 60 * 60));

            Log.d("distanceInKm", "distanceInKm = " + distanceInKm);
            Log.d("totalTimeInHrs", "totalTimeInHrs = " + totalTimeInHrs);
            speed = (distanceInKm / totalTimeInHrs);
            speed *= 100;
            speed = Math.round(speed);
            speed /= 100;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return speed;
    }

    private void showDriverCallPopUp() {

        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);

        View popupView = layoutInflater.inflate(R.layout.lalyout_driver_call, null);

        PopupWindow popupWindow = new PopupWindow(popupView,
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT,
                true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);

        Rect rc = new Rect();
        imgCallButton.getWindowVisibleDisplayFrame(rc);
        int[] xy = new int[2];
        imgCallButton.getLocationInWindow(xy);
        rc.offset(xy[0], xy[1]);


        //   popupWindow.showAtLocation(popupView, Gravity.TOP, imgCallButton.getLeft(),
        //            imgCallButton.getHeight());

        // popupWindow.showAsDropDown(imgCallButton, 0, Gravity.TOP | Gravity.RIGHT);

        popupWindow.showAtLocation(popupView, Gravity.CENTER, imgCallButton.getLeft() - (imgCallButton.getWidth() * 2),
                imgCallButton.getTop() + (imgCallButton.getHeight() * 2));
        /// popupWindow.showAtLocation(imgCallButton, Gravity.BOTTOM, 0,
        //         imgCallButton.getBottom());
        //popupWindow.showAtLocation(imgCallButton, Gravity.TOP|Gravity.RIGHT, 0, 5);

        final TextView txtCustomerOrHospital = (TextView) popupView.findViewById(R.id.txtCustomerOrHospital);
        TextView txtCommandCenter = (TextView) popupView.findViewById(R.id.txtCommandCenter);

        String currentStatus = bookingDetails.getCurrentBookingStatus();
        if (currentStatus.equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION) ||
                currentStatus.equalsIgnoreCase(Constants.REACHED_DESTINATION)) {

            txtCustomerOrHospital.setText("Hospital");
            Drawable img = getResources().getDrawable(R.drawable.hospital);
            txtCustomerOrHospital.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        } else {

            txtCustomerOrHospital.setText("Customer");
            Drawable img = getResources().getDrawable(R.drawable.driver_call);
            txtCustomerOrHospital.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        }

        txtCustomerOrHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (txtCustomerOrHospital.getText().toString().equalsIgnoreCase("hospital")) {

                        String mobile = null;
                        if (bookingDetails != null) {
                            mobile = bookingDetails.getHospitalNumber();
                        } else {
                            mobile = "9010001269";
                        }
                        makeCallFromMobile(mobile);
                    } else {

                        String mobile = null;
                        if (bookingDetails != null) {
                            mobile = bookingDetails.getCustomerMobile();
                        } else {
                            mobile = "9010001269";
                        }
                        makeCallFromMobile(mobile);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        txtCommandCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String mobile = null;
                    if (bookingDetails != null) {
                        mobile = bookingDetails.getCommandCenter();
                    } else {
                        mobile = "9010001269";
                    }
                    makeCallFromMobile(mobile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void makeCallFromMobile(String mobile) {

        // removing the 91 while making the call
        mobile = removeIndianCountryCode(mobile);

        int result = ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.CALL_PHONE);

        if (result != PackageManager.PERMISSION_GRANTED) {
            Log.d("ACTION_DIAL", "ACTION_DIAL");
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mobile, null)));
        } else {
            String uri = "tel:" + mobile.trim();
            Log.d("ACTION_CALL", "ACTION_CALL");
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(uri)));
        }
    }

    /**
     * @param numbersList taking second number in list as TO number
     */
    private void makeCallViaProxy(final String... numbersList) {

        if (Common.isNetworkAvailable(this)) {


            String numbers = "";
            String delim = "";
            for (String num : numbersList) {
                //   numbers = numbers + delim + num;
                // removing the indian country code
                numbers = numbers + delim + removeIndianCountryCode(num);
                delim = ",";
            }

            Log.d("numbers","numbers = "+numbers);
            Common.displayProgress(mContext);
            ApiInterface apiService =
                    ApiClient.getClientConnectCall().create(ApiInterface.class);
            Call<Object> call = apiService.connectCall(numbers);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        Object jsonElement = serverResponse.body();

                        Log.d("jsonElement", "jsonElement = " + jsonElement);

                        if (jsonElement == null) {
                            // call success
                        } else {
                            // call failure
                            makeCallFromMobile(numbersList[1]);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :makeCallViaProxy: " + "eroorrrrrrrrrrrr");
                    Log.e("ero makeCallViaProxy", throwable.toString());
                    System.out.print("erro = makeCallViaProxy" + throwable.toString());
                    Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
                    simpleSingleButtonDialog =
                            Common.displayAlertDialog(mContext, getString(R.string.networkError));
                    MyApplication.getApplication().trackEvent("Dashboard", getString(R.string.networkError), "makeCallViaProxy", false);

                    makeCallFromMobile(numbersList[1]);
                }
            });
        } else {
            Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
            simpleSingleButtonDialog =
                    Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }

    private void calculateDistance() {

        if (previousLocation != null) {

            if (bookingDetails.getCurrentBookingStatus().equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)) {

                bookingDetails.setCalculatedDistanceFromEmergencyToHospitalValue(
                        (bookingDetails.getCalculatedDistanceFromEmergencyToHospitalValue() + previousLocation.distanceTo(currentLocation)));

            } else {

                bookingDetails.setCalculatedDistanceFromAmbToEmergencyValue(
                        (bookingDetails.getCalculatedDistanceFromAmbToEmergencyValue() + previousLocation.distanceTo(currentLocation)));
            }

        }
        previousLocation = currentLocation;
    }

    private void requestLocationUpdates() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
    }


    private void updateMapToShowAllMarkers(ArrayList<Marker> markers) {
        if (mMap != null) {
            LatLngBounds.Builder b = new LatLngBounds.Builder();
            for (Marker m : markers) {
                b.include(m.getPosition());
            }
            LatLngBounds bounds = b.build();
            //Change the padding as per needed
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mMap.animateCamera(cu);
        }
    }

    private void showBookingCancelledOrTransferredDialog(String type) {

        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog, mContext);
        playCancelSound();
        String message = "";
        switch (type) {
            case MyFirebaseMessagingService.cancelBooking:
                message = getString(R.string.customer_has_cancelled_your_booking);
                break;
            case MyFirebaseMessagingService.cancelBookingCommandCenter:
                message = getString(R.string.command_has_cancelled_your_booking);
                break;
            case MyFirebaseMessagingService.transferBooking:
                message = getString(R.string.transferred_your_booking);
                break;

            default:
                break;
        }
        if (!message.isEmpty()) {
            simpleSingleButtonDialog =
                    Common.simpleSingleButtonAlertDialog(mContext, message, new MyAlertListener() {
                        @Override
                        public void buttonClicked(int type) {
                            stopCancelSound();
                        }
                    });

        }

    }

    private void closeAllPopups() {
        if (dialogRejectionReason != null && dialogRejectionReason.isShowing()) {
            dialogRejectionReason.dismiss();
        }
        if (dialogAtEmergency != null && dialogAtEmergency.isShowing()) {
            dialogAtEmergency.dismiss();
        }
        if (dialogTripCompleted != null && dialogTripCompleted.isShowing()) {
            dialogTripCompleted.dismiss();
        }
        if (dialogWhomToCall != null && dialogWhomToCall.isShowing()) {
            dialogWhomToCall.dismiss();
        }

    }

    private void getCommandCenterNumber() {

        if (Common.isNetworkAvailable(this)) {

            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceApp", true);
            //map.put("customer_id",Common.get);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Common.displayProgress(mContext);
            Call<JsonElement> call2 = apiService.getCommandCenterNumber(map);
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {
                        Common.stopProgressDialog();

                        Log.d("getCommandCenterNumber", "getCommandCenterNumber = " + serverResponse.body());
                        System.out.print("getCommandCenterNumber =" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            String num = response.getString("responseData");
                            Common.saveStringPref(mContext, Constants.commandCenterNumber, num);
                            makeCallFromMobile(num);
                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(DashboardActivity.this);

                        } else {
                            Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    Common.stopProgressDialog();
                    Log.d("", "getCommandCenterNumber.0 :getCommandCenterNumber: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", "getCommandCenterNumber" + throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }

    private void clearCache() {
        //MyApplication.getApplication().clearApplicationData();



       /* Intent intent = new Intent(mContext, SplashScreen.class);
        if (Build.VERSION.SDK_INT >= 11) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        startActivity(intent);
        finish();*/
    }
    private String  removeIndianCountryCode(String number){
        Log.d("removeIndianCountryCode","number = before "+number);
        number = number.trim();

        if(number != null && number.length() == 12 && number.startsWith("91")){

            number = number.substring(2);
        }
        Log.d("removeIndianCountryCode","number = after "+number);

        return number;
    }
    private String getFullOfShortCode(String input){

        String response = input;
        switch (input){
            case Constants.BOOKING_ACCEPTED:

                response = Constants.BOOKING_ACCEPTED_FULL;
                break;
            case Constants.WAY_TO_EMERGENCY_LOCATION:

                response = Constants.WAY_TO_EMERGENCY_LOCATION_FULL;
                break;
            case Constants.AT_EMERGENCY_LOCATION:

                response = Constants.AT_EMERGENCY_LOCATION_FULL;
                break;
            case Constants.WAY_BACK_TO_DESTINATION:

                response = Constants.WAY_BACK_TO_DESTINATION_FULL;
                break;
            case Constants.REACHED_DESTINATION:

                response = Constants.REACHED_DESTINATION_FULL;
                break;
            case Constants.TRIP_CLOSED:

                response = Constants.TRIP_CLOSED_FULL;
                break;
            case Constants.FAKE_BOOKING:

                response = Constants.FAKE_BOOKING_FULL;
                break;
            case "Started":

                response = "Started";
                break;
        }

        return response;
    }

}
