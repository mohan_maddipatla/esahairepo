package com.esahaiambulance.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.db.DatabaseHelper;
import com.esahaiambulance.models.AmbulanceAttributesModel;
import com.esahaiambulance.models.EmergencyType;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileVerificationActivity extends AppCompatActivity {


    EditText etMobileNumber, etOtp;
    Button btnSubmit;
    Context mContext;
    boolean isGcmTokenGenerated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        mContext = this;
        initViews();
        etMobileNumber.setText("+91 " + getIntent().getStringExtra("mobileNumber"));
        //etOtp.setText(getIntent().getStringExtra("otp"));
    }

    private void initViews() {

        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etOtp = (EditText) findViewById(R.id.etOtp);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setEnabled(false);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.getApplication().trackEvent("Moible verification", "Button", "" + etMobileNumber.getText().toString());
                verifyOtp();
            }
        });

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etOtp.getText().toString().length() == 6) {
                    Common.closeKeyBoard(mContext, etOtp);
                    btnSubmit.setEnabled(true);
                } else {
                    btnSubmit.setEnabled(false);
                }
            }
        });
        etOtp
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                               @Override
                                               public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                                                   boolean handled = false;
                                                   if (actionId == EditorInfo.IME_ACTION_DONE) {
                                                       Common.closeKeyBoard(mContext, textView);
                                                       verifyOtp();
                                                       handled = true;
                                                   }
                                                   return handled;
                                               }
                                           }
                );
    }

    private void verifyOtp() {

        if (etOtp.getText().toString().length() != 6) {
            etOtp.setError(getString(R.string.validOtp));
            return;
        }

        Log.d("mobile", "after encrypt mobile" + Common.encrypt(etMobileNumber.getText().toString().replace("+91 ", "")));
        /*Intent intent = new Intent(mContext, AmbulanceProfileActivity.class);
        startActivity(intent);
        finish();*/
        Log.d("otp", "otp" + Common.encrypt(etOtp.getText().toString()));
        if (Common.isNetworkAvailable(this)) {
            HashMap<String, Object> map = new HashMap<>();
            try {

                String token = FirebaseInstanceId.getInstance().getToken();
                if (token == null) {
                    token = "12345";
                } else {
                    isGcmTokenGenerated = true;
                }
                map.put("ambulanceMobile", Common.encrypt(etMobileNumber.getText().toString().replace("+91 ", "")));
                map.put("deviceIMEI", getImeiId());
                map.put("otp", Common.encrypt(etOtp.getText().toString()));
                map.put("deviceType", getosVersion());
                map.put("deviceOs", "Android");
                map.put("deviceToken", token);
                map.put("fromApp", "true");
                map.put("app_version", getAppVersion());


            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("req", "req = " + Arrays.asList(map));
            Common.displayProgress(this);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<JsonElement> call2 = apiService.verifyAmbulanceOTP(map);
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            // Common.saveBooleanPref(mContext, Constants.isDeviceLoggedIn, true);
                            MyApplication.getApplication().trackEvent("Moible verification", "Success", "" + etMobileNumber.getText().toString(), false);

                            JSONObject jsonAmbulanceDetails = response.getJSONObject("responseData");
                            Common.saveStringPref(mContext, Constants.ambId, jsonAmbulanceDetails.getString("amb_id"));
                            Common.saveStringPref(mContext, Constants.ambulanceNumber, jsonAmbulanceDetails.getString("ambulance_number"));
                            Common.saveStringPref(mContext, Constants.ambGroupId, jsonAmbulanceDetails.getString("group_id"));
                            Common.saveStringPref(mContext, Constants.ambGroupNmae, jsonAmbulanceDetails.getString("group_name"));

                            Common.saveStringPref(mContext, Constants.ambProfilePic, jsonAmbulanceDetails.getString("file_url"));
                            Common.saveStringPref(mContext, Constants.ambMobileNumber, jsonAmbulanceDetails.getString("ambulance_mobile"));
                            Common.saveStringPref(mContext, Constants.accessToken, jsonAmbulanceDetails.getString("token"));
                            Common.saveStringPref(mContext, Constants.emergencyType, jsonAmbulanceDetails.getString("emergency_type"));
                            Common.saveStringPref(mContext, Constants.vehicleType, jsonAmbulanceDetails.getString("vehicle_type"));


                            //Common.saveStringPref(mContext, Constants.groupName, jsonAmbulanceDetails.getString("group_name"));


                            Common.saveStringPref(mContext, Constants.ambHospitalNumber, jsonAmbulanceDetails.getString("group_mobile_number"));

                            //gcm_key

                            String api =  Common.decrypt(jsonAmbulanceDetails.getString("gcm_key"));
                            Log.d("api","api = "+api);

                            Common.saveStringPref(mContext, Constants.pnApiKey,api);

                            Common.saveBooleanPref(mContext, Constants.isDeviceLoggedIn, true);

                            if (isGcmTokenGenerated) {
                                Common.saveBooleanPref(mContext, Constants.isTokenSentToServer, true);
                            }


                            JSONArray arraysList = jsonAmbulanceDetails.getJSONArray("ambulance_attributes");
                            DatabaseHelper dbHelper = DatabaseHelper.getInstance(mContext);
                            dbHelper.clearAttributes();
                            if (arraysList.length() > 0) {


                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<AmbulanceAttributesModel>>() {
                                }.getType();
                                ArrayList<AmbulanceAttributesModel> arrayList = gson.fromJson("" + arraysList, listType);


                                for (AmbulanceAttributesModel item : arrayList) {

                                    dbHelper.insertAmbulanceAttributes(item.getAttributeName(), item.getAttributeDetails());
                                }
                            }
                            JSONArray arraysListEmergencyTypes = jsonAmbulanceDetails.getJSONArray("emergency_types");
                            dbHelper.clearEmergencyTypes();
                            if (arraysListEmergencyTypes.length() > 0) {

                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<EmergencyType>>() {
                                }.getType();
                                ArrayList<EmergencyType> arrayList = gson.fromJson("" + arraysListEmergencyTypes, listType);


                                for (EmergencyType item : arrayList) {

                                    dbHelper.insertEmergencyType(item.getTypeName(), item.getId());
                                }

                            }
                            dbHelper.getEmergencyTypes();


                            Intent intent = new Intent(mContext, AmbulanceProfileActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            MyApplication.getApplication().trackEvent("Moible verification", "Failed", "" + etMobileNumber.getText().toString(), false);
                            Common.displayAlertDialog(MobileVerificationActivity.this, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    MyApplication.getApplication().trackEvent("Moible verification", "Verify mobile Network failure", "" + etMobileNumber.getText().toString(), false);
                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    Common.displayAlertDialog(MobileVerificationActivity.this, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }

    }

    private String getImeiId() {
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        String imei = tm.getDeviceId();
        Log.d("imei", imei);
        return imei;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private String getosVersion() {
        String versionRelease = Build.VERSION.RELEASE;

        return versionRelease;

    }

    private Double getScreenSize() {
        WindowManager w = getWindowManager();
        Display d = w.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        d.getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;
// includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
            try {
                widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
                heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
            } catch (Exception ignored) {
            }
// includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 17)
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
                widthPixels = realSize.x;
                heightPixels = realSize.y;
            } catch (Exception ignored) {
            }
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        double x = Math.pow(widthPixels / dm.xdpi, 2);
        double y = Math.pow(heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);

        screenInches = Math.round(screenInches * 10.0) / 10.0;

        return screenInches;
    }
    public int getAppVersion() {
        int versionCode = 1;
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return versionCode;
    }
}
