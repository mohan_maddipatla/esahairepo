package com.esahaiambulance.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.models.TripDetails;

import java.util.concurrent.TimeUnit;

/**
 * Created by prashanth on 20/9/16.
 */

public class TripDetailsActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtTitle;
    Context mContext;
    Button btnTripCompleted;
    TextView txtTotalDistance, txtTotalTime, txtAvgSpeed, txtCost;

    TripDetails tripDetails;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_completed);
        Log.d("onCreate", "onCreate");
        mContext = this;
        tripDetails = (TripDetails) getIntent().getSerializableExtra("tripDetails");
        Log.d("bpooking details","details"+tripDetails.toString());
        initViews();
        txtTitle.setText(""+tripDetails.getBookingId());
        try {
            double distance = (Double.parseDouble(tripDetails.getDistance()) / 1000d);
            distance = distance * 1000;
            distance = Math.round(distance);
            distance = distance / 1000;
            txtTotalDistance.setText( distance+ " Km");
        } catch (Exception e) {
            e.printStackTrace();
            txtTotalDistance.setText(tripDetails.getDistance());
        }
        try {
            txtTotalTime.setText(getFormattedTime(Long.parseLong(tripDetails.getDuration())));
        } catch (Exception e) {
            txtTotalTime.setText(tripDetails.getDuration());
            e.printStackTrace();
        }
        txtAvgSpeed.setText(tripDetails.getSpeed());
        txtCost.setText("Rs. "+tripDetails.getCost());
    }

    private void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText(getString(R.string.app_name));

        ImageView img = (ImageView)toolbar.findViewById(R.id.img_toolbarimg);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtTotalDistance = (TextView) findViewById(R.id.txtTotalDistance);
        txtTotalTime = (TextView) findViewById(R.id.txtTotalTime);
        txtAvgSpeed = (TextView) findViewById(R.id.txtAvgSpeed);
        txtCost = (TextView) findViewById(R.id.txtCost);

        btnTripCompleted = (Button) findViewById(R.id.btnTripCompleted);

        btnTripCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(mContext,DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                finish();
            }
        });
    }

    private String getFormattedTime(long millis) {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
