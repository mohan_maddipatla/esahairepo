package com.esahaiambulance.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.models.BookingDetails;
import com.esahaiambulance.utils.AppObserver;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.utils.MyAlertListener;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;

import com.esahaiambulance.webservices.CallbackWithRetry;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;

/**
 * Created by prashanth on 20/9/16.
 */
@SuppressWarnings({"MissingPermission"})
public class NewTripRequestActivity extends AppCompatActivity implements AppObserver {

    Toolbar toolbar;
    TextView txtTitle;
    Context mContext;

    Button btnAccept, btnReject, btnCount;
    TextView txtDistanceDetails, txtTripCause, txtBookingId;

    CountDownTimer countDownTimer = null;
    BookingDetails bookingDetails;
    MediaPlayer ringTone = null;

    boolean isBookingAcceptanceInProgress = false;
    private Dialog simpleSingleButtonDialog = null;

    private PowerManager.WakeLock inCallWakeLock;
    private PowerManager powerManager;

    boolean isOnresumeFirstTime = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_trip_request);
        Log.d("onCreate", "onCreate cout ++++");

        mContext = this;

        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        inCallWakeLock = powerManager.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "wakelock");
        inCallWakeLock.setReferenceCounted(false);

        if (inCallWakeLock != null) {
            inCallWakeLock.acquire();
        }

        MyApplication.getApplication().addObserver(this);
        initViews();
        if (getIntent().hasExtra(Constants.bookingDetails)) {
            bookingDetails = (BookingDetails) getIntent().getSerializableExtra("bookingDetails");

        }

        Log.d("booking details", "" + bookingDetails);
        startBookingShow();

        Common.increaseGcmHeartBeat(this);

    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText(getString(R.string.app_name));


        txtDistanceDetails = (TextView) findViewById(R.id.txtDistanceDetails);
        txtTripCause = (TextView) findViewById(R.id.txtTripCause);
        txtBookingId = (TextView) findViewById(R.id.txtBookingId);

        btnAccept = (Button) findViewById(R.id.btnAccept);
        btnReject = (Button) findViewById(R.id.btnCancel);
        btnCount = (Button) findViewById(R.id.button_count);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                acceptBooking();
               /* Intent intent = new Intent();
                intent.putExtra("status", true);
                NewTripRequestActivity.this.setResult(RESULT_OK, intent);
                finish();*/


            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // MyApplication.isBookingInProgress = false;
                MyApplication.getApplication().setBookingInProgress(false);
                StopRingtone();
                countDownTimer.cancel();
                Intent intent = new Intent();
                intent.putExtra("status", false);
                NewTripRequestActivity.this.setResult(DashboardActivity.MY_TRIP_ACCEPT_REQUEST_CODE, intent);

                Intent intentToTripRejection = new Intent(mContext, TripRejectionActivity.class);
                intentToTripRejection.putExtra("isFromNewTrip", true);
                finish();
                startActivity(intentToTripRejection);
            }
        });

    }

    private void startBookingShow() {

        MyApplication.getApplication().setBookingInProgress(true);
        if (bookingDetails != null) {
            txtDistanceDetails.setText(bookingDetails.getDistance() + " Km"+", " + bookingDetails.getDuration() + " "+getString(R.string.away)
                    + "\n" + bookingDetails.getAddress());

            txtBookingId.setText(bookingDetails.getBookingId());
            txtTripCause.setText(bookingDetails.getEmergencyType());

            Log.d("Hospital name", "" + bookingDetails.getHospital());
        }
        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog,mContext);

        countDownTimer = new CountDownTimer(1000 * 20 * 1, 1000) {

            public void onTick(long millisUntilFinished) {
                btnCount.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {

                //  StopRingtone();
                // btnReject.performClick();
                loadNextBooking();
            }

        };
        countDownTimer.start();
        StartRingtone();
    }

    @Override
    protected void onStop() {

        Log.d("onStop", "onStop");
        super.onStop();

        //StopRingtone();
        MyApplication.getApplication().deleteObserver(this);
    }

    @Override
    protected void onDestroy() {
        countDownTimer.cancel();
        Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog,mContext);
        super.onDestroy();
    }

    private void acceptBooking() {

        if (Common.isNetworkAvailable(mContext)) {

            countDownTimer.cancel();
            StopRingtone();

            MyApplication.getApplication().getBookingList().remove(bookingDetails.getBookingId());
            isBookingAcceptanceInProgress = true;


            Log.d("acceptBooking", "acceptBooking");
            Location currentLocation = null;
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (currentLocation == null) {
                currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            HashMap<String, Object> reqJson = new HashMap<>();
            HashMap<String, Object> params = new HashMap<>();
            HashMap<String, Object> conditionalParams = new HashMap<>();

            params.put("ambulance_id", Common.getAmbulanceId(mContext));
            params.put("ambulance_number", Common.getStringPref(this, Constants.ambulanceNumber, ""));


            reqJson.put(Constants.ambulanceApp, true);
            reqJson.put(Constants.driverId, Common.getDriverId(mContext));
            // added booking type
            reqJson.put("pn_type", bookingDetails.getBookingType());


            params.put("ambulance_start_latitude", currentLocation.getLatitude());
            params.put("ambulance_start_longitude", currentLocation.getLongitude());
            params.put("ambulance_start_geoHashCode", Common.getGeoHashCode(currentLocation));
            params.put("booking_status", "Accepted");

            params.put("distance", bookingDetails.getDistance());
            params.put("duration", bookingDetails.getDuration());
            params.put("cost", " ");

            reqJson.put("driverName", Common.getStringPref(this, Constants.driverName, ""));
            reqJson.put("vehicleNumber", Common.getStringPref(this, Constants.ambulanceNumber, ""));
            reqJson.put("driverMobileNumber", Common.getStringPref(this, Constants.driverMobileNumber, ""));
            reqJson.put("driverProfilePic", Common.getStringPref(this, Constants.driverProfilePic, ""));

            reqJson.put("emergencyLatitude", bookingDetails.getLatitude());
            reqJson.put("emergencyLongitude", bookingDetails.getLongitude());

            reqJson.put("ambulanceHospital", Common.getStringPref(this, Constants.ambGroupNmae, ""));
            reqJson.put("deviceToken", bookingDetails.getGcmToken());

            // conditionalParams.put("is_booked", true);
            conditionalParams.put("booking_id", bookingDetails.getBookingId());


            reqJson.put("params", params);
            reqJson.put("conditionalParams", conditionalParams);

            // hospital
            reqJson.put("hospital_latitude", bookingDetails.getHospitalLatitude());
            reqJson.put("hospital_longitude", bookingDetails.getHospitalLongitude());

            reqJson.put("device_type",bookingDetails.getDeviceType());

            Log.d("req", "req = " + Arrays.asList(reqJson));


            Common.displayProgress(mContext);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<JsonElement> call = apiService.assignBooking(reqJson, Common.getUserId(mContext), Common.getAccessToken(mContext));

            call.enqueue(new CallbackWithRetry<JsonElement>(call) {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    Common.stopProgressDialog();


                    try {

                        System.out.print("bodyyy = 22222" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            Log.d("ambulance details", "updated ambulance status");

                            if (response.has("centreNumber")) {
                                Common.saveStringPref(mContext, Constants.commandCenterNumber, response.getString("centreNumber"));
                                bookingDetails.setCommandCenter(response.getString("centreNumber"));
                            }
                            Common.cancelAccRegNotification(mContext);

                            bookingDetails.setBookingId(bookingDetails.getBookingId().replaceFirst("^0+(?!$)", ""));
                            //  MyApplication.isBookingInProgress = true;
                            MyApplication.getApplication().setBookingInProgress(true);

                            bookingDetails.setCurrentBookingStatus(Constants.BOOKING_ACCEPTED);
                            Intent intent1 = new Intent(NewTripRequestActivity.this, DashboardActivity.class);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent1.putExtra(Constants.bookingDetails, bookingDetails);

                            MyApplication.getApplication().getBookingList().clear();
                            NewTripRequestActivity.this.finish();
                            startActivity(intent1);

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {

                            //MyApplication.isBookingInProgress = false;
                            MyApplication.getApplication().setBookingInProgress(false);
                            Common.doLogOut(mContext);

                        } else {

                            // MyApplication.isBookingInProgress = false;

                            MyApplication.getApplication().setBookingInProgress(false);
                            simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, response.getString(Constants.statusMessage), new MyAlertListener() {
                                @Override
                                public void buttonClicked(int type) {
                                    simpleSingleButtonDialog = null;
                                   /* if(MyApplication.bookingsList.size()>0){

                                        loadNextBooking();

                                    }else{

                                        Intent intent1 = new Intent(NewTripRequestActivity.this, DashboardActivity.class);
                                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent1);

                                    }*/
                                    loadNextBooking();
                                }
                            });
                        }
                    } catch (Exception e) {

                        isBookingAcceptanceInProgress = false;
                        e.printStackTrace();
                        moveToDashBoard();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Log.d("", "RetroFit2.0 :getRoute: " + "eroorrrrrrrrrrrr");
                    Log.e("eroo getRoute", throwable.toString());
                    System.out.print("eroooooooorr accept " + throwable.toString());
                    // Log.e(TAG, throwable.getLocalizedMessage());
                    if (retryCount++ < TOTAL_RETRIES) {
                        Log.v(TAG, "Retrying... (" + retryCount + " out of " + TOTAL_RETRIES + ")");
                        retry();
                    }
                    else{

                        isBookingAcceptanceInProgress = false;
                        Common.stopProgressDialog();

                        Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                        Log.e("eroooooooorr", throwable.toString());
                        // Common.displayAlertDialog(mContext, mContext.getString(R.string.networkError));
                        if (mContext != null) {
                            AppCompatActivity activity = (AppCompatActivity) mContext;
                            if (!activity.isFinishing()) {
                                simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, mContext.getString(R.string.networkError), new MyAlertListener() {
                                    @Override
                                    public void buttonClicked(int type) {
                                        simpleSingleButtonDialog = null;
                                        loadNextBooking();
                                    }
                                });
                            }
                        }
                    }

                }
            });
        } else {

            Common.displayAlertDialog(mContext, mContext.getString(R.string.offline));
        }
    }


    private void rejectBooking() {

    }

    public void StartRingtone() {
        try {
            Uri notification = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_RINGTONE);

            Log.d("notification", "uri path" + notification.getPath());
            if (ringTone == null) {
                ringTone = MediaPlayer.create(this, R.raw.ambulance_siron);
                //  ringTone = MediaPlayer.create(this, R.raw.dialtone);
                // DialTone.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                ringTone.setLooping(true);
                //ringTone.setTIm
                ringTone.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void StopRingtone() {
        try {
            if (ringTone != null && ringTone.isPlaying()) {
                System.out.println("StopDialTone :: isPlaying");
                ringTone.stop();
                ringTone.release();
                ringTone = null;
            }
        } catch (IllegalStateException e) {
            System.out.println("StopDialTone exception");
            e.printStackTrace();
            return;
        }

    }

    @Override
    public void update(final int event, final Object arg) {

        Log.d("update", "new trip request dashboard update started = "+isBookingAcceptanceInProgress);

        if ((event == MyApplication.BOOKING_CANCELLED || event == MyApplication.BOOKING_CANCELLED_COMMAND_CENTER) && !isBookingAcceptanceInProgress) {
            String bookId = (String) arg;
            handleBookingCancelledOrAssignedToOther(event, bookId);


        } else if (event == MyApplication.OTHER_AMBULANCE_ACCEPTED) {
            String bookId = (String) arg;
            handleBookingCancelledOrAssignedToOther(MyApplication.OTHER_AMBULANCE_ACCEPTED, bookId);

        }
        else if (event == MyApplication.TRANSFERRED_NEW_BOOKING || event == MyApplication.COMMAND_CENTER_NEW_BOOKING){
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            StopRingtone();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Common.stopProgressDialog();
                    Common.hidesimpleSingleButtonAlertDialog(simpleSingleButtonDialog,mContext);
                    isBookingAcceptanceInProgress = false;
                    bookingDetails = (BookingDetails) arg;
                    Intent intent1 = new Intent(NewTripRequestActivity.this, DashboardActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent1.putExtra(Constants.bookingDetails, bookingDetails);
                    intent1.putExtra("typeFromNewTripScreen", event);

                    NewTripRequestActivity.this.finish();
                    startActivity(intent1);
                    MyApplication.getApplication().getBookingList().clear();

                }
            });

        }
        else if (event == 1) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    MyApplication.getApplication().setBookingInProgress(true);

                    BookingDetails details = (BookingDetails) arg;
                  /*  Intent intent = new Intent(DashboardActivity.this, NewTripRequestActivity.class);
                    intent.putExtra(Constants.bookingDetails, details);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                    finish();*/

                    if (MyApplication.getApplication().getBookingList().size() > 0) {
                        MyApplication.getApplication().getBookingList().put(details.getBookingId(), details);
                    }else{

                        bookingDetails =  (BookingDetails)arg;
                        MyApplication.getApplication().setBookingInProgress(true);
                        MyApplication.getApplication().getBookingList().remove(bookingDetails.getBookingId());
                        startBookingShow();
                    }
                }
            });

        }
    }

    private void loadNextBooking() {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        StopRingtone();
        if (MyApplication.getApplication().getBookingList().size() > 0) {

           /* for (HashMap.Entry<String, BookingDetails> entry : MyApplication.getApplication().getBookingList().entrySet()) {
                bookingDetails = entry.getValue();
                break;
            }*/

            List<Map.Entry<String, BookingDetails>> list = new ArrayList<>(MyApplication.getApplication().getBookingList().entrySet());


            // last in first out
/*
            for (int i = list.size() - 1; i >= 0; i--) {
                Map.Entry<String, BookingDetails> entry = list.get(i);
                bookingDetails = entry.getValue();
                break;
            }
*/

            // first in first out
            boolean isBookingFound = false;
            for (int i = 0; i < list.size(); i++) {
                Map.Entry<String, BookingDetails> entry = list.get(i);
                bookingDetails = entry.getValue();
                if ((Calendar.getInstance().getTimeInMillis() - bookingDetails.getBookingReceivedTime()) < 1000 * 20) {
                    isBookingFound = true;
                    break;
                }

            }

            // MyApplication.isBookingInProgress = true;
            if (isBookingFound) {
                MyApplication.getApplication().setBookingInProgress(true);
                MyApplication.getApplication().getBookingList().remove(bookingDetails.getBookingId());
                startBookingShow();
            } else {

                MyApplication.getApplication().setBookingInProgress(false);
                moveToDashBoard();
            }


        } else {

            MyApplication.getApplication().setBookingInProgress(false);
            moveToDashBoard();

        }
    }

    @Override
    public void onBackPressed() {

    }

    private void handleBookingCancelledOrAssignedToOther(final int type, final String bookingId) {

        if (bookingDetails != null && bookingDetails.getBookingId().equalsIgnoreCase(bookingId) && !isBookingAcceptanceInProgress) {
            Log.d("same booking details", "same booking details");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MyApplication.getApplication().getBookingList().remove(bookingId);
                    if (MyApplication.getApplication().getBookingList().size() > 0) {
                        loadNextBooking();
                        Log.d("bookingDetails", "loadNextBooking");
                    } else {

                        MyApplication.getApplication().setBookingInProgress(false);


                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                        }
                        StopRingtone();
                        if (mContext != null) {
                            Log.d("bookingDetails", "showing alert");

                            if (type == MyApplication.OTHER_AMBULANCE_ACCEPTED) {
                                simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, getString(R.string.other_ambulance_accepted), new MyAlertListener() {
                                    @Override
                                    public void buttonClicked(int type) {

                                        simpleSingleButtonDialog = null;
                                        moveToDashBoard();

                                    }
                                });
                            } else if (type == MyApplication.BOOKING_CANCELLED) {

                                simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, getString(R.string.customer_has_cancelled_your_booking), new MyAlertListener() {
                                    @Override
                                    public void buttonClicked(int type) {

                                        // MyApplication.isBookingInProgress = false;
                                        simpleSingleButtonDialog = null;

                                        moveToDashBoard();
                                    }
                                });
                            }else if (type == MyApplication.BOOKING_CANCELLED_COMMAND_CENTER) {

                                simpleSingleButtonDialog = Common.simpleSingleButtonAlertDialog(mContext, getString(R.string.command_has_cancelled_your_booking), new MyAlertListener() {
                                    @Override
                                    public void buttonClicked(int type) {

                                        // MyApplication.isBookingInProgress = false;
                                        simpleSingleButtonDialog = null;

                                        moveToDashBoard();
                                    }
                                });
                            }
                        }
                    }

                }
            });

        } else {
            MyApplication.getApplication().getBookingList().remove(bookingId);
            Log.d("removing que booking", "removing que booking");

        }

    }

    private void moveToDashBoard() {

        MyApplication.getApplication().setBookingInProgress(false);
        Intent intent1 = new Intent(NewTripRequestActivity.this, DashboardActivity.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        NewTripRequestActivity.this.finish();
        startActivity(intent1);
    }

    @Override
    protected void onPause() {
        if (inCallWakeLock != null && inCallWakeLock.isHeld()) {
            inCallWakeLock.release();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if(!isOnresumeFirstTime) {
            if (inCallWakeLock != null) {
                inCallWakeLock.acquire();
            }
        }
        isOnresumeFirstTime = false;
        super.onResume();
    }
}
