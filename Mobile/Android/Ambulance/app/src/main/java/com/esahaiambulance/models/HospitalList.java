package com.esahaiambulance.models;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by ragamai on 3/10/16.
 */
public class HospitalList {
    /*
    "statusCode": "300",
	"statusMessage": "Hospital List fetched successfully"
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<HospitalDetails> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<HospitalDetails> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<HospitalDetails> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "HospitalList{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
    public class HospitalDetails implements Comparable<HospitalDetails>{
    /*
    {
		"group_id": "12",
		"group_name": "Citi Neuro Centre",
		"group_short_name": "Citi Neuro",
		"group_type": "2",
		"group_limit": "20",
		"group_longitude": "78.4358395",
		"group_address": "Rd Number 12, MLA Colony, Banjara Hills",
		"group_city": "Hyderabad",
		"pincode": "500034",
		"group_state": "Telangana",
		"is_active": "Active",
		"emergency_contact": "Kavitha",
		"emergency_number": "09494984747",
		"emergency_email": "info@citineuro.com",
		"updated_date": "0000-00-00 00:00:00",
		"created_date": "2016-10-03 09:50:37",
		"group_latitude": "17.4176973",
		"geoHashCode": "tepg0fg1r",
		"distance": "13.033",
		"duration": "32 mins"
	}
     */

        public String group_id;
        public String group_name;
        public String group_short_name;
        public String group_type;
        public String group_limit;
        public String group_longitude;
        public String group_latitude;
        public String group_address;
        public String group_city;
        public String pincode;
        public String group_state;
        public String is_active;
        public String emergency_contact;
        public String emergency_number;
        public String emergency_email;
        public String updated_date;
        public String created_date;
        public String geoHashCode;
        public String distance;
        public String duration;


        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getGroup_short_name() {
            return group_short_name;
        }

        public void setGroup_short_name(String group_short_name) {
            this.group_short_name = group_short_name;
        }

        public String getGroup_type() {
            return group_type;
        }

        public void setGroup_type(String group_type) {
            this.group_type = group_type;
        }

        public String getGroup_limit() {
            return group_limit;
        }

        public void setGroup_limit(String group_limit) {
            this.group_limit = group_limit;
        }

        public String getGroup_longitude() {
            return group_longitude;
        }

        public void setGroup_longitude(String group_longitude) {
            this.group_longitude = group_longitude;
        }

        public String getGroup_latitude() {
            return group_latitude;
        }

        public void setGroup_latitude(String group_latitude) {
            this.group_latitude = group_latitude;
        }

        public String getGroup_address() {
            return group_address;
        }

        public void setGroup_address(String group_address) {
            this.group_address = group_address;
        }

        public String getGroup_city() {
            return group_city;
        }

        public void setGroup_city(String group_city) {
            this.group_city = group_city;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getGroup_state() {
            return group_state;
        }

        public void setGroup_state(String group_state) {
            this.group_state = group_state;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getEmergency_contact() {
            return emergency_contact;
        }

        public void setEmergency_contact(String emergency_contact) {
            this.emergency_contact = emergency_contact;
        }

        public String getEmergency_number() {
            return emergency_number;
        }

        public void setEmergency_number(String emergency_number) {
            this.emergency_number = emergency_number;
        }

        public String getEmergency_email() {
            return emergency_email;
        }

        public void setEmergency_email(String emergency_email) {
            this.emergency_email = emergency_email;
        }

        public String getUpdated_date() {
            return updated_date;
        }

        public void setUpdated_date(String updated_date) {
            this.updated_date = updated_date;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getGeoHashCode() {
            return geoHashCode;
        }

        public void setGeoHashCode(String geoHashCode) {
            this.geoHashCode = geoHashCode;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        @Override
        public String toString() {
            return "HospitalDetails{" +
                    "group_id='" + group_id + '\'' +
                    ", group_name='" + group_name + '\'' +
                    ", group_short_name='" + group_short_name + '\'' +
                    ", group_type='" + group_type + '\'' +
                    ", group_limit='" + group_limit + '\'' +
                    ", group_longitude='" + group_longitude + '\'' +
                    ", group_latitude='" + group_latitude + '\'' +
                    ", group_address='" + group_address + '\'' +
                    ", group_city='" + group_city + '\'' +
                    ", pincode='" + pincode + '\'' +
                    ", group_state='" + group_state + '\'' +
                    ", is_active='" + is_active + '\'' +
                    ", emergency_contact='" + emergency_contact + '\'' +
                    ", emergency_number='" + emergency_number + '\'' +
                    ", emergency_email='" + emergency_email + '\'' +
                    ", updated_date='" + updated_date + '\'' +
                    ", created_date='" + created_date + '\'' +
                    ", geoHashCode='" + geoHashCode + '\'' +
                    ", distance='" + distance + '\'' +
                    ", duration='" + duration + '\'' +
                    '}';
        }


        @Override
        public int compareTo(HospitalDetails hospitalDetails) {
            return distance.toLowerCase().compareTo(hospitalDetails.getDistance().toLowerCase());
        }
    }
}
