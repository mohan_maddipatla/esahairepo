package com.esahaiambulance.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.activity.DashboardActivity;
import com.esahaiambulance.activity.DriverAuthenticationActivity;
import com.esahaiambulance.activity.LoginActivity;
import com.esahaiambulance.models.BookingDetails;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import ch.hsr.geohash.GeoHash;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 15/9/16.
 */

public class Common {


    static ProgressDialog pDialog;
    static Dialog alertDialog = null;
    public static final int ACC_REGISTER_NOTIFICATION_ID = 12334;

    public static void saveStringPref(Context context, String key, String value) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringPref(Context context, String key,
                                       String defaultValue) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);
        return sharedPref.getString(key, defaultValue);
    }

    public static void saveIntPref(Context context, String key, int value) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getIntPerf(Context context, String key,
                                 int defaultValue) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);
        return sharedPref.getInt(key, defaultValue);
    }

    public static void saveBooleanPref(Context context, String key, boolean value) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBooleanPerf(Context context, String key,
                                         boolean defaultValue) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);
        return sharedPref.getBoolean(key, defaultValue);
    }

    public static void deletePref(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);
        editor.commit();
    }

    public static boolean isNetworkAvailable(Context context) {
       /* ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isFailover())
            return false;
        else if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;*/


        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static Dialog displayAlertDialog(Context context, String message) {

        // to handle if context is not of activity
        try {
            if (context == null || ((Activity) context).isFinishing()) {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        final Dialog dialog = new Dialog(context);
        alertDialog = dialog;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.simple_single_button_alert);
        Button btnYes = (Button) dialog.findViewById(R.id.btnSubmit);

        TextView textStatus = (TextView) dialog.findViewById(R.id.txtMessage);
        textStatus.setText(message);

        //Grab the window of the dialog, and change the width
        Window window = dialog.getWindow();
        //This makes the dialog take up the full width
        window.setDimAmount(0.7f);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //myAlertListener.buttonClicked(0);


            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
                //myAlertListener.buttonClicked(0);
            }
        });

        if (!dialog.isShowing())
            dialog.show();

    return dialog;

    }

    public static void displayProgress(Context context) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.cancel();
            pDialog = null;
        }
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(true);
        pDialog.show();
    }

    public static void stopProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.cancel();
        }

    }

    /// user details
    public static String getAmbulanceNumber(Context context) {

        return getStringPref(context, Constants.ambulanceNumber, "");
    }

    public static String getAmbulanceId(Context context) {

        return getStringPref(context, Constants.ambId, "");
    }

    public static String getUserId(Context context) {

        return getStringPref(context, Constants.ambId, "");
    }

    public static String getAccessToken(Context context) {

        return getStringPref(context, Constants.accessToken, "");
    }

    // driver details
    public static String getDriverId(Context context) {

        return getStringPref(context, Constants.driverId, "");
    }

    public static Dialog simpleSingleButtonAlertDialog(final Context ctx, String message, final MyAlertListener myAlertListener) {

        if(ctx == null || ((Activity)ctx).isFinishing()){
            return null;
        }
        // custom dialog
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.simple_single_button_alert);
        Button btnYes = (Button) dialog.findViewById(R.id.btnSubmit);

        TextView textStatus = (TextView) dialog.findViewById(R.id.txtMessage);
        textStatus.setText(message);

        //Grab the window of the dialog, and change the width
        Window window = dialog.getWindow();
        //This makes the dialog take up the full width
        window.setDimAmount(0.7f);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                myAlertListener.buttonClicked(0);


            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
                myAlertListener.buttonClicked(0);
            }
        });

        if (!dialog.isShowing())
            dialog.show();
        return dialog;
    }

    public static void hidesimpleSingleButtonAlertDialog(Dialog pDialog ,Context context) {
        try {
            if (context != null && ((Activity) context).isFinishing()) {
                return;
            }
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
                pDialog = null;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Dialog simpleTwoButtonAlertDialog(final Context ctx, String message, final MyAlertListener myAlertListener) {
        // custom dialog
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.simple_alert_two_buttons);
        Button btnYes = (Button) dialog.findViewById(R.id.btnPossitive);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNegative);

        TextView textStatus = (TextView) dialog.findViewById(R.id.txtMsg);
        textStatus.setText(message);

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                myAlertListener.buttonClicked(0);


            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                myAlertListener.buttonClicked(1);
            }
        });
        if (!dialog.isShowing())
            dialog.show();

        return dialog;
    }

    public static void closeKeyBoard(Context context, View view) {
        InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public static int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.online_notification : R.mipmap.ic_launcher;
    }


    public static void showAccRegNotification(Context context) {
        String title = getStringPref(context, Constants.driverName, "");
        if (context == null || title == null) {
            return;
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(getNotificationIcon()).setContentTitle(
                title);
        mBuilder.setContentText(getStringPref(context, Constants.driverMobileNumber, ""));
        mBuilder.setOngoing(true);
        if (!(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)) {
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(
                    context.getResources(), R.mipmap.ic_launcher));
        }
        mBuilder.setColor(context.getResources().getColor(
                R.color.colorPrimary));
        // Creates an explicit intent for an Activity in your app

        Intent intent = new Intent(context.getApplicationContext(), DashboardActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);

        // The stack builder object will contain an artificial back stack for
        // the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        // TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        // stackBuilder.addParentStack(ResultActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        //resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      /*  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);*/

        /*PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0 *//* Request code *//*, pIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);*/
        mBuilder.setContentIntent(pIntent);
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(ACC_REGISTER_NOTIFICATION_ID, mBuilder.build());
    }

    public static void cancelAccRegNotification(Context ctx) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx
                .getSystemService(ns);
        nMgr.cancel(ACC_REGISTER_NOTIFICATION_ID);
    }

    /*
     * old encrypted
     */

/*
    public static String encrypt(String inputString) {

        String password = "@*%$!eSaHai";
        //  String password =  "@*%$!RetailerApp";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();

            byte[] salt = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};

            SecretKeyFactory factory =
                    SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

            //String str = new String(digest);
            KeySpec spec = new PBEKeySpec(Base64.encodeToString(digest, Base64.NO_WRAP).toCharArray(), salt, 1000, 256 + 128);
            SecretKey secretKey = factory.generateSecret(spec);
            //SecretKeySpec secretKeySpec = new SecretKeySpec(digest, "AES");
            byte[] data = secretKey.getEncoded();
            byte[] keyBytes = new byte[256 / 8];
            byte[] ivBytes = new byte[128 / 8];

            System.arraycopy(data, 0, keyBytes, 0, 256 / 8);
            System.arraycopy(data, 0, ivBytes, 0, 128 / 8);

            IvParameterSpec iv = new IvParameterSpec(ivBytes);
            SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(inputString.getBytes());

            return Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
*/
// new encrypt

    public static String encrypt( String stringToEncode) throws NullPointerException {
        String keyString = "@*%$!eSaHai";

        if (stringToEncode.length() == 0 || stringToEncode == null) {
            throw new NullPointerException("Please give text");
        }

        try {
            SecretKeySpec skeySpec = getKey(keyString);
            byte[] clearText = stringToEncode.getBytes("UTF8");

            // IMPORTANT TO GET SAME RESULTS ON iOS and ANDROID
            final byte[] iv = new byte[16];
            Arrays.fill(iv, (byte) 0x00);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            // Cipher is not thread safe
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);

            String encrypedValue = Base64.encodeToString(cipher.doFinal(clearText), Base64.DEFAULT);
            Log.d("jacek", "Encrypted: " + stringToEncode + " -> " + encrypedValue);
            return encrypedValue;

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return "";
    }


        /*
     * old decrypt
     */
/*
    public static String decrypt( String decrypt) {
        String password = "@*%$!eSaHai";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();

            byte[] salt = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};

            SecretKeyFactory factory =
                    SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            Log.d("====", Base64.encodeToString(digest, Base64.NO_WRAP));
            KeySpec spec = new PBEKeySpec(Base64.encodeToString(digest, Base64.NO_WRAP).toCharArray(), salt, 1000, 256 + 128);
            SecretKey secretKey = factory.generateSecret(spec);
            //SecretKeySpec secretKeySpec = new SecretKeySpec(digest, "AES");
            byte[] data = secretKey.getEncoded();
            byte[] keyBytes = new byte[256 / 8];
            byte[] ivBytes = new byte[128 / 8];

            System.arraycopy(data, 0, keyBytes, 0, 256 / 8);
            System.arraycopy(data, 0, ivBytes, 0, 128 / 8);
            IvParameterSpec iv = new IvParameterSpec(ivBytes);
            SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decode(decrypt, Base64.NO_WRAP));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }


    }
*/
    // new decrypt

    public static String decrypt( String text) throws NullPointerException {

        String password = "@*%$!eSaHai";

        if (password.length() == 0 || password == null) {
            throw new NullPointerException("Please give Password");
        }

        if (text.length() == 0 || text == null) {
            throw new NullPointerException("Please give text");
        }

        try {
            SecretKey key = getKey(password);

            // IMPORTANT TO GET SAME RESULTS ON iOS and ANDROID
            final byte[] iv = new byte[16];
            Arrays.fill(iv, (byte) 0x00);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            byte[] encrypedPwdBytes = Base64.decode(text, Base64.DEFAULT);
            // cipher is not thread safe
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec);
            byte[] decrypedValueBytes = (cipher.doFinal(encrypedPwdBytes));

            String decrypedValue = new String(decrypedValueBytes);
            Log.d("", "Decrypted: " + text + " -> " + decrypedValue);
            return decrypedValue;

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return "";
    }



    /**
     * Generates a SecretKeySpec for given password
     *
     * @param password
     * @return SecretKeySpec
     * @throws UnsupportedEncodingException
     */
    private static SecretKeySpec getKey(String password) throws UnsupportedEncodingException {

        // You can change it to 128 if you wish
        int keyLength = 256;
        byte[] keyBytes = new byte[keyLength / 8];
        // explicitly fill with zeros
        Arrays.fill(keyBytes, (byte) 0x0);

        // if password is shorter then key length, it will be zero-padded
        // to key length
        byte[] passwordBytes = password.getBytes("UTF-8");
        int length = passwordBytes.length < keyBytes.length ? passwordBytes.length : keyBytes.length;
        System.arraycopy(passwordBytes, 0, keyBytes, 0, length);
        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
        return key;
    }



    public static void doLogOut(Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                "Pref-Values", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static boolean checkLocationPermission(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean isLocationServiceEnabled(LocationManager lm) {

        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return gps_enabled || network_enabled;
    }


    /// update ambulance status to server


  /*  public static void updateAmbulanceStatus(HashMap<String, Object> json, final Context context, final boolean isShowProgressDialog) {

        json.put("ambulanceId", getAmbulanceId(context));
        json.put("driverId", getDriverId(context));
        json.put("ambulanceApp", true);

        if (Common.isNetworkAvailable(context)) {

            if (isShowProgressDialog) {
                Common.displayProgress(context);
            }
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<JsonElement> call = apiService.updateAmbulanceStatus(json, getUserId(context), getAccessToken(context));

            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    if (isShowProgressDialog) {
                        Common.stopProgressDialog();
                    }

                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            Log.d("ambulance details", "updated ambulance status");


                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(context);

                        } else {
                            Common.displayAlertDialog(context, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    if (isShowProgressDialog) {
                        Common.stopProgressDialog();
                    }
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    Common.displayAlertDialog(context, context.getString(R.string.networkError));
                }
            });
        } else {

            Common.displayAlertDialog(context, context.getString(R.string.offline));
        }

    }
*/
    public static void sendRegistrationToServer(final Context context) {


        Log.d("sendRegistration","sendRegistrationToServer");


        String userId = Common.getAmbulanceId(context);

        if (userId.isEmpty() ||
                Common.getBooleanPerf(context, Constants.isTokenSentToServer, false)) {

            return;
        }

        Log.d("sendRegistration","sendRegistrationToServer");
        String token = FirebaseInstanceId.getInstance().getToken();

        if (!userId.isEmpty()) {
            HashMap<String, Object> reqJson = new HashMap<>();
            reqJson.put("deviceToken", token);
            reqJson.put("ambulanceApp", true);


            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Log.d("sendRegistration", "req = " + Arrays.asList(reqJson));

            Call<JsonElement> call = apiService.updateDeviceToken(reqJson, Common.getUserId(context), Common.getAccessToken(context));
            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Log.d("sendRegistratio", "sendRegistrationToServer >> sent the  token(updated) **????###");
                    try {
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        if (context == null) {
                            Log.d("context", "context nulllllll");
                            return;
                        }
                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            Log.d("token sent", "token sent yessssssssssssssssssss");
                            Common.saveBooleanPref(context, Constants.isTokenSentToServer, true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    System.out.println("eroooooooorr" + throwable.toString());
                }
            });

        }

    }


    public static String getGeoHashCode(Location location) {

        GeoHash st = GeoHash.withCharacterPrecision(location.getLatitude(), location.getLongitude(), Constants.locationPrecisionNumber);
        return st.toBase32();
    }

    public static String getGeoHashCode(LatLng location) {

        GeoHash st = GeoHash.withCharacterPrecision(location.latitude, location.longitude, Constants.locationPrecisionNumber);
        return st.toBase32();
    }

    public static void increaseGcmHeartBeat(Context context){
        context.sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
        context.sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));
    }

    public static void saveBookingDetails(Context context, BookingDetails details){

        Log.d("saveBookingDetails","saveBookingDetails common"+details);
        if(details != null) {
            Gson gson = new Gson();
            String json = gson.toJson(details);
            saveStringPref(context, Constants.bookingDetailsPref, json);
        }
        else{
            saveStringPref(context, Constants.bookingDetailsPref, "");
        }
    }
    public static BookingDetails getBookingDetails(Context context){

        BookingDetails details = null;
        String json = getStringPref(context,Constants.bookingDetailsPref, "");
        if(!json.isEmpty()){

            Gson gson = new Gson();
            details    = gson.fromJson(json, BookingDetails.class);

        }
        Log.d("getBookingDetails","getBookingDetails common"+details);
        return details;
    }
}

