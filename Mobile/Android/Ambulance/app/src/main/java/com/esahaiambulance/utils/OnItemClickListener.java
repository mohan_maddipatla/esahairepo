package com.esahaiambulance.utils;

import android.view.View;

/**
 * Created by prashanth on 3/1/17.
 */

public interface OnItemClickListener {
    public void onClick(View view, int position);
}
