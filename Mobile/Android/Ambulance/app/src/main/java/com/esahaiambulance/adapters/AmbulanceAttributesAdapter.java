package com.esahaiambulance.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.activity.DriverAuthenticationActivity;
import com.esahaiambulance.models.AmbulanceAttributesModel;
import com.esahaiambulance.models.DriverDetails;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by prashanth on 19/10/16.
 */

public class AmbulanceAttributesAdapter extends RecyclerView.Adapter<AmbulanceAttributesAdapter.ViewHolder> {


    Context context;
    ArrayList<AmbulanceAttributesModel> arrayList;

    public AmbulanceAttributesAdapter(Context context, ArrayList<AmbulanceAttributesModel> arrayList) {

        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ambulance_attributes_item, parent, false);
        AmbulanceAttributesAdapter.ViewHolder viewHolder = new AmbulanceAttributesAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        AmbulanceAttributesModel item = arrayList.get(position);
        holder.txtAttributeName.setText(item.getAttributeName());
        holder.txtAttributeValue.setText(item.getAttributeDetails());

        if (position % 2 == 0) {
            holder.txtAttributeName.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGrayLight));
            holder.txtAttributeValue.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGrayLight));
        } else {
           // holder.rootView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            holder.txtAttributeName.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            holder.txtAttributeValue.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public AmbulanceAttributesModel getItem(int position) {
        return arrayList.get(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView txtAttributeName, txtAttributeValue;
        LinearLayout rootView;


        public ViewHolder(View view) {
            super(view);
            txtAttributeName = (TextView) view.findViewById(R.id.txtAttributeName);
            txtAttributeValue = (TextView) view.findViewById(R.id.txtAttributeValue);
            rootView = (LinearLayout) view.findViewById(R.id.rootView);

        }


    }

}
