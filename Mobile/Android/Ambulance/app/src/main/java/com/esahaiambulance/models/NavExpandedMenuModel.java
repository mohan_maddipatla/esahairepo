package com.esahaiambulance.models;

/**
 * Created by adheesh on 24/01/17.
 */

public class NavExpandedMenuModel {

    String iconName;
    int imageId;

    public NavExpandedMenuModel(String iconName,int imageId){

        this.iconName = iconName;
        this.imageId = imageId;
    }


    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }




}
