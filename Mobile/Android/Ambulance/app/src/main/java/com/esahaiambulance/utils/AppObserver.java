package com.esahaiambulance.utils;


public interface AppObserver {
	public void update(int event, Object arg);
}
