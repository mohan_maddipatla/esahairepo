package com.esahaiambulance.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esahaiambulance.R;
import com.esahaiambulance.adapters.TripListAdapter;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 13/10/16.
 */

public class DriverProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtTitle;
    EditText etDriverName, etDriverMobilerNumber, etDriverEmail, etDriverLicense;
    Button btnSubmit;
    CircleImageView profilePic;

    Context mContext;


   private final int REQUEST_CAMERA = 100, SELECT_FILE = 101;

    private final int MY_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1;

    private boolean askOptionsAfterPermissionGranted = false;
    private boolean isReadExternalStoragePermissionGranted = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_profile);
        mContext = this;
        initViews();
        setDetails();
        setEnableDisable(false);

        if (!checkExternalStoragePermission()) {

            //requestPermission();
        } else {
            isReadExternalStoragePermissionGranted = true;
        }
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       /* getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);*/


        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText(getString(R.string.driver_profile));

        ImageView img = (ImageView)toolbar.findViewById(R.id.img_toolbarimg);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        etDriverName = (EditText) findViewById(R.id.etDriverName);
        etDriverMobilerNumber = (EditText) findViewById(R.id.etDriverMobilerNumber);
        etDriverEmail = (EditText) findViewById(R.id.etDriverEmail);
        etDriverLicense = (EditText) findViewById(R.id.etDriverLicense);
        profilePic = (CircleImageView) findViewById(R.id.profilePic);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

       /* profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isReadExternalStoragePermissionGranted) {
                    selectImage();
                } else {
                    askOptionsAfterPermissionGranted = true;
                    requestPermission();
                }
            }
        });*/
        Picasso.with(this).load(Common.getStringPref(this, Constants.driverProfilePic, "")).into(profilePic);
    }

    private void setDetails() {
        etDriverName.setText(Common.getStringPref(mContext, Constants.driverName, ""));
        etDriverMobilerNumber.setText(Common.getStringPref(mContext, Constants.driverMobileNumber, ""));
        etDriverEmail.setText(Common.getStringPref(mContext, Constants.driverEmail, ""));
        etDriverLicense.setText(Common.getStringPref(mContext, Constants.driverLicenceNumber, ""));
    }

    private void setEnableDisable(boolean isEnable) {

        etDriverName.setEnabled(isEnable);
        etDriverMobilerNumber.setEnabled(isEnable);
        etDriverEmail.setEnabled(isEnable);
        etDriverLicense.setEnabled(isEnable);
        btnSubmit.setEnabled(isEnable);
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_driver_profile, menu);

        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_edit) {

            setEnableDisable(true);
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void submitForm() {

        if (!validateName()) {
            return;
        }
        if (!validateMobileNumber()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validateLicenceNumber()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }


        //Toast.makeText(getApplicationContext(), "Thank You!",
        //		Toast.LENGTH_SHORT).show();
        //  sendDetailsToServer();
    }

    private boolean validateName() {
        if (etDriverName.getText().toString().trim().isEmpty()) {
            etDriverName.setError("Enter Valid Name");
            requestFocus(etDriverName);
            return false;
        } else {
            etDriverName.setError(null);
        }

        return true;
    }

    private boolean validateMobileNumber() {
        if (etDriverMobilerNumber.getText().toString().trim().isEmpty()) {
            etDriverMobilerNumber.setError("Enter Valid Mobile Number");
            requestFocus(etDriverMobilerNumber);
            return false;
        } else {
            etDriverMobilerNumber.setError(null);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = etDriverEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            etDriverEmail.setError("Enter Valid Email");
            requestFocus(etDriverEmail);
            return false;
        } else {
            etDriverEmail.setError(null);
        }

        return true;
    }

    private boolean validateLicenceNumber() {
        if (etDriverLicense.getText().toString().trim().isEmpty()) {
            etDriverLicense.setError("Enter Valid Licence number");
            requestFocus(etDriverLicense);
            return false;
        } else {
            etDriverLicense.setError(null);
        }

        return true;
    }

    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public boolean checkExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    public void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(DriverProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

         //   Toast.makeText(mContext, "Read external storage permission required for profile pic upload ", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(DriverProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    isReadExternalStoragePermissionGranted = true;
                    if (askOptionsAfterPermissionGranted) {
                        askOptionsAfterPermissionGranted = false;
                        selectImage();
                    }
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        System.out.println("select image");
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);

            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            ;
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        // profilePic.setImageBitmap(getResizedBitmap(thumbnail));

        try {
            File file = new File(mContext.getCacheDir(), "profile_pic");
            file.createNewFile();

//Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            uploadFileToServer(file, thumbnail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



/*    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
//                File file = new File(data.getData().getPath());

                Uri tempUri = getImageUri(getApplicationContext(), bm);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File file = new File(getRealPathFromURI(tempUri));
                uploadFileToServer(file,bm);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       //profilePic.setImageBitmap(getResizedBitmap(bm));
    }*/
   /* @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                File file = new File(data.getData().getPath());
                uploadFileToServer(file, bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

       // profile_image.setImageBitmap(getResizedBitmap(bm));
    }*/

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        System.out.println("selectedImageUri = " + selectedImageUri);
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        // Get the cursor
        Cursor cursor = getContentResolver().query(selectedImageUri,
                filePathColumn, null, null, null);
        // Move to first row
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String imgPath1 = cursor.getString(columnIndex);
        System.out.println("imgPath = " + imgPath1);
        cursor.close();

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        // selectedBitmap = BitmapFactory.decodeFile(imgPath1);
        // System.out.println("selectedBitmap = " + selectedBitmap);
        try {
            BitmapFactory.decodeStream(
                    getContentResolver().openInputStream(selectedImageUri),
                    null, options);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        options.inSampleSize = calculateInSampleSize(options);
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(imgPath1, options);
       /* String fileNameSegments[] = imgPath1.split("/");
        String fileName = fileNameSegments[fileNameSegments.length - 1];
        String fileFormateSegments[] = fileName.split(".");
        if (fileFormateSegments.length > 1) {
            fileFormate = "."
                    + fileNameSegments[fileFormateSegments.length - 1];
            System.out.println("fileFormate = " + fileFormate);
        }*/
        //  return bm;
        uploadFileToServer(new File(imgPath1), bm);
    }


    private void uploadFileToServer(File file, final Bitmap bitmap) {

        if (Common.isNetworkAvailable(this)) {

            Common.displayProgress(this);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

            //   RequestBody userId = RequestBody.create(MediaType.parse("text/plain"),preferences.getCustomerId());
            //   RequestBody acceToken = RequestBody.create(MediaType.parse("text/plain"), preferences.getAccessToken());

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            HashMap<String, Object> request = new HashMap<>();
            request.put("customerApp", false);
            request.put("driverId", Common.getDriverId(this));
            Call<JsonElement> call = apiService.uploadProfilePic(request, filePart, Common.getAmbulanceId(this), Common.getAccessToken(this));

            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {
                        Common.stopProgressDialog();
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {
                            Common.saveStringPref(mContext, Constants.driverProfilePic, response.getString("imageUrl"));
                            profilePic.setImageBitmap(getResizedBitmap(bitmap));
                        } else {
                            Common.displayAlertDialog(mContext, response.getString("statusMessage"));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :uploadFileToServer: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                }
            });
        } else {
            Common.displayAlertDialog(mContext, mContext.getString(R.string.offline));
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm) {
        int newWidth = 100;
        int newHeight = 100;
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String path = cursor.getString(idx);
        try {

            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    private int calculateInSampleSize(BitmapFactory.Options options) {
        // Raw height and width of image
        int reqWidth = 100;
        int reqHeight = 100;
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
