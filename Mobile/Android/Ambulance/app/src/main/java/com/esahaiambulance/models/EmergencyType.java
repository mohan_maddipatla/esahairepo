package com.esahaiambulance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by prashanth on 4/10/16.
 */

public class EmergencyType {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type_name")
    @Expose
    private String typeName;


    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName The type_name
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


}
