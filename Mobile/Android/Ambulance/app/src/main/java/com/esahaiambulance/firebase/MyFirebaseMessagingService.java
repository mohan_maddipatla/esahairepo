package com.esahaiambulance.firebase;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.crittercism.internal.m;
import com.esahaiambulance.R;
import com.esahaiambulance.activity.DashboardActivity;
import com.esahaiambulance.activity.DriverListActivity;
import com.esahaiambulance.activity.MyApplication;
import com.esahaiambulance.activity.NewTripRequestActivity;
import com.esahaiambulance.activity.TripProgressService;
import com.esahaiambulance.db.DatabaseHelper;
import com.esahaiambulance.models.AmbulanceAttributesModel;
import com.esahaiambulance.models.BookingDetails;
import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.models.EmergencyType;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 29/9/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    //pn types

    final private String newBooking = "new_booking";


    public static final String cancelBooking = "cancel_booking";

    public static final String cancelBookingCommandCenter = "command_centre_cancelled_booking";

    // admin trasnferred booking to other ambulance
    public static final String transferBooking = "transfer_booking";

    final private String driverLogout = "driver_logout";

    // new booking which is transferred by other admin // need to accept it
    final private String transferredNewBooking = "transferred_new_booking";

    // new booking which is booked by command center
    final public static String bookingFromCommandCentre = "booking_from_commandCentre";

    // stoping ringtone when other ambulance accepted
    final private String otherAmbulanceAccepted = "other_accepted";

    final private String ambulanceTransferredBooking = "ambulance_transferred_booking";


    final private String ambulanceProfileUpdate = "update_ambulance_profile";

    final private String ambulanceLogout = "ambulance_logout";



    MyApplication myApplication;
    private Context mContext;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        myApplication = (MyApplication) getApplicationContext();
        Log.d("myApplication 88", "myApplication ***" + myApplication);

        mContext = this;
        // Check if message contains a data payload.
        if (remoteMessage.getFrom().startsWith("/topics/")) {
            if (remoteMessage.getFrom().startsWith("/topics/" + MyFirebaseInstanceIDService.TOPICS[0])) {

                Common.saveBooleanPref(this,Constants.isFaqsDownloaded,false);

                MyApplication.getApplication().notifyObservers(MyApplication.FAQS_UPDATE,null);
            }
        } else {
            try {


                if (remoteMessage.getData().size() > 0) {

                    Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                    //sendNotification( remoteMessage.getData().toString());
                    Map<String, String> data = remoteMessage.getData();
                    String type = data.get("type");

                    Log.d(TAG, "type type: " + type);

                    Log.d(TAG, "application flag: " + MyApplication.getApplication().getBookingInProgress());

                    Log.d("booking size **", "size **" + MyApplication.getApplication().getBookingList().size());


                    /// booking related pns.......
                    if (!Common.getUserId(this).isEmpty() && Common.getBooleanPerf(this, Constants.isDriverLoggedIn, false)) {
                        if (type.equalsIgnoreCase(newBooking) || type.equalsIgnoreCase(ambulanceTransferredBooking)) {


                            BookingDetails bookingDetails = parseBookingDetails(type, data);

                            MyApplication.getApplication().trackEvent("Bookings", "New Booking", "" + bookingDetails.getBookingId(), false);

                            Log.d("getBookingInProgress", "getBookingInProgress " + MyApplication.getApplication().getBookingInProgress());
                            if (!MyApplication.getApplication().getBookingInProgress()) {
                                //  MyApplication.isBookingInProgress = true;
                                MyApplication.getApplication().setBookingInProgress(true);
                                //  Log.d(TAG, "application flag 2: " + MyApplication.isBookingInProgress);


                                if (isAppInForeground(this) && MyApplication.getApplication().getNumberOfObserver() > 0) {
                                    MyApplication.getApplication().startBooking(bookingDetails);
                                } else {
                                    Intent intent = new Intent(this, NewTripRequestActivity.class);
                                    intent.putExtra(Constants.bookingDetails, bookingDetails);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }

                            } else {

                                if (bookingDetails != null) {
                                    MyApplication.getApplication().getBookingList().put(bookingDetails.getBookingId(), bookingDetails);

                                    Log.d("booking size **", "size **" + MyApplication.getApplication().getBookingList().size());
                                }
                            }

                        } else if ((type.equalsIgnoreCase(cancelBooking) || type.equalsIgnoreCase(cancelBookingCommandCenter)) && MyApplication.getApplication().getBookingInProgress()) {

                            Log.d("boking candla ", "**** canel booking**" + MyApplication.getApplication().getBookingInProgress());

                            String bookingId = "";
                            JSONObject json = new JSONObject(data.get("title").trim());
                            bookingId = json.getString("booking_id");
                            Log.d("bookingId", "bookingId = " + bookingId + " pra");
                            TripProgressService.IS_SERVICE_RUNNING = false;
                            TripProgressService.bookingDetails = null;
                            Common.saveBookingDetails(this, null);
                            if (isAppInForeground(this) && MyApplication.getApplication().getNumberOfObserver() > 0) {

                                if (type.equalsIgnoreCase(cancelBooking)) {
                                    MyApplication.getApplication().notifyObservers(MyApplication.BOOKING_CANCELLED, bookingId);
                                } else if (type.equalsIgnoreCase(cancelBookingCommandCenter)) {

                                    MyApplication.getApplication().notifyObservers(MyApplication.BOOKING_CANCELLED_COMMAND_CENTER, bookingId);
                                }
                            } else {

                                if (type.equalsIgnoreCase(cancelBooking)) {
                                    MyApplication.getApplication().notifyObservers(MyApplication.BOOKING_CANCELLED, bookingId);
                                } else if (type.equalsIgnoreCase(cancelBookingCommandCenter)) {

                                    MyApplication.getApplication().notifyObservers(MyApplication.BOOKING_CANCELLED_COMMAND_CENTER, bookingId);
                                }
                                Intent intent = new Intent(this, DashboardActivity.class);
                                intent.putExtra("cancelBooking", type);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                        } else if (type.equalsIgnoreCase(transferBooking) && MyApplication.getApplication().getBookingInProgress()) {


                            Common.saveBookingDetails(this, null);
                            TripProgressService.IS_SERVICE_RUNNING = false;
                            TripProgressService.bookingDetails = null;
                            if (isAppInForeground(this) && MyApplication.getApplication().getNumberOfObserver() > 0) {
                                MyApplication.getApplication().notifyObservers(MyApplication.BOOKING_TRANSFERRED, "");
                            } else {
                                MyApplication.getApplication().notifyObservers(MyApplication.BOOKING_TRANSFERRED, "");
                                Intent intent = new Intent(this, DashboardActivity.class);
                                intent.putExtra("cancelBooking", type);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }


                        } else if (type.equalsIgnoreCase(driverLogout)) {
                            MyApplication.getApplication().trackEvent("Driver Logout", "From Admin", "" + "Log out", false);

                            clearDriverDetails(MyFirebaseMessagingService.this);
                            if (isAppInForeground(MyFirebaseMessagingService.this)) {
                                Intent intent = new Intent(MyFirebaseMessagingService.this, DriverListActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        } else if (type.equalsIgnoreCase(transferredNewBooking) || type.equalsIgnoreCase(bookingFromCommandCentre)) {
                            Log.d("transferredNewBooking", "transferredNewBooking");

                            BookingDetails bookingDetails = parseBookingDetails(type, data);

                            MyApplication.getApplication().trackEvent("Bookings", "Transferred Booking", "" + bookingDetails.getBookingId(), false);


                            if (!MyApplication.getApplication().getBookingInProgress()) {
                                //  MyApplication.isBookingInProgress = true;
                                MyApplication.getApplication().setBookingInProgress(true);
                                //  Log.d(TAG, "application flag 2: " + MyApplication.isBookingInProgress);
                                if (isAppInForeground(this) && MyApplication.getApplication().getNumberOfObserver() > 0) {
                                    if (type.equalsIgnoreCase(transferredNewBooking)) {
                                        MyApplication.getApplication().notifyObservers(MyApplication.TRANSFERRED_NEW_BOOKING, bookingDetails);
                                    } else if (type.equalsIgnoreCase(bookingFromCommandCentre)) {
                                        MyApplication.getApplication().notifyObservers(MyApplication.COMMAND_CENTER_NEW_BOOKING, bookingDetails);
                                    }
                                } else {

                                    Intent intent = new Intent(this, DashboardActivity.class);
                                    intent.putExtra(Constants.bookingDetails, bookingDetails);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    if (type.equalsIgnoreCase(transferredNewBooking)) {
                                        intent.putExtra("typeFromNewTripScreen", MyApplication.TRANSFERRED_NEW_BOOKING);
                                    } else {
                                        intent.putExtra("typeFromNewTripScreen", MyApplication.COMMAND_CENTER_NEW_BOOKING);
                                    }
                                    startActivity(intent);
                                }


                            } else {

                                if (bookingDetails != null) {
                                    MyApplication.getApplication().getBookingList().put(bookingDetails.getBookingId(), bookingDetails);

                                    Log.d("booking size **", "size **" + MyApplication.getApplication().getBookingList().size());
                                    if (type.equalsIgnoreCase(transferredNewBooking)) {
                                        MyApplication.getApplication().notifyObservers(MyApplication.TRANSFERRED_NEW_BOOKING, bookingDetails);
                                    } else if (type.equalsIgnoreCase(bookingFromCommandCentre)) {
                                        MyApplication.getApplication().notifyObservers(MyApplication.COMMAND_CENTER_NEW_BOOKING, bookingDetails);
                                    }
                                }
                            }
                        } else if (type.equalsIgnoreCase(otherAmbulanceAccepted)) {

                            Log.d("otherAmbulanceAccepted", "otherAmbulanceAccepted");
                            JSONObject jsonObject = new JSONObject(data.get("title").trim());
                            MyApplication.getApplication().trackEvent("Bookings", "Other ambulance accepted", "" + jsonObject.get("booking_id"), false);
                            MyApplication.getApplication().notifyObservers(MyApplication.OTHER_AMBULANCE_ACCEPTED, jsonObject.get("booking_id"));

                        }
                    }
                    // other update pns
                    if (!Common.getUserId(this).isEmpty()) {
                        if (type.equalsIgnoreCase(ambulanceProfileUpdate)) {

                            getAmbulanceProfile();
                        }
                        if (type.equalsIgnoreCase(ambulanceLogout)) {

                            Log.d("ambulanceLogout","ambulanceLogout ***");
                            if(isAppInForeground(mContext)){
                                Common.doLogOut(mContext);
                            }else{

                                SharedPreferences sharedPref = mContext.getSharedPreferences(
                                        "Pref-Values", Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.clear();
                                editor.commit();
                            }

                        }
                    }

                }

                // Check if message contains a notification payload.
                if (remoteMessage.getNotification() != null) {
                    Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
                    sendNotification(remoteMessage.getNotification().getBody());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("FCM Message")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void clearDriverDetails(Context mContext) {
        try {

            // making driver log out
            Common.saveBooleanPref(mContext, Constants.isDriverLoggedIn, false);


            Common.saveStringPref(mContext, Constants.driverId, "");

            Common.saveStringPref(mContext, Constants.driverGroupId, "");
            Common.saveStringPref(mContext, Constants.driverName, "");
            Common.saveStringPref(mContext, Constants.driverMobileNumber, "");
            Common.saveStringPref(mContext, Constants.driverEmail, "");
            Common.saveStringPref(mContext, Constants.driverLicenceNumber, "");

            Common.saveStringPref(mContext, Constants.driverProfilePic, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isAppInForeground(Context context) {
        Log.d("isAppInForeground", "isAppInForeground");
        List<ActivityManager.RunningTaskInfo> task =
                ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
                        .getRunningTasks(1);
        if (task.isEmpty()) {
            return false;
        }
        return task
                .get(0)
                .topActivity
                .getPackageName()
                .equalsIgnoreCase(context.getPackageName());
    }

    private BookingDetails parseBookingDetails(String type, Map<String, String> data) {

        BookingDetails bookingDetails = null;
        try {

            JSONObject jsonObject = new JSONObject(data.get("title").trim());
            Type listType = new TypeToken<BookingDetails>() {
            }.getType();
            Gson gson = new Gson();
            bookingDetails = gson.fromJson("" + jsonObject, listType);
            bookingDetails.setBookingReceivedTime(Calendar.getInstance().getTimeInMillis());
            bookingDetails.setBookingType(type);
            MyApplication.getApplication().trackEvent("Bookings", "Transferred Booking", "" + bookingDetails.getBookingId(), false);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bookingDetails;
    }


    private void getAmbulanceProfile() {

        if (Common.isNetworkAvailable(this)) {

            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceApp", true);
            //map.put("customer_id",Common.get);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call<JsonElement> call2 = apiService.getAmbulanceProfile(map, Common.getUserId(mContext), Common.getAccessToken(mContext));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {
                        Common.stopProgressDialog();

                        Log.d("getAmbulanceProfile", "getAmbulanceProfile = " + serverResponse.body());
                        System.out.print("getAmbulanceProfile =" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            JSONObject jsonAmbulanceDetails = response.getJSONObject("responseData");

                            Common.saveStringPref(mContext, Constants.ambulanceNumber, jsonAmbulanceDetails.getString("ambulance_number"));
                            Common.saveStringPref(mContext, Constants.ambGroupId, jsonAmbulanceDetails.getString("group_id"));
                            Common.saveStringPref(mContext, Constants.ambGroupNmae, jsonAmbulanceDetails.getString("group_name"));

                            Common.saveStringPref(mContext, Constants.ambProfilePic, jsonAmbulanceDetails.getString("file_url"));
                            Common.saveStringPref(mContext, Constants.ambMobileNumber, jsonAmbulanceDetails.getString("ambulance_mobile"));
                            Common.saveStringPref(mContext, Constants.accessToken, jsonAmbulanceDetails.getString("token"));
                            Common.saveStringPref(mContext, Constants.emergencyType, jsonAmbulanceDetails.getString("emergency_type"));
                            Common.saveStringPref(mContext, Constants.vehicleType, jsonAmbulanceDetails.getString("vehicle_type"));


                            //Common.saveStringPref(mContext, Constants.groupName, jsonAmbulanceDetails.getString("group_name"));


                            Common.saveStringPref(mContext, Constants.ambHospitalNumber, jsonAmbulanceDetails.getString("group_mobile_number"));

                            JSONArray arraysList = jsonAmbulanceDetails.getJSONArray("ambulance_attributes");
                            DatabaseHelper dbHelper = DatabaseHelper.getInstance(mContext);
                            dbHelper.clearAttributes();
                            if (arraysList.length() > 0) {


                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<AmbulanceAttributesModel>>() {
                                }.getType();
                                ArrayList<AmbulanceAttributesModel> arrayList = gson.fromJson("" + arraysList, listType);


                                for (AmbulanceAttributesModel item : arrayList) {

                                    dbHelper.insertAmbulanceAttributes(item.getAttributeName(), item.getAttributeDetails());
                                }
                            }
                            JSONArray arraysListEmergencyTypes = jsonAmbulanceDetails.getJSONArray("emergency_types");
                            dbHelper.clearEmergencyTypes();
                            if (arraysListEmergencyTypes.length() > 0) {

                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<EmergencyType>>() {
                                }.getType();
                                ArrayList<EmergencyType> arrayList = gson.fromJson("" + arraysListEmergencyTypes, listType);


                                for (EmergencyType item : arrayList) {

                                    dbHelper.insertEmergencyType(item.getTypeName(), item.getId());
                                }

                            }

                            MyApplication.getApplication().notifyObservers(MyApplication.AMBULANCE_PROFILE_UPDATE, null);

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(MyFirebaseMessagingService.this);

                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    Common.stopProgressDialog();
                    Log.d("", "getAmbulanceProfile.0 :getAmbulanceProfile: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", "getAmbulanceProfile" + throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                }
            });
        } else {

        }
    }
}
