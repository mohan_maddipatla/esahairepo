package com.esahaiambulance.activity;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ahmedjazzar.rosetta.LanguageSwitcher;
import com.crittercism.app.Crittercism;
import com.esahaiambulance.R;
import com.esahaiambulance.models.BookingDetails;
import com.esahaiambulance.utils.AnalyticsTrackers;
import com.esahaiambulance.utils.AppObserver;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Retrofit;

/**
 * Created by prashanth on 26/9/16.
 */

public class MyApplication extends Application{

    private  List<AppObserver> mObservers = new ArrayList<>();
    private static MyApplication myApplication;

    private  boolean isBookingInProgress = false;

    public static final int NEW_BOOKING = 1;
    public static final int BOOKING_CANCELLED = 2;

    // this indicates admin as trasnferred  our booking to other ambulance
    public static final int BOOKING_TRANSFERRED = 3;

    // this indication admin as given  booking to us from other ambulance
    public static final int TRANSFERRED_NEW_BOOKING = 4;

    // this indicates new booking from command center
    public static final int COMMAND_CENTER_NEW_BOOKING = 4;

    public static final int OTHER_AMBULANCE_ACCEPTED = 5;

    public static final int BOOKING_CANCELLED_COMMAND_CENTER = 6;

    public static final int AMBULANCE_PROFILE_UPDATE = 7;
    public static final int FAQS_UPDATE = 8;


    private LinkedHashMap<String,BookingDetails> bookingsList = new LinkedHashMap<>();

    private Tracker mTracker;

    private Locale firstLaunchLocale;
    private HashSet<Locale> supportedLocales;
    public  LanguageSwitcher languageSwitcher;

    @Override
    public void onCreate() {
        super.onCreate();

        init();
        myApplication = this;
        Crittercism.initialize(getApplicationContext(), "bbc8d471590142508806fc12a552aad200555300");
       // FirebaseAnalytics mFirebaseAnalytics  = FirebaseAnalytics.getInstance(this);
        if(ApiClient.BASE_URL.equalsIgnoreCase(ApiClient.PROD_URL_FOR_CHECK)) {
            Log.d("PROD_URL_FOR_CHECK","url matched");
            AnalyticsTrackers.initialize(this);
            AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        }


    }

    public static MyApplication getApplication(){
        return myApplication;
    }
    public void notifyObservers(int event, Object arg) {
        for (int i = 0; i < mObservers.size(); i++) {
            Log.i("", "*******************  loop  notifyObservers( count = "+i+"event = " + event
                    );
            mObservers.get(i).update(event, arg);
           // break;
        }
    }

    public synchronized void addObserver(AppObserver obs) {
        Log.i("", "addObserver(" + obs + ")");
        if (mObservers.indexOf(obs) < 0) {
            Log.i("", " inside addObserver(" + obs + ")");
            mObservers.add(obs);
        }
    }

    /**
     * When an observer wants to unregister to stop receiving change
     * notifications, it calls here.
     */
    public synchronized void deleteObserver(AppObserver obs) {
        Log.i("", "deleteObserver(" + obs + ")");
        mObservers.remove(obs);
    }
    public synchronized int getNumberOfObserver() {

        Log.d("getNumberOfObserver","getNumberOfObserver = "+mObservers.size());
        return mObservers.size();
    }

    synchronized public void setBookingInProgress(boolean value){
        isBookingInProgress = value;
    }

    synchronized public boolean getBookingInProgress(){
       return isBookingInProgress;
    }

   synchronized public LinkedHashMap<String,BookingDetails> getBookingList(){

        return bookingsList;

    }


    public void startBooking(BookingDetails bookingDetails){

        notifyObservers(1,bookingDetails);
    }


    // analytics

    //Google analytics
    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    public void trackScreenView(String screenName) {

        if(!ApiClient.BASE_URL.equalsIgnoreCase(ApiClient.PROD_URL_FOR_CHECK)){

            return;
        }
        //clie.baseUrl()
        Tracker t = getGoogleAnalyticsTracker();
        //Tracker t2 = GoogleAnalytics.getInstance(this).newTracker("UA-69072164-1");
        // Set screen name.
        t.setScreenName(screenName);
        //String dimensionValue = "B081595";
		/*String dimensionValue = "SOME_DIMENSION_VALUE";
		tracker.set(Fields.customDimension(1), dimensionValue);*/
		/*NSString *dimensionValue = @"SOME_DIMENSION_VALUE";
		[tracker set:[GAIFields customDimensionForIndex:1] value:dimensionValue];*/
        //t.setClientId("B081595");
        //Log.d("trackScreenView getTrackerId",""+dbHelper.getTrackerId());
        // t.set("&uid", Common.getRetailerNumber(context));
        t.set("&cd1", Common.getAmbulanceNumber(this));

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     */
    public void trackException(Exception e) {
        if(!ApiClient.BASE_URL.equalsIgnoreCase(ApiClient.PROD_URL_FOR_CHECK)){

            return;
        }
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     */
    public void trackEvent(String category, String action, String label,boolean isNonInteraction) {
        if(!ApiClient.BASE_URL.equalsIgnoreCase(ApiClient.PROD_URL_FOR_CHECK)){

            return;
        }
        Tracker t = getGoogleAnalyticsTracker();
        t.set("&cd1",Common.getAmbulanceNumber(this));
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).setNonInteraction(isNonInteraction).build());
    }
    public void trackEvent(String category, String action, String label) {
        if(!ApiClient.BASE_URL.equalsIgnoreCase(ApiClient.PROD_URL_FOR_CHECK)){

            return;
        }
        trackEvent(category,action,label,true);
    }


    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.app_tracker);
        }
        return mTracker;
    }



    private void init(){
        manualSupportedLocales();

        languageSwitcher = new LanguageSwitcher(this, firstLaunchLocale);
        languageSwitcher.setSupportedLocales(supportedLocales);
    }
    private void manualSupportedLocales() {
        // This is the locale that you wanna your app to launch with.
        firstLaunchLocale = new Locale("en");

        Locale telugu = new Locale("te");
        // You can use a HashSet<String> instead and call 'setSupportedStringLocales()' :)
        supportedLocales = new HashSet<>();
        supportedLocales.add(telugu);
        supportedLocales.add(firstLaunchLocale);
    }

    // clear cache data
    public void clearApplicationData() {
      /*  SharedPreferences sharedPref = getSharedPreferences("Pref-Values",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
        SharedPreferences sharedPrefPort = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor1 = sharedPrefPort.edit();
        editor1.clear();
        editor1.commit();*/
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG",
                            "**************** File /data/data/APP_PACKAGE/" + s
                                    + " DELETED *******************");
                }
            }
        }
    }
    public  boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
}
