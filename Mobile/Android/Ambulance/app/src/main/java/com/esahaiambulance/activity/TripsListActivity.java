package com.esahaiambulance.activity;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.adapters.TripListAdapter;
import com.esahaiambulance.fragments.DateRangePickerFragment;
import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.models.TripDetails;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.utils.MyAlertListener;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 20/9/16.
 */

public class TripsListActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtTitle;
    RecyclerView recyclerView;
    TripListAdapter adapter;

    Context mContext;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips_list);
        initViews();
        mContext = this;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        adapter = new TripListAdapter(mContext, new ArrayList<TripDetails>());
        recyclerView.setAdapter(adapter);

        getTripsList(getCurrentDate(), getTomorrowDate());
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
       setSupportActionBar(toolbar);
     /*    ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);*/


        int padding = getResources().getDimensionPixelSize(R.dimen.views_gap);

        toolbar.setContentInsetsAbsolute(0, 0);


        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText(getString(R.string.bookings_list));

        recyclerView = (RecyclerView) findViewById(R.id.reclyerView);
        ImageView img = (ImageView)toolbar.findViewById(R.id.img_toolbarimg);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
      /*  if (id == R.id.action_date) {

            showDataPicker();
            return true;
        }*/
        return super.onOptionsItemSelected(menuItem);
    }

    private void getTripsList(String startDate, String endDate) {

        Log.d("startDate", "startDate = " + startDate);
        Log.d("endDate", "endDate = " + endDate);
        if (Common.isNetworkAvailable(this)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulance_id", Common.getAmbulanceId(this));
            map.put("ambulanceApp", true);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            map.put("driver_id", Common.getDriverId(this));

            Common.displayProgress(this);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            //Log.d("response", "user id " + Common.getUserId(this));
            //  Log.d("response", "access token " + Common.getAccessToken(this));

            Call<JsonElement> call2 = apiService.getTripList(map, Common.getUserId(mContext), Common.getAccessToken(mContext));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            JSONArray arrayTripsList = response.getJSONArray("responseData");
                            if (arrayTripsList.length() > 0) {
                               /* for (int i = 0; i < arrayDriversList.length(); i++) {
                                    JSONObject jsonDriver = arrayDriversList.getJSONObject(i);

                                }*/

                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<TripDetails>>() {
                                }.getType();

                                ArrayList<TripDetails> arrayList = gson.fromJson("" + arrayTripsList, listType);

                                adapter.changeList(arrayList);
                            }

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(TripsListActivity.this);

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase("411")) {
                            Common.simpleSingleButtonAlertDialog(TripsListActivity.this, getString(R.string.no_trips), new MyAlertListener() {
                                @Override
                                public void buttonClicked(int type) {

                                    TripsListActivity.this.finish();
                                }
                            });

                        } else {
                            Common.displayAlertDialog(TripsListActivity.this, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                    Common.displayAlertDialog(TripsListActivity.this, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }

    public String getCurrentDate() {
        DateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        return format.format(Calendar.getInstance().getTimeInMillis());
    }

    public String getTomorrowDate() {
        DateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        return format.format(Calendar.getInstance().getTimeInMillis() + 24 * 60 * 60 * 1000);
    }

    private void showDataPicker() {


        DateRangePickerFragment picker = DateRangePickerFragment.newInstance(new DateRangePickerFragment.OnDateRangeSelectedListener() {
            @Override
            public void onDateRangeSelected(int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear) {

                // String startDate = "",endDate="";
                DateFormat format = new SimpleDateFormat(
                        "yyyy-MM-dd", Locale.getDefault());

                Calendar startDate = Calendar.getInstance();
                startDate.setTimeInMillis(0);
                startDate.set(startYear, startMonth, startDay);

                Calendar endDate = Calendar.getInstance();
                endDate.setTimeInMillis(0);
                endDate.set(endYear, endMonth, endDay);
                if (startDay == endDay && startMonth == endMonth && startYear == endYear) {

                    Log.d("is equal", "equal ****");
                    endDate.setTimeInMillis(endDate.getTimeInMillis() + 24 * 60 * 60 * 1000);
                }
                getTripsList(format.format(startDate.getTimeInMillis()), format.format(endDate.getTimeInMillis()));
            }
        }, false);

        picker.show(getSupportFragmentManager(), "djfa");
    }
}
