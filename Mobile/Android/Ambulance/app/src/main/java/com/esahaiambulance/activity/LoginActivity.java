package com.esahaiambulance.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.esahaiambulance.R;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.utils.PermissionUtils;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText etMobileNumber;
    Button btnSubmit;
    Context mContext;
    private final int MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        initViews();

        if (!checkRequiredPermissions()) {
            requestPermissions();
        }
    }

    private void initViews() {

        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setEnabled(false);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.getApplication().trackEvent("Login","Login button press",""+etMobileNumber.getText().toString());
                sendNumberToServer();
            }
        });

        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etMobileNumber.getText().toString().length() == 10) {
                    Common.closeKeyBoard(mContext, etMobileNumber);
                    btnSubmit.setEnabled(true);
                } else {
                    btnSubmit.setEnabled(false);
                }
            }
        });
        etMobileNumber
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                               @Override
                                               public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                                                   boolean handled = false;
                                                   if (actionId == EditorInfo.IME_ACTION_DONE) {
                                                       Common.closeKeyBoard(mContext, etMobileNumber);
                    /* handle action here */
                                                       sendNumberToServer();
                                                       handled = true;
                                                   }
                                                   return handled;
                                               }
                                           }
                );

        Log.d("token:","token: "+ FirebaseInstanceId.getInstance().getToken());
    }

    private void sendNumberToServer() {
        if (etMobileNumber.getText().toString().length() != 10) {
            etMobileNumber.setError(getString(R.string.validMobileNumber));
            return;
        }
        if (!checkRequiredPermissions()) {
            requestPermissions();
            return;
        }
       /* Intent intent = new Intent(LoginActivity.this, MobileVerificationActivity.class);
        intent.putExtra("mobileNumber", etMobileNumber.getText().toString());
        intent.putExtra("otp", "123456");
        startActivity(intent);
        finish();*/
        if (Common.isNetworkAvailable(this)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceMobile", Common.encrypt(etMobileNumber.getText().toString()));
            map.put("deviceIMEI", getImeiId());
            map.put("fromApp", "true");

            Log.d("mobile", "after encrypt mobile" + Common.encrypt(etMobileNumber.getText().toString()));
            Common.displayProgress(this);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<JsonElement> call2 = apiService.generateAmbulanceOtp(map);
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {
                            MyApplication.getApplication().trackEvent("Login","Login success",""+etMobileNumber.getText().toString(),false);
                            Intent intent = new Intent(LoginActivity.this, MobileVerificationActivity.class);
                            intent.putExtra("mobileNumber", etMobileNumber.getText().toString());
                            intent.putExtra("otp", response.getString("otp"));
                            startActivity(intent);
                            finish();
                        } else {
                            Common.displayAlertDialog(LoginActivity.this, response.getString(Constants.statusMessage));
                            MyApplication.getApplication().trackEvent("Login","Login fail",""+etMobileNumber.getText().toString(),false);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    Common.displayAlertDialog(LoginActivity.this, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }

    }

    public boolean checkRequiredPermissions() {
        int permissionCheck1 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        int permissionCheck2 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        return (permissionCheck1 == PackageManager.PERMISSION_GRANTED
                && permissionCheck2 == PackageManager.PERMISSION_GRANTED);
    }

    public void requestPermissions() {

     /*   if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {

            Toast.makeText(this, "Read phonse staus permission required to read IMEI", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE);
        }*/
        ActivityCompat.requestPermissions(this, new String[] {
                android.Manifest.permission.CALL_PHONE,
                android.Manifest.permission.READ_PHONE_STATE
        }, MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE) {
            // Enable the My Location button if the permission has been granted.
           /* if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.READ_PHONE_STATE)) {

            } else {
            }*/

        }
    }

    private String getImeiId() {
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        String imei = tm.getDeviceId();
        Log.d("imei", imei);
        return imei;
    }



}
