package com.esahaiambulance.activity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.esahaiambulance.R;
import com.esahaiambulance.models.BookingDetails;
import com.esahaiambulance.utils.AppObserver;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.utils.SendPushNotification;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.AlarmManager.ELAPSED_REALTIME;
import static android.os.SystemClock.elapsedRealtime;

/**
 * Created by prashanth on 20/10/16.
 */
@SuppressWarnings({"MissingPermission"})
public class TripProgressService extends Service implements LocationListener, AppObserver {


    private static final String LOG_TAG = "ForegroundService";

    public static final int NOTIFICATION_ID = 123;

    // cases:
    public static String STARTFOREGROUND_ACTION = "com.esahaiambulance.activity.action.startforeground";
    public static String STOPFOREGROUND_ACTION = "com.truiton.foregroundservice.action.stopforeground";

    public static String MAIN_ACTION = "com.truiton.foregroundservice.action.main";
    public static String PREV_ACTION = "com.truiton.foregroundservice.action.prev";
    public static String PLAY_ACTION = "com.truiton.foregroundservice.action.play";
    public static String NEXT_ACTION = "com.truiton.foregroundservice.action.next";

    public static BookingDetails bookingDetails = null;


    // locations
    private LocationManager locationManager;

    Location currentLocation;

    Context mContext;


    private Timer timerSendLatLantToCustomer;
    private TimerTask timerTaskSendLatLantToCustomer;
    public static boolean IS_SERVICE_RUNNING = false;
    private Location previousLocation = null;


    boolean isServiceKilledbyUs = false;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //
        if (intent != null && null != intent.getAction()) {

            MyApplication.getApplication().addObserver(this);
            if (intent.getAction().equals(STARTFOREGROUND_ACTION)) {
                Log.i(LOG_TAG, "Received start Foreground Intent");
                startInitialStates(intent);

            } else if (intent.getAction().equals(
                    STOPFOREGROUND_ACTION)) {

                Log.i(LOG_TAG, "Received Stop Foreground Intent");
                killService();
            }
        } else {
            Log.d("intent null in ", "intent null in service ***********");
            killService();
            return START_NOT_STICKY;
        }

        return START_STICKY;
    }

    public void update(int event, final Object arg) {
        if (event == MyApplication.BOOKING_CANCELLED || event == MyApplication.BOOKING_CANCELLED_COMMAND_CENTER ) {
//            Toast.makeText(this,getString(R.string.customer_has_cancelled_your_booking),Toast.LENGTH_LONG).show();
            Log.i(LOG_TAG, "Received Stop update update");
            killService();

        } else if (event == MyApplication.BOOKING_TRANSFERRED) {

            //     Toast.makeText(this,getString(R.string.transferred_your_booking),Toast.LENGTH_LONG).show();
            //isServiceKilledbyUs = true;
            Log.i(LOG_TAG, "update BOOKING_CANCELLED");
            //stopForeground(true);
           // stopSelf();
            killService();
        }
    }

    @Override
    public void onDestroy() {
        stopSendLatLangtimertask();
        MyApplication.getApplication().deleteObserver(this);

        Log.d("isServiceKilledbyUs", "isServiceKilledbyUs = " + isServiceKilledbyUs);
        Log.i(LOG_TAG, "In onDestroy");
        if (!isServiceKilledbyUs) {
            Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());

            restartServiceIntent.setAction(TripProgressService.STARTFOREGROUND_ACTION);
            restartServiceIntent.putExtra("bookingDetails", bookingDetails);

            PendingIntent restartServicePendingIntent = PendingIntent.getService(
                    getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager alarmService = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmService.set(ELAPSED_REALTIME, elapsedRealtime() + 1000,
                    restartServicePendingIntent);
            Common.saveBookingDetails(this, bookingDetails);
            super.onDestroy();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.i(LOG_TAG, "onTaskRemoved");
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case of bound services.
        return null;
    }


    @Override
    public void onLocationChanged(Location location) {

        Log.d("onLocationChanged", "onLocationChanged in servie");
        if (isBetterLocation(location, currentLocation)) {
            currentLocation = location;
            calculateDistance();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void updateDeviceStatus(final boolean isShowProgressDialog, HashMap<String, Object> map, final int type) {

        map.put(Constants.latitude, currentLocation.getLatitude());
        map.put(Constants.longitude, currentLocation.getLongitude());
        map.put(Constants.geoHashCode, Common.getGeoHashCode(currentLocation));
        map.put("ambulanceId", Common.getAmbulanceId(mContext));
        map.put(Constants.driverId, Common.getDriverId(mContext));
        map.put(Constants.ambulanceApp, true);
        if (bookingDetails != null) {
            map.put("booking_id", bookingDetails.getBookingId());
            map.put("booking_status", bookingDetails.getCurrentBookingStatus());
        }

        if (bookingDetails != null && type != -1) {
            map.put("is_booked", true);
            map.put("booking_id", bookingDetails.getBookingId());

            map.put("end_latitude", bookingDetails.getHospitalLatitude());
            map.put("end_longitude", bookingDetails.getHospitalLongitude());
            map.put("end_geoHashCode", bookingDetails.getHospitalGeoHashCode());

            // emergency Location
            map.put("emergency_latitude", bookingDetails.getLatitude());
            map.put("emergency_longitude", bookingDetails.getLongitude());
        }

        Log.d("req", "req = from service ***** " + Arrays.asList(map));
        if (Common.isNetworkAvailable(mContext)) {

            if (isShowProgressDialog) {
                //  Common.displayProgress(mContext);
            }
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<JsonElement> call = apiService.updateAmbulanceStatus(map, Common.getUserId(mContext), Common.getAccessToken(mContext));

            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    if (isShowProgressDialog) {
                        //  Common.stopProgressDialog();
                    }

                    try {
                        Log.d("response", "response = 222 service ***** " + serverResponse.body());
                        System.out.print("response = 22222 service ***** " + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222 service ***** " + serverResponse.body());
                        System.out.print("response = 22222 service ***** " + serverResponse.body());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {


                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(mContext);

                        }else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseBookingCancelled)) {
                         //   isServiceKilledbyUs = true;
                            Log.i(LOG_TAG, "Received Stop Foreground Intent");
                         //   stopForeground(true);
                         //   stopSelf();
                            killService();
                        }
                        else {
                            // Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                            Log.d("trip progress", "trip progress " + response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    if (isShowProgressDialog) {
                        // Common.stopProgressDialog();
                    }
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    // Common.displayAlertDialog(mContext, mContext.getString(R.string.networkError));
                }
            });
        } else {

            //Common.displayAlertDialog(mContext, mContext.getString(R.string.offline));
        }
    }

    public void startSendLatLangTimer() {
        if (timerSendLatLantToCustomer != null) {
            timerSendLatLantToCustomer.cancel();
            timerSendLatLantToCustomer = null;
        }
        // set a new Timer
        timerSendLatLantToCustomer = new Timer();
        // initialize the TimerTask's job
        initializeSendLatLangTimerTask();
        // schedule the timer, after the first 5000ms the TimerTask will run
        // every 10000ms
        timerSendLatLantToCustomer.schedule(timerTaskSendLatLantToCustomer, 0, Constants.deviceLocationUpdateIntervalWhileBookingInProgress); //
    }

    public void stopSendLatLangtimertask() {
        // stop the timer, if it's not already null
        if (timerSendLatLantToCustomer != null) {
            timerSendLatLantToCustomer.cancel();
            timerSendLatLantToCustomer = null;
        }
    }

    @SuppressLint("NewApi")
    public void initializeSendLatLangTimerTask() {
        timerTaskSendLatLantToCustomer = new TimerTask() {
            public void run() {

                try {

                    if (currentLocation == null) {

                        Log.d("currentLocation", "currentLocation in service" + currentLocation);
                        return;
                    }
                    HashMap<String, Object> mGcmData = new HashMap<>();
                    HashMap<String, Object> mData = new HashMap<>();
                    //updateDeviceStatus(false, map, locationUpdate);
                    mGcmData.put("to", bookingDetails.getGcmToken());
                    mData.put(Constants.latitude, currentLocation.getLatitude());
                    mData.put(Constants.longitude, currentLocation.getLongitude());
                    mData.put("type", "ambulanceUpdate");

                    mGcmData.put("data", mData);

                    SendPushNotification sendPushNotification = new SendPushNotification(mContext);
                    sendPushNotification.sendPn(mGcmData);
                    // isSendingStatusUpdate is always false as we are sending only locatoin update in service
                    //  if (!isSendingStatusUpdate) {
                    HashMap<String, Object> hashMapServerUpdate = new HashMap<>();
                    hashMapServerUpdate.put("end_latitude", bookingDetails.getHospitalLatitude());
                    hashMapServerUpdate.put("end_longitude", bookingDetails.getHospitalLongitude());
                    hashMapServerUpdate.put("is_location_update", true);
                    updateDeviceStatus(false, hashMapServerUpdate, -1);
                    //   }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }


        };
    }

    private void showNotification() {
        Intent notificationIntent = new Intent(getApplicationContext(), DashboardActivity.class);
        // notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        Log.d("bookingDetails", "bookingDetails in service" + bookingDetails);
        notificationIntent.putExtra("bookingDetails", bookingDetails);
        notificationIntent.setAction(MAIN_ACTION);
        notificationIntent.putExtra("isFromService", true);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(),
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Booking In progress")
                .setTicker("Booking In progress")
                .setContentText(bookingDetails.getCurrentBookingStatus())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();
        startForeground(NOTIFICATION_ID,
                notification);

    }

    // checking is better location
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > DashboardActivity.timeDifference;
        boolean isSignificantlyOlder = timeDelta < -DashboardActivity.timeDifference;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private void calculateDistance() {

        if (bookingDetails != null) {
            if (previousLocation != null) {

                if (bookingDetails.getCurrentBookingStatus().equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)) {

                    bookingDetails.setCalculatedDistanceFromEmergencyToHospitalValue(
                            (bookingDetails.getCalculatedDistanceFromEmergencyToHospitalValue() + previousLocation.distanceTo(currentLocation)));

                } else {

                    bookingDetails.setCalculatedDistanceFromAmbToEmergencyValue(
                            (bookingDetails.getCalculatedDistanceFromAmbToEmergencyValue() + previousLocation.distanceTo(currentLocation)));
                }

            }
        } else {
            isServiceKilledbyUs = true;
            Log.i(LOG_TAG, "Received Stop Foreground Intent");
            stopForeground(true);
            stopSelf();
        }
        previousLocation = currentLocation;
    }

    private void startInitialStates(Intent intent) {
        bookingDetails = (BookingDetails) intent.getSerializableExtra("bookingDetails");
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (currentLocation == null) {
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, DashboardActivity.MIN_TIME, DashboardActivity.MIN_DISTANCE, this);
        showNotification();
        startSendLatLangTimer();
    }

    private void killService(){

        stopSendLatLangtimertask();
        isServiceKilledbyUs = true;
        stopForeground(true);
        stopSelf();
    }
}
