package com.esahaiambulance.utils;

/**
 * Created by prashanth on 16/9/16.
 */

public class Constants {

    //  webservices constants

    public static final String statusCode = "statusCode";
    public static final String statusMessage = "statusMessage";
    public static final String responseStatusSuccess = "300";

    public static final String responseInvalidToken = "204";
    public static final String responseBookingCancelled = "411";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String geoHashCode = "geoHashCode";
    public static final String ambulanceApp = "ambulanceApp";


    // ambulance details
    public static final String userId = "userId";
    public static final String accessToken = "token";
    public static final String ambulanceNumber = "ambulanceNumber";
    public static final String ambId = "ambId";
    public static final String ambGroupId = "ambGroupId";
    public static final String ambGroupNmae = "ambGroupNmae";
    public static final String ambProfilePic = "ambProfilePic";
    public static final String ambMobileNumber = "ambMobileNumber";
    public static final String emergencyType = "emergencyType";
    public static final String vehicleType = "vehicleType";
    //public static final String groupName = "groupName";

    public static final String ambHospitalNumber = "ambHospitalNumber";

    public static final String commandCenterNumber = "commandCenterNumber";

    // api key for pns
    public static final String pnApiKey = "pnApiKey";



    //driver details
    public static final String driverId = "driverId";
    public static final String driverGroupId = "driverGroupId";
    public static final String driverName = "driverName";
    public static final String driverMobileNumber = "driverMobileNumber";
    public static final String driverEmail = "driverEmail";
    public static final String driverLicenceNumber = "driverLicenceNumber";
    public static final String driverProfilePic = "driverProfilePic";

    //booking details
    public static final String bookingDetailsPref = "bookingDetails";

    // location
    public static final byte locationPrecisionNumber = 12;

    // gcm
    public static String isTokenSentToServer = "isTokenSentToServer";

    // login constants
    public static final String isDeviceLoggedIn = "isDeviceLoggedIn";

    public static final String isDriverLoggedIn = "isDriverLoggedIn";

    // static download constants
    public static final String isEmergencyListFeteched = "isEmergencyListFeteched";


    public static final String bookingDetails = "bookingDetails";


    public static final String isFaqsDownloaded = "isFaqsDownloaded";
    public static final String faqsData = "faqsData";

    // ambulance booking status list
    public static final String BOOKING_ACCEPTED_FULL = "Booking Accepted";
    public static final String WAY_TO_EMERGENCY_LOCATION_FULL = "Way to Emergency Location";
    public static final String AT_EMERGENCY_LOCATION_FULL = "At Emergency Location";
    public static final String WAY_BACK_TO_DESTINATION_FULL = "Way Back to Destination";
    public static final String REACHED_DESTINATION_FULL = "Reached Destination";
    public static final String TRIP_CLOSED_FULL = "Trip Closed";
    public static final String FAKE_BOOKING_FULL = "Fake booking";

    public static final String BOOKING_ACCEPTED = "ACTD";
    public static final String WAY_TO_EMERGENCY_LOCATION = "WTEL";
    public static final String AT_EMERGENCY_LOCATION = "AEL";
    public static final String WAY_BACK_TO_DESTINATION = "WBTD";
    public static final String REACHED_DESTINATION = "RD";
    public static final String TRIP_CLOSED = "TC";
    public static final String FAKE_BOOKING = "Fake booking";

    public static final long deviceLocationUpdateInterval = 1000 * 60 * 5;//1000*60;
    public static final long deviceLocationUpdateIntervalWhileBookingInProgress = 1000 * 30;


    public static final String API_KEY = "AIzaSyBILQohUbnLLLvNH-Y-rnLo-3bkeZHoJSs";
}
