package com.esahaiambulance.webservices;

import android.util.Log;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by prashanth on 16/9/16.
 */

public class ApiClient {

    // dev
   //public static final String BASE_URL = "http://dev.esahai.in/esahai/";
    //public static final String BASE_URL = "http://dev.esahai.in/esahai-ios/";

    // local
    // public static final String BASE_URL = "http://192.168.0.129:3000/";

    // qa

    //new QA
 // public static final String BASE_URL = "http://qa.esahai.in/esahai/";

    // prod
   public static final String BASE_URL ="https://prod.esahai.in/esahai/";

    public static final String PROD_URL_FOR_CHECK ="https://prod.esahai.in/esahai/";

    public static final String BASE_URL_GOOGLE = "http://maps.googleapis.com/";

   // public static final String BASE_URL_CONNECT_CALL = "http://122.175.47.248/ConVox3.0/";
   public static final String BASE_URL_CONNECT_CALL = "http://call.esahai.in/ConVox3.0/";

   // http://call.esahai.in/ConVox3.0/user_conference.php?conf_no_list=9440957396,04065556111

    private static Retrofit retrofit = null;
    private static Retrofit retrofitGoogle = null;

    private static Retrofit retrofitConnectCall = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getUnsafeOkHttpClient())
                    .build();
            Log.d("retrofit", "retro = " + retrofit.baseUrl());
        }

        return retrofit;
    }
    public static Retrofit getClientGoogle() {
        if (retrofitGoogle == null) {
            retrofitGoogle = new Retrofit.Builder()
                    .baseUrl(BASE_URL_GOOGLE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getUnsafeOkHttpClient())
                    .build();
            Log.d("retrofit", "retro = " + retrofitGoogle.baseUrl());
        }

        return retrofitGoogle;
    }

    public static Retrofit getClientConnectCall() {
        if (retrofitConnectCall == null) {
            retrofitConnectCall = new Retrofit.Builder()
                    .baseUrl(BASE_URL_CONNECT_CALL)
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getUnsafeOkHttpClient())
                    .build();
            Log.d("retrofit", "retro = " + retrofitConnectCall.baseUrl());
        }

        return retrofitConnectCall;
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            builder.connectTimeout(45, TimeUnit.SECONDS);
            builder.readTimeout(45, TimeUnit.SECONDS);
            builder.writeTimeout(45, TimeUnit.SECONDS);

           // logger
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);

            OkHttpClient okHttpClient = builder.build();
            //  builder.setReadTimeout(60, TimeUnit.SECONDS);
            // builder.setConnectTimeout(60, TimeUnit.SECONDS);

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
