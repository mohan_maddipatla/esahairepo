package com.esahaiambulance.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by prashanth on 15/2/17.
 */

public class BootCompleted extends BroadcastReceiver {
    static final String ACTION = "android.intent.action.BOOT_COMPLETED";

    public void onReceive(Context context, Intent intent) {

        Log.d("BootCompleted","BootCompleted  ");
        if(intent.getAction().equals(ACTION)) {

             Intent GTALK_HEART_BEAT_INTENT = new Intent("com.google.android.intent.action.GTALK_HEARTBEAT");
             Intent MCS_MCS_HEARTBEAT_INTENT = new Intent("com.google.android.intent.action.MCS_HEARTBEAT");
            context.sendBroadcast(GTALK_HEART_BEAT_INTENT);
            context.sendBroadcast(MCS_MCS_HEARTBEAT_INTENT);
            Log.d("BootCompleted", "HeartbeatReceiver sent heartbeat request");
        }
    }
}
