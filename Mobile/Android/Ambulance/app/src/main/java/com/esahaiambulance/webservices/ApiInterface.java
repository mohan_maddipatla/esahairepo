package com.esahaiambulance.webservices;

import com.esahaiambulance.models.DirectionResults;
import com.esahaiambulance.models.HospitalList;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by prashanth on 16/9/16.
 */

public interface ApiInterface {


    @POST("db/app_user/generateOTPAmbulance")
    Call<JsonElement> generateAmbulanceOtp(@Body HashMap<String, Object> body);

    @POST("db/app_user/verifyAmbulanceOTP")
    Call<JsonElement> verifyAmbulanceOTP(@Body HashMap<String , Object> json);

    @POST("db/getDriversListByAmbulanceId")
    Call<JsonElement> getDriversListByAmbulanceId(@Body HashMap<String , Object> json, @Header("userId") String userId, @Header("token") String token);

    @POST("db/app_user/driverLogin")
    Call<JsonElement> driverLogin(@Body HashMap<String , Object> json);

    @POST("db/updateAmbulanceStatus")
    Call<JsonElement> updateAmbulanceStatus(@Body HashMap<String , Object> json,@Header("userId") String userId, @Header("token") String token);

    @POST("db/assignBooking")
    Call<JsonElement> assignBooking(@Body HashMap<String , Object> json,@Header("userId") String userId, @Header("token") String token);



    @POST("db/updateDeviceToken")
    Call<JsonElement> updateDeviceToken(@Body HashMap<String , Object> json,@Header("userId") String userId, @Header("token") String token);

    @POST("db/updateRejectReason")
    Call<JsonElement> sendBookingRejectionReason(@Body HashMap<String , Object> json,@Header("userId") String userId, @Header("token") String token);

    @POST("db/app_user/driverLogout")
    Call<JsonElement> driverLogOut(@Body HashMap<String , Object> json, @Header("userId") String userId, @Header("token") String token);

    @POST("db/getEmergencyTypeList")
    Call<JsonElement> getEmergencyTypeList(@Body HashMap<String , Object> json);

    //get nearby hospitals list
    @POST("db/getNearByHospitalList")
    Call<HospitalList> getNearByHospitalsList(@Body HashMap<String, Object> locationDetails, @Header("userId") String userId, @Header("token") String token);


    @POST("db/updateAmbulanceLocation")
    Call<JsonElement> updateAmbulanceLocation(@Body HashMap<String, Object> locationDetails, @Header("userId") String userId, @Header("token") String token);

    @POST("db/getTripList")
    Call<JsonElement> getTripList(@Body HashMap<String, Object> locationDetails, @Header("userId") String userId, @Header("token") String token);

    @POST("db/transferBooking")
    Call<JsonElement> transferBooking(@Body HashMap<String, Object> bookingDetails, @Header("userId") String userId, @Header("token") String token);

    //  driver canceling the booking as he found it as fake booking
    @POST("db/cancelBooking")
    Call<JsonElement> fakeBooking(@Body HashMap<String, Object> bookingDetails, @Header("userId") String userId, @Header("token") String token);

    // get patient details
    @POST("db/getPatientInfo")
    Call<JsonElement> getPatientInfo(@Body HashMap<String, Object> bookingDetails, @Header("userId") String userId, @Header("token") String token);

    // update profile photo
    @Multipart
    @POST("db/updateProfile")

    Call<JsonElement> uploadProfilePic(@PartMap HashMap<String, Object> customerRequest,
                                              @Part MultipartBody.Part file,
                                              @Header("userId") String userId,
                                              @Header("token") String token);


    @GET("db/getAppVersion")
    Call<JsonElement> getAppVersion( @Query("appType") String appType);

    // get patient details
    @POST("db/getCommandCentreNUmber")
    Call<JsonElement> getCommandCenterNumber(@Body HashMap<String, Object> bookingDetails);

    // get patient details
    @POST("db/getAmbulanceProfile")
    Call<JsonElement> getAmbulanceProfile(@Body HashMap<String, Object> bookingDetails, @Header("userId") String userId, @Header("token") String token);


    // get faqs
    @POST("db/faqList")
    Call<JsonElement> getFaqs(@Body HashMap<String, Object> faqs, @Header("userId") String userId, @Header("token") String token);


    // maps related
    @GET("/maps/api/directions/json")
    Call<DirectionResults> getRoute(@Query("origin") String origin, @Query("destination") String destination,@Query("key") String key);


    // send push notification

    @POST("send")
    Call<JsonElement> sendPushNotification(@Body HashMap<String , Object> json, @Header("Authorization") String authorization, @Header("Content-Type") String token);



    // connect call

    @GET("user_conference.php")
    Call<Object> connectCall( @Query("conf_no_list") String appType);
}
