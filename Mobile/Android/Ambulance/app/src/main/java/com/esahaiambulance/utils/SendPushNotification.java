package com.esahaiambulance.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.esahaiambulance.R;
import com.esahaiambulance.activity.DashboardActivity;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by prashanth on 30/9/16.
 */

public class SendPushNotification {

    private  String API_KEY = "";

    Context mContext;

    boolean isShowProgress = false;

    public SendPushNotification( Context mContext){

        API_KEY = Common.getStringPref(mContext,Constants.pnApiKey,"");
        this.mContext = mContext;
    }
    public SendPushNotification( Context mContext,boolean isShowProgress){

        this(mContext);
        this.mContext = mContext;
        this.isShowProgress = isShowProgress;
    }


    public void sendPn( HashMap<String,Object> data){
        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl("https://android.googleapis.com/gcm/")
                //.client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        data.put("time_to_live",30);

        //  data.put("collapse_key",0);



        if (Common.isNetworkAvailable(mContext)) {

            if(isShowProgress) {
                Common.displayProgress(mContext);
            }
            ApiInterface apiService =
                    restAdapter.create(ApiInterface.class);

            Call<JsonElement> call = apiService.sendPushNotification(data,"key=" + API_KEY,"application/json");

            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    //  Common.stopProgressDialog();


                    try {
                        if(serverResponse != null) {
                            JsonElement jsonElement = serverResponse.body();
                            JSONObject response = new JSONObject(jsonElement.toString());
                            Log.d("pn status", "pns " + serverResponse.body());

                        }else{

                            System.out.print("pns " + "pn failed gcm token nulllllllllllllllllll");
                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    if (isShowProgress) {
                        Common.stopProgressDialog();
                    }
                    Log.e("eroooooooorr", throwable.toString());
                    //Common.displayAlertDialog(mContext, mContext.getString(R.string.networkError));
                }
            });
        } else {

            if (isShowProgress) {
                //Common.displayAlertDialog(mContext, mContext.getString(R.string.offline));
            }
        }
    }


}
