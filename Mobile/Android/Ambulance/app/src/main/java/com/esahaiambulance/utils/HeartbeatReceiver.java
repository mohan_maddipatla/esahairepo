package com.esahaiambulance.utils;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.format.DateFormat;
import android.util.Log;

/**
 * Created by prashanth on 19/12/16.
 */

public class HeartbeatReceiver extends BroadcastReceiver {
    private static final Intent GTALK_HEART_BEAT_INTENT = new Intent("com.google.android.intent.action.GTALK_HEARTBEAT");
    private static final Intent MCS_MCS_HEARTBEAT_INTENT = new Intent("com.google.android.intent.action.MCS_HEARTBEAT");

    private static final String TAG = "HeartbeatReceiver";
    public static final int intervalMillis = 1000 * 45;

    @Override
    public void onReceive(Context context, Intent intent) {
        context.sendBroadcast(GTALK_HEART_BEAT_INTENT);
        context.sendBroadcast(MCS_MCS_HEARTBEAT_INTENT);
        Log.d(TAG, "HeartbeatReceiver sent heartbeat request");
        scheduleHeartbeatRequest(context);
    }

    public static void scheduleHeartbeatRequest(Context context) {
        Log.d(TAG, "HeartbeatFixerUtils, scheduleHeartbeatRequest, fromNetworkStateChange: ");


        setNextHeartbeatRequest(context, intervalMillis);

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static void setNextHeartbeatRequest(Context context, int intervalMillis) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        long triggerAtMillis = System.currentTimeMillis() + intervalMillis;
        Log.d(TAG, "setNextHeartbeatRequest at: " + DateFormat.format("yyyy-MM-dd hh:mm:ss", triggerAtMillis));
        PendingIntent broadcastPendingIntent = getBroadcastPendingIntent(context);
        int rtcWakeup = AlarmManager.RTC_WAKEUP;
        if (hasKitkat()) {
            alarmManager.setExact(rtcWakeup, triggerAtMillis, broadcastPendingIntent);
        } else {
            alarmManager.set(rtcWakeup, triggerAtMillis, broadcastPendingIntent);
        }
    }

    private static PendingIntent getBroadcastPendingIntent(Context context) {
        return PendingIntent.getBroadcast(context, 0, new Intent(context, HeartbeatReceiver.class), 0);
    }


    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasKitkat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static boolean hasLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static void cancelHeartbeatRequest(Context context) {
        Log.d(TAG, "cancelHeartbeatRequest");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(getBroadcastPendingIntent(context));
    }
}
