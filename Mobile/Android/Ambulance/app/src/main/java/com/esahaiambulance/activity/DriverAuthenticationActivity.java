package com.esahaiambulance.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.R;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.utils.HeartbeatReceiver;
import com.esahaiambulance.utils.MyAlertListener;
import com.esahaiambulance.utils.PermissionUtils;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import ch.hsr.geohash.GeoHash;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 19/9/16.
 */

@SuppressWarnings({"MissingPermission"})
public class DriverAuthenticationActivity extends AppCompatActivity {


    private final int MY_LOCATION_PERMISSION_REQUEST_CODE = 1;
    Toolbar toolbar;
    TextView txtTitle;
    TextView txtStatus;
    TextView txtName;
    EditText etDriverPin;
    Button btnSubmit;
    Context mContext;
    DriverDetails driverDetails;
    CircleImageView profilePic;
    private boolean mLocationPermissionGranted = false;
    private LocationManager locationManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_authentication);
        Log.d("onCreate", "onCreate");
        mContext = this;
        initViews();
        driverDetails = (DriverDetails) getIntent().getExtras().getSerializable("driverDetails");
        Picasso.with(mContext).load(driverDetails.getFileUrl()).into(profilePic);
        txtName.setText(getString(R.string.HiEnterPin).replace("xxxx", driverDetails.getDriverName()));
        //txtName.setText("Welcome "+driverDetails.getDriverName());

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!Common.checkLocationPermission(this)) {
            requestLocationPermission(MY_LOCATION_PERMISSION_REQUEST_CODE);
        }else{
            mLocationPermissionGranted = true;

        }

        Log.d("user id = ","user id = "+ Common.getUserId(mContext));
        Log.d("acces token = ","acces token = "+Common.getAccessToken(mContext));
        Common.sendRegistrationToServer(this.getApplicationContext());
    }

    private void initViews() {

        Log.d("onCreate", "initViews");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText( Common.getAmbulanceNumber(this));

        txtStatus = (TextView) findViewById(R.id.txtStatus);

        txtName = (TextView) findViewById(R.id.txtName);
        etDriverPin = (EditText) findViewById(R.id.etDriverPin);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        profilePic = (CircleImageView) findViewById(R.id.profile_image);

        btnSubmit.setEnabled(false);
        btnSubmit.setFocusable(true);
        btnSubmit.setFocusableInTouchMode(true);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("onCreate", "setOnClickListener end ****");
                MyApplication.getApplication().trackEvent("Driver Authentication","Button Driver Login",""+driverDetails.getDriverMobileNumber());
                verifyDriverPin();

            }
        });

        etDriverPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etDriverPin.getText().toString().length() == 4) {
                    Common.closeKeyBoard(mContext, etDriverPin);
                    btnSubmit.setEnabled(true);
                    etDriverPin.clearFocus();
                    btnSubmit.requestFocus();
                } else {
                    btnSubmit.setEnabled(false);
                }


            }
        });


    }
    public void requestLocationPermission(int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog
                    .newInstance(requestCode, false).show(
                    getSupportFragmentManager(), "dialog");
        } else {
            // Location permission has not been granted yet, request it.
            PermissionUtils.requestPermission(this, requestCode,
                    Manifest.permission.ACCESS_FINE_LOCATION, false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_LOCATION_PERMISSION_REQUEST_CODE) {
            // Enable the My Location button if the permission has been granted.
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                mLocationPermissionGranted = true;
            } else {
                mLocationPermissionGranted = false;
            }

        }
    }
    private void verifyDriverPin() {

        if(!mLocationPermissionGranted){
            requestLocationPermission(MY_LOCATION_PERMISSION_REQUEST_CODE);
            return;
        }

        if(!Common.isLocationServiceEnabled(locationManager)){

            enableLocationServices();
            return;
        }


        if (Common.isNetworkAvailable(this)) {

            Location location = null;
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if(location == null){
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            String currentGeoHash ="";
            if(location != null) {
               currentGeoHash = Common.getGeoHashCode(location);
            }
            Log.d("currentGeoHash","currentGeoHash = "+currentGeoHash);

            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceId", Common.getAmbulanceId(this));
            map.put("driverId", driverDetails.getDriverId());
            map.put("driverPin", Common.encrypt(etDriverPin.getText().toString()));
            map.put(Constants.geoHashCode, currentGeoHash);
            if(location != null) {
                map.put(Constants.latitude, location.getLatitude());
                map.put(Constants.longitude, location.getLongitude());

            }else{
                map.put(Constants.latitude, "000000");
                map.put(Constants.longitude, "0000000");
            }


        //    Log.d("req", "req = " + Arrays.asList(map));
            //  Log.d("mobile","after encrypt mobile"+Common.encrypt(etMobileNumber.getText().toString()));


            Common.displayProgress(this);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            //Log.d("response", "user id " + Common.getUserId(this));
          //  Log.d("response", "access token " + Common.getAccessToken(this));

            Call<JsonElement> call2 = apiService.driverLogin(map);
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            MyApplication.getApplication().trackEvent("Driver Authentication","Sucess",""+driverDetails.getDriverMobileNumber(),false);
                            // saving driver details

                            JSONObject jsonDriverDetails = response.getJSONObject("responseData");

                            saveDriverDetails(mContext,jsonDriverDetails);
                            // saving driver login status
                            Common.saveBooleanPref(mContext,Constants.isDriverLoggedIn,true);

                            Common.showAccRegNotification(mContext);

                            HeartbeatReceiver.scheduleHeartbeatRequest(DriverAuthenticationActivity.this);
                            Intent intent = new Intent(DriverAuthenticationActivity.this, DashboardActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(DriverAuthenticationActivity.this);
                            MyApplication.getApplication().trackEvent("Driver Authentication","Log out",""+driverDetails.getDriverMobileNumber(),false);

                        } else {
                            MyApplication.getApplication().trackEvent("Driver Authentication","Failure",""+driverDetails.getDriverMobileNumber(),false);
                            Common.displayAlertDialog(DriverAuthenticationActivity.this, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    System.out.print("erro ="+throwable.toString());
                    Common.displayAlertDialog(DriverAuthenticationActivity.this, getString(R.string.networkError));
                    MyApplication.getApplication().trackEvent("Driver Authentication",getString(R.string.networkError),""+driverDetails.getDriverMobileNumber(),false);
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableLocationServices();
    }
    private void enableLocationServices(){
        if(mLocationPermissionGranted &&  !Common.isLocationServiceEnabled(locationManager)){

            /*Common.simpleTwoButtonAlertDialog(this,getString(R.string.locationServiceEnable), new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {
                    if(type == 0){
                        // positive
                        Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(myIntent);
                    }

                }
            });*/

            Common.simpleSingleButtonAlertDialog(this, getString(R.string.locationServiceEnable), new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {

                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(myIntent);
                }
            });
        }
    }
    private  void saveDriverDetails(Context mContext,JSONObject jsonDriverDetails){
        try {
            Common.saveStringPref(mContext, Constants.driverId, jsonDriverDetails.getString("driver_id"));

            Common.saveStringPref(mContext, Constants.driverGroupId, jsonDriverDetails.getString("group_id"));
            Common.saveStringPref(mContext, Constants.driverName, jsonDriverDetails.getString("driver_name"));
            Common.saveStringPref(mContext, Constants.driverMobileNumber, jsonDriverDetails.getString("driver_mobile_number"));
            Common.saveStringPref(mContext, Constants.driverEmail, jsonDriverDetails.getString("driver_email"));
            Common.saveStringPref(mContext, Constants.driverLicenceNumber, jsonDriverDetails.getString("license_number"));

            Common.saveStringPref(mContext, Constants.driverProfilePic, jsonDriverDetails.getString("file_url"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
