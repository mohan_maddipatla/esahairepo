package com.esahaiambulance.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.adapters.ExpandableListAdapter;
import com.esahaiambulance.models.Faqlist;
import com.esahaiambulance.utils.AppObserver;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 14/11/16.
 */

public class FaqsActivity extends AppCompatActivity implements AppObserver {


    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    LinkedHashMap<String, List<String>> expandableListDetail;
    private int lastExpandedPosition = -1;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);
        mContext= this;
        MyApplication.getApplication().addObserver(this);
        initViews();


        expandableListView
                .setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (lastExpandedPosition != -1
                                && groupPosition != lastExpandedPosition) {
                            expandableListView
                                    .collapseGroup(lastExpandedPosition);
                        }
                        lastExpandedPosition = groupPosition;
                    }
                });

        expandableListView
                .setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                    @Override
                    public void onGroupCollapse(int groupPosition) {

                    }
                });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        if(Common.getBooleanPerf(mContext,Constants.isFaqsDownloaded,false)){
            setAdapter();
        }else{
            getFaqsFromServer();
        }

    }

    @Override
    protected void onResume() {
        MyApplication.getApplication().addObserver(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        MyApplication.getApplication().deleteObserver(this);
        super.onPause();
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        ImageView img = (ImageView)toolbar.findViewById(R.id.img_toolbarimg);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText(getString(R.string.faqs));

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
    }

    private void setAdapter(){
        try {
            JSONArray array = new JSONArray(Common.getStringPref(mContext, Constants.faqsData, ""));
            expandableListDetail = getData(array);
            expandableListTitle = new ArrayList<String>(
                    expandableListDetail.keySet());

            expandableListAdapter = new ExpandableListAdapter(FaqsActivity.this,
                    expandableListTitle, expandableListDetail);

            expandableListView.setAdapter(expandableListAdapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private LinkedHashMap<String, List<String>> getData(JSONArray jary) {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();
        List<Faqlist> faqarraylist = new ArrayList<>();
        try {
            Faqlist lrv;
            // JSONArray jary = responseData.getJSONArray("Result");
            for (int i = 0; i < jary.length(); i++) {
                JSONObject questionData = jary.getJSONObject(i);
                lrv = new Faqlist(
                        questionData.get("question").toString().trim(),
                        questionData.get("answer").toString().trim());
                faqarraylist.add(lrv);
            }
        } catch (JSONException je) {
            je.printStackTrace();
        }

      //  Collections.sort(faqarraylist, new Faqlist());

        System.out.println("faq:::" + faqarraylist);
        for (final Faqlist faqlist : faqarraylist) {
            String question = faqlist.getQuestion();
            String answer = faqlist.getAnswer();
            List<String> technology = new ArrayList<String>();
            technology.add(answer);
            expandableListDetail.put(question, technology);
        }
        return expandableListDetail;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(menuItem);
    }

    private void getFaqsFromServer(){

        if (Common.isNetworkAvailable(this)) {

            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceApp", true);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Common.displayProgress(mContext);
            Call<JsonElement> call2 = apiService.getFaqs(map,Common.getAmbulanceId(this), Common.getAccessToken(this));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {

                        Common.stopProgressDialog();
                        Log.d("response", "response = getFaqsFromServer" + serverResponse.body());
                        System.out.print("response = getFaqsFromServer" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());


                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            JSONArray data = response.getJSONArray("responseData");
                            Common.saveBooleanPref(mContext,Constants.isFaqsDownloaded,true);
                            Common.saveStringPref(mContext,Constants.faqsData,""+data);
                            setAdapter();

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(FaqsActivity.this);

                        }else {

                            Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));

                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {
                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getFaqsFromServer: " + "eroorrrrrrrrrrrr");
                    Log.e("getFaqsFromServer", throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                }
            });
        }
    }

    @Override
    public void update(int event, Object arg) {
        if(event == MyApplication.FAQS_UPDATE ) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getFaqsFromServer();
                }
            });
        }
    }
}
