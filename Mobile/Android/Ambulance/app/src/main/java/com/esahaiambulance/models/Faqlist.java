package com.esahaiambulance.models;

import java.util.Comparator;

public class Faqlist  {// implements Comparator<Faqlist> {
	private int id;
	private String question;
	private String answer;

    public Faqlist() {
    }

    public Faqlist( String question, String answer) {
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

/*	@Override
	public int compare(Faqlist lhs, Faqlist rhs) {
		if (lhs.getId() > rhs.getId()) {
			return 1;
		} else {
			return -1;
		}
	}*/
}
