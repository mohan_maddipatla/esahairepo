package com.esahaiambulance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by prashanth on 20/9/16.
 */

public class TripDetails implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ambulance_id")
    @Expose
    private String ambulanceId;
    @SerializedName("current_status")
    @Expose
    private Object currentStatus;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("speed")
    @Expose
    private String speed;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("geoHashCode")
    @Expose
    private String geoHashCode;
    @SerializedName("is_booked")
    @Expose
    private String isBooked;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("booking_status")
    @Expose
    private String bookingStatus;
    @SerializedName("end_longitude")
    @Expose
    private String endLongitude;
    @SerializedName("end_latitude")
    @Expose
    private String endLatitude;
    @SerializedName("end_geoHashCode")
    @Expose
    private String endGeoHashCode;
    @SerializedName("estimated_time_arrival")
    @Expose
    private String estimatedTimeArrival;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("emergency_longitude")
    @Expose
    private String emergencyLongitude;
    @SerializedName("emergency_latitude")
    @Expose
    private String emergencyLatitude;



    @SerializedName("driver_profile")
    @Expose
    private String driverProfile;

    @SerializedName("emergency_location")
    @Expose
    private String emergencyLocation;

    @SerializedName("hospital_location")
    @Expose
    private String hospitalLocation;

    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("duration")
    @Expose
    private String duration;

    @SerializedName("cost")
    @Expose
    private String cost;


    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The ambulanceId
     */
    public String getAmbulanceId() {
        return ambulanceId;
    }

    /**
     * @param ambulanceId The ambulance_id
     */
    public void setAmbulanceId(String ambulanceId) {
        this.ambulanceId = ambulanceId;
    }

    /**
     * @return The currentStatus
     */
    public Object getCurrentStatus() {
        return currentStatus;
    }

    /**
     * @param currentStatus The current_status
     */
    public void setCurrentStatus(Object currentStatus) {
        this.currentStatus = currentStatus;
    }

    /**
     * @return The driverId
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * @param driverId The driver_id
     */
    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    /**
     * @return The speed
     */
    public String getSpeed() {
        return speed;
    }

    /**
     * @param speed The speed
     */
    public void setSpeed(String speed) {
        this.speed = speed;
    }

    /**
     * @return The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate The created_date
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return The geoHashCode
     */
    public String getGeoHashCode() {
        return geoHashCode;
    }

    /**
     * @param geoHashCode The geoHashCode
     */
    public void setGeoHashCode(String geoHashCode) {
        this.geoHashCode = geoHashCode;
    }

    /**
     * @return The isBooked
     */
    public String getIsBooked() {
        return isBooked;
    }

    /**
     * @param isBooked The is_booked
     */
    public void setIsBooked(String isBooked) {
        this.isBooked = isBooked;
    }

    /**
     * @return The bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     * @param bookingId The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * @return The bookingStatus
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * @param bookingStatus The booking_status
     */
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    /**
     * @return The endLongitude
     */
    public String getEndLongitude() {
        return endLongitude;
    }

    /**
     * @param endLongitude The end_longitude
     */
    public void setEndLongitude(String endLongitude) {
        this.endLongitude = endLongitude;
    }

    /**
     * @return The endLatitude
     */
    public String getEndLatitude() {
        return endLatitude;
    }

    /**
     * @param endLatitude The end_latitude
     */
    public void setEndLatitude(String endLatitude) {
        this.endLatitude = endLatitude;
    }

    /**
     * @return The endGeoHashCode
     */
    public String getEndGeoHashCode() {
        return endGeoHashCode;
    }

    /**
     * @param endGeoHashCode The end_geoHashCode
     */
    public void setEndGeoHashCode(String endGeoHashCode) {
        this.endGeoHashCode = endGeoHashCode;
    }

    /**
     * @return The estimatedTimeArrival
     */
    public String getEstimatedTimeArrival() {
        return estimatedTimeArrival;
    }

    /**
     * @param estimatedTimeArrival The estimated_time_arrival
     */
    public void setEstimatedTimeArrival(String estimatedTimeArrival) {
        this.estimatedTimeArrival = estimatedTimeArrival;
    }

    /**
     * @return The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The emergencyLongitude
     */
    public String getEmergencyLongitude() {
        return emergencyLongitude;
    }

    /**
     * @param emergencyLongitude The emergency_longitude
     */
    public void setEmergencyLongitude(String emergencyLongitude) {
        this.emergencyLongitude = emergencyLongitude;
    }

    /**
     * @return The emergencyLatitude
     */
    public String getEmergencyLatitude() {
        return emergencyLatitude;
    }

    /**
     * @param emergencyLatitude The emergency_latitude
     */
    public void setEmergencyLatitude(String emergencyLatitude) {
        this.emergencyLatitude = emergencyLatitude;
    }

    public String getDriverProfile() {
        return driverProfile;
    }

    public void setDriverProfile(String driverProfile) {
        this.driverProfile = driverProfile;
    }

    public String getEmergencyLocation() {
        return emergencyLocation;
    }

    public void setEmergencyLocation(String emergencyLocation) {
        this.emergencyLocation = emergencyLocation;
    }

    public String getHospitalLocation() {
        return hospitalLocation;
    }

    public void setHospitalLocation(String hospitalLocation) {
        this.hospitalLocation = hospitalLocation;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }


    @Override
    public String toString() {
        return "{"+"booking status = "+getBookingStatus()+"}";
    }
}
