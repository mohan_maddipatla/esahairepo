package com.esahaiambulance.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.esahaiambulance.R;
import com.esahaiambulance.utils.OnItemClickListener;

/**
 * Created by prashanth on 19/9/16.
 */

public class TripRejectionReasonAdapter extends RecyclerView.Adapter<TripRejectionReasonAdapter.ViewHolder> {


    private Context context;
    private String[] reasonsList;
    public int mCheckedPostion =-1;
    OnItemClickListener listner;

    public TripRejectionReasonAdapter(Context context, String[] arrayList) {

        this.context = context;
        this.reasonsList = arrayList;

    }

    public TripRejectionReasonAdapter(Context context, String[] arrayList, OnItemClickListener listner) {

        this.context = context;
        this.reasonsList = arrayList;
        this.listner = listner;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.trip_rejection_reclyer_item, parent, false);
        TripRejectionReasonAdapter.ViewHolder viewHolder = new TripRejectionReasonAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {

        holder.checkBox.setText(reasonsList[position]);

        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (position == mCheckedPostion) {
                    holder.checkBox.setChecked(false);
                    mCheckedPostion = -1;
                    //  checkedPositionsList.remove(position);

                } else {
                    // checkedPositionsList.put(position,position);
                    mCheckedPostion = position;
                    holder.checkBox.setChecked(true);
                    notifyDataSetChanged();
                }
                if(listner != null){
                    listner.onClick(holder.checkBox,mCheckedPostion);
                }

            }
        });
        holder.checkBox.setChecked(position == mCheckedPostion);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == mCheckedPostion) {

                    holder.checkBox.setChecked(false);
                    mCheckedPostion = -1;


                }
                else{
                    mCheckedPostion = position;
                    holder.checkBox.setChecked(true);
                    notifyDataSetChanged();
                }
                if(listner != null){
                    listner.onClick(holder.checkBox,mCheckedPostion);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return reasonsList.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        CheckBox checkBox;
        LinearLayout llRoot;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            llRoot = (LinearLayout) view.findViewById(R.id.llRoot);
        }

        @Override
        public void onClick(View view) {
           /* Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(view.getContext(), DriverAuthenticationActivity.class);
            intent.putExtra("driverDetails", arrayList.get(getPosition()));
            context.startActivity(intent);*/
        }

    }
}
