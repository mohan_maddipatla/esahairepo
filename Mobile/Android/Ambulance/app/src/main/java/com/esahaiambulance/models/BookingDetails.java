package com.esahaiambulance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by prashanth on 30/9/16.
 */

public class BookingDetails implements Serializable {

    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("duration")
    @Expose
    private String duration ="";
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("customerMobile")
    @Expose
    private String customerMobile;
    @SerializedName("customerName")
    @Expose
    private String customerName;

    @SerializedName("age")
    @Expose
    private String age;


    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("gcmToken")
    @Expose
    private String gcmToken;

    @SerializedName("hospital_latitude")
    @Expose
    private String hospitalLatitude;

    @SerializedName("hospital_longitude")
    @Expose
    private String hospitalLongitude;

    @SerializedName("hospital_geoHashCode")
    @Expose
    private String hospitalGeoHashCode;

    @SerializedName("hospital")
    @Expose
    private String hospitalName;

    @SerializedName("hospitalNumber")
    @Expose
    private String hospitalNumber;

    @SerializedName("emergencyType")
    @Expose
    private String emergencyType;

    @SerializedName("drop_location")
    @Expose
    private boolean dropLocation = false;

    // present booking status

    @Expose(deserialize = false,serialize = false)
    private  String currentBookingStatus = "";

    // location
    @Expose(deserialize = false,serialize = false)
    private  double ambulanceStartLat;
    @Expose(deserialize = false,serialize = false)
    private  double ambulanceStartLong;

    // distance from google api
    @Expose(deserialize = false,serialize = false)
    private String googleDistanceFromAmbToEmergency;
    @Expose(deserialize = false,serialize = false)
    private String googleDistanceFromEmergencyToHospital;

    @Expose(deserialize = false,serialize = false)
    private String googleDistanceFromAmbToEmergencyValue;
    @Expose(deserialize = false,serialize = false)
    private String googleDistanceFromEmergencyToHospitalValue;

    // distance from google api
    @Expose(deserialize = false,serialize = false)
    private String estimateTimeFromAmbulanceToEmergency;
    @Expose(deserialize = false,serialize = false)
    private String estimateTimeFromEmergencyToHospital;

    // caluclated distance
    @Expose(deserialize = false,serialize = false)
    private double calculatedDistanceFromAmbToEmergencyValue;
    @Expose(deserialize = false,serialize = false)
    private double calculatedDistanceFromEmergencyToHospitalValue;

    @Expose(deserialize = false,serialize = false)
    private double totalCalculatedDistance;

    // time
    @Expose(deserialize = false,serialize = false)
    private  long ambulanceStartTime;
    @Expose(deserialize = false,serialize = false)
    private  long emergencyReachedTime;
    @Expose(deserialize = false,serialize = false)
    private  long hospitalReachedTime;


    @Expose(deserialize = false,serialize = false)
    private  String commandCenter;

    // indications whether new booking,admin transfered,ambulance transfered and admin created booking
    @Expose(deserialize = false,serialize = false)
    private  String bookingType;

    @Expose(deserialize = false,serialize = false)
    private  String tripCost;

    @Expose(deserialize = false,serialize = false)
    private  double speed;


    //patinet details
    @Expose(deserialize = false,serialize = false)
    private  String patientName;
    @Expose(deserialize = false,serialize = false)
    private  String patientContactNumber;
    @Expose(deserialize = false,serialize = false)
    private  String patientCondition;
    @Expose(deserialize = false,serialize = false)
    private  String patientAge;
    @Expose(deserialize = false,serialize = false)
    private  String patientGender;
    /**
     * @return The bookingId
     */

    @Expose(deserialize = false,serialize = false)
    private  long bookingReceivedTime;


    @SerializedName("is_medical_taxi")
    @Expose
    private boolean isMedicalTaxi;


    @SerializedName("device_type")
    @Expose
    private String deviceType="android";


    public String getBookingId() {
        return bookingId;
    }

    /**
     * @param bookingId The booking_id
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * @return The duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return The distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     * @return The latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The customerMobile
     */
    public String getCustomerMobile() {
        return customerMobile;
    }

    /**
     * @param customerMobile The customerMobile
     */
    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    /**
     * @return The customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName The customerName
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return The longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGcmToken() {
        return gcmToken;
    }

    public void setGcmToken(String gcmToken) {
        this.gcmToken = gcmToken;
    }

    public String getHospitalLatitude() {
        return hospitalLatitude;
    }

    public void setHospitalLatitude(String hospitalLatitude) {
        this.hospitalLatitude = hospitalLatitude;
    }

    public String getHospitalLongitude() {
        return hospitalLongitude;
    }

    public void setHospitalLongitude(String hospitalLongitude) {
        this.hospitalLongitude = hospitalLongitude;
    }

    public String getHospitalGeoHashCode() {
        return hospitalGeoHashCode;
    }

    public void setHospitalGeoHashCode(String hospitalGeoHashCode) {
        this.hospitalGeoHashCode = hospitalGeoHashCode;
    }

    public String getHospital() {
        return hospitalName;
    }

    public void setHospital(String hospital) {
        this.hospitalName = hospital;
    }

    public String getEmergencyType() {
        return emergencyType;
    }

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public String getCurrentBookingStatus() {
        return currentBookingStatus;
    }

    public void setCurrentBookingStatus(String currentBookingStatus) {
        this.currentBookingStatus = currentBookingStatus;
    }

    public double getAmbulanceStartLat() {
        return ambulanceStartLat;
    }

    public void setAmbulanceStartLat(double ambulanceStartLat) {
        this.ambulanceStartLat = ambulanceStartLat;
    }

    public double getAmbulanceStartLong() {
        return ambulanceStartLong;
    }

    public void setAmbulanceStartLong(double ambulanceStartLong) {
        this.ambulanceStartLong = ambulanceStartLong;
    }

    public long getAmbulanceStartTime() {
        return ambulanceStartTime;
    }

    public void setAmbulanceStartTime(long ambulanceStartTime) {
        this.ambulanceStartTime = ambulanceStartTime;
    }

    public long getEmergencyReachedTime() {
        return emergencyReachedTime;
    }

    public void setEmergencyReachedTime(long emergencyReachedTime) {
        this.emergencyReachedTime = emergencyReachedTime;
    }

    public long getHospitalReachedTime() {
        return hospitalReachedTime;
    }

    public void setHospitalReachedTime(long hospitalReachedTime) {
        this.hospitalReachedTime = hospitalReachedTime;
    }

    public String getGoogleDistanceFromAmbToEmergency() {
        return googleDistanceFromAmbToEmergency;
    }

    public void setGoogleDistanceFromAmbToEmergency(String googleDistanceFromAmbToEmergency) {
        this.googleDistanceFromAmbToEmergency = googleDistanceFromAmbToEmergency;
    }

    public String getGoogleDistanceFromEmergencyToHospital() {
        return googleDistanceFromEmergencyToHospital;
    }

    public void setGoogleDistanceFromEmergencyToHospital(String googleDistanceFromEmergencyToHospital) {
        this.googleDistanceFromEmergencyToHospital = googleDistanceFromEmergencyToHospital;
    }


    public String getEstimateTimeFromAmbulanceToEmergency() {
        return estimateTimeFromAmbulanceToEmergency;
    }

    public void setEstimateTimeFromAmbulanceToEmergency(String estimateTimeFromAmbulanceToEmergency) {
        this.estimateTimeFromAmbulanceToEmergency = estimateTimeFromAmbulanceToEmergency;
    }

    public String getEstimateTimeFromEmergencyToHospital() {
        return estimateTimeFromEmergencyToHospital;
    }

    public void setEstimateTimeFromEmergencyToHospital(String estimateTimeFromEmergencyToHospital) {
        this.estimateTimeFromEmergencyToHospital = estimateTimeFromEmergencyToHospital;
    }

    public String getCommandCenter() {
        return commandCenter;
    }

    public void setCommandCenter(String commandCenter) {
        this.commandCenter = commandCenter;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientContactNumber() {
        return patientContactNumber;
    }

    public void setPatientContactNumber(String patientContactNumber) {
        this.patientContactNumber = patientContactNumber;
    }

    public String getPatientCondition() {
        return patientCondition;
    }

    public void setPatientCondition(String patientCondition) {
        this.patientCondition = patientCondition;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getGoogleDistanceFromAmbToEmergencyValue() {
        return googleDistanceFromAmbToEmergencyValue;
    }

    public void setGoogleDistanceFromAmbToEmergencyValue(String googleDistanceFromAmbToEmergencyValue) {
        this.googleDistanceFromAmbToEmergencyValue = googleDistanceFromAmbToEmergencyValue;
    }

    public String getGoogleDistanceFromEmergencyToHospitalValue() {
        return googleDistanceFromEmergencyToHospitalValue;
    }

    public void setGoogleDistanceFromEmergencyToHospitalValue(String googleDistanceFromEmergencyToHospitalValue) {
        this.googleDistanceFromEmergencyToHospitalValue = googleDistanceFromEmergencyToHospitalValue;
    }

    public String getTripCost() {
        return tripCost;
    }

    public void setTripCost(String tripCost) {
        this.tripCost = tripCost;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public long getBookingReceivedTime() {
        return bookingReceivedTime;
    }

    public void setBookingReceivedTime(long bookingReceivedTime) {
        this.bookingReceivedTime = bookingReceivedTime;
    }


    public double getCalculatedDistanceFromAmbToEmergencyValue() {
        return calculatedDistanceFromAmbToEmergencyValue;
    }

    public void setCalculatedDistanceFromAmbToEmergencyValue(double calculatedDistanceFromAmbToEmergencyValue) {
        this.calculatedDistanceFromAmbToEmergencyValue = calculatedDistanceFromAmbToEmergencyValue;
    }

    public double getCalculatedDistanceFromEmergencyToHospitalValue() {
        return calculatedDistanceFromEmergencyToHospitalValue;
    }

    public void setCalculatedDistanceFromEmergencyToHospitalValue(double calculatedDistanceFromEmergencyToHospitalValue) {
        this.calculatedDistanceFromEmergencyToHospitalValue = calculatedDistanceFromEmergencyToHospitalValue;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalNumber() {
        return hospitalNumber;
    }

    public void setHospitalNumber(String hospitalNumber) {
        this.hospitalNumber = hospitalNumber;
    }

    public double getTotalCalculatedDistance() {
        return totalCalculatedDistance;
    }

    public void setTotalCalculatedDistance(double totalCalculatedDistance) {
        this.totalCalculatedDistance = totalCalculatedDistance;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }


    public boolean getIsDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(boolean dropLocation) {
        this.dropLocation = dropLocation;
    }


    public boolean isDropLocation() {
        return dropLocation;
    }

    public boolean isMedicalTaxi() {
        return isMedicalTaxi;
    }

    public void setMedicalTaxi(boolean medicalTaxi) {
        isMedicalTaxi = medicalTaxi;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
