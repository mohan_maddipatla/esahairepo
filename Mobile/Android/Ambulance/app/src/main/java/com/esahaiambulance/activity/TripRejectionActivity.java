package com.esahaiambulance.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esahaiambulance.R;
import com.esahaiambulance.adapters.TripRejectionReasonAdapter;
import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 19/9/16.
 */

public class TripRejectionActivity extends AppCompatActivity {

    Context mContext;
    Spinner spinnerEmergencyType;
    RecyclerView recyclerView;
    Toolbar toolbar;
    TextView txtTitle;
    String[] tripRejectList;

    Button btnSubmit;


    TripRejectionReasonAdapter tripRejectionReasonAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_rejection);
        Log.d("onCreate", "onCreate");
        mContext = this;
        initViews();
        tripRejectList = getResources().getStringArray(R.array.tripRejectionType);

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
                .createFromResource(this, R.array.emergencyType,
                        R.layout.my_spinner_layout);
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmergencyType.setAdapter(spinnerAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        tripRejectionReasonAdapter = new TripRejectionReasonAdapter(this, tripRejectList);
        recyclerView.setAdapter(tripRejectionReasonAdapter);
    }

    private void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText( Common.getAmbulanceNumber(this));

        ImageView img = (ImageView)toolbar.findViewById(R.id.img_toolbarimg);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        spinnerEmergencyType = (Spinner) findViewById(R.id.spinnerEmergencyType);
        recyclerView = (RecyclerView) findViewById(R.id.reclyerView);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tripRejectionReasonAdapter.mCheckedPostion != -1) {
                    Intent intent = new Intent(mContext, DashboardActivity.class);
                    if (getIntent().hasExtra("isFromNewTrip")) {
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);

                    } else {
                        setResult(RESULT_OK);
                        finish();
                    }


                } else {
                    Toast.makeText(mContext, "Please select the reason to reject", Toast.LENGTH_SHORT).show();
                    recyclerView.requestFocus();
                }
            }
        });
    }

    private void sendReasonToServer() {

        if (Common.isNetworkAvailable(this)) {


            HashMap<String, Object> map = new HashMap<>();
            map.put("reasonToReject", tripRejectList[tripRejectionReasonAdapter.mCheckedPostion]);
            map.put("emergencyType", "" + spinnerEmergencyType.getSelectedItem());


            Common.displayProgress(this);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            //Log.d("response", "user id " + Common.getUserId(this));
            //  Log.d("response", "access token " + Common.getAccessToken(this));

            Call<JsonElement> call2 = apiService.sendBookingRejectionReason(map, Common.getUserId(this), Common.getAccessToken(this));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            Intent intent = new Intent(mContext, DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);


                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(mContext);

                        } else {
                            Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                    Common.displayAlertDialog(mContext, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }

    }

    @Override
    public void onBackPressed() {

    }
}
