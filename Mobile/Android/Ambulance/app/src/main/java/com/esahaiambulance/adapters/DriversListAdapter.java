package com.esahaiambulance.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.esahaiambulance.activity.MyApplication;
import com.esahaiambulance.activity.TripRejectionActivity;
import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.R;
import com.esahaiambulance.activity.DriverAuthenticationActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by prashanth on 19/9/16.
 */

public class DriversListAdapter extends RecyclerView.Adapter<DriversListAdapter.ViewHolder> {


    private Context context;
    private ArrayList<DriverDetails> arrayList;


    public DriversListAdapter(Context context, ArrayList<DriverDetails> arrayList) {

        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_list_reclyer_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        DriverDetails driver = arrayList.get(position);
        holder.txtName.setText(driver.getDriverName());
        Picasso.with(context).load(driver.getFileUrl()).into(holder.profilePic);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public DriverDetails getItem(int position) {
        return arrayList.get(position);
    }

    public void changeList( ArrayList<DriverDetails> arrayList){

        this.arrayList =arrayList;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView txtName;
        CircleImageView profilePic;


        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            txtName = (TextView) view.findViewById(R.id.txtName);
            profilePic = (CircleImageView) view.findViewById(R.id.profile_image);

        }

        @Override
        public void onClick(View view) {
           // Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(view.getContext(), DriverAuthenticationActivity.class);
            intent.putExtra("driverDetails", arrayList.get(getPosition()));
            context.startActivity(intent);
            MyApplication.getApplication().trackEvent("Driver list","Driver login ",""+arrayList.get(getPosition()).getDriverMobileNumber());
        }

    }
}
