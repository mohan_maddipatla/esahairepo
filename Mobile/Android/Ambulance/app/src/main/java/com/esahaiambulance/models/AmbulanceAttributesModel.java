package com.esahaiambulance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by prashanth on 4/10/16.
 */

public class AmbulanceAttributesModel {

    @SerializedName("attribute_name")
    @Expose
    private String attributeName;
    @SerializedName("attribute_details")
    @Expose
    private String attributeDetails;

    /**
     * @return The attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName The attribute_name
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     * @return The attributeDetails
     */
    public String getAttributeDetails() {
        return attributeDetails;
    }

    /**
     * @param attributeDetails The attribute_details
     */
    public void setAttributeDetails(String attributeDetails) {
        this.attributeDetails = attributeDetails;
    }

}

