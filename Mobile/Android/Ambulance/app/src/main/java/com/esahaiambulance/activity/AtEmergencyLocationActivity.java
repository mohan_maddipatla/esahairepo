package com.esahaiambulance.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.adapters.DriversListAdapter;
import com.esahaiambulance.db.DatabaseHelper;
import com.esahaiambulance.models.BookingDetails;
import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.models.EmergencyType;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 23/9/16.
 */

@SuppressWarnings({"MissingPermission"})
public class AtEmergencyLocationActivity extends AppCompatActivity {


    Toolbar toolbar;
    TextView txtTitle;
    TextView txtStatus;
    Button btnOnWayToHospital;
    Spinner spinnerEmergencyType;
    ArrayList<EmergencyType> emergencyTypeArrayList;
    Location currentLocation;
    private LocationManager locationManager;
    Context mContext;
    BookingDetails bookingDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_at_emergency_location);
        initViews();
        mContext = this;
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this);

        emergencyTypeArrayList = dbHelper.getEmergencyTypes();

        String [] emergencyArray = new String[emergencyTypeArrayList.size()];

        for(int i=0;i<emergencyTypeArrayList.size();i++){
            emergencyArray[i] = emergencyTypeArrayList.get(i).getTypeName();
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this,  R.layout.my_spinner_layout, emergencyArray);
      /*  ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
                .createFromResource(this, emergencyArray,
                        R.layout.my_spinner_layout);*/
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmergencyType.setAdapter(spinnerArrayAdapter);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (currentLocation == null) {
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        bookingDetails = (BookingDetails) getIntent().getSerializableExtra("bookingDetails");
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText(getString(R.string.app_name));

        txtStatus = (TextView) findViewById(R.id.txtStatus);

        btnOnWayToHospital = (Button) findViewById(R.id.btnOnWayToHospital);
        spinnerEmergencyType = (Spinner) findViewById(R.id.spinnerEmergencyType);

        btnOnWayToHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, Object> map = new HashMap<>();
                map.put("booking_status", "Way back to Hospital");
                // starting the trip
               // isBookingInProgress = true;
                updateDeviceStatus(true, map, 1);
            }
        });


    }

    private void updateDeviceStatus(final boolean isShowProgressDialog, HashMap<String, Object> map, final int type) {

        map.put(Constants.latitude, currentLocation.getLatitude());
        map.put(Constants.longitude, currentLocation.getLongitude());
        map.put(Constants.geoHashCode, Common.getGeoHashCode(currentLocation));
        map.put("ambulanceId", Common.getAmbulanceId(mContext));
        map.put(Constants.driverId, Common.getDriverId(mContext));
        map.put(Constants.ambulanceApp, true);

        if (bookingDetails != null && type != -1) {
            map.put("is_booked", true);
            map.put("booking_id", bookingDetails.getBookingId());

            map.put("end_latitude", bookingDetails.getHospitalLatitude());
            map.put("end_longitude", bookingDetails.getHospitalLongitude());
            map.put("end_geoHashCode", bookingDetails.getHospitalGeoHashCode());
        }

        Log.d("req", "req = " + Arrays.asList(map));
        if (type != -1) {
          //  isSendingStatusUpdate = true;
        }
        if (Common.isNetworkAvailable(mContext)) {

            if (isShowProgressDialog) {
                Common.displayProgress(mContext);
            }
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<JsonElement> call = apiService.updateAmbulanceStatus(map, Common.getUserId(mContext), Common.getAccessToken(mContext));

            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    if (isShowProgressDialog) {
                        Common.stopProgressDialog();
                    }

                    try {
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                      //  isSendingStatusUpdate = false;

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            Intent intent = new Intent();
                            intent.putExtra("MESSAGE", "");
                            setResult(RESULT_OK, intent);
                            finish();

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(mContext);

                        } else {
                            Common.displayAlertDialog(mContext, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    if (isShowProgressDialog) {
                        Common.stopProgressDialog();
                    }
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    Common.displayAlertDialog(mContext, mContext.getString(R.string.networkError));
                }
            });
        } else {

            Common.displayAlertDialog(mContext, mContext.getString(R.string.offline));
        }
    }


}
