package com.esahaiambulance.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.esahaiambulance.adapters.DriversListAdapter;
import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.R;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverListActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtTitle;
    TextView txtStatus, txtNoDrivers;
    RecyclerView recyclerView;
    ArrayList<DriverDetails> arrayListOfDrivers;
    DriversListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_list);
        initViews();
        arrayListOfDrivers = new ArrayList<>();
        //createDriverList();
        GridLayoutManager layoutManager = new GridLayoutManager(DriverListActivity.this, 2);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new DriversListAdapter(this, arrayListOfDrivers);
        recyclerView.setAdapter(adapter);

        getDriversList();

        // recyclerView.
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);

        txtTitle.setText(Common.getAmbulanceNumber(this));

        txtStatus = (TextView) findViewById(R.id.txtStatus);
        txtNoDrivers = (TextView) findViewById(R.id.txtNoDrivers);


        recyclerView = (RecyclerView) findViewById(R.id.reclyerView);

    }

    private void createDriverList() {
        arrayListOfDrivers = new ArrayList<>();
        DriverDetails driver = new DriverDetails();
        driver.setDriverName("Prashanth");
        driver.setFileUrl("http://i.imgur.com/DvpvklR.png");
        for (int i = 0; i < 10; i++) {
            arrayListOfDrivers.add(driver);
        }

    }

    private void getDriversList() {
        if (Common.isNetworkAvailable(this)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("ambulanceId", Common.getAmbulanceId(this));
            map.put("ambulanceApp", "true");

            System.out.println("req = " + Arrays.asList(map));
            //  Log.d("mobile","after encrypt mobile"+Common.encrypt(etMobileNumber.getText().toString()));


            Common.displayProgress(this);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Log.d("response", "user id " + Common.getUserId(this));
            Log.d("response", "access token " + Common.getAccessToken(this));

            Call<JsonElement> call2 = apiService.getDriversListByAmbulanceId(map, Common.getUserId(this), Common.getAccessToken(this));
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    Common.stopProgressDialog();
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            JSONArray arrayDriversList = response.getJSONArray("responseData");
                            if (arrayDriversList.length() > 0) {
                               /* for (int i = 0; i < arrayDriversList.length(); i++) {
                                    JSONObject jsonDriver = arrayDriversList.getJSONObject(i);

                                }*/

                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<DriverDetails>>() {
                                }.getType();

                                arrayListOfDrivers = gson.fromJson("" + arrayDriversList, listType);
                                adapter.changeList(arrayListOfDrivers);
                            } else {
                                txtNoDrivers.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);

                            }

                        } else if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseInvalidToken)) {
                            Common.doLogOut(DriverListActivity.this);

                        } else {
                            Common.displayAlertDialog(DriverListActivity.this, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Common.stopProgressDialog();
                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    Common.displayAlertDialog(DriverListActivity.this, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }



}
