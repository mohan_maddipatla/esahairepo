package com.esahaiambulance.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.esahaiambulance.R;
import com.esahaiambulance.db.DatabaseHelper;
import com.esahaiambulance.models.DriverDetails;
import com.esahaiambulance.models.EmergencyType;
import com.esahaiambulance.utils.Common;
import com.esahaiambulance.utils.Constants;
import com.esahaiambulance.utils.MyAlertListener;
import com.esahaiambulance.webservices.ApiClient;
import com.esahaiambulance.webservices.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreen extends AppCompatActivity {


     Context mContext;
     DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        mContext = this;

        Common.sendRegistrationToServer(this.getApplicationContext());
        Log.d("TripProgre","TripProgressService.IS_SERVICE_RUNNING  = "+TripProgressService.IS_SERVICE_RUNNING);
        Log.d("TripProgre","TripProgressService.bookingDetails  = "+TripProgressService.bookingDetails);
        if (TripProgressService.IS_SERVICE_RUNNING && TripProgressService.bookingDetails != null) {
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.putExtra(Constants.bookingDetails, TripProgressService.bookingDetails);
            intent.putExtra("isFromService", true);
            startActivity(intent);
            finish();

            Log.d("status","status***"+TripProgressService.bookingDetails.getCurrentBookingStatus());

        }else if(Common.getBookingDetails(this) != null){

            Log.d("booking from share","booking from shared pref");
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.putExtra(Constants.bookingDetails, Common.getBookingDetails(this));
            intent.putExtra("isFromService", true);
            startActivity(intent);
            finish();
        }else {
            checkAppVersion();
        }


    }

    private void moveToDriversListScreen() {

        Intent intent = new Intent(mContext, DriverListActivity.class);
        startActivity(intent);
        finish();
    }

    private void moveToLoginScreen() {

        Intent intent = new Intent(mContext, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void moveToDashBoardScreen() {

        Intent intent = new Intent(mContext, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    private void getEmergencyTypeList() {
        if (Common.isNetworkAvailable(this)) {
            HashMap<String, Object> map = new HashMap<>();
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<JsonElement> call2 = apiService.getEmergencyTypeList(map);
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {
                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {

                            JSONArray arrayDriversList = response.getJSONArray("responseData");
                            if (arrayDriversList.length() > 0) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<EmergencyType>>() {
                                }.getType();
                                ArrayList<EmergencyType> arrayList = gson.fromJson("" + arrayDriversList, listType);


                                for (EmergencyType item : arrayList) {

                                    dbHelper.insertEmergencyType(item.getTypeName(), item.getId());
                                }

                            }
                            //dbHelper.getEmergencyTypes();
                            Common.saveBooleanPref(mContext, Constants.isEmergencyListFeteched, true);
                            moveToLoginScreen();

                        }

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Log.e("eroooooooorr", throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                    Common.displayAlertDialog(SplashScreen.this, getString(R.string.networkError));
                }
            });
        } else {
            Common.displayAlertDialog(this, getString(R.string.offline));
        }
    }

    private void checkAppVersion() {
        if (Common.isNetworkAvailable(this)) {

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call<JsonElement> call2 = apiService.getAppVersion("Ambulance_Android");
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.d("response", "response = 222" + serverResponse.body());
                        System.out.print("response = 22222" + serverResponse.body());

                        if (response.getString(Constants.statusCode).equalsIgnoreCase(Constants.responseStatusSuccess)) {
                            JSONObject details = response.getJSONObject("responseData");
                            int serverVersion = details.getInt("version_no");
                            int localVersion = getAppVersion();
                            Log.d("versions","app versions"+serverVersion+" "+localVersion);
                            if (serverVersion <= localVersion ) {

                                checkInitialConditionsAndLoad();
                            } else {
                                if (details.getInt("force_upgrade") == 1) {

                                    // force upgrade
                                    forceUpdateOrShowMessage(SplashScreen.this, 1, getResources().getString(R.string.app_not_supported));
                                } else {

                                    // update to get new features
                                    forceUpdateOrShowMessage(SplashScreen.this, 2, getResources().getString(R.string.update_to_get_new_features));
                                }
                            }


                        } else {
                            Common.displayAlertDialog(SplashScreen.this, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
                    Common.simpleSingleButtonAlertDialog(SplashScreen.this, getString(R.string.networkError), new MyAlertListener() {
                        @Override
                        public void buttonClicked(int type) {
                            SplashScreen.this.finish();
                        }
                    });
                }
            });
        } else {
           // Common.displayAlertDialog(this, getString(R.string.offline));
            Common.simpleSingleButtonAlertDialog(SplashScreen.this, getString(R.string.offline), new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {
                    SplashScreen.this.finish();
                }
            });
        }
    }

    public int getAppVersion() {
        int versionCode = 1;
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return versionCode;
    }

    private void checkInitialConditionsAndLoad() {

        if (Common.getBooleanPerf(this, Constants.isDeviceLoggedIn, false)) {
            if (Common.getBooleanPerf(this, Constants.isDriverLoggedIn, false)) {

                moveToDashBoardScreen();
            } else {
                moveToDriversListScreen();
            }

        } else {
            moveToLoginScreen();
        }
    }

    private void forceUpdateOrShowMessage(final Context context, int buttonCount, String message) {
        simpleSingleButtonAlertDialogForceUpdate(SplashScreen.this, "", message, new MyAlertListener() {
            @Override
            public void buttonClicked(int type) {

                if (type == 0) {
                    Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName()));
                    boolean marketFound = false;

                    // find all applications able to handle our rateIntent
                    final List<ResolveInfo> otherApps = context.getPackageManager().queryIntentActivities(rateIntent, 0);
                    for (ResolveInfo otherApp : otherApps) {
                        // look for Google Play application
                        if (otherApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {

                            ActivityInfo otherAppActivity = otherApp.activityInfo;
                            ComponentName componentName = new ComponentName(
                                    otherAppActivity.applicationInfo.packageName,
                                    otherAppActivity.name
                            );
                            rateIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                            rateIntent.setComponent(componentName);
                            context.startActivity(rateIntent);
                            marketFound = true;
                            break;

                        }
                    }

                    // if GP not present on device, open web browser
                    if (!marketFound) {
                        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName()));
                        context.startActivity(webIntent);
                    }
                    SplashScreen.this.finish();
                } else {
                    checkInitialConditionsAndLoad();
                }
            }

        }, buttonCount);

    }

    private void simpleSingleButtonAlertDialogForceUpdate(final Context ctx, String title, String message, final MyAlertListener myAlertListener, int showButtonCount) {
        // custom dialog
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.force_update_dialog);
        Button btnYes = (Button) dialog.findViewById(R.id.btnPossitive);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNegative);

        if (showButtonCount == 1) {
            btnNo.setVisibility(View.GONE);
        }

        TextView textStatus = (TextView) dialog.findViewById(R.id.txtMsg);
        textStatus.setText(message);

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                myAlertListener.buttonClicked(0);


            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                myAlertListener.buttonClicked(1);


            }
        });

        if (!dialog.isShowing())
            dialog.show();
    }
}

