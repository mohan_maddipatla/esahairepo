package com.esahaiambulance.webservices;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by prashanth on 21/11/16.
 */

public abstract class CallbackWithRetry<T> implements Callback<T> {

    public static final int TOTAL_RETRIES = 3;
    public static final String TAG = "CallbackWithRetry";
    public final Call<T> call;
    public int retryCount = 0;

    public CallbackWithRetry(Call<T> call) {
        this.call = call;
    }

  /*  @Override
    public void onFailure(Throwable t) {
        Log.e(TAG, t.getLocalizedMessage());
        if (retryCount++ < TOTAL_RETRIES) {
            Log.v(TAG, "Retrying... (" + retryCount + " out of " + TOTAL_RETRIES + ")");
            retry();
        }
    }*/

    public void retry() {
        call.clone().enqueue(this);
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.e(TAG, t.getLocalizedMessage());
        if (retryCount++ < TOTAL_RETRIES) {
            Log.v(TAG, "Retrying... (" + retryCount + " out of " + TOTAL_RETRIES + ")");
            retry();
        }
    }
}
