package com.esahaiambulance.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.esahaiambulance.models.AmbulanceAttributesModel;
import com.esahaiambulance.models.EmergencyType;

import java.util.ArrayList;

/**
 * Created by prashanth on 30/9/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "esahai_ambulance.db";
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper sInstance;

    public class EmergencyTypes {
        public static final String TABLE_NAME = "EmergencyTypes";
        public static final String COL_NAME = "type_name";
        public static final String COL_ID = "id";

    }

    public class AmbulanceAttributes {
        public static final String TABLE_NAME = "AmbulanceAttributes";
        public static final String COL_ID = "id";
        public static final String COL_ATTRIBUTE_NAME = "attribute_name";
        public static final String COL_ATTRIBUTE_DETAILS = "attribute_details";

    }


    public static synchronized DatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_EMERGENCY_TYPE_TABLE = "CREATE TABLE " + EmergencyTypes.TABLE_NAME +
                "(" + EmergencyTypes.COL_ID + " TEXT," + EmergencyTypes.COL_NAME + " TEXT" + ")";


        String ATTRIBUTES_TABLE = "CREATE TABLE " + AmbulanceAttributes.TABLE_NAME + "(" + AmbulanceAttributes.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + AmbulanceAttributes.COL_ATTRIBUTE_NAME
                + " TEXT," + AmbulanceAttributes.COL_ATTRIBUTE_DETAILS + " TEXT" +

                ")";
//        String CREATE_LANGUAGE_TABLE = "CREATE TABLE " + LANGUAGE_TABLE +
//                "("+LANGUAGE_COL_NAME+" TEXT"+")";
        db.execSQL(CREATE_EMERGENCY_TYPE_TABLE);
        db.execSQL(ATTRIBUTES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + EmergencyTypes.TABLE_NAME);
        //db.execSQL("DROP TABLE IF EXISTS " + CHANNEL_TABLE);
        onCreate(db);
    }

    public void insertEmergencyType(String name, String id) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(EmergencyTypes.COL_NAME, name);
            values.put(EmergencyTypes.COL_ID, id);

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(EmergencyTypes.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("", "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }

    }

    public void insertAmbulanceAttributes(String name, String details) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(AmbulanceAttributes.COL_ATTRIBUTE_NAME, name);
            values.put(AmbulanceAttributes.COL_ATTRIBUTE_DETAILS, details);

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(AmbulanceAttributes.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("", "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }

    }

 /*   public long getLanguageCount() {
        SQLiteDatabase db = getReadableDatabase();
        Log.d("language count","language =  "+DatabaseUtils.queryNumEntries(db, LANGUAGE_TABLE));
        return DatabaseUtils.queryNumEntries(db, LANGUAGE_TABLE);
    }*/

    public ArrayList<EmergencyType> getEmergencyTypes() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<EmergencyType> typesList = new ArrayList<>();
        db.beginTransaction();
        try {
            Cursor cursor = db.query(EmergencyTypes.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    EmergencyType type = new EmergencyType();
                    type.setId(cursor.getString(0));
                    type.setTypeName(cursor.getString(1));
                    typesList.add(type);
                } while (cursor.moveToNext());

            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("", "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
        Log.d("getEmergencyTypes", "getEmergencyTypes ==" + typesList.size());
        return typesList;
    }

    public ArrayList<AmbulanceAttributesModel> getAmbulanceAttributes() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<AmbulanceAttributesModel> typesList = new ArrayList<>();
        db.beginTransaction();
        try {
            Cursor cursor = db.query(AmbulanceAttributes.TABLE_NAME, null, null, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    AmbulanceAttributesModel type = new AmbulanceAttributesModel();
                    type.setAttributeName(cursor.getString(1));
                    type.setAttributeDetails(cursor.getString(2));
                    typesList.add(type);
                } while (cursor.moveToNext());

            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("", "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
        Log.d("getAmbulanceAttributes", "getAmbulanceAttributes ==" + typesList.size());
        return typesList;
    }

    public void clearEmergencyTypes() {
        SQLiteDatabase db = getWritableDatabase();
        ArrayList<AmbulanceAttributesModel> typesList = new ArrayList<>();
        db.beginTransaction();
        try {
            db.execSQL("delete from " + EmergencyTypes.TABLE_NAME);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            db.endTransaction();
        }
    }

    public void clearAttributes() {
        SQLiteDatabase db = getWritableDatabase();
        ArrayList<AmbulanceAttributesModel> typesList = new ArrayList<>();
        db.beginTransaction();
        try {
            db.execSQL("delete from " + AmbulanceAttributes.TABLE_NAME);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            db.endTransaction();
        }
    }

}
