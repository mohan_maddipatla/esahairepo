package com.esahai.dataObjects;

/**
 * Created by ragamai on 17/2/17.
 */
public class HospitalAttributes {

    /*
    "id": "27",
			"attribute_name": "Test Update Attribute",
			"attribute_details": "dsdfa"

     */

    public String id;
    public String attribute_name;
    public String attribute_details;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAttribute_name() {
        return attribute_name;
    }

    public void setAttribute_name(String attribute_name) {
        this.attribute_name = attribute_name;
    }

    public String getAttribute_details() {
        return attribute_details;
    }

    public void setAttribute_details(String attribute_details) {
        this.attribute_details = attribute_details;
    }

    @Override
    public String toString() {
        return "HospitalAttributes{" +
                "id='" + id + '\'' +
                ", attribute_name='" + attribute_name + '\'' +
                ", attribute_details='" + attribute_details + '\'' +
                '}';
    }
}
