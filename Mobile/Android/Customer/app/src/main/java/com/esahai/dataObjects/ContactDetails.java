package com.esahai.dataObjects;

/**
 * Created by ragamai on 13/10/16.
 */
public class ContactDetails {
    /*
    "id": "1",
 		"customer_id": "92322442",
 		"contact_person": "ramya",
 		"relation": "Cousin",
 		"mobile_number": "9264000999"
     */

    public String id;
    public String customer_id;
    public String contact_person;
    public String relation;
    public String mobile_number;

    public int is_notify;

    public int getIs_notify() {
        return is_notify;
    }

    public void setIs_notify(int is_notify) {
        this.is_notify = is_notify;
    }






    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }


    @Override
    public String toString() {
        return "ContactDetails{" +
                "id='" + id + '\'' +
                ", customer_id='" + customer_id + '\'' +
                ", contact_person='" + contact_person + '\'' +
                ", relation='" + relation + '\'' +
                ", mobile_number='" + mobile_number + '\'' +
                ", is_notify='" + is_notify + '\'' +
                '}';
    }
}
