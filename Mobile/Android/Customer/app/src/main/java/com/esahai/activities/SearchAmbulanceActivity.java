package com.esahai.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.BookingStatus;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ragamai on 16/9/16.
 */
public class SearchAmbulanceActivity extends CommonActivity {

//    Button button_cancel,button_count;
    ImageView button_cancel;
    CountDownTimer handler = null;
    double latitude,longitude;
    ApiInterface apiService;
    AppPreferences preferences;
    boolean assigning = false;
    AVLoadingIndicatorView avi;
    TextView textView_finding;
    ImageView imageView_ambulance;
    AlertDialog alert;
    long millis = 60000;//35000;
    long onPauseTime,onResumeTime;
    boolean isNetWorkCallInProgress = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_ambulance);

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText("Search Ambulance");

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);*/

//        text_ambulances_count = (TextView)findViewById(R.id.text_ambulances_count);

        preferences = new AppPreferences(SearchAmbulanceActivity.this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        avi = (AVLoadingIndicatorView)findViewById(R.id.avi);

        /*try{
            latitude = getIntent().getExtras().getDouble("latitude");
            longitude = getIntent().getExtras().getDouble("longitude");
        }catch (Exception e){e.printStackTrace();}*/

        try{
            if(getIntent().hasExtra("assigning")){
                assigning = true;
            }
        }catch (Exception e){e.printStackTrace();}


        button_cancel = (ImageView)findViewById(R.id.button_cancel);
        textView_finding = (TextView)findViewById(R.id.textView_finding);
        imageView_ambulance = (ImageView)findViewById(R.id.imageView_ambulance);
        /*button_count = (Button)findViewById(R.id.button_count);*/

        if(preferences.isMedicalTaxi()){
            textView_finding.setText(getResources().getString(R.string.finding_medical_taxi));
        }
        else{
            textView_finding.setText(getResources().getString(R.string.finding_ambulance));
        }


        if(assigning ==  true){

            if(preferences.isMedicalTaxi()){
                textView_finding.setText(getResources().getString(R.string.finding_medical_taxi_cancelled_msg));
            }
            else{
                textView_finding.setText(getResources().getString(R.string.finding_ambulance_cancelled_msg));
            }

        }else {
            try {
//                callNetworkMethod();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        avi.show();
        setupAnimObject();

        try {
            handler = new CountDownTimer(millis, 1000) {

                public void onTick(long millisUntilFinished) {
//                    button_count.setText("" + millisUntilFinished / 1000);
                    Log.i("timer","" + millisUntilFinished / 1000);

                }

                public void onFinish() {
                   /* avi.hide();
                    stopAnimation(imageView_ambulance);
                    if(alert!=null){
                        alert.dismiss();
                    }
                    if(assigning ==  true) {
                    Intent confirmed = new Intent(SearchAmbulanceActivity.this, DashboardActivity.class);
                    startActivity(confirmed);
                    }
                    preferences.setNoAmbulance(true);
                    finish();*/
                    networkCallForBookingStatus(2);
                }

            };
        }
        catch (Exception e){
            e.printStackTrace();
        }
        handler.start();

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showAlertDialog();
                }catch (Exception e){e.printStackTrace();}
            }
        });

    }


    private void showAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.cancel_booking_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        networkCallForCancelBooking();

                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void setupAnimObject() {
        try {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            Log.i("width", "" + width);

            Animation animation = new TranslateAnimation(-5, width, 0, 0);
            animation.setDuration(3000);
            animation.setRepeatCount(Animation.INFINITE);
            if(preferences.isMedicalTaxi()) {
                imageView_ambulance.setImageResource(R.drawable.medical_taxi_img);
            }
            imageView_ambulance.startAnimation(animation);
        }catch (Exception e){e.printStackTrace();}

    }

    public void stopAnimation(View v) {
        v.clearAnimation();

    }

    private void networkCallForCancelBooking() {
        try{
            if(Utils.isNetworkAvailable(SearchAmbulanceActivity.this)){
                final HashMap<String,Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id",preferences.getCustomerId());
                bookingDetails.put("booking_id",preferences.getBookingId());
                bookingDetails.put("ambulanceApp",false);
                bookingDetails.put("customerApp",true);
                bookingDetails.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

                Log.i("details for cancelling", Arrays.asList(bookingDetails).toString());

                Call<StatusResponse> cancelBooking = apiService.cancelBooking(bookingDetails,preferences.getCustomerId(),preferences.getAccessToken());
                Utils.showProgressDialog(SearchAmbulanceActivity.this);
                cancelBooking.enqueue(new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        Utils.hideProgressDialog();
                        try {
                            if(response.body()!=null) {
                                Log.i("cancel booking response", response.body().toString());
                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE1 ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE2 ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE3) {
                                    preferences.setTripState(false);
                                    preferences.setIsSelf(true);
                                    if (handler != null) {
                                        handler.cancel();
                                    }
                                    stopAnimation(imageView_ambulance);
                                    if(assigning ==  true) {

                                        if(preferences.isMedicalTaxi()){
                                            Intent confirmed = new Intent(SearchAmbulanceActivity.this, MedicalTaxiActivity.class);
                                            startActivity(confirmed);
                                        }
                                        else {
                                            Intent confirmed = new Intent(SearchAmbulanceActivity.this, EMSActivity.class);
                                            startActivity(confirmed);
                                        }
                                    }
//                                    Intent i = new Intent(SearchAmbulanceActivity.this,DashboardActivity.class);
//                                    startActivity(i);
                                    finish();
                                    Toast.makeText(SearchAmbulanceActivity.this, getResources().getString(R.string.booking_cancel_confirmation), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(SearchAmbulanceActivity.this, getResources().getString(R.string.booking_cancel_failure), Toast.LENGTH_SHORT).show();
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            Utils.showToast(SearchAmbulanceActivity.this,getResources().getString(R.string.network_error));
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        Utils.hideProgressDialog();
                        //Utils.showToast(SearchAmbulanceActivity.this,t.toString());
                        Utils.showToast(SearchAmbulanceActivity.this,getResources().getString(R.string.network_error));
                        preferences.setBookingId("");
                        finish();


                    }
                });





            }else{Utils.showToast(SearchAmbulanceActivity.this,getResources().getString(R.string.network_error));
            finish();}

        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("on destroy","true......................");

        try {
            if(handler!=null){
                handler.cancel();
            }
            preferences.setIsBooking(false);
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("onPause-->","SearchAmbulanceActivity onPause");

        /*if(handler!=null){
            handler.cancel();
        }*/
        
        try {
            preferences.setIsBooking(true);
        }catch (Exception e){e.printStackTrace();}
        onPauseTime = System.currentTimeMillis();

//        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("onResume-->","SearchAmbulanceActivity onResume");
        try{
            onResumeTime = System.currentTimeMillis();
            long remainingTime = (onResumeTime - onPauseTime)/1000;
            Log.i("backgroundtime",""+remainingTime);
        }catch (Exception e){e.printStackTrace();
        }

    }

    /* private void callNetworkMethod() {

        *//*
        {
          "emergencyGeoHash": "KzQZngf",
          "latitude": "89.0",
          "longitude": "0.909",
          "customerApp": false,
          "ambulanceApp": false
        }
         *//*

        if(Utils.isNetworkAvailable(SearchAmbulanceActivity.this)){

            String customerId = preferences.getCustomerId();
            String access_token = preferences.getAccessToken();

            Location location = new Location("");
            location.setLongitude(longitude);
            location.setLatitude(latitude);
            String geoHash = Utils.getGeoHashCode(location);

            HashMap<String, Object> request = new HashMap<>();
            request.put("emergencyGeoHash", geoHash);
            request.put("latitude", latitude);
            request.put("longitude", longitude);
            request.put("customerApp", true);
            request.put("ambulanceApp", "false");

            Call<NearByAmbulances> ambulancesList = apiService.searchNearByAmbulances(request,customerId,access_token);

            ambulancesList.enqueue(new Callback<NearByAmbulances>() {
                @Override
                public void onResponse(Call<NearByAmbulances> call, Response<NearByAmbulances> response) {

                        try {
                            if(response.body()!=null) {
                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                    Log.i("ambulances list", response.body().toString());
                                    ArrayList<AmbulanceDetails> ambulances = response.body().getResponseData();
                                    text_ambulances_count.setText("Found " + ambulances.size() + " ambulances");
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }



                }

                @Override
                public void onFailure(Call<NearByAmbulances> call, Throwable t) {

                }
            });



        }
        else{Utils.showToast(SearchAmbulanceActivity.this,"Check your Internet Connection");}




    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private void networkCallForBookingStatus( final int type) {

        Log.d("networkCallFor","networkCallForBookingStatus");
        try{
            if(Utils.isNetworkAvailable(SearchAmbulanceActivity.this)){
                HashMap<String,Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id",preferences.getCustomerId());
                bookingDetails.put("booking_id",preferences.getBookingId());
                bookingDetails.put("ambulanceApp",false);
                bookingDetails.put("customerApp",true);
                bookingDetails.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

                if(assigning){
                    Log.d("booking","transferred");
                    bookingDetails.put("transferred",true);
                }
                Log.i("details BookingStatus", Arrays.asList(bookingDetails).toString());

                Call<BookingStatus> bookingStatus = apiService.getBookingStatus(bookingDetails,preferences.getCustomerId(),preferences.getAccessToken());
                //Utils.showProgressDialog(SearchAmbulanceActivity.this);
                bookingStatus.enqueue(new Callback<BookingStatus>() {
                    @Override
                    public void onResponse(Call<BookingStatus> call, Response<BookingStatus> response) {
                        //Utils.hideProgressDialog();
                        try {
                            if(response.body()!=null) {
                                Log.i("stats booking response", response.body().toString());
                                //Log.i("stats booking response", response.body().getResponseData().toString());
                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE ) {
                                    preferences.setTripState(false);
                                    if (handler != null) {
                                        handler.cancel();
                                    }
                                    stopAnimation(imageView_ambulance);
                                    //BookingConfirmedDetails bookingDetails = response.body().getResponseData();

                                    Intent intent = new Intent(SearchAmbulanceActivity.this, TrackAmbulanceActivity.class);
                                    intent.putExtra("bookingDetails", response.body().getResponseData());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                } else {
                                    if(type ==2){
                                        avi.hide();
                                        stopAnimation(imageView_ambulance);
                                        if(alert!=null){
                                            alert.dismiss();
                                        }
                                        if(assigning ==  true) {

                                            if(preferences.isMedicalTaxi()){
                                                Intent confirmed = new Intent(SearchAmbulanceActivity.this, MedicalTaxiActivity.class);
                                                startActivity(confirmed);
                                            }
                                            else {
                                                Intent confirmed = new Intent(SearchAmbulanceActivity.this, EMSActivity.class);
                                                startActivity(confirmed);
                                            }
                                        }
                                        preferences.setNoAmbulance(true);
                                        finish();
                                    }else{
                                        Toast.makeText(SearchAmbulanceActivity.this, response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();

                                    }
                                                                    }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            Utils.showToast(SearchAmbulanceActivity.this,getResources().getString(R.string.network_error));
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<BookingStatus> call, Throwable t) {
                        //Utils.hideProgressDialog();
                        //Utils.showToast(SearchAmbulanceActivity.this,t.toString());
                        Utils.showToast(SearchAmbulanceActivity.this, getResources().getString(R.string.network_slow));
                        finish();
                    }
                });





            }else{Utils.showToast(SearchAmbulanceActivity.this,getResources().getString(R.string.network_error));
                finish();
            }

        }catch (Exception e){e.printStackTrace();}
    }
}
