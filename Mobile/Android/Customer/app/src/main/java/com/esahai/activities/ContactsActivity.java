package com.esahai.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.adapters.ContactsAdapter;
import com.esahai.dataObjects.ContactDetails;
import com.esahai.dataObjects.ContactsList;
import com.esahai.dataObjects.CountryCode;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ragamai on 16/9/16.
 */
public class ContactsActivity extends CommonActivity {

    Button button_submit;
    AppPreferences preference;
    ApiInterface apiInterface;
    RecyclerView recycle_list_contacts;
    ContactsAdapter recyclerViewAdapterForContacts;
    RecyclerView.LayoutManager recyclerViewLayoutManager;
    FloatingActionButton fActionButton;

    FloatingActionButton fabAddContactManually;
    FloatingActionButton fabAddContactFromPhbook;

    //Animations
    Animation show_fab_manual;
    Animation hide_fab_manual;
    Animation show_fab_Phbook;
    Animation hide_fab_Phbook;

    //Dialog edit text views
    EditText editText_name;
    EditText editText_contact;
    EditText editText_relation;
    EditText editText_cCode;
    CheckBox emergencyNotifyCheckbox;

    TextView textViewMessage;

    Spinner spinner_country_names;
    ArrayList<CountryCode> countryCodesList;
    ArrayList<String> COUNTRY_CODE;
    ArrayList<String> CountryNameList;

    String countryCode;

    ArrayList<ContactDetails> contacts;

    boolean isEditContact;


    Boolean isNotify;

    int PICK_CONTACT = 1;

    int PERMISSION_REQUEST_CONTACT = 100;

    private boolean FAB_Status;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_emergency_contacts);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.emergency_contacts));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        preference = new AppPreferences(ContactsActivity.this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        initViews();

        networkCallForGetContacts();

    }

    public void networkCallForGetContacts() {
        if (Utils.isNetworkAvailable(ContactsActivity.this)) {
            /*
            {
              "customerId": 12345678,
              "customerApp": false,
              "ambulanceApp": false
            }
             */
            HashMap<String, Object> details = new HashMap<>();
            details.put("customerId", preference.getCustomerId());
            details.put("customerApp", true);
            details.put("ambulanceApp", false);
            details.put("customer_mobile", Utils.encrypt(preference.getMobileNumber()));

            Log.i("contacts getReq", Arrays.asList(details).toString());

            Call<ContactsList> emergencyContacts = apiInterface.getEmergencyContacts(details,
                    preference.getCustomerId(),
                    preference.getAccessToken());
            Utils.showProgressDialog(ContactsActivity.this);
            emergencyContacts.enqueue(new Callback<ContactsList>() {
                @Override
                public void onResponse(Call<ContactsList> call, Response<ContactsList> response) {
                    Utils.hideProgressDialog();
                    try {
                        if (response.body() != null) {
                            Log.i("contacts data", response.body().toString());
                            contacts = response.body().getResponseData();
                            if (recyclerViewAdapterForContacts != null) {
                                recyclerViewAdapterForContacts = null;
                            }

                            recyclerViewAdapterForContacts = new ContactsAdapter(ContactsActivity.this,
                                    contacts, recycle_list_contacts);

                            fabAddContactManually.hide();
                            fabAddContactFromPhbook.hide();

                            recyclerViewAdapterForContacts.setOnItemClickListener(new ContactsAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(int position, ContactDetails contactDetails, String type) {

                                    if (type.equalsIgnoreCase("update")) {
                                        isEditContact = true;
                                        addContacts("", "", contacts.get(position));
                                    }
                                    if (type.equalsIgnoreCase("delete")) {

                                        showAlertDialog(contacts.get(position).getId());

                                        fActionButton.show();
                                        //fabAddContactManually.hide();
                                        //fabAddContactFromPhbook.hide();

                                        //fabAddContactFromPhbook.setVisibility(View.GONE);
                                        //fabAddContactManually.setVisibility(View.GONE);

                                    }

                                }
                            });

                            recycle_list_contacts.setAdapter(recyclerViewAdapterForContacts);


                            if (response.body().getResponseData().toString() == null ||
                                    response.body().getResponseData().isEmpty()) {

                                textViewMessage.setVisibility(View.VISIBLE);
                                recycle_list_contacts.setVisibility(View.GONE);

                                //Toast.makeText(ContactsActivity.this, getResources().getString(R.string.add_contact_message), Toast.LENGTH_LONG).show();
                            } else {
                                recycle_list_contacts.setVisibility(View.VISIBLE);
                                textViewMessage.setVisibility(View.GONE);
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ContactsList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(ContactsActivity.this, getResources().getString(R.string.network_slow));
                }
            });


        } else {
            Utils.showToast(ContactsActivity.this, getResources().getString(R.string.network_error));
        }
    }

    private void initViews() {


        button_submit = (Button) findViewById(R.id.button_submit);
        fActionButton = (FloatingActionButton) findViewById(R.id.fActionButton);

        fabAddContactManually = (FloatingActionButton) findViewById(R.id.fab_1);
        fabAddContactFromPhbook = (FloatingActionButton) findViewById(R.id.fab_2);

        textViewMessage = (TextView) findViewById(R.id.message_textview);

        //Animations
        show_fab_manual = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_show_manual);
        hide_fab_manual = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_hide_manual);
        show_fab_Phbook = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_show_phbook);
        hide_fab_Phbook = AnimationUtils.loadAnimation(getApplication(), R.anim.fab_hide_phbook);

        recycle_list_contacts = (RecyclerView) findViewById(R.id.recycle_list_contacts);
        recyclerViewLayoutManager = new LinearLayoutManager(ContactsActivity.this);
//        recycle_list_contacts.addItemDecoration(new DividerItemDecoration(ContactsActivity.this, DividerItemDecoration.VERTICAL_LIST));

//        recycle_list_contacts.setHasFixedSize(true);
        recycle_list_contacts.setLayoutManager(recyclerViewLayoutManager);
//        recycle_list_contacts.setItemAnimator(new DefaultItemAnimator());

        recycle_list_contacts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    fActionButton.hide();
                    //fabAddContactManually.hide();
                    //fabAddContactFromPhbook.hide();

                    fabAddContactFromPhbook.setVisibility(View.GONE);
                    fabAddContactManually.setVisibility(View.GONE);

                } else if (dy < 0) {
                    fActionButton.show();
                }
            }
        });


        fActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("Contacts", "FabStatus::" + FAB_Status);

                if (FAB_Status == false) {
                    //Display FAB menu
                    expandFAB();
                    FAB_Status = true;
                } else {
                    //Close FAB menu
                    hideFAB();
                    FAB_Status = false;
                }

                //addContacts();
            }
        });

        fabAddContactManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFAB();
                FAB_Status = false;

                isEditContact = false;
                addContacts("", "", null);

            }
        });


        fabAddContactFromPhbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideFAB();
                FAB_Status = false;

                askForContactPermission();

            }
        });

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                validateViews();
//                getCombinedData();
            }
        });


    }


    private void addContacts(String name, String phNumber, final ContactDetails contactDetails) {

        AlertDialog.Builder builder;
        if (isEditContact) {
            builder = new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.update_contact));

        } else {
            builder = new AlertDialog.Builder(ContactsActivity.this)
                    .setTitle(getResources().getString(R.string.add_contact));

        }
        final FrameLayout frameView = new FrameLayout(ContactsActivity.this);
        builder.setView(frameView);

        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.add_contact, frameView);
        Button button_submit = (Button) dialoglayout.findViewById(R.id.button_submit);
        Button button_cancel = (Button) dialoglayout.findViewById(R.id.button_cancel);
        editText_name = (EditText) dialoglayout.findViewById(R.id.editText_name);
        editText_contact = (EditText) dialoglayout.findViewById(R.id.editText_contact);
        editText_relation = (EditText) dialoglayout.findViewById(R.id.editText_relation);

        editText_cCode = (EditText) dialoglayout.findViewById(R.id.etCountryCode);

        emergencyNotifyCheckbox = (CheckBox) dialoglayout.findViewById(R.id.check_box);

        spinner_country_names = (Spinner) dialoglayout.findViewById(R.id.spinner_country_code);

        button_submit.setVisibility(View.VISIBLE);
        button_cancel.setVisibility(View.VISIBLE);

        editText_name.setText("" + name);
        editText_contact.setText("" + phNumber);


        if (isEditContact) {
            editText_name.setText(contactDetails.getContact_person());
            if (contactDetails.getMobile_number() != null) {

                String mobile_number = contactDetails.getMobile_number();
                String[] parts = mobile_number.split("-");
                String mobNumber = "";
                if (parts.length > 1) {
                    countryCode = parts[0]; // +91
                    mobNumber = parts[1]; // 2323232323
                    editText_contact.setText(mobNumber);
                } else {
                    countryCode = "91";
                    mobNumber = parts[0];
                    int length = mobNumber.length();
                    mobNumber = length >= 12 ? mobNumber.substring(length - 10) : mobNumber;
                    editText_contact.setText(mobNumber);

                }


                /*String mobile_number = contactDetails.getMobile_number();
                if(mobile_number.length() == 12){mobile_number = mobile_number.substring(2);}
                if(mobile_number.length() == 11){mobile_number = mobile_number.substring(1);}
                editText_contact.setText(mobile_number);*/
            }

            editText_relation.setText(contactDetails.getRelation());

            if (contactDetails.getIs_notify() == 1) {

                emergencyNotifyCheckbox.setChecked(true);
            } else {
                emergencyNotifyCheckbox.setChecked(false);

            }


        }

        /*
        Getting the countrycodes from sharedPreferences and setting them in
        the dialog.
         */

        COUNTRY_CODE = new ArrayList<String>();
        CountryNameList = new ArrayList<String>();
        int defaultSelectoin = 0;

        if (preference.getAllCountryCodes() != null &&
                preference.getAllCountryCodes().size() > 0) {
            countryCodesList = preference.getAllCountryCodes();


            for (int i = 0; i < countryCodesList.size(); i++) {

                COUNTRY_CODE.add(countryCodesList.get(i).getCode());
                CountryNameList.add(countryCodesList.get(i).getCountry());
            }


            //boolean isCountryFound = false;
            //int indianCodePosition = 0;
            for (int i = 0; i < CountryNameList.size(); i++) {
                /*if (CountryNameList.get(i).equalsIgnoreCase("india")) {
                    indianCodePosition = i;
                    // break;
                }*/
                if (isEditContact) {
                    //Log.d("Contacts", "isEditContact_countryCode::" + countryCode);
                    //Log.d("Contacts", "isEditContact_countryCode_LIST::" + COUNTRY_CODE.get(i).toString());


                    if (COUNTRY_CODE.get(i).equalsIgnoreCase(countryCode)) {
                        //isCountryFound = true;
                        defaultSelectoin = i;
                        //Log.d("Contacts", "defaultSelectoin::" + defaultSelectoin);
                        break;
                    }

                } else {

                    if (CountryNameList.get(i).equalsIgnoreCase("india")) {
                        //indianCodePosition = i;
                        defaultSelectoin = i;
                        break;
                    }
                }
            }
            // defaultSelectoin = isCountryFound?defaultSelectoin:indianCodePosition;
        } else {
            CountryNameList.add("India");
            COUNTRY_CODE.add("+91");

        }


        ArrayAdapter codes_adapter = new ArrayAdapter<String>(ContactsActivity.this,
                android.R.layout.simple_dropdown_item_1line, CountryNameList);
        spinner_country_names.setAdapter(codes_adapter);
        spinner_country_names.setSelection(defaultSelectoin);


        spinner_country_names.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                editText_cCode.setText("" + COUNTRY_CODE.get(position).toString());

                //Setting the Max length of mobile number as 10 when india is selected.
                if(spinner_country_names.getSelectedItem().toString().equalsIgnoreCase("india")){
                    editText_contact.setFilters(new InputFilter[] {new InputFilter.LengthFilter(10)});
                }
                else{
                    editText_contact.setFilters(new InputFilter[] {new InputFilter.LengthFilter(15)});
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditContact) {

                    if (emergencyNotifyCheckbox.isChecked()) {
                        isNotify = true;
                    } else {
                        isNotify = false;
                    }

                    if (!validateName()) {
                        return;
                    }
                    if (!validateMobileNumber()) {
                        return;
                    }
                    if (!validateRelation()) {
                        return;
                    }

                    updateContact(contactDetails);
                    alertDialog.dismiss();

                } else {


                    if (editText_name.getText().toString().length() != 0 &&
                            editText_contact.getText().toString().length() != 0 &&
                            editText_relation.getText().toString().length() != 0) {

                        if (emergencyNotifyCheckbox.isChecked()) {
                            isNotify = true;
                            Log.d("Contacts", "isNotify::" + isNotify);
                        } else {
                            isNotify = false;
                            Log.d("Contacts", "isNotify::" + isNotify);
                        }

                        int position = spinner_country_names.getSelectedItemPosition();


                        Map<String, Object> contact = new HashMap<String, Object>();
                        contact.put("customer_id", preference.getCustomerId());
                        contact.put("mobile_number", COUNTRY_CODE.get(position).toString() + "-" + editText_contact.getText().toString());
                        contact.put("contact_person", editText_name.getText().toString());
                        contact.put("relation", editText_relation.getText().toString());
                        contact.put("is_notify", isNotify);
                        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                        list.add(contact);
                        HashMap<String, Object> request = new HashMap<>();
                        request.put("emergecy_contacts", list);
                        request.put("customerApp", true);
                        request.put("customer_mobile", Utils.encrypt(preference.getMobileNumber()));
                        Log.i("contacts request", Arrays.asList(request).toString());


                        if (!validateName()) {
                            return;
                        }
                        if (!validateMobileNumber()) {
                            return;
                        }
                        if (!validateRelation()) {
                            return;
                        }


                        alertDialog.dismiss();

                        if (emergencyNotifyCheckbox.isChecked()) {
                            preference.setEmergencyContactToNotify(true);
                        } else {
                            preference.setEmergencyContactToNotify(false);
                        }

                        networkCallForAddContacts(request);


                    } else {
                        Utils.showToast(ContactsActivity.this, getResources().getString(R.string.all_details));
                    }
                }
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }


    private boolean validateName() {
        if (editText_name.getText().toString().length() == 0) {

            editText_name.requestFocus();
            editText_name.setError(getResources().getString(R.string.valid_name));
            return false;
        } else {
        }
        return true;
    }

    private boolean validateMobileNumber() {
        if (editText_contact.getText().toString().length() < 5) {
            editText_contact.requestFocus();
            editText_contact.setError(getResources().getString(R.string.valid_mobile_number));
            return false;
        } else {
            if (editText_contact.getText().toString().equalsIgnoreCase("0000000000")) {
                editText_contact.requestFocus();
                editText_contact.setError(getResources().getString(R.string.valid_mobile_number));
                return false;
            }
        }
        return true;
    }

    private boolean validateRelation() {
        if (editText_relation.getText().toString().length() == 0) {
            editText_relation.requestFocus();
            editText_relation.setError(getResources().getString(R.string.valid_relation));
            return false;
        } else {
        }
        return true;
    }


    private void networkCallForAddContacts(HashMap<String, Object> request) {
        if (Utils.isNetworkAvailable(ContactsActivity.this)) {
            Call<StatusResponse> contactsUpdate =
                    apiInterface.addEmergencyContacts(request, preference.getCustomerId(),
                            preference.getAccessToken());
            Utils.showProgressDialog(ContactsActivity.this);
            contactsUpdate.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    Utils.hideProgressDialog();
                    try {
                        if (response.body() != null && Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                            Log.i("contacts response", response.body().toString());
//                            finish();
                            networkCallForGetContacts();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(ContactsActivity.this, getResources().getString(R.string.network_slow));
                }
            });

        } else {
            Utils.showToast(ContactsActivity.this, getResources().getString(R.string.network_error));
        }
    }


    private void expandFAB() {

        //Floating Action Button 1

        fabAddContactManually.startAnimation(show_fab_manual);
        fabAddContactManually.setClickable(true);

        //Floating Action Button 2

        fabAddContactFromPhbook.startAnimation(show_fab_Phbook);
        fabAddContactFromPhbook.setClickable(true);


    }


    private void hideFAB() {

        //Floating Action Button 1

        fabAddContactManually.startAnimation(hide_fab_manual);
        fabAddContactManually.setClickable(false);

        //Floating Action Button 2

        fabAddContactFromPhbook.startAnimation(hide_fab_Phbook);
        fabAddContactFromPhbook.setClickable(false);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case (1):
                if (resultCode == Activity.RESULT_OK) {


                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                            phones.moveToFirst();
                            String phNum = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                            String name = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));


                            Log.d("Contacts", "NAME::" + name);
                            Log.d("Contacts", "PhNum::" + phNum);

                            phNum = phNum.replace(" ", "");

                            if (phNum.length() > 10) {
                                phNum = phNum.substring(phNum.length() - 10);
                            }


                            isEditContact = false;
                            addContacts(name, phNum, null);

                        }
                    }
                }
                break;
        }
    }


    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                /*
                If the permission is already granted...
                 */
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        } else {
            /*
                If the API level is less than 23.
                 */
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, 1);

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    Toast.makeText(this, "", Toast.LENGTH_SHORT).show();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void updateContact(ContactDetails contactDetails) {

        int position = spinner_country_names.getSelectedItemPosition();
        if (Utils.isNetworkAvailable(this)) {
            Map<String, Object> contact = new HashMap<String, Object>();
            contact.put("id", contactDetails.getId());
            contact.put("customer_id", preference.getCustomerId());
            contact.put("mobile_number", COUNTRY_CODE.get(position).toString() + "-" + editText_contact.getText().toString());
            contact.put("contact_person", editText_name.getText().toString());
            contact.put("relation", editText_relation.getText().toString());
            contact.put("is_notify", isNotify);
            List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
            list.add(contact);
            HashMap<String, Object> request = new HashMap<>();
            request.put("emergecy_contacts", list);
            request.put("customerApp", true);
            request.put("customer_mobile", Utils.encrypt(preference.getMobileNumber()));

            Log.i("req for update", Arrays.asList(request).toString());

            Call<StatusResponse> updateContact = apiInterface.updateEmergencyContact(request,
                    preference.getCustomerId(), preference.getAccessToken());
            updateContact.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("update response", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                networkCallForGetContacts();

                            } else {
                                Utils.showToast(ContactsActivity.this, getResources().getString(R.string.update_contact_failure));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {

                }
            });

        } else {
            Utils.showToast(ContactsActivity.this, getResources().getString(R.string.network_error));
        }
    }

    private void showAlertDialog(final String contactId) {
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(this.getResources().getString(R.string.delete_contact_message))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteContact(contactId);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void deleteContact(String contactId) {
        if (Utils.isNetworkAvailable(this)) {
            List<Map<String, String>> list = new ArrayList<Map<String, String>>();
            final HashMap<String, Object> request = new HashMap<>();
            HashMap<String, String> id = new HashMap<>();
            id.put("id", contactId);
            list.add(id);
            request.put("emergecy_contacts", list);
            request.put("customerApp", "true");
            request.put("customer_mobile", Utils.encrypt(preference.getMobileNumber()));

            Log.i("req for delete", Arrays.asList(request).toString());

            Call<StatusResponse> deleteContact = apiInterface.deleteEmergencyContact(request,
                    preference.getCustomerId(), preference.getAccessToken());
            deleteContact.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("delete response", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {

                                networkCallForGetContacts();

                            } else {
                                Utils.showToast(ContactsActivity.this, getResources().getString(R.string.delete_contact_failure));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {

                }
            });

        } else {
            Utils.showToast(ContactsActivity.this, getResources().getString(R.string.network_error));
        }
    }


}
