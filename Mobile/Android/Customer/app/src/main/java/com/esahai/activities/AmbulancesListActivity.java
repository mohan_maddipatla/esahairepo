package com.esahai.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.adapters.AmbulanceAdapter;
import com.esahai.dataObjects.AmbulanceDetails;

import java.util.ArrayList;


/**
 * Created by ragamai on 16/9/16.
 */
public class AmbulancesListActivity extends CommonActivity {
    RecyclerView.Adapter recyclerViewAdapter;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    RecyclerView recycle_list_ambulances;
    ArrayList<AmbulanceDetails> ambulances;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulances_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText("Ambulances");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        try{
            ambulances = (ArrayList<AmbulanceDetails>) getIntent().getSerializableExtra("ambulances");
        }catch (Exception e){
            e.printStackTrace();
        }

        recycle_list_ambulances = (RecyclerView) findViewById(R.id.recycle_list_ambulances);

        recylerViewLayoutManager = new LinearLayoutManager(AmbulancesListActivity.this);

        recycle_list_ambulances.setLayoutManager(recylerViewLayoutManager);

        recyclerViewAdapter = new AmbulanceAdapter(AmbulancesListActivity.this, ambulances);

        recycle_list_ambulances.setAdapter(recyclerViewAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
