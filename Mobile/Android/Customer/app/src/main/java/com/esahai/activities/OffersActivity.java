package com.esahai.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.adapters.FaqAdapter;
import com.esahai.adapters.OffersAdapter;
import com.esahai.dataObjects.FaqDetails;
import com.esahai.dataObjects.FaqsList;
import com.esahai.dataObjects.PromoDetails;
import com.esahai.dataObjects.PromosList;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersActivity extends CommonActivity {

    RecyclerView recycle_list_offers;
    RecyclerView.Adapter recyclerViewAdapterForOffers;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    ApiInterface apiService;
    AppPreferences preferences;
    ArrayList<PromoDetails> offers;

    TextView tvNoOffersMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);

        preferences = new AppPreferences(OffersActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.offers));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tvNoOffersMsg = (TextView)findViewById(R.id.message_textoffers);
        recycle_list_offers = (RecyclerView)findViewById(R.id.recycle_list_faq);
        recylerViewLayoutManager = new LinearLayoutManager(OffersActivity.this);

        recycle_list_offers.setLayoutManager(recylerViewLayoutManager);

//        if(preferences.getFAQs()==null) {

            networkCallForFaqs();
        /*}else{
            setData();
        }*/

    }

    /*private void setData() {
        try{
            Log.i("local data","FAQs");
            if (faqs != null && faqs.size() > 0) {
                recyclerViewAdapterForOffers = new OffersAdapter(OffersActivity.this, faqs, recycle_list_offers);
                recycle_list_offers.setAdapter(recyclerViewAdapterForOffers);

            }
        }catch (Exception e){e.printStackTrace();}
    }*/

    public void networkCallForFaqs(){


        if(Utils.isNetworkAvailable(OffersActivity.this)){
            apiService = ApiClient.getClient().create(ApiInterface.class);

            HashMap<String,Object> request = new HashMap<>();
            request.put("customerApp",true);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Log.i("offers request", Arrays.asList(request).toString());
            Call<PromosList> faqsList = apiService.getPromosList(request,preferences.getCustomerId(),preferences.getAccessToken());
            Utils.showProgressDialog(OffersActivity.this);
            faqsList.enqueue(new Callback<PromosList>() {
                @Override
                public void onResponse(Call<PromosList> call, Response<PromosList> response) {
                    Utils.hideProgressDialog();
                    try {
                        if(response!=null) {
//                            Log.i("faqs list", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                offers = response.body().getResult();

                                Log.d("offers","offers_Response     "+offers);
                                if (offers != null && offers.size() > 0) {

                                    tvNoOffersMsg.setVisibility(View.GONE);
                                    recycle_list_offers.setVisibility(View.VISIBLE);

                                    recyclerViewAdapterForOffers = new OffersAdapter(OffersActivity.this, offers, recycle_list_offers);
                                    recycle_list_offers.setAdapter(recyclerViewAdapterForOffers);

                                }
                                else{
                                    tvNoOffersMsg.setVisibility(View.VISIBLE);
                                    recycle_list_offers.setVisibility(View.GONE);
                                }
                            }else{
                                tvNoOffersMsg.setVisibility(View.VISIBLE);
                                recycle_list_offers.setVisibility(View.GONE);
                            }
                        }
                        else{
                            tvNoOffersMsg.setVisibility(View.VISIBLE);
                            recycle_list_offers.setVisibility(View.GONE);
                            Log.d("Faq","Response is null");
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<PromosList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    //Utils.showToast(EmergenciesActivity.this,t.toString());
                    Utils.showToast(OffersActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        }else{Utils.showToast(OffersActivity.this,getResources().getString(R.string.network_error));}
    }
}
