package com.esahai.services;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.esahai.activities.DashboardActivity;
import com.esahai.customVIews.Shaker;
import com.esahai.preferences.AppPreferences;

import java.util.List;

/**
 * Created by ragamai on 21/10/16.
 */
public class ShakerService extends Service implements Shaker.Callback,Shaker.OnShakeListener {


    private Shaker mShaker;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private SensorManager mgr = null;
    private static final int MIN_MOVEMENTS = 3;
    private Shaker.OnShakeListener mShakeListener;

    Context context;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {

        super.onCreate();

        context = this;
        this.mSensorManager = ((SensorManager) getSystemService(Context.SENSOR_SERVICE));
        this.mAccelerometer = this.mSensorManager.getDefaultSensor(1);
        //mShaker = new Shaker(this);
        mShaker = new Shaker(context, 1.25d, 500, this);
        //   mShaker.setOnShakeListener(this);


    }

    @Override
    public void onShake() {
        /*if(!isAppInForeground(context)) {
            Intent i = new Intent();
            i.setClass(context, DashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
            // Utils.DoOnShake(this);
        }*/
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("In service", "Entering here");
        /*if(!isAppInForeground(context)) {
            Intent i = new Intent();
            i.setClass(context, DashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
        }*/
        return super.onStartCommand(intent, flags, startId);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("stopppppppppp","stoppppppppppppppppppppppp");
    }

    public void shakingStarted() {
        Log.d("In service", "Entering here");
        Log.d("ShakerDemo", "Shaking started!");
        AppPreferences preferences  = new AppPreferences(ShakerService.this);
        if(!isAppInForeground(context) &&  preferences.isShake()) {
            Intent i = new Intent();
            i.setClass(context, DashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
        }


    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 500,
                restartServicePendingIntent);


        super.onTaskRemoved(rootIntent);
    }

    public void shakingStopped() {
        Log.d("In service", "Entering here");
        Log.d("ShakerDemo", "Shaking stopped!");

       /* if(!isAppInForeground(context)) {
            Intent i = new Intent();
            i.setClass(context, DashboardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
        }*/

    }
    public static boolean isAppInForeground(Context context) {
            Log.d("isAppInForeground", "isAppInForeground");
            List<ActivityManager.RunningTaskInfo> task =
                    ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
                            .getRunningTasks(1);
            if (task.isEmpty()) {
                return false;
            }
        return task
                .get(0)
                .topActivity
                .getPackageName()
                .equalsIgnoreCase(context.getPackageName());
    }
}
