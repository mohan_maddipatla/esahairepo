package com.esahai.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.GoogleAddress;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ragamai on 16/9/16.
 */
public class BloodRequestActivity extends CommonActivity implements LocationListener{

    Button button_submit;
    EditText editText_place,editText_contact,editText_name;
    Spinner spinner_blood_group,spinner_quantity;
    ApiInterface apiInterface,apiInterface1;
    AppPreferences preferences;
    String geoHash;
    double latitude,longitude;
    String[] BLOOD_GROUP = {"A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"};
    String[] QUANTITY = {"50 ml", "100 ml", "200 ml", "300 ml", "400 ml", "500 ml"};
    ArrayAdapter<String> blood_group_adapter, quantity_adapter;
    String bloodGroup,quantity,address;
    ImageView imageView_back;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_blood_request);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface1 = ApiClient.getGoogleClient().create(ApiInterface.class);
        preferences = new AppPreferences(BloodRequestActivity.this);

        initViews();

    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.blood));
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utils.closeKeyBoard(BloodRequestActivity.this,imageView_back);
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        editText_place = (EditText)findViewById(R.id.editText_place);
        editText_contact = (EditText)findViewById(R.id.editText_contact);
        editText_name = (EditText)findViewById(R.id.editText_name);

        spinner_blood_group = (Spinner)findViewById(R.id.spinner_blood_group);
        spinner_quantity = (Spinner)findViewById(R.id.spinner_quantity);

        button_submit = (Button)findViewById(R.id.button_submit);

        blood_group_adapter = new ArrayAdapter<String>(BloodRequestActivity.this,
                android.R.layout.simple_dropdown_item_1line, BLOOD_GROUP);
        spinner_blood_group.setAdapter(blood_group_adapter);

        quantity_adapter = new ArrayAdapter<String>(BloodRequestActivity.this,
                android.R.layout.simple_dropdown_item_1line, QUANTITY);
        spinner_quantity.setAdapter(quantity_adapter);

        spinnerSelectsItem();
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation();

            }
        });

        getLocation();

    }

    private void spinnerSelectsItem() {

        spinner_blood_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bloodGroup = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_quantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                quantity = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void checkValidation() {
        if(editText_name.getText().toString().length() == 0 && editText_contact.getText().toString().length() == 0 &&
                editText_place.getText().toString().length() == 0){
            Utils.showToast(BloodRequestActivity.this,getResources().getString(R.string.all_details));
        }
        else if(editText_contact.getText().toString().length()<10){
            Utils.showToast(BloodRequestActivity.this,getResources().getString(R.string.valid_mobile_number));
        }
        else{
//            networkCallForLatLng(latitude,longitude);
            networkCallForBloodRequest();
        }

    }

    public static String getCurrentMilliSeconds() {
        String dateFormat = "yyyy:MM:hh hh:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        return simpleDateFormat.format(calendar.getTime());
    }

    private void networkCallForBloodRequest() {
        if(Utils.isNetworkAvailable(BloodRequestActivity.this)){

            HashMap<String ,Object> request = new HashMap<>();

            request.put("customer_id",preferences.getCustomerId());
            request.put("customerApp",true);
            request.put("name",editText_name.getText().toString());
            request.put("contact",preferences.getCountryCode()+""+editText_contact.getText().toString());
            request.put("blood_group",bloodGroup);
            request.put("quantity",quantity);
            request.put("latitude",latitude);
            request.put("longitude",longitude);
            request.put("geohash",geoHash);
            request.put("time",getCurrentMilliSeconds());
            request.put("location",address);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Log.i("blood request", Arrays.asList(request).toString());

            Call<StatusResponse> bloodRequest = apiInterface.sendBloodRequest(request,
                    preferences.getCustomerId(),preferences.getAccessToken());
            Utils.showProgressDialog(BloodRequestActivity.this);
            bloodRequest.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    Utils.hideProgressDialog();

                    try{
                        if(response.body()!=null){
                            Log.i("blood request",response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                EsahaiApplication.getApplication().trackEvent("Blood request", "Send blood request", "Successfully sent");
                                finish();
                            }
                            else if(Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE2){
                                showAlert(BloodRequestActivity.this,response.body().getStatusMessage());
                            }
                        }
                        else{
                            Utils.showToast(BloodRequestActivity.this,getResources().getString(R.string.blood_request_error));
                        }

                    }catch (Exception e){e.printStackTrace();}

                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(BloodRequestActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        }else{Utils.showToast(BloodRequestActivity.this,getResources().getString(R.string.network_error));}
    }


    public static AlertDialog alert = null;
    private void showAlert(final Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        alert.dismiss();
                        finish();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void getLocation() {
        if (!Utils.isGPSOn(BloodRequestActivity.this)) {
            Utils.turnOnGPS(BloodRequestActivity.this);
        }
        else {

            LocationManager locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            } else {
                try {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, BloodRequestActivity.this);
                    Location location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Location loc = new Location("");
                    loc.setLatitude(latitude);
                    loc.setLongitude(longitude);
                    geoHash = Utils.getGeoHashCode(loc);
                    networkCallForLatLng(latitude, longitude);
                }catch (Exception e){e.printStackTrace();}

            }
        }
    }

    private String networkCallForLatLng(final double latitude, final double longitude){
        if(Utils.isNetworkAvailable(BloodRequestActivity.this)){
            Call<GoogleAddress> getLatLng = apiInterface1.getAddress(latitude+","+longitude,
                    getResources().getString(R.string.geo_coding_key));

            getLatLng.enqueue(new Callback<GoogleAddress>() {
                @Override
                public void onResponse(Call<GoogleAddress> call, Response<GoogleAddress> response) {
                    try{
                        if(response.body()!=null && response.body().getStatus().equalsIgnoreCase("OK")) {
                            Log.i("latLng response", response.body().toString());
                            address = response.body().getResults().get(0).getFormatted_address();
                        }
                        else{
                            address = latitude+","+longitude;
                        }
//                        networkCallForBloodRequest();
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<GoogleAddress> call, Throwable t) {

                }
            });


        }else{Utils.showToast(BloodRequestActivity.this,getResources().getString(R.string.network_error));}
        return address;
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
