package com.esahai.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.TermsAndConditionsResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 21/10/16.
 */
public class TermsAndConditionsActivity extends CommonActivity {
    AppPreferences preferences;
    ApiInterface apiInterface;
    WebView webView_terms_and_conditions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_card);
        preferences = new AppPreferences(TermsAndConditionsActivity.this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.terms));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        webView_terms_and_conditions = (WebView)findViewById(R.id.webView_rateCard);

        if(preferences.getTNC().equalsIgnoreCase("")) {
            networkCallForTerms();
        }else{setData();}

    }

    private void setData() {
        try{
            if(preferences.getTNC()!=null){
                String terms = preferences.getTNC();
                webView_terms_and_conditions.getSettings().setJavaScriptEnabled(true);

                webView_terms_and_conditions.loadDataWithBaseURL("", terms,
                        "text/html", "UTF-8", "");
            }
        }catch (Exception e){e.printStackTrace();}
    }

    private void networkCallForTerms() {

        Utils.showProgressDialog(this);
        if(Utils.isNetworkAvailable(TermsAndConditionsActivity.this)){

            Call<TermsAndConditionsResponse> getTerms = apiInterface.getTermsAndConditions();

            Log.i("terms response","Calling the T&C APIr");
            getTerms.enqueue(new Callback<TermsAndConditionsResponse>() {
                @Override
                public void onResponse(Call<TermsAndConditionsResponse> call, Response<TermsAndConditionsResponse> response) {
                    try{
                        if(response.body()!=null){
                            Log.i("terms response",response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                preferences.setTNC(response.body().getResponseData().get(0).getTerms_and_conditions());
                                webView_terms_and_conditions.getSettings().setJavaScriptEnabled(true);

                                webView_terms_and_conditions.loadDataWithBaseURL("", response.body().
                                        getResponseData().get(0).getTerms_and_conditions(),
                                        "text/html", "UTF-8", "");

                                Utils.hideProgressDialog();
                            }
                            else{
                                Utils.hideProgressDialog();
                            }
                            Utils.hideProgressDialog();
                        }
                        else{
                            Utils.hideProgressDialog();
                        }

                    }catch (Exception e){
                        Utils.hideProgressDialog();
                        e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<TermsAndConditionsResponse> call, Throwable t) {

                    Utils.hideProgressDialog();
                    Utils.showToast(TermsAndConditionsActivity.this, getResources().getString(R.string.network_slow));

                }
            });

        }else{

            Utils.hideProgressDialog();
            Utils.showToast(TermsAndConditionsActivity.this,getResources().getString(R.string.network_error));}
    }


    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
                finish();
        }
        return super.onOptionsItemSelected(item);
    }*/
}
