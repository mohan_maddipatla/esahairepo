package com.esahai.Extras;

import android.net.Uri;

import com.esahai.dataObjects.BookingConfirmedDetails;

/**
 * Created by ragamai on 29/9/16.
 */
public class Constants {

    public static final byte locationPrecisionNumber = 12;
    public static final int SUCCESS_CODE = 300;
    public static final int FAILURE_CODE = 500;

    public static final int CANCELLED_CODE1 = 411;
    public static final int CANCELLED_CODE2 = 412;
    public static final int CANCELLED_CODE3 = 413;

    public static final int NO_DATA = 303;
    private static Uri savedProfilePic = null;
    public static final int DISPLAY_NOTIFICATION = 1;
    public static final int BLOOD_REQUEST = 2;
    public static final int LOGOUT = 3;


    //Confirmed Booking Details
    public static BookingConfirmedDetails bookingConfirmedDetails = null;

    /*public static void saveProfilePic(Uri profilePic){
        savedProfilePic = profilePic;
    }

    public static Uri getSavedProfilePic(){
        return savedProfilePic;
    }*/
    /*public static final String STARTED = "Started";
    public static final String ACCEPTED = "Accepted";
    public static final String BOOKING_ACCEPTED = "Booking Accepted";
    public static final String WAY_TO_EMERGENCY_LOCATION = "Way to Emergency Location";
    public static final String AT_EMERGENCY_LOCATION = "At Emergency Location";
    public static final String REACHED_EMERGENCY_LOCATION = "Reached Emergency Location";
    public static final String WAY_BACK_TO_DESTINATION = "Way Back to Destination";
    public static final String REACHED_DESTINATION = "Reached Destination";
    public static final String TRIP_CLOSED = "Trip Closed";
    public static final String FAKE_BOOKING = "Fake booking"; */

    public static final String STARTED = "Started";
    public static final String ACCEPTED = "ACTD";
    public static final String BOOKING_ACCEPTED = "Booking Accepted";
    public static final String WAY_TO_EMERGENCY_LOCATION = "WTEL";
    public static final String AT_EMERGENCY_LOCATION = "AEL";
    public static final String REACHED_EMERGENCY_LOCATION = "Reached Emergency Location";
    public static final String WAY_BACK_TO_DESTINATION = "WBTD";
    public static final String REACHED_DESTINATION = "RD";
    public static final String TRIP_CLOSED = "TC";
    public static final String FAKE_BOOKING = "Fake booking";

    public static final String CANCELLED = "CAN";
    public static final String CANCELLED_FAKE = "CANCFAKE";
    public static final String PENDING = "Pending";





}
