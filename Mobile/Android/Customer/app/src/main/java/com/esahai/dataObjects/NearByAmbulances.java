package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 30/9/16.
 */
public class NearByAmbulances {
    /*
    {"statusCode":"300",
    "statusMessage":"Ambulance List fetched successfully",
    "responseData":[{"id":"52",
    "ambulance_id":"6",
    "driver_id":"4",
    "status":"Online",
    "created_date":"2016-09-29 17:17:04",
    "updated_date":"2016-09-29 18:42:17",
    "longitude":"78.3922",
    "latitude":"17.4946",
    "geoHashCode":"umumyng5ttpp",
    "distance":"6.114",
    "duration":"16 mins"}]}
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<AmbulanceDetails> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<AmbulanceDetails> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<AmbulanceDetails> responseData) {
        this.responseData = responseData;
    }


    @Override
    public String toString() {
        return "NearByAmbulances{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
