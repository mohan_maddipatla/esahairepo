package com.esahai.dataObjects;

/**
 * Created by ragamai on 25/10/16.
 */
public class SystemAttributes {

    /*
    "id": "3",
			"type_name": "test3"
     */

    public String id;
    public String type_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }


    @Override
    public String toString() {
        return "Attributes{" +
                "id='" + id + '\'' +
                ", type_name='" + type_name + '\'' +
                '}';
    }
}
