package com.esahai.dataObjects;

/**
 * Created by ragamai on 21/10/16.
 */
public class Location {
    /*
    "lat" : 17.4410483,
               "lng" : 78.3856447
     */
    public double lat;
    public double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Location{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
