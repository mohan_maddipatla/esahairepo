package com.esahai.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.AmbulanceType;
import com.esahai.dataObjects.AmbulanceTypeListResponse;
import com.esahai.dataObjects.EmergencyType;
import com.esahai.dataObjects.EmergencyTypeList;
import com.esahai.interfaces.MyAlertListener;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.google.gson.JsonElement;
import com.moe.pushlibrary.MoEHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 29/9/16.
 */
public class SplashScreenActivity extends CommonActivity {

    AppPreferences preferences;
    TextView textView_internet;
    ApiInterface apiInterface;
    private final int MY_CALL_PHONE_PERMISSION_REQUEST_CODE = 2;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        preferences = new AppPreferences(SplashScreenActivity.this);
        textView_internet = (TextView)findViewById(R.id.textView_internet);
        textView_internet.setVisibility(View.INVISIBLE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        /*try{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    MyFirebaseInstanceIDService fm = new MyFirebaseInstanceIDService();
                    fm.onTokenRefresh();
                }
            }).start();
        }catch (Exception e){e.printStackTrace();}*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!Utils.isPlayServicesAvailable(SplashScreenActivity.this)){
            textView_internet.setVisibility(View.INVISIBLE);
            Utils.showToast(SplashScreenActivity.this,getResources().getString(R.string.play_services));
        }


        if (!Utils.isGPSOn(SplashScreenActivity.this)) {
            textView_internet.setVisibility(View.INVISIBLE);
            Utils.turnOnGPS(SplashScreenActivity.this);
        }else if(Utils.isGPSOn(SplashScreenActivity.this)){
            firstTimeRun();
        }
    }

    private void firstTimeRun() {
        try{
            if(preferences.isFirstRun()){
                preferences.setFirstRun(false);
                    checkNetwork();
            }
            else{
                    if (!Utils.isNetworkAvailable(SplashScreenActivity.this)) {
                        textView_internet.setVisibility(View.VISIBLE);
                        setHandler(false);

                    }
                    if (Utils.isNetworkAvailable(SplashScreenActivity.this)) {
                        textView_internet.setVisibility(View.INVISIBLE);
//                        setHandler(true);
                        checkAppVersion();
                    }
            }
        }catch (Exception e){e.printStackTrace();}
    }

    private void checkNetwork() {
        if(!Utils.isNetworkAvailable(SplashScreenActivity.this)){
            textView_internet.setVisibility(View.VISIBLE);
            showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
        }
        if(Utils.isNetworkAvailable(SplashScreenActivity.this)) {
            textView_internet.setVisibility(View.INVISIBLE);
//            setHandler(true);
            checkAppVersion();
        }
    }

    private void showAlertDialog(String message, final boolean isNetwork) {
        try {
            String yes, no;
            if (isNetwork) {
                yes = "Yes";
                no = "No";
                Log.d("Dialog", "Dialog_IF set to : " + isNetwork);
            } else {
                yes = "Connect to Customer service";
                no = "Try other options ";
                Log.d("Dialog", "Dialog_ELSE set to : " + isNetwork);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                                Log.d("CustomerCare", "Number IS::::" + "04047911911");
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "04047911911"));
                                startActivity(intent);
                                dialog.cancel();
                                finish();
                                System.exit(0);
                                /*System.exit(0);
                                preferences.setFirstRun(true);*/
                            } else {
                                requestPermissionForCall();
                            }
                        }


                    });
            if (isNetwork) {
                builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
            }
            AlertDialog alert = builder.create();
            alert.show();
        }catch (Exception e){e.printStackTrace();}
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(SplashScreenActivity.this,
                new String[]{Manifest.permission.CALL_PHONE},
                MY_CALL_PHONE_PERMISSION_REQUEST_CODE);


    }

    private void goToActivities(){
        String access_token = preferences.getAccessToken();
        Log.i("token",access_token);
        if(!access_token.equals("")){

            if( preferences.isTrip() == false){
                Intent dashboard = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                startActivity(dashboard);
                finish();
            }
            else if(preferences.isTrip() == true){
                Intent booking = new Intent(SplashScreenActivity.this, TrackAmbulanceActivity.class);
                booking.putExtra("splash",true);
                startActivity(booking);
                finish();
            }

        }else{
            Intent login = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(login);
            finish();
        }
    }

    private void setHandler(boolean isInternet) {

        if(isInternet) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(preferences.getEmergencies()==null) {
                        networkCallForEmergencies();
                    }else{
                        networkCallForAmbulanceTypes();
                        goToActivities();
                    }

                }
            }, 3000);
        }else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    networkCallForAmbulanceTypes();
                    goToActivities();

                }
            }, 3000);
        }
    }

    private void networkCallForEmergencies() {

        if (Utils.isNetworkAvailable(SplashScreenActivity.this)) {

            Call<EmergencyTypeList> emergencyTypeListCall = apiInterface.getEmergencyTypes();
            emergencyTypeListCall.enqueue(new Callback<EmergencyTypeList>() {
                @Override
                public void onResponse(Call<EmergencyTypeList> call, Response<EmergencyTypeList> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("emergenciesList", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                ArrayList<EmergencyType> emergencyList = new ArrayList<EmergencyType>();
                                emergencyList = response.body().getResponseData();
//                                Utils.setEmergencyTypes(emergencyList);
                                preferences.setEmergencies(emergencyList);

                                if (emergencyList != null) {
                                    for (int i = 0; i < emergencyList.size(); i++) {
                                        if(emergencyList.get(i).getType_name().equalsIgnoreCase("Medical Taxi")){
                                            preferences.setMedicalTaxiId(emergencyList.get(i).getId());
                                        }
                                    }
                                }

//                                if(Utils.getAmbulanceTypes()==null) {
                                    networkCallForAmbulanceTypes();
//                                }
                                goToActivities();


                            }
                        }
                        else{
                            Log.d("Response","Response_dashboard is null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<EmergencyTypeList> call, Throwable t) {
//                    Utils.showToast(SplashScreenActivity.this, getResources().getString(R.string.network_slow));
                    textView_internet.setVisibility(View.VISIBLE);

                }
            });

        } else {
//            Utils.showToast(SplashScreenActivity.this, getResources().getString(R.string.network_error));
            textView_internet.setVisibility(View.VISIBLE);
        }

    }

    private void networkCallForAmbulanceTypes() {
        if (Utils.isNetworkAvailable(SplashScreenActivity.this)) {
            HashMap<String, Object> request = new HashMap<>();
            request.put("customerApp", true);
//            request.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

            Log.i("req for ambTypes", Arrays.asList(request).toString());

            Call<AmbulanceTypeListResponse> ambulanceTypeList = apiInterface.getAmbulanceTypes(request,
                    preferences.getCustomerId(), preferences.getAccessToken());

            ambulanceTypeList.enqueue(new Callback<AmbulanceTypeListResponse>() {
                @Override
                public void onResponse(Call<AmbulanceTypeListResponse> call, Response<AmbulanceTypeListResponse> response) {

                    try {

                        if (response.body() != null) {
                            Log.i("ambulancetypelist", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                ArrayList<AmbulanceType> ambulanceType = new ArrayList<AmbulanceType>();
                                ambulanceType = response.body().getResponseData();
                                Utils.setAmbulanceTypes(ambulanceType);
                                preferences.setAmbulanceTYpes(ambulanceType);
                            }
                        }

                        else{
                            Log.d("Response","Response is null");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onFailure(Call<AmbulanceTypeListResponse> call, Throwable t) {
                    Log.i("ambul failure", t.toString());
                    Utils.showToast(SplashScreenActivity.this, getResources().getString(R.string.network_slow));

                }
            });
        } else {
            Utils.showToast(SplashScreenActivity.this, getResources().getString(R.string.network_error));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == MY_CALL_PHONE_PERMISSION_REQUEST_CODE) {
            // Enable the My Location button if the permission has been granted.

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                if(preferences.isFirstRun()) {
                    showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
//                }

            }else{
                // denied
                preferences.setFirstRun(true);
                finish();

            }

        }
    }


    private void checkAppVersion() {
        if (Utils.isNetworkAvailable(this)) {

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call<JsonElement> call2 = apiService.getAppVersion("Customer_Android");
            call2.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> serverResponse) {

                    try {
                        JsonElement jsonElement = serverResponse.body();
                        JSONObject response = new JSONObject(jsonElement.toString());
                        Log.i("response", "response = 222" + serverResponse.body());

                        if (Integer.parseInt(response.getString("statusCode")) == Constants.SUCCESS_CODE) {
                            JSONObject details = response.getJSONObject("responseData");
                            int serverVersion = details.getInt("version_no");
                            int localVersion = Utils.getAppVersion();
                            Log.d("versions", "app versions" + serverVersion + " " + localVersion);
                            if (serverVersion <= localVersion) {

                                checkInitialConditionsAndLoad();
                            } else {
                                if (details.getInt("force_upgrade") == 1) {

                                    // force upgrade
                                    forceUpdateOrShowMessage(SplashScreenActivity.this, 1, getResources().getString(R.string.app_not_supported));
                                } else {

                                    // update to get new features
                                    forceUpdateOrShowMessage(SplashScreenActivity.this, 2, getResources().getString(R.string.update_to_get_new_features));
                                }
                            }


                        } else {
//                            Common.displayAlertDialog(SplashScreen.this, response.getString(Constants.statusMessage));
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable throwable) {

                    Log.d("", "RetroFit2.0 :getAppVersion: " + "eroorrrrrrrrrrrr");
                    Log.e("eroooooooorr", throwable.toString());
//                    Common.displayAlertDialog(SplashScreen.this, getString(R.string.networkError));
                }
            });
        } else {
//            Common.displayAlertDialog(this, getString(R.string.offline));

        }
    }


        private void forceUpdateOrShowMessage(final Context context, int buttonCount, String message) {
            simpleSingleButtonAlertDialogForceUpdate(SplashScreenActivity.this, "", message, new MyAlertListener() {
                @Override
                public void buttonClicked(int type) {

                    if (type == 0) {
                        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
                        boolean marketFound = false;

                        // find all applications able to handle our rateIntent
                        final List<ResolveInfo> otherApps = getPackageManager().queryIntentActivities(rateIntent, 0);
                        for (ResolveInfo otherApp : otherApps) {
                            // look for Google Play application
                            if (otherApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {

                                ActivityInfo otherAppActivity = otherApp.activityInfo;
                                ComponentName componentName = new ComponentName(
                                        otherAppActivity.applicationInfo.packageName,
                                        otherAppActivity.name
                                );
                                rateIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                rateIntent.setComponent(componentName);
                                startActivity(rateIntent);
                                marketFound = true;
                                break;

                            }
                        }

                        // if GP not present on device, open web browser
                        if (!marketFound) {
                            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName()));
                            startActivity(webIntent);
                        }
                        finish();
                    } else {
                        checkInitialConditionsAndLoad();
                    }
                }

            }, buttonCount);

        }



    private void checkInitialConditionsAndLoad() {

        setHandler(true);
    }


    private void simpleSingleButtonAlertDialogForceUpdate(final Context ctx, String title, String message, final MyAlertListener myAlertListener, int showButtonCount) {
        // custom dialog
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.force_update_dialog);
        Button btnYes = (Button) dialog.findViewById(R.id.btnPossitive);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNegative);

        if (showButtonCount == 1) {
            btnNo.setVisibility(View.GONE);
        }

        TextView textStatus = (TextView) dialog.findViewById(R.id.txtMsg);
        textStatus.setText(message);

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                myAlertListener.buttonClicked(0);


            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                myAlertListener.buttonClicked(1);


            }
        });

        if (!dialog.isShowing())
            dialog.show();
    }

}
