package com.esahai.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.activities.AmbulancesListActivity;
import com.esahai.dataObjects.AmbulanceDetails;

import java.util.ArrayList;

/**
 * Created by ragamai on 4/10/16.
 */
public class AmbulanceAdapter extends RecyclerView.Adapter<AmbulanceAdapter.ViewHolder> {

    Context context;
    ArrayList<AmbulanceDetails> ambulances;



    public AmbulanceAdapter(Context context, ArrayList<AmbulanceDetails> ambulances) {
        this.context = context;
        this.ambulances = ambulances;
    }

    @Override
    public AmbulanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ambulances_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AmbulanceAdapter.ViewHolder holder, int position) {
        try {
            holder.text_name.setText(ambulances.get(position).getAmbulance_number() + "(~" +ambulances.get(position).getDistance() + " Kms)");
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public int getItemCount() {
        return ambulances.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView text_name;

        public ViewHolder(View view) {
            super(view);
            text_name = (TextView)view.findViewById(R.id.text_name);
        }



    }
}
