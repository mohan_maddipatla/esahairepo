package com.esahai.dataObjects;

/**
 * Created by ragamai on 29/9/16.
 */
public class VerifyOTPResponse {
    /*
     "statusCode": "300",
  "statusMessage": "Verified successfully",

     */

    public String statusCode;
    public String statusMessage;
    public CustomerDetails responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public CustomerDetails getResponseData() {
        return responseData;
    }

    public void setResponseData(CustomerDetails responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "VerifyOTPResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
