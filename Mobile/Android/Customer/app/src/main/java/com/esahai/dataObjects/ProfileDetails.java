package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 12/10/16.
 */
public class ProfileDetails{

    public  String customer_id;
    public  String customer_mobile_number;
    public  String customer_name;
    public  String customer_email;
    public  String address;
    public  String city;
    public  String state;
    public  String pincode;
    public  String device_imei;
    public  String device_os;
    public  String device_type;
    public  String is_active;
    public  String device_token;
    public  String created_date;
    public  String updated_date;
    public  String file_url;
    public  String otp;
    public  String loginStatus;
    public  String is_verified;
    public  String sms_status;
    public  String token;
    public  String age;
    public  String gender;
    public ArrayList<Attributes> Attributes;
    public ArrayList<SystemAttributes> systemAttributes;
    public String blood_group;
    public String donating_blood;
    public String come_to_know_by;
    public String email;
    /*
    blood_group,donating_blood,come_to_know_by,email, address, city,state,pincode
     */

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_mobile_number() {
        return customer_mobile_number;
    }

    public void setCustomer_mobile_number(String customer_mobile_number) {
        this.customer_mobile_number = customer_mobile_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getDevice_imei() {
        return device_imei;
    }

    public void setDevice_imei(String device_imei) {
        this.device_imei = device_imei;
    }

    public String getDevice_os() {
        return device_os;
    }

    public void setDevice_os(String device_os) {
        this.device_os = device_os;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public String getSms_status() {
        return sms_status;
    }

    public void setSms_status(String sms_status) {
        this.sms_status = sms_status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<com.esahai.dataObjects.Attributes> getAttributes() {
        return Attributes;
    }

    public void setAttributes(ArrayList<com.esahai.dataObjects.Attributes> attributes) {
        Attributes = attributes;
    }

    public ArrayList<SystemAttributes> getSystemAttributes() {
        return systemAttributes;
    }

    public void setSystemAttributes(ArrayList<SystemAttributes> systemAttributes) {
        this.systemAttributes = systemAttributes;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getDonating_blood() {
        return donating_blood;
    }

    public void setDonating_blood(String donating_blood) {
        this.donating_blood = donating_blood;
    }

    public String getCome_to_know_by() {
        return come_to_know_by;
    }

    public void setCome_to_know_by(String come_to_know_by) {
        this.come_to_know_by = come_to_know_by;
    }

    @Override
    public String toString() {
        return "ProfileDetails{" +
                "customer_id='" + customer_id + '\'' +
                ", customer_mobile_number='" + customer_mobile_number + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", customer_email='" + customer_email + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", pincode='" + pincode + '\'' +
                ", device_imei='" + device_imei + '\'' +
                ", device_os='" + device_os + '\'' +
                ", device_type='" + device_type + '\'' +
                ", is_active='" + is_active + '\'' +
                ", device_token='" + device_token + '\'' +
                ", created_date='" + created_date + '\'' +
                ", updated_date='" + updated_date + '\'' +
                ", file_url='" + file_url + '\'' +
                ", otp='" + otp + '\'' +
                ", loginStatus='" + loginStatus + '\'' +
                ", is_verified='" + is_verified + '\'' +
                ", sms_status='" + sms_status + '\'' +
                ", token='" + token + '\'' +
                ", age='" + age + '\'' +
                ", gender='" + gender + '\'' +
                ", Attributes=" + Attributes +
                ", systemAttributes=" + systemAttributes +
                ", blood_group='" + blood_group + '\'' +
                ", donating_blood='" + donating_blood + '\'' +
                ", come_to_know_by='" + come_to_know_by + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
