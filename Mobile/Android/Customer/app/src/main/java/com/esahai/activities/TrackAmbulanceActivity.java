package com.esahai.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.BookingConfirmedDetails;
import com.esahai.dataObjects.BookingStatus;
import com.esahai.dataObjects.EmergencyDetails;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.parsers.DirectionJSONParser;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 27/9/16.
 */
public class TrackAmbulanceActivity extends CommonActivity implements OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,GoogleMap.OnCameraMoveStartedListener {

    GoogleMap googleMap;
    LatLng origin, dest, hospital_latLng;
    Button button_call;
    LatLng locationUpdateLatLng;
    BookingConfirmedDetails bookingConfirmedDetails;
    Timer timer = null, status_timer = null;
    TextView text_truck_number, text_distance, text_driver_name,  text_bookingId;
    TextSwitcher text_ambulance_status;
    AppPreferences preference;
    ApiInterface apiService;
    TimerTask locationTask, statusTask;
    double ambulance_lat, ambulance_lng;
    boolean flag_status_timer = false;
    Button button_cancel, button_patient_info;
    CircleImageView profile_image;
    View view;
    LinearLayout layout_main;

    float previousZoomLevel = 16 ;
    boolean isZooming = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    public static long lastPnReceivedTime = System.currentTimeMillis();

    Marker ambulanceMarker;

    boolean isRouteDrawedFromAmbToEmergency;
    boolean isRouteDrawedFromEmergenceyToHospital;

    boolean isEmergencyMarkerAdded ;
    boolean isHospitalMarkerAdded ;
    boolean isAmbMarkerAdded ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_ambulance);
        preference = new AppPreferences(TrackAmbulanceActivity.this);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        initViews();
        try {
            bookingConfirmedDetails = (BookingConfirmedDetails) getIntent().getSerializableExtra("bookingConfirmedDetails");
            if (bookingConfirmedDetails == null) {
                bookingConfirmedDetails = preference.getBookingDetails();
                ambulance_lat = bookingConfirmedDetails.getAmbulance_latitude();
                ambulance_lng = bookingConfirmedDetails.getAmbulance_longitude();
                Log.d("booking","oncreate details"+bookingConfirmedDetails.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (getIntent().hasExtra("transferred")) {
                showAlertDialogForTransferred();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        setData();
        setTimer();
        setTimerForStatus();
        Log.d("booking","splash"+getIntent().hasExtra("splash"));
        if(getIntent().hasExtra("splash")){
            networkCallForBookingStatus();
        }

        try {
//            startService();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void setData() {
        try {
            if (bookingConfirmedDetails != null) {
                text_driver_name.setText(getResources().getString(R.string.driver)+": " + bookingConfirmedDetails.getDriverName());
//                text_distance.setText("ETA is approximately " + bookingConfirmedDetails.getDistance() + " kms Away" + " / " + bookingConfirmedDetails.getDuration());
                text_truck_number.setText(bookingConfirmedDetails.getAmbulanceNumber());
//                button_call.setText("Call "+bookingConfirmedDetails.getDriverMobileNumber());

                String url = bookingConfirmedDetails.getDriverProfilePic();
                String image_url = url.replace("\\", "");
                Picasso
                        .with(TrackAmbulanceActivity.this)
                        .load(image_url)
                        .into(profile_image);

                if (preference.getBookingId() != null) {
                    text_bookingId.setText(getResources().getString(R.string.booking_id)+ ": "+ preference.getBookingId());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTimer() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            timer = new Timer();
            locationTask = new TimerTask() {
                @Override
                public void run() {
                    try {
                        Log.d("locationUpdateLatLng","locationUpdateLatLng = "+locationUpdateLatLng);
                        if((System.currentTimeMillis()-lastPnReceivedTime)<1000*45) {
                            locationUpdateLatLng = Utils.getLatLng();


                            if (locationUpdateLatLng != null) {
//                                Log.i("location update-->", locationUpdateLatLng.latitude + "longitude" + locationUpdateLatLng.longitude);
                                bookingConfirmedDetails.setAmbulance_latitude(locationUpdateLatLng.latitude);
                                bookingConfirmedDetails.setAmbulance_longitude(locationUpdateLatLng.longitude);
                            }
                            setMap();
                        }else{

                            networkCallForBookingStatus();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            timer.schedule(locationTask, 0l, 20000);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        }).start();
    }


    private void setTimerForStatus() {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (status_timer != null) {
                        status_timer.cancel();
                        ;
                        status_timer = null;
                    }
                    status_timer = new Timer();
                    statusTask = new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("Track","status:::"+Utils.getAmbulanceStatus());

                                        if (Utils.getAmbulanceStatus() != null && bookingConfirmedDetails!=null)
                                        {
                                            bookingConfirmedDetails.setBookingStatus(Utils.getAmbulanceStatus());


                                            if(Utils.getAmbulanceStatus().equalsIgnoreCase(Constants.STARTED) ||
                                            Utils.getAmbulanceStatus().equalsIgnoreCase(Constants.WAY_TO_EMERGENCY_LOCATION)){

                                                    if(preference.isMedicalTaxi() ||bookingConfirmedDetails.is_medical_taxi()){
                                                    text_ambulance_status.setText(getResources().getString(R.string.on_the_way_med_taxi));
                                                }
                                                else {
                                                    text_ambulance_status.setText(getResources().getString(R.string.on_the_way));
                                                }

                                            }
                                            else if(Utils.getAmbulanceStatus().equalsIgnoreCase(Constants.ACCEPTED)){
                                                if(preference.isMedicalTaxi() || bookingConfirmedDetails.is_medical_taxi()){
                                                    text_ambulance_status.setText(getResources().getString(R.string.med_taxi_accept));
                                                }
                                                else {
                                                    text_ambulance_status.setText(getResources().getString(R.string.ambulance_accept));
                                                }
                                            }
                                            else if(Utils.getAmbulanceStatus().equalsIgnoreCase(Constants.AT_EMERGENCY_LOCATION) ||
                                            Utils.getAmbulanceStatus().equalsIgnoreCase(Constants.REACHED_EMERGENCY_LOCATION)){

                                                if( preference.isMedicalTaxi() ||bookingConfirmedDetails.is_medical_taxi()){
                                                    text_ambulance_status.setText(getResources().getString(R.string.at_emergency_location__med_taxi));
                                                }
                                                else {
                                                    text_ambulance_status.setText(getResources().getString(R.string.at_emergency_location));
                                                }

                                            }
                                            else if(Utils.getAmbulanceStatus().equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)){

                                                if(preference.isMedicalTaxi() || bookingConfirmedDetails.is_medical_taxi()){
                                                    text_ambulance_status.setText(getResources().getString(R.string.way_back_to_dest_med_taxi));
                                                }
                                                else {
                                                    text_ambulance_status.setText(getResources().getString(R.string.way_back_to_hospital));
                                                }
                                                setPolyLineForHospital();
                                            }

                                            else if(Utils.getAmbulanceStatus().equalsIgnoreCase(Constants.REACHED_DESTINATION)){

                                                if(preference.isMedicalTaxi() || bookingConfirmedDetails.is_medical_taxi()){
                                                    text_ambulance_status.setText(getResources().getString(R.string.reached_dest_med_taxi));
                                                }
                                                else {
                                                    text_ambulance_status.setText(getResources().getString(R.string.reached_hospital));
                                                }

                                            }


                                        } else {
                                            text_ambulance_status.setText(getResources().getString(R.string.please_wait));
                                        }
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    status_timer.schedule(statusTask, 0l, 5000);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (timer != null) {
                timer.cancel();
                timer.purge();
                timer = null;
            }
            if (status_timer != null) {
                status_timer.cancel();
                status_timer.purge();
                status_timer = null;
            }
            if (statusTask != null) {
                statusTask.cancel();
            }
            if (locationTask != null) {
                locationTask.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
//            stopService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initViews() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setVisibility(View.INVISIBLE);


        if(preference.isMedicalTaxi() ){
            txtTitle.setText(getResources().getString(R.string.track_med_taxi));
        }
        else{
            txtTitle.setText(getResources().getString(R.string.track_ambulance));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        MapFragment mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(TrackAmbulanceActivity.this);
        button_call = (Button) findViewById(R.id.button_call);
        button_cancel = (Button) findViewById(R.id.button_cancel);
        button_patient_info = (Button) findViewById(R.id.button_patient_info);

        text_truck_number = (TextView) findViewById(R.id.text_truck_number);
        text_distance = (TextView) findViewById(R.id.text_distance);
        text_driver_name = (TextView) findViewById(R.id.text_driver_name);

        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        view = findViewById(R.id.view);
        layout_main = (LinearLayout) findViewById(R.id.layout_main);

        text_bookingId = (TextView) findViewById(R.id.text_bookingId);
        text_ambulance_status = (TextSwitcher) findViewById(R.id.text_ambulance_status);
        text_ambulance_status.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView switcherTextView = new TextView(getApplicationContext());
                switcherTextView.setTextSize(16);
                switcherTextView.setTextColor(Color.WHITE);
                switcherTextView.setText(getResources().getString(R.string.please_wait));
                switcherTextView.setGravity(Gravity.CENTER_HORIZONTAL);
                //switcherTextView.setShadowLayer(6, 6, 6, Color.WHITE);
                return switcherTextView;
            }
        });
        Animation animationOut = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);
        Animation animationIn = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);


        text_ambulance_status.setOutAnimation(animationOut);
        text_ambulance_status.setInAnimation(animationIn);

        button_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                selectOption();
                showPopUp();
            }
        });

        button_patient_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent update_info = new Intent(TrackAmbulanceActivity.this, UpdatePatientInfoActivity.class);
                startActivity(update_info);
            }
        });


        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preference.getBookingCancellable()) {
                    showAlertDialog();
                } else  {
                    Utils.showToast(TrackAmbulanceActivity.this, getResources().getString(R.string.trip_started));
                }
            }
        });


    }

    private void showPopUp() {

        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);

        View popupView = layoutInflater.inflate(R.layout.profile_image_selection, null);

        PopupWindow popupWindow = new PopupWindow(popupView,
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT,
                true);

        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);

        Rect rc = new Rect();
        view.getWindowVisibleDisplayFrame(rc);
        int[] xy = new int[2];
        view.getLocationInWindow(xy);
        rc.offset(xy[0], xy[1]);



        popupWindow.showAtLocation(popupView, Gravity.CENTER, view.getLeft()-(view.getWidth()*2),
                view.getTop()+(view.getHeight()*2));

        TextView textView_call_driver = (TextView)popupView.findViewById(R.id.textView_call_driver);
        TextView textView_customer_care = (TextView)popupView.findViewById(R.id.textView_customer_care);


        textView_call_driver.setText(getResources().getString(R.string.call_driver));
        textView_customer_care.setText(getResources().getString(R.string.call_customer_care));
        textView_call_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    apiForCallDriver();
//                    callDriver();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        textView_customer_care.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    callCustomerCare();
                }catch (Exception e){e.printStackTrace();}
            }
        });





        /*PopupMenu popup = new PopupMenu(TrackAmbulanceActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
//        setMenuBackground();

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId() == R.id.call_driver){
                    callDriver();
                }
                if(item.getItemId() == R.id.call_customer_care){
                    callCustomerCare();
                }
//                Toast.makeText(TrackAmbulanceActivity.this,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        popup.show();*/
    }




    private void callDriver() {
        if (ActivityCompat.checkSelfPermission(TrackAmbulanceActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            if (bookingConfirmedDetails != null) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + bookingConfirmedDetails.getDriverMobileNumber()));
                startActivity(intent);
            }
        } else {
            requestPermissionForCall();
        }
    }

    private void callCustomerCare() {

        //String customerCareNumber = "04038119911";
        Log.d("callCustomerCare","callCustomerCare = "+preference.getCustomerCareNumber());
        if (ActivityCompat.checkSelfPermission(TrackAmbulanceActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            if (bookingConfirmedDetails != null) {
                Log.d("bookingConfirmed","bookingConfirmedDetails "+preference.getCustomerCareNumber());
                if(preference.getCustomerCareNumber()!=null) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + preference.getCustomerCareNumber()));
                    startActivity(intent);

                }else{
                    Log.d("zeroo","zero****");
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + 0));
                    startActivity(intent);
                }
            }else{
                Log.d("zeroo","zero**** nullll" );
            }
        } else {
            requestPermissionForCall();
        }
    }

    private void selectOption() {
        final CharSequence[] items = {"Call Driver", "Call Customer Care",
                "Cancel"};
//        System.out.println("select image");
        AlertDialog.Builder builder = new AlertDialog.Builder(TrackAmbulanceActivity.this);
        builder.setTitle("Contact");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Call Driver")) {
                    callDriver();
                } else if (items[item].equals("Call Customer Care")) {
                    callCustomerCare();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void apiForCallDriver() {
        if(Utils.isNetworkAvailable(TrackAmbulanceActivity.this)){
            String numbers = preference.getPhoneNumberWithoutCc()+","+bookingConfirmedDetails.getDriverMobileNumber();

            Utils.showProgressDialog(TrackAmbulanceActivity.this);
            ApiInterface apiService =
                    ApiClient.getClientConnectCall().create(ApiInterface.class);
            final Call<Object> callDriver = apiService.connectCall(numbers);
            callDriver.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> serverResponse) {
                        Utils.hideProgressDialog();
                    try {
                        Object jsonElement = serverResponse.body();

                        Log.d("jsonElement","jsonElement = "+jsonElement);

                        if(jsonElement == null){
                            // call success
                        }else{
                            // call failure
                            callDriver();
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable throwable) {

                    Utils.hideProgressDialog();
                    EsahaiApplication.getApplication().trackEvent("Dashboard", getString(R.string.network_error), "makeCallViaProxy", false);

                    callDriver();
                }
            });



        }else{
            callDriver();
//            Utils.showToast(TrackAmbulanceActivity.this,getResources().getString(R.string.network_error));
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }


    private void showAlertDialogForTransferred() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.ambulance_assigned))
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.cancel_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            if (timer != null) {
                                timer.cancel();
                                timer.purge();
                            }
                            if (status_timer != null) {
                                status_timer.cancel();
                                status_timer.purge();
                            }
                            if (statusTask != null) {
                                statusTask.cancel();
                                statusTask = null;
                            }
                            if (locationTask != null) {
                                locationTask.cancel();
                                locationTask = null;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        networkCallForCancelBooking();

                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(TrackAmbulanceActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }


    private void networkCallForCancelBooking() {
        try {
            if (Utils.isNetworkAvailable(TrackAmbulanceActivity.this)) {
                HashMap<String, Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id", preference.getCustomerId());
                bookingDetails.put("booking_id", preference.getBookingId());
                bookingDetails.put("ambulanceApp", false);
                bookingDetails.put("customerApp", true);
                bookingDetails.put("customer_mobile", Utils.encrypt(preference.getMobileNumber()));

                Log.i("details for cancelling", Arrays.asList(bookingDetails).toString());



                Call<StatusResponse> cancelBooking = apiService.cancelBooking(bookingDetails, preference.getCustomerId(), preference.getAccessToken());
                Utils.showProgressDialog(TrackAmbulanceActivity.this);
                cancelBooking.enqueue(new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        Utils.hideProgressDialog();
                        try {
                            if (response.body() != null) {
                                Log.i("cancel booking response", response.body().toString());
                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE1 ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE2 ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE3) {
                                    preference.setTripState(false);
                                    preference.setIsSelf(true);
                                    EsahaiApplication.getApplication().trackEvent("Track ambulance", "Cancel booking", "Booking cancelled");
                                    Intent i = new Intent(TrackAmbulanceActivity.this, DashboardActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                    TrackAmbulanceActivity.this.finish();
                                    Toast.makeText(TrackAmbulanceActivity.this, getResources().getString(R.string.booking_cancel_confirmation), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(TrackAmbulanceActivity.this, getResources().getString(R.string.booking_cancel_failure), Toast.LENGTH_SHORT).show();
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        Utils.hideProgressDialog();
                        Utils.showToast(TrackAmbulanceActivity.this, t.toString());
                    }
                });


            } else {
                Utils.showToast(TrackAmbulanceActivity.this, getResources().getString(R.string.network_error));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   /* private void requestPermissionForLocation() {
        ActivityCompat.requestPermissions(
                TrackAmbulanceActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                1338);
    }*/

    private void setMap() {
        Log.d("booking","setMap");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bitmap ambulance_marker = BitmapFactory.decodeResource(getResources(), R.drawable.ambulance_icn);
                BitmapDescriptor ambulance = BitmapDescriptorFactory.fromBitmap(ambulance_marker);

                try {
                    //googleMap.clear();

                    Log.d("booking","setMap"+bookingConfirmedDetails.toString());
                    if (bookingConfirmedDetails != null) {
                        Bitmap patient_marker = BitmapFactory.decodeResource(getResources(), R.drawable.me_marker_icon);
                        BitmapDescriptor patient = BitmapDescriptorFactory.fromBitmap(patient_marker);
                        double latitude = bookingConfirmedDetails.emergencyLatitude;
                        double longitude = bookingConfirmedDetails.emergencyLongitude;
                        LatLng latLng = new LatLng(latitude, longitude);
                        dest = latLng;
                        if(!isEmergencyMarkerAdded){
                            isEmergencyMarkerAdded = true;
                            googleMap.addMarker(new MarkerOptions().position(latLng).icon(patient));
                        }

                        Log.d("Activebooking","Latitude"+latitude+"Longitude"+longitude);

                    }

                    if (locationUpdateLatLng != null) {
                        origin = new LatLng(locationUpdateLatLng.latitude, locationUpdateLatLng.longitude);
                        Log.d("origin","origin_locationUpdateLatLng != null"+origin);
                        if(!isRouteDrawedFromAmbToEmergency) {
                            if (!isAmbMarkerAdded) {
                                isAmbMarkerAdded = true;
                                ambulanceMarker =
                                        googleMap.addMarker(new MarkerOptions().position(origin).icon(ambulance));
                            }
                        }
                    } else {
                             origin = new LatLng(bookingConfirmedDetails.getAmbulance_latitude(), bookingConfirmedDetails.getAmbulance_longitude());
                        Log.d("origin","origin_locationUpdateLatLng == null"+origin);
                        if(!isRouteDrawedFromAmbToEmergency) {
                           if (!isAmbMarkerAdded) {
                               isAmbMarkerAdded = true;
                               ambulanceMarker =
                                       googleMap.addMarker(new MarkerOptions().position(origin).icon(ambulance));
                           }
                           }
                    }


                    setPolyLine();
                    setPolyLineForHospital();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        }
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        if (item.getItemId() == android.R.id.home) {
            try {
                if(timer!=null){
                    timer.cancel();
                    timer.purge();
                }
                if(status_timer!=null){
                    status_timer.cancel();
                    status_timer.purge();
                }
                if(statusTask!=null){
                    statusTask.cancel();
                    statusTask = null;
                }
                if(locationTask!=null){
                    locationTask.cancel();
                    locationTask = null;
                }
            }catch (Exception e){e.printStackTrace();}

        }
        return super.onOptionsItemSelected(item);
    }*/

    private void setPolyLine() {

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask(false);

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    private void setPolyLineForHospital() {

        Bitmap hospital_marker = BitmapFactory.decodeResource(getResources(), R.drawable.hospital_icn);
        BitmapDescriptor hospital = BitmapDescriptorFactory.fromBitmap(hospital_marker);

        if (!preference.getHospitalLat().equals("") && !preference.getHospitalLng().equals("")) {
            hospital_latLng = new LatLng(Double.parseDouble(preference.getHospitalLat()),
                    Double.parseDouble(preference.getHospitalLng()));
            if(!isHospitalMarkerAdded) {
                isHospitalMarkerAdded = true;
                googleMap.addMarker(new MarkerOptions().position(hospital_latLng).icon(hospital));
            }
            }

        // Getting URL to the Google Directions API
        String url = getHospitalDirectionsUrl(origin, hospital_latLng);

       if(!url.isEmpty()) {
           DownloadTask downloadTask = new DownloadTask(true);

           // Start downloading json data from Google Directions API
           downloadTask.execute(url);
       }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Destination of route
        String key = "key=" +getResources().getString(R.string.geo_coding_key);

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+key;
        Log.i("directions url", url);

        return url;
    }

    private String getHospitalDirectionsUrl(LatLng origin, LatLng dest) {

        String url = "";
        try {
                // Origin of route
                String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

                // Destination of route
                String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

                // Sensor enabled
                String sensor = "sensor=false";

                // Building the parameters to the web service
                String parameters = str_origin + "&" + str_dest + "&" + sensor;

                // Output format
                String output = "json";

                // Building the url to the web service
                url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&maptype=roadmap";
        }catch (Exception e){
//            e.printStackTrace();
            url="";
        }
        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Track Ambulance", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    @Override
    protected void onResume() {
        super.onResume();
        /*if(!Utils.isGPSOn(TrackAmbulanceActivity.this)){
            Utils.turnOnGPS(TrackAmbulanceActivity.this);
        }else {
            setMap();
        }*/
        try {
            if (timer == null) {
                setTimer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (status_timer == null) {
                setTimerForStatus();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap mMap) {
        try {
            googleMap = mMap;
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
       /* if(!Utils.isGPSOn(TrackAmbulanceActivity.this)){
            Utils.turnOnGPS(TrackAmbulanceActivity.this);
        }else {*/
            setMap();
            mMap.setOnCameraIdleListener(this);
            mMap.setOnCameraMoveStartedListener(this);
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCameraIdle() {
        Log.d("onCameraIdle","onCameraIdle = ");
        //  Toast.makeText(this, "The camera has stopped moving.",
        //         Toast.LENGTH_SHORT).show();
       // previousZoomLevel = googleMap.getCameraPosition().zoom;
    }

    @Override
    public void onCameraMoveStarted(int reason) {

        Log.d("onCameraMoveStarted","onCameraMoveStarted = "+reason);
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            //  Toast.makeText(this, "The user gestured on the map.",
            //         Toast.LENGTH_SHORT).show();
            previousZoomLevel = googleMap.getCameraPosition().zoom;
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            //  Toast.makeText(this, "The user tapped something on the map.",
            //        Toast.LENGTH_SHORT).show();
            previousZoomLevel = googleMap.getCameraPosition().zoom;
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            // Toast.makeText(this, "The app moved the camera.",
            //         Toast.LENGTH_SHORT).show();
        }
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        boolean isToHospital;

        public DownloadTask(boolean isToHospital){

            this.isToHospital = isToHospital;
        }
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service

            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("booking","Driving directions response_data:::"+data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("booking","Driving directions response_result::"+result);
            ParserTask parserTask = new ParserTask(isToHospital);

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        boolean isToHospital;

        public ParserTask(boolean isToHospital){

            this.isToHospital = isToHospital;
        }
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {


            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            // Traversing through all the routes
            if (result != null && result.size() > 0) {
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    LatLng position = null;
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        try {

                            double lat = Double.parseDouble(point.get("lat"));
                            double lng = Double.parseDouble(point.get("lng"));
                            String distance = point.get("distance");
                            String duration = point.get("duration");
                            text_distance.setText("ETA is approximately " + distance + " Away / " + duration);
                            position = new LatLng(lat, lng);

                            points.add(position);
                            builder.include(position);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    LatLngBounds bounds = builder.build();
                    int padding = 0; // offset from edges of the map in pixels


//                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//                googleMap.animateCamera(cu);
//                googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));


                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(
                            new LatLng(bookingConfirmedDetails.getAmbulance_latitude(),
                                    bookingConfirmedDetails.ambulance_longitude)));

                    //googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(dest.latitude, dest.longitude)));
                    Log.d("camerazoom","previousZoomLevel"+previousZoomLevel);
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(previousZoomLevel));

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(7);
                    lineOptions.color(Color.RED);

                }



                // Drawing polyline in the Google Map for the i-th route
                if(isToHospital){
                    if(!isRouteDrawedFromEmergenceyToHospital){
                        isRouteDrawedFromEmergenceyToHospital = true;
                        Log.d("booking","Tracking_hospital path drawn");
                        googleMap.addPolyline(lineOptions);


                    }
                }else{
                    if(!isRouteDrawedFromAmbToEmergency){
                        isRouteDrawedFromAmbToEmergency = true;
                        Log.d("booking","Tracking_Ambulance path drawn");
                        googleMap.addPolyline(lineOptions);
                    }
                }
                //LatLng ambulanceLatLng = new LatLng(locationUpdateLatLng.latitude,ambulance_lng);
                animateMarker(ambulanceMarker,locationUpdateLatLng,false);


                /*googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {
                        CameraPosition cameraPosition = googleMap.getCameraPosition();
                        Log.d("Zoom", "Zoom: " + cameraPosition.zoom);

                        if(previousZoomLevel != cameraPosition.zoom)
                        {
                            isZooming = true;
                        }

                        previousZoomLevel = cameraPosition.zoom;

                    }


                });*/

//            moveToBounds(lineOptions);
            }
        }
    }



    public static Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    private void networkCallForBookingStatus() {

        Log.d("networkCallFor","networkCallForBookingStatus");
        try{
            if(Utils.isNetworkAvailable(TrackAmbulanceActivity.this)){
                HashMap<String,Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id",preference.getCustomerId());
                bookingDetails.put("booking_id",preference.getBookingId());
                bookingDetails.put("ambulanceApp",false);
                bookingDetails.put("customerApp",true);
                bookingDetails.put("customer_mobile",Utils.encrypt(preference.getMobileNumber()));

                Log.i("details BookingStatus", Arrays.asList(bookingDetails).toString());

                Call<BookingStatus> bookingStatus = apiService.getBookingStatus(bookingDetails,preference.getCustomerId(),preference.getAccessToken());
                //Utils.showProgressDialog(SearchAmbulanceActivity.this);
                bookingStatus.enqueue(new Callback<BookingStatus>() {
                    @Override
                    public void onResponse(Call<BookingStatus> call, Response<BookingStatus> response) {
                        //Utils.hideProgressDialog();
                        try {
                            if(response.body()!=null) {
                                Log.i("stats booking response", response.body().toString());
                                //Log.i("stats booking response", response.body().getResponseData().toString());
                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE ) {

                                    bookingConfirmedDetails = response.body().getResponseData();
                                    Utils.setAmbulanceStatus(response.body().getResponseData().booking_status);

                                    preference.setBookingDetails(response.body().getResponseData());

                                    bookingConfirmedDetails.setEmergencyLatitude(
                                            response.body().getResponseData().emergencyLatitude);
                                    bookingConfirmedDetails.setEmergencyLongitude(
                                            response.body().getResponseData().emergencyLongitude);
                                    bookingConfirmedDetails.setIs_medical_taxi(response.body().getResponseData().is_medical_taxi());

                                    preference.setHospitalLat(response.body().getResponseData().hospital_latitude);
                                    preference.setHospitalLng(response.body().getResponseData().hospital_longitude);
                                    preference.setMedicalTaxi(response.body().getResponseData().is_medical_taxi());

                                    if(response.body().getResponseData().getBookingStatus().
                                            equals(Constants.WAY_BACK_TO_DESTINATION)){
                                        preference.setBookingCancellable(false);
                                        Log.d("ActiveBooking","Way back to Destination"
                                                +response.body().getResponseData().booking_status);
                                    }

                                     if(response.body().getResponseData().getBookingStatus().
                                            equals(Constants.TRIP_CLOSED)){

                                        Log.d("ActiveBooking","TripClosed"
                                                +response.body().getResponseData().booking_status);


                                        preference.setBookingCancellable(true);
                                        Utils.setAmbulanceStatus("");

                                        preference.setTripState(false);
                                        Utils.setAmbulanceStatus("");
//
                                        EmergencyDetails bookingDetails = new EmergencyDetails();
                                        bookingDetails.setDuration(response.body().getResponseData().duration);
                                        bookingDetails.setCost(response.body().getResponseData().cost);
                                        bookingDetails.setDistance(response.body().getResponseData().distance);
                                        //bookingDetails.setSpeed(response.body().getResponseData().speed);

                                        Intent intent = new Intent(TrackAmbulanceActivity.this, TripDetailsActivity.class);
                                        intent.putExtra("trip_closed_details", bookingDetails);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);



                                    }
                                    else if(response.body().getResponseData().getBookingStatus().
                                            equalsIgnoreCase(Constants.CANCELLED_FAKE) ||
                                            response.body().getResponseData().getBookingStatus().
                                                    equalsIgnoreCase("Cancelled")){

                                        Log.d("ActiveBooking","Cancelled/Fake"
                                                +response.body().getResponseData().booking_status);

                                        preference.setTripState(false);
                                        Utils.setAmbulanceStatus("");
                                        Intent intent = new Intent(TrackAmbulanceActivity.this, DashboardActivity.class);
                                        intent.putExtra("cancelled","cancelled");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);

                                    }



                                } else{
                                        //Toast.makeText(TrackAmbulanceActivity.this, response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();

                                    }

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            Utils.showToast(TrackAmbulanceActivity.this,getResources().getString(R.string.network_error));
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<BookingStatus> call, Throwable t) {
                        //Utils.hideProgressDialog();
                        Utils.showToast(TrackAmbulanceActivity.this, getResources().getString(R.string.network_slow));

                        finish();
                    }
                });





            }else{Utils.showToast(TrackAmbulanceActivity.this,getResources().getString(R.string.network_error));
                finish();
            }

        }catch (Exception e){e.printStackTrace();}
    }


/*
    public void startService() {
        startService(new Intent(this, LocationUpdateService.class));
    }

    public void stopService() {
        stopService(new Intent(this, LocationUpdateService.class));
    }*/


    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {

        Log.d("animateMarker","animateMarker");
        if(marker == null || toPosition == null){
            return;
        }
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                //Log.d("booking","Marker being moved_animated");

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }
}
