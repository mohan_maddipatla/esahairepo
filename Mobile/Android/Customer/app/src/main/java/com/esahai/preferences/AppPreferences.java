package com.esahai.preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.esahai.dataObjects.AmbulanceType;
import com.esahai.dataObjects.BookingConfirmedDetails;
import com.esahai.dataObjects.CountryCode;
import com.esahai.dataObjects.CountryList;
import com.esahai.dataObjects.EmergencyType;
import com.esahai.dataObjects.FaqDetails;
import com.esahai.dataObjects.KnowBy;
import com.esahai.dataObjects.ProfileDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class AppPreferences {
	private static final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName();
	private SharedPreferences _sharedPrefs;
	private Editor _prefsEditor;

	private static String ACCESS_TOKEN = "access_token";
	private static String GCM_TOKEN = "gcm_token";
	private static String CUSTOMER_ID = "customer_id";
	private static String CUSTOMER_NAME = "customer_name";
	private static String OTP = "otp";
	private static String MOBILE_NUMBER = "mobile_number";
	private static String BOOKING_DETAILS = "bookingDetails";
	private static String PROFILE_DETAILS = "profileDetails";
	private static String TRIP_STATE = "trip_state";
	private static String BOOKING_ID = "booking_id";
	private static String IMAGE_URL = "image_url";
	private static String HOSPITAL_LAT = "hospital_lat";
	private static String HOSPITAL_LONG = "hospital_lng";
	private static String SETTING_SHAKE = "shake";
	private static String SETTING_SELF = "isSelf";
	private static String IS_BOOKING = "isBooking";
	private static String PROFILE_PIC = "profile_pic";
	private static String NO_AMBULANCE = "no_ambulance";
	private static String CUSTOMER_CARE = "customer_care";

	private static String BOOKING_CANCELABLE = "BOOKING_CANCELABLE";

	private static String user_Age = "user_Age";

	private static String PROFILE_FILLED_STATE = "shake";

	private static String patient_mobileNumber = "patient_mobileNumber";
	private static String MEDICAL_TAXI_ID = "medical_taxi_id";
	private static String FIRST_TIME_RUN = "first_time_installed";
	private static String IS_PROD = "isProd";
	private static String FAQS = "FAQ";
	private static String PRIVACY_POLICY = "privacy_policy";
	private static String TNC = "terms";
	private static String COME_TO_KNOW = "comeToKnowBy";
	private static String EMERGENCIES = "emergencies";
	private static String AMBULANCE_TYPES = "ambulance_types";
	private static String COUNTRY_CODE = "country_code";
	private static String PHONE_NUMBER_WITHOUT_CC = "phone_number";
	private static String MEDICAL_TAXI = "medical_taxi";
	private static String SELECTED_LANGUAGE = "selected_language";

	private static String ADD_TO_NOTIFY = "add_to_notify";

	private static String ALL_COUNTRY_CODES = "all_country_codes";

	private static String NEW_USER = "NEW_USER";





	public AppPreferences(Context context) {
		try {
			this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
			this._prefsEditor = _sharedPrefs.edit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clearPreferences(){
//		Editor editor =
//				getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE).edit();
		try {
			_prefsEditor.clear();
			_prefsEditor.commit();
		}catch (Exception e){e.printStackTrace();}
	}


	public String getAccessToken() {
		return _sharedPrefs.getString(ACCESS_TOKEN, "");
	}

	public void setAccessToken(String token){
		_prefsEditor.putString(ACCESS_TOKEN, token);
		_prefsEditor.commit();
	}

	public String getGCMToken() {
		return _sharedPrefs.getString(GCM_TOKEN, "");
	}

	public void setGCMToken(String token){
		_prefsEditor.putString(GCM_TOKEN, token);
		_prefsEditor.commit();
	}


	public String getCustomerId() {
		return _sharedPrefs.getString(CUSTOMER_ID, "");
	}

	public void setCustomerId(String customerId){
		_prefsEditor.putString(CUSTOMER_ID, customerId);
		_prefsEditor.commit();
	}


	public String getOTP() {
		return _sharedPrefs.getString(OTP, "");
	}

	public void setOTP(String otp){
		_prefsEditor.putString(OTP, otp);
		_prefsEditor.commit();
	}

	public String getMobileNumber() {
		return _sharedPrefs.getString(MOBILE_NUMBER, "");
	}

	public void setMobileNumber(String mobileNumber){
		_prefsEditor.putString(MOBILE_NUMBER, mobileNumber);
		_prefsEditor.commit();
	}

	public void setBookingDetails(BookingConfirmedDetails bookingDetails){
		Gson gson = new Gson();
		String json = gson.toJson(bookingDetails);
		_prefsEditor.putString(BOOKING_DETAILS, json);
		_prefsEditor.commit();
	}

	public BookingConfirmedDetails getBookingDetails(){
		Gson gson = new Gson();
		String json = _sharedPrefs.getString(BOOKING_DETAILS, "");
		BookingConfirmedDetails obj = gson.fromJson(json, BookingConfirmedDetails.class);
		return obj;
	}

	public void setProfileDetails(ProfileDetails profile){
		Gson gson = new Gson();
		String json = gson.toJson(profile);
		_prefsEditor.putString(PROFILE_DETAILS, json);
		_prefsEditor.commit();
	}

	public ProfileDetails getProfileDetails(){
		Gson gson = new Gson();
		String json = _sharedPrefs.getString(PROFILE_DETAILS, "");
		ProfileDetails obj = gson.fromJson(json, ProfileDetails.class);
		return obj;
	}

	public boolean isTrip() {
		return _sharedPrefs.getBoolean(TRIP_STATE, false);
	}

	public void setTripState(boolean isTrip){
		_prefsEditor.putBoolean(TRIP_STATE, isTrip);
		_prefsEditor.commit();
	}

	public String getBookingId() {
		return _sharedPrefs.getString(BOOKING_ID, "");
	}

	public void setBookingId(String bookingId){
		_prefsEditor.putString(BOOKING_ID, bookingId);
		_prefsEditor.commit();
	}

	public void setProfileImage(String imageURL){
		_prefsEditor.putString(IMAGE_URL, imageURL);
		_prefsEditor.commit();
	}

	public String getProfileImage() {
		return _sharedPrefs.getString(IMAGE_URL, "");
	}


	public String getHospitalLat() {
		return _sharedPrefs.getString(HOSPITAL_LAT, "");
	}

	public void setHospitalLat(String hospitalLat){
		_prefsEditor.putString(HOSPITAL_LAT, hospitalLat);
		_prefsEditor.commit();
	}

	public String getHospitalLng() {
		return _sharedPrefs.getString(HOSPITAL_LONG, "");
	}

	public void setHospitalLng(String  hospitalLng) {
		_prefsEditor.putString(HOSPITAL_LONG, hospitalLng);
		_prefsEditor.commit();
	}

	public boolean isShake() {
		return _sharedPrefs.getBoolean(SETTING_SHAKE, false);
	}

	public void setShakeState(boolean isShake){
		_prefsEditor.putBoolean(SETTING_SHAKE, isShake);
		_prefsEditor.commit();
	}



	public boolean isMedicalTaxi() {
		return _sharedPrefs.getBoolean(MEDICAL_TAXI, false);
	}

	public void setMedicalTaxi(boolean isMedicalTaxi){
		_prefsEditor.putBoolean(MEDICAL_TAXI, isMedicalTaxi);
		_prefsEditor.commit();
	}



	public String getCustomerName() {
		return _sharedPrefs.getString(CUSTOMER_NAME, "");
	}

	public void setCustomerName(String  name) {
		_prefsEditor.putString(CUSTOMER_NAME, name);
		_prefsEditor.commit();
	}

	public boolean isNoAmbulance() {
		return _sharedPrefs.getBoolean(NO_AMBULANCE, false);
	}

	public void setNoAmbulance(boolean isNoAmbulance){
		_prefsEditor.putBoolean(NO_AMBULANCE, isNoAmbulance);
		_prefsEditor.commit();
	}

	public boolean isSelf() {
		return _sharedPrefs.getBoolean(SETTING_SELF, false);
	}

	public void setIsSelf(boolean isSelf){
		_prefsEditor.putBoolean(SETTING_SELF, isSelf);
		_prefsEditor.commit();
	}

	public boolean isBooking() {
		return _sharedPrefs.getBoolean(IS_BOOKING, false);
	}

	public void setIsBooking(boolean isBooking){
		_prefsEditor.putBoolean(IS_BOOKING, isBooking);
		_prefsEditor.commit();
	}

	public String getCustomerCareNumber() {
		return _sharedPrefs.getString(CUSTOMER_CARE, "");
	}

	public void setCustomerCareNumber(String  number) {
		_prefsEditor.putString(CUSTOMER_CARE, number);
		_prefsEditor.commit();
	}

	public boolean getBookingCancellable() {
		return _sharedPrefs.getBoolean(BOOKING_CANCELABLE, true);
	}

	public void setBookingCancellable(boolean  number) {
		_prefsEditor.putBoolean(BOOKING_CANCELABLE, number);
		_prefsEditor.commit();
	}

	public String getUser_Age() {
		return _sharedPrefs.getString(user_Age, "");
	}

	public void setUser_Age(String  number) {
		_prefsEditor.putString(user_Age, number);
		_prefsEditor.commit();
	}

	public String getPatient_MobileNumber() {
		return _sharedPrefs.getString(patient_mobileNumber, "");
	}

	public void setPatient_MobileNumber(String  number) {
		_prefsEditor.putString(patient_mobileNumber, number);
		_prefsEditor.commit();
	}

	public String getMedicalTaxiId(){
		return _sharedPrefs.getString(MEDICAL_TAXI_ID,"");
	}

	public void setMedicalTaxiId(String id){
		_prefsEditor.putString(MEDICAL_TAXI_ID,id);
		_prefsEditor.commit();

	}

	public boolean isFirstRun() {
		return _sharedPrefs.getBoolean(FIRST_TIME_RUN, true);
	}

	public void setFirstRun(boolean isFirst) {
		_prefsEditor.putBoolean(FIRST_TIME_RUN, isFirst);
		_prefsEditor.commit();
	}

	public boolean isProd() {
		return _sharedPrefs.getBoolean(IS_PROD, true);
	}

	public void setProd(boolean isProd) {
		_prefsEditor.putBoolean(IS_PROD, isProd);
		_prefsEditor.commit();
	}

	public void setFAQs(ArrayList<FaqDetails> faqs){
		Gson gson = new Gson();
		String FAQ = gson.toJson(faqs);
		_prefsEditor.putString(FAQS, FAQ);
		_prefsEditor.commit();
	}

	public ArrayList<FaqDetails> getFAQs(){
		String jsonFaqs = _sharedPrefs.getString(FAQS, null);
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<FaqDetails>>() {}.getType();
		ArrayList<FaqDetails> arrayList = gson.fromJson(jsonFaqs, type);
		return arrayList;
	}

	public String getPrivacyPolicy(){
		return _sharedPrefs.getString(PRIVACY_POLICY,"");
	}

	public void setPrivacyPolicy(String privacyPolicy){
		_prefsEditor.putString(PRIVACY_POLICY,privacyPolicy);
		_prefsEditor.commit();

	}

	public String getTNC(){
		return _sharedPrefs.getString(TNC,"");
	}

	public void setTNC(String terms){
		_prefsEditor.putString(TNC,terms);
		_prefsEditor.commit();

	}

	public void setComeToKnow(ArrayList<KnowBy> comeToKnow){
		Gson gson = new Gson();
		String FAQ = gson.toJson(comeToKnow);
		_prefsEditor.putString(COME_TO_KNOW, FAQ);
		_prefsEditor.commit();
	}

	public ArrayList<KnowBy> getComeToKnowBy(){
		String jsonFaqs = _sharedPrefs.getString(COME_TO_KNOW, null);
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<KnowBy>>() {}.getType();
		ArrayList<KnowBy> arrayList = gson.fromJson(jsonFaqs, type);
		return arrayList;
	}

	public void setEmergencies(ArrayList<EmergencyType> emergencies){
		Gson gson = new Gson();
		String FAQ = gson.toJson(emergencies);
		_prefsEditor.putString(EMERGENCIES, FAQ);
		_prefsEditor.commit();
	}

	public ArrayList<EmergencyType> getEmergencies(){
		String jsonFaqs = _sharedPrefs.getString(EMERGENCIES, null);
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<EmergencyType>>() {}.getType();
		ArrayList<EmergencyType> arrayList = gson.fromJson(jsonFaqs, type);
		return arrayList;
	}

	public String getCountryCode(){
		return _sharedPrefs.getString(COUNTRY_CODE,"");
	}

	public void setCountryCode(String countryCode){
		_prefsEditor.putString(COUNTRY_CODE,countryCode);
		_prefsEditor.commit();

	}

	public String getPhoneNumberWithoutCc(){
		return _sharedPrefs.getString(PHONE_NUMBER_WITHOUT_CC,"");
	}

	public void setPhoneNumberWithoutCc(String number){
		_prefsEditor.putString(PHONE_NUMBER_WITHOUT_CC,number);
		_prefsEditor.commit();

	}


	public String getSelectedLanguage(){
		return _sharedPrefs.getString(SELECTED_LANGUAGE,"");
	}

	public void setSelectedLanguage(String language){
		_prefsEditor.putString(SELECTED_LANGUAGE,language);
		_prefsEditor.commit();

	}


	public boolean isEmergencyContactToNotify() {
		return _sharedPrefs.getBoolean(ADD_TO_NOTIFY, true);
	}

	public void setEmergencyContactToNotify(boolean addToNotify) {
		_prefsEditor.putBoolean(ADD_TO_NOTIFY, addToNotify);
		_prefsEditor.commit();
	}

	public void setAmbulanceTYpes(ArrayList<AmbulanceType> ambulanceTypes){
		Gson gson = new Gson();
		String FAQ = gson.toJson(ambulanceTypes);
		_prefsEditor.putString(AMBULANCE_TYPES, FAQ);
		_prefsEditor.commit();
	}

	public ArrayList<AmbulanceType> getAmbulanceTypes(){
		String jsonFaqs = _sharedPrefs.getString(AMBULANCE_TYPES, null);
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<AmbulanceType>>() {}.getType();
		ArrayList<AmbulanceType> arrayList = gson.fromJson(jsonFaqs, type);
		return arrayList;
	}


	public void setAllCountryCodes(ArrayList<CountryCode> countryCodes){
		Gson gson = new Gson();
		String country_code = gson.toJson(countryCodes);
		_prefsEditor.putString(ALL_COUNTRY_CODES, country_code);
		_prefsEditor.commit();
	}

	public ArrayList<CountryCode> getAllCountryCodes(){
		String jsoncountryCodes = _sharedPrefs.getString(ALL_COUNTRY_CODES, null);
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<CountryCode>>() {}.getType();
		ArrayList<CountryCode> arrayList = gson.fromJson(jsoncountryCodes, type);
		return arrayList;
	}


	public boolean isNewUser() {
		return _sharedPrefs.getBoolean(NEW_USER, true);
	}

	public void setNewUser(boolean newUser) {
		_prefsEditor.putBoolean(NEW_USER, newUser);
		_prefsEditor.commit();
	}



}