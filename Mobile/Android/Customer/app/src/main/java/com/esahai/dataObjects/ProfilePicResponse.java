package com.esahai.dataObjects;

/**
 * Created by ragamai on 4/11/16.
 */
public class ProfilePicResponse {

    /*
    "statusCode": "300",
	"statusMessage": "Profile Updated Successfully",
	"imageUrl": "http://54.200.233.193/patients/customer-1478242535563.*"

     */

    public String statusCode;
    public String statusMessage;
    public String imageUrl;


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "ProfilePicResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
