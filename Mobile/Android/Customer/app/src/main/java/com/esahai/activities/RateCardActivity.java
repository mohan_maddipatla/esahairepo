package com.esahai.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.AmbulanceTypeListResponse;
import com.esahai.dataObjects.RateCardResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.services.ShakerService;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.Arrays;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 21/10/16.
 */
public class RateCardActivity extends CommonActivity {
    AppPreferences preferences;
    ApiInterface apiInterface;
    WebView webView_rateCard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_card);
        preferences = new AppPreferences(RateCardActivity.this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.rate_card));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        webView_rateCard = (WebView)findViewById(R.id.webView_rateCard);

        networkCallForRateCard();

    }

    private void networkCallForRateCard() {
        if(Utils.isNetworkAvailable(RateCardActivity.this)){

            HashMap<String,Object> request = new HashMap<>();
            request.put("customerApp", true);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Log.i("req for rateCard", Arrays.asList(request).toString());
            Utils.showProgressDialog(RateCardActivity.this);
            Call<RateCardResponse> rateCard = apiInterface.getRateCard(request,
                    preferences.getCustomerId(),preferences.getAccessToken());

            rateCard.enqueue(new Callback<RateCardResponse>() {
                @Override
                public void onResponse(Call<RateCardResponse> call, Response<RateCardResponse> response) {
                    Utils.hideProgressDialog();
                    try{
                        if(response.body()!=null){
                            Log.i("rateCard res",response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                webView_rateCard.getSettings().setJavaScriptEnabled(true);
                                webView_rateCard.loadDataWithBaseURL("", response.body().getResponseData().get(0).getMain_content(), "text/html", "UTF-8", "");
                            }
                        }

                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<RateCardResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(RateCardActivity.this,getResources().getString(R.string.network_slow));
                }
            });

        }else{Utils.showToast(RateCardActivity.this,getResources().getString(R.string.network_error));}
    }


   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
                finish();
        }
        return super.onOptionsItemSelected(item);
    }*/
}
