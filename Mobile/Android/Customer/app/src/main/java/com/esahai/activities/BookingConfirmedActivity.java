package com.esahai.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.R;
import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.dataObjects.BookingConfirmedDetails;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 27/9/16.
 */
public class BookingConfirmedActivity extends CommonActivity implements View.OnClickListener {

    Button button_patient_info, button_track_ambulance, button_call_driver;
    CircleImageView profile_image;
    BookingConfirmedDetails bookingConfirmedDetails;
    TextView text_driver_name,text_hospital_ambulance,text_distance,text_hospital;
    ImageView image_cancel_trip;
    AppPreferences preference;
    Timer status_timer = null;
    TextView text_ambulance_status;
    ApiInterface apiService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmed_booking);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        initViews();
        preference = new AppPreferences(BookingConfirmedActivity.this);

        try{
            bookingConfirmedDetails = (BookingConfirmedDetails) getIntent().getSerializableExtra("bookingDetails");
            if(bookingConfirmedDetails==null){
                bookingConfirmedDetails = preference.getBookingDetails();
            }
            Log.i("bookingConfirmedDetails",bookingConfirmedDetails.toString());
        }catch (Exception e){e.printStackTrace();}


        try{
            if(getIntent().hasExtra("status")) {
                String status = getIntent().getExtras().getString("status");
                if (status.equalsIgnoreCase("locationUpdate")) {
                    button_track_ambulance.performClick();
                }
            }
        }catch (Exception e){e.printStackTrace();}

        setProfileImage();
        setData();
        setTimerForStatus();

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {

            if (status_timer == null) {
                setTimerForStatus();
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if(status_timer!=null){
                status_timer.cancel();
                status_timer = null;
            }
        }catch (Exception e){e.printStackTrace();}
    }

    private void setData() {
        if(bookingConfirmedDetails!=null){
            text_driver_name.setText("Mr. "+bookingConfirmedDetails.getDriverName());
            text_hospital_ambulance.setText("Booking Id: "+preference.getBookingId());
            text_distance.setText(bookingConfirmedDetails.getDistance()+" Kms Away"+"(ETA: "+bookingConfirmedDetails.getDuration()+")");
//            text_hospital.setText("");
        }

    }

    private void setTimerForStatus() {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (status_timer != null) {
                        status_timer.cancel();
                        status_timer = null;
                    }
                    status_timer = new Timer();
                    TimerTask minuteTask = new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (Utils.getAmbulanceStatus() != null)
                                            text_ambulance_status.setText("" + Utils.getAmbulanceStatus());
                                        else
                                            text_ambulance_status.setText("");
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    status_timer.schedule(minuteTask, 0l, 200);
                }
            }).start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initViews() {

        button_patient_info = (Button) findViewById(R.id.button_patient_info);
        button_track_ambulance = (Button) findViewById(R.id.button_track_ambulance);
        button_call_driver = (Button) findViewById(R.id.button_call_driver);
        image_cancel_trip = (ImageView)findViewById(R.id.image_cancel_trip);

        profile_image = (CircleImageView)findViewById(R.id.profile_image);

        text_driver_name = (TextView)findViewById(R.id.text_driver_name);
        text_hospital_ambulance = (TextView)findViewById(R.id.text_hospital_ambulance);
        text_distance = (TextView)findViewById(R.id.text_distance);
        text_ambulance_status = (TextView)findViewById(R.id.text_ambulance_status);
        text_hospital = (TextView)findViewById(R.id.text_hospital);


        button_patient_info.setOnClickListener(this);
        button_track_ambulance.setOnClickListener(this);
        button_call_driver.setOnClickListener(this);
        image_cancel_trip.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private void setProfileImage() {

    try {
        if (bookingConfirmedDetails != null) {
            String url = bookingConfirmedDetails.getDriverProfilePic();
            String image_url = url.replace("\\", "");
            Picasso
                    .with(BookingConfirmedActivity.this)
                    .load(image_url)
                    .into(profile_image);
        }
    }catch (Exception e){e.printStackTrace();}

    }

    private void requestPermissionForCall(){
        ActivityCompat.requestPermissions(BookingConfirmedActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_call_driver:

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                   if(bookingConfirmedDetails!=null) {
                       Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + bookingConfirmedDetails.getDriverMobileNumber()));
                       startActivity(intent);
                   }
                }
                else{
                    requestPermissionForCall();
//                    Toast.makeText(BookingConfirmedActivity.this,"Permission denied",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.button_track_ambulance:
                Intent track = new Intent(BookingConfirmedActivity.this, TrackAmbulanceActivity.class);
                track.putExtra("bookingConfirmedDetails",bookingConfirmedDetails);
                startActivity(track);
                break;
            case R.id.button_patient_info:
                Intent update_info = new Intent(BookingConfirmedActivity.this, UpdatePatientInfoActivity.class);
                startActivity(update_info);
                break;

            case R.id.image_cancel_trip:
                showAlertDialog();
//                finish();
                break;
        }

    }


    private void showAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.cancel_message))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        networkCallForCancelBooking();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void networkCallForCancelBooking() {
        try{
            if(Utils.isNetworkAvailable(BookingConfirmedActivity.this)){
                HashMap<String,Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id",preference.getCustomerId());
                bookingDetails.put("booking_id",preference.getBookingId());
                bookingDetails.put("ambulanceApp",false);
                bookingDetails.put("customerApp",true);

                Log.i("details for cancelling", Arrays.asList(bookingDetails).toString());

                Call<StatusResponse> cancelBooking = apiService.cancelBooking(bookingDetails,preference.getCustomerId(),preference.getAccessToken());
                Utils.showProgressDialog(BookingConfirmedActivity.this);
                cancelBooking.enqueue(new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        Utils.hideProgressDialog();
                        try {
                            if(response.body()!=null) {
                                Log.i("cancel booking response", response.body().toString());
                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE1 ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE2 ||
                                        Integer.parseInt(response.body().getStatusCode()) == Constants.CANCELLED_CODE3) {
                                    preference.setTripState(false);
                                    Intent i = new Intent(BookingConfirmedActivity.this, DashboardActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP );
                                    startActivity(i);
                                    BookingConfirmedActivity.this.finish();
                                    Toast.makeText(BookingConfirmedActivity.this, getResources().getString(R.string.booking_cancel_confirmation), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(BookingConfirmedActivity.this, getResources().getString(R.string.booking_cancel_failure), Toast.LENGTH_SHORT).show();
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        Utils.hideProgressDialog();
                        Toast.makeText(BookingConfirmedActivity.this, getResources().getString(R.string.booking_cancel_failure), Toast.LENGTH_SHORT).show();

                    }
                });





            }else{Utils.showToast(BookingConfirmedActivity.this,getResources().getString(R.string.network_error));}

        }catch (Exception e){e.printStackTrace();}
    }
}
