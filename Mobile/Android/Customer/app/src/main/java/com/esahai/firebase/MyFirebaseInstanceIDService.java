package com.esahai.firebase;

import android.util.Log;

import com.esahai.preferences.AppPreferences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.moengage.push.PushManager;

/**
 * Created by prashanth on 29/9/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /*public static final String[] TOPICS = {"faq","terms_conditions","privacy_policy","emergency_types",
            "come_to_know_by"};*/

    public static final String[] TOPICS = {"FAQ","TNC","PRIVACY-POLICY","EMERGENCY-TYPES",
            "HOW-DO-YOU-KNOW","COUNTRY_LIST"};



    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]


    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        PushManager.getInstance().refreshToken(getApplicationContext(), refreshedToken);
        Log.d("MOENGAGE","Token:::"+refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        Common.saveBooleanPref(this,Constants.isTokenSentToServer,false);
//        Common.sendRegistrationToServer(this);
      //  FirebaseMessaging.getInstance().subscribeToTopic("bloodRequest");
        registerTopics();
    }
    // [END refresh_token]




    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     *
     */



    private void registerTopics(){
        for(String topic:TOPICS){
            FirebaseMessaging.getInstance().subscribeToTopic(topic);
        }

    }

}
