package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by adheesh on 30/01/17.
 */

public class PrivacyPolicyResponse {

    public String statusCode;
    //public String statusMessage;
    public PolicyResponse responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

   /* public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
    */

    public PolicyResponse getResponseData() {
        return responseData;
    }

    public void setResponseData(PolicyResponse responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "PrivacyPolicyResponse{" +
                "statusCode='" + statusCode + '\'' +
                //", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }



}
