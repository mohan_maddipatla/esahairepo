package com.esahai.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.dataObjects.NavExpandedMenuModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by adheesh on 24/01/17.
 */

public class NavExpandableListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    //private ArrayList<String> mListDataHeader; // header titles

    // child data in format of header title, child title
    //private HashMap<String, ArrayList<String>> mListDataChild;
    ArrayList<NavExpandedMenuModel> mListDataHeader;
    HashMap<NavExpandedMenuModel, ArrayList<String>> mListDataChild;
    ExpandableListView expandList;
    public NavExpandableListAdapter(Context context, ArrayList<NavExpandedMenuModel> listDataHeader, HashMap<NavExpandedMenuModel, ArrayList<String>> listChildData, ExpandableListView mView)
    {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
        this.expandList=mView;
    }

    @Override
    public int getGroupCount() {
        int i= mListDataHeader.size();
//        Log.d("GROUPCOUNT",String.valueOf(i));
        return this.mListDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCount=0;
        //if(groupPosition!=2)
        //{

                childCount = this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                        .size();

       // }
        return childCount;
    }

    @Override
    public Object getGroup(int groupPosition) {

        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
//        Log.d("CHILD",mListDataChild.get(this.mListDataHeader.get(groupPosition))
//                .get(childPosition).toString());
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        NavExpandedMenuModel headerTitle = (NavExpandedMenuModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.listheader, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.submenu);
        ImageView headerIcon=    (ImageView)convertView.findViewById(R.id.iconimage);

        ImageView arrowIcon=    (ImageView)convertView.findViewById(R.id.arrowimg);

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.getIconName());

        headerIcon.setImageDrawable(mContext.getResources().getDrawable(headerTitle.getImageId()));

        if(getChildrenCount(groupPosition) == 0 ){
            arrowIcon.setVisibility(View.INVISIBLE);
        }
        else {
            arrowIcon.setVisibility(View.VISIBLE);
            if (isExpanded) {
//                Log.d("Navigation", "Expanded::::");
                arrowIcon.setImageResource(R.drawable.arrow_up);
            } else {
//                Log.d("Navigation", "Shrinked::::");
                arrowIcon.setImageResource(R.drawable.arrow_down);
            }
        }


        return convertView;


    }



    @Override
    public View getChildView(int groupPosition, int childPosition,  boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_submenu, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.submenu);

        txtListChild.setText(childText);





        return convertView;
    }




    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



}
