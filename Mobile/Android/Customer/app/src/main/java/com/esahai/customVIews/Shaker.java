package com.esahai.customVIews;

/**
 * Created by adheesh on 17/10/16.
 */

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.util.Log;

public class Shaker {

    private static final int MIN_FORCE = 20;
    private static final int MIN_DIRECTION_CHANGE = 3;

    /** Maximum pause between movements. */
    private static final int MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE = 200;

    /** Maximum allowed time for shake gesture. */
    private static final int MAX_TOTAL_DURATION_OF_SHAKE = 400;

    /** Time when the gesture started. */
    private long mFirstDirectionChangeTime = 0;

    /** Time when the last movement started. */
    private long mLastDirectionChangeTime;

    /** How many movements are considered so far. */
    private int mDirectionChangeCount = 0;

    /** The last x position. */
    private float lastX = 0;

    /** The last y position. */
    private float lastY = 0;

    /** The last z position. */
    private float lastZ = 0;



    private SensorManager mgr=null;
    private long lastShakeTimestamp=0;
    private double threshold=1.0d;
    private long gap=0;
    private Shaker.Callback cb=null;

    private OnShakeListener mShakeListener;

    public Shaker(Context ctxt, double threshold, long gap,
                  Shaker.Callback cb) {
        this.threshold=threshold*threshold;
        this.threshold=this.threshold
                *SensorManager.GRAVITY_EARTH
                *SensorManager.GRAVITY_EARTH;
        this.gap=gap;
        this.cb=cb;

        mgr=(SensorManager)ctxt.getSystemService(Context.SENSOR_SERVICE);
        mgr.registerListener(listener,
                mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);
    }

    public void close() {
        mgr.unregisterListener(listener);
    }

    private void isShaking() {
        long now=SystemClock.uptimeMillis();

        if (lastShakeTimestamp==0) {
            lastShakeTimestamp=now;

            if (cb!=null) {
                cb.shakingStarted();
            }
        }
        else {
            lastShakeTimestamp=now;
        }
    }

    private void isNotShaking() {
        long now=SystemClock.uptimeMillis();

        if (lastShakeTimestamp>0) {
            if (now-lastShakeTimestamp>gap) {
                lastShakeTimestamp=0;

                if (cb!=null) {
                    cb.shakingStopped();
                }
            }
        }
    }

    public interface Callback {
        void shakingStarted();
        void shakingStopped();

    }

    private SensorEventListener listener=new SensorEventListener() {
        public void onSensorChanged(SensorEvent e) {

            // get sensor data
            float x = e.values[SensorManager.DATA_X];
            float y = e.values[SensorManager.DATA_Y];
            float z = e.values[SensorManager.DATA_Z];

            // calculate movement
            float totalMovement = Math.abs(x + y + z - lastX - lastY - lastZ);

            if (totalMovement > MIN_FORCE) {

                // get time
                long now = System.currentTimeMillis();

                // store first movement time
                if (mFirstDirectionChangeTime == 0) {
                    mFirstDirectionChangeTime = now;
                    mLastDirectionChangeTime = now;
                }

                // check if the last movement was not long ago
                long lastChangeWasAgo = now - mLastDirectionChangeTime;
                if (lastChangeWasAgo < MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE) {

                    // store movement data
                    mLastDirectionChangeTime = now;
                    mDirectionChangeCount++;

                    // store last sensor data
                    lastX = x;
                    lastY = y;
                    lastZ = z;

                    Log.d("Shaking","No of times shaked:"+mDirectionChangeCount);

                    // check how many movements are so far
                    if (mDirectionChangeCount == MIN_DIRECTION_CHANGE) {

                        Log.d("ConditionAccepted","No of times shaked:"+mDirectionChangeCount);
                        // check total duration
                        long totalDuration = now - mFirstDirectionChangeTime;
                        if (totalDuration < MAX_TOTAL_DURATION_OF_SHAKE) {

                            Log.d("ShakeContinous","isShaking METHOD:"+mDirectionChangeCount);

                            isShaking();
                            resetShakeParameters();

                        }
                    }

                }
                else {
                    resetShakeParameters();
                    isNotShaking();
                    Log.d("NotShaking","Values Reset");
                }
            }

            else {
                //resetShakeParameters();
                isNotShaking();
                // Log.d("NotShaking","IN Outer ELSE");
            }

        }

        private void resetShakeParameters() {
            mFirstDirectionChangeTime = 0;
            mDirectionChangeCount = 0;
            mLastDirectionChangeTime = 0;
            lastX = 0;
            lastY = 0;
            lastZ = 0;
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // unused
        }
    };

    public interface OnShakeListener
    {
        public void onShake();
    }

    public void setOnShakeListener(OnShakeListener listener) {
        mShakeListener = listener;
    }
}