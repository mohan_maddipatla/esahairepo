package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 21/10/16.
 */
public class GoogleAddress {

    public ArrayList<Address> results;
    public String status;

    public ArrayList<Address> getResults() {
        return results;
    }

    public void setResults(ArrayList<Address> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GoogleAddress{" +
                "results=" + results +
                ", status='" + status + '\'' +
                '}';
    }
}
