package com.esahai.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.dataObjects.FaqDetails;


import java.util.ArrayList;

/**
 * Created by adheesh on 10/03/17.
 */

public class FaqExpandableListAdapter extends BaseExpandableListAdapter {
    private Context mContext;

    ExpandableListView expandList;

    ArrayList<FaqDetails> mListQuesAns;

    public FaqExpandableListAdapter(Context context, ArrayList<FaqDetails> listQuesAns , ExpandableListView mView)
    {
        this.mContext = context;
        this.mListQuesAns = listQuesAns;
        this.expandList=mView;
    }

    @Override
    public int getGroupCount() {
        int i= mListQuesAns.size();
        return this.mListQuesAns.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCount=1;

        return childCount;
    }

    @Override
    public Object getGroup(int groupPosition) {

        return this.mListQuesAns.get(groupPosition).getQuestion();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return this.mListQuesAns.get(groupPosition).getAnswer();

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.faq_list_header, null);
        }
        TextView questionListHeaderTv = (TextView) convertView
                .findViewById(R.id.question_sub);

        ImageView arrowIcon=    (ImageView)convertView.findViewById(R.id.arrowimg);

        questionListHeaderTv.setTypeface(null, Typeface.BOLD);
        questionListHeaderTv.setText(headerTitle);



        if(getChildrenCount(groupPosition) == 0 ){
            arrowIcon.setVisibility(View.INVISIBLE);
        }
        else {
            arrowIcon.setVisibility(View.VISIBLE);
            if (isExpanded) {
//                Log.d("Navigation", "Expanded::::");
                arrowIcon.setImageResource(R.drawable.up_arrow_blue);
            } else {
//                Log.d("Navigation", "Shrinked::::");
                arrowIcon.setImageResource(R.drawable.down_arrow_blue);
            }
        }


        return convertView;


    }



    @Override
    public View getChildView(int groupPosition, int childPosition,  boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.faq_list_submenu, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.submenu_item);

        txtListChild.setText(childText);





        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



}
