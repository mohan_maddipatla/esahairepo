package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 1/12/16.
 */
public class CustomerCareResponse {

    /*
    {"statusCode":"300","tableName":"app_settings","Result":[{"id":"1","fieldName":"centre_number","value":"9030002764","created_date":"2016-11-04 08:28:36"}]}
     */

    public String statusCode;
    public String tableName;
    public String responseData;

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }


    //public ArrayList<CustomerCareNumberResponse> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /*public ArrayList<CustomerCareNumberResponse> getResult() {
        return responseData;
    }

    public void setResult(ArrayList<CustomerCareNumberResponse> responseData) {
        this.responseData = responseData;
    }*/

    @Override
    public String toString() {
        return "CustomerCareResponse{" +
                "statusCode='" + statusCode + '\'' +
               // ", tableName='" + tableName + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
