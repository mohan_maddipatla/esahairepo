package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 3/10/16.
 */
public class EmergencyTypeList {

    /*
    "statusCode":"300","responseData":[{"id":"1","type_name":"Critical Emergency","created_date":"2016-09-19 20:36:12","updated_date":"0000-00-00 00:00:00"},{"id":"2","type_name":"Cardiac Arrest","created_date":"2016-09-22 12:14:06","updated_date":null},{"id":"3","type_name":"Chronic Chest Pain","created_date":"2016-09-22 12:14:20","updated_date":null},{"id":"4","type_name":"Heavy Bleeding","created_date":"2016-09-22 12:14:46","updated_date":null},{"id":"5","type_name":"Poisoning","created_date":"2016-09-22 12:14:55","updated_date":null},{"id":"6","type_name":"General Medical Help","created_date":"2016-09-22 12:15:08","updated_date":null}]}
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<EmergencyType> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<EmergencyType> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<EmergencyType> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "EmergencyTypeList{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
