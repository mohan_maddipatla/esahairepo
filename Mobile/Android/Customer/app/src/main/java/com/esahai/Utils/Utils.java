package com.esahai.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.R;
import com.esahai.activities.OTPVerificationActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import ch.hsr.geohash.GeoHash;

/**
 * Created by ragamai on 20/9/16.
 */
public class Utils {

    private static ProgressDialog progressDialog;
    private static LatLng latLng;
    private static String ambulance_status;


    public static void closeKeyBoard(Context context, View view){
        try {
            InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public static String encrypt(String userName) {

        String password = "@*%$!eSaHai";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();

            byte[] salt = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};

            SecretKeyFactory factory =
                    SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

            /* String str = new String(digest); */
            KeySpec spec = new PBEKeySpec(Base64.encodeToString(digest, Base64.NO_WRAP).toCharArray(), salt, 1000, 256 + 128);
            SecretKey secretKey = factory.generateSecret(spec);
            //SecretKeySpec secretKeySpec = new SecretKeySpec(digest, "AES");
            byte[] data = secretKey.getEncoded();
            byte[] keyBytes = new byte[256 / 8];
            byte[] ivBytes = new byte[128 / 8];

            System.arraycopy(data, 0, keyBytes, 0, 256 / 8);
            System.arraycopy(data, 0, ivBytes, 0, 128 / 8);

            IvParameterSpec iv = new IvParameterSpec(ivBytes);
            SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(userName.getBytes());

            return Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String getGeoHashCode(Location location){

        GeoHash st = GeoHash.withCharacterPrecision(location.getLatitude(), location.getLongitude(), Constants.locationPrecisionNumber);
        return st.toBase32();
    }

    public static String getIMEINumber(Context context){
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public static void showProgressDialog(Context context){
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("loading");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Exception e){e.printStackTrace();}
    }

    public static void hideProgressDialog(){
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }catch (Exception e){e.printStackTrace();}

    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model.toUpperCase();
        } else {
            return manufacturer.toUpperCase() + " " + model;
        }
    }

    public static String getOSVersion() {
        String versionRelease = Build.VERSION.RELEASE;

        return versionRelease;

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }


    public static AlertDialog alert = null;
    public static void showAlert(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        alert.dismiss();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    public static boolean isGPSOn(final Context context){
        boolean gps_enabled = false;
        boolean network_enabled = false;

        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return  gps_enabled || network_enabled;

    }

    public static AlertDialog.Builder dialog;
    public static void turnOnGPS(final Context context){
            // notify user
            dialog = new AlertDialog.Builder(context);
            dialog.setMessage("GPS not enabled");
            dialog.setPositiveButton("Turn on GPS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    dialog.create().dismiss();
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    dialog.create().dismiss();

                }
            });
            dialog.show();
        }


    public static void updateLatLng(double latitude, double longitude){

         latLng = new LatLng(latitude,longitude);

    }


    public static LatLng getLatLng(){
        return latLng;
    }


    public static void setAmbulanceStatus(String status){
        ambulance_status = status;
    }

    public static String getAmbulanceStatus(){
        return ambulance_status;
    }


}
