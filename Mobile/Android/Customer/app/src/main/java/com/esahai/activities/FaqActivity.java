package com.esahai.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.adapters.FaqAdapter;
import com.esahai.adapters.FaqExpandableListAdapter;
import com.esahai.adapters.NavExpandableListAdapter;
import com.esahai.dataObjects.FaqDetails;
import com.esahai.dataObjects.FaqsList;
import com.esahai.dataObjects.NavExpandedMenuModel;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaqActivity extends CommonActivity {


    ApiInterface apiService;
    AppPreferences preferences;
    ArrayList<FaqDetails> faqs;

    ExpandableListView expandableListFAQ;
    FaqExpandableListAdapter mMenuAdapter;

    ArrayList<String> listDataQuestions ;
    HashMap<FaqDetails,String> listDataAnswers ;
    private int lastExpandedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        preferences = new AppPreferences(FaqActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.faqs));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        expandableListFAQ = (ExpandableListView) findViewById(R.id.list_view_faq);


        listDataQuestions = new ArrayList<String>();
        listDataAnswers = new HashMap<FaqDetails,String>();

        if(preferences.getFAQs()==null) {

            networkCallForFaqs();
        }else{
            setData();
        }

        expandableListFAQ.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableListFAQ.collapseGroup(lastExpandedPosition);
                    lastExpandedPosition = -1;
                }

                lastExpandedPosition = groupPosition;


            }
        });

    }



    private void setData() {
        try{
            Log.i("local data","FAQs");
            faqs = preferences.getFAQs();
            if (faqs != null && faqs.size() > 0) {

                Log.d("faqs","faqs_Response"+faqs);

                mMenuAdapter = new FaqExpandableListAdapter(this,faqs, expandableListFAQ);

                mMenuAdapter.notifyDataSetChanged();

                // setting list adapter
                expandableListFAQ.setAdapter(mMenuAdapter);

            }
        }catch (Exception e){e.printStackTrace();}


    }

    public void networkCallForFaqs(){


        if(Utils.isNetworkAvailable(FaqActivity.this)){
            apiService = ApiClient.getClient().create(ApiInterface.class);

            HashMap<String,Object> faqRequest = new HashMap<>();
            faqRequest.put("customerApp",true);
            faqRequest.put("ambulanceApp",false);
            faqRequest.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Log.i("faq request", Arrays.asList(faqRequest).toString());
            Call<FaqsList> faqsList = apiService.getFaqsList(faqRequest,preferences.getCustomerId(),preferences.getAccessToken());
            Utils.showProgressDialog(FaqActivity.this);
            faqsList.enqueue(new Callback<FaqsList>() {
                @Override
                public void onResponse(Call<FaqsList> call, Response<FaqsList> response) {
                    Utils.hideProgressDialog();
                    try {
                        if(response!=null) {
//                            Log.i("faqs list", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                faqs = response.body().getResponseData();
                                preferences.setFAQs(faqs);

                                Log.d("faqs","faqs_Response"+faqs);
                                if (faqs != null && faqs.size() > 0) {
                                    mMenuAdapter = new FaqExpandableListAdapter(FaqActivity.this, faqs, expandableListFAQ);

                                    mMenuAdapter.notifyDataSetChanged();

                                    // setting list adapter
                                    expandableListFAQ.setAdapter(mMenuAdapter);

                                }
                            }
                        }
                        else{
                            Log.d("Faq","Response is null");
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<FaqsList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    //Utils.showToast(EmergenciesActivity.this,t.toString());
                    Utils.showToast(FaqActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        }else{Utils.showToast(FaqActivity.this,getResources().getString(R.string.network_error));}
    }
}
