package com.esahai.dataObjects;

/**
 * Created by ragamai on 31/1/17.
 */
public class KnowBy {

    /*
    "id": "1",
     "know_by": "Friends",
      "status": "Active"
     */

    public String id;
    public String know_by;
    public String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKnow_by() {
        return know_by;
    }

    public void setKnow_by(String know_by) {
        this.know_by = know_by;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "KnowBy{" +
                "id='" + id + '\'' +
                ", know_by='" + know_by + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
