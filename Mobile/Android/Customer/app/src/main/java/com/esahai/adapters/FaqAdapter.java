package com.esahai.adapters;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.esahai.R;
import com.esahai.dataObjects.FaqDetails;
import java.util.ArrayList;

/**
 * Created by adheesh on 30/01/17.
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.ViewHolder> {

    Context context;
    ArrayList<FaqDetails> faqsList;
    RecyclerView recycle_list_faqs;

    public FaqAdapter(Context context, ArrayList<FaqDetails> faqsList, RecyclerView recycle_list_faqs) {
        this.context = context;
        this.faqsList = faqsList;
        this.recycle_list_faqs = recycle_list_faqs;
    }

    @Override
    public FaqAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.faqs_list_item, parent, false);

        FaqAdapter.ViewHolder viewHolder = new FaqAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(faqsList != null){
            holder.TvQuestion.setText(""+faqsList.get(position).getQuestion());
            holder.TvAnswer.setText(""+faqsList.get(position).getAnswer());

        }

    }


    @Override
    public int getItemCount() {
        return faqsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView TvQuestion;
        TextView TvAnswer;


        public ViewHolder(View view) {
            super(view);
            TvQuestion = (TextView)view.findViewById(R.id.textView_question);
            TvAnswer = (TextView)view.findViewById(R.id.textView_answer);

        }
    }

}
