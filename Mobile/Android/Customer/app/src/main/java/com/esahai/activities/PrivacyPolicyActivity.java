package com.esahai.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.PrivacyPolicyResponse;
import com.esahai.dataObjects.TermsAndConditionsResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyPolicyActivity extends CommonActivity {

    AppPreferences preferences;
    ApiInterface apiInterface;
    WebView webView_privacy_policy;
    String privacyPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        preferences = new AppPreferences(PrivacyPolicyActivity.this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.privacy_policy));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        webView_privacy_policy = (WebView)findViewById(R.id.webView_pri_policy);

        if(preferences.getPrivacyPolicy().equalsIgnoreCase("")) {
            networkCallForPrivacyPolicy();
        }else{
            setData();
        }

    }

    private void setData() {
        try{
            if(preferences.getPrivacyPolicy()!=null){
                privacyPolicy = preferences.getPrivacyPolicy();
                webView_privacy_policy.getSettings().setJavaScriptEnabled(true);

                webView_privacy_policy.loadDataWithBaseURL("", privacyPolicy,
                        "text/html", "UTF-8", "");
            }
        }catch (Exception e){e.printStackTrace();}
    }


    public void networkCallForPrivacyPolicy(){
        Utils.showProgressDialog(this);
        if(Utils.isNetworkAvailable(PrivacyPolicyActivity.this)){

            Call<PrivacyPolicyResponse> getPolicy = apiInterface.getPrivacyPolicy();

            Log.i("policy response","Calling the P&P API");
            getPolicy.enqueue(new Callback<PrivacyPolicyResponse>() {
                @Override
                public void onResponse(Call<PrivacyPolicyResponse> call, Response<PrivacyPolicyResponse> response) {
                    try{
                        if(response.body()!=null){
                            Log.i("policy response",response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                preferences.setPrivacyPolicy(response.body().getResponseData().getPrivacy_policy());
                                webView_privacy_policy.getSettings().setJavaScriptEnabled(true);

                                webView_privacy_policy.loadDataWithBaseURL("", response.body().
                                                getResponseData().getPrivacy_policy(),
                                        "text/html", "UTF-8", "");

                                Utils.hideProgressDialog();
                            }
                            else{
                                Utils.hideProgressDialog();
                            }
                            Utils.hideProgressDialog();
                        }
                        else{
                            Utils.hideProgressDialog();
                        }

                    }catch (Exception e){
                        Utils.hideProgressDialog();
                        e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<PrivacyPolicyResponse> call, Throwable t) {

                    t.printStackTrace();
                    Utils.hideProgressDialog();
                    Utils.showToast(PrivacyPolicyActivity.this, getResources().getString(R.string.network_slow));

                }
            });

        }else{

            Utils.hideProgressDialog();
            Utils.showToast(PrivacyPolicyActivity.this,getResources().getString(R.string.network_error));}

    }

}
