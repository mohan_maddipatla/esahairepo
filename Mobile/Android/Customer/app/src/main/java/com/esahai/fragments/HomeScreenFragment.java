package com.esahai.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.Utils.AppObserver;
import com.esahai.activities.BloodRequestActivity;
import com.esahai.activities.EMSActivity;
import com.esahai.activities.MedicalTaxiActivity;
import com.esahai.activities.NearByHospitalsActivity;
import com.esahai.activities.TrackAmbulanceActivity;
import com.esahai.dataObjects.BookingStatus;
import com.esahai.dataObjects.StatusListResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 19/1/17.
 */
public class HomeScreenFragment extends Fragment implements View.OnClickListener,AppObserver {

    TextView textView_ambulance,textView_medical_taxi,textView_blood_bank,textView_nearest_hospital;


    AppPreferences preferences;
    ApiInterface apiInterface;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_home_screen, null);

        preferences = new AppPreferences(getActivity());
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        initialize(root);



        if (Utils.isNetworkAvailable(getActivity())) {

            getActiveBooking();
//            getStatusList();

        } else {
//            showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
            Utils.showToast(getActivity(), getResources().getString(R.string.network_error));
        }


        return root;

    }

    private void initialize(ViewGroup root) {
        try{

            textView_ambulance = (TextView)root.findViewById(R.id.textView_ambulance);
            textView_medical_taxi = (TextView)root.findViewById(R.id.textView_medical_taxi);
            textView_blood_bank = (TextView)root.findViewById(R.id.textView_blood_bank);
            textView_nearest_hospital = (TextView)root.findViewById(R.id.textView_nearest_hospital);

            textView_ambulance.setOnClickListener(HomeScreenFragment.this);
            textView_medical_taxi.setOnClickListener(HomeScreenFragment.this);
            textView_blood_bank.setOnClickListener(HomeScreenFragment.this);
            textView_nearest_hospital.setOnClickListener(HomeScreenFragment.this);

        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textView_ambulance:callAmbulanceActivity();break;
            case R.id.textView_medical_taxi:callMedicalTaxiActivity();break;
            case R.id.textView_blood_bank:callBloodBankActivity();break;
            case R.id.textView_nearest_hospital:callNearestHospitalActivity();break;
        }

    }

    private void callNearestHospitalActivity() {
        Intent bloodRequest = new Intent(getActivity(), NearByHospitalsActivity.class);
        startActivity(bloodRequest);
    }

    private void callBloodBankActivity() {
        Intent bloodRequest = new Intent(getActivity(), BloodRequestActivity.class);
        startActivity(bloodRequest);
    }

    private void callMedicalTaxiActivity() {
        Intent medicalTaxi = new Intent(getActivity(), MedicalTaxiActivity.class);
        startActivity(medicalTaxi);
    }

    private void callAmbulanceActivity() {

        Intent emsActivity = new Intent(getActivity(), EMSActivity.class);
        startActivity(emsActivity);
    }

    @Override
    public void update(int event, Object arg) {
        if(event == EsahaiApplication.bookingStatusUpdate){
            getActiveBooking();
        }

    }




    private void getActiveBooking() {



        Log.d("Activebooking", "ActivebookingID");
        try {
            if (Utils.isNetworkAvailable(getActivity())) {
                HashMap<String, Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id", preferences.getCustomerId());
                bookingDetails.put("ambulanceApp", false);
                bookingDetails.put("customerApp", true);
                bookingDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

                Log.i("Activebooking", Arrays.asList(bookingDetails).toString());

                Call<BookingStatus> activeBooking = apiInterface.getActiveBooking(bookingDetails, preferences.getCustomerId(), preferences.getAccessToken());
                //Utils.showProgressDialog(SearchAmbulanceActivity.this);
                activeBooking.enqueue(new Callback<BookingStatus>() {
                    @Override
                    public void onResponse(Call<BookingStatus> call, Response<BookingStatus> response) {
                        //Utils.hideProgressDialog();
                        //Log.i("Activebooking ", "active booking response_outside try" + response.body().toString());
                        try {
                            //Log.i("Activebooking ", "active booking response_outside IF" + response.body().toString());
                            if (response.body() != null) {
                                Log.i("Activebooking ", "active booking response!=NULL" + response.body().toString());

                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE ) {


                                    preferences.setBookingDetails(response.body().getResponseData());

                                    preferences.setBookingId(response.body().getResponseData().booking_id);

                                    preferences.setHospitalLat(response.body().getResponseData().hospital_latitude);
                                    preferences.setHospitalLng(response.body().getResponseData().hospital_longitude);

                                    /*if(response.body().getResponseData().booking_status.
                                            equalsIgnoreCase("Way Back to Hospital")){
                                        preferences.setBookingCancellable(false);
                                        Log.d("ActiveBooking","Way back to hospital");
                                    }else{
                                        preferences.setBookingCancellable(true);
                                    }*/

                                    Utils.setAmbulanceStatus(response.body().getResponseData().booking_status);

                                    Intent intent = new Intent(getActivity(), TrackAmbulanceActivity.class);
                                    intent.putExtra("bookingDetails", response.body().getResponseData());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);

                                }else{

                                    Log.d("booking","status_message"+response.body().getStatusMessage());
                                    //Utils.showToast(getActivity(),response.body().getStatusMessage());

                                }

                            } else {
                                //Toast.makeText(getActivity(), response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();
//                                Log.d("Activebooking","Activebooking_Error"+response.body().getStatusMessage());

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<BookingStatus> call, Throwable t) {
                        Utils.hideProgressDialog();
                        Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));

                    }
                });


            } else {
                Log.d("Activebooking","Activebooking_No internet");
                Utils.showToast(getActivity(), getResources().getString(R.string.network_error));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   /*private void getStatusList(){
       try{
           if(Utils.isNetworkAvailable(getActivity())){
               Call<StatusListResponse> getStatusList = apiInterface.getStatusList();
               getStatusList.enqueue(new Callback<StatusListResponse>() {
                   @Override
                   public void onResponse(Call<StatusListResponse> call, Response<StatusListResponse> response) {
                    if(response.body()!=null) {
                        if(Integer.parseInt(response.body().getStatusCode())==Constants.SUCCESS_CODE) {
                            *//*for(int i = 0 ; i < response.body().getResponseData().size() ; i++) {
                                Utils.addStatus(response.body().getResponseData().get(i).getShort_code(),
                                        response.body().getResponseData().get(i).getStatus());
                            }*//*
                        }
                    }
                   }

                   @Override
                   public void onFailure(Call<StatusListResponse> call, Throwable t) {
                    Utils.showToast(getActivity(),getActivity().getResources().getString(R.string.network_slow));
                   }
               });

           }else{
               Utils.showToast(getActivity(), getResources().getString(R.string.network_error));
           }

       }catch (Exception e){e.printStackTrace();}
   }*/




}
