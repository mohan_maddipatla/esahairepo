package com.esahai.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.preferences.AppPreferences;
import com.esahai.services.ShakerService;

/**
 * Created by ragamai on 21/10/16.
 */
public class SettingsActivity extends CommonActivity {
    AppPreferences preferences;


    CheckBox checkBox;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        preferences = new AppPreferences(SettingsActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText("Settings");
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        checkBox = (CheckBox) findViewById(R.id.chShake);
        checkBox.setChecked(preferences.isShake());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.setShakeState(isChecked);
                if (isChecked) {
                    startShakerService();

                } else {

                    stopShakerService();
                }
            }
        });

    }

    private void startShakerService() {
        Intent i = new Intent(this, ShakerService.class);
        startService(i);

    }

    private void stopShakerService() {
        Intent i = new Intent(this, ShakerService.class);
        stopService(i);

    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
                finish();
        }
        return super.onOptionsItemSelected(item);
    }*/
}
