package com.esahai.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.CountryCode;
import com.esahai.dataObjects.CountryList;
import com.esahai.dataObjects.GenerateOTPResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends CommonActivity implements AdapterView.OnItemSelectedListener {

    EditText etMobileNumber;
    Button btnSubmit;
    CheckBox checkbox_terms;
    Context mContext;
    TextView textView_terms;
    ApiInterface apiService;
    HashMap<String, Object> loginDetails;
    String customerMobile, deviceIMEI;
    private final int MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE = 1;
    private final int MY_READ_SMS_PERMISSION_REQUEST_CODE = 2;
    boolean isTermsChecked = false;

    boolean isSubmitNumberToServerInPermissionResults = false;
    private final int MY_CALL_PHONE_PERMISSION_REQUEST_CODE = 2;
    AppPreferences preferences;
    Spinner spinner_country_code;
    ArrayList<CountryCode> countryList;
    ArrayList<String> COUNTRY_CODE;
    String CountryCode;
    EditText etCountryCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        initViews();
        if (!checkReadSmsPermission()) {
            requestReadSmsPermission();
        }else{
            if (!checkPhoneStatePermission()) {
                requestReadPhonePermission();
            }
        }

        preferences = new AppPreferences(LoginActivity.this);
        preferences.setCountryCode("91");

        networkCallForCountryList();

    }

    private void networkCallForCountryList() {
        countryList = new ArrayList<CountryCode>();
        COUNTRY_CODE = new ArrayList<String>();
        if(Utils.isNetworkAvailable(LoginActivity.this)){
            Utils.showProgressDialog(LoginActivity.this);
            Call<CountryList> getCountryList = apiService.getCountryList();
            getCountryList.enqueue(new Callback<CountryList>() {
                @Override
                public void onResponse(Call<CountryList> call, Response<CountryList> response) {
                    Utils.hideProgressDialog();
                    try{
                        if (response.body() != null) {
                            Log.i("country response", response.body().toString());
                            Utils.hideProgressDialog();

                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {

                                countryList = response.body().getResponseData();

                                for(int i = 0 ; i <countryList.size() ; i++){
                                    COUNTRY_CODE.add(countryList.get(i).getCountry());
                                }
                                Log.i("countr", Arrays.asList(COUNTRY_CODE).toString());

                                preferences.setAllCountryCodes(countryList);

                                ArrayAdapter gender_adapter = new ArrayAdapter<String>(LoginActivity.this,
                                        android.R.layout.simple_dropdown_item_1line, COUNTRY_CODE);
                                spinner_country_code.setAdapter(gender_adapter);

                                for(int i = 0 ; i < COUNTRY_CODE.size() ; i++) {
                                    if (COUNTRY_CODE.get(i).equalsIgnoreCase("india")){
                                        spinner_country_code.setSelection(i);
                                    }
                                }


                            }
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CountryList> call, Throwable t) {
                    Utils.hideProgressDialog();
                }
            });
        }else{
            COUNTRY_CODE.add("India");
            etCountryCode.setText("+91");
            ArrayAdapter gender_adapter = new ArrayAdapter<String>(LoginActivity.this,
                    android.R.layout.simple_dropdown_item_1line, COUNTRY_CODE);
            spinner_country_code.setAdapter(gender_adapter);
            Utils.showToast(LoginActivity.this, getResources().getString(R.string.network_error));
        }
    }

    private void initViews() {

        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        checkbox_terms = (CheckBox) findViewById(R.id.checkbox_terms);
        textView_terms = (TextView) findViewById(R.id.textView_terms);
        spinner_country_code = (Spinner) findViewById(R.id.spinner_country_code);
        etCountryCode = (EditText) findViewById(R.id.etCountryCode);
//        btnSubmit.setEnabled(false);

        textView_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent terms = new Intent(LoginActivity.this, TermsAndConditionsActivity.class);
                startActivity(terms);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isTermsChecked == true) {
                    sendNumberToServer();
                } else {
                    Utils.showToast(LoginActivity.this, getResources().getString(R.string.check_terms));
                }
            }
        });

        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etMobileNumber.getText().toString().length() == 15) {
                    Utils.closeKeyBoard(mContext, etMobileNumber);
//                    btnSubmit.setEnabled(true);
                } /*else {
                    btnSubmit.setEnabled(false);
                }*/
            }
        });
        etMobileNumber
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                               @Override
                                               public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                                                   boolean handled = false;
                                                   if (actionId == EditorInfo.IME_ACTION_DONE) {
                                                       Utils.closeKeyBoard(mContext, etMobileNumber);
                    /* handle action here */
                                                       if (isTermsChecked == true) {
                                                           sendNumberToServer();
                                                       } else {
                                                           Utils.showToast(LoginActivity.this, getResources().getString(R.string.check_terms));
                                                       }
                                                       handled = true;
                                                   }
                                                   return handled;
                                               }
                                           }
                );

        checkbox_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    isTermsChecked = true;
                } else {
                    isTermsChecked = false;
                }
            }
        });

        spinner_country_code.setOnItemSelectedListener(this);

        if (!Utils.isGPSOn(LoginActivity.this)) {
            Utils.turnOnGPS(LoginActivity.this);
        }

    }


    private void sendNumberToServer() {
        if (etMobileNumber.getText().toString().length() < 5) {
            etMobileNumber.setError("Please check the Mobile Number");
            return;
        } else {
            if (!checkPhoneStatePermission()) {
                isSubmitNumberToServerInPermissionResults = true;
                requestReadPhonePermission();
                return;
            } else {
                customerMobile = Utils.encrypt(CountryCode+""+etMobileNumber.getText().toString());
                deviceIMEI = Utils.getIMEINumber(LoginActivity.this);
                try {
                    callNetworkCall();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public boolean checkPhoneStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkReadSmsPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        return (result == PackageManager.PERMISSION_GRANTED);
    }


    public void requestReadPhonePermission() {


        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE);

    }

    private void requestReadSmsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, MY_READ_SMS_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE) {
            // Enable the My Location button if the permission has been granted.

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (isSubmitNumberToServerInPermissionResults) {
                    sendNumberToServer();
                }
            }

        }else if(requestCode == MY_READ_SMS_PERMISSION_REQUEST_CODE){
            if (!checkPhoneStatePermission()) {
                requestReadPhonePermission();
            }
        }else if (requestCode == MY_CALL_PHONE_PERMISSION_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
            }else{
                finish();
            }

        }
    }

    private void callNetworkCall() {
        preferences.setPhoneNumberWithoutCc(etMobileNumber.getText().toString());
        loginDetails = new HashMap<>();
        loginDetails.put("customerMobile", customerMobile);
        loginDetails.put("deviceIMEI", deviceIMEI);

        Log.i("customerMobile------->", customerMobile);
        Log.i("deviceIMEI------->", deviceIMEI);

        if (Utils.isNetworkAvailable(LoginActivity.this)) {
            Call<GenerateOTPResponse> loginCall = apiService.getCustomerOTP(loginDetails);
            Utils.showProgressDialog(LoginActivity.this);
            loginCall.enqueue(new Callback<GenerateOTPResponse>() {
                @Override
                public void onResponse(Call<GenerateOTPResponse> call, Response<GenerateOTPResponse> response) {

                    try {
                        if (response.body() != null) {
                            Log.i("login response------->", response.body().toString());
                            Utils.hideProgressDialog();
                            GenerateOTPResponse generateOTP = response.body();

                            if (Integer.parseInt(generateOTP.getStatusCode()) == Constants.SUCCESS_CODE) {
//                    showAlert("success", generateOTP.getStatusMessage());
//                                showNotification("" + generateOTP.getOtp());
//                                EsahaiApplication.getApplication().trackEvent("Login", "OTP "+generateOTP.getOtp(), true);
                                EsahaiApplication.getApplication().trackEvent("Login", "Generate OTP", "" + generateOTP.getOtp());

                                Log.d("OTP","OTP is :: "+generateOTP.getOtp());
                                Intent intent = new Intent(getBaseContext(), OTPVerificationActivity.class);
                                intent.putExtra("mobileNumber", CountryCode+""+etMobileNumber.getText().toString());
                                intent.putExtra("otp", "" + generateOTP.getOtp());
                                startActivity(intent);
                                finish();
                            } else {
                                Utils.showAlert(LoginActivity.this, generateOTP.getStatusMessage());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<GenerateOTPResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(LoginActivity.this, getResources().getString(R.string.network_error));

                    Log.i("error", t.toString());
                }
            });
        } else {
//            Utils.showToast(LoginActivity.this, getResources().getString(R.string.network_error));
            showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
        }


    }

    private void showAlertDialog(String message, final boolean isNetwork) {
        try {
            String yes, no;
            if (isNetwork) {
                yes = "Yes";
                no = "No";
                Log.d("Dialog", "Dialog_IF set to : " + isNetwork);
            } else {
                yes = "Connect to Customer service";
                no = "Try other options ";
                Log.d("Dialog", "Dialog_ELSE set to : " + isNetwork);
            }
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(LoginActivity.this);
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                                Log.d("CustomerCare", "Number IS::::" + "04047911911");
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "04047911911"));
                                startActivity(intent);
                                dialog.cancel();
                                finish();
                                System.exit(0);
                                /*System.exit(0);
                                preferences.setFirstRun(true);*/
                            } else {
                                requestPermissionForCall();
                            }
                        }


                    });
            if (isNetwork) {
                builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
            }
            android.support.v7.app.AlertDialog alert = builder.create();
            alert.show();
        }catch (Exception e){e.printStackTrace();}
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(LoginActivity.this,
                new String[]{Manifest.permission.CALL_PHONE},
                MY_CALL_PHONE_PERMISSION_REQUEST_CODE);


    }



    public void showAlert(final String status, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (status.equalsIgnoreCase("success")) {
                            Intent intent = new Intent(getBaseContext(), OTPVerificationActivity.class);
                            intent.putExtra("mobileNumber", etMobileNumber.getText().toString());
                            startActivity(intent);
                            finish();
                        }

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(Utils.isNetworkAvailable(LoginActivity.this)) {
            CountryCode = countryList.get(position).getCode();
            etCountryCode.setText(CountryCode);
            CountryCode = CountryCode.replace("+", "");
//        preferences.setCountryCode(country_code);
            Log.i("country code", CountryCode);
        }else{
            CountryCode = "91";
            etCountryCode.setText("+91");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

   /* public void showNotification(String OTP){

        // This pending intent will open after notification click
        PendingIntent pendingIntent=PendingIntent.getActivity(getApplicationContext(), 0,
                new Intent(),0);

        NotificationManager notificationManager=
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder notification  = new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle("OTP from eSahai")
                .setContentText(OTP)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);


        notificationManager.notify(0, notification.build());

    }*/


}
