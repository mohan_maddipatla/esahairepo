package com.esahai.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.BookingResponse;
import com.esahai.dataObjects.GoogleAddress;
import com.esahai.dataObjects.HospitalDetails;
import com.esahai.dataObjects.HospitalList;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 19/1/17.
 */
public class MedicalTaxiActivity extends CommonActivity implements View.OnClickListener,LocationListener, RadioGroup.OnCheckedChangeListener {

    TextView textView_pickup,textView_drop_location,textView_drop_hospital;
    Button button_book_medical_taxi,button_book_schedule_medical_taxi;
    LocationManager locationManager;
    String selected_location = "",drop_location = "";
    public static final long MIN_TIME = 1000 * 60;
    public static final float MIN_DISTANCE = 100;
    String address;
    double latitude,longitude,drop_latitude,drop_longitude,
            hospital_latitude,hospital_longitude;
//    double pickup_latitude, pickup_longitude;

    ApiInterface apiInterface_google, apiInterface;
    String assistance;
    int is_self;
    EditText editText_mobile_number;
    RadioGroup radioGroup_destination;
    RadioButton radioButton_location,radioButton_hospital;
    AppPreferences preferences;

    LinearLayout layout_hospital;
    LinearLayout layout_location;

    int PICK_UP_LOCATION = 1001;
    int DROP_LOCAION = 1002;
    private int SEARCH_HOSPITAL = 101;
    String hospitalId, hospital;
    String group_type_id;
    RadioGroup radio_group_taxi_for,radio_group_assistance;
    RadioButton radio_button_myself,radio_button_someone,radio_button_no,radio_button_yes;
    boolean isHospitalsAvailable = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_taxi);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.medical_taxi));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utils.closeKeyBoard(MedicalTaxiActivity.this,editText_mobile_number);
                    Intent i = new Intent(MedicalTaxiActivity.this,DashboardActivity.class);
                    startActivity(i);
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });
        preferences = new AppPreferences(MedicalTaxiActivity.this);
        initialize();
        setAdaptersForSpinners();
        apiInterface_google = ApiClient.getGoogleClient().create(ApiInterface.class);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);


        try{
                getGroupTypeId();
        }catch (Exception e){e.printStackTrace();}


        editText_mobile_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (editText_mobile_number.getText().toString().length() == 10) {

                    editText_mobile_number.setCursorVisible(false);
                    InputMethodManager in = (InputMethodManager) MedicalTaxiActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(editText_mobile_number.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


                } else {

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        try {
            if(Utils.isGPSOn(MedicalTaxiActivity.this)) {
                getLocation();
            }else{
                Utils.turnOnGPS(MedicalTaxiActivity.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assistance = "No";
        is_self = 1;

    }

    private void getGroupTypeId() {
        if(Utils.getAmbulanceTypes() !=null && Utils.getAmbulanceTypes().size() > 0){
            for(int i = 0 ; i <Utils.getAmbulanceTypes().size() ; i++){
                if(Utils.getAmbulanceTypes().get(i).getType_name().equalsIgnoreCase("Any Ambulance")){
                    group_type_id = Utils.getAmbulanceTypes().get(i).getId();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (preferences != null) {
            if (preferences.isNoAmbulance() == true) {
                preferences.setNoAmbulance(false);
                showAlertDialog(getResources().getString(R.string.no_medical_taxi_contact_cc), true);
            }
        }

        if (!Utils.isGPSOn(this)) {
            Utils.turnOnGPS(this);
        } else {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                requestPermissionForLocation();
            } else {
                locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, MedicalTaxiActivity.this);
            }

        }

    }

    private void initialize() {
        textView_pickup = (TextView)findViewById(R.id.textView_pickup);
        textView_drop_location = (TextView)findViewById(R.id.textView_drop_location);
        textView_drop_hospital = (TextView)findViewById(R.id.textView_drop_hospital);
        button_book_medical_taxi = (Button)findViewById(R.id.button_book_medical_taxi);
        button_book_schedule_medical_taxi = (Button)findViewById(R.id.button_book_schedule_medical_taxi);
        editText_mobile_number = (EditText)findViewById(R.id.editText_mobile_number);

        radioGroup_destination = (RadioGroup)findViewById(R.id.radioGroup_destination);
        radioButton_location = (RadioButton)findViewById(R.id.radioButton_location);
        radioButton_hospital = (RadioButton)findViewById(R.id.radioButton_hospital);
        layout_hospital = (LinearLayout) findViewById(R.id.layout_hospital);
        layout_location = (LinearLayout) findViewById(R.id.location_layout);

        button_book_medical_taxi.setOnClickListener(MedicalTaxiActivity.this);
        button_book_schedule_medical_taxi.setOnClickListener(MedicalTaxiActivity.this);
        radioGroup_destination.setOnCheckedChangeListener(MedicalTaxiActivity.this);
        layout_location.setVisibility(View.VISIBLE);
        layout_hospital.setVisibility(View.GONE);

        radio_group_taxi_for = (RadioGroup)findViewById(R.id.radio_group_taxi_for);
        radio_button_myself = (RadioButton)findViewById(R.id.radio_button_myself);
        radio_button_someone = (RadioButton)findViewById(R.id.radio_button_someone);
        radio_group_taxi_for.setOnCheckedChangeListener(this);

        radio_group_assistance = (RadioGroup)findViewById(R.id.radio_group_assistance);
        radio_button_no = (RadioButton)findViewById(R.id.radio_button_no);
        radio_button_yes = (RadioButton)findViewById(R.id.radio_button_yes);
        radio_group_assistance.setOnCheckedChangeListener(this);


        textView_pickup.setOnClickListener(this);
        textView_drop_location.setOnClickListener(this);
        textView_drop_hospital.setOnClickListener(this);
    }

    private void setAdaptersForSpinners() {
        /*for_adapter = new ArrayAdapter<String>(MedicalTaxiActivity.this,
                android.R.layout.simple_dropdown_item_1line, FOR);
        assistance_adapter = new ArrayAdapter<String>(MedicalTaxiActivity.this,
                android.R.layout.simple_dropdown_item_1line, ASSISTANCE);*/


//        spinner_assistance.setAdapter(assistance_adapter);
//        spinner_for.setAdapter(for_adapter);

        spinnerSelectItem();
    }

    private void spinnerSelectItem() {
        /*spinner_assistance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                assistance = (String) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        /*spinner_for.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("For Myself")) {
                    is_self = 1;
                    editText_mobile_number.setText("");
                    editText_mobile_number.setVisibility(View.GONE);
                    editText_mobile_number.setText("");
                } else {
                    editText_mobile_number.setVisibility(View.VISIBLE);
                    is_self = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        /*spinner_hospital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    hospital = (String) parent.getItemAtPosition(position);
                    if (position == 0) {
                        hospitalId = "Any";
                        hospital_latitude = 0.0;
                        hospital_longitude = 0.0;
                    } else {
                        hospitalId = hospitals.get(position - 1).getGroup_id();
                        hospital_latitude = Double.parseDouble(hospitals.get(position - 1).getGroup_latitude());
                        hospital_longitude = Double.parseDouble(hospitals.get(position - 1).getGroup_longitude());
                        Log.i("hospital position", "" + (position - 1));
                        Log.i("hospital", hospital);
                    }
                }catch (Exception e){e.printStackTrace();}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
    }

    private void getLocation() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if ((ContextCompat.checkSelfPermission(MedicalTaxiActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(MedicalTaxiActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionForLocation();
        } else {
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, MedicalTaxiActivity.this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, MedicalTaxiActivity.this);

//            Location location = locationManager
//                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Location location;
                location = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location == null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                latitude = location.getLatitude();
                longitude = location.getLongitude();

//                networkCallForLatLng(latitude,longitude);


                if (!selected_location.equals("")) {
                    textView_pickup.setText(selected_location);
                    address = selected_location;
                } else {
                    Log.d("latlng","networkCallForLatLng1"+latitude+","+longitude);
                    address = networkCallForLatLng(latitude, longitude);
                    textView_pickup.setText(address);
                }
                networkCallForHospitals();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

//        networkCallForHospitals();

    }

    private void requestPermissionForLocation() {
        ActivityCompat.requestPermissions(
                MedicalTaxiActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1338);
    }

    private String networkCallForLatLng(final double latitude, final double longitude) {

        Log.d("latlng","address::: lat"+latitude+"long"+longitude);
        if (Utils.isNetworkAvailable(MedicalTaxiActivity.this)) {
            Call<GoogleAddress> getLatLng = apiInterface_google.getAddress(latitude + "," + longitude,
                    getResources().getString(R.string.geo_coding_key));

            getLatLng.enqueue(new Callback<GoogleAddress>() {
                @Override
                public void onResponse(Call<GoogleAddress> call, Response<GoogleAddress> response) {
                    try {
                        if (response.body() != null && response.body().getStatus().equalsIgnoreCase("OK")) {
                            Log.i("latLng response", response.body().toString());
                            address = response.body().getResults().get(0).getFormatted_address();
                        } else {
                            address = latitude + "," + longitude;
                        }
                        textView_pickup.setText(address);
                        Utils.hideProgressDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<GoogleAddress> call, Throwable t) {

                    Utils.hideProgressDialog();
                    Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        } else {
            address = latitude + "," + longitude;
            textView_pickup.setText(address);
            Utils.hideProgressDialog();
            Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.network_error));
        }
        return address;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.textView_pickup:
                textView_pickup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!Utils.isGPSOn(MedicalTaxiActivity.this)) {
                            Utils.turnOnGPS(MedicalTaxiActivity.this);
                        } else {
                            Intent i = new Intent(MedicalTaxiActivity.this, MapActivity.class);
                            startActivityForResult(i, PICK_UP_LOCATION);
                        }
                    }
                });
                break;
            case R.id.textView_drop_location:
                textView_drop_location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!Utils.isGPSOn(MedicalTaxiActivity.this)) {
                            Utils.turnOnGPS(MedicalTaxiActivity.this);
                        } else {
                            Intent i = new Intent(MedicalTaxiActivity.this, MapActivity.class);
                            startActivityForResult(i, DROP_LOCAION);
                        }
                    }
                });
                break;
            case R.id.button_book_medical_taxi:
                    validations();

                break;

            case R.id.button_book_schedule_medical_taxi:
                showAlertDialog(getResources().getString(R.string.contact_cc), true);
                break;

            case R.id.textView_drop_hospital:
                if (!Utils.isGPSOn(MedicalTaxiActivity.this)) {
                    Utils.turnOnGPS(MedicalTaxiActivity.this);
                } else {
                    Intent i = new Intent(MedicalTaxiActivity.this, NearestHospitalsActivity.class);
                    i.putExtra("pickUpLatitude",latitude);
                    i.putExtra("pickUpLongitude",longitude);
                    i.putExtra("emergencyType",preferences.getMedicalTaxiId());
                    startActivityForResult(i, SEARCH_HOSPITAL);
                }
                break;
        }

    }

    private void validations() {
        try{
            if(Utils.isNetworkAvailable(MedicalTaxiActivity.this)) {

                if(radioButton_location.isChecked()){
                    if (is_self == 0) {
                        if (editText_mobile_number.getText().toString().length() == 0 ||
                                editText_mobile_number.getText().toString().length() < 10) {
                            Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.valid_mobile_number));
                        }
                        else{
                            if(!editText_mobile_number.getText().toString().
                                    equalsIgnoreCase("0000000000")) {
                                if(textView_drop_location.getText().toString().length() == 0 ||
                                        textView_drop_location.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_location))) {
                                    Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.select_location));
                                }
                                else if(textView_pickup.getText().toString().equals(textView_drop_location.getText().toString())){
                                    Utils.showToast(MedicalTaxiActivity.this,getResources().getString(R.string.same_locations) );
                                }else {

//                                    if (HOSPITAL_TYPE != null) {
                                    /*if (textView_drop_hospital.getText().toString().
                                            equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
                                        Utils.showToast(MedicalTaxiActivity.this,getResources().getString(R.string.select_hospital));
//                                            showAlertDialog(getResources().getString(R.string.no_medical_taxi_contact_cc), true);
                                        } else {*/
                                    hospitalId = "Any";
                                    hospital = "Any nearby hospital";
                                            bookMedicalTaxiApi();
//                                        }
                                    }
//                                }

                            }
                            else{
                                editText_mobile_number.requestFocus();
                                editText_mobile_number.setError(getResources().getString(R.string.valid_mobile_number));

                            }
                        }

                    }
                    else if(is_self == 1){
                        if(textView_drop_location.getText().toString().length() == 0 ||
                                textView_drop_location.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_location))) {
                            Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.select_location));
                        }
                        else if(textView_pickup.getText().toString().equals(textView_drop_location.getText().toString())){
                            Utils.showToast(MedicalTaxiActivity.this,getResources().getString(R.string.same_locations) );
                        }else {
//                            if (HOSPITAL_TYPE != null) {
                            /*if (textView_drop_hospital.getText().toString().
                                    equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
                                Utils.showToast(MedicalTaxiActivity.this,getResources().getString(R.string.select_hospital));
//                                    showAlertDialog(getResources().getString(R.string.no_medical_taxi_contact_cc), true);
                                } else {*/
                            hospitalId = "Any";
                            hospital = "Any nearby hospital";
                                    bookMedicalTaxiApi();
//                                }
                            }
//                        }
                    }

                }else if(radioButton_hospital.isChecked()){
                    if(isHospitalsAvailable){
                    if(is_self == 0){
                        if (editText_mobile_number.getText().toString().length() == 0 ||
                                editText_mobile_number.getText().toString().length() < 10) {
                            Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.valid_mobile_number));
                        }
                        else{
                            if(!editText_mobile_number.getText().toString().
                                    equalsIgnoreCase("0000000000")) {
//                                if (HOSPITAL_TYPE != null) {
//                                if (textView_drop_hospital.getText().toString().
//                                        equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
//                                    Utils.showToast(MedicalTaxiActivity.this,getResources().getString(R.string.select_hospital));
////                                        showAlertDialog(getResources().getString(R.string.no_hospitals), true);
//                                    }else{
                                if(textView_drop_hospital.getText().toString().
                                        equals(getResources().getString(R.string.any_near))){
                                    hospitalId = "Any";
                                    hospital = "Any nearby hospital";


                                }

                                        bookMedicalTaxiApi();
//                                    }
//                                }

                            }
                            else{
                                editText_mobile_number.requestFocus();
                                editText_mobile_number.setError(getResources().getString(R.string.valid_mobile_number));

                            }
                        }

                    }else if(is_self == 1){
//                        if (HOSPITAL_TYPE != null) {
//                        if (textView_drop_hospital.getText().toString().
//                                equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
//                            Utils.showToast(MedicalTaxiActivity.this,getResources().getString(R.string.select_hospital));
////                                showAlertDialog(getResources().getString(R.string.no_hospitals), true);
//                            }else{
                        if(textView_drop_hospital.getText().toString().
                                equals(getResources().getString(R.string.any_near))){
                            hospitalId = "Any";
                            hospital = "Any nearby hospital";


                        }

                                bookMedicalTaxiApi();
//                            }
                        }
                    }
                    else{
                        showAlertDialog(getResources().getString(R.string.no_hospitals),true);
                    }
                }

            }else{showAlertDialog(getResources().getString(R.string.network_error_contact_cc) , true);}


        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{

            if(resultCode == this.RESULT_OK){
                if(requestCode == PICK_UP_LOCATION){
//                    locationManager.removeUpdates(this);
                    selected_location = (String) data.getExtras().get("location");
                    latitude = data.getExtras().getDouble("latitude");
                    longitude = data.getExtras().getDouble("longitude");
                    Log.d("lat lang","pick up lat langi = "+latitude+", "+longitude);
//                    Utils.showProgressDialog(getActivity());
                    if (!selected_location.equals("")) {
                        textView_pickup.setText(selected_location);
//
                    }
                    else{
                        Log.i("entering","yes");
                        String address = networkCallForLatLng(latitude,longitude);
                        textView_pickup.setText(address);

                    }
                    networkCallForHospitals();
                }
                else if(requestCode == DROP_LOCAION){
                    drop_location = (String) data.getExtras().get("location");
                    drop_latitude = data.getExtras().getDouble("latitude");
                    drop_longitude = data.getExtras().getDouble("longitude");
                    Log.d("Medical_Taxi","___onActivityResult_drop_latitude::: "+drop_latitude+", drop_longitude::: "+drop_longitude);
//                    Utils.showProgressDialog(getActivity());
                    if (!drop_location.equals("")) {
                        textView_drop_location.setText(drop_location);
//
                    }
                    else{
                        Log.i("entering","yes");
//                        String address = networkCallForLatLng(latitude,longitude);
                        textView_drop_location.setText("");

                    }

                }
                else if(requestCode == SEARCH_HOSPITAL){
                    hospital_latitude = data.getExtras().getDouble("hospital_latitude");
                    hospital_longitude = data.getExtras().getDouble("hospital_longitude");
                    hospital = data.getExtras().getString("hospital");
                    hospitalId = data.getExtras().getString("hospital_id");
                    textView_drop_hospital.setText(hospital);
                    Log.i("hosp details",hospital+" , "+hospitalId+" , "+hospital_latitude+" , "+hospital_longitude);
                }
            }

        }catch (Exception e){e.printStackTrace();}
    }


    private void bookMedicalTaxiApi() {

        if (Utils.isNetworkAvailable(MedicalTaxiActivity.this)) {
            facebookEventForBooking();
            //button_submit.setEnabled(false);
//            final AppPreferences preference = new AppPreferences(getActivity());
            String token = preferences.getAccessToken();
            String encrypted_mobile_number = Utils.encrypt(preferences.getMobileNumber());
            String customerId = preferences.getCustomerId();
//            convertAddress(text_location.getText().toString());
//            networkCallForConvertAddress(text_location.getText().toString());


            Location location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);



            Log.d("Medical_Taxi", "Latitude::::"+drop_latitude +
                    " Longitude::::"+drop_longitude);

            String geoHash = Utils.getGeoHashCode(location);


//            Utils.srcLatLng(latitude,longitude);

            HashMap<String, Object> createBooking = new HashMap<>();
            HashMap<String, Object> bookingDetails = new HashMap<>();

            if (preferences.getCustomerName() != null && !preferences.getCustomerName().equalsIgnoreCase("")) {
                bookingDetails.put("customer_name", preferences.getCustomerName());
            } else {
                bookingDetails.put("customer_name", "No Name");
            }

            if (is_self == 0) {
                bookingDetails.put("patient_number", /*preferences.getCountryCode()+""+*/editText_mobile_number.getText().toString());
            } else {
                bookingDetails.put("patient_number", "");
            }
            bookingDetails.put("is_self", is_self);
            bookingDetails.put("customer_mobile", encrypted_mobile_number);
            bookingDetails.put("booked_by", "Customer");

            bookingDetails.put("emergency_type", "Medical Taxi");

            bookingDetails.put("emergency_longitude", longitude);
            bookingDetails.put("emergency_latitude", latitude);
            bookingDetails.put("emergency_geoHashCode", geoHash);
            if(radioButton_hospital.isChecked()) {
                bookingDetails.put("hospital", hospital);
            }else{
                bookingDetails.put("hospital", textView_drop_location.getText().toString());
            }
            bookingDetails.put("customer_id", customerId);


            bookingDetails.put("assistance", assistance);


            Log.d("booking_request"," latitude::"+latitude+" longitude::"+longitude);



            createBooking.put("params", bookingDetails);
            createBooking.put("customerApp", true);
            createBooking.put("address", textView_pickup.getText().toString());
            createBooking.put("gcmToken", FirebaseInstanceId.getInstance().getToken());

//             convertAddress(hospital);


//            if(hospital_ambulance == true) {
            Location hospitalLocation = new Location("");
            Location dest_location = new Location("");
            if(!radioButton_hospital.isChecked()){
                createBooking.put("group_id", "Any");
                /*hospitalLocation.setLatitude(0.0);
                hospitalLocation.setLongitude(0.0);
                createBooking.put("hospital_latitude", 0.0);
                createBooking.put("hospital_longitude", 0.0);*/

                bookingDetails.put("drop_latitude", drop_latitude);
                bookingDetails.put("drop_longitude", drop_longitude);
                dest_location.setLatitude(drop_latitude);
                dest_location.setLongitude(drop_longitude);
                String dropGeoHash = Utils.getGeoHashCode(dest_location);
                bookingDetails.put("drop_geoHashCode", dropGeoHash);
            }else {

                createBooking.put("group_id", hospitalId);
                hospitalLocation.setLatitude(hospital_latitude);
                hospitalLocation.setLongitude(hospital_longitude);
                createBooking.put("hospital_latitude", hospital_latitude);
                createBooking.put("hospital_longitude", hospital_longitude);

                /*bookingDetails.put("drop_latitude", 0.0);
                bookingDetails.put("drop_longitude", 0.0);
                dest_location.setLatitude(0.0);
                dest_location.setLongitude(0.0);
                String dropGeoHash = Utils.getGeoHashCode(dest_location);
                bookingDetails.put("drop_geoHashCode", dropGeoHash);*/

            }
            createBooking.put("emergency_type", preferences.getMedicalTaxiId());
            createBooking.put("group_type_id", "8");



            Log.d("booking","getUserAge_fromPref"
                    +preferences.getUser_Age());
            //if (is_self == 0) {
            createBooking.put("age", preferences.getUser_Age());
            //}

//            }




            createBooking.put("hospital_geoHashCode", Utils.getGeoHashCode(hospitalLocation));
            createBooking.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

            Log.i("booking details", String.valueOf(Arrays.asList(createBooking)));

            Log.d("eSahai", "USER_ID::::" + customerId);
            Log.d("eSahai", "TOKEN::::" + token);


            Call<BookingResponse> booking = apiInterface.createBooking(createBooking, customerId, token);
            Utils.showProgressDialog(MedicalTaxiActivity.this);
            // button_submit.setEnabled(true);

            booking.enqueue(new Callback<BookingResponse>() {
                @Override
                public void onResponse(Call<BookingResponse> call, Response<BookingResponse> response) {
                    Utils.hideProgressDialog();

                    String customerCareNumber;

                            try {
                                if (response.body() != null) {

                                    if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
//                                        facebookEventForBooking();
                                        Log.i("create booking response", response.body().toString());
                                        preferences.setBookingId(response.body().getResponseData().getBooking_id());


                                        Log.d("Response", "setCustomercareNumber Response::::" + response.body().getResponseData().getCommandCentreNumber());

                                        customerCareNumber = response.body().getResponseData().getCommandCentreNumber();

                                        Log.i("create booking response", response.body().toString());
                                        if (customerCareNumber != null && !customerCareNumber.equalsIgnoreCase("null")
                                                && !customerCareNumber.isEmpty()) {

                                            preferences.setCustomerCareNumber(response.body().getResponseData().getCommandCentreNumber());
                                            Log.d("Response", "setCustomercareNumber Response::::" + response.body().getResponseData().getCommandCentreNumber());

                                        }
                                        Log.i("create booking response", response.body().toString());
                                        EsahaiApplication.getApplication().trackEvent("Dashboard", "Booking-->Booking id",
                                                response.body().getResponseData().getBooking_id());

//                                        isbtnSubmitClicked = false;
                                        Intent ambulance = new Intent(MedicalTaxiActivity.this, SearchAmbulanceActivity.class);
                                        ambulance.putExtra("latitude", latitude);
                                        ambulance.putExtra("longitude", longitude);
                                        ambulance.putExtra("booking_id", preferences.getBookingId());

                                        preferences.setMedicalTaxi(true);

                                        startActivity(ambulance);

                                    } else {

//                                        Log.d("SUBMIT","Button submit clicked in " +
//                                                "else dialog previous booking---> "+isbtnSubmitClicked);
//                                        isbtnSubmitClicked = false;
                                        Utils.hideProgressDialog();
                                        showAlertDialog("" + response.body().getStatusMessage() +
                                                " Do you want to call: "
                                                + preferences.getCustomerCareNumber(), true);
                                        //Utils.showToast(getActivity(),response.body().getStatusMessage()
                                        // +preference.getCustomerCareNumber());

                                    }
                                } else {
//                                    isbtnSubmitClicked = false;
                                }
                            } catch (Exception e) {
//                                isbtnSubmitClicked = false;
                                Utils.hideProgressDialog();
                                e.printStackTrace();
                            }



                }

                @Override
                public void onFailure(Call<BookingResponse> call, Throwable t) {
//                    isbtnSubmitClicked = false;
                    Log.i("error", t.toString());
                    Utils.hideProgressDialog();
                    Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        } else {/*Utils.showToast(getActivity(),"Check your Internet Connection");*/
            /*isbtnSubmitClicked = false;
            showAlertDialog(getResources().getString(R.string.network_error_contact_cc) , true);*/
        }

    }

    private void showAlertDialog(String message, final boolean isNetwork) {
        String yes, no;
        if (isNetwork) {
            yes = getResources().getString(R.string.yes);
            no = getResources().getString(R.string.no);
            Log.d("Dialog","Dialog_IF set to : "+isNetwork);
        } else {
            yes = "Connect to Customer service";
            no = "Try other options ";
            Log.d("Dialog","Dialog_ELSE set to : "+isNetwork);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(MedicalTaxiActivity.this);
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (ActivityCompat.checkSelfPermission(MedicalTaxiActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            if (preferences.getCustomerCareNumber() != null) {

                                Log.d("CustomerCare", "Number IS::::" + preferences.getCustomerCareNumber());
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + preferences.getCustomerCareNumber()));
                                startActivity(intent);

                            } else {
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + 0));
                                startActivity(intent);
                            }
                            dialog.cancel();
                        } else {
                            requestPermissionForCall();
//                    Toast.makeText(BookingConfirmedActivity.this,"Permission denied",Toast.LENGTH_SHORT).show();
                        }
                    }


                });
        if(isNetwork) {
            builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        }
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(MedicalTaxiActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }

    private void facebookEventForBooking() {
        try {
            if(preferences.isProd()) {
                AppEventsLogger logger = AppEventsLogger.newLogger(this);
                logger.logEvent(AppEventsConstants.EVENT_NAME_PURCHASED);
            }
        }catch (Exception e){e.printStackTrace();}
//        finish();
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        try{
            if(group == radioGroup_destination) {
                switch (checkedId) {
                    case R.id.radioButton_location:
                        layout_location.setVisibility(View.VISIBLE);
                        layout_hospital.setVisibility(View.GONE);
//                        textView_drop_hospital.setText(getResources().getString(R.string.any_near));
                        if(isHospitalsAvailable){
                            textView_drop_hospital.setText(getResources().getString(R.string.any_near));
                            textView_drop_hospital.setClickable(true);
                        }else{
                            textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                            textView_drop_hospital.setClickable(false);
                        }
                        break;
                    case R.id.radioButton_hospital:
//                        networkCallForHospitals();
                        layout_location.setVisibility(View.GONE);
                        layout_hospital.setVisibility(View.VISIBLE);
                        textView_drop_location.setText(getResources().getString(R.string.select_location));
                        break;
                }
            }else if(group == radio_group_taxi_for){
                switch (checkedId){
                    case R.id.radio_button_myself:
                        editText_mobile_number.setVisibility(View.GONE);
                        is_self = 1;
//                        preferences.setIsSelf(true);
                        editText_mobile_number.setText("");
                        try{
                            Utils.closeKeyBoard(MedicalTaxiActivity.this,editText_mobile_number);
                        }catch (Exception e){e.printStackTrace();}
                        break;
                    case R.id.radio_button_someone:
                        editText_mobile_number.setVisibility(View.VISIBLE);
                        is_self = 0;
//                        preferences.setIsSelf(false);
                        break;
                }
            }else if(group == radio_group_assistance){
                switch (checkedId){
                    case R.id.radio_button_no:
                        assistance = "No";
                        Log.i("assiatnce",assistance);
                        break;
                    case R.id.radio_button_yes:
                        assistance = "Yes";
                        Log.i("assiatnce",assistance);
                        break;
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

    ArrayList<HospitalDetails> hospitals;
    private void networkCallForHospitals() {
        Utils.hideProgressDialog();
        HashMap<String, Object> locationDetails = new HashMap<>();
        Location location;
        location = new Location("");

        try {
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            Log.i("location", latitude + "," + longitude + "-->" + location.getLatitude() + "," + location.getLongitude());
        } catch (Exception e) {
            Utils.hideProgressDialog();
            e.printStackTrace();
        }

        locationDetails.put("emergencyGeoHash", Utils.getGeoHashCode(location));
        locationDetails.put("latitude", latitude);
        locationDetails.put("longitude", longitude);
        locationDetails.put("groupType", "2");
        locationDetails.put("customerApp", true);
        locationDetails.put("ambulanceApp", false);
        locationDetails.put("emergency_type", preferences.getMedicalTaxiId());
        locationDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));
        hospitals = new ArrayList<HospitalDetails>();
        Log.i("location details", Arrays.asList(locationDetails).toString());
        Utils.showProgressDialog(MedicalTaxiActivity.this);
        if (Utils.isNetworkAvailable(MedicalTaxiActivity.this)) {
            Call<HospitalList> getHospitalsList = apiInterface.getNearByHospitalsList(locationDetails, preferences.getCustomerId(), preferences.getAccessToken());
//            Utils.showProgressDialog(NearByHospitalsActivity.this);
            getHospitalsList.enqueue(new Callback<HospitalList>() {
                @Override
                public void onResponse(Call<HospitalList> call, Response<HospitalList> response) {
                    Utils.hideProgressDialog();
                    try {
                        if (response.body() != null) {
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                Log.i("hospitals list", response.body().toString());

                                hospitals = response.body().getResponseData();
                                if(hospitals!=null) {
                                    isHospitalsAvailable = true;
                                    textView_drop_hospital.setText(getResources().getString(R.string.any_near));
                                    textView_drop_hospital.setClickable(true);
                                }else{
                                    isHospitalsAvailable = false;
                                    textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                                    textView_drop_hospital.setClickable(false);
                                }

                            }
                            else{
                                isHospitalsAvailable = false;
                                textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                                textView_drop_hospital.setClickable(false);
                            }
                        }
                        else{
                            isHospitalsAvailable = false;
                            textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                            textView_drop_hospital.setClickable(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }

                }

                @Override
                public void onFailure(Call<HospitalList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.network_slow));

                }
            });
        } else {

            Utils.showToast(MedicalTaxiActivity.this, getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
        }

    }
}
