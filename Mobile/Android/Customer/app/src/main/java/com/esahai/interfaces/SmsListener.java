package com.esahai.interfaces;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.esahai.activities.DashboardActivity;
import com.esahai.customVIews.Shaker;
import com.esahai.preferences.AppPreferences;

import java.util.List;

/**
 * Created by ragamai on 21/10/16.
 */
public interface SmsListener {
    public void messageReceived(String messageText);
}
