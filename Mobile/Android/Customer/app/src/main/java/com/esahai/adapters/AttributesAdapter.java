package com.esahai.adapters;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.activities.UpdateProfileActivity;
import com.esahai.dataObjects.AmbulanceDetails;
import com.esahai.dataObjects.SystemAttributes;

import java.util.ArrayList;

/**
 * Created by ragamai on 4/10/16.
 */
public class AttributesAdapter extends RecyclerView.Adapter<AttributesAdapter.ViewHolder> {

    Context context;
    ArrayList<SystemAttributes> attributes;
    AlertDialog alertDialog;


    public AttributesAdapter(Context context, ArrayList<SystemAttributes> attributes, AlertDialog alertDialog) {
        this.context = context;
        this.attributes = attributes;
        this.alertDialog = alertDialog;

    }

    @Override
    public AttributesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_attributes, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AttributesAdapter.ViewHolder holder, final int position) {
        try {
            holder.textView_attribute.setText(attributes.get(position).getType_name());
            holder.button_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("add click","yes  "+position);
                    Log.d("profile","Inside_add"+position);
                    alertDialog.dismiss();
                    if(context instanceof UpdateProfileActivity){
                        Log.d("profile","BeforeEntering dynamicViews");
                        ((UpdateProfileActivity)context).addDynamicViews(attributes.get(position));
                    }
                }
            });
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public int getItemCount() {
        return attributes.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView textView_attribute;
        Button button_add;

        public ViewHolder(View view) {
            super(view);
            textView_attribute = (TextView)view.findViewById(R.id.textView_attribute);
            button_add = (Button)view.findViewById(R.id.button_add);
        }



    }
}
