package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 7/11/16.
 */
public class PatientResponse {

    /*
    statusCode": "300",
 	"responseData"
     */

    public String statusCode;
    public ArrayList<PatientInfo> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ArrayList<PatientInfo> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<PatientInfo> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "PatientResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
