package com.esahai.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.activities.CommonActivity;
import com.esahai.dataObjects.EmergencyDetails;
import com.esahai.dataObjects.FaqDetails;
import com.esahai.dataObjects.ProfileDetails;
import com.esahai.dataObjects.PromoDetails;
import com.esahai.preferences.AppPreferences;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by adheesh on 30/01/17.
 */

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> implements View.OnClickListener {

    Context context;
    ArrayList<PromoDetails> promoList;
    RecyclerView recycle_list_offers;
    AppPreferences preferences;
    String offerId;
    ImageView imageView_banner;

    public OffersAdapter(Context context, ArrayList<PromoDetails> promoList, RecyclerView recycle_list_faqs) {
        this.context = context;
        this.promoList = promoList;
        this.recycle_list_offers = recycle_list_faqs;
        preferences = new AppPreferences(context);
    }

    @Override
    public OffersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_list_item, parent, false);

        OffersAdapter.ViewHolder viewHolder = new OffersAdapter.ViewHolder(v);
        v.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(promoList != null){
            holder.textView_title.setText(promoList.get(position).getTitle());
            holder.textView_description.setText(promoList.get(position).getDescription());

            String startDate = promoList.get(position).start_date;
            String endDate = promoList.get(position).end_date;
            holder.textView_valid_date.setText(Utils.convertedToDate(startDate)+" - "+Utils.convertedToDate(endDate));
        }

    }


    @Override
    public int getItemCount() {
        return promoList.size();
    }

    @Override
    public void onClick(View v) {
        int itemPosition = recycle_list_offers.getChildLayoutPosition(v);
        PromoDetails promoDetails = promoList.get(itemPosition);
        offerId = promoDetails.getOfferid();
        showDialogForBanner(promoDetails.getTitle(),promoDetails.getDescription(),promoDetails.getBanner_path());
        try {
            if(preferences.isProd()) {
                AppEventsLogger logger = AppEventsLogger.newLogger(context);
                logger.logEvent("Promo Banner Clicked");
            }
        }catch (Exception e){e.printStackTrace();}
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView_title;
        TextView textView_description;
        TextView textView_valid_date;

        public ViewHolder(View view) {
            super(view);
            textView_title = (TextView)view.findViewById(R.id.textView_title);
            textView_description = (TextView)view.findViewById(R.id.textView_description);
            textView_valid_date = (TextView)view.findViewById(R.id.textView_valid_date);
        }
    }


    private void showDialogForBanner(String title,String description,final String bannerImage) {

        final Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.banner);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView textView_title = (TextView)dialog.findViewById(R.id.textView_title);
        TextView textView_description = (TextView) dialog.findViewById(R.id.textView_description);
        imageView_banner = (ImageView) dialog.findViewById(R.id.imageView_banner);
        ImageView close = (ImageView) dialog.findViewById(R.id.imageView_delete);

        textView_title.setText(title);
        textView_description.setText(description);

        if (!bannerImage.equals("")) {
            String s = "/";
            bannerImage.replaceAll(s, "");
//                    new LoadImageUrl(this,context).execute(bannerImage);

            Picasso
                    .with(context)
                    .load(bannerImage)
                    .placeholder(R.drawable.progress_rotation)
                    .into(imageView_banner);
//                    imageView_banner.setImageBitmap(Utils.getBitmapFromUrl(bannerImage));
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileDetails profileDetails =preferences.getProfileDetails();
                try {
            if(preferences.isProd()) {
                    AppEventsLogger logger = AppEventsLogger.newLogger(context);
                    Bundle bundle = new Bundle();
                    bundle.putString("offer id",offerId);
                    bundle.putString("gender",profileDetails.getGender());
                    bundle.putString("location", CommonActivity.latitude+" , "+CommonActivity.longitude);
                    logger.logEvent("Promo Banner viewed",bundle);
            }
                }catch (Exception e){e.printStackTrace();}
                dialog.dismiss();
            }
        });


        dialog.show();
    }



}
