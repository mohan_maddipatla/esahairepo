package com.esahai.Utils;

import com.esahai.dataObjects.BookingConfirmedDetails;

/**
 * Created by ragamai on 29/9/16.
 */
public class Constants {

    public static final byte locationPrecisionNumber = 12;
    public static final int SUCCESS_CODE = 300;
    public static final int FAILURE_CODE = 500;

    public static final int CANCELLED_CODE1 = 411;
    public static final int CANCELLED_CODE2 = 412;
    public static final int CANCELLED_CODE3 = 413;

    public static final int NO_DATA = 303;


    //Confirmed Booking Details
    public static BookingConfirmedDetails bookingConfirmedDetails = null;


}
