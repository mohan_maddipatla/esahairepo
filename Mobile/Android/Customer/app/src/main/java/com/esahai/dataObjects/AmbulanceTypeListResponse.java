package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 1/11/16.
 */
public class AmbulanceTypeListResponse {
    /*
    "statusCode": "300",
	"statusMessage": "ambulance types fetched successfully",
	"responseData"
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<AmbulanceType> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<AmbulanceType> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<AmbulanceType> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "AmbulanceTypeListResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
