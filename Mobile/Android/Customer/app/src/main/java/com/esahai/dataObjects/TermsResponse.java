package com.esahai.dataObjects;

/**
 * Created by ragamai on 14/11/16.
 */
public class TermsResponse {

    /*
    "id": "4",
		"terms_and_conditions": "<p>Welcome to ESAHAI</p>\n"
     */

    public String id;
    public String terms_and_conditions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTerms_and_conditions() {
        return terms_and_conditions;
    }

    public void setTerms_and_conditions(String terms_and_conditions) {
        this.terms_and_conditions = terms_and_conditions;
    }

    @Override
    public String toString() {
        return "TermsResponse{" +
                "id='" + id + '\'' +
                ", terms_and_conditions='" + terms_and_conditions + '\'' +
                '}';
    }
}
