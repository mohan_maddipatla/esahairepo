package com.esahai.dataObjects;

/**
 * Created by ragamai on 1/11/16.
 */
public class AmbulanceType {
    /*
    "id": "1",
		"type_name": "Any Ambulance",
		"created_date": "2016-11-01 05:35:37",
		"updated_date": null
     */

    public String id;
    public String type_name;
    public String created_date;
    public String updated_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    @Override
    public String toString() {
        return "AmbulanceType{" +
                "id='" + id + '\'' +
                ", type_name='" + type_name + '\'' +
                ", created_date='" + created_date + '\'' +
                ", updated_date='" + updated_date + '\'' +
                '}';
    }
}
