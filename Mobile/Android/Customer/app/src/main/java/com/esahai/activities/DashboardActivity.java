package com.esahai.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.adapters.NavExpandableListAdapter;
import com.esahai.customVIews.CustomTypefaceSpan;
import com.esahai.dataObjects.CustomerCareResponse;
import com.esahai.dataObjects.NavExpandedMenuModel;
import com.esahai.dataObjects.Profile;
import com.esahai.dataObjects.ProfileDetails;
import com.esahai.fragments.HomeScreenFragment;
import com.esahai.preferences.AppPreferences;
import com.esahai.services.ShakerService;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DashboardActivity extends CommonActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    NavExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    ArrayList<NavExpandedMenuModel> listDataHeader;
    HashMap<NavExpandedMenuModel, ArrayList<String>> listDataChild;
    // ArrayList<String> listDataHeader;
    //HashMap<String, ArrayList<String>> listDataChild;


    CircleImageView profile_image;
    TextView text_name, text_mobile_number;
    FragmentTransaction fragmentTransaction;
    AppPreferences preferences;
    ApiInterface apiService;
    NavigationView nav_view;
    LinearLayout layout_main;

    private int lastExpandedPosition = -1;
    String bannerImage, description, title;

    RadioGroup radioGroup_decision;
    RadioButton radioButton_enable;
    RadioButton radioButton_disable;

    RadioGroup radioGroup_language;
    RadioButton radioButton_english;
    RadioButton radioButton_telugu;

    protected MoEHelper helper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        preferences = new AppPreferences(DashboardActivity.this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        //Defining toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        // getSupportActionBar().setCustomView(R.layout.action_bar_home);'


        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("mobileNumber", preferences.getMobileNumber())
                ;
        MoEHelper.getInstance(this).trackEvent("UserDetails", builder.build());

        expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);
        // NavigationView navigationView = (NavigationView)    findViewById(R.id.nav_view);



       /* if (navigationView != null) {
            setupDrawerContent(navigationView);
        }*/

        //Defining drawer
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        //Defining navigationView
        nav_view = (NavigationView) findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.invalidate();
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

                Log.d("Drawer", "Drawer opened:::");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                Log.d("Drawer", "Drawer closed:::");
                expandableList.collapseGroup(lastExpandedPosition);
                expandableList.smoothScrollToPosition(0);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });


//        nav_view.getMenu().getItem(0).setChecked(true);
        nav_view.setItemIconTintList(null);
        nav_view.setNavigationItemSelectedListener(this);
        View header = nav_view.getHeaderView(0);
        profile_image = (CircleImageView) header.findViewById(R.id.profile_image);
        text_name = (TextView) header.findViewById(R.id.text_name);
        layout_main = (LinearLayout) header.findViewById(R.id.layout_main);
        text_mobile_number = (TextView) header.findViewById(R.id.text_mobile_number);
        //textView_version = (TextView)nav_view.findViewById(R.id.textView_version) ;

        //setFonts();
//        setFontsForHeader(layout_main);

        turnGPSOn();

        //Default dashboard fragment
        /*DashboardFragment dashboardFragment = new DashboardFragment();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.flContent,dashboardFragment);
        fragmentTransaction.commit();*/


        HomeScreenFragment homeScreenFragment = new HomeScreenFragment();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.flContent, homeScreenFragment);
        fragmentTransaction.commit();

        try {
            if (getIntent().hasExtra("cancelled")) {
                showAlert(DashboardActivity.this, getResources().getString(R.string.booking_cancelled));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            startShakerService();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (getIntent().hasExtra("blood_request")) {
                showAlert(DashboardActivity.this, getIntent().getExtras().getString("blood_request"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (getIntent().hasExtra("banner")) {
                bannerImage = getIntent().getExtras().getString("banner");
                if (getIntent().hasExtra("description")) {
                    description = getIntent().getExtras().getString("description");
                    if (getIntent().hasExtra("title")) {
                        title = getIntent().getExtras().getString("title");
                        showDialogForBanner();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        networkCallForProfile();
        networkCallForCustomerCareNumber();

        prepareListData();
        mMenuAdapter = new NavExpandableListAdapter(this, listDataHeader, listDataChild, expandableList);

        mMenuAdapter.notifyDataSetChanged();

        // setting list adapter
        expandableList.setAdapter(mMenuAdapter);


        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                final String selected = (String) mMenuAdapter.getChild(
                        groupPosition, childPosition);

                Log.d("Navigation", "selected item_CHILD:::" + selected);
                Log.d("Navigation", "selected item_CHILD:::" + getResources().getString(R.string.profile));


                Log.d("Navigation", "selected item_CHILD__groupPosition:::" + groupPosition);
                Log.d("Navigation", "selected item_CHILD__childPosition:::" + childPosition);

                // Switch case to open selected child element activity on child element selection.

                Intent intent;

                if (selected.equalsIgnoreCase(getResources().getString(R.string.profile))) {
                    Log.d("Navigation", "selected item_CHILD::: Profile");
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, UpdateProfileActivity.class);
                    startActivity(intent);
                    return true;
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.my_trips))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                    startActivity(intent);
                    return true;
                }
                /******/
                else if (selected.equalsIgnoreCase(getResources().getString(R.string.ambulance_nv))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, EMSActivity.class);
                    startActivity(intent);
                    return true;
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.medical_taxi_nv))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, MedicalTaxiActivity.class);
                    startActivity(intent);
                    return true;
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.blood))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, BloodRequestActivity.class);
                    startActivity(intent);
                    return true;
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.nearest_hospital_nv))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, NearByHospitalsActivity.class);
                    startActivity(intent);
                    return true;
                }

                /// ******* SETTINGS *********
                else if (selected.equalsIgnoreCase(getResources().getString(R.string.choose_language))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();

                    languageChangeAlertDialog(DashboardActivity.this);



                    // EsahaiApplication.getApplication().languageSwitcher.
                    //       showChangeLanguageDialog(DashboardActivity.this);

                    return true;
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.clear_cache))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    clearAppCache();
                    return true;

                }


                else if (selected.equalsIgnoreCase(getResources().getString(R.string.launch_app_shake))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    shakerServiceAlertDialog(DashboardActivity.this);

                    return true;

                }

               /* if(selected.equalsIgnoreCase(getResources().getString(R.string.permissions))){
                   //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                    startActivity(intent);
                    return true;
                }*/

                /*if(selected.equalsIgnoreCase(getResources().getString(R.string.settings_video))){
                   //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    /*intent = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                    startActivity(intent);
                    return true;
                }*/

                /// ********* SUPPORT ***********
                else if (selected.equalsIgnoreCase(getResources().getString(R.string.request_call_back))) {

                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    requestCallbackApi();
                    return true;

                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.call_customer_care))) {

                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    callCustomerCareAlertDialog(getResources().getString(R.string.connect_customer_care));
                    return true;

                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.email_nv))) {

                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    sendEmail();
                    return true;
                }

                /// ********** HELP *****************
                else if (selected.equalsIgnoreCase(getResources().getString(R.string.faq_nv))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, FaqActivity.class);
                    startActivity(intent);
                    return true;
                }


                /// ********* ABOUT ******************

                else if (selected.equalsIgnoreCase(getResources().getString(R.string.about_us))) {
                    Log.d("Navigation", "selected item_CHILD::: about us");
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, AboutUsActivity.class);
                    startActivity(intent);
                    return true;
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.privacy_policy))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, PrivacyPolicyActivity.class);
                    startActivity(intent);
                    return true;
                } else if (selected.equalsIgnoreCase(getResources().getString(R.string.terms_and_conditions))) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, TermsAndConditionsActivity.class);
                    startActivity(intent);
                    return true;
                }

               /* if(selected.equalsIgnoreCase(getResources().getString(R.string.help_improve))){
                  //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                   /* intent = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                    startActivity(intent);
                    return true;
                }*/

                 /*if(selected.equalsIgnoreCase(getResources().getString(R.string.version))){
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                    startActivity(intent);
                    return true;
                }*/

                /*if(selected.equalsIgnoreCase(getResources().getString(R.string.website))){
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    /*intent = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                    startActivity(intent);
                    return true;
                }

                if(selected.equalsIgnoreCase(getResources().getString(R.string.address_nv))){
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    /*intent = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                    startActivity(intent);
                    return true;
                }*/


                return false;
            }
        });


        expandableList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expandableList.collapseGroup(lastExpandedPosition);
                    lastExpandedPosition = -1;
                }

                lastExpandedPosition = groupPosition;


            }
        });


        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {


                Intent intent;
                if (groupPosition == 0) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    /*intent = new Intent(DashboardActivity.this, DashboardActivity.class);
                    startActivity(intent);*/
                    return true;
                } else if (groupPosition == 3) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, ContactsActivity.class);
                    startActivity(intent);
                    return true;
                } else if (groupPosition == 4) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, RateCardActivity.class);
                    startActivity(intent);
                    return true;
                }
                else if (groupPosition == 5) {
                    //Closing the navigation drawer on going to the next screen.
                    //Pr hiding it in case if the user comes back to home screen.
                    drawer.closeDrawers();
                    intent = new Intent(DashboardActivity.this, OffersActivity.class);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });
        /*try{
            if(preferences.getProfileImage()!=null && !preferences.getProfileImage().isEmpty()){
                String url = preferences.getProfileImage();
                String image_url = url.replace("\\","");
                Picasso
                        .with(DashboardActivity.this)
                        .load(image_url)
                        .into(profile_image);
            }

        }catch (Exception e){e.printStackTrace();}*/

    }

    private void showDialogForBanner() {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(DashboardActivity.this);

                dialog.setContentView(R.layout.banner);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                TextView textView_title = (TextView)dialog.findViewById(R.id.textView_title);
                TextView textView_description = (TextView) dialog.findViewById(R.id.textView_description);
                ImageView imageView_banner = (ImageView) dialog.findViewById(R.id.imageView_banner);
                ImageView close = (ImageView) dialog.findViewById(R.id.imageView_delete);

                textView_title.setText(title);

                textView_description.setText(description);

                if (!bannerImage.equals("")) {
                    String s = "/";
                    bannerImage.replaceAll(s, "");
                    Picasso
                            .with(DashboardActivity.this)
                            .load(bannerImage)
                            .into(imageView_banner);
//                    imageView_banner.setImageBitmap(Utils.getBitmapFromUrl(bannerImage));
                }

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });

    }

    public int GetPixelFromDips(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<NavExpandedMenuModel>();
        listDataChild = new HashMap<NavExpandedMenuModel, ArrayList<String>>();

        // Adding data header
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.home), R.drawable.home));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.my_account), R.drawable.profile));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.services), R.drawable.emergencies));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.emergency_contacts),
                R.drawable.emergency_contact));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.rate_card), R.drawable.rate_card));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.offers),
                R.drawable.offers));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.settings), R.drawable.settings));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.support), R.drawable.support));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.help), R.drawable.help));
        listDataHeader.add(new NavExpandedMenuModel(
                getResources().getString(R.string.about), R.drawable.about));

        // Adding child data
        ArrayList<String> myAccHeaderList = new ArrayList<>();
        myAccHeaderList.add(getResources().getString(R.string.profile));
        myAccHeaderList.add(getResources().getString(R.string.my_trips));


        ArrayList<String> servicesHeaderList = new ArrayList<>();
        servicesHeaderList.add(getResources().getString(R.string.ambulance_nv));
        servicesHeaderList.add(getResources().getString(R.string.medical_taxi_nv));
        servicesHeaderList.add(getResources().getString(R.string.blood));
        servicesHeaderList.add(getResources().getString(R.string.nearest_hospital_nv));

        ArrayList<String> settingsHeaderList = new ArrayList<>();
        settingsHeaderList.add(getResources().getString(R.string.choose_language));
        //settingsHeaderList.add(getResources().getString(R.string.permissions));
        // settingsHeaderList.add(getResources().getString(R.string.notification_handle));
        settingsHeaderList.add(getResources().getString(R.string.clear_cache));
        settingsHeaderList.add(getResources().getString(R.string.launch_app_shake));
        //settingsHeaderList.add(getResources().getString(R.string.settings_video));

        ArrayList<String> supportHeaderList = new ArrayList<>();
        supportHeaderList.add(getResources().getString(R.string.request_call_back));
        supportHeaderList.add(getResources().getString(R.string.call_customer_care));
        supportHeaderList.add(getResources().getString(R.string.email_nv));

        ArrayList<String> helpHeaderList = new ArrayList<>();
        helpHeaderList.add(getResources().getString(R.string.faq_nv));

        ArrayList<String> aboutHeaderList = new ArrayList<>();
        //aboutHeaderList.add(getResources().getString(R.string.version));
        //aboutHeaderList.add(getResources().getString(R.string.website));
        //aboutHeaderList.add(getResources().getString(R.string.address_nv));
        aboutHeaderList.add(getResources().getString(R.string.about_us));
        aboutHeaderList.add(getResources().getString(R.string.privacy_policy));
        //aboutHeaderList.add(getResources().getString(R.string.help_improve));
        aboutHeaderList.add(getResources().getString(R.string.terms_and_conditions));


        listDataChild.put(listDataHeader.get(0), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(1), myAccHeaderList);// Header, Child data
        listDataChild.put(listDataHeader.get(2), servicesHeaderList);
        listDataChild.put(listDataHeader.get(3), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(4), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(5), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(6), settingsHeaderList);
        listDataChild.put(listDataHeader.get(7), supportHeaderList);
        listDataChild.put(listDataHeader.get(8), helpHeaderList);
        listDataChild.put(listDataHeader.get(9), aboutHeaderList);

    }


    public void requestCallbackApi() {

        String customerCareNumber = preferences.getCustomerCareNumber();

        if(customerCareNumber.startsWith("+91")){
            //remove +9140
            customerCareNumber = customerCareNumber.substring(5);
        }

        //customerCareNumber = customerCareNumber.substring(3);
        Log.d("Dashboard", "CustomercareNumber:::" + customerCareNumber);
        if (Utils.isNetworkAvailable(DashboardActivity.this)) {
            String numbers = preferences.getPhoneNumberWithoutCc() + "," + customerCareNumber;


            Log.d("Dashboard", "numbers:::" + numbers);
            Utils.showProgressDialog(DashboardActivity.this);
            ApiInterface apiService =
                    ApiClient.getClientConnectCall().create(ApiInterface.class);
            final Call<Object> callDriver = apiService.connectCall(numbers);
            callDriver.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> serverResponse) {
                    Utils.hideProgressDialog();
                    try {
                        Object jsonElement = serverResponse.body();

                        Log.d("jsonElement", "jsonElement = " + jsonElement);

                        if (jsonElement == null) {
                            Toast.makeText(DashboardActivity.this, getResources().getString(R.string.request_call_back_success), Toast.LENGTH_SHORT).show();
                            // call success
                        } else {
                            Log.d("Request_callback", "Failure");
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable throwable) {

                    Utils.hideProgressDialog();
                    EsahaiApplication.getApplication().trackEvent("Dashboard", getString(R.string.network_error), "makeCallViaProxy", false);


                }
            });


        } else {

//            Utils.showToast(TrackAmbulanceActivity.this,getResources().getString(R.string.network_error));
        }
    }


    public void sendEmail() {

        String deviceModel = android.os.Build.MODEL;
        String deviceManufacturer = android.os.Build.MANUFACTURER;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();


        String toEmail[] = new String[]{getResources().getString((R.string.emaillink))};
        String subject = getResources().getString(R.string.subject);

        String messageBody = "______________________________\nThe data below will be used for complaint resolution. Please do not delete any information.\n\nMobile Number : "
                + preferences.getMobileNumber()
                + "\nHandset Make & Model, OS Version : "
                + deviceManufacturer
                + ", "
                + deviceModel
                + ", "
                + "Android "
                + android.os.Build.VERSION.RELEASE
                + "\nDate & Time : "
                + dateFormat.format(cal.getTime())
                + "\n___________________________________" + "\n\n";


        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, toEmail);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, messageBody);
        this.startActivity(Intent.createChooser(intent, ""));
    }

    private void callCustomerCareAlertDialog(String message) {
        String yes, no;

        yes = getResources().getString(R.string.yes);
        no = getResources().getString(R.string.no);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            if (preferences.getCustomerCareNumber() != null) {

                                Log.d("CustomerCare", "Number IS::::" + preferences.getCustomerCareNumber());
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + preferences.getCustomerCareNumber()));
                                startActivity(intent);

                            } else {
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + 0));
                                startActivity(intent);
                            }
                            dialog.cancel();
                        } else {
                            requestPermissionForCall();
//                    Toast.makeText(BookingConfirmedActivity.this,"Permission denied",Toast.LENGTH_SHORT).show();
                        }
                    }


                });

        builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }


    public void clearAppCache() {

        try {
            File dir = this.getCacheDir();
            deleteDir(dir);
            Toast.makeText(DashboardActivity.this, getResources().getString(R.string.cache_success_msg), Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
        }

    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }

            return dir.delete();

        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    private void networkCallForCustomerCareNumber() {
        if (Utils.isNetworkAvailable(DashboardActivity.this)) {


            HashMap<String, Object> request = new HashMap<>();
            HashMap<String, Object> params = new HashMap<>();
            params.put("fieldName", "centre_number");
            //request.put("tableName","app_settings");
            request.put("params", params);
            request.put("customerApp", true);
            request.put("AmbulanceApp", false);
            request.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

            Log.i("customer care request", Arrays.asList(request).toString());

            Call<CustomerCareResponse> getCustomerCareNumber = apiService.getCustomerCareNumber(request,
                    preferences.getCustomerId(), preferences.getAccessToken());

            getCustomerCareNumber.enqueue(new Callback<CustomerCareResponse>() {
                @Override
                public void onResponse(Call<CustomerCareResponse> call, Response<CustomerCareResponse> response) {
                    try {

                        if (response.body() != null) {
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                Log.i("customer care", "IN_Activity:::CustomerCareNumber" + response.body().toString());
                                //preferences.setCustomerCareNumber(response.body().getResult().get(0).getValue());
                                preferences.setCustomerCareNumber(response.body().getResponseData().toString());
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CustomerCareResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });
    }


    /*private void setFonts() {
        try{
            Menu m = nav_view.getMenu();
            for (int i=0;i<m.size();i++) {
                MenuItem mi = m.getItem(i);

                applyFontToMenuItem(mi);
            }
        }catch (Exception e){e.printStackTrace();}
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        try {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            /*if (nav_view != null) {
                nav_view.getMenu().getItem(0).setChecked(true);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (preferences == null) {
            preferences = new AppPreferences(DashboardActivity.this);

        } else {
            if (preferences.getProfileImage() != null && !preferences.getProfileImage().isEmpty()) {
                Picasso
                        .with(DashboardActivity.this)
                        .load(preferences.getProfileImage())
                        .into(profile_image);
            }

            if (preferences.getCustomerName() != null) {
                text_name.setText(preferences.getCustomerName());
            }
            if (preferences.getMobileNumber() != null) {
                text_mobile_number.setText("+"+preferences.getMobileNumber());
            }
        }

        networkCallForProfile();
        networkCallForCustomerCareNumber();

    }



    private void networkCallForProfile() {
        if (Utils.isNetworkAvailable(DashboardActivity.this)) {
            HashMap<String, Object> request = new HashMap<>();
            request.put("customer_id", preferences.getCustomerId());
            request.put("customerApp", true);
            request.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

            Log.i("customer profile", Arrays.asList(request).toString());


            Call<Profile> profileDetails = apiService.getProfileDetails(request, preferences.getCustomerId(), preferences.getAccessToken());
            profileDetails.enqueue(new Callback<Profile>() {
                @Override
                public void onResponse(Call<Profile> call, Response<Profile> response) {
                    try {
                        if (response.body() != null && Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                            Log.i("profile data", response.body().toString());
                            ProfileDetails profile = response.body().getResponseData();
                            if (preferences != null) {
                                preferences.setProfileDetails(profile);
                            }
//                            setData(profile);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Profile> call, Throwable t) {
                    //Utils.showToast(DashboardActivity.this, t.toString());
                    Utils.showToast(DashboardActivity.this, getResources().getString(R.string.network_slow));
                    Log.i("profile data failure", t.toString());
                }
            });


        } else {
            Utils.showToast(DashboardActivity.this, getResources().getString(R.string.network_error));
        }

    }

    private void setData(ProfileDetails profile) {
        try {
            if (profile != null) {
                if (profile.getCustomer_name() != null) {
                    text_name.setText(profile.getCustomer_name());
                    preferences.setCustomerName(profile.getCustomer_name());
                }
                if (profile.getCustomer_mobile_number() != null) {
                    text_mobile_number.setText(profile.getCustomer_mobile_number());
                }
                if (profile.getFile_url() != null && !profile.getFile_url().isEmpty()) {
                    String url = profile.getFile_url();
                    String image_url = url.replace("\\", "");
                    Picasso
                            .with(DashboardActivity.this)
                            .load(image_url)
                            .into(profile_image);
                } else {
                    if (preferences.getProfileImage() != null && !preferences.getProfileImage().isEmpty()) {
                        Picasso
                                .with(DashboardActivity.this)
                                .load(preferences.getProfileImage())
                                .into(profile_image);
                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static AlertDialog alert = null;

    public void showAlert(final Context context, final String message) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alert.dismiss();
                            }
                        });
                alert = builder.create();
                alert.show();
            }
        });
    }

    private void turnGPSOn() {
//        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        startActivity(this, new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    private void startActivity(DashboardActivity dashboardActivity, Intent intent) {
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
//            finish();
            finishAffinity();
            System.exit(0);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {

            case R.id.nav_emergencies:
                Intent emergencies = new Intent(DashboardActivity.this, EmergenciesActivity.class);
                startActivity(emergencies);
                break;

            case R.id.nav_contacts:
                Intent contacts = new Intent(DashboardActivity.this, ContactsActivity.class);
                startActivity(contacts);
                break;

            case R.id.nav_blood_request:
                Intent blood_request = new Intent(DashboardActivity.this, BloodRequestActivity.class);
                startActivity(blood_request);
                break;

            case R.id.nav_profile:
                Intent profile = new Intent(DashboardActivity.this, UpdateProfileActivity.class);
                startActivity(profile);
                break;

            case R.id.nav_settings:
                Intent settings = new Intent(DashboardActivity.this, SettingsActivity.class);
                startActivity(settings);
                break;

            case R.id.nav_rate_card:
                Intent rateCard = new Intent(DashboardActivity.this, RateCardActivity.class);
                startActivity(rateCard);
                break;


            default:
                break;
        }

        // Highlight the selected item has been done by NavigationView
        item.setChecked(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "gotham_condensed_bold.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void languageChangeAlertDialog(final Context ctx) {
        // custom dialog
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.language_alert_dialog);
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);

        radioGroup_language = (RadioGroup) dialog.findViewById(R.id.language_type);
        radioButton_english = (RadioButton) dialog.findViewById(R.id.english_btn);
        radioButton_telugu = (RadioButton) dialog.findViewById(R.id.telugu_btn);


        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        if(EsahaiApplication.getApplication().
                languageSwitcher.getCurrentLocale().toString().equalsIgnoreCase("en")){
            radioButton_english.setChecked(true);
        }
        else{
            radioButton_telugu.setChecked(true);

        }


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (radioButton_english.isChecked()) {

                    Locale firstLaunchLocale = new Locale("en");

                    EsahaiApplication.getApplication().languageSwitcher.
                            setLocale(firstLaunchLocale, DashboardActivity.this);

                }

                if (radioButton_telugu.isChecked()) {

                    Locale telugu = new Locale("te");

                    EsahaiApplication.getApplication().languageSwitcher.
                            setLocale(telugu, DashboardActivity.this);
                }
                dialog.dismiss();


            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        if (!dialog.isShowing())
            dialog.show();
    }




















    public void shakerServiceAlertDialog(final Context ctx) {
        // custom dialog
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.shaker_service_dialog);
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);

        radioGroup_decision = (RadioGroup) dialog.findViewById(R.id.decision_type);
        radioButton_enable = (RadioButton) dialog.findViewById(R.id.enable_btn);
        radioButton_disable = (RadioButton) dialog.findViewById(R.id.disable_btn);


        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        if(preferences.isShake()){
            radioButton_enable.setChecked(true);
        }
        else{
            radioButton_disable.setChecked(true);

        }


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (radioButton_enable.isChecked()) {

                    preferences.setShakeState(true);
                    startShakerService();


                }

                if (radioButton_disable.isChecked()) {

                    preferences.setShakeState(false);
                    stopShakerService();

                }
                dialog.dismiss();


            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        if (!dialog.isShowing())
            dialog.show();
    }

    private void startShakerService() {
        if (preferences.isShake()) {
            Intent i = new Intent(this, ShakerService.class);
            startService(i);
        }

    }

    private void stopShakerService() {
        if (!preferences.isShake()) {

            Intent i = new Intent(this, ShakerService.class);
            stopService(i);
        }

    }


}
