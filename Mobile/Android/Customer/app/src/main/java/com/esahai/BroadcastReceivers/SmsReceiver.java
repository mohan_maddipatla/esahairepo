package com.esahai.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public abstract class SmsReceiver extends BroadcastReceiver {

	private final String TAG = "OTPListener";

	@Override
	public void onReceive(Context context, Intent intent) {
		System.out.println(" in on receive sms");

		if (intent.getAction()
				.equals("android.provider.Telephony.SMS_RECEIVED")) {
			abortBroadcast();
			Bundle bundle = intent.getExtras(); // ---get the SMS message
												// passed in---
			SmsMessage[] msgs = null;
			String msg_from, msgBody;
			if (bundle != null) {
				System.out.println("sms bundle not null");
				// ---retrieve the SMS message received---
				try {
					Object[] pdus = (Object[]) bundle.get("pdus");
					msgs = new SmsMessage[pdus.length];

					for (int i = 0; i < msgs.length; i++) {
						msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
						msg_from = msgs[i].getOriginatingAddress();
						msgBody = msgs[i].getMessageBody();

						Log.i(TAG, "message |" + msg_from + "| dsf: " + msgBody
								+ "  equals "
								+ msg_from.trim().contains("eSahai"));

						if (msg_from.trim().contains("eSahai")) {

							String bodyString = msgBody.trim();
							String otp;
							if (bodyString == null || bodyString.length() < 4) {
								otp = bodyString;
							} else {
								otp = bodyString.trim();
										
							}
							System.out.println("otp:::" + otp);
							onSMSArrived(otp);
							// if (listener != null) {
							// System.out.println("listner not null");
							// listener.onSmsReceived(otp);
							// }else{
							// onSMSArrived(otp);
							// }
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	protected abstract void onSMSArrived(String otp);
}
