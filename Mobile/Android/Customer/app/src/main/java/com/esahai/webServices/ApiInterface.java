package com.esahai.webServices;

import com.esahai.dataObjects.AmbulanceTypeListResponse;
import com.esahai.dataObjects.BookingResponse;
import com.esahai.dataObjects.BookingStatus;
import com.esahai.dataObjects.ComeToKnowBy;
import com.esahai.dataObjects.ContactsList;
import com.esahai.dataObjects.CountryList;
import com.esahai.dataObjects.CustomerCareResponse;
import com.esahai.dataObjects.EmergenciesList;
import com.esahai.dataObjects.EmergencyTypeList;
import com.esahai.dataObjects.FaqsList;
import com.esahai.dataObjects.GenerateOTPResponse;
import com.esahai.dataObjects.GoogleAddress;
import com.esahai.dataObjects.GoogleLatLng;
import com.esahai.dataObjects.HospitalList;
import com.esahai.dataObjects.NearByAmbulances;
import com.esahai.dataObjects.PatientResponse;
import com.esahai.dataObjects.PrivacyPolicyResponse;
import com.esahai.dataObjects.Profile;
import com.esahai.dataObjects.ProfilePicResponse;
import com.esahai.dataObjects.PromosList;
import com.esahai.dataObjects.RateCardResponse;
import com.esahai.dataObjects.StatusListResponse;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.dataObjects.TermsAndConditionsResponse;
import com.esahai.dataObjects.VerifyOTPResponse;
import com.google.gson.JsonElement;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by Ragamai on 16/9/16.
 */

public interface ApiInterface {

    //Login or customer generate OTP
    @POST("db/app_user/customerGenerateOTP")
    Call<GenerateOTPResponse>
            getCustomerOTP(@Body HashMap<String, Object> loginDetails);

    //Verify OTP
    @POST("db/app_user/verifyCustomerOTP")
    Call<VerifyOTPResponse>
            verifyOTP(@Body HashMap<String, Object> verifyOTP);

    //create booking
    @POST("db/createBooking")
    Call<BookingResponse>
            createBooking(@Body HashMap<String, Object> bookingDetails,
                          @Header("userId") String userId,
                          @Header("token") String token);

   /* //search nearby ambulances list
    @POST("db/getNearByAmbulanceList")
    Call<NearByAmbulances>
            searchNearByAmbulances(@Body HashMap<String, Object> searchRequest,
                                   @Header("userId") String userId,
                                   @Header("token") String token);
*/
    //update profile
    @POST("db/updateCustomerProfile")
    Call<StatusResponse>
            updateProfile(@Body HashMap<String, Object> profileDetails,
                          @Header("userId") String userId,
                          @Header("token") String token);

    //get emergency type list
    @POST("db/getEmergencyTypeList")
    Call<EmergencyTypeList> getEmergencyTypes();

    //get nearby hospitals list
    @POST("db/getNearByHospitalList")
    Call<HospitalList>
            getNearByHospitalsList(@Body HashMap<String, Object> locationDetails,
                                   @Header("userId") String userId,
                                   @Header("token") String token);

    //cancel booking
    @POST("db/cancelBooking")
    Call<StatusResponse>
            cancelBooking(@Body HashMap<String, Object> locationDetails,
                          @Header("userId") String userId,
                          @Header("token") String token);

    //emergencies list
    @POST("db/getEmergencies")
    Call<EmergenciesList>
            getEmergenciesList(@Body HashMap<String, Object> emergencyDetails,
                               @Header("userId") String userId,
                               @Header("token") String token);

    //trip details
    @POST("db/getTripDetails")
    Call<ResponseBody>
            getTripDetails(@Body HashMap<String, Object> emergencyDetails,
                           @Header("userId") String userId,
                           @Header("token") String token);

    //get profile details
    @POST("db/getCustomerprofile")
    Call<Profile>
            getProfileDetails(@Body HashMap<String, Object> emergencyDetails,
                              @Header("userId") String userId,
                              @Header("token") String token);

    //add emergency contacts
    @POST("db/addEmergencyContactForCustomer")
    Call<StatusResponse>
            addEmergencyContacts(@Body HashMap<String, Object> emergencyDetails,
                                    @Header("userId") String userId,
                                    @Header("token") String token);

    //get emergency contacts
    @POST("db/getEmergencyContactsForCustomer")
    Call<ContactsList>
            getEmergencyContacts(@Body HashMap<String, Object> details,
                                 @Header("userId") String userId,
                                 @Header("token") String token);

    //update emergency contact
    @POST("db/updateEmergencyContactForCustomer")
    Call<StatusResponse>
            updateEmergencyContact(@Body HashMap<String, Object> searchRequest,
                                   @Header("userId") String userId,
                                   @Header("token") String token);

    //delete emergency contact
    @POST("db/deleteEmergencyContactForCustomer")
    Call<StatusResponse>
            deleteEmergencyContact(@Body HashMap<String, Object> searchRequest,
                                   @Header("userId") String userId,
                                   @Header("token") String token);


    /*//search nearby ambulances list based on hospitals
    @POST("db/getNearByAmbulanceListByHospitalAndEmergeType")
    Call<NearByAmbulances>
            searchNearByAmbulancesByHospitals(@Body HashMap<String, Object> searchRequest,
                                              @Header("userId") String userId,
                                              @Header("token") String token);*/

    //get Ambulance lit
    @POST("db/getNearByAmbulanceListSpecificType")
    Call<NearByAmbulances>
        getAmbulanceList(@Body HashMap<String, Object> searchRequest,
                                      @Header("userId") String userId,
                                      @Header("token") String token);

    //send blood request
    @POST("db/addBloodRequest")
    Call<StatusResponse>
            sendBloodRequest(@Body HashMap<String, Object> searchRequest,
                             @Header("userId") String userId,
                             @Header("token") String token);


    //get Emergency types
    @POST("db/getAmbulanceTypes")
    Call<AmbulanceTypeListResponse> getAmbulanceTypes(@Body HashMap<String, Object> customerRequest,
                                                      @Header("userId") String userId,
                                                      @Header("token") String token);

    //get rate card
    @POST("db/getRateCard")
    Call<RateCardResponse> getRateCard(@Body HashMap<String, Object> customerRequest,
                                       @Header("userId") String userId,
                                       @Header("token") String token);

    // update profile photo
    @Multipart
    @POST("db/updateProfile")
    Call<ProfilePicResponse> uploadProfilePic(@PartMap HashMap<String, Object> customerRequest,
                                              @Part MultipartBody.Part file,
                                              @Header("userId") String userId,
                                              @Header("token") String token);

    //send feedback
    @POST("db/addfeedback")
    Call<ResponseBody> sendFeedback(@Body HashMap<String, Object> customerRequest,
                                    @Header("userId") String userId,
                                    @Header("token") String token);

    //update patient info
    @POST("db/addPatientInfo")
    Call<StatusResponse> updatePatientInfo(@Body HashMap<String, Object> customerRequest,
                                        @Header("userId") String userId,
                                        @Header("token") String token);

    //get patient info
    @POST("db/getPatientInfo")
    Call<PatientResponse> getPatientInfo(@Body HashMap<String, Object> customerRequest,
                                         @Header("userId") String userId,
                                         @Header("token") String token);


    //faqs list
    @POST("db/faqList")
    Call<FaqsList>
    getFaqsList(@Body HashMap<String, Object> faqDetails,
                       @Header("userId") String userId,
                       @Header("token") String token);

    //get terms and conditions
    @GET("db/getTermsAndConditions")
    Call<TermsAndConditionsResponse> getTermsAndConditions();


    //get privacy policy
    @GET("db/getPrivacyPolicy")
    Call<PrivacyPolicyResponse> getPrivacyPolicy();

    //get come to know by
    @GET("db/getComeToKnowBy")
    Call<ComeToKnowBy> getComeToKnow();

    //get app version
    @GET("db/getAppVersion")
    Call<JsonElement> getAppVersion( @Query("appType") String appType);

    //get customer care number
    @POST("db/getCommandCentreNUmber")
    Call<CustomerCareResponse> getCustomerCareNumber(@Body HashMap<String, Object> customerRequest,
                                                     @Header("userId") String userId,
                                                     @Header("token") String token);


    @POST("db/checkMyBookingStatus")
    Call<BookingStatus>getBookingStatus(@Body HashMap<String, Object> customerRequest,
                                        @Header("userId") String userId,
                                        @Header("token") String token);

    @POST("db/getActiveBooking")
    Call<BookingStatus>getActiveBooking(@Body HashMap<String, Object> customerRequest,
                                        @Header("userId") String userId,
                                        @Header("token") String token);


    //get location address
    @GET("api/geocode/json")
    Call<GoogleAddress>
            getAddress(@Query("latlng") String latlng, @Query("key") String key);

    //get location latLng
    @GET("api/geocode/json")
    Call<GoogleLatLng>
            getLatLng(@Query("address") String address,@Query("key") String key);

    //call
    @GET("user_conference.php")
    Call<Object> connectCall( @Query("conf_no_list") String mobileNumbers);

    //get country list
    @GET("db/getCountryList")
    Call<CountryList> getCountryList();

    //get promos list
    @POST("db/getpromos")
    Call<PromosList> getPromosList(@Body HashMap<String, Object> customerRequest,
                                   @Header("userId") String userId,
                                   @Header("token") String token);


}
