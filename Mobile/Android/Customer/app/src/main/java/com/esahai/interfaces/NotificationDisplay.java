package com.esahai.interfaces;

/**
 * Created by ragamai on 6/3/17.
 */
public interface NotificationDisplay {

    public void displayNotification(String image, String title, String description);
}
