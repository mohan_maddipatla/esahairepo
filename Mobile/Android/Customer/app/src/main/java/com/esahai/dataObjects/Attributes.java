package com.esahai.dataObjects;

/**
 * Created by ragamai on 25/10/16.
 */
public class Attributes {

    /*
    "id": "41",
 			"attribute_name": "Blood Group",
 			"attribute_details": "B+",
 			"customer_id": "16670705",
 			"customer_e_type_id": "3"
     */

    public String id;
    public String attribute_name;
    public String attribute_details;
    public String customer_id;
    public String customer_e_type_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAttribute_name() {
        return attribute_name;
    }

    public void setAttribute_name(String attribute_name) {
        this.attribute_name = attribute_name;
    }

    public String getAttribute_details() {
        return attribute_details;
    }

    public void setAttribute_details(String attribute_details) {
        this.attribute_details = attribute_details;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_e_type_id() {
        return customer_e_type_id;
    }

    public void setCustomer_e_type_id(String customer_e_type_id) {
        this.customer_e_type_id = customer_e_type_id;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "id='" + id + '\'' +
                ", attribute_name='" + attribute_name + '\'' +
                ", attribute_details='" + attribute_details + '\'' +
                ", customer_id='" + customer_id + '\'' +
                ", customer_e_type_id='" + customer_e_type_id + '\'' +
                '}';
    }
}
