package com.esahai.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.preferences.AppPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AboutUsActivity extends CommonActivity {


    TextView TvAppVersion;
    TextView TvPhInfo;
    TextView TvEmailInfo;
    TextView TvWebsiteInfo;
    ImageView ImgFbBtn;
    ImageView ImgInstaBtn;
    ImageView ImgTwitterBtn;


    AppPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);


        //Defining toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.about_us));

        TvAppVersion = (TextView) findViewById(R.id.app_version);
        TvPhInfo = (TextView) findViewById(R.id.ph_info);
        TvEmailInfo = (TextView) findViewById(R.id.email_info);
        TvWebsiteInfo = (TextView) findViewById(R.id.website_info);
        ImgFbBtn = (ImageView) findViewById(R.id.fb_img);
        ImgInstaBtn= (ImageView) findViewById(R.id.insta_img);
        ImgTwitterBtn = (ImageView) findViewById(R.id.twitter_img);

        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        preferences = new AppPreferences(AboutUsActivity.this);

        try{
            TvAppVersion.setText("V - "+ Utils.getAppVersionName());

            TvPhInfo.setText("Ph: "+
                    getResources().getString(R.string.phone_num));
            TvEmailInfo.setText(getResources().getString(R.string.email)+": " +
                    getResources().getString(R.string.emaillink));
            TvWebsiteInfo.setText(getResources().getString(R.string.website)+": "+
                    getResources().getString(R.string.website_link));

        }catch (Exception e){e.printStackTrace();}

        ImgFbBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse(getResources().getString(R.string.fb_link)); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

        ImgInstaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse(getResources().getString(R.string.insta_link)); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

        ImgTwitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse(getResources().getString(R.string.twitter_link)); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

        TvPhInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(AboutUsActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +
                            getResources().getString(R.string.phone_num)));
                    startActivity(intent);
                }
                else{
                    requestPermissionForCall();
                }

            }
        });

        TvEmailInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String deviceModel = android.os.Build.MODEL;
                String deviceManufacturer = android.os.Build.MANUFACTURER;

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Calendar cal = Calendar.getInstance();


                String toEmail[] = new String[] { getResources().getString((R.string.emaillink))};
                String subject = getResources().getString(R.string.subject);

                String messageBody = "______________________________\nThe data below will be used for complaint resolution. Please do not delete any information.\n\nMobile Number : "
                        + preferences.getMobileNumber()
                        + "\nHandset Make & Model, OS Version : "
                        + deviceManufacturer
                        + ", "
                        + deviceModel
                        + ", "
                        + "Android "
                        + android.os.Build.VERSION.RELEASE
                        + "\nDate & Time : "
                        + dateFormat.format(cal.getTime())
                        + "\n___________________________________"+ "\n\n";


                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, toEmail);
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.putExtra(Intent.EXTRA_TEXT, messageBody);
                startActivity(Intent.createChooser(intent, ""));

            }
        });

        TvWebsiteInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("http://"+getResources().getString(R.string.website_link)); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });

    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }

}







