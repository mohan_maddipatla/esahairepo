package com.esahai.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Telephony;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esahai.BroadcastReceivers.SmsReceiver;
import com.esahai.R;
import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.dataObjects.VerifyOTPResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.moe.pushlibrary.MoEHelper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OTPVerificationActivity extends CommonActivity {

    private   final String TAG = "OTPVerificationActivity";
    EditText etMobileNumber, etOtp;
    Button btnSubmit;
    ApiInterface apiService;
    HashMap<String, Object> OTPVerification;
    String customerMobileNumber;
    AppPreferences preferences;
    private Timer myTimer;
    private TimerTask timerTask;
    private BroadcastReceiver optReceiver;
    public Runnable timeCounter = null;
    int counter = 0;

    TextView txtTime;

    protected MoEHelper helper = null;

    Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        initViews();
        preferences = new AppPreferences(OTPVerificationActivity.this);

        helper = MoEHelper.getInstance(this);

        try {
            customerMobileNumber = getIntent().getStringExtra("mobileNumber");
            etMobileNumber.setText("+"+customerMobileNumber);
//            etOtp.setText("" + getIntent().getExtras().getString("otp"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        optReceiver = new SmsReceiver() {

            @Override
            protected void onSMSArrived(String otp) {
                try {
                    unregisterReceiver(optReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                timerTask.cancel();
                stoptimertask();
                etOtp.setText(otp);
                callNetworkMethod();
            }
        };
        startTimer();
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        // mIntentFilter.setPriority(2147483647);
        registerReceiver(optReceiver, mIntentFilter);
        timeCounter = new Runnable() {

            @Override
            public void run() {
                counter++;
                txtTime.setText("" + counter);
                if (counter <= 75) {
                    handler.postDelayed(timeCounter, 750);
                } else if (counter > 75 && counter < 100) {
                    handler.postDelayed(timeCounter, 1500);
                } else if (counter >= 100) {
                    handler.removeCallbacks(timeCounter);
                    timerTask.cancel();
                    stoptimertask();

                //    linearCannotDetect.setVisibility(View.VISIBLE);
                 //   linearVerificationProgress.setVisibility(View.GONE);

                }
            }
        };

        apiService = ApiClient.getClient().create(ApiInterface.class);

    }

    private void initViews() {

        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etOtp = (EditText) findViewById(R.id.etOtp);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setEnabled(false);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyOtp();
            }
        });

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etOtp.getText().toString().length() == 6) {
                    closeKeyBoard(etOtp);
                    btnSubmit.setEnabled(true);
                } else {
                    btnSubmit.setEnabled(false);
                }
            }
        });

        etOtp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                            @Override
                                            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                                                boolean handled = false;
                                                if (actionId == EditorInfo.IME_ACTION_DONE) {
                                                    closeKeyBoard(textView);
                                                    verifyOtp();
                                                    handled = true;
                                                }
                                                return handled;
                                            }
                                        }
        );
    }

    private void verifyOtp() {
        if(!Utils.isGPSOn(OTPVerificationActivity.this)){
            Utils.turnOnGPS(OTPVerificationActivity.this);
        }else {

            if (etOtp.getText().toString().length() == 0) {
                etOtp.setError("Please check the OTP");
                return;
            } else {
                try {
                    callNetworkMethod();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void callNetworkMethod() {

        OTPVerification = new HashMap<>();
        String otp = Utils.encrypt(etOtp.getText().toString());
        String mobileNumber = Utils.encrypt(customerMobileNumber);
        String deviceIMEI = Utils.getIMEINumber(OTPVerificationActivity.this);
        String deviceType = Utils.getDeviceName();
        String deviceOs = Utils.getOSVersion();
        int version = Utils.getAppVersion();

        final String gcmRegId = FirebaseInstanceId.getInstance().getToken();
        preferences.setGCMToken(gcmRegId);
//        Log.i("gcmtoken--->",gcmRegId);

        Log.i("otp details--->", otp + "---customerMobileNumber--" + mobileNumber + "---deviceIMEI--" + deviceIMEI + "---deviceType--" + deviceType + "---deviceOs---" + deviceOs);

        OTPVerification.put("otp", otp);
        OTPVerification.put("customerMobile", mobileNumber);
        OTPVerification.put("deviceIMEI", deviceIMEI);
        OTPVerification.put("deviceType", "Android");
        OTPVerification.put("deviceOs", deviceOs);
        OTPVerification.put("deviceToken", preferences.getGCMToken());
        OTPVerification.put("fromApp", true);
        OTPVerification.put("app_version",""+version);

        Log.i("OTPVerification--", Arrays.asList(OTPVerification).toString());

        if (Utils.isNetworkAvailable(OTPVerificationActivity.this)) {
            Call<VerifyOTPResponse> verifyOTP = apiService.verifyOTP(OTPVerification);
            Utils.showProgressDialog(OTPVerificationActivity.this);
            verifyOTP.enqueue(new Callback<VerifyOTPResponse>() {
                @Override
                public void onResponse(Call<VerifyOTPResponse> call, Response<VerifyOTPResponse> response) {
                    try {
                        if (response.body() != null) {
                            Utils.hideProgressDialog();

                            VerifyOTPResponse verifyOTP = response.body();
                            Log.i("verify OTP", verifyOTP.toString());

                            if (Integer.parseInt(verifyOTP.getStatusCode()) == Constants.SUCCESS_CODE) {
                                AppPreferences preferences = new AppPreferences(OTPVerificationActivity.this);
                                preferences.setAccessToken(verifyOTP.getResponseData().getToken());
                                preferences.setCustomerId(verifyOTP.getResponseData().getCustomer_id());
                                preferences.setOTP(verifyOTP.getResponseData().getOtp());
                                preferences.setMobileNumber(verifyOTP.getResponseData().getCustomer_mobile_number());
                                preferences.setGCMToken(gcmRegId);
                                preferences.setProfileImage(verifyOTP.getResponseData().getFile_url());
                                preferences.setCustomerName(verifyOTP.getResponseData().getCustomer_name());

                                Log.d("new user",verifyOTP.getResponseData().getNew_user());

                                facebookEventForRegistration();

                                helper.setNumber(preferences.getMobileNumber());
                                helper.setUniqueId(preferences.getMobileNumber());

//                                EsahaiApplication.getApplication().trackEvent("OTP verification", "verify OTP", ""+generateOTP.getOtp());

                                if (verifyOTP.getResponseData().getNew_user().equalsIgnoreCase("yes")) {

                                    Intent intent = new Intent(OTPVerificationActivity.this, UpdateProfileActivity.class);
                                    intent.putExtra("isNewUser",true);
                                    startActivity(intent);
                                    finish();
                                } else {
                                   // preferences.setProfileFilledState(true);
                                    Intent intent = new Intent(OTPVerificationActivity.this, DashboardActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                            } else {
                                Utils.showAlert(OTPVerificationActivity.this, verifyOTP.getStatusMessage());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<VerifyOTPResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                }
            });
        } else {
            Utils.showToast(OTPVerificationActivity.this, getResources().getString(R.string.network_error));
        }


    }

    private void facebookEventForRegistration() {
        try {
            if(preferences.isProd()) {
                AppEventsLogger logger = AppEventsLogger.newLogger(this);
                logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION);
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private void closeKeyBoard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void startTimer() {
        if (myTimer != null) {
            myTimer.cancel();
            myTimer = null;
        }
        // set a new Timer
        myTimer = new Timer();
        // initialize the TimerTask's job
        initializeTimerTask();
        // schedule the timer, after the first 5000ms the TimerTask will run
        // every 10000ms
        myTimer.schedule(timerTask, 0, 15 * 1000); //
    }

    public void stoptimertask() {
        // stop the timer, if it's not already null
        if (myTimer != null) {
            myTimer.cancel();
            myTimer = null;
        }
    }

    @SuppressLint("NewApi")
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            @SuppressWarnings("unused")
            public void run() {
                if(ContextCompat.checkSelfPermission(OTPVerificationActivity.this,
                        android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED){
                    final Uri uriSMSURI = Uri.parse("content://sms/inbox");
                    long now = System.currentTimeMillis();
                    Log.d(TAG, " now = " + now);
                    long last20 = now - 20 * 1000;
                    String[] selectionArgs = new String[] { Long.toString(last20) };
                    String selection = "date" + ">?";
                    // showToast("Running Timer!");
                    String[] project = new String[] { Telephony.Sms.Inbox._ID,
                            Telephony.Sms.Inbox.ADDRESS, Telephony.Sms.Inbox.DATE,
                            Telephony.Sms.Inbox.BODY };
                    Cursor cursor = getContentResolver().query(
                            Telephony.Sms.Inbox.CONTENT_URI, project, selection,
                            selectionArgs, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        if (cursor.moveToFirst()) {
                            do {

                                String address = cursor.getString(1);
                                String body = cursor.getString(3);
                                if (address.trim().contains("eSahai")) {
                                    // showToast("Found SMS!");
                                    Log.d(TAG, " message |" + body + "|");
                                    String bodyString = body.trim();
                                    String otp;
                                    if (bodyString == null
                                            || bodyString.length() < 4) {
                                        otp = bodyString;
                                    } else {
                                        otp = bodyString.trim();
                                    }
                                    final String otp2 = otp;
                                    // String otp = body.substring(16).trim();
                                    timerTask.cancel();
                                    stoptimertask();
                                    runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            handler.removeCallbacks(timeCounter);
                                            try {
                                                unregisterReceiver(optReceiver);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            etOtp.setText(otp2);
                                            callNetworkMethod();
                                        }
                                    });

                                    Log.d(TAG, "otp = " + otp);
                                    break;
                                }
                            } while (cursor.moveToNext());
                        } else {
                            // showToast("Cannot read SMS!");
                        }
                        if (cursor != null && !cursor.isClosed())
                            cursor.close();
                    }
                }else{
                    stoptimertask();
                }
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(optReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        stoptimertask();
        handler.removeCallbacks(timeCounter);
    }
}
