package com.esahai.dataObjects;

/**
 * Created by ragamai on 21/10/16.
 */
public class LocationLatLng {

    public Geometry geometry;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    @Override
    public String toString() {
        return "LocationLatLng{" +
                "geometry=" + geometry +
                '}';
    }
}
