package com.esahai.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.Utils.AppObserver;
import com.esahai.activities.AmbulancesListActivity;
import com.esahai.activities.MapActivity;
import com.esahai.activities.NearestHospitalsActivity;
import com.esahai.activities.SearchAmbulanceActivity;
import com.esahai.activities.TrackAmbulanceActivity;
import com.esahai.dataObjects.AmbulanceDetails;
import com.esahai.dataObjects.AmbulanceType;
import com.esahai.dataObjects.AmbulanceTypeListResponse;
import com.esahai.dataObjects.BookingConfirmedDetails;
import com.esahai.dataObjects.BookingResponse;
import com.esahai.dataObjects.BookingStatus;
import com.esahai.dataObjects.EmergencyType;
import com.esahai.dataObjects.EmergencyTypeList;
import com.esahai.dataObjects.GoogleAddress;
import com.esahai.dataObjects.GoogleLatLng;
import com.esahai.dataObjects.HospitalDetails;
import com.esahai.dataObjects.HospitalList;
import com.esahai.dataObjects.NearByAmbulances;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ragamai on 15/9/16.
 */
@SuppressWarnings({"MissingPermission"})
public class DashboardFragment extends Fragment implements LocationListener,AppObserver {

    ArrayAdapter<String> ambulance_adapter, for_adapter, emergency_type_adapter, hospital_adapter;
    LocationManager locationManager;
    String selected_location = "";
    ApiInterface apiInterface, apiInterface1;
    String emergency_type, hospital;
    AppPreferences preferences;
    TextView text_ambulances_list;
    ArrayList<AmbulanceDetails> ambulances;
    boolean booking = false;
    ArrayList<HospitalDetails> hospitals;
    double hospital_latitude, hospital_longitude;
    ArrayList<EmergencyType> emergencyList;
    ArrayList<AmbulanceType> ambulanceType;
    String emergency_id;
    boolean hospital_ambulance;
    String address;
    String group_type_id;
    LinearLayout layout_mobile;
    int is_self = 1;
    EditText editText_phone_number;
    RelativeLayout layout_main;
    private Spinner spinner_ambulance, spinner_for, spinner_emergency, spinner_hospital;
    private Button button_submit;
    private TextView text_location, text_pickup;
    //    private String[] AMBULANCE_TYPE = {"Any Ambulance", "Hospital Ambulance", "108 Ambulance", "Any Nearest Hospital"};
    private String[] FOR_TYPE = {"For Myself", "For Someone",};
    private ArrayList<String> EMERGENCY_TYPE, HOSPITAL_TYPE, AMBULANCE_TYPE;
    private double latitude;
    private double longitude;
    private int SEARCH_LOCATION = 100;
    private String hospitalId;

    public static final int timeDifference = 1000 * 60 ;

    private Timer myTimer;

    boolean isLocationFetchedFromUser = false;

    boolean isbtnSubmitClicked;

    boolean isTripPending;

    Location currentLocation = null;



    BookingConfirmedDetails bookingConfirmedDetails
            = new BookingConfirmedDetails();

    public static final long MIN_TIME = 1000 * 60;
    public static final float MIN_DISTANCE = 100;

    public static Fragment newInstance(Context context) {
        DashboardFragment f = new DashboardFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.dashboard_fragment, null);

        preferences = new AppPreferences(getActivity());
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface1 = ApiClient.getGoogleClient().create(ApiInterface.class);

        initViews(root);



        try {
            getLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utils.isNetworkAvailable(getActivity())) {

            networkCallforEmergencyTypes();
            setAdaptersForSpinners();

            getActiveBooking();

            //networkCallForAmbulanceTypes();
           // networkCallforHospitals();
        } else {
            showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
        }

       // Utils.closeKeyBoard(getActivity(),editText_phone_number);



        return root;
    }

    private void networkCallForAmbulanceTypes() {
        if (Utils.isNetworkAvailable(getActivity())) {
            HashMap<String, Object> request = new HashMap<>();
            request.put("customerApp", true);
            request.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

            Log.i("req for ambTypes", Arrays.asList(request).toString());

            Call<AmbulanceTypeListResponse> ambulanceTypeList = apiInterface.getAmbulanceTypes(request,
                    preferences.getCustomerId(), preferences.getAccessToken());

            ambulanceTypeList.enqueue(new Callback<AmbulanceTypeListResponse>() {
                @Override
                public void onResponse(Call<AmbulanceTypeListResponse> call, Response<AmbulanceTypeListResponse> response) {

//                    Log.i("ambulancetype",response.toString());
                    try {

                        if (response.body() != null) {
                            Log.i("ambulancetypelist", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                ambulanceType = new ArrayList<AmbulanceType>();
                                ambulanceType = response.body().getResponseData();
                                AMBULANCE_TYPE = new ArrayList<String>();
                                for (int i = 0; i < ambulanceType.size(); i++) {
                                    AMBULANCE_TYPE.add(ambulanceType.get(i).getType_name());
                                }
                                AMBULANCE_TYPE.add("Any Nearest Hospital");
                                ambulance_adapter = new ArrayAdapter<String>(getActivity(),
                                        android.R.layout.simple_dropdown_item_1line, AMBULANCE_TYPE);
                                spinner_ambulance.setAdapter(ambulance_adapter);
                            }
                            //Utils.hideProgressDialog();
                        }

                        else{
                            Utils.hideProgressDialog();
                            Log.d("Response","Response is null");

                        }
                    } catch (Exception e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                }


                @Override
                public void onFailure(Call<AmbulanceTypeListResponse> call, Throwable t) {
                    Log.i("ambul failure", t.toString());
                    Utils.hideProgressDialog();
                    Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));

                }
            });
        } else {
            Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
        }

        networkCallforHospitals(); // Checking for loader
    }

    private void networkCallforEmergencyTypes() {

        Utils.showProgressDialog(getActivity());
        if (Utils.isNetworkAvailable(getActivity())) {

            Call<EmergencyTypeList> emergencyTypeListCall = apiInterface.getEmergencyTypes();
            emergencyTypeListCall.enqueue(new Callback<EmergencyTypeList>() {
                @Override
                public void onResponse(Call<EmergencyTypeList> call, Response<EmergencyTypeList> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("emergenciesList", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                emergencyList = new ArrayList<EmergencyType>();
                                emergencyList = response.body().getResponseData();

                                if (emergencyList != null) {
                                    EMERGENCY_TYPE = new ArrayList<String>();
                                    for (int i = 0; i < emergencyList.size(); i++) {
                                        EMERGENCY_TYPE.add(emergencyList.get(i).getType_name());
//                            Log.i("emergencies",Arrays.asList(EMERGENCY_TYPE).toString());
                                    }
                                    emergency_type_adapter = new ArrayAdapter<String>(getActivity(),
                                            android.R.layout.simple_dropdown_item_1line, EMERGENCY_TYPE);
                                    spinner_emergency.setAdapter(emergency_type_adapter);
                                }
                            }
                            //Utils.hideProgressDialog();
                        }
                        else{
                            Utils.hideProgressDialog();
                            Log.d("Response","Response_dashboard is null");
                        }
                    } catch (Exception e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<EmergencyTypeList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));


                }
            });

        } else {
            Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
        }

        networkCallForAmbulanceTypes(); // Checking for a loader
    }

    private void networkCallforHospitals(/*String address*/) {
        HashMap<String, Object> locationDetails = new HashMap<>();
//        convertAddress(text_locatx.getText().toString());
//        networkCallForConvertAddress(address);
        Location location;
        location = new Location("");

        try {
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            Log.i("location", latitude + "," + longitude + "-->" + location.getLatitude() + "," + location.getLongitude());
        } catch (Exception e) {
            Utils.hideProgressDialog();
            e.printStackTrace();
        }

        locationDetails.put("emergencyGeoHash", Utils.getGeoHashCode(location));
        locationDetails.put("latitude", latitude);
        locationDetails.put("longitude", longitude);
        locationDetails.put("groupType", "2");
        locationDetails.put("emergency_type", emergency_id);
        locationDetails.put("customerApp", true);
        locationDetails.put("ambulanceApp", false);
        locationDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

        Log.i("location details", Arrays.asList(locationDetails).toString());

        if (Utils.isNetworkAvailable(getActivity())) {
            Call<HospitalList> getHospitalsList = apiInterface.getNearByHospitalsList(locationDetails, preferences.getCustomerId(), preferences.getAccessToken());
            //Utils.showProgressDialog(getActivity());
            getHospitalsList.enqueue(new Callback<HospitalList>() {
                @Override
                public void onResponse(Call<HospitalList> call, Response<HospitalList> response) {
                    //Utils.hideProgressDialog();
                    try {
                        hospitals = new ArrayList<HospitalDetails>();
                        if (hospitals != null) {
                            hospitals.clear();
                            hospitals = null;
                        }

                        HOSPITAL_TYPE = new ArrayList<String>();
                        Log.i("hospitals list", response.body().toString());

                        if (response.body() != null && Integer.parseInt(response.body().getStatusCode())
                                == Constants.SUCCESS_CODE) {


                            Log.i("hospitals list", response.body().toString());
                            try {
                                hospitals = response.body().getResponseData();
                                if (hospitals != null && hospitals.size() > 0) {
                                    //button_submit.setEnabled(true);
                                    if (booking == true) {
                                        HOSPITAL_TYPE.add("Any Nearby Hospital");
                                    }
                                    if (hospitals != null && hospitals.size() > 0) {
                                        for (int i = 0; i < hospitals.size(); i++) {
                                            HOSPITAL_TYPE.add(hospitals.get(i).getGroup_short_name() + " (~" + hospitals.get(i).getDistance() + " Kms )");
                                        }
                                        hospital_adapter = new ArrayAdapter<String>(getActivity(),
                                                android.R.layout.simple_dropdown_item_1line, HOSPITAL_TYPE);
                                        spinner_hospital.setAdapter(hospital_adapter);
                                    }
                                } else {
                                    HOSPITAL_TYPE.add("No Hospitals Found");
                                    hospital_adapter = new ArrayAdapter<String>(getActivity(),
                                            android.R.layout.simple_dropdown_item_1line, HOSPITAL_TYPE);
                                    spinner_hospital.setAdapter(hospital_adapter);
                                }
                            } catch (Exception e) {
                                Utils.hideProgressDialog();
                                e.printStackTrace();
                            }
                            Utils.hideProgressDialog();

                        } else if (response.body() != null && Integer.parseInt(response.body().getStatusCode()) == Constants.NO_DATA) {
                            try {
                                HOSPITAL_TYPE.add("No Hospitals Found");
                                hospital_adapter = new ArrayAdapter<String>(getActivity(),
                                        android.R.layout.simple_dropdown_item_1line, HOSPITAL_TYPE);
                                spinner_hospital.setAdapter(hospital_adapter);
                               // Utils.hideProgressDialog();
                            } catch (Exception e) {
                                Utils.hideProgressDialog();
                                e.printStackTrace();
                            }
                            Utils.hideProgressDialog();
                        } else {
                            try {
                                HOSPITAL_TYPE.add("No Hospitals Found");
                                hospital_adapter = new ArrayAdapter<String>(getActivity(),
                                        android.R.layout.simple_dropdown_item_1line, HOSPITAL_TYPE);
                                spinner_hospital.setAdapter(hospital_adapter);

                            } catch (Exception e) {
                                Utils.hideProgressDialog();
                                e.printStackTrace();
                            }
                            Utils.hideProgressDialog();
                        }

//                        callNetworkMethodForAmbulances();
                    } catch (Exception e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<HospitalList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));

                }
            });
        } else {

            Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
        }

    }

    private void requestPermissionForLocation() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1338);
    }

    private void getLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionForLocation();
        } else {
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, DashboardFragment.this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, DashboardFragment.this);

//            Location location = locationManager
//                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Location location;
                location = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location == null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                latitude = location.getLatitude();
                longitude = location.getLongitude();

//                networkCallForLatLng(latitude,longitude);


                if (!selected_location.equals("")) {
                    text_location.setText(selected_location);
                    address = selected_location;
                } else {
                    Log.d("latlng","networkCallForLatLng1"+latitude+","+longitude);
                    address = networkCallForLatLng(latitude, longitude);
                    text_location.setText(address);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

//        networkCallforHospitals();

    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("onresume", "------------");

        EsahaiApplication.getApplication().addObserver(this);
        try {
            if(!isLocationFetchedFromUser){
                getLocation();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utils.isNetworkAvailable(getActivity())) {

          //  networkCallforEmergencyTypes();
          //  setAdaptersForSpinners();
          //  networkCallForAmbulanceTypes();
            networkCallforHospitals(/*text_location.getText().toString()*/);
        }/*else {
            showAlertDialog("No internet connection! Do you want to connect to Customer Service for booking?",true);
        }*/

        if (preferences != null) {
            if (preferences.isNoAmbulance() == true) {
                preferences.setNoAmbulance(false);
                showAlertDialog(getResources().getString(R.string.no_ambulance_contact_cc), true);
            }
        }



        if (!Utils.isGPSOn(getActivity())) {
            Utils.turnOnGPS(getActivity());
        } else {
            if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                    (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                requestPermissionForLocation();
            } else {
                locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, DashboardFragment.this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, DashboardFragment.this);
            }
//            getLocation();
//            networkCallforHospitals(/*text_location.getText().toString()*/);
        }
    }

    @Override
    public void onStop() {
        EsahaiApplication.getApplication().deleteObserver(this);
        super.onStop();
    }

    private void setAdaptersForSpinners() {
//        ambulance_adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, AMBULANCE_TYPE);

        for_adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, FOR_TYPE);


//        spinner_ambulance.setAdapter(ambulance_adapter);
        spinner_for.setAdapter(for_adapter);
        spinner_emergency.setAdapter(emergency_type_adapter);
        spinner_hospital.setAdapter(hospital_adapter);

        spinnerSelectItems();

    }

    private void spinnerSelectItems() {
        spinner_ambulance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("Any Nearest Hospital")) {
                    booking = false;
//                    text_pickup.setVisibility(View.GONE);
//                    text_location.setVisibility(View.GONE);
                } else {
                    booking = true;
//                    text_pickup.setVisibility(View.VISIBLE);
//                    text_location.setVisibility(View.VISIBLE);
                    group_type_id = ambulanceType.get(position).getId();
                    callNetworkMethodForGetAmbulancesList();
                }
                networkCallforHospitals();

                /*if(parent.getItemAtPosition(position).equals("Any Nearest Hospital")){
                    text_pickup.setVisibility(View.GONE);
                    text_location.setVisibility(View.GONE);
                    text_ambulances_list.setVisibility(View.GONE);
                    booking = false;
                }
                else{
                    text_pickup.setVisibility(View.VISIBLE);
                    text_location.setVisibility(View.VISIBLE);
                    text_ambulances_list.setVisibility(View.VISIBLE);
                    booking = true;
                }

                if(parent.getItemAtPosition(position).equals("Hospital Ambulance")){
                    hospital_ambulance = true;
                }
                else{
                    hospital_ambulance = false;
                }*/

//                networkCallforHospitals(text_location.getText().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //emergency type spinner
        spinner_emergency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                emergency_type = (String) parent.getItemAtPosition(position);
                emergency_id = emergencyList.get(position).getId();
                callNetworkMethodForGetAmbulancesList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //hospital spinner
        spinner_hospital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hospital = (String) parent.getItemAtPosition(position);
                /*if(booking==true) {
                    if (position != 0 && hospitals != null && hospitals.size() > 0) {
                        hospital = hospitals.get(position - 1).getGroup_short_name();
                        hospitalId = hospitals.get(position - 1).getGroup_id();
                        Log.i("hospital position", "" + position);
                        Log.i("hospital", hospital);
                        hospital_latitude = Double.parseDouble(hospitals.get(position - 1).getGroup_latitude());
                        hospital_longitude = Double.parseDouble(hospitals.get(position - 1).getGroup_longitude());
//                        callNetworkMethodForAmbulances();
                    }

                    *//*if(hospital_ambulance == true ){
                        if(position == 0){
                            callNetworkMethodForAmbulances();
                        }else{
                            networkCallForAmbulancesBasedOnHospitals();
                        }
                    }*//*
                }
                else if (hospitals != null && hospitals.size() > 0) {

                        hospital = hospitals.get(position).getGroup_short_name();
                        hospitalId = hospitals.get(position).getGroup_id();
//                        Log.i("hospital position", "" + position);
                        Log.i("hospital", hospital);
                        hospital_latitude = Double.parseDouble(hospitals.get(position).getGroup_latitude());
                        hospital_longitude = Double.parseDouble(hospitals.get(position).getGroup_longitude());
//                        callNetworkMethodForAmbulances();
                }
*/
                /*if(booking == true && hospital_ambulance == true){
                    if(position == 0){
                        callNetworkMethodForAmbulances();
                    }else {
                        networkCallForAmbulancesBasedOnHospitals();
                    }
                }
                else if(booking == true && hospital_ambulance == false){
                    callNetworkMethodForAmbulances();
                }*/

                /*if(hospital_ambulance == true){
                    hospitalId = hospitals.get(position - 1).getGroup_id();
                    //callfor ambu;ances
                }else{

                }*/
                try {
                    if (booking == true) {
                        if (position == 0) {
                            hospitalId = "Any";
                            hospital_latitude = 0.0;
                            hospital_longitude = 0.0;
                        } else {
                            hospitalId = hospitals.get(position - 1).getGroup_id();
                            hospital_latitude = Double.parseDouble(hospitals.get(position - 1).getGroup_latitude());
                            hospital_longitude = Double.parseDouble(hospitals.get(position - 1).getGroup_longitude());
                            Log.i("hospital position", "" + (position - 1));
                            Log.i("hospital", hospital);
                        }
                        callNetworkMethodForGetAmbulancesList();
                    } else if (booking == false) {
                        hospitalId = hospitals.get(position).getGroup_id();
                        hospital_latitude = Double.parseDouble(hospitals.get(position).getGroup_latitude());
                        hospital_longitude = Double.parseDouble(hospitals.get(position).getGroup_longitude());
                        Log.i("hospital position", "" + (position));
                        Log.i("hospital", hospital);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_for.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).equals("For Myself")) {
                    layout_mobile.setVisibility(View.GONE);
                    is_self = 1;
                    preferences.setIsSelf(true);
                } else {
                    layout_mobile.setVisibility(View.VISIBLE);
                    is_self = 0;
                    preferences.setIsSelf(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initViews(ViewGroup root) {
        spinner_ambulance = (Spinner) root.findViewById(R.id.spinner_ambulance);
        spinner_for = (Spinner) root.findViewById(R.id.spinner_for);
        spinner_emergency = (Spinner) root.findViewById(R.id.spinner_emergency);
        spinner_hospital = (Spinner) root.findViewById(R.id.spinner_hospital);

        text_location = (TextView) root.findViewById(R.id.text_location);
        text_pickup = (TextView) root.findViewById(R.id.text_pickup);
        button_submit = (Button) root.findViewById(R.id.button_submit);

        layout_mobile = (LinearLayout) root.findViewById(R.id.layout_mobile);
        editText_phone_number = (EditText) root.findViewById(R.id.editText_phone_number);
        layout_main = (RelativeLayout) root.findViewById(R.id.layout_main);

        text_ambulances_list = (TextView) root.findViewById(R.id.text_ambulances_list);
        text_ambulances_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ambulances != null) {
                    Intent ambulanceList = new Intent(getActivity(), AmbulancesListActivity.class);
                    ambulanceList.putExtra("ambulances", ambulances);
                    startActivity(ambulanceList);
                }
            }
        });

        text_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isGPSOn(getActivity())) {
                    Utils.turnOnGPS(getActivity());
                } else {
                    Intent i = new Intent(getActivity(), MapActivity.class);
                    startActivityForResult(i, SEARCH_LOCATION);
                }
            }
        });

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkAvailable(getActivity())) {

                    if (emergency_type.equalsIgnoreCase("Schedule Medical Taxi")) {
                        isbtnSubmitClicked = false;
                        showAlertDialog(getResources().getString(R.string.contact_cc), true);

                    } else {

                        //button_submit.setEnabled(false);
                        if (booking == true) {

                            if (is_self == 0) {
                                if (editText_phone_number.getText().toString().length() != 0 &&
                                        editText_phone_number.getText().toString().length() < 10) {
                                    Utils.showToast(getActivity(), "Enter valid mobile Number");
                                } else {
                                    if (!text_ambulances_list.getText().toString().equalsIgnoreCase("No Ambulances Found")) {
                                        try {

                                            Log.d("Booking", "'! NoAmbulancesFound'");
                                            if (!isbtnSubmitClicked) {

                                                isbtnSubmitClicked = true;

                                                Log.d("Booking", "Button Submit clicked = " + isbtnSubmitClicked);


                                                /*if (!emergency_type.equalsIgnoreCase("Schedule Medical Taxi")) {
                                                    Log.d("Booking", "'! ScheduleMedicalTaxi'");
                                                    callNetworkMethod();
                                                } else {
                                                    isbtnSubmitClicked = false;
                                                    showAlertDialog(getResources().getString(R.string.contact_cc), true);

                                                }*/
                                                //isbtnSubmitClicked = false;
                                            }

                                            Log.d("Profile", "ForSomeOne");

                                            if (!editText_phone_number.getText().toString().isEmpty()) {
                                                Log.d("Profile", "ForSomeOne" + editText_phone_number.getText().toString());
                                                preferences.setPatient_MobileNumber(
                                                        editText_phone_number.getText().toString());

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                /*If the address is out of bound then we show no ambulance,
                                but after coming back in the range and user presses the submit
                                button instantly the no ambulances keeps displaying all the time.
                                So doing a network call for ambulances here.
                                */
                                        networkCallforHospitals();
                                        Log.d("Booking", "isself== 0");
                                        showAlertDialog(getResources().getString(R.string.no_ambulance_contact_cc), true);/*Utils.showToast(getActivity(),"No Ambulances found nearby, Try again.");*/
                                    }


                                }
                            } else if (is_self == 1) {

                                if (!text_ambulances_list.getText().toString().equalsIgnoreCase("No Ambulances Found")) {
                                    try {

                                        Log.d("Booking", "'! NoAmbulancesFound'___is_self==1");
                                        Log.d("Booking", "isbtnSubmitClicked:::" + isbtnSubmitClicked);
                                        if (!isbtnSubmitClicked) {
                                            isbtnSubmitClicked = true;

                                            Log.d("Booking", "is_self==1___Button Submit clicked = " + isbtnSubmitClicked);
                                            if (!emergency_type.equalsIgnoreCase("Schedule Medical Taxi")) {
                                                Log.d("Booking", "'is_self==1___! ScheduleMedicalTaxi'");
                                                callNetworkMethod();
                                            } else {
                                                isbtnSubmitClicked = false;
                                                showAlertDialog(getResources().getString(R.string.contact_cc), true);

                                            }
                                            //isbtnSubmitClicked = false;
                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {

                                    networkCallforHospitals();
                                    Log.d("Booking", "isself== 1");
                                    showAlertDialog(getResources().getString(R.string.no_ambulance_contact_cc), true);/*Utils.showToast(getActivity(),"No Ambulances found nearby, Try again.");*/
                                }


                            }
                        } else {
//                    getLocation();
                            if (hospitalId != null) {
                                Intent hospitals = new Intent(getActivity(), NearestHospitalsActivity.class);
                                hospitals.putExtra("latitude", latitude);
                                hospitals.putExtra("longitude", longitude);
                                hospitals.putExtra("hospital_latitude", hospital_latitude);
                                hospitals.putExtra("hospital_longitude", hospital_longitude);
                                startActivity(hospitals);
                            } else {
                                Utils.showToast(getActivity(), getResources().getString(R.string.select_hospital));
                            }
                        }
                    }

                    }else{
                        showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
                    }

            }
        });
//        setFontsForHeader(layout_main);


        editText_phone_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (editText_phone_number.getText().toString().length() == 10) {

                    editText_phone_number.setCursorVisible(false);
                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(editText_phone_number.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);


                } else {

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }



    /* private void setFontsForHeader(ViewGroup v) {
         try {
             for (int i=0; i < v.getChildCount(); i++) {
                 View view = v.getChildAt(i);
                 if (view instanceof TextView)
                     ((TextView)view).setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "robotoslab_bold.ttf"));
                 else if (view instanceof ViewGroup)
                     setFontsForHeader((ViewGroup) view);
             }

         } catch (Exception e) {e.printStackTrace();
         }
     }
 */
    private void callNetworkMethodForGetAmbulancesList() {

        /*
        {
          "emergencyGeoHash": "KzQZngf",
          "latitude": "89.0",
          "longitude": "0.909",
          "customerApp": false,
          "ambulanceApp": false
        }
         */

        if (Utils.isNetworkAvailable(getActivity())) {

            String customerId = preferences.getCustomerId();
            String access_token = preferences.getAccessToken();
            networkCallForConvertAddress(text_location.getText().toString());
            Location location;
            location = new Location("");
            try {

                location.setLatitude(latitude);
                location.setLongitude(longitude);
            } catch (Exception e) {
            }
//            }

            String geoHash = Utils.getGeoHashCode(location);

            HashMap<String, Object> request = new HashMap<>();
            request.put("emergencyGeoHash", geoHash);
            request.put("latitude", latitude);
            request.put("longitude", longitude);
            request.put("customerApp", true);
            request.put("ambulanceApp", "false");
            request.put("group_id", hospitalId);
            request.put("emergency_type", emergency_id);
            request.put("group_type_id", group_type_id);
            request.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

            Log.i("request for ambulances", Arrays.asList(request).toString());

            Call<NearByAmbulances> ambulancesList = apiInterface.getAmbulanceList(request, customerId, access_token);

            //Utils.showProgressDialog(getActivity());
            ambulancesList.enqueue(new Callback<NearByAmbulances>() {
                @Override
                public void onResponse(Call<NearByAmbulances> call, Response<NearByAmbulances> response) {
                    try {
                        if (response.body() != null &&
                                Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                            //Utils.hideProgressDialog();
                            Log.i("ambulances list", response.body().toString());
                            ambulances = response.body().getResponseData();
                            if (ambulances.size() > 0) {
                                text_ambulances_list.setText("Found " + ambulances.size() + " ambulances");
                                text_ambulances_list.setClickable(true);
                            } else {
                                text_ambulances_list.setText("No Ambulances Found");
                                text_ambulances_list.setClickable(false);
                            }


                        } else if (response.body() != null && Integer.parseInt(response.body().getStatusCode()) == Constants.NO_DATA) {
                            text_ambulances_list.setText("No Ambulances Found");
                            text_ambulances_list.setClickable(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<NearByAmbulances> call, Throwable t) {
                   // Utils.hideProgressDialog();
                    try {

                        if(getActivity()== null){
                            return;
                        }
                        else {

                            Utils.hideProgressDialog();
                            Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));
                        }
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }

                }
            });

        } else {
            Utils.showToast(getActivity(), getResources().getString(R.string.network_error));
        }
    }



   /* private void callNetworkMethodForAmbulances() {

        *//*
        {
          "emergencyGeoHash": "KzQZngf",
          "latitude": "89.0",
          "longitude": "0.909",
          "customerApp": false,
          "ambulanceApp": false
        }
         *//*

        if(Utils.isNetworkAvailable(getActivity())){

            String customerId = preferences.getCustomerId();
            String access_token = preferences.getAccessToken();
//            convertAddress(text_location.getText().toString());
            networkCallForConvertAddress(text_location.getText().toString());
            Location location;
            location = new Location("");

//            try {
//                location = convertAddress(text_location.getText().toString());
//            }catch (Exception e){
            try {

                location.setLatitude(latitude);
                location.setLongitude(longitude);
            }catch (Exception e){}
//            }

            String geoHash = Utils.getGeoHashCode(location);

            HashMap<String, Object> request = new HashMap<>();
            request.put("emergencyGeoHash", geoHash);
            request.put("latitude", latitude);
            request.put("longitude", longitude);
            request.put("customerApp", true);
            request.put("ambulanceApp", "false");
//            request.put("group_id", hospitalId);
//            request.put(" emergency_type", emergency_type);

            Log.i("request for ambulances",Arrays.asList(request).toString());

            Call<NearByAmbulances> ambulancesList = apiInterface.searchNearByAmbulances(request,customerId,access_token);

            ambulancesList.enqueue(new Callback<NearByAmbulances>() {
                @Override
                public void onResponse(Call<NearByAmbulances> call, Response<NearByAmbulances> response) {
                    try {
                    if(response.body()!=null && Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {

                            Log.i("ambulances list", response.body().toString());
                            ambulances = response.body().getResponseData();
                            if(ambulances.size()>0) {
                                text_ambulances_list.setText("Found " + ambulances.size() + " ambulances");
                                text_ambulances_list.setClickable(true);
                            }
                            else{
                                text_ambulances_list.setText("No Ambulances Found");
                                text_ambulances_list.setClickable(false);
                            }



                    }
                    else  if(Integer.parseInt(response.body().getStatusCode()) == Constants.NO_DATA){
                        text_ambulances_list.setText("No Ambulances Found");
                        text_ambulances_list.setClickable(false);
                    }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<NearByAmbulances> call, Throwable t) {

                }
            });

        }
        else{Utils.showToast(getActivity(),"Check your Internet Connection");}
    }*/


    private void callNetworkMethod() {

        if (Utils.isNetworkAvailable(getActivity())) {
            //button_submit.setEnabled(false);
            final AppPreferences preference = new AppPreferences(getActivity());
            String token = preference.getAccessToken();
            String encrypted_mobile_number = Utils.encrypt(preference.getMobileNumber());
            String customerId = preference.getCustomerId();
//            convertAddress(text_location.getText().toString());
            networkCallForConvertAddress(text_location.getText().toString());


            Location location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);

            String geoHash = Utils.getGeoHashCode(location);

//            Utils.srcLatLng(latitude,longitude);

            HashMap<String, Object> createBooking = new HashMap<>();
            HashMap<String, Object> bookingDetails = new HashMap<>();

            if (preference.getCustomerName() != null && !preference.getCustomerName().equalsIgnoreCase("")) {
                bookingDetails.put("customer_name", preference.getCustomerName());
            } else {
                bookingDetails.put("customer_name", "No Name");
            }

            if (is_self == 0) {
                bookingDetails.put("patient_number", editText_phone_number.getText().toString());
            } else {
                bookingDetails.put("patient_number", "");
            }
            bookingDetails.put("is_self", is_self);
            bookingDetails.put("customer_mobile", encrypted_mobile_number);
            bookingDetails.put("booked_by", "Customer");

            bookingDetails.put("emergency_type", emergency_type);

            bookingDetails.put("emergency_longitude", longitude);
            bookingDetails.put("emergency_latitude", latitude);
            bookingDetails.put("emergency_geoHashCode", geoHash);
            bookingDetails.put("hospital", hospital);
            bookingDetails.put("customer_id", customerId);

            Log.d("booking_request"," latitude::"+latitude+" longitude::"+longitude);



            createBooking.put("params", bookingDetails);
            createBooking.put("customerApp", true);
            createBooking.put("address", text_location.getText().toString());
            createBooking.put("gcmToken", FirebaseInstanceId.getInstance().getToken());

//             convertAddress(hospital);

            createBooking.put("hospital_latitude", hospital_latitude);
            createBooking.put("hospital_longitude", hospital_longitude);
//            if(hospital_ambulance == true) {
            createBooking.put("group_id", hospitalId);
            createBooking.put("emergency_type", emergency_id);
            createBooking.put("group_type_id", group_type_id);



            Log.d("booking","getUserAge_fromPref"
                    +preferences.getUser_Age());
            //if (is_self == 0) {
                createBooking.put("age", preferences.getUser_Age());
            //}

//            }

            Location hospitalLocation = new Location("");
            hospitalLocation.setLatitude(hospital_latitude);
            hospitalLocation.setLongitude(hospital_longitude);

            createBooking.put("hospital_geoHashCode", Utils.getGeoHashCode(hospitalLocation));
            createBooking.put("customer_mobile", Utils.encrypt(preference.getMobileNumber()));

            Log.i("booking details", String.valueOf(Arrays.asList(createBooking)));

            Log.d("eSahai", "USER_ID::::" + customerId);
            Log.d("eSahai", "TOKEN::::" + token);


            Call<BookingResponse> booking = apiInterface.createBooking(createBooking, customerId, token);
            Utils.showProgressDialog(getActivity());
           // button_submit.setEnabled(true);

            booking.enqueue(new Callback<BookingResponse>() {
                @Override
                public void onResponse(Call<BookingResponse> call, Response<BookingResponse> response) {
                    Utils.hideProgressDialog();

                    String customerCareNumber;

                    try {
                        if (response.body() != null) {

                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                Log.i("create booking response", response.body().toString());
                                preference.setBookingId(response.body().getResponseData().getBooking_id());


                                Log.d("Response", "setCustomercareNumber Response::::" + response.body().getResponseData().getCommandCentreNumber());

                                customerCareNumber = response.body().getResponseData().getCommandCentreNumber();

                                Log.i("create booking response", response.body().toString());
                                if (customerCareNumber != null && !customerCareNumber.equalsIgnoreCase("null")
                                        && !customerCareNumber.isEmpty()) {

                                    preference.setCustomerCareNumber(response.body().getResponseData().getCommandCentreNumber());
                                    Log.d("Response", "setCustomercareNumber Response::::" + response.body().getResponseData().getCommandCentreNumber());

                                }
                                Log.i("create booking response", response.body().toString());
                                EsahaiApplication.getApplication().trackEvent("Dashboard", "Booking-->Booking id",
                                        response.body().getResponseData().getBooking_id());

                                isbtnSubmitClicked = false;
                                Intent ambulance = new Intent(getActivity(), SearchAmbulanceActivity.class);
                                ambulance.putExtra("latitude", latitude);
                                ambulance.putExtra("longitude", longitude);
                                ambulance.putExtra("booking_id", preference.getBookingId());

                                startActivity(ambulance);

                            }else{

                                Log.d("SUBMIT","Button submit clicked in " +
                                        "else dialog previous booking---> "+isbtnSubmitClicked);
                                isbtnSubmitClicked = false;
                                Utils.hideProgressDialog();
                                showAlertDialog(""+response.body().getStatusMessage()+
                                        " Do you want to call: "
                                         +preference.getCustomerCareNumber(),true);
                                //Utils.showToast(getActivity(),response.body().getStatusMessage()
                                       // +preference.getCustomerCareNumber());

                            }
                        }
                        else{
                            isbtnSubmitClicked = false;
                        }
                    } catch (Exception e) {
                        isbtnSubmitClicked = false;
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }



                }

                @Override
                public void onFailure(Call<BookingResponse> call, Throwable t) {
                    isbtnSubmitClicked = false;
                    Log.i("error", t.toString());
                    Utils.hideProgressDialog();
                    Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));

                }
            });


        } else {/*Utils.showToast(getActivity(),"Check your Internet Connection");*/
            isbtnSubmitClicked = false;
            showAlertDialog(getResources().getString(R.string.network_error_contact_cc) , true);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == SEARCH_LOCATION && resultCode == getActivity().RESULT_OK) {
                if (data != null) {

                    //button_submit.setEnabled(false);
                    isLocationFetchedFromUser = true;
                    locationManager.removeUpdates(this);
                    selected_location = (String) data.getExtras().get("location");
                    latitude = data.getExtras().getDouble("latitude");
                    longitude = data.getExtras().getDouble("longitude");
                    Log.d("lat lang","dash onactivity lat langi = "+latitude+", "+longitude);
                    Utils.showProgressDialog(getActivity());
                    if (!selected_location.equals("")) {
                        text_location.setText(selected_location);
//                        networkCallForConvertAddress(selected_location);
//                        Utils.showProgressDialog(getActivity());
                        networkCallforHospitals();
                    }
                    else{
                        Log.i("entering","yes");
//                        Utils.showProgressDialog(getActivity());
                        String address = networkCallForLatLng(latitude,longitude);

                        text_location.setText(address);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*String result = null;
    private String getAddress(final double latitude, final double longitude){
        this.latitude = latitude;
        this.longitude = longitude;

        new Thread(new Runnable() {
            @Override
            public void run() {

        try{
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<android.location.Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                android.location.Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                result = sb.toString();
            }
        }
        catch (Exception e){
            result = latitude+" , "+longitude;
        }
            }
        }).start();
        return  result;
    }*/

    private String networkCallForLatLng(final double latitude, final double longitude) {

        Log.d("latlng","address::: lat"+latitude+"long"+longitude);
        if (Utils.isNetworkAvailable(getActivity())) {
            Call<GoogleAddress> getLatLng = apiInterface1.getAddress(latitude + "," + longitude,
                    getActivity().getResources().getString(R.string.geo_coding_key));

            getLatLng.enqueue(new Callback<GoogleAddress>() {
                @Override
                public void onResponse(Call<GoogleAddress> call, Response<GoogleAddress> response) {
                    try {
                        if (response.body() != null && response.body().getStatus().equalsIgnoreCase("OK")) {
                            Log.i("latLng response", response.body().toString());
                            address = response.body().getResults().get(0).getFormatted_address();
                        } else {
                            address = latitude + "," + longitude;
                        }
                        text_location.setText(address);
                        Utils.hideProgressDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<GoogleAddress> call, Throwable t) {

                    Utils.hideProgressDialog();
                    Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));

                }
            });


        } else {
            Utils.hideProgressDialog();
            Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
        }
        return address;
    }

    private void networkCallForConvertAddress(String address) {
        Log.d("address 8", "address" + address);
        if (address != null) {
            if (Utils.isNetworkAvailable(getActivity())) {

                Log.i("eSahai", "Internet is on");

                Call<GoogleLatLng> convertAddress = apiInterface1.getLatLng(address,
                        getActivity().getResources().getString(R.string.geo_coding_key));

                if (convertAddress.toString() != null) {
                    Log.i("eSahai", "convertAddress:::" + convertAddress);
                }

                convertAddress.enqueue(new Callback<GoogleLatLng>() {
                    @Override
                    public void onResponse(Call<GoogleLatLng> call, Response<GoogleLatLng> response) {
                        try {

                            Log.i("eSahai", "IN on_response");

                            if (response.body() != null && response.body().getStatus().equals("OK")) {
                                Log.i("address res", response.body().toString());
                                //latitude = response.body().getResults().get(0).getGeometry().getLocation().getLat();
                                //longitude = response.body().getResults().get(0).getGeometry().getLocation().getLng();
                                Log.i("address latlng", latitude + " , " + longitude);
                            } else {
                              //  Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<GoogleLatLng> call, Throwable t) {

                        Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));

                    }
                });

            } else {
                Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
            }
        }
    }




    /*public Location convertAddress(final String address) {
        final Location location = new Location("");
        if (address != null && !address.isEmpty()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
            try {
                Geocoder geoCoder = new Geocoder(getActivity());
                List<Address> addressList = geoCoder.getFromLocationName(address, 1);
                if (addressList != null && addressList.size() > 0) {
                    double lat = addressList.get(0).getLatitude();
                    double lng = addressList.get(0).getLongitude();
                    latitude = lat;
                    longitude = lng;
                    location.setLatitude(latitude);
                    location.setLongitude(longitude);
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    String string = address;
                    if (!address.equalsIgnoreCase("My current location")) {
                        String[] parts = string.split(" , ");
                        latitude = Double.parseDouble(parts[0]);
                        longitude = Double.parseDouble(parts[1]);
                        location.setLatitude(latitude);
                        location.setLongitude(longitude);
                    }
                }catch (Exception e1){};
            }
                }
            }).start();
        }

        return location;
    }*/

    @Override
    public void onLocationChanged(Location location) {
        Log.i("location change", "changeeee");
        try {

           // if (text_location.getText().toString().equalsIgnoreCase("Fetching...")) {
               if(!isLocationFetchedFromUser) {
                   Log.i("location change", location.getLatitude() + "," + location.getLongitude());
                   if(isBetterLocation(location,currentLocation)) {
                       currentLocation = location;
                       latitude = location.getLatitude();
                       longitude = location.getLongitude();

                       Log.d("latlng","networkCallForLatLng2"+latitude+","+longitude);
                       text_location.setText(networkCallForLatLng(latitude, longitude));
                       networkCallforHospitals();
                   }
               }
          //  }
        } catch (Exception e) {
            e.printStackTrace();
        }



       /* latitude = location.getLatitude();
        longitude = location.getLongitude();
//        text_location.setText(getAddress(latitude,longitude));
//        networkCallForLatLng(latitude,longitude);
        Log.i("lat and lng",latitude+" , "+longitude);

        try {
            if(!selected_location.equals("")){
                text_location.setText(selected_location);
                address = selected_location;
            }
            else{
                text_location.setText(networkCallForLatLng(latitude,longitude));
            }
        }catch (Exception e){
            e.printStackTrace();

            text_location.setText(networkCallForLatLng(latitude,longitude));
        }*/
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onPause() {
        if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionForLocation();
        } else {
            try {
                locationManager.removeUpdates(DashboardFragment.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    private void showAlertDialog(String message, final boolean isNetwork) {
        String yes, no;
        if (isNetwork) {
            yes = "Yes";
            no = "No";
            Log.d("Dialog","Dialog_IF set to : "+isNetwork);
        } else {
            yes = "Connect to Customer service";
            no = "Try other options ";
            Log.d("Dialog","Dialog_ELSE set to : "+isNetwork);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            if (preferences.getCustomerCareNumber() != null) {

                                Log.d("CustomerCare", "Number IS::::" + preferences.getCustomerCareNumber());
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + preferences.getCustomerCareNumber()));
                                startActivity(intent);

                            } else {
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + 0));
                                startActivity(intent);
                            }
                            dialog.cancel();
                        } else {
                            requestPermissionForCall();
//                    Toast.makeText(BookingConfirmedActivity.this,"Permission denied",Toast.LENGTH_SHORT).show();
                        }
                    }


                });
        if(isNetwork) {
            builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        }
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }


    private void getActiveBooking() {



        Log.d("Activebooking", "ActivebookingID");
        try {
            if (Utils.isNetworkAvailable(getActivity())) {
                HashMap<String, Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id", preferences.getCustomerId());
                bookingDetails.put("ambulanceApp", false);
                bookingDetails.put("customerApp", true);
                bookingDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

                Log.i("Activebooking", Arrays.asList(bookingDetails).toString());

                Call<BookingStatus> activeBooking = apiInterface.getActiveBooking(bookingDetails, preferences.getCustomerId(), preferences.getAccessToken());
                //Utils.showProgressDialog(SearchAmbulanceActivity.this);
                activeBooking.enqueue(new Callback<BookingStatus>() {
                    @Override
                    public void onResponse(Call<BookingStatus> call, Response<BookingStatus> response) {
                        //Utils.hideProgressDialog();
                        //Log.i("Activebooking ", "active booking response_outside try" + response.body().toString());
                        try {
                            //Log.i("Activebooking ", "active booking response_outside IF" + response.body().toString());
                            if (response.body() != null) {
                                Log.i("Activebooking ", "active booking response!=NULL" + response.body().toString());

                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE ) {


                                    preferences.setBookingDetails(response.body().getResponseData());

                                    preferences.setBookingId(response.body().getResponseData().booking_id);

                                    preferences.setHospitalLat(response.body().getResponseData().hospital_latitude);
                                    preferences.setHospitalLng(response.body().getResponseData().hospital_longitude);

                                    /*if(response.body().getResponseData().booking_status.
                                            equalsIgnoreCase("Way Back to Hospital")){
                                        preferences.setBookingCancellable(false);
                                        Log.d("ActiveBooking","Way back to hospital");
                                    }else{
                                        preferences.setBookingCancellable(true);
                                    }*/

                                    Utils.setAmbulanceStatus(response.body().getResponseData().booking_status);

                                    Intent intent = new Intent(getActivity(), TrackAmbulanceActivity.class);
                                    intent.putExtra("bookingDetails", response.body().getResponseData());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);

                                }else{

                                    Log.d("booking","status_message"+response.body().getStatusMessage());
                                    //Utils.showToast(getActivity(),response.body().getStatusMessage());

                                }

                                } else {
                                //Toast.makeText(getActivity(), response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();
                                Log.d("Activebooking","Activebooking_Error"+response.body().getStatusMessage());

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<BookingStatus> call, Throwable t) {
                        Utils.hideProgressDialog();
                        Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_slow));

                    }
                });


            } else {
                Log.d("Activebooking","Activebooking_No internet");
                Utils.showToast(getActivity(), getResources().getString(R.string.network_error));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // checking is better location
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
       Log.d("Location_updates","currentBestLocation"+currentBestLocation) ;
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > timeDifference;
        boolean isSignificantlyOlder = timeDelta < -timeDifference;
        boolean isNewer = timeDelta > 0;


        Log.d("Location_updates","isSignificantlyNewer"+isSignificantlyNewer) ;//false
        Log.d("Location_updates","isSignificantlyOlder"+isSignificantlyOlder) ;//false
        Log.d("Location_updates","isNewer"+isNewer) ;   //true
        Log.d("Location_updates","timeDelta"+timeDelta) ; //   19986


        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        /*

         */

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta >= 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 40;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

         Log.d("Location_updates","accuracyDelta"+accuracyDelta) ;//0
         Log.d("Location_updates","isLessAccurate"+isLessAccurate) ;  //false
         Log.d("Location_updates","isMoreAccurate"+isMoreAccurate) ; //false
         Log.d("Location_updates","isSignificantlyLessAccurate"+isSignificantlyLessAccurate) ;  //false
        Log.d("Location_updates","isFromSameProvider"+isFromSameProvider) ;      //true
        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            Log.d("Location_updates","IN_isMoreAccurate") ;
            return true;
        } else if (isNewer && !isLessAccurate) {
            Log.d("Location_updates","IN_isNewer && !isLessAccurate") ;
            return true;
        } else if (isNewer && isSignificantlyLessAccurate && isFromSameProvider) {
            Log.d("Location_updates","IN_isNewer && !isSignificantlyLessAccurate && isFromSameProvider") ;
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    @Override
    public void update(int event, Object arg) {

        if(event == EsahaiApplication.bookingStatusUpdate){
            getActiveBooking();
        }
    }
}
