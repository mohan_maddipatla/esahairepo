package com.esahai.dataObjects;

/**
 * Created by adheesh on 30/01/17.
 */

public class PolicyResponse {

    public String id;
    public String privacy_and_policy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrivacy_policy() {
        return privacy_and_policy;
    }

    public void setPrivacy_policy(String privacy_and_policy) {
        this.privacy_and_policy = privacy_and_policy;
    }

    @Override
    public String toString() {
        return "PolicyResponse{" +
                "id='" + id + '\'' +
                ", privacy_and_policy='" + privacy_and_policy + '\'' +
                '}';
    }

}
