package com.esahai.dataObjects;

/**
 * Created by ragamai on 7/11/16.
 */
public class PatientInfo {

    /*
    "patient_id": "1",
 		"patient_name": "ragamai",
 		"contact_number": "9654813548",
 		"patient_condition": "critical",
 		"gender": "Female",
 		"age": "24",
 		"customer_id": "66056056",
 		"booking_id": "817124",
 		"created_date": "2016-11-07 12:36:52",
 		"updated_ate": null
     */

    public String patient_id;
    public String patient_name;
    public String contact_number;
    public String patient_condition;
    public String gender;
    public String customer_id;
    public String booking_id;
    public String created_date;
    public String updated_ate;
    public String age;

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getPatient_condition() {
        return patient_condition;
    }

    public void setPatient_condition(String patient_condition) {
        this.patient_condition = patient_condition;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_ate() {
        return updated_ate;
    }

    public void setUpdated_ate(String updated_ate) {
        this.updated_ate = updated_ate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "PatientInfo{" +
                "patient_id='" + patient_id + '\'' +
                ", patient_name='" + patient_name + '\'' +
                ", contact_number='" + contact_number + '\'' +
                ", patient_condition='" + patient_condition + '\'' +
                ", gender='" + gender + '\'' +
                ", customer_id='" + customer_id + '\'' +
                ", booking_id='" + booking_id + '\'' +
                ", created_date='" + created_date + '\'' +
                ", updated_ate='" + updated_ate + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
