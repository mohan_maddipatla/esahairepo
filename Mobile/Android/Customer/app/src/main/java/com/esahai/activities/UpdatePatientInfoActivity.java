package com.esahai.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.dataObjects.PatientInfo;
import com.esahai.dataObjects.PatientResponse;
import com.esahai.dataObjects.ProfileDetails;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 27/9/16.
 */
public class UpdatePatientInfoActivity extends CommonActivity {

    Button button_update;
    EditText editText_condition,editText_age,editText_patient,editText_phone;
    Spinner spinner_gender;
    String[] GENDER = {"Male", "Female"};
    ArrayAdapter<String> gender_adapter;
    String gender;
    ApiInterface apiInterface;
    AppPreferences preferences;
    String patient_number;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_patient_info);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        preferences = new AppPreferences(UpdatePatientInfoActivity.this);

        initViews();
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.update_patient_info));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        button_update = (Button)findViewById(R.id.button_update);

        spinner_gender = (Spinner)findViewById(R.id.spinner_gender);
        editText_condition = (EditText)findViewById(R.id.editText_condition);
        editText_age = (EditText)findViewById(R.id.editText_age);
        editText_phone = (EditText)findViewById(R.id.editText_phone);
        editText_patient = (EditText)findViewById(R.id.editText_patient);

        gender_adapter = new ArrayAdapter<String>(UpdatePatientInfoActivity.this,
                android.R.layout.simple_dropdown_item_1line, GENDER);
        spinner_gender.setAdapter(gender_adapter);

        spinnerSelectsItem();

        editText_age.setText(""+preferences.getUser_Age());
        Log.d("userAge","Patient_Info"
                +preferences.getUser_Age());


        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                validateViews();
            }
        });

        networkCallForGetPatientInfo();
    }

    private void networkCallForGetPatientInfo() {
        if(Utils.isNetworkAvailable(UpdatePatientInfoActivity.this)){

            // booking_id
            HashMap<String,Object> request = new HashMap<>();
            request.put("booking_id",preferences.getBookingId());
            request.put("customerApp",true);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));


            Call<PatientResponse> getPatientInfo = apiInterface.getPatientInfo(request,
                    preferences.getCustomerId(),preferences.getAccessToken());
            Utils.showProgressDialog(UpdatePatientInfoActivity.this);
            getPatientInfo.enqueue(new Callback<PatientResponse>() {
                @Override
                public void onResponse(Call<PatientResponse> call, Response<PatientResponse> response) {
                    Utils.hideProgressDialog();
                    try{
                        Log.i("get info",""+response.body());
                        if(response.body()!=null){
                            Log.i("get info",response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                setData(response.body().getResponseData().get(0));
                            }
                            if(Integer.parseInt(response.body().getStatusCode())== Constants.CANCELLED_CODE1){
                                setData(null);
                            }
                        }
                        else{
                            Log.i("response","NULL");
                        }

                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<PatientResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(UpdatePatientInfoActivity.this, getResources().getString(R.string.network_slow));

                }
            });

        }else{Utils.showToast(UpdatePatientInfoActivity.this,getResources().getString(R.string.network_error));}
    }

    private void setData(PatientInfo patientInfo) {
        if(patientInfo!=null){
            try{
                if(patientInfo.getPatient_name()!=null){editText_patient.setText(patientInfo.getPatient_name());}
                if(!patientInfo.getAge().equals("") && Integer.parseInt(patientInfo.getAge()) != 0){editText_age.setText(patientInfo.getAge());}
                if(patientInfo.getContact_number()!=null){
                    patient_number = patientInfo.getContact_number();
                    if(patientInfo.getContact_number().length() == 12) {
                        patient_number = patient_number.substring(2);
                    }
                    if(patientInfo.getContact_number().length() == 11){
                        patient_number = patient_number.substring(1);
                    }
                    editText_phone.setText(patient_number);
                }
                if(patientInfo.getPatient_condition()!=null){editText_condition.setText(patientInfo.getPatient_condition());}
                if(patientInfo.getGender()!=null){
                    if(patientInfo.getGender().equalsIgnoreCase("Female")){spinner_gender.setSelection(1);}
                    else{spinner_gender.setSelection(0);}
//                    if(patientInfo.getGender().equalsIgnoreCase("male")){spinner_gender.setSelection(1);}
                }


            }catch (Exception e){e.printStackTrace();}
        }
        else if(patientInfo == null){
            if(preferences.isSelf() == true){
                try {
                    ProfileDetails profile = preferences.getProfileDetails();
                    if (preferences.getProfileDetails() != null) {

                        if (profile.getCustomer_name() != null) {
                            editText_patient.setText(profile.getCustomer_name());
                        }
                        if (profile.getCustomer_mobile_number() != null) {
                            patient_number = profile.getCustomer_mobile_number();
                            if(profile.getCustomer_mobile_number().length() == 12) {
                                patient_number = patient_number.substring(2);
                            }
                            if(profile.getCustomer_mobile_number().length() == 11){
                                patient_number = patient_number.substring(1);
                            }
                            editText_phone.setText(patient_number);

                        }
                        if (profile.getAge() != null) {
                            editText_age.setText(profile.getAge());
                        }
                        if (profile.getGender() != null) {
                            if (profile.getGender().equalsIgnoreCase("Female")) {
                                spinner_gender.setSelection(1);
                            } else {
                                spinner_gender.setSelection(0);
                            }
                        }
                    }
                    else{
                        Log.i("profile data","null");

                    }
                }catch (Exception e){e.printStackTrace();}
            }
            else if(preferences.isSelf() == false){
                if(!preferences.getPatient_MobileNumber().equals("")){
                    patient_number = preferences.getPatient_MobileNumber();
                    if(preferences.getPatient_MobileNumber().length() == 12) {
                        patient_number = patient_number.substring(2);
                    }
                    if(preferences.getPatient_MobileNumber().length() == 11){
                        patient_number = patient_number.substring(1);
                    }
                    editText_phone.setText(patient_number);

                    spinner_gender.setSelection(2);


                }
                else {
                    try {
                        ProfileDetails profile = preferences.getProfileDetails();
                        if (preferences.getProfileDetails() != null) {

                            if (profile.getCustomer_name() != null) {
                                editText_patient.setText(profile.getCustomer_name());
                            }
                            if (profile.getCustomer_mobile_number() != null) {
                                patient_number = profile.getCustomer_mobile_number();
                                if(profile.getCustomer_mobile_number().length() == 12) {
                                    patient_number = patient_number.substring(2);
                                }
                                if(profile.getCustomer_mobile_number().length() == 11){
                                    patient_number = patient_number.substring(1);
                                }
                                editText_phone.setText(patient_number);

                            }
                            if (profile.getAge() != null) {
                                editText_age.setText(profile.getAge());
                            }
                            if (profile.getGender() != null) {
                                if (profile.getGender().equalsIgnoreCase("Female")) {
                                    spinner_gender.setSelection(1);
                                } else {
                                    spinner_gender.setSelection(0);
                                }
                            }
                        }
                        else{
                            Log.i("profile data","null");

                        }
                    }catch (Exception e){e.printStackTrace();}
                }
            }

        }
        /*else if(patientInfo == null && preferences.isSelf() == true){
            try {
                ProfileDetails profile = preferences.getProfileDetails();
                if (preferences.getProfileDetails() != null) {

                    if (profile.getCustomer_name() != null) {
                        editText_patient.setText(profile.getCustomer_name());
                    }
                    if (profile.getCustomer_mobile_number() != null) {
                        patient_number = profile.getCustomer_mobile_number();
                        if(profile.getCustomer_mobile_number().length() == 12) {
                            patient_number = patient_number.substring(2);
                        }
                        if(profile.getCustomer_mobile_number().length() == 11){
                            patient_number = patient_number.substring(1);
                        }
                        editText_phone.setText(patient_number);

                    }
                    if (profile.getAge() != null) {
                        editText_age.setText(profile.getAge());
                    }
                    if (profile.getGender() != null) {
                        if (profile.getGender().equalsIgnoreCase("Female")) {
                            spinner_gender.setSelection(1);
                        } else {
                            spinner_gender.setSelection(0);
                        }
                    }
                }
                else{
                    Log.i("profile data","null");

                }
            }catch (Exception e){e.printStackTrace();}
        }
        else{

            if (!preferences.getPatient_MobileNumber().equals("")) {
                patient_number = preferences.getPatient_MobileNumber();
                if(preferences.getPatient_MobileNumber().length() == 12) {
                    patient_number = patient_number.substring(2);
                }
                if(preferences.getPatient_MobileNumber().length() == 11){
                    patient_number = patient_number.substring(1);
                }
                editText_phone.setText(patient_number);
            }
            else{
                patient_number = preferences.getMobileNumber();
                if(preferences.getMobileNumber().length() == 12) {
                    patient_number = patient_number.substring(2);
                }
                if(preferences.getMobileNumber().length() == 11){
                    patient_number = patient_number.substring(1);
                }
                editText_phone.setText(patient_number);
            }
        }*/
    }

    private void spinnerSelectsItem() {

        spinner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void validateViews() {
       /* if(editText_patient.getText().toString().length()==0 || editText_phone.getText().toString().length()==0 ||
                editText_age.getText().toString().length()==0 || editText_condition.getText().toString().length()==0){
            Utils.showToast(UpdatePatientInfoActivity.this,"Enter all the fields");
        }
        else{*/

        if(editText_phone.getText().toString().length()!=0 && editText_phone.getText().toString().length()<10){
            Utils.showToast(UpdatePatientInfoActivity.this,getResources().getString(R.string.valid_mobile_number));
        }else {

            networkCallForPatientInfo();
        }
//        }
    }

    private void networkCallForPatientInfo() {
        if(Utils.isNetworkAvailable(UpdatePatientInfoActivity.this)){
            // patient_name, contact_number,patient_condition,gender,age,booking_id
            HashMap<String,Object> request = new HashMap<>();
            if(editText_patient.getText().toString().length() == 0){
                request.put("patient_name"," ");
            }else {
                request.put("patient_name", editText_patient.getText().toString());
            }

            if(editText_phone.getText().toString().length() == 0) {
                request.put("contact_number"," ");
            }else {
                request.put("contact_number", preferences.getCountryCode()+""+editText_phone.getText().toString());
            }
            if(editText_condition.getText().toString().length() == 0) {
                request.put("patient_condition"," ");
            }else {
                request.put("patient_condition", editText_condition.getText().toString());
            }
            request.put("gender",gender);
            if(editText_age.getText().toString().length() == 0) {
                request.put("age",0);
            }else {
                request.put("age", editText_age.getText().toString());
            }
            request.put("booking_id",preferences.getBookingId());
            request.put("customerApp",true);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));


            Log.i("request", Arrays.asList(request).toString());


            Call<StatusResponse> getPatientInfo = apiInterface.updatePatientInfo(request,
                    preferences.getCustomerId(),preferences.getAccessToken());
            Utils.showProgressDialog(UpdatePatientInfoActivity.this);
            getPatientInfo.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    Utils.hideProgressDialog();
                    try{
                        if(response.body()!=null){
                            Log.i("get info",response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                EsahaiApplication.getApplication().trackEvent("Update patient info", "Update patient info", "updated successfully");
                                finish();
                            }
                        }

                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(UpdatePatientInfoActivity.this, getResources().getString(R.string.network_slow));

                }
            });

        }else{Utils.showToast(UpdatePatientInfoActivity.this,getResources().getString(R.string.network_error));}
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/
}
