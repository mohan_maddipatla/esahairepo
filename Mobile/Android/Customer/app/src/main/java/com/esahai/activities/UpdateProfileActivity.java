package com.esahai.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.adapters.AttributesAdapter;
import com.esahai.dataObjects.Attributes;
import com.esahai.dataObjects.ComeToKnowBy;
import com.esahai.dataObjects.KnowBy;
import com.esahai.dataObjects.Profile;
import com.esahai.dataObjects.ProfileDetails;
import com.esahai.dataObjects.ProfilePicResponse;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.dataObjects.SystemAttributes;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 27/9/16.
 */
public class UpdateProfileActivity extends CommonActivity {

    Button button_submit;
    EditText editText_name, editText_age,editText_email,editText_address,
            editText_state,editText_city,editText_pincode;
    Spinner spinner_gender,spinner_blood_group,spinner_feed;
    CheckBox checkBox_donate_blood;
    String[] GENDER = {"Male", "Female"};
    ArrayAdapter<String> gender_adapter,blood_group_adapter,feed_adapter;
    String name, age, gender, city;
    CircleImageView profile_image;
    PopupWindow popupWindow;
    LayoutInflater layoutInflater;

    AppPreferences preferences;
    ApiInterface apiService;
    LinearLayout layout_profile,layout,layout_main;
    ArrayList<SystemAttributes> array_systSystemAttributes;
    ArrayList<Attributes> array_attributes;
    ArrayList<ComeToKnowBy> comeToKnowBy;
    FloatingActionButton fActionButton;
    HashMap<String ,Object> attributes;
    EditText editTextView;
    TextView textView_attribute;
    ArrayList<KnowBy> knowBy;
    ArrayList<String> KNOW_BY;
    ImageView imageView_back;

    boolean isAllowedCheck;

    Context mContext;
    final int REQUEST_CAMERA = 100, SELECT_FILE = 101;

    private final int MY_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1;

    private boolean askOptionsAfterPermissionGranted = false;
    private boolean isReadExternalStoragePermissionGranted = false;
    String[] BLOOD_GROUP = {"A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"};

//    String[] KNOW = {"Advertisement" ,"Family", "Friends", "News Paper","Numaish", "Social Media","Others" };
    String blood_group;
    String donate_blood;
    String know;
    boolean isNewUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        preferences = new AppPreferences(UpdateProfileActivity.this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        mContext = this;
        attributes = new HashMap<>();
        initViews();
//        setFonts(layout);
        comeToKnowBy = new ArrayList<>();
        netwrokCallForComeToKnowBy();
        networkCallForProfile();
        if (!checkExternalStoragePermission()) {
            requestPermission();
        }else{
            isReadExternalStoragePermissionGranted = true;
        }


//        setSpinnerData();

        try{
            if(getIntent().hasExtra("isNewUser")){
                isNewUser = getIntent().getExtras().getBoolean("isNewUser");
                if(isNewUser){
                    imageView_back.setVisibility(View.INVISIBLE);
                }else{
                    imageView_back.setVisibility(View.VISIBLE);
                }
            }
        }catch (Exception e){e.printStackTrace();}

    }

    private void setSpinnerData() {
        try{
//            if(preferences.getComeToKnowBy() == null){

            /*}else{
                knowBy = new ArrayList<KnowBy>();
                KNOW_BY = new ArrayList<String>();
                knowBy = preferences.getComeToKnowBy();

                if(knowBy!=null && knowBy.size()>0){
                    for(int i = 0 ; i < knowBy.size() ;i++){
                        if (knowBy.get(i).getStatus().equalsIgnoreCase("Active")) {
                            KNOW_BY.add((knowBy.get(i).getKnow_by()));
                        }
                    }
                    feed_adapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                            R.layout.simple_spinner_feed_text_view, KNOW_BY);
                    spinner_feed.setAdapter(feed_adapter);
                }
            }*/
        }catch (Exception e){e.printStackTrace();}
    }

    private void setFonts(ViewGroup v) {
        try {
            for (int i=0; i < v.getChildCount(); i++) {
                View view = v.getChildAt(i);
                if (view instanceof TextView)
                    ((TextView)view).setTypeface(Typeface.createFromAsset(getAssets(), "gotham_condensed_bold.ttf"));
                else if (view instanceof ViewGroup)
                    setFonts((ViewGroup) view);
            }
            /*if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    setFonts(child);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(getAssets(), "gotham_condensed_bold.ttf"));
            }*/
        } catch (Exception e) {e.printStackTrace();
        }
    }

    private void networkCallForProfile() {
        if(Utils.isNetworkAvailable(UpdateProfileActivity.this)){
            HashMap<String,Object> request = new HashMap<>();
            request.put("customer_id",preferences.getCustomerId());
            request.put("customerApp",true);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Log.i("customer profile",Arrays.asList(request).toString());


            Call<Profile> profileDetails = apiService.getProfileDetails(request, preferences.getCustomerId(),preferences.getAccessToken());
            Utils.showProgressDialog(UpdateProfileActivity.this);
            profileDetails.enqueue(new Callback<Profile>() {
                @Override
                public void onResponse(Call<Profile> call, Response<Profile> response) {

                    try {
                        Utils.hideProgressDialog();
                        if (response.body() != null) {
                            Log.i("profile data",response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                ProfileDetails profile = response.body().getResponseData();
                                preferences.setProfileDetails(profile);
                                array_systSystemAttributes = new ArrayList<SystemAttributes>();
                                array_attributes = new ArrayList<Attributes>();
                                array_systSystemAttributes = response.body().getResponseData().getSystemAttributes();
                                array_attributes = response.body().getResponseData().getAttributes();
                                setData(profile);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<Profile> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(UpdateProfileActivity.this,getResources().getString(R.string.network_slow));
                    Log.i("profile data failure",t.toString());
                }
            });




        }else{
            Utils.hideProgressDialog();
            Utils.showToast(UpdateProfileActivity.this,getResources().getString(R.string.network_error));}
    }

    private void setData(ProfileDetails profile) {
        try {
            if (profile != null) {
                if(profile.getCustomer_name()!=null) {
                    editText_name.setText(profile.getCustomer_name());
                }
                if(profile.getAge()!=null){
                    preferences.setUser_Age(age);
                    editText_age.setText(profile.getAge());
                }
                if(profile.getGender()!=null){
                    if(profile.getGender().equalsIgnoreCase("Female")){
                        spinner_gender.setSelection(1);
                    }
                    else if(profile.getGender().equalsIgnoreCase("Male")){
                        spinner_gender.setSelection(0);
                    }
                }
                if(profile.getCustomer_email()!=null){
                    editText_email.setText(profile.getCustomer_email());
                }
                if(profile.getAddress()!=null){
                    editText_address.setText(profile.getAddress());
                }
                if(profile.getCity()!=null){
                    editText_city.setText(profile.getCity());
                }
                if(profile.getState()!=null){
                    editText_state.setText(profile.getState());
                }
                if(profile.getPincode()!=null){
                    editText_pincode.setText(profile.getPincode());
                }
                /*if(profile.getCome_to_know_by()!=null){
                    editText_feed.setText(profile.getCome_to_know_by());
                }*/
                if(profile.getDonating_blood().equalsIgnoreCase("1")){
                    checkBox_donate_blood.setChecked(true);
                    donate_blood = "1";
                }else{
                    checkBox_donate_blood.setChecked(false);
                    donate_blood = "0";
                }
                if(profile.getFile_url()!=null && !profile.getFile_url().isEmpty()){
                    preferences.setProfileImage(profile.getFile_url());
                }

                if(preferences.getProfileImage()!=null && !preferences.getProfileImage().isEmpty()){
                    Picasso
                            .with(UpdateProfileActivity.this)
                            .load(preferences.getProfileImage())
                            .into(profile_image);
                }

                if(profile.getBlood_group()!=null){
                    for(int i = 0 ; i < BLOOD_GROUP.length ; i++){
                        if(profile.getBlood_group().equalsIgnoreCase(BLOOD_GROUP[i])){
                            spinner_blood_group.setSelection(i);
                        }
                    }
                }

                if(!profile.getCome_to_know_by().equals("")){


                        for (int i = 0; i < KNOW_BY.size(); i++) {
                            if (profile.getCome_to_know_by().equalsIgnoreCase(KNOW_BY.get(i))) {
                                spinner_feed.setSelection(i);
                            }
                        }

                }
            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

            if(array_attributes!=null && array_attributes.size()>0 &&
                    array_systSystemAttributes!=null && array_systSystemAttributes.size()>0){
                for(int i = 0 ; i < array_attributes.size() ; i++){
                    for(int j = 0 ; j <array_systSystemAttributes.size() ; j++){
                        if(array_attributes.get(i).getAttribute_name().
                                equalsIgnoreCase(array_systSystemAttributes.get(j).getType_name())){
                            addDynamicViewsForAttributes(array_attributes.get(i));
                        }
                    }
                }
            }



        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onBackPressed() {
       /* if (!preferences.getProfileFilledState()) {
            return;
        }*/
        if(!isNewUser) {
            super.onBackPressed();
        }
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.profile));
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utils.closeKeyBoard(UpdateProfileActivity.this,imageView_back);
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

//        if (preferences.getProfileFilledState()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
//        }

        button_submit = (Button) findViewById(R.id.button_submit);
        editText_name = (EditText) findViewById(R.id.editText_name);
        editText_age = (EditText) findViewById(R.id.editText_age);
        editText_city = (EditText) findViewById(R.id.editText_city);
        editText_email = (EditText) findViewById(R.id.editText_email);
        editText_address = (EditText) findViewById(R.id.editText_address);
        editText_state = (EditText) findViewById(R.id.editText_state);
        editText_pincode = (EditText) findViewById(R.id.editText_pincode);
        spinner_feed = (Spinner) findViewById(R.id.spinner_feed);

        spinner_gender = (Spinner) findViewById(R.id.spinner_gender);
        spinner_blood_group = (Spinner) findViewById(R.id.spinner_blood_group);

        checkBox_donate_blood = (CheckBox)findViewById(R.id.checkBox_donate_blood);

        profile_image = (CircleImageView)findViewById(R.id.profile_image);
        layout_profile = (LinearLayout)findViewById(R.id.layout_profile);
        fActionButton = (FloatingActionButton)findViewById(R.id.fActionButton);
        layout_main = (LinearLayout)findViewById(R.id.layout_main);

        gender_adapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                android.R.layout.simple_dropdown_item_1line, GENDER);
        spinner_gender.setAdapter(gender_adapter);

        blood_group_adapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                R.layout.simple_spinner_feed_text_view, BLOOD_GROUP);
        spinner_blood_group.setAdapter(blood_group_adapter);

        /*feed_adapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                R.layout.simple_spinner_feed_text_view, KNOW);
        //android.R.layout.simple_dropdown_item_1line
        spinner_feed.setAdapter(feed_adapter);*/

        editText_name.requestFocus();

        spinnerSelectsItem();


        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editText_name.getText().toString() == null ||
                        editText_name.getText().toString().equalsIgnoreCase("")){
                    editText_name.requestFocus();
                    editText_name.setError(getResources().getString(R.string.valid_name));
                }
                else if(editText_age.getText().toString() == null ||
                    editText_age.getText().toString().equalsIgnoreCase("")){

                    editText_age.requestFocus();
                    editText_age.setError(getResources().getString(R.string.valid_age));
                }
                else {
                    validateFields();
                }

            }
        });

        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        fActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogForAttributes();

            }
        });

        /*if(preferences.getSavedProfiePic()!=null){
            profile_image.setImageURI(preferences.getSavedProfiePic());
        }else{
            profile_image.setImageDrawable(getResources().getDrawable(R.drawable.mine));
        }*/

        checkBox_donate_blood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkBox_donate_blood.isChecked()){
                    showAlertDialog(getResources().getString(R.string.donate_blood_message_1));
                }
                else{
                    showAlertDialog(getResources().getString(R.string.donate_blood_message_2));
                }




            }
        });

        /*checkBox_donate_blood.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                donate_blood = buttonView.isChecked();


                if(isChecked == true){
                    checkBox_donate_blood.setChecked(true);

                }
                else{
                    checkBox_donate_blood.setChecked(false);
                }
                Log.i("donate blood",""+isChecked);
            }
        });*/

//        setFonts(layout_main);
    }

    private void showDialogForAttributes() {
        if(array_systSystemAttributes!=null && array_systSystemAttributes.size()>0) {
            AlertDialog channelsAlertDialog = null;
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(UpdateProfileActivity.this);
            LayoutInflater inflater = UpdateProfileActivity.this.getLayoutInflater();
            View view = inflater.inflate(R.layout.dialog_attributes, null);
            TextView txtTitle = (TextView) view.findViewById(R.id.textView_title);
            txtTitle.setText(getResources().getString(R.string.add_attributes));

            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycle_list_attributes);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            alertDialog.setView(view);
            alertDialog.setCancelable(true);
            channelsAlertDialog = alertDialog.create();


            if (array_systSystemAttributes != null && array_systSystemAttributes.size() > 0) {
                AttributesAdapter adapter = new AttributesAdapter(UpdateProfileActivity.this, array_systSystemAttributes, channelsAlertDialog);


                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            channelsAlertDialog.show();
        }
    }

    public void addDynamicViews(SystemAttributes systemAttributes) {
        try {
            array_systSystemAttributes.remove(array_systSystemAttributes.indexOf(systemAttributes));
            textView_attribute = new TextView(this);
            editTextView = new EditText(this);
            //editTextView.setGravity(Gravity.LEFT);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1);

            params.setMargins(40,5,30,0);

            textView_attribute.setLayoutParams(params);
            textView_attribute.setText(systemAttributes.getType_name());
            textView_attribute.setTextColor(getResources().getColor(R.color.button_color));
            textView_attribute.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            textView_attribute.setTypeface(Typeface.DEFAULT_BOLD);

            editTextView.setLayoutParams(params);
            editTextView.setHint(systemAttributes.getType_name());
            editTextView.setId(Integer.parseInt(systemAttributes.getId()));
            editTextView.setTag("");
            editTextView.setBackground(getResources().getDrawable(R.drawable.gray_line));

            layout_profile.addView(textView_attribute);
            layout_profile.addView(editTextView);


        }catch (Exception e){e.printStackTrace();}
    }

    public void addDynamicViewsForAttributes(Attributes attributes) {
        try {
            for(int i = 0 ; i < array_systSystemAttributes.size() ; i++){
                if(attributes.getAttribute_name().equalsIgnoreCase(array_systSystemAttributes.get(i).getType_name())){
                    array_systSystemAttributes.remove(i);
                }
            }
            textView_attribute = new TextView(this);
            editTextView = new EditText(this);
//            editTextView.setGravity(Gravity.LEFT);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1);

            params.setMargins(40,5,30,0);

            textView_attribute.setLayoutParams(params);
            textView_attribute.setText(attributes.getAttribute_name());
            textView_attribute.setTextColor(getResources().getColor(R.color.button_color));
            textView_attribute.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            textView_attribute.setTypeface(Typeface.DEFAULT_BOLD);

            editTextView.setLayoutParams(params);
            editTextView.setHint(attributes.getAttribute_name());
            editTextView.setText(attributes.getAttribute_details());
            editTextView.setId(Integer.parseInt(attributes.getCustomer_e_type_id()));
            editTextView.setTag(attributes.getId());
            editTextView.setBackground(getResources().getDrawable(R.drawable.gray_line));
//            TextInputLayout in = new TextInputLayout(this);
//            in.addView(editTextView);

            layout_profile.addView(textView_attribute);
            layout_profile.addView(editTextView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }


    private void validateFields() {
        Utils.closeKeyBoard(UpdateProfileActivity.this,button_submit);
        String name = editText_name.getText().toString();
        String age = editText_age.getText().toString();
        String city = editText_city.getText().toString();
        String email = editText_email.getText().toString();
        String address = editText_address.getText().toString();
        String state = editText_state.getText().toString();
        String pincode = editText_pincode.getText().toString();
        String feed = know;

        preferences.setUser_Age(age);

        /*if (name.length() == 0 || age.length() == 0 || city.length() == 0 || email.length() == 0 || address.length() == 0
                || state.length()== 0 || pincode.length() == 0 || feed.length() == 0) {
            Utils.showToast(UpdateProfileActivity.this, "Enter all the fields");
        }*/




        if( editText_email.getText().toString().length()!=0 && !android.util.Patterns.EMAIL_ADDRESS.matcher(editText_email.getText().toString()).matches()){
            Utils.showToast(UpdateProfileActivity.this, getResources().getString(R.string.valid_email));
        }
        else {
            try {

                View tempView;
                List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                for(int i = 0; i < layout_profile.getChildCount(); i++){
                    tempView = layout_profile.getChildAt(i);
                    HashMap<String,Object> attr = new HashMap<>();
                    if(tempView instanceof EditText){
                        try {
                            int id = tempView.getId();
                            String hint = ((EditText) tempView).getHint().toString();
                            String assembledString = ((EditText) tempView).getText().toString();
                            String attr_id = ((EditText) tempView).getTag().toString();
                            if(attr_id!=null && !attr_id.equalsIgnoreCase("")){
                                attr.put("id",attr_id);
                            }
                            attr.put("attribute_name",hint);
                            attr.put("attribute_details",assembledString);
                            attr.put("customer_e_type_id",id);
                            list.add(attr);

                    /*
                    {"id":1,"attribute_name":"blood","attribute_details":"ioi","customer_e_type_id":1}
                     */
                            Log.i("attributes", Arrays.asList(list).toString());
                        }catch (Exception e){e.printStackTrace();}
                    }
                    else{
                        Log.d("Profile","Attributes_Not an instance");
                    }
                }


                callNetworkMethod(list);
                Log.d("Profile","During network call attributes list:::"+list.toString());
            }catch (Exception e){e.printStackTrace();}
        }
    }

    private void callNetworkMethod(List<Map<String, Object>> list) {
        Log.d("Profile","attributes list"+list.toString());
        if (Utils.isNetworkAvailable(UpdateProfileActivity.this)) {
            /*
            "customerId": 8128275,
          "customerName": "Ragamai Kanumuri",
          "gender": "Female",
          "age": 24
             */
            HashMap<String, Object> profileDetails = new HashMap<>();

            profileDetails.put("customerId", Integer.parseInt(preferences.getCustomerId()));
            profileDetails.put("customerName", editText_name.getText().toString());
            profileDetails.put("gender", gender);
            profileDetails.put("age", editText_age.getText().toString());
            profileDetails.put("customerApp", true);
            profileDetails.put("email", editText_email.getText().toString());
            profileDetails.put("address", editText_address.getText().toString());
            profileDetails.put("city", editText_city.getText().toString());
            profileDetails.put("state", editText_state.getText().toString());
            profileDetails.put("pincode", editText_pincode.getText().toString());
            profileDetails.put("blood_group", blood_group);
            profileDetails.put("donating_blood",donate_blood);
            profileDetails.put("come_to_know_by", know);
            profileDetails.put("Attributes",list);
            profileDetails.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Log.i("profileDetails", String.valueOf(Arrays.asList(profileDetails)));

            Call<StatusResponse> profile = apiService.updateProfile(profileDetails, preferences.getCustomerId(),
                    preferences.getAccessToken());
            Utils.showProgressDialog(UpdateProfileActivity.this);
            Log.i("attr list",Arrays.asList(list).toString());

            profile.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {

                    try {
                        Utils.hideProgressDialog();
                        if(response!=null) {
                            Utils.hideProgressDialog();
                            Log.i("Update profile response", response.body().toString());
                            ;
                            if (Integer.parseInt(response.body().statusCode) == Constants.SUCCESS_CODE) {
                                EsahaiApplication.getApplication().trackEvent("Update profile", "profile update", "Profile update successful");
                                Utils.showToast(UpdateProfileActivity.this,getResources().getString(R.string.profile_update_success));
                                preferences.setCustomerName(editText_name.getText().toString());


                                Intent intent = new Intent(UpdateProfileActivity.this, DashboardActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            } else {
                                Utils.hideProgressDialog();
                                Utils.showAlert(UpdateProfileActivity.this, response.body().statusMessage);
                            }
                        }
                        else{
                            Utils.hideProgressDialog();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }

                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(UpdateProfileActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        } else {
            Utils.hideProgressDialog();
            Utils.showToast(UpdateProfileActivity.this, getResources().getString(R.string.network_error));
        }
    }

    private void spinnerSelectsItem() {

        spinner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_blood_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    blood_group = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_feed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                know = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/



    public boolean checkExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    public void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(UpdateProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Toast.makeText(mContext, getResources().getString(R.string.profile_pic_permission), Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(UpdateProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    isReadExternalStoragePermissionGranted = true;
                    if (askOptionsAfterPermissionGranted) {
                        askOptionsAfterPermissionGranted = false;
                        selectImage();
                    }
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.from_gallery),
                getResources().getString(R.string.cancel)};
        System.out.println("select image");
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getResources().getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals(getResources().getString(R.string.from_gallery))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {

            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);

            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            ;
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        Uri tempUri = getImageUri(getApplicationContext(), thumbnail);
       String pathToImage =  getRealPathFromURI(tempUri);

        String fileNameSegments[] = pathToImage.split("/");
        String fileName = fileNameSegments[fileNameSegments.length - 1];
        String fileFormateSegments[] = fileName.split(".");
        String fileFormate = "";
        if (fileFormateSegments.length > 1) {
            fileFormate = "."
                    + fileNameSegments[fileFormateSegments.length - 1];
            System.out.println("fileFormate = " + fileFormate);
        }
        String finalFileName = "file"+fileFormate;

        Log.d("finalFileName",""+finalFileName);
        try {
            File file = new File(mContext.getCacheDir(), finalFileName);
            file.createNewFile();

//Convert bitmap to byte array
            profile_image.setDrawingCacheEnabled(false);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            uploadFileToServer(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
//                File file = new File(data.getData().getPath());

                Uri tempUri = getImageUri(getApplicationContext(), bm);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File file = new File(getRealPathFromURI(tempUri));
                uploadFileToServer(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void uploadFileToServer(File file) {

        if (Utils.isNetworkAvailable(this)) {

            Utils.showProgressDialog(mContext);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

            HashMap<String,Object> request = new HashMap<>();
            request.put("customerApp",true);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));



            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<ProfilePicResponse> call = apiService.uploadProfilePic(request,filePart,
                    preferences.getCustomerId(),preferences.getAccessToken());

            call.enqueue(new Callback<ProfilePicResponse>() {
                @Override
                public void onResponse(Call<ProfilePicResponse> call, Response<ProfilePicResponse> serverResponse) {
                    try {
                        Utils.hideProgressDialog();
                        Log.d("profile","serverResponse.body()"+serverResponse.body());
                        if(serverResponse.body()!=null){
                            Log.i("uploadpic res",serverResponse.body().toString());
                            if(Integer.parseInt(serverResponse.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                preferences.setProfileImage(serverResponse.body().getImageUrl());
                                if(preferences.getProfileImage()!=null && !preferences.getProfileImage().isEmpty()){
                                    Picasso
                                            .with(UpdateProfileActivity.this)
                                            .load(preferences.getProfileImage())
                                            .into(profile_image);
                                }
                            }else {
                                Utils.showToast(UpdateProfileActivity.this,getResources().getString(R.string.profile_pic_failed));
                            }
                        }
                        else{
                            Log.d("Profile","Response is null");

                        }

                    } catch (Exception e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ProfilePicResponse> call, Throwable throwable) {
                    Utils.hideProgressDialog();
                    Log.e("eroooooooorr", throwable.toString());
                    System.out.print("erro =" + throwable.toString());
                    Utils.showToast(UpdateProfileActivity.this, getResources().getString(R.string.network_slow));

                }
            });
        } else {
            Utils.showToast(mContext, getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
        }
    }
    public Bitmap getResizedBitmap(Bitmap bm) {
        int newWidth = 100;
        int newHeight = 100;
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void netwrokCallForComeToKnowBy(){
        if(Utils.isNetworkAvailable(UpdateProfileActivity.this)){
//            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ComeToKnowBy> getTerms = apiService.getComeToKnow();
            //Log.d("Profile","Progress_Dialog___Show::IN_netwrokCallForComeToKnowBy");
            //Utils.showProgressDialog(UpdateProfileActivity.this);
            getTerms.enqueue(new Callback<ComeToKnowBy>() {
                @Override
                public void onResponse(Call<ComeToKnowBy> call, Response<ComeToKnowBy> response) {

                    try{
                        Utils.hideProgressDialog();
//                        Log.d("Profile","Progress_Dialog___Hide 1::IN_netwrokCallForComeToKnowBy");
                        Log.d("Profile","How did you come to know by Response::: "+
                        response.body().toString());
                        if(response.body()!=null){
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                knowBy = new ArrayList<KnowBy>();
                                KNOW_BY = new ArrayList<String>();
                                knowBy = response.body().getResponseData();
                                preferences.setComeToKnow(knowBy);

                                Log.d("Profile","How did you come to know by Response::: "+
                                        response.body().getResponseData().toString());
                                if(knowBy!=null && knowBy.size()>0){
                                    for(int i = 0 ; i < knowBy.size() ;i++) {
                                        if (knowBy.get(i).getStatus().equalsIgnoreCase("Active")){
                                            KNOW_BY.add((knowBy.get(i).getKnow_by()));
                                        }
                                    }
                                    feed_adapter = new ArrayAdapter<String>(UpdateProfileActivity.this,
                                            R.layout.simple_spinner_feed_text_view, KNOW_BY);
                                    spinner_feed.setAdapter(feed_adapter);
                                }


                            }
                        }
                    }catch (Exception e){
                        Utils.hideProgressDialog();
//                        Log.d("Profile","Progress_Dialog___Hide 2::IN_netwrokCallForComeToKnowBy");
                        e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<ComeToKnowBy> call, Throwable t) {
                    Utils.hideProgressDialog();
//                    Log.d("Profile","Progress_Dialog___Hide 3::IN_netwrokCallForComeToKnowBy");
                    Utils.showToast(UpdateProfileActivity.this,getResources().getString(R.string.network_slow));
                }
            });
        }else{
            Utils.hideProgressDialog();
//            Log.d("Profile","Progress_Dialog___Hide 4::IN_netwrokCallForComeToKnowBy");
            Utils.showToast(UpdateProfileActivity.this,getResources().getString(R.string.network_error));
        }
    }

    private void showAlertDialog(String message) {
        String yes, no;

        yes = getResources().getString(R.string.yes);
        no = getResources().getString(R.string.no);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        donate_blood = "1";

                        checkBox_donate_blood.setChecked(true);

                    }

                });

        builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                donate_blood = "0";
                checkBox_donate_blood.setChecked(false);

            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

}
