package com.esahai.Extras;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.ahmedjazzar.rosetta.LanguageSwitcher;
import com.crittercism.app.Crittercism;
import com.esahai.R;
import com.esahai.Utils.AppObserver;
import com.esahai.preferences.AppPreferences;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.moe.pushlibrary.MoEHelper;
import com.moengage.push.PushManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;


/**
 * Created by ragamai on 17/10/16.
 */
public class EsahaiApplication extends Application {

    private String APP_ID = "19e50a6ba5a7403ab4cb240a804f631900555300";

    private Tracker mTracker;

    private  AppPreferences preferences;
    public static EsahaiApplication myApplication;
    public static Context applicationContext;

    public static final int bookingStatusUpdate = 1;

    private List<AppObserver> mObservers = new ArrayList<>();


    private Locale firstLaunchLocale;
    private HashSet<Locale> supportedLocales;
    public LanguageSwitcher languageSwitcher;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        myApplication = this;
        init();     // To change language
        Crittercism.initialize(getApplicationContext(),APP_ID);
        preferences = new AppPreferences(this);
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        faceBookInitialize();
        //setFonts();

        //MoEngage
        MoEHelper.getInstance(getApplicationContext()).autoIntegrate(this);

        MoEHelper helper = MoEHelper.getInstance(this);

        if( preferences.isNewUser()){
            helper.setExistingUser(false);
            preferences.setNewUser(false);
        }else{
            helper.setExistingUser(true);
        }


    }

    private void init(){
        manualSupportedLocales();

        languageSwitcher = new LanguageSwitcher(this, firstLaunchLocale);
        languageSwitcher.setSupportedLocales(supportedLocales);
    }

    private void manualSupportedLocales() {
        // This is the locale that you wanna your app to launch with.
        firstLaunchLocale = new Locale("en");

        Locale telugu = new Locale("te");
        // You can use a HashSet<String> instead and call 'setSupportedStringLocales()' :)
        supportedLocales = new HashSet<>();
        supportedLocales.add(telugu);
        supportedLocales.add(firstLaunchLocale);
    }

    private void faceBookInitialize(){
        try {
            if(preferences.isProd()) {
                Log.i("is prod","yesssssssssssssssssssssssssssssssssssssssssssss");
                FacebookSdk.sdkInitialize(getApplicationContext());
                AppEventsLogger.activateApp(this);
            }else{
                Log.i("is prod base","------not prod------------------------");
            }
        }catch (Exception e){e.printStackTrace();}
    }

    /*private void setFonts() {
        FontsOverride.setDefaultFont(this, "MONOSPACE", "robotoslab_bold.ttf");
//        FontsOverride.setDefaultFont(this, "MONOSPACE", "MyFontAsset2.ttf");
//        FontsOverride.setDefaultFont(this, "SERIF", "MyFontAsset3.ttf");
//        FontsOverride.setDefaultFont(this, "SANS_SERIF", "MyFontAsset4.ttf");
    }
*/
    public static EsahaiApplication getApplication(){
        return myApplication;
    }


    // analytics

    //Google analytics
    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();
        //Tracker t2 = GoogleAnalytics.getInstance(this).newTracker("UA-69072164-1");
        // Set screen name.
        t.setScreenName(screenName);
        //String dimensionValue = "B081595";
		/*String dimensionValue = "SOME_DIMENSION_VALUE";
		tracker.set(Fields.customDimension(1), dimensionValue);*/
		/*NSString *dimensionValue = @"SOME_DIMENSION_VALUE";
		[tracker set:[GAIFields customDimensionForIndex:1] value:dimensionValue];*/
        //t.setClientId("B081595");
        //Log.d("trackScreenView getTrackerId",""+dbHelper.getTrackerId());
        // t.set("&uid", Common.getRetailerNumber(context));
        t.set("&cd1", preferences.getCustomerId());

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     */
    public void trackEvent(String category, String action, String label,boolean isNonInteraction) {
        Tracker t = getGoogleAnalyticsTracker();
        t.set("&cd1", preferences.getMobileNumber());
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).setNonInteraction(isNonInteraction).build());
    }
    public void trackEvent(String category, String action, String label) {
        trackEvent(category,action,label,true);
    }


    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.app_tracker);
        }
        return mTracker;
    }


    public void notifyObservers(int event, Object arg) {
        for (int i = 0; i < mObservers.size(); i++) {
            Log.i("", "*******************  loop  notifyObservers( count = "+i+"event = " + event
            );
            mObservers.get(i).update(event, arg);
            // break;
        }
    }

    public synchronized void addObserver(AppObserver obs) {
        Log.i("", "addObserver(" + obs + ")");
        if (mObservers.indexOf(obs) < 0) {
            Log.i("", " inside addObserver(" + obs + ")");
            mObservers.add(obs);
        }
    }

    public synchronized void deleteObserver(AppObserver obs) {
        Log.i("", "deleteObserver(" + obs + ")");
        mObservers.remove(obs);
    }


}
