package com.esahai.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.Utils.AppObserver;
import com.esahai.dataObjects.PromoDetails;
import com.esahai.preferences.AppPreferences;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.JsonObject;
import com.moe.pushlibrary.MoEHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ragamai on 6/3/17.
 */
public class CommonActivity extends AppCompatActivity implements AppObserver,LocationListener {

    public static AlertDialog alert = null;
    LocationManager locationManager;
    public static final long MIN_TIME = 1000 * 60;
    public static final float MIN_DISTANCE = 100;
    public static double latitude,longitude;
    AppPreferences preferences;

    private MoEHelper mHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new AppPreferences(CommonActivity.this);
        mHelper = MoEHelper.getInstance(this);
        try{
            getLocation();
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHelper.onResume(this);
        EsahaiApplication.getApplication().addObserver(CommonActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHelper.onStop(this);
        EsahaiApplication.getApplication().deleteObserver(CommonActivity.this);
    }

    private void requestPermissionForLocation() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1338);
    }

    private void getLocation() {
        locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionForLocation();
        } else {
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);


                Location location;
                location = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location == null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                latitude = location.getLatitude();
                longitude = location.getLongitude();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void showDialogForBanner(final String bannerImage, final String title, final String description) {
        new Thread(new Runnable() {
            @Override
            public void run() {

        CommonActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(CommonActivity.this);

                dialog.setContentView(R.layout.banner);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                TextView textView_title = (TextView)dialog.findViewById(R.id.textView_title);
                TextView textView_description = (TextView) dialog.findViewById(R.id.textView_description);
                ImageView imageView_banner = (ImageView) dialog.findViewById(R.id.imageView_banner);
                ImageView close = (ImageView) dialog.findViewById(R.id.imageView_delete);

                textView_title.setText(title);
                textView_description.setText(description);

                if (!bannerImage.equals("")) {
                    String s = "/";
                    bannerImage.replaceAll(s, "");
                    Picasso
                            .with(CommonActivity.this)
                            .load(bannerImage)
                            .into(imageView_banner);
//                    imageView_banner.setImageBitmap(Utils.getBitmapFromUrl(bannerImage));
                }

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    //TODO for prod and qa
                        try {
                            if(preferences.isProd()) {
                                AppEventsLogger logger = AppEventsLogger.newLogger(CommonActivity.this);
                                logger.logEvent("Promo Banner viewed");
                            }
                        }catch (Exception e){e.printStackTrace();}dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });
            }
        }).start();

    }

    public void showAlert( final String message, final String status) {
        new Thread(new Runnable() {
            @Override
            public void run() {

        CommonActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(CommonActivity.this);
                builder.setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if(status.equalsIgnoreCase("logout")){
                                    Intent intent = new Intent(CommonActivity.this, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                                alert.dismiss();
                            }
                        });
                alert = builder.create();
                alert.show();
            }
        });
            }
        }).start();
    }

    @Override
    public void update(int event, Object arg) {
        try {
            if (event == Constants.DISPLAY_NOTIFICATION) {
                PromoDetails promoDetails = (PromoDetails) arg;
                showDialogForBanner(promoDetails.getBanner_path(), promoDetails.getTitle(), promoDetails.getDescription());
            }

            if (event == Constants.BLOOD_REQUEST) {
                PromoDetails promoDetails = (PromoDetails) arg;
                Log.i("blood req", promoDetails.getTitle());
                try {
                    showAlert(promoDetails.getTitle(), "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (event == Constants.LOGOUT) {
                showAlert(getResources().getString(R.string.logout), "logout");
            }
        }catch (Exception e){e.printStackTrace();}
    }


    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mHelper.onRestoreInstanceState(savedInstanceState);
    }

}
