package com.esahai.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.esahai.Extras.Utils;
import com.esahai.R;


/**
 * Created by Dell on 4/21/2017.
 */

public class BloodBankMenuActivity extends CommonActivity implements View.OnClickListener {

    TextView tv_bloodbankmenu_bloodbankdata, tv_bloodbankmenu_bloodbankrequest;
    ImageView imageView_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bloodbankmenu);
        initViews();

    }

    private void initViews() {

        tv_bloodbankmenu_bloodbankdata = (TextView) findViewById(R.id.tv_activity_bloodbankmenu_bloodbankdata);
        tv_bloodbankmenu_bloodbankrequest = (TextView) findViewById(R.id.tv_activity_bloodbankmenu_bloodbankrequest);

        tv_bloodbankmenu_bloodbankdata.setOnClickListener(this);
        tv_bloodbankmenu_bloodbankrequest.setOnClickListener(this);

       /* tv_bloodbankmenu_bloodbankdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.blood_bank));
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utils.closeKeyBoard(BloodBankMenuActivity.this,imageView_back);
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){



            case R.id.tv_activity_bloodbankmenu_bloodbankrequest:
                //Toast.makeText(getApplicationContext(),"Boold Bank request",Toast.LENGTH_LONG).show();

                Intent bloodRequest = new Intent(BloodBankMenuActivity.this,BloodRequestActivity.class);
                startActivity(bloodRequest);

                break;


            case R.id.tv_activity_bloodbankmenu_bloodbankdata:

                Intent viewBloodBanks = new Intent(BloodBankMenuActivity.this,BloodBankActivity.class);
                startActivity(viewBloodBanks);

                // Toast.makeText(getApplicationContext(),"Boold Bank Data",Toast.LENGTH_LONG).show();
                break;

        }

    }
}
