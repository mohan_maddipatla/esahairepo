package com.esahai.dataObjects;

/**
 * Created by ragamai on 12/10/16.
 */
public class Profile {

    public String statusCode;
    public String statusMessage;
    public ProfileDetails responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ProfileDetails getResponseData() {
        return responseData;
    }

    public void setResponseData(ProfileDetails responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }


}
