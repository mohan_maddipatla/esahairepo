package com.esahai.Utils;

import android.app.Application;

import com.crittercism.app.Crittercism;


/**
 * Created by ragamai on 17/10/16.
 */
public class EsahaiApplication extends Application {

    private String APP_ID = "19e50a6ba5a7403ab4cb240a804f631900555300";

    @Override
    public void onCreate() {
        super.onCreate();
        Crittercism.initialize(getApplicationContext(),APP_ID);
    }


}
