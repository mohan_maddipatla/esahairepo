package com.esahai.webServices;

import android.util.Log;

import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.preferences.AppPreferences;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by prashanth on 16/9/16.
 */

public class ApiClient {

    //Local url
//    public static final String BASE_URL = "http://192.168.0.108:3000/";

//    Dev url
//    public static final String BASE_URL = "http://dev.esahai.in/esahai/";

    //dev ios
//        public static final String BASE_URL = "http://dev.esahai.in/esahai-ios/";

    //dev ip url
//    public static final String BASE_URL = "http://54.200.233.193/esahai/";


    //new QA url
//    public static final String BASE_URL ="http://qa.esahai.in/esahai/";

    //prod url
    public static final String BASE_URL = "https://prod.esahai.in/esahai/";

//    public static final String BASE_URL_CONNECT_CALL = "http://122.175.47.248/ConVox3.0/";
    public static final String BASE_URL_CONNECT_CALL = "http://call.esahai.in/ConVox3.0/";


    private static Retrofit retrofit = null;
    private static Retrofit retrofit1 = null;
    private static Retrofit retrofitConnectCall = null;
    private static AppPreferences preferences = new AppPreferences(EsahaiApplication.myApplication);

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getUnsafeOkHttpClient())
                    .build();
            Log.d("retrofit","retro = "+retrofit.baseUrl());

            if(BASE_URL.equalsIgnoreCase("https://prod.esahai.in/esahai/")){
                preferences.setProd(true);
            }
            else{
                preferences.setProd(false);
            }

        }
        return retrofit;
    }

    public static Retrofit getGoogleClient() {
        if (retrofit1==null) {
            retrofit1 = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/maps/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getUnsafeOkHttpClient())
                    .build();
            Log.d("retrofit","retro = "+retrofit1.baseUrl());
        }
        return retrofit1;
    }

    public static Retrofit getClientConnectCall() {
        if (retrofitConnectCall == null) {
            retrofitConnectCall = new Retrofit.Builder()
                    .baseUrl(BASE_URL_CONNECT_CALL)
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getUnsafeOkHttpClient())
                    .build();
            Log.d("retrofit", "retro = " + retrofitConnectCall.baseUrl());
        }
        return retrofitConnectCall;
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            builder.connectTimeout(45,TimeUnit.SECONDS );
            builder.readTimeout(45,TimeUnit.SECONDS);
            builder.writeTimeout(45, TimeUnit.SECONDS);

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
