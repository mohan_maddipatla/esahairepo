package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 21/2/17.
 */
public class StatusListResponse {

    public String statusCode;
    public ArrayList<Status> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ArrayList<Status> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<Status> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "StatusListResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
