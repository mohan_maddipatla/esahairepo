package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 1/11/16.
 */
public class RateCardResponse {

    /*
    "statusCode": "300",
	"statusMessage": "Rate card fetched successfully",
	"responseData"
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<RateCardDetails> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<RateCardDetails> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<RateCardDetails> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "RateCardResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
