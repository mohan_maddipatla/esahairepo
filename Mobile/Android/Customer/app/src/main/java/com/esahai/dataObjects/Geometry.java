package com.esahai.dataObjects;

/**
 * Created by ragamai on 21/10/16.
 */
public class Geometry {
    public Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Geometry{" +
                "location=" + location +
                '}';
    }
}
