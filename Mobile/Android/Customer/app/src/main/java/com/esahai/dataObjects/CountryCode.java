package com.esahai.dataObjects;

/**
 * Created by ragamai on 15/2/17.
 */
public class CountryCode {
    /*
    "country":"US","code":"+1"
     */

    public String country;
    public String code;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "CountryCode{" +
                "country='" + country + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
