package com.esahai.dataObjects;

/**
 * Created by ragamai on 21/2/17.
 */
public class Status {

    /*
    {"id":"1","status":"Accepted","short_code":"ACTD"}
     */

    public String id;
    public String status;
    public String short_code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShort_code() {
        return short_code;
    }

    public void setShort_code(String short_code) {
        this.short_code = short_code;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", short_code='" + short_code + '\'' +
                '}';
    }
}
