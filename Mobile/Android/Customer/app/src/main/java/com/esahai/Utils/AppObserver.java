package com.esahai.Utils;


public interface AppObserver {
	public void update(int event, Object arg);
}
