package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 15/2/17.
 */
public class CountryList {

    public String statusCode;
    public String statusMessage;
    public ArrayList<CountryCode> responseData;


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ArrayList<CountryCode> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<CountryCode> responseData) {
        this.responseData = responseData;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "CountryList{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
