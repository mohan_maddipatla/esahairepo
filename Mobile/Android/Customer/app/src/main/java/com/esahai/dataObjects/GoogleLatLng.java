package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 21/10/16.
 */
public class GoogleLatLng {

    public String status;
    public ArrayList<LocationLatLng> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<LocationLatLng> getResults() {
        return results;
    }

    public void setResults(ArrayList<LocationLatLng> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "GoogleLatLng{" +
                "status='" + status + '\'' +
                ", results=" + results +
                '}';
    }
}
