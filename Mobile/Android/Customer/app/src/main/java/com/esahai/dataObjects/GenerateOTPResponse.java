package com.esahai.dataObjects;

/**
 * Created by ragamai on 29/9/16.
 */
public class GenerateOTPResponse {

    /*
    {
  "statusCode": "300",
  "statusMessage": "Your otp has been sent to registered mobile number",
  "otp": 987412,
  "customerMobile": "9010001269"
}
     */

    private String statusCode;
    private String statusMessage;
    private int otp;
    private String customerMobile;

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        return "GenerateOTPResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", otp=" + otp +
                ", customerMobile='" + customerMobile + '\'' +
                '}';
    }
}
