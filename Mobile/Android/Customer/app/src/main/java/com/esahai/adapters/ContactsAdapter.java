package com.esahai.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.activities.ContactsActivity;
import com.esahai.dataObjects.ContactDetails;
import com.esahai.dataObjects.CountryCode;
import com.esahai.dataObjects.StatusResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 13/10/16.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder>{

    Context context;
    ArrayList<ContactDetails> contactsList;
    RecyclerView recycle_list_emergencies;
    ApiInterface apiInterface;
    AppPreferences preference;

    EditText editText_name;
    EditText editText_contact;
    EditText editText_relation;
    CheckBox emergencyNotifyCheckbox;

    Boolean isNotify;

    Spinner spinner_country_codes;
    ArrayList<CountryCode> countryCodesList;
    ArrayList<String> COUNTRY_CODE;

    OnItemClickListener onItemClickListener;



    public ContactsAdapter(Context context, ArrayList<ContactDetails> contactsList, RecyclerView recycle_list_emergencies
                           ) {
        this.context = context;
        this.contactsList = contactsList;
        this.recycle_list_emergencies = recycle_list_emergencies;


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        preference = new AppPreferences(context);

    }

    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ContactsAdapter.ViewHolder holder, final int position) {
        try {

            holder.text_name.setText(contactsList.get(position).getContact_person());
            if(contactsList.get(position).getMobile_number()!=null){
                String mobile_number = contactsList.get(position).getMobile_number();
                //if(mobile_number.length() == 12){mobile_number = mobile_number.substring(2);}
                //if(mobile_number.length() == 11){mobile_number = mobile_number.substring(1);}
                holder.text_contact.setText(mobile_number);
            }

            holder.text_relation.setText(contactsList.get(position).getRelation());

            holder.button_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    deleteContact(contactsList.get(position).getId());
                    if(onItemClickListener != null){
                        onItemClickListener.onItemClick(position,contactsList.get(position),"delete");
                    }
                }
            });

            holder.button_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //updateContactsAlert(contactsList.get(position));
                    if(onItemClickListener != null){
                        onItemClickListener.onItemClick(position,contactsList.get(position),"update");
                    }
                }
            });


            if(contactsList.get(position).getIs_notify() == 1) {

                holder.ContactNotifyIndicator.setChecked(true);
            }
            else{
                holder.ContactNotifyIndicator.setChecked(false);

            }



        }catch (Exception e){e.printStackTrace();}
    }

    /*private void showAlertDialog(final String contactId){
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getResources().getString(R.string.delete_contact_message))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteContact(contactId);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void deleteContact(String contactId) {
        if(Utils.isNetworkAvailable(context)){
            List<Map<String, String>> list = new ArrayList<Map<String, String>>();
            final HashMap<String,Object> request = new HashMap<>();
            HashMap<String,String> id = new HashMap<>();
            id.put("id",contactId);
            list.add(id);
            request.put("emergecy_contacts",list);
            request.put("customerApp","true");
            request.put("customer_mobile",Utils.encrypt(preference.getMobileNumber()));

            Log.i("req for delete", Arrays.asList(request).toString());

            Call<StatusResponse> deleteContact = apiInterface.deleteEmergencyContact(request,
                    preference.getCustomerId(),preference.getAccessToken());
            deleteContact.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    try{
                        if(response.body()!=null) {
                            Log.i("delete response", response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                if (context instanceof ContactsActivity) {
                                    ((ContactsActivity) context).networkCallForGetContacts();
                                }
                            }
                            else{Utils.showToast(context,context.getResources().getString(R.string.delete_contact_failure));}
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {

                }
            });

        }else{Utils.showToast(context,context.getResources().getString(R.string.network_error));}
    }

    */
    /*private void updateContact(ContactDetails contactDetails) {
        if(Utils.isNetworkAvailable(context)){
            Map<String, Object> contact = new HashMap<String, Object>();
            contact.put("id",contactDetails.getId());
            contact.put("customer_id", preference.getCustomerId());
            contact.put("mobile_number", preference.getCountryCode()+""+editText_contact.getText().toString());
            contact.put("contact_person", editText_name.getText().toString());
            contact.put("relation", editText_relation.getText().toString());
            contact.put("is_notify",isNotify);
            List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
            list.add(contact);
            HashMap<String , Object> request = new HashMap<>();
            request.put("emergecy_contacts",list);
            request.put("customerApp", true);
            request.put("customer_mobile",Utils.encrypt(preference.getMobileNumber()));

            Log.i("req for update", Arrays.asList(request).toString());

            Call<StatusResponse> updateContact= apiInterface.updateEmergencyContact(request,
                    preference.getCustomerId(),preference.getAccessToken());
            updateContact.enqueue(new Callback<StatusResponse>() {
                @Override
                public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                    try{
                        if(response.body()!=null) {
                            Log.i("update response", response.body().toString());
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                if (context instanceof ContactsActivity) {
                                    ((ContactsActivity) context).networkCallForGetContacts();
                                }
                            }
                            else{Utils.showToast(context,context.getResources().getString(R.string.update_contact_failure));}
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<StatusResponse> call, Throwable t) {

                }
            });

        }else{Utils.showToast(context,context.getResources().getString(R.string.network_error));}
    }

    private void updateContactsAlert(final ContactDetails contactDetails) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("Update Contact");
        final FrameLayout frameView = new FrameLayout(context);
        builder.setView(frameView);

        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.add_contact, frameView);
        Button button_submit = (Button)dialoglayout.findViewById(R.id.button_submit);
        Button button_cancel = (Button)dialoglayout.findViewById(R.id.button_cancel);
        editText_name = (EditText)dialoglayout.findViewById(R.id.editText_name);
        editText_contact = (EditText)dialoglayout.findViewById(R.id.editText_contact);
        editText_relation = (EditText)dialoglayout.findViewById(R.id.editText_relation);

        emergencyNotifyCheckbox = (CheckBox)dialoglayout.findViewById(R.id.check_box);

        spinner_country_codes = (Spinner) dialoglayout.findViewById(R.id.spinner_country_code);


        button_submit.setVisibility(View.VISIBLE);
        button_cancel.setVisibility(View.VISIBLE);



        editText_name.setText(contactDetails.getContact_person());
        if(contactDetails.getMobile_number()!=null){
         String mobile_number = contactDetails.getMobile_number();
            if(mobile_number.length() == 12){mobile_number = mobile_number.substring(2);}
            if(mobile_number.length() == 11){mobile_number = mobile_number.substring(1);}
            editText_contact.setText(mobile_number);
        }

        editText_relation.setText(contactDetails.getRelation());

        if(contactDetails.getIs_notify() == 1) {

            emergencyNotifyCheckbox.setChecked(true);
        }
        else{
            emergencyNotifyCheckbox.setChecked(false);

        }

       /*

        Getting the countrycodes from sharedPreferences and setting them in
        the dialog.

         COUNTRY_CODE = new ArrayList<String>();

        int defaultSelectoin = 0;


        if(preference.getAllCountryCodes()!= null &&
                preference.getAllCountryCodes().size() > 0) {
            countryCodesList = preference.getAllCountryCodes();


            for (int i = 0; i < countryCodesList.size(); i++) {
                COUNTRY_CODE.add(countryCodesList.get(i).getCode());
            }



            for (int i = 0; i < COUNTRY_CODE.size(); i++) {
                if (COUNTRY_CODE.get(i).equalsIgnoreCase("+91")) {
                    defaultSelectoin = i;
                    break;
                }
            }
        }else{
            COUNTRY_CODE.add("+91");


        }

        spinner_country_codes.setSelection(defaultSelectoin);
        //Log.d("Contacts","defaultSelectoin::"+defaultSelectoin);
        spinner_country_codes.setSelection(1);


        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(emergencyNotifyCheckbox.isChecked()){
                    isNotify = true;
                }
                else{
                    isNotify = false;
                }



                if(!validateName()){
                    return;
                }
                if(!validateMobileNumber()){
                    return;
                }
                if(!validateRelation()){
                    return;
                }

                updateContact(contactDetails);
                alertDialog.dismiss();


            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    */


    private boolean validateName(){
        if(editText_name.getText().toString().length() == 0 ){

            editText_name.requestFocus();
            editText_name.setError(context.getResources().getString(R.string.valid_name));
            return false;
        }
        else{
        }
        return true;
    }

    private boolean validateMobileNumber(){
        if(editText_contact.getText().toString().length() < 10 ){
            editText_contact.requestFocus();
            editText_contact.setError(context.getResources().getString(R.string.valid_mobile_number));
            return false;
        }
        else{
            if(editText_contact.getText().toString().equalsIgnoreCase("0000000000")){
                editText_contact.requestFocus();
                editText_contact.setError(context.getResources().getString(R.string.valid_mobile_number));
                return false;
            }
        }
        return true;
    }

    private boolean validateRelation(){
        if(editText_relation.getText().toString().length() == 0 ){
            editText_relation.requestFocus();
            editText_relation.setError(context.getResources().getString(R.string.valid_relation));
            return false;
        }
        else{
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView text_name,text_contact,text_relation;
        ImageView button_update,button_delete;
        CheckBox ContactNotifyIndicator;

        public ViewHolder(View view) {
            super(view);
            text_name = (TextView)view.findViewById(R.id.textView_name);
            text_contact = (TextView)view.findViewById(R.id.textView_contact);
            text_relation = (TextView)view.findViewById(R.id.textView_relation);

            button_update = (ImageView) view.findViewById(R.id.button_update);
            button_delete = (ImageView) view.findViewById(R.id.button_delete);

            ContactNotifyIndicator = (CheckBox) view.findViewById(R.id.check_box_adptr);
        }

    }

    public void setOnItemClickListener( OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
    public interface OnItemClickListener {
        public void onItemClick(int position , ContactDetails contactDetails,String type);
    }

}
