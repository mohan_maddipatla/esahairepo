package com.esahai.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.adapters.SearchHospitalsAdapter;
import com.esahai.customVIews.CustomAutoCompleteTextView;
import com.esahai.dataObjects.GoogleAddress;
import com.esahai.dataObjects.GoogleLatLng;
import com.esahai.dataObjects.HospitalAttributes;
import com.esahai.dataObjects.HospitalDetails;
import com.esahai.dataObjects.HospitalList;
import com.esahai.parsers.DirectionJSONParser;
import com.esahai.parsers.PlaceJSONParser;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 27/1/17.
 */
public class NearByHospitalsActivity extends CommonActivity implements LocationListener, OnMapReadyCallback,View.OnClickListener {

    RecyclerView recycle_list_hospitals;
    RecyclerView.LayoutManager recyclerViewLayoutManager;
    double latitude, longitude;
    AppPreferences preferences;
    ApiInterface apiInterface,googleInterface;
    LocationManager locationManager;
    public static final long MIN_TIME = 1000 * 60;
    public static final float MIN_DISTANCE = 100;
    static GoogleMap map;
    RecyclerView.Adapter hospitalsAdapter;
    MapFragment mapFragment;
    TextView textView_mapView, textView_listView;
    FrameLayout layout_map;
    static double hospital_lat, hospital_longi;
    ImageView button_navigate;
    Boolean cameraMovedProgrammatically;
    ImageView image_marker;
    CustomAutoCompleteTextView autocomplete_text_places;
    ImageView image_search, image_delete, image_mylocation;
    PlacesTask placesTask;
    ParserTaskForPlaces parserTask;
    LinearLayout layout_search;

    Location location;
    ArrayAdapter adapter;
    boolean isShowing = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_nearby_hospitals);

        preferences = new AppPreferences(NearByHospitalsActivity.this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        googleInterface = ApiClient.getGoogleClient().create(ApiInterface.class);

        initViews();

    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.nearest_hospitals));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        recycle_list_hospitals = (RecyclerView) findViewById(R.id.recycle_list_hospitals);
        recyclerViewLayoutManager = new LinearLayoutManager(NearByHospitalsActivity.this);

        textView_listView = (TextView) findViewById(R.id.textView_listView);
        textView_mapView = (TextView) findViewById(R.id.textView_mapView);
        layout_map = (FrameLayout) findViewById(R.id.layout_map);
        button_navigate = (ImageView) findViewById(R.id.button_navigate);
        recycle_list_hospitals.setLayoutManager(recyclerViewLayoutManager);
        image_marker = (ImageView) findViewById(R.id.image_marker);

        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(NearByHospitalsActivity.this);

        autocomplete_text_places = (CustomAutoCompleteTextView) findViewById(R.id.autocomplete_text_places);
        autocomplete_text_places.setImeActionLabel("Done", EditorInfo.IME_ACTION_DONE);
        autocomplete_text_places.setThreshold(1);
        image_delete = (ImageView) findViewById(R.id.image_delete);
        image_search = (ImageView) findViewById(R.id.image_search);

        layout_search = (LinearLayout)findViewById(R.id.layout_search);
        image_mylocation = (ImageView) findViewById(R.id.image_mylocation);


        image_delete.setOnClickListener(this);
        image_search.setOnClickListener(this);
        image_mylocation.setOnClickListener(this);


        autocomplete_text_places.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                image_search.performClick();

            }
        });

        autocomplete_text_places.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Utils.closeKeyBoard(NearByHospitalsActivity.this, autocomplete_text_places);
                    //image_search.setVisibility(View.GONE);
                    //image_delete.setVisibility(View.VISIBLE);
                    String address = autocomplete_text_places.getText().toString();
                    networkCallForConvertAddress(address);
                    return true;
                }
                return false;
            }
        });

        autocomplete_text_places.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 30) {
                    if(Utils.isNetworkAvailable(NearByHospitalsActivity.this)) {
                        placesTask = new PlacesTask();
                        placesTask.execute(s.toString().trim());
                        Log.i("coming", "yes");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i("getting into", "yes");
            }
        });


        textView_listView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                networkCallForHospitals();
                recycle_list_hospitals.setVisibility(View.VISIBLE);
                layout_map.setVisibility(View.GONE);
                button_navigate.setVisibility(View.GONE);
            }
        });

        textView_mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    map.clear();
                    image_marker.setVisibility(View.INVISIBLE);
                    mapFragment.getMapAsync(NearByHospitalsActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                layout_search.setVisibility(View.VISIBLE);
                recycle_list_hospitals.setVisibility(View.GONE);
                layout_map.setVisibility(View.VISIBLE);
                button_navigate.setVisibility(View.GONE);
            }
        });

        button_navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (hospital_lat != 0.0 && hospital_longi != 0.0) {
                        Log.i("hosp", hospital_lat + " , " + hospital_longi);
                        Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?saddr=" + latitude + "," + longitude + "&daddr=" + hospital_lat + "," + hospital_longi);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    ArrayList<HospitalDetails> hospitals;

    private void networkCallForHospitals() {
        Utils.hideProgressDialog();
        HashMap<String, Object> locationDetails = new HashMap<>();
        Location location;
        location = new Location("");

        try {
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            Log.i("location", latitude + "," + longitude + "-->" + location.getLatitude() + "," + location.getLongitude());
        } catch (Exception e) {
            Utils.hideProgressDialog();
            e.printStackTrace();
        }

        locationDetails.put("emergencyGeoHash", Utils.getGeoHashCode(location));
        locationDetails.put("latitude", latitude);
        locationDetails.put("longitude", longitude);
        locationDetails.put("groupType", "2");
        locationDetails.put("customerApp", true);
        locationDetails.put("ambulanceApp", false);
//        locationDetails.put("emergency_type", preferences.getMedicalTaxiId());
        locationDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));
        hospitals = new ArrayList<HospitalDetails>();
        Log.i("location details", Arrays.asList(locationDetails).toString());
        Utils.showProgressDialog(NearByHospitalsActivity.this);
        if (Utils.isNetworkAvailable(NearByHospitalsActivity.this)) {
            Call<HospitalList> getHospitalsList = apiInterface.getNearByHospitalsList(locationDetails, preferences.getCustomerId(), preferences.getAccessToken());
//            Utils.showProgressDialog(NearByHospitalsActivity.this);
            getHospitalsList.enqueue(new Callback<HospitalList>() {
                @Override
                public void onResponse(Call<HospitalList> call, Response<HospitalList> response) {
                    Utils.hideProgressDialog();
                    try {
                        if (response.body() != null) {
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                Log.i("hospitals list", response.body().toString());


                                hospitals = response.body().getResponseData();

                                if(hospitals!=null) {
                                    for (int i = 0; i < hospitals.size(); i++) {

                                        createMarker(Double.parseDouble(hospitals.get(i).getGroup_latitude()),
                                                Double.parseDouble(hospitals.get(i).getGroup_longitude()),
                                                hospitals.get(i).getGroup_short_name(),
                                                hospitals.get(i).getGroup_id(),hospitals.get(i).getAttributes(),true);
                                    }
                                    hospitalsAdapter = new SearchHospitalsAdapter(NearByHospitalsActivity.this, hospitals, recycle_list_hospitals);
                                    recycle_list_hospitals.setAdapter(hospitalsAdapter);
                                }else{
                                    hospitals.clear();
                                    hospitals.add(noHospitals());
                                    hospitalsAdapter = new SearchHospitalsAdapter(NearByHospitalsActivity.this, hospitals, recycle_list_hospitals);
                                    recycle_list_hospitals.setAdapter(hospitalsAdapter);
                                    showAlertDialog(getResources().getString(R.string.no_hospitals), true);
                                }

                            } else {
                                hospitals.clear();
                                hospitals.add(noHospitals());
                                hospitalsAdapter = new SearchHospitalsAdapter(NearByHospitalsActivity.this, hospitals, recycle_list_hospitals);
                                recycle_list_hospitals.setAdapter(hospitalsAdapter);
                                showAlertDialog(getResources().getString(R.string.no_hospitals), true);
                            }
                        } else {
                            hospitals.clear();
                            hospitals.add(noHospitals());
                            hospitalsAdapter = new SearchHospitalsAdapter(NearByHospitalsActivity.this, hospitals, recycle_list_hospitals);
                            recycle_list_hospitals.setAdapter(hospitalsAdapter);
                            showAlertDialog(getResources().getString(R.string.no_hospitals), true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }

                }

                @Override
                public void onFailure(Call<HospitalList> call, Throwable t) {
                    Log.i("failure",t.toString());
                    Utils.hideProgressDialog();
                    Utils.showToast(NearByHospitalsActivity.this, getResources().getString(R.string.network_slow));

                }
            });
        } else {

            Utils.showToast(NearByHospitalsActivity.this, getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
        }

    }

    private HospitalDetails noHospitals() {
        HospitalDetails hospitalDetails = new HospitalDetails();
        hospitalDetails.group_name = getResources().getString(R.string.no_nearby_hospitals);
        hospitalDetails.distance = "";
        return hospitalDetails;
    }


    private void requestPermissionForLocation() {
        ActivityCompat.requestPermissions(
                NearByHospitalsActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1338);
    }

    HospitalDetails hospDetails;
    protected void createMarker(double hLatitude, double hLongitude, String title,
                                String id, ArrayList<HospitalAttributes> attributes,final boolean isShow) {
        Log.i("marker latlng", hLatitude + " , " + hLongitude);
        LatLng hlatlng = new LatLng(hLatitude, hLongitude);

        HospitalDetails hospitalDetails = new HospitalDetails();
        hospitalDetails.group_name = title;
        hospitalDetails.group_latitude = String.valueOf(hLatitude);
        hospitalDetails.group_longitude = String.valueOf(hLongitude);
        hospitalDetails.group_id = id;
        hospitalDetails.attributes = attributes;

        MarkerOptions tp = new MarkerOptions()
                .position(hlatlng)
                .title(title)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.hosp_map));
        final Marker marker = map.addMarker(tp);
        marker.setTag(hospitalDetails);

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View view = getLayoutInflater().inflate(R.layout.custom_map_view, null);
//                view.setLayoutParams(new LinearLayout.LayoutParams(
//                        ViewGroup.LayoutParams.WRAP_CONTENT,
//                        ViewGroup.LayoutParams.WRAP_CONTENT));
                TextView textView_select = (TextView) view.findViewById(R.id.textView_select);
                TextView textView_hospital_name = (TextView)view.findViewById(R.id.textView_hospital_name);
                ListView list_attributes = (ListView) view.findViewById(R.id.list_attributes);
                try {

                    if (isShow) {
                        textView_select.setVisibility(View.VISIBLE);
                    } else {
                        textView_select.setVisibility(View.GONE);
                    }

                    hospDetails = (HospitalDetails) marker.getTag();
                    if (hospDetails != null) {
                        textView_hospital_name.setText(hospDetails.getGroup_name());
                        ArrayList<HospitalAttributes> hospitalAttributes = hospDetails.getAttributes();
                        ArrayList<String> attribute = new ArrayList<String>();
                        if (hospitalAttributes != null && hospitalAttributes.size() > 0) {
                            for (int i = 0; i < hospitalAttributes.size(); i++) {
                                attribute.add(hospitalAttributes.get(i).getAttribute_name()+" : "+
                                        hospitalAttributes.get(i).getAttribute_details());
                            }
                            adapter = new ArrayAdapter<String>(NearByHospitalsActivity.this,R.layout.list_item,attribute);
                            list_attributes.setAdapter(adapter);
                            setListViewHeightBasedOnChildren(list_attributes);
                        }
                    }
                }catch (Exception e){e.printStackTrace();}

                return view;
            }
        });

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                mapForSelectedHospital((HospitalDetails) marker.getTag());
            }
        });



    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ArrayAdapter listAdapter = (ArrayAdapter) listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {

//            latitude = 0.0;
//            longitude = 0.0;


            if (hospital_lat != 0.0 && hospital_longi != 0.0) {
                button_navigate.setVisibility(View.GONE);
            } else {
                button_navigate.setVisibility(View.VISIBLE);
            }


            map = googleMap;
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            image_marker.setVisibility(View.VISIBLE);
            if (!Utils.isGPSOn(NearByHospitalsActivity.this)) {
                Utils.turnOnGPS(NearByHospitalsActivity.this);
            } else {

                LocationManager locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                        (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                    requestPermissionForLocation();
                } else {
//                Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
                    map.getUiSettings().setZoomGesturesEnabled(true);

                    //map.getUiSettings().setMyLocationButtonEnabled(false);

                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, NearByHospitalsActivity.this);
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, NearByHospitalsActivity.this);
//                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, NearByHospitalsActivity.this);


                    location = locationManager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location == null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }

                    if(latitude == 0.0 && longitude == 0.0) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
                    LatLng latLng = new LatLng(latitude, longitude);

//                    cameraMovedProgrammatically = true;
                    Log.d("Marker_Location", "Lat" + latitude + "Long" + longitude);
//                map.addMarker(new MarkerOptions().position(latLng));
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng)      // Sets the center of the map to Mountain View
                            .zoom(12)                   // Sets the zoom
                            .bearing(90)                // Sets the orientation of the camera to east
//                            .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    //map.setMyLocationEnabled(false);

                    map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            LatLng cameraLatLng = cameraPosition.target;
                            try {
                                map.clear();

                                latitude = cameraLatLng.latitude;
                                longitude = cameraLatLng.longitude;
                                if (ActivityCompat.checkSelfPermission(NearByHospitalsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(NearByHospitalsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.
                                    return;
                                }
                                CircleOptions circleOptions = new CircleOptions()
                                        .center(new LatLng(latitude,longitude))
                                        .radius(5000)
                                        .strokeColor(getResources().getColor(android.R.color.transparent))
                                        .fillColor(getResources().getColor(R.color.map_blue));

                                map.addCircle(circleOptions);
                                //map.setMyLocationEnabled(false);
                                autocomplete_text_places.setText(networkCallForLatLng(latitude, longitude));

                            networkCallForHospitals();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


            }
        }
    }

    catch( Exception e) {
        e.printStackTrace();
    }



    }


    String address;
    private String networkCallForLatLng(final double latitude, final double longitude){
        if(Utils.isNetworkAvailable(NearByHospitalsActivity.this)){
            Call<GoogleAddress> getLatLng = googleInterface.getAddress(latitude+","+longitude,
                    getResources().getString(R.string.geo_coding_key));
            Utils.hideProgressDialog();
            Utils.showProgressDialog(NearByHospitalsActivity.this);
            Log.d("latLng response","starting progress dialog");
            getLatLng.enqueue(new Callback<GoogleAddress>() {
                @Override
                public void onResponse(Call<GoogleAddress> call, Response<GoogleAddress> response) {
                    Utils.hideProgressDialog();
                    Log.d("latLng response","Stopping progress dialog");
                    try{
                        Log.i("latLng response", "adreessssssss"+response.body().toString());

                        if(response.body().getStatus().equalsIgnoreCase("OK")) {
                            Log.i("latLng response", "adreessssssss"+response.body().toString());


                            address = response.body().getResults().get(0).getFormatted_address();

                        }
                        else{
                            address = latitude+","+longitude;
                        }
                        autocomplete_text_places.setText(address);
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<GoogleAddress> call, Throwable t) {
                    t.printStackTrace();
                    Utils.hideProgressDialog();
                    Utils.showToast(NearByHospitalsActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        }else{
            Utils.showToast(NearByHospitalsActivity.this,getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
            Log.d("latLng response","Stopping progress dialog_IN ELSE");
        }

        return address;
    }

    private void setPolyLine() {
        LatLng origin = new LatLng(latitude,longitude);
        LatLng dest = new LatLng(hospital_lat,hospital_longi);

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }
    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Track Ambulance", e.toString());
        }finally{
            if(iStream!=null) {
                iStream.close();
            }
            if(urlConnection!=null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }

    private void networkCallForConvertAddress(String address) {
        Log.d("address 8", "address" + address);
        if (address != null) {
            if (Utils.isNetworkAvailable(this)) {

                Log.i("eSahai", "Internet is on");

                Call<GoogleLatLng> convertAddress = googleInterface.getLatLng(address,
                        getResources().getString(R.string.geo_coding_key));

                if (convertAddress.toString() != null) {
                    Log.i("eSahai", "convertAddress:::" + convertAddress.toString());
                }

                convertAddress.enqueue(new Callback<GoogleLatLng>() {
                    @Override
                    public void onResponse(Call<GoogleLatLng> call, Response<GoogleLatLng> response) {
                        try {

                            Log.i("eSahai", "IN on_response");

                            if (response.body() != null && response.body().getStatus().equals("OK")) {
                                Log.i("address res", response.body().toString());
                                double latitude = response.body().getResults().get(0).getGeometry().getLocation().getLat();
                                double longitude = response.body().getResults().get(0).getGeometry().getLocation().getLng();

                                Log.i("address latlng", latitude + " , " + longitude);
                                LatLng latLng = new LatLng(latitude, longitude);
                                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                map.animateCamera(CameraUpdateFactory.zoomTo(12));
                            } else {
                                //  Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<GoogleLatLng> call, Throwable t) {

                        Utils.showToast(NearByHospitalsActivity.this, getResources().getString(R.string.network_slow));

                    }
                });

            } else {
                Utils.showToast(NearByHospitalsActivity.this, getResources().getString(R.string.network_error));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_delete:
                autocomplete_text_places.setText("");
                break;
            case R.id.image_search:
                String address = autocomplete_text_places.getText().toString();
                networkCallForConvertAddress(address);
                break;

            case R.id.image_mylocation:
                try {
                    map.clear();
//                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new
//                        LatLng(location.getLatitude(),
//                        location.getLongitude()), 15));
                    //autocomplete_text_places.setText("");
                    autocomplete_text_places.setText(networkCallForLatLng(location.getLatitude(), location.getLongitude()));
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    //map.addMarker(new MarkerOptions().position(latLng));
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    map.animateCamera(CameraUpdateFactory.zoomTo(12));
//                    map.getUiSettings().setMyLocationButtonEnabled(false);

//                    map.clear();
//                    image_marker.setVisibility(View.INVISIBLE);
//                    mapFragment.getMapAsync(NearByHospitalsActivity.this);


                } catch (Exception e) {
                    e.printStackTrace();
                }

                layout_search.setVisibility(View.VISIBLE);
                recycle_list_hospitals.setVisibility(View.GONE);
                layout_map.setVisibility(View.VISIBLE);
                button_navigate.setVisibility(View.GONE);
                break;

            default:
                break;
        }
        Utils.closeKeyBoard(NearByHospitalsActivity.this, autocomplete_text_places);
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service

            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(Utils.isNetworkAvailable(NearByHospitalsActivity.this)) {

                ParserTask parserTask = new ParserTask();

                // Invokes the thread for parsing the JSON data
                parserTask.execute(result);
            }
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTaskForPlaces extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                Log.d("address","Address_Response:::"+jsonData[0]);

                //Log.d("address","Address_Response___1:::"+jsonData[1]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

                Log.d("address","Places_Response:::"+places);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            try {

                Log.d("result", "result_onPostExecute = " + result);
                String[] from = new String[]{"description"};
                int[] to = new int[]{android.R.id.text1};

                ArrayList<String> resultList = new ArrayList<String>();

                if (resultList != null && result!=null) {

                    for (HashMap<String, String> map : result) {
                        resultList.addAll(map.values());
                    }


                    // Creating a SimpleAdapter for the AutoCompleteTextView
                    SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

                    // Setting the adapter
                    autocomplete_text_places.setAdapter(adapter);

                    adapter.notifyDataSetChanged();
                }


            }catch (Exception e){e.printStackTrace();}
        }
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            //String key = "key="+API_KEY;

            String key = "key=" + getResources().getString(R.string.geo_coding_key);

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "UTF-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            //String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters + "&maptype=roadmap";


            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + key+"&"+input;

//
// System.out.print("places url--->"+url);
            Log.i("places url *****", url);



            try {

                data = downloadUrl(url);
                Log.d("data","data = "+data);
            }


            catch (Exception e) {
                Log.d("Background Task", e.toString());
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(Utils.isNetworkAvailable(NearByHospitalsActivity.this)) {

                Log.d("result", "placesssss = " + result);
                // Creating ParserTask


                parserTask = new ParserTaskForPlaces();

                // Starting Parsing the JSON string returned by Web Service
                parserTask.execute(result);
            }
        }
    }


    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            try {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    LatLng position = null;
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        position = new LatLng(lat, lng);

                        points.add(position);
                        builder.include(position);
                    }


                    LatLngBounds bounds = builder.build();
                    int padding = 45; // offset from edges of the map in pixels

                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                map.animateCamera(cu);
//                    LatLng latlng = new LatLng(latitude, longitude);
//                    map.moveCamera(CameraUpdateFactory.newLatLng(latlng));
//                    map.animateCamera(CameraUpdateFactory.zoomTo(15));

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(8);
                    lineOptions.color(Color.RED);

                }

                // Drawing polyline in the Google Map for the i-th route
                if(lineOptions!=null) {
                    map.addPolyline(lineOptions);
                }
            }catch (Exception e){e.printStackTrace();}

//            moveToBounds(lineOptions);
        }
    }

    public void mapForSelectedHospital(HospitalDetails hospitalDetails){

        try {
            if (hospitalDetails != null && !hospitalDetails.group_name.equalsIgnoreCase(getResources().getString(R.string.no_nearby_hospitals))) {
                map.clear();
                image_marker.setVisibility(View.INVISIBLE);
                layout_search.setVisibility(View.GONE);
                hospital_lat = Double.parseDouble(hospitalDetails.getGroup_latitude());
                hospital_longi = Double.parseDouble(hospitalDetails.getGroup_longitude());
                if (hospital_lat != 0.0 && hospital_longi != 0.0) {
                    LatLng hosp_latLng = new LatLng(hospital_lat, hospital_longi);
                    LatLng loc_latLng = new LatLng(latitude, longitude);

                    recycle_list_hospitals.setVisibility(View.GONE);
                    layout_map.setVisibility(View.VISIBLE);
                    button_navigate.setVisibility(View.VISIBLE);
                    map.setOnCameraChangeListener(null);
                    map.addMarker(new MarkerOptions().position(loc_latLng));
                    map.moveCamera(CameraUpdateFactory.newLatLng(loc_latLng));
                    map.animateCamera(CameraUpdateFactory.zoomTo(15));
                    createMarker(hospital_lat, hospital_longi, hospitalDetails.getGroup_name(),
                            hospitalDetails.getGroup_id(), hospitalDetails.getAttributes(),false);
                    setPolyLine();
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void showAlertDialog(String message, final boolean isNetwork) {
        if(!isShowing) {
            isShowing = true;
            String yes, no;
            if (isNetwork) {
                yes = getResources().getString(R.string.yes);
                no = getResources().getString(R.string.no);
                Log.d("Dialog", "Dialog_IF set to : " + isNetwork);
            } else {
                yes = "Connect to Customer service";
                no = "Try other options ";
                Log.d("Dialog", "Dialog_ELSE set to : " + isNetwork);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(NearByHospitalsActivity.this);
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (ActivityCompat.checkSelfPermission(NearByHospitalsActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                                if (preferences.getCustomerCareNumber() != null) {

                                    Log.d("CustomerCare", "Number IS::::" + preferences.getCustomerCareNumber());
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + preferences.getCustomerCareNumber()));
                                    startActivity(intent);

                                } else {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + 0));
                                    startActivity(intent);
                                }
                                dialog.cancel();
                                isShowing = false;
                            } else {
                                requestPermissionForCall();
//                    Toast.makeText(BookingConfirmedActivity.this,"Permission denied",Toast.LENGTH_SHORT).show();
                            }
                        }


                    });
            if (isNetwork) {
                builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        isShowing = false;
                    }
                });
            }
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(NearByHospitalsActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }


}
