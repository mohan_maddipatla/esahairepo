package com.esahai.dataObjects;

/**
 * Created by ragamai on 2/3/17.
 */
public class PromoDetails {
    /*
    {
      "offerid": "1",
      "banner_path": "http://dev.esahai.in/promoBanners/banner-1488364088812.jpeg",
      "created_date": "2017-03-01 15:58:08",
      "updated_date": null,
      "title": "Promo",
      "description": "DEv",
      "start_date": "2017-02-28 00:00:00",
      "end_date": "2017-03-07 00:00:00",
      "status": "Active"
    },

     */

    public String offerid;
    public String banner_path;
    public String created_date;
    public String updated_date;
    public String title;
    public String description;
    public String start_date;
    public String end_date;
    public String status;

    public String getOfferid() {
        return offerid;
    }

    public void setOfferid(String offerid) {
        this.offerid = offerid;
    }

    public String getBanner_path() {
        return banner_path;
    }

    public void setBanner_path(String banner_path) {
        this.banner_path = banner_path;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PromoDetails{" +
                "offerid='" + offerid + '\'' +
                ", banner_path='" + banner_path + '\'' +
                ", created_date='" + created_date + '\'' +
                ", updated_date='" + updated_date + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
