package com.esahai.Extras;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.BuildConfig;
import com.esahai.R;
import com.esahai.Utils.*;
import com.esahai.dataObjects.AmbulanceType;
import com.esahai.dataObjects.EmergencyType;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import ch.hsr.geohash.GeoHash;

/**
 * Created by ragamai on 20/9/16.
 */
public class Utils {

    private static ProgressDialog progressDialog;
    private static LatLng latLng;
    private static String ambulance_status;
    private static boolean canCancel = true;
    private static int PLAY_SERVICES_RESOLUTION_REQUEST = 2404;
    private static ArrayList<EmergencyType> emergencies;
    private static ArrayList<AmbulanceType> ambulanceTypes;
//    static boolean isProd;
    private static final HashMap<String, String> map = new HashMap<>();


    public static void closeKeyBoard(Context context, View view){
        try {
            InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    //*****NEW ENCRYPTION CODE*****

    public static String encrypt(String stringToEncode) {

        String keyString = "@*%$!eSaHai";

        try {
            SecretKeySpec skeySpec = getKey(keyString);
            byte[] clearText = stringToEncode.getBytes("UTF8");

            // IMPORTANT TO GET SAME RESULTS ON iOS and ANDROID
            final byte[] iv = new byte[16];
            Arrays.fill(iv, (byte) 0x00);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            // Cipher is not thread safe
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);

            String encrypedValue = Base64.encodeToString(cipher.doFinal(clearText), Base64.DEFAULT);
            Log.d("jacek", "Encrypted: " + stringToEncode + " -> " + encrypedValue);
            return encrypedValue;

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return "";


    }
    private static SecretKeySpec getKey(String password) throws UnsupportedEncodingException {

        // You can change it to 128 if you wish
        int keyLength = 256;
        byte[] keyBytes = new byte[keyLength / 8];
        // explicitly fill with zeros
        Arrays.fill(keyBytes, (byte) 0x0);

        // if password is shorter then key length, it will be zero-padded
        // to key length
        byte[] passwordBytes = password.getBytes("UTF-8");
        int length = passwordBytes.length < keyBytes.length ? passwordBytes.length : keyBytes.length;
        System.arraycopy(passwordBytes, 0, keyBytes, 0, length);
        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
        return key;
    }








    //*****OLD ENCRYPTION CODE*****

    /*public static String encrypt(String userName) {

        String password = "@*%$!eSaHai";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();

            byte[] salt = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};

            SecretKeyFactory factory =
                    SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");


    KeySpec spec = new PBEKeySpec(Base64.encodeToString(digest, Base64.NO_WRAP).toCharArray(), salt, 1000, 256 + 128);
    SecretKey secretKey = factory.generateSecret(spec);
    //SecretKeySpec secretKeySpec = new SecretKeySpec(digest, "AES");
    byte[] data = secretKey.getEncoded();
    byte[] keyBytes = new byte[256 / 8];
    byte[] ivBytes = new byte[128 / 8];

    System.arraycopy(data, 0, keyBytes, 0, 256 / 8);
    System.arraycopy(data, 0, ivBytes, 0, 128 / 8);

    IvParameterSpec iv = new IvParameterSpec(ivBytes);
    SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

    byte[] encrypted = cipher.doFinal(userName.getBytes());

    return Base64.encodeToString(encrypted, Base64.NO_WRAP);
} catch (Exception ex) {
        ex.printStackTrace();
        }

        return null;
        }*/


    public static String getGeoHashCode(Location location){

        GeoHash st = GeoHash.withCharacterPrecision(location.getLatitude(), location.getLongitude(), Constants.locationPrecisionNumber);
        return st.toBase32();
    }

    public static String getIMEINumber(Context context){
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public static int getAppVersion(){
        return BuildConfig.VERSION_CODE;
    }

    public static String getAppVersionName(){
        return BuildConfig.VERSION_NAME;
    }

    public static void showProgressDialog(Context context){
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("loading");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }catch (Exception e){e.printStackTrace();}
    }

    public static void hideProgressDialog(){
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }catch (Exception e){e.printStackTrace();}

    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model.toUpperCase();
        } else {
            return manufacturer.toUpperCase() + " " + model;
        }
    }

    public static String getOSVersion() {
        String versionRelease = Build.VERSION.RELEASE;

        return versionRelease;

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public static boolean isPlayServicesAvailable(Activity context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                //Log.i("", "This device is not supported.");
//                finish();
            }
            return false;
        }
        return true;
    }


    public static AlertDialog alert = null;
    public static void showAlert(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        alert.dismiss();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    public static boolean isGPSOn(final Context context){
        boolean gps_enabled = false;
        boolean network_enabled = false;

        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return  gps_enabled || network_enabled;

    }

    public static AlertDialog.Builder dialog;
    public static void turnOnGPS(final Context context){
            // notify user
            dialog = new AlertDialog.Builder(context);
            dialog.setMessage("GPS not enabled");
        dialog.setCancelable(false);
            dialog.setPositiveButton("Turn on GPS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    dialog.create().dismiss();
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                    //get gps
                }
            });
            /*dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    dialog.create().dismiss();

                }
            });*/
            dialog.show();
        }


    public static void updateLatLng(double latitude, double longitude){

         latLng = new LatLng(latitude,longitude);

    }


    public static LatLng getLatLng(){
        return latLng;
    }


    public static void setAmbulanceStatus(String status){
        ambulance_status = status;
    }

    public static String getAmbulanceStatus(){
        return ambulance_status;
    }

    public static void setAmbulanceTypes(ArrayList<AmbulanceType> ambulances){
        ambulanceTypes = new ArrayList<AmbulanceType>();
        ambulanceTypes = ambulances;
    }

    public static ArrayList<AmbulanceType> getAmbulanceTypes(){
        return  ambulanceTypes;
    }


  //  public static void setBookingCancel(boolean value){
  //      canCancel = value;
   // }

    public static boolean isCanCancel(){
        return canCancel;
    }

    // For generatin Facebook Keyhash code

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            // getting application package name, as defined in manifest
            String packageName = context.getApplicationContext()
                    .getPackageName();

            // Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(
                    packageName, PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext()
                    .getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=@@@@@@: ", key);
                Log.w("Key Hash@@@@=@@@@@@: ", key);
                //03-12 12:51:33.385: E/Key Hash=@@@@@@:(29354): OdlgTNgFF5dgBpD0qZORmdIY8R0=

            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    /*public static void setProd(boolean isProdBuild){
        isProd = isProdBuild;
    }

    public static boolean isProdBuild(){
        return isProd;
    }*/

    public static Bitmap GetBitmapMarker(Context mContext, int resourceId, String mText) {
        try {
            Resources resources = mContext.getResources();
            float scale = resources.getDisplayMetrics().density;
            Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId);

            android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();

            // set default bitmap config if none
            if (bitmapConfig == null)
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;

            bitmap = bitmap.copy(bitmapConfig, true);

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.BLACK);
            paint.setTextSize((int) (14 * scale));
            paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY);

            // draw text to the Canvas center
            Rect bounds = new Rect();
            paint.getTextBounds(mText, 0, mText.length(), bounds);
            int x = (bitmap.getWidth() - bounds.width()) / 2;
            int y = (bitmap.getHeight() + bounds.height()) / 2;

            canvas.drawText(mText, x * scale, y * scale, paint);

            return bitmap;

        } catch (Exception e) {
            return null;
        }
    }

    /*
    *To get a Bitmap image from the URL received
    * */
    public static Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            Log.e("imageUrl",imageUrl);
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.e("Bitmap","returned");
            return myBitmap;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Exception",e.getMessage());
            return null;
        }
    }

    public static String convertedDate(String date){
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date value = formatter.parse(date);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy hh:mm a"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            date = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            date = "00-00-0000 00:00";
        }
        return  date;

    }

    public static String convertedToDate(String date){
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date value = formatter.parse(date);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            date = dateFormatter.format(value);

        }
        catch (Exception e)
        {
            date = "00-00-0000 00:00";
        }
        return  date;

    }


}
