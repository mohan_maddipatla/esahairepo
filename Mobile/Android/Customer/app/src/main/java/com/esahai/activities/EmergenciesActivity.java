package com.esahai.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.esahai.R;
import com.esahai.Extras.Constants;
import com.esahai.Extras.Utils;
import com.esahai.adapters.EmergenciesAdapter;
import com.esahai.dataObjects.EmergenciesList;
import com.esahai.dataObjects.EmergencyDetails;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ragamai on 15/9/16.
 */
public class EmergenciesActivity extends CommonActivity {

    RecyclerView recycle_list_emergencies;
    RecyclerView.Adapter recyclerViewAdapterForEmergencies;
    RecyclerView.LayoutManager recylerViewLayoutManager;
    ApiInterface apiService;
    AppPreferences preferences;
    ArrayList<EmergencyDetails> emergencies;
    TextView textView_no_emergencies;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_emergencies);

        preferences = new AppPreferences(EmergenciesActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.my_emergencies));
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        recycle_list_emergencies = (RecyclerView)findViewById(R.id.recycle_list_emergencies);
        recylerViewLayoutManager = new LinearLayoutManager(EmergenciesActivity.this);
        textView_no_emergencies = (TextView)findViewById(R.id.textView_no_emergencies);
        textView_no_emergencies.setVisibility(View.GONE);

        recycle_list_emergencies.setLayoutManager(recylerViewLayoutManager);

        networkCallForEmergencies();


    }

    public void networkCallForEmergencies() {
        /*{
            "customerApp":true,
            "customer_id":33703311
           }
         */
        if(Utils.isNetworkAvailable(EmergenciesActivity.this)){
            apiService = ApiClient.getClient().create(ApiInterface.class);

            HashMap<String,Object> emergencyRequest = new HashMap<>();
            emergencyRequest.put("customerApp",true);
            emergencyRequest.put("customer_id",preferences.getCustomerId());
            emergencyRequest.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Call<EmergenciesList> emergenciesList = apiService.getEmergenciesList(emergencyRequest,preferences.getCustomerId(),preferences.getAccessToken());
            Utils.showProgressDialog(EmergenciesActivity.this);
            emergenciesList.enqueue(new Callback<EmergenciesList>() {
                @Override
                public void onResponse(Call<EmergenciesList> call, Response<EmergenciesList> response) {
                    Utils.hideProgressDialog();
                    try {
                        if(response!=null) {
                            Log.i("emergencies list", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                emergencies = response.body().getResponseData();

                                Log.d("emergencies","emergencies_Response"+emergencies);
                                if (emergencies != null && emergencies.size() > 0) {
                                    recyclerViewAdapterForEmergencies = new EmergenciesAdapter(EmergenciesActivity.this, emergencies, recycle_list_emergencies);
                                    recycle_list_emergencies.setAdapter(recyclerViewAdapterForEmergencies);
                                    recycle_list_emergencies.setVisibility(View.VISIBLE);
                                    textView_no_emergencies.setVisibility(View.GONE);
                                }
                                else{
                                    recycle_list_emergencies.setVisibility(View.GONE);
                                    textView_no_emergencies.setVisibility(View.VISIBLE);
                                }
                            }
                            else{
                                recycle_list_emergencies.setVisibility(View.GONE);
                                textView_no_emergencies.setVisibility(View.VISIBLE);
                            }
                        }else{
                            recycle_list_emergencies.setVisibility(View.GONE);
                            textView_no_emergencies.setVisibility(View.VISIBLE);
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<EmergenciesList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    recycle_list_emergencies.setVisibility(View.GONE);
                    textView_no_emergencies.setVisibility(View.VISIBLE);
                    //Utils.showToast(EmergenciesActivity.this,t.toString());
                    Utils.showToast(EmergenciesActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        }else{Utils.showToast(EmergenciesActivity.this,getResources().getString(R.string.network_error));}
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/

}
