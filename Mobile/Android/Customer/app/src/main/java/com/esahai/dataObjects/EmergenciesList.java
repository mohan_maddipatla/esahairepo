package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 10/10/16.
 */
public class EmergenciesList {

    /*
    {"statusCode":"300","responseData":[{"created_datetime":"2016-10-07 10:48:58","booking_id":"783773","emergency_type":"Cardiac Arrest","emergency_longitude":"78.3720453","emergency_latitude":"17.4732376","hospital":"Rem Hii"}]
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<EmergencyDetails> responseData;


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<EmergencyDetails> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<EmergencyDetails> responseData) {
        this.responseData = responseData;
    }


    @Override
    public String toString() {
        return "EmergenciesList{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
