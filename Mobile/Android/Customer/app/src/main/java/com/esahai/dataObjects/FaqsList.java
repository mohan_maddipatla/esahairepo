package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by adheesh on 30/01/17.
 */

public class FaqsList {


    public String statusCode;
    //public String statusMessage;
    public ArrayList<FaqDetails> responseData;


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /*public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }*/

    public ArrayList<FaqDetails> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<FaqDetails> responseData) {
        this.responseData = responseData;
    }


    @Override
    public String toString() {
        return "FaqsList{" +
                "statusCode='" + statusCode + '\'' +
               // ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
