package com.esahai.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.Extras.Utils;

import com.esahai.R;
import com.esahai.adapters.BloodBanksAdapter;
import com.esahai.customVIews.DividerItemDecoration;
import com.esahai.dataObjects.BloodbankDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 4/28/2017.
 */

public class BloodBankActivity extends CommonActivity {

    private List<BloodbankDetails> bloodbankDetailsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BloodBanksAdapter mAdapter;
    ImageView imageView_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bloodbanks_recyclerview_main);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.view_blood_banks));
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utils.closeKeyBoard(BloodBankActivity.this,imageView_back);
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);



        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new BloodBanksAdapter(bloodbankDetailsList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        /*recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BloodbankDetails bbd = bloodbankDetailsList.get(position);
                Toast.makeText(getApplicationContext(), bbd.getBloodbank_name() + " is selected!", Toast.LENGTH_SHORT).show();



                sendNotify();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        prepareBankDetailsData();




    }


    private void prepareBankDetailsData() {

        /*List<String> numbers = new ArrayList<String>();

        numbers = new ArrayList<String>();
        numbers.add("+914027162146");
        numbers.add("+919866192122");
        BloodbankDetails bbd = new BloodbankDetails("Asian Blood Bank Of Asian Health Foundation",numbers,"Plot No 2 Shanthi Surabhi Complex, Main Road, A S Rao Nagar, Hyderabad - 500762","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914024745243");
        bbd = new BloodbankDetails("Bhanji Kheraji Blood Bank",numbers,"Inside feroze gandhi park, opp sbi head office, bank street , koti, Hyderabad, Telangana 500095","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914042417760");
        bbd = new BloodbankDetails("Challa Hospital Blood Bank",numbers,"7-1-71/A/1, Dharam Karam Road, Ameerpet, Hyderabad, Telangana 500016","http://www.challahospital.com/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023555005");
        bbd = new BloodbankDetails("Chiranjeevi Eye & Blood Bank",numbers,"No.8-2-293/82/A/C.E.B.B, Road Number 1, Jubilee Hills, Hyderabad, Telangana 500033","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914027505566");
        numbers.add("+914027510053");
        bbd = new BloodbankDetails("Gandhi Hospital",numbers,"Main Road, Musheerabad, Hyderabad - 500048","http://www.gandhihospital.in/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023411658");
        bbd = new BloodbankDetails("Gene Blood Bank",numbers,"Kanthisikara Cmplx, Opp Model House, Panjagutta, Hyderabad - 500004","http://www.genebloodbank.in/Home?index.html");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023403882");
        numbers.add("+914023411658");
        bbd = new BloodbankDetails("Genetic Products India Ltd",numbers,"G Blk Kanthisikaracplx Punjagutta82, Hyderabad - 500082","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914024523641");
        bbd = new BloodbankDetails("Govt Maternity Hospital Nayapool",numbers,"Ameen Bagh, Nayapul, Hyderabad - 500002","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023150496");
        bbd = new BloodbankDetails("Janani Voluntary Blood Bank",numbers,"G.P Rao Enclave, EWS 58/A, 1st Floor, Road Number 3, Hyderabad, Telangana 500072","http://www.jananivoluntary.org/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914027707088");
        bbd = new BloodbankDetails("Jeeva Dhara Voluntary Blood Bank",numbers,"9-4-269-275, 2nd Floor, Above Saluja Hospital, Opposite Railway Station, Behind Swathi tiffin center, Regimental Bazar, Secunderabad, Telangana 500025","http://jeevadhaarabloodbank.org/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914039879999");
        numbers.add("+914039879298");
        bbd = new BloodbankDetails("Kamineni Blood Bank",numbers,"L B Nagar, Hyderabad - 500036","http://www.kaminenihospitals.com/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914024745243");
        bbd = new BloodbankDetails("Lions club of hyd  blood bank",numbers,"Door No 4-4-248, Koti, , Near Government Ent Hospital,Hyderabad - 500095.","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914024745243");
        bbd = new BloodbankDetails("Lions Club Of Hyderabad",numbers,"4-4-248, Near Koti, Sultan Bazar, Hyderabad - 500095","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023774469");
        numbers.add("+914066194469");
        bbd = new BloodbankDetails("M M Voluntary Blood Bank",numbers,"Ferozeguda, 4-194/4, Balanagar, Beside B B R Hospital, Balanagar, Hyderabad, 500042","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914027550238");
        numbers.add("+914026822271");
        bbd = new BloodbankDetails("Mythri Charitable Turst Blood Bank & Transfusion",numbers,"3-4-808, Barkatpura, Hyderabad - 500027","http://www.mythribloodbank.org/mythri_charitable.html");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023314095");
        numbers.add("+914023394265");
        bbd = new BloodbankDetails("Niloufer Hospital For Women & Children",numbers,"Red Hills, Hyderabad - 500004","http://www.nilouferhospital.com/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914030799999");
        bbd = new BloodbankDetails("Ntr Memorial Trust",numbers,"8-2-120/Nbtr/2, Road No 2, Banjara Hills, Hyderabad - 500034, Behind TDP Party Office","http://ntrtrust.org/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+919666663811");
        bbd = new BloodbankDetails("Rajyalaxmi Charitable Trust Blood Bank",numbers,"VR Colony, Kamala Nagar, Dilsukhnagar, Hyderabad, Telangana 500060","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914027633087");
        bbd = new BloodbankDetails("Red Cross Blood Bank",numbers,"1-9-310, Street No 6, Near Spencers Supermarket, Vidya Nagar, Hyderabad, Telangana 500044","http://redcrossbloodbank.site/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023220222");
        numbers.add("+914065883770");
        bbd = new BloodbankDetails("Rudhira Voluntary Blood Bank",numbers,"3-6-10/A 1st Floor, Main Road, Anasurya Commercial Complex, Himayat Nagar, Hyderabad - 500029","https://www.healthfrog.in/laboratory/rudhira-voluntary-blood-bank-hyderabad-telangana-25_3l9q_f.html");
        bloodbankDetailsList.add(bbd);


        numbers = new ArrayList<String>();
        numbers.add("+914065522343");
        bbd = new BloodbankDetails("Sanjeevani Blood Bank",numbers,"1-1-79/A Bhagyanagar Complex, Rtc X Roads, Hyderabad - 500020","http://www.sanjeeviniblooddonors.org/callus.php");
        bloodbankDetailsList.add(bbd);



        numbers = new ArrayList<String>();
        numbers.add("+919985409444");
        bbd = new BloodbankDetails("Social Service Blood Bank",numbers,"9-1-127/1/D/1, Adj To Madhava Nursing Home, Lane Opp St Marys Church, S D Road, Hyderabad - 500003","");
        bloodbankDetailsList.add(bbd);


        numbers = new ArrayList<String>();
        numbers.add("+914027016016");
        bbd = new BloodbankDetails("Sridevi blood bank",numbers,"207 2nd Flr Pavani Anasuya Towers, Opp Huda, Tarnaka, Hyderabad - 500017","");
        bloodbankDetailsList.add(bbd);*/
















        List<String> numbers = new ArrayList<String>();



		/* Ameerpet */
        numbers = new ArrayList<String>();
        numbers.add("+914042417760");
        BloodbankDetails bbd = new BloodbankDetails("Challa Hospital Blood Bank",numbers,"7-1-71/A/1, Dharam Karam Road, Ameerpet, Hyderabad, Telangana 500016","http://www.challahospital.com/");
        bloodbankDetailsList.add(bbd);

		/* AS Rao Nagar */
        numbers = new ArrayList<String>();
        numbers.add("+914027162146");
        numbers.add("+919866192122");
        bbd = new BloodbankDetails("Asian Blood Bank Of Asian Health Foundation",numbers,"Plot No 2 Shanthi Surabhi Complex, Main Road, A S Rao Nagar, Hyderabad - 500762","");
        bloodbankDetailsList.add(bbd);

		/* Balanagar */
        numbers = new ArrayList<String>();
        numbers.add("+914023774469");
        numbers.add("+914066194469");
        bbd = new BloodbankDetails("M M Voluntary Blood Bank",numbers,"Ferozeguda, 4-194/4, Balanagar, Beside B B R Hospital, Balanagar, Hyderabad, 500042","");
        bloodbankDetailsList.add(bbd);

		/* Banjara Hills */
        numbers = new ArrayList<String>();
        numbers.add("+914030799999");
        bbd = new BloodbankDetails("Ntr Memorial Trust",numbers,"8-2-120/Nbtr/2, Road No 2, Banjara Hills, Hyderabad - 500034, Behind TDP Party Office","http://ntrtrust.org/");
        bloodbankDetailsList.add(bbd);


		/* Barkatpura */
        numbers = new ArrayList<String>();
        numbers.add("+914027550238");
        numbers.add("+914026822271");
        bbd = new BloodbankDetails("Mythri Charitable Turst Blood Bank & Transfusion",numbers,"3-4-808, Barkatpura, Hyderabad - 500027","http://www.mythribloodbank.org/mythri_charitable.html");
        bloodbankDetailsList.add(bbd);


		/* Dilsukhnagar */
        numbers = new ArrayList<String>();
        numbers.add("+919666663811");
        bbd = new BloodbankDetails("Rajyalaxmi Charitable Trust Blood Bank",numbers,"VR Colony, Kamala Nagar, Dilsukhnagar, Hyderabad, Telangana 500060","");
        bloodbankDetailsList.add(bbd);

		/* HimayatNagar */
        numbers = new ArrayList<String>();
        numbers.add("+914023220222");
        numbers.add("+914065883770");
        bbd = new BloodbankDetails("Rudhira Voluntary Blood Bank",numbers,"3-6-10/A 1st Floor, Main Road, Anasurya Commercial Complex, HimayatNagar, Hyderabad - 500029","https://www.healthfrog.in/laboratory/rudhira-voluntary-blood-bank-hyderabad-telangana-25_3l9q_f.html");
        bloodbankDetailsList.add(bbd);

		/* Jubilee Hills */

        numbers = new ArrayList<String>();
        numbers.add("+914023555005");
        bbd = new BloodbankDetails("Chiranjeevi Eye & Blood Bank",numbers,"No.8-2-293/82/A/C.E.B.B, Road Number 1, Jubilee Hills, Hyderabad, Telangana 500033","");
        bloodbankDetailsList.add(bbd);

		/* Koti */

        numbers = new ArrayList<String>();
        numbers.add("+914024745243");
        bbd = new BloodbankDetails("Bhanji Kheraji Blood Bank",numbers,"Inside feroze gandhi park, opp sbi head office, bank street , Koti, Hyderabad, Telangana 500095","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914024745243");
        bbd = new BloodbankDetails("Lions club of hyd  blood bank",numbers,"Door No 4-4-248, Koti, , Near Government Ent Hospital,Hyderabad - 500095.","");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914024745243");
        bbd = new BloodbankDetails("Lions Club Of Hyderabad",numbers,"4-4-248, Near Koti, Sultan Bazar, Hyderabad - 500095","");
        bloodbankDetailsList.add(bbd);

		/* Kukatpally */
        numbers = new ArrayList<String>();
        numbers.add("+914023150496");
        bbd = new BloodbankDetails("Janani Voluntary Blood Bank",numbers,"G.P Rao Enclave, EWS 58/A, 1st Floor, Road Number 3, Kukatpally, Hyderabad, Telangana 500072","http://www.jananivoluntary.org/");
        bloodbankDetailsList.add(bbd);


		/* L B Nagar */
        numbers = new ArrayList<String>();
        numbers.add("+914039879999");
        numbers.add("+914039879298");
        bbd = new BloodbankDetails("Kamineni Blood Bank",numbers,"L B Nagar, Hyderabad - 500036","http://www.kaminenihospitals.com/");
        bloodbankDetailsList.add(bbd);


		/* Musheerabad */

        numbers = new ArrayList<String>();
        numbers.add("+914027505566");
        numbers.add("+914027510053");
        bbd = new BloodbankDetails("Gandhi Hospital",numbers,"Main Road, Musheerabad, Hyderabad - 500048","http://www.gandhihospital.in/");
        bloodbankDetailsList.add(bbd);

        /* Nayapul */
        numbers = new ArrayList<String>();
        numbers.add("+914024523641");
        bbd = new BloodbankDetails("Govt Maternity Hospital Nayapool",numbers,"Ameen Bagh, Nayapul, Hyderabad - 500002","");
        bloodbankDetailsList.add(bbd);


		/* Panjagutta */

        numbers = new ArrayList<String>();
        numbers.add("+914023411658");
        bbd = new BloodbankDetails("Gene Blood Bank",numbers,"Kanthisikara Cmplx, Opp Model House, Punjagutta, Hyderabad - 500004","http://www.genebloodbank.in/Home?index.html");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+914023403882");
        numbers.add("+914023411658");
        bbd = new BloodbankDetails("Genetic Products India Ltd",numbers,"G Blk Kanthisikaracplx, Punjagutta, Hyderabad - 500082","");
        bloodbankDetailsList.add(bbd);

		/* Red Hills */
        numbers = new ArrayList<String>();
        numbers.add("+914023314095");
        numbers.add("+914023394265");
        bbd = new BloodbankDetails("Niloufer Hospital For Women & Children",numbers,"Red Hills, Hyderabad - 500004","http://www.nilouferhospital.com/");
        bloodbankDetailsList.add(bbd);

		/* RTC X Roads */
        numbers = new ArrayList<String>();
        numbers.add("+914065522343");
        bbd = new BloodbankDetails("Sanjeevani Blood Bank",numbers,"1-1-79/A Bhagyanagar Complex, RTC X Roads, Hyderabad - 500020","http://www.sanjeeviniblooddonors.org/callus.php");
        bloodbankDetailsList.add(bbd);


		/* Secunderabad */
        numbers = new ArrayList<String>();
        numbers.add("+914027707088");
        bbd = new BloodbankDetails("Jeeva Dhara Voluntary Blood Bank",numbers,"9-4-269-275, 2nd Floor, Above Saluja Hospital, Opposite Railway Station, Behind Swathi tiffin center, Regimental Bazar, Secunderabad, Telangana 500025","http://jeevadhaarabloodbank.org/");
        bloodbankDetailsList.add(bbd);

        numbers = new ArrayList<String>();
        numbers.add("+919985409444");
        bbd = new BloodbankDetails("Social Service Blood Bank",numbers,"9-1-127/1/D/1, Adj To Madhava Nursing Home, Lane Opp St Marys Church, S D Road, Secunderabad - 500003","");
        bloodbankDetailsList.add(bbd);

		/* Tarnaka */
        numbers = new ArrayList<String>();
        numbers.add("+914027016016");
        bbd = new BloodbankDetails("Sridevi blood bank",numbers,"207 2nd Flr Pavani Anasuya Towers, Opp Huda, Tarnaka, Hyderabad - 500017","");
        bloodbankDetailsList.add(bbd);

		/* Vidya Nagar */
        numbers = new ArrayList<String>();
        numbers.add("+914027633087");
        bbd = new BloodbankDetails("Red Cross Blood Bank",numbers,"1-9-310, Street No 6, Near Spencers Supermarket, Vidya Nagar, Hyderabad, Telangana 500044","http://redcrossbloodbank.site/");
        bloodbankDetailsList.add(bbd);


















        mAdapter.notifyDataSetChanged();

    }


}
