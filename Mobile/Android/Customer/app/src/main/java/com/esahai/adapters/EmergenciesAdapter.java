package com.esahai.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.Extras.Constants;
import com.esahai.activities.ContactsActivity;
import com.esahai.activities.DashboardActivity;
import com.esahai.activities.EmergenciesActivity;
import com.esahai.activities.TripDetailsActivity;
import com.esahai.dataObjects.AmbulanceDetails;
import com.esahai.dataObjects.EmergenciesList;
import com.esahai.dataObjects.EmergencyDetails;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ragamai on 4/10/16.
 */
public class EmergenciesAdapter extends RecyclerView.Adapter<EmergenciesAdapter.ViewHolder>
            implements View.OnClickListener{

    Context context;
    ArrayList<EmergencyDetails> emergenciesLists;
    RecyclerView recycle_list_emergencies;



    public EmergenciesAdapter(Context context, ArrayList<EmergencyDetails> emergenciesLists, RecyclerView recycle_list_emergencies) {
        this.context = context;
        this.emergenciesLists = emergenciesLists;
        this.recycle_list_emergencies = recycle_list_emergencies;
    }

    @Override
    public EmergenciesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.emergencies_list_item, parent, false);
        v.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EmergenciesAdapter.ViewHolder holder, final int position) {
        try {
            if(emergenciesLists!=null) {
                holder.textView_date.setText(Utils.convertedDate(emergenciesLists.get(position).getCreated_datetime()));
                holder.textView_ambulance_number.setText(emergenciesLists.get(position).getAmbulance_number());

                //set booking id
                if(emergenciesLists.get(position).getBooking_id()!=null){
                    holder.textView_booking_id.setText("Booking Id: "+emergenciesLists.get(position).getBooking_id());
                }

                //set booking cost
                if (emergenciesLists.get(position).getCost() != null
                        &&( !emergenciesLists.get(position).getCost().equalsIgnoreCase("undefined") ||
                        !emergenciesLists.get(position).getCost().equalsIgnoreCase("") )) {
                    holder.textView_cost.setVisibility(View.VISIBLE);
                    holder.textView_cost.setText(context.getResources().getString(R.string.Rs)+emergenciesLists.get(position).getCost());
                } else {
                    holder.textView_cost.setVisibility(View.GONE);
                }

                //set booking status
                if (emergenciesLists.get(position).getBooking_status().equalsIgnoreCase(Constants.CANCELLED) ||
                        emergenciesLists.get(position).getBooking_status().equalsIgnoreCase(Constants.CANCELLED_FAKE)) {
                    holder.textView_status.setVisibility(View.VISIBLE);
                    holder.textView_status.setText("");
                    holder.textView_cost.setVisibility(View.GONE);
                } else {
                    holder.textView_status.setVisibility(View.GONE);
                    holder.textView_cost.setVisibility(View.VISIBLE);
                }

                //set driver profile image
                if (emergenciesLists.get(position).getDriver_profile() != null
                        && !emergenciesLists.get(position).getDriver_profile().isEmpty()) {
                    Log.i("driver profile img", emergenciesLists.get(position).getDriver_profile());
                    Picasso
                            .with(context)
                            .load(emergenciesLists.get(position).getDriver_profile())
                            .into(holder.imageView_profile);
                }

                //set locations
                if(emergenciesLists.get(position).getEme_address()!=null){
                    holder.textView_from_location.setText(emergenciesLists.get(position).getEme_address());
                }
                if(emergenciesLists.get(position).getHospital()!=null){
                    holder.textView_to_location.setText(emergenciesLists.get(position).getHospital());
                }

                //set rating
                holder.ratingBar.setNumStars(5);

                if(emergenciesLists.get(position).getFeedback()!=null){
                    holder.ratingBar.setRating(Float.parseFloat(emergenciesLists.get(position).getFeedback().getPoints()));
                    holder.textView_give_rating.setVisibility(View.GONE);
                    holder.ratingBar.setVisibility(View.VISIBLE);
                }
                else{
                    holder.ratingBar.setRating(0f);
                    if(emergenciesLists.get(position).getBooking_status().equalsIgnoreCase(Constants.TRIP_CLOSED)){
                        holder.textView_give_rating.setVisibility(View.VISIBLE);
                        holder.ratingBar.setVisibility(View.GONE);
                    }else{
                        holder.textView_give_rating.setVisibility(View.GONE);
                        holder.ratingBar.setVisibility(View.GONE);
                    }
                }



                //set driver name
                if(emergenciesLists.get(position).getDriver_name()!=null){
                    holder.textView_driverName.setText(emergenciesLists.get(position).getDriver_name());
                }

                holder.textView_give_rating.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feedbackDialog(emergenciesLists.get(position).getBooking_id());
                    }
                });


                //set static map for locations
                if (emergenciesLists.get(position).getEmergency_latitude() != null
                        && emergenciesLists.get(position).getEmergency_longitude() != null
                        && emergenciesLists.get(position).getHospital_latitude() != null
                        && emergenciesLists.get(position).getHospital_longitude() != null) {
                    String emergency_latitude = emergenciesLists.get(position).getEmergency_latitude();
                    String emergency_longitude = emergenciesLists.get(position).getEmergency_longitude();
                    String hospital_latitude = emergenciesLists.get(position).getHospital_latitude();
                    String hospital_longitude = emergenciesLists.get(position).getHospital_longitude();
                    String url = null;
                    String origin = emergency_latitude+","+emergency_longitude;
                    String dest = hospital_latitude+","+hospital_longitude;

                    /*if (emergenciesLists.get(position).getBooking_status().equalsIgnoreCase("Cancelled")) {
                        url = "https://maps.googleapis.com/maps/api/staticmap?" +
                                "size=300x200&maptype=roadmap&markers=color:green|" + origin +"&key=" +
                                context.getResources().getString(R.string.geo_coding_key);
                    }else {


                        if (hospital_latitude != null && !hospital_latitude.equalsIgnoreCase("0")
                                && hospital_longitude != null && !hospital_longitude.equalsIgnoreCase("0")) {
                            url = "https://maps.googleapis.com/maps/api/staticmap?" +
                                    "size=300x200&maptype=roadmap&markers=color:green|" + origin + "|&markers=color:red|" + dest + "&key=" +
                                    context.getResources().getString(R.string.geo_coding_key);
                        }
                        else{
                            url = "https://maps.googleapis.com/maps/api/staticmap?" +
                                    "size=300x200&maptype=roadmap&markers=color:green|" + origin +"&key=" +
                                    context.getResources().getString(R.string.geo_coding_key);
                        }
                    }

                        if(url!=null && !url.isEmpty()){
                            Picasso
                                    .with(context)
                                    .load(url)
                                    .into(holder.imageView_map);
                        }*/




                }

            }

        }catch (Exception e){e.printStackTrace();}
    }

    private void feedbackDialog(final String booking_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("Feedback");
        final FrameLayout frameView = new FrameLayout(context);
        builder.setView(frameView);

        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.feedback, frameView);
        Button button_submit = (Button)dialoglayout.findViewById(R.id.button_submit);
        Button button_cancel = (Button)dialoglayout.findViewById(R.id.button_cancel);
        final RatingBar ratingBar = (RatingBar)dialoglayout.findViewById(R.id.ratingBar);
        final EditText editText_comment = (EditText)dialoglayout.findViewById(R.id.editText_comment);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.button_color), PorterDuff.Mode.SRC_ATOP);

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rating= (int) ratingBar.getRating();
                String comment = editText_comment.getText().toString();
                if(rating != 0){

                    sendFeedBack(rating,comment,booking_id);
                    alertDialog.dismiss();

                    /*Intent i = new Intent(TripDetailsActivity.this, DashboardActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
//                    startActivity(i);
//                    finish();
//                    cancelNotifications();


                }
                else{
                    Utils.showToast(context,context.getResources().getString(R.string.give_rating));}
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                /*Intent i = new Intent(context, DashboardActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
                /*startActivity(i);
                finish();
                cancelNotifications();*/
            }
        });

        alertDialog.show();
    }

    private void sendFeedBack(int rating,String comment, String booking_id) {
        if(Utils.isNetworkAvailable(context)){
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            AppPreferences preferences = new AppPreferences(context);
            HashMap<String,Object> request = new HashMap<>();
            //points,booking_id,customer_id
            request.put("points",rating);
            request.put("booking_id",booking_id/*emergencyDetails.getBooking_id()*/);
            request.put("customer_id",preferences.getCustomerId());
            request.put("comment",comment);
            request.put("customerApp",true);
            request.put("customer_mobile",Utils.encrypt(preferences.getMobileNumber()));

            Log.i("feedback request", Arrays.asList(request).toString());

            Call<ResponseBody> sendFeedBack = apiInterface.sendFeedback(request,preferences.getCustomerId(),preferences.getAccessToken());
//            Utils.showProgressDialog(TripDetailsActivity.this);
            sendFeedBack.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    Utils.hideProgressDialog();
                    try{
                        if(response.body()!=null){
                            Log.i("feedback response",response.body().string());
                            EsahaiApplication.getApplication().trackEvent("Send feedback", "Feedback on the trip", "Feedback sent successfully");
                            if (context instanceof EmergenciesActivity) {
                                ((EmergenciesActivity) context).networkCallForEmergencies();
                            }
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Utils.hideProgressDialog();
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return emergenciesLists.size();
    }

    @Override
    public void onClick(View view) {
        int itemPosition = recycle_list_emergencies.getChildLayoutPosition(view);
        EmergencyDetails emergencyDetails = emergenciesLists.get(itemPosition);
       String status = emergencyDetails.getBooking_status();
        Log.d("status ","status ***"+status);
        if(status.equalsIgnoreCase(Constants.CANCELLED) ||
                status.equalsIgnoreCase(Constants.CANCELLED_FAKE)
                ||status.equalsIgnoreCase(Constants.PENDING)){

           return;
       }
        Intent tripDetails = new Intent(context, TripDetailsActivity.class);
        tripDetails.putExtra("trip_details",emergencyDetails);
        context.startActivity(tripDetails);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView textView_date;
        TextView textView_ambulance_number;
        TextView textView_cost;
        TextView textView_status;
        TextView textView_driverName;
        TextView textView_from_location;
        TextView textView_to_location;
        TextView textView_booking_id;
        TextView textView_give_rating;
        RatingBar ratingBar;

        ImageView imageView_map;
        CircleImageView imageView_profile;


        public ViewHolder(View view) {
            super(view);
            textView_date = (TextView)view.findViewById(R.id.textView_date);
            textView_ambulance_number = (TextView)view.findViewById(R.id.textView_ambulance_number);
            textView_cost = (TextView)view.findViewById(R.id.textView_cost);
            textView_status = (TextView)view.findViewById(R.id.textView_status);
            textView_from_location = (TextView)view.findViewById(R.id.textView_from_location);
            textView_to_location = (TextView)view.findViewById(R.id.textView_to_location);
            textView_booking_id = (TextView)view.findViewById(R.id.textView_booking_id);
            textView_give_rating = (TextView)view.findViewById(R.id.textView_give_rating);

            textView_driverName = (TextView)view.findViewById(R.id.textView_driverName);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);

            imageView_map = (ImageView)view.findViewById(R.id.imageView_map);
            imageView_profile = (CircleImageView)view.findViewById(R.id.imageView_profile);
        }



    }



}
