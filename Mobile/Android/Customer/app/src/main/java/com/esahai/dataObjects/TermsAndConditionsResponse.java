package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 14/11/16.
 */
public class TermsAndConditionsResponse {

    /*
    "statusCode": "300",
	"statusMessage": "terms and conditions fetched successfully",
	"responseData": [{
		"id": "4",
		"terms_and_conditions": "<p>Welcome to ESAHAI</p>\n"
	}]
}
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<TermsResponse> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<TermsResponse> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<TermsResponse> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "TermsAndConditionsResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
