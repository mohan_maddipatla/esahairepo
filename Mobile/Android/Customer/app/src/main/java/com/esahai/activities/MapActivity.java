package com.esahai.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.customVIews.CustomAutoCompleteTextView;
import com.esahai.dataObjects.GoogleAddress;
import com.esahai.dataObjects.GoogleLatLng;
import com.esahai.parsers.PlaceJSONParser;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ragamai on 16/9/16.
 */
public class MapActivity extends CommonActivity implements android.location.LocationListener,
        AdapterView.OnItemClickListener, View.OnClickListener, OnMapReadyCallback {

    Button button_select_location;
    GoogleMap map;
    CustomAutoCompleteTextView autocomplete_text_places;
    ImageView image_search, image_delete, image_mylocation;

    private static final String API_KEY = "AIzaSyBwzi7hXo-zEs27qlc38M-toU6nlRbRxq0";

    PlacesTask placesTask;
    ParserTask parserTask;
    Location location;
    String address = "";
    ApiInterface apiInterface;
    double lat, longi;
    ImageView imageView_back;

    private LatLng mCenterLatLong;

    boolean cameraMovedProgrammatically = false;

    String isOldLocation =  "";




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_location);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.location));
        imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utils.closeKeyBoard(MapActivity.this, imageView_back);
                    if (autocomplete_text_places.getText().toString().length() != 0) {

                        assignLatLang();
                        networkCallForConvertAddress(autocomplete_text_places.getText().toString(), 1, true);

                        Log.d("Bundle", "Bundle" + "imageView_back");
                        Bundle b = new Bundle();
                        b.putString("location", autocomplete_text_places.getText().toString());
                        b.putDouble("latitude", lat);
                        b.putDouble("longitude", longi);
                        Intent i = getIntent();
                        i.putExtras(b);
                        setResult(Activity.RESULT_OK, i);
                        finish();
                    } else {

                        Toast.makeText(MapActivity.this, getResources().getString(R.string.select_location), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        autocomplete_text_places = (CustomAutoCompleteTextView) findViewById(R.id.autocomplete_text_places);
        autocomplete_text_places.setImeActionLabel("Done", EditorInfo.IME_ACTION_DONE);
        autocomplete_text_places.setThreshold(1);

        final MapFragment mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(MapActivity.this);

        button_select_location = (Button) findViewById(R.id.button_select_location);
        image_delete = (ImageView) findViewById(R.id.image_delete);
        image_search = (ImageView) findViewById(R.id.image_search);
        image_mylocation = (ImageView) findViewById(R.id.image_mylocation);

        apiInterface = ApiClient.getGoogleClient().create(ApiInterface.class);


        image_delete.setOnClickListener(this);
        image_search.setOnClickListener(this);
        button_select_location.setOnClickListener(this);
        image_mylocation.setOnClickListener(this);



        autocomplete_text_places.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                image_search.performClick();

            }
        });




        autocomplete_text_places.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Utils.closeKeyBoard(MapActivity.this, autocomplete_text_places);
                    //image_search.setVisibility(View.GONE);
                    //image_delete.setVisibility(View.VISIBLE);
                    String address = autocomplete_text_places.getText().toString();
                    networkCallForConvertAddress(address, 0, true);
                    return true;
                }
                return false;
            }
        });

        autocomplete_text_places.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() <= 30) {
                    placesTask = new PlacesTask();
                    placesTask.execute(s.toString().trim());
                    Log.i("coming", "yes");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i("getting into", "yes");
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.isGPSOn(MapActivity.this)) {
            Utils.turnOnGPS(MapActivity.this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            if (autocomplete_text_places.getText().toString().length() != 0) {
                networkCallForConvertAddress(autocomplete_text_places.getText().toString(), 1, false);
            } else {
                Log.d("Bundle", "Bundle" + "home");
                Bundle b = new Bundle();
                b.putString("location", autocomplete_text_places.getText().toString());
                b.putDouble("latitude", lat);
                b.putDouble("longitude", longi);
                Intent i = getIntent();
                i.putExtras(b);
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLocationChanged(Location location) {
//        TextView locationTv = (TextView) findViewById(R.id.latlongLocation);
       /* double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
//        googleMap.addMarker(new MarkerOptions().position(latLng));
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(15));*/
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = (String) parent.getItemAtPosition(position);
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }


    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        Log.d("url***","downloadUrl ***"+strUrl);
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("MapActivity", ""+e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d("MAp","On Map Readyyyy");
        try {
            map = googleMap;
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            if (!Utils.isGPSOn(MapActivity.this)) {
                Utils.turnOnGPS(MapActivity.this);
            } else {

                LocationManager locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                        (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

//                Toast.makeText(this, "error while permissions", Toast.LENGTH_SHORT).show();
                } else {
//                Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
                    map.getUiSettings().setZoomGesturesEnabled(true);

                    //map.getUiSettings().setMyLocationButtonEnabled(false);


                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, MapActivity.this);
//            Location currentLocation = locationManager
//                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    location = locationManager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location == null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }

                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    lat = latitude;
                    longi = longitude;
                    LatLng latLng = new LatLng(latitude, longitude);

                    cameraMovedProgrammatically = true;

                    Log.d("CAMERA","cameraMovedProgrammatically = true::::1");
                    Log.d("Marker_Location", "Lat" + latitude + "Long" + longitude);
                    //map.addMarker(new MarkerOptions().position(latLng));
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    map.animateCamera(CameraUpdateFactory.zoomTo(17));
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    map.setMyLocationEnabled(false);

                    networkCallForLatLng(latitude,longitude);



                    map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            Log.d("Camera postion change" + "", cameraPosition + "");
                            mCenterLatLong = cameraPosition.target;


                            map.clear();

                            try {

                                Location mLocation = new Location("");
                                mLocation.setLatitude(mCenterLatLong.latitude);
                                mLocation.setLongitude(mCenterLatLong.longitude);
                                lat = mCenterLatLong.latitude;
                                longi = mCenterLatLong.longitude;

                                Log.d("lat lang", "camera lat lang = " + lat + ", " + longi);
                                double newLatitude = mCenterLatLong.latitude;

                                double newLongitude = mCenterLatLong.longitude;

                                if(cameraMovedProgrammatically){
                                    //autocomplete_text_places.setText(""+address);
                                    /*In Some cases onCamera change is being called twice.
                                    So isOldLocation helps to check if the call is being done on the
                                    same location or not.
                                    If they are different then make a network call to get address from latlong
                                   */

                                    isOldLocation = cameraPosition.target.toString();
                                    Log.d("Camera","cameraMovedProgrammatically true--address::::"+address);

                                    Log.d("CAMERA","cameraMovedProgrammatically = true::::2"+ isOldLocation);

                                    cameraMovedProgrammatically = false;
                                }else {

                                    Log.d("Camera","cameraMovedProgrammatically false");
                                    Log.d("CAMERA", "cameraMovedProgrammatically = FALSE::::1");

                                    if(!isOldLocation.equalsIgnoreCase(cameraPosition.target.toString())) {

                                        //isOldLocation = cameraPosition.target.toString();
                                        Log.d("Camera","Both locations are DIfferent::isOldLocation--"+ isOldLocation +"cameraPosition--"+cameraPosition.target.toString());
                                        autocomplete_text_places.setText(networkCallForLatLng(newLatitude, newLongitude));
                                    }
                                    else{
                                        Log.d("Camera","Both locations are Same::isOldLocation--"+ isOldLocation +"cameraPosition--"+cameraPosition.target.toString());
                                    }
                                }

                                //startIntentService(mLocation);
                                //mLocationMarkerText.setText("Lat : " + mCenterLatLong.latitude + "," + "Long : " + mCenterLatLong.longitude);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    });











                    /*map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                        @Override
                        public void onMapClick(LatLng latLng) {

                            // Creating a marker
                            MarkerOptions markerOptions = new MarkerOptions();

                            // Setting the position for the marker
                            markerOptions.position(latLng);

                            autocomplete_text_places.setText(networkCallForLatLng(latLng.latitude,latLng.longitude));

                            // Setting the title for the marker.
                            // This will be displayed on taping the marker
//                        markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                            // Clears the previously touched position
                            try {
                                map.clear();
                            }catch (Exception e){e.printStackTrace();}

                            // Animating to the touched position
                            map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                            // Placing a marker on the touched position
                            map.addMarker(markerOptions);
                        }
                    });*/


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            //String key = "key="+API_KEY;

            String key = "key=" + getResources().getString(R.string.geo_coding_key);

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "UTF-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            //String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters + "&maptype=roadmap";


            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + key+"&"+input;

//
// System.out.print("places url--->"+url);
            Log.i("places url *****", url);



            try {
                /*String keyNew = "" + getResources().getString(R.string.geo_coding_key);

                StringBuilder sb = new StringBuilder(PLACES_API_BASE
                        + TYPE_AUTOCOMPLETE + OUT_JSON);
                sb.append("?key=" + keyNew);
                // sb.append("&components=country:gr");
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));

                String urlNew = sb.toString();

                data = downloadUrl(urlNew);*/

                data = downloadUrl(url);
                Log.d("data","data = "+data);
            }


            catch (Exception e) {
                Log.d("Background Task", e.toString());
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("result","placesssss = "+result);
            // Creating ParserTask


            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }


    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                Log.d("address","Address_Response:::"+jsonData[0]);

                //Log.d("address","Address_Response___1:::"+jsonData[1]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

                Log.d("address","Places_Response:::"+places);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            try {
                Log.d("result", "result_onPostExecute = " + result);
                String[] from = new String[]{"description"};
                int[] to = new int[]{android.R.id.text1};

                ArrayList<String> resultList = new ArrayList<String>();

                if (resultList != null) {


                    for (HashMap<String, String> map : result) {
                        resultList.addAll(map.values());
                    }


                    // Creating a SimpleAdapter for the AutoCompleteTextView
                    SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

                    // Setting the adapter
                    autocomplete_text_places.setAdapter(adapter);

                    adapter.notifyDataSetChanged();
                }
            }catch (Exception e){e.printStackTrace();}



        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_delete:
                autocomplete_text_places.setText("");
               // image_search.setVisibility(View.VISIBLE);
                //image_delete.setVisibility(View.GONE);
                break;

            case R.id.image_search:
                //image_search.setVisibility(View.GONE);
               // image_delete.setVisibility(View.VISIBLE);
                String address = autocomplete_text_places.getText().toString();
//                convertAddress(address);
                networkCallForConvertAddress(address, 0, true);
                break;

            case R.id.button_select_location:
                try {
                    if (autocomplete_text_places.getText().toString().length() != 0) {
                        assignLatLang();
                        networkCallForConvertAddress(autocomplete_text_places.getText().toString(), 1, true);

                        Log.d("Bundle", "Bundle" + "button_select_location");
                        Bundle b = new Bundle();
                        b.putString("location", autocomplete_text_places.getText().toString());
                        b.putDouble("latitude", lat);
                        b.putDouble("longitude", longi);
                        Intent i = getIntent();
                        i.putExtras(b);
                        setResult(Activity.RESULT_OK, i);
                        finish();
                    } else {


                        Toast.makeText(this, getResources().getString(R.string.select_location), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.image_mylocation:
                try {
                    map.clear();
//                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new
//                        LatLng(location.getLatitude(),
//                        location.getLongitude()), 15));
                    //autocomplete_text_places.setText("");
                    autocomplete_text_places.setText(networkCallForLatLng(location.getLatitude(), location.getLongitude()));
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    //map.addMarker(new MarkerOptions().position(latLng));
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    map.animateCamera(CameraUpdateFactory.zoomTo(17));
                    map.getUiSettings().setMyLocationButtonEnabled(false);



                }catch (Exception e){e.printStackTrace();}
                break;

            default:
                break;
        }

        Utils.closeKeyBoard(MapActivity.this,autocomplete_text_places);
    }



    /*public void convertAddress(final String address) {
        if (address != null && !address.isEmpty()) {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {

            try {
                Geocoder geoCoder = new Geocoder(MapActivity.this,Locale.getDefault());
                ArrayList<Address> addresses = (ArrayList<Address>) geoCoder.getFromLocationName(address, 50);
                for(Address add : addresses){
                    if (add!=null) {//Controls to ensure it is right address such as country etc.
                        double longitude = add.getLongitude();
                        double latitude = add.getLatitude();
                        LatLng latLng = new LatLng(latitude, longitude);
                        map.addMarker(new MarkerOptions().position(latLng));
                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        map.animateCamera(CameraUpdateFactory.zoomTo(15));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

//                }
//            }).start();
        }
    }*/


    //address to lat lang
    private void networkCallForConvertAddress(final String address, final int flag, final boolean isConvertAddress){
        Log.d("address 8","address"+address);
        Log.d("address 8","address"+isConvertAddress);
        if(address!=null) {
            if (Utils.isNetworkAvailable(MapActivity.this)) {


                Call<GoogleLatLng> convertAddress = apiInterface.getLatLng(address,
                        getResources().getString(R.string.geo_coding_key));
                Utils.showProgressDialog(MapActivity.this);
                convertAddress.enqueue(new Callback<GoogleLatLng>() {
                    @Override
                    public void onResponse(Call<GoogleLatLng> call, Response<GoogleLatLng> response) {
                        Utils.hideProgressDialog();
                        try {
                            Log.i("address res", "address"+response.body().toString());
                            if (response.body().getStatus().equals("OK")) {
                                Log.i("address res", response.body().toString());
                                double latitude = response.body().getResults().get(0).getGeometry().getLocation().getLat();
                                double longitude = response.body().getResults().get(0).getGeometry().getLocation().getLng();

                                Log.d("lat lang","lat lang = "+latitude+", "+longitude);
                                if(isConvertAddress) {
                                   lat = latitude;
                                   longi = longitude;
                               }else{
                                    assignLatLang();
                                }
                                if(flag == 0) {
                                    cameraMovedProgrammatically = true;
                                    Log.d("CAMERA","cameraMovedProgrammatically = true::::3");
                                    LatLng latLng = new LatLng(latitude, longitude);
                                    //map.addMarker(new MarkerOptions().position(latLng));
                                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                    map.animateCamera(CameraUpdateFactory.zoomTo(17));
                                }else if(flag == 1){

                                    Log.d("lat lang","Bundle_lat langi = "+lat+", "+longi);
                                    Bundle b = new Bundle();
                                    b.putString("location", autocomplete_text_places.getText().toString());
                                    b.putDouble("latitude",lat);
                                    b.putDouble("longitude",longi);
                                    Intent i = getIntent();
                                    i.putExtras(b);
                                    setResult(Activity.RESULT_OK, i);
                                    finish();
                                }
                            }else if(response.body().getStatus().equals("OVER_QUERY_LIMIT")){

                                Utils.showToast(MapActivity.this, getResources().getString(R.string.limitexceed_error));

                            }
                            else if(response.body().getStatus().equals("ZERO_RESULTS")){


                                // Getting location details and show on map
                                Geocoder geo = new Geocoder(MapActivity.this, Locale.getDefault());
                                try {
                                    List<Address> addresses = geo.getFromLocationName(address, 1);

                                    if (addresses == null && addresses.isEmpty()) {
                                        // ignore
                                        // //Log.d(LOG_TAG, "addresses " + addresses);
                                    } else {
                                        if (addresses.size() > 0) {
                                            // //Log.d(LOG_TAG, "addresses " + addresses.get(0));
                                            LatLng newposition = new LatLng(addresses.get(0)
                                                    .getLatitude(), addresses.get(0).getLongitude());
                                            // googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                            // newposition, ZOOM_LEVEL));
                                            //
                                            double latitude = newposition.latitude;
                                            double longitude = newposition.longitude;
                                            Log.d("lat lang","lat lang = "+latitude+", "+longitude);
                                            if(isConvertAddress) {
                                                lat = latitude;
                                                longi = longitude;
                                            }else{
                                                assignLatLang();
                                            }
                                            if(flag == 0) {
                                                cameraMovedProgrammatically = true;
                                                Log.d("CAMERA","cameraMovedProgrammatically = true::::4");
                                                LatLng latLng = new LatLng(latitude, longitude);
                                                //map.addMarker(new MarkerOptions().position(latLng));
                                                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                                map.animateCamera(CameraUpdateFactory.zoomTo(17));
                                            }else if(flag == 1){

                                                Log.d("lat lang","Bundle_lat langi = "+lat+", "+longi);
                                                Bundle b = new Bundle();
                                                b.putString("location", autocomplete_text_places.getText().toString());
                                                b.putDouble("latitude",lat);
                                                b.putDouble("longitude",longi);
                                                Intent i = getIntent();
                                                i.putExtras(b);
                                                setResult(Activity.RESULT_OK, i);
                                                finish();
                                            }

                                        } else {

                                        }

                                    }

                                    // InputMethodManager mgr = (InputMethodManager) getContext()
                                    // .getSystemService(Context.INPUT_METHOD_SERVICE);
                                    // mgr.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(),
                                    // 0);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                            }

                            else {
                                //Utils.showToast(MapActivity.this, getResources().getString(R.string.limitexceed_error));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<GoogleLatLng> call, Throwable t) {
                        t.printStackTrace();
                        Utils.hideProgressDialog();
                        Utils.showToast(MapActivity.this, getResources().getString(R.string.network_slow));

                    }
                });


            } else {
                Utils.showToast(MapActivity.this, getResources().getString(R.string.network_error));
            }
        }
    }


    private String networkCallForLatLng(final double latitude, final double longitude) {
        lat = latitude;
        longi = longitude;
        if (Utils.isNetworkAvailable(MapActivity.this)) {


            Call<GoogleAddress> getLatLng = apiInterface.getAddress(latitude+","+longitude,
                    getResources().getString(R.string.geo_coding_key));
            Utils.hideProgressDialog();
            Utils.showProgressDialog(MapActivity.this);
            Log.d("latLng response","starting progress dialog");
            getLatLng.enqueue(new Callback<GoogleAddress>() {
                @Override
                public void onResponse(Call<GoogleAddress> call, Response<GoogleAddress> response) {
                    Utils.hideProgressDialog();
                    Log.d("latLng response","Stopping progress dialog");
                    try{

                        if(response.body().getStatus().equalsIgnoreCase("OK")) {
                            address = response.body().getResults().get(0).getFormatted_address();
                                Log.i("address response", Arrays.asList(response.body().getResults()).toString());




                        }
                        else{
                            address = latitude+","+longitude;
                        }
                        autocomplete_text_places.setText(address);
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<GoogleAddress> call, Throwable t) {
                    t.printStackTrace();
                    Utils.hideProgressDialog();
                    Utils.showToast(MapActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        }else{
            Utils.showToast(MapActivity.this,getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
            Log.d("latLng response","Stopping progress dialog_IN ELSE");
        }

        return address;
    }


   /* String result = null;
    private String getAddress(final double latitude, final double longitude){
//        this.latitude = latitude;
//        this.longitude = longitude;

        new Thread(new Runnable() {
            @Override
            public void run() {

        try{
            Geocoder geocoder = new Geocoder(MapActivity.this, Locale.getDefault());
            List<android.location.Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                android.location.Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                result = sb.toString();
            }
        }
        catch (Exception e){
            e.printStackTrace();;
            result = latitude+" , "+longitude;
        }
            }
        }).start();;
        return  result;
    }
*/


    private void assignLatLang(){
       LatLng latLang =  map.getCameraPosition().target;

        lat = latLang.latitude;
        longi = latLang.longitude;
    }

}


/*
Maintain a flag to check if the search button is clicked or
the location is selected from the drop down. If the search location is clicked then
perform as it is.
or else dont convert lat long to address
 */
