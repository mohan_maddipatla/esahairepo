package com.esahai.dataObjects;

/**
 * Created by ragamai on 30/9/16.
 */
public class StatusResponse {

    /*
    {"statusCode":"300","statusMessage":"updated successfully"}
     */

    public String statusCode;
    public String statusMessage;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "StatusResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}
