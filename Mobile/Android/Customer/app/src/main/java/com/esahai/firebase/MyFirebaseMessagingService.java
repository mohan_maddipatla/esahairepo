package com.esahai.firebase;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.activities.DashboardActivity;
import com.esahai.activities.LoginActivity;
import com.esahai.activities.SearchAmbulanceActivity;
import com.esahai.activities.TrackAmbulanceActivity;
import com.esahai.activities.TripDetailsActivity;
import com.esahai.dataObjects.BookingConfirmedDetails;
import com.esahai.dataObjects.BookingStatus;
import com.esahai.dataObjects.ComeToKnowBy;
import com.esahai.dataObjects.EmergencyDetails;
import com.esahai.dataObjects.EmergencyType;
import com.esahai.dataObjects.EmergencyTypeList;
import com.esahai.dataObjects.FaqDetails;
import com.esahai.dataObjects.FaqsList;
import com.esahai.dataObjects.KnowBy;
import com.esahai.dataObjects.PrivacyPolicyResponse;
import com.esahai.dataObjects.PromoDetails;
import com.esahai.dataObjects.TermsAndConditionsResponse;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.moengage.pushbase.push.MoEngageNotificationUtils;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prashanth on 29/9/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    //pn types

    private String LOCATION_UPDATE = "ambulanceUpdate";
    private String BOOKING_CONFIRMED = "booking_confirmed";
    private String TRIP_STATUS = "bookingStatusUpdate";
    private String CANCELLED = "cancel_booking";
    private String CANCELLED_AMBULANCE = "cancelledBooking";
    private String COMMAND_CENTRE_CANCELLED = "command_centre_cancelled_booking";
    private String BLOOD_REQUEST = "blood_request";
    private String TRANSFER_BOOKING = "booking_transferred";
    private String ASSIGNING = "ambulanceTrasnferredTheBooking";
    private String AMBULANCE_TRANSFER = "amb_transferred_booking_confirmed";
    private String LOGOUT = "customer_logout";


    private String FAQ = "/topics/FAQ";
    private String PRIVACY_POLICY = "/topics/PRIVACY-POLICY";
    private String TNC = "/topics/TNC";
    private String COME_TO_KNOW = "/topics/HOW-DO-YOU-KNOW";
    private String EMERGENCIES = "/topics/EMERGENCY-TYPES";

    private String BANNER = "new_banner";

    BookingConfirmedDetails bookingDetails;
    AppPreferences preferences;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        preferences = new AppPreferences(this);
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getFrom().startsWith("/topics/")) {
            preferences = new AppPreferences(getBaseContext());
            if (remoteMessage.getFrom().equalsIgnoreCase(FAQ)) {
                handleFAQ();
            }
            if(remoteMessage.getFrom().equalsIgnoreCase(PRIVACY_POLICY)){
                handlePrivacyPolicy();
            }
            if(remoteMessage.getFrom().equalsIgnoreCase(TNC)){
                handleTermsAndConditions();
            }
            if(remoteMessage.getFrom().equalsIgnoreCase(COME_TO_KNOW)){
                handleComeToKnowBy();
            }
            if(remoteMessage.getFrom().equalsIgnoreCase(EMERGENCIES)){
                handleEmergencies();
            }

        } else {
            if (remoteMessage.getData().size() > 0) {

                // extras is push payload
                if (MoEngageNotificationUtils.isFromMoEngagePlatform(remoteMessage.getData())
                        && !MoEngageNotificationUtils.isSilentPush(Bundle.EMPTY)) {
                    // Show Notification
                    //Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ambulance_icn);
                    //sendNotificationWithImage(largeIcon,"eSahai","Do you need an ambulance?");

                }

                Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                //sendNotification( remoteMessage.getData().toString());
                Map<String, String> data = remoteMessage.getData();


                if(data.get("type")!= null) {
                    String type = data.get("type");
//                sendNotification("Hai");

                    Log.d(TAG, "type type: " + type);
                    if (type.equalsIgnoreCase(BOOKING_CONFIRMED) || type.equalsIgnoreCase(TRANSFER_BOOKING)
                            || type.equalsIgnoreCase(AMBULANCE_TRANSFER)) {
//                    sendNotification("Booking Confirmed");
                        sendNotification(type);
                        bookingDetails = new BookingConfirmedDetails();
                        preferences.setTripState(true);
                        if (type.equalsIgnoreCase(BOOKING_CONFIRMED)) {
                            Utils.setAmbulanceStatus(Constants.ACCEPTED);
                        }
                        try {

                            JSONObject jsonObject = new JSONObject(data.get("title"));
//                        Type listType = new TypeToken<BookingConfirmedDetails>(){}.getType();
//                        Gson gson = new Gson();
//                        bookingDetails = gson.fromJson(jsonObject);

                            bookingDetails.setDistance(jsonObject.getString("distance"));
                            bookingDetails.setDriverMobileNumber(jsonObject.getString("driverMobileNumber"));
                            bookingDetails.setDriverName(jsonObject.getString("driverName"));
                            bookingDetails.setDuration(jsonObject.getString("duration"));
                            bookingDetails.setAmbulanceHospital(jsonObject.getString("ambulanceHospital"));
                            bookingDetails.setAmbulanceNumber(jsonObject.getString("ambulanceNumber"));
                            bookingDetails.setDriverProfilePic(jsonObject.getString("driverProfilePic"));
                            bookingDetails.setEmergencyLatitude(jsonObject.getDouble("emergencyLatitude"));
                            bookingDetails.setEmergencyLongitude(jsonObject.getDouble("emergencyLongitude"));
                            bookingDetails.setAmbulance_latitude(jsonObject.getDouble("ambulance_latitude"));
                            bookingDetails.setAmbulance_longitude(jsonObject.getDouble("ambulance_longitude"));
                            bookingDetails.setIs_medical_taxi(jsonObject.getBoolean("is_medical_taxi"));


                            preferences.setBookingDetails(bookingDetails);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(this, TrackAmbulanceActivity.class);
                        intent.putExtra("bookingDetails", bookingDetails);
                        if (type.equalsIgnoreCase(AMBULANCE_TRANSFER) || type.equalsIgnoreCase(TRANSFER_BOOKING)) {
                            intent.putExtra("transferred", "transferred");
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }

                /*if(type.equalsIgnoreCase(TRANSFER_BOOKING)){

                }*/


                    if (type.equalsIgnoreCase(LOCATION_UPDATE)) {
                        TrackAmbulanceActivity.lastPnReceivedTime = System.currentTimeMillis();
                        double latitude = Double.parseDouble(data.get("latitude"));
                        double longitude = Double.parseDouble(data.get("longitude"));
                        Utils.updateLatLng(latitude, longitude);
//                    sendNotification("location update");
                    }
                    if (type.equalsIgnoreCase(TRIP_STATUS)) {
                        Log.i("booking status", data.get("booking_status"));

                        TrackAmbulanceActivity.lastPnReceivedTime = System.currentTimeMillis();
                    /*
                    {hospitalLong=78.385301,
                    booking_status=Way back to Hospital,
                    hospitalLat=17.443306, type=bookingStatusUpdate}
                     */

//                    try {
                        String hospitalLat = data.get("hospitalLat");
                        String hospitalLong = data.get("hospitalLong");
                        preferences.setHospitalLat(hospitalLat);
                        preferences.setHospitalLng(hospitalLong);
                        String status = data.get("booking_status");

                        Utils.setAmbulanceStatus(status);
                        if (status.equalsIgnoreCase(Constants.STARTED)) {
                            sendNotification("Started");
                            if (!preferences.isTrip()) {
                                getActiveBooking();
                            }
                            preferences.setTripState(true);
                        }
                        if (status.equalsIgnoreCase(Constants.ACCEPTED)) {
                            sendNotification("Accepted");
                        }
                        if (status.equalsIgnoreCase(Constants.WAY_TO_EMERGENCY_LOCATION)) {
                            sendNotification("Way to emergency location");
                        }
                        if (status.equalsIgnoreCase(Constants.AT_EMERGENCY_LOCATION)) {
                            sendNotification("At emergency location");
                        }

                        if (status.equalsIgnoreCase(Constants.WAY_BACK_TO_DESTINATION)) {
                            sendNotification("Way back to destination");
//                        boolean canCancel = false;
                            //Utils.setBookingCancel(false);
                            preferences.setBookingCancellable(false);
                        }
                        if (status.equalsIgnoreCase(Constants.REACHED_DESTINATION)) {
                            sendNotification("Reached destination");
                        }
                        if (status.equalsIgnoreCase(Constants.TRIP_CLOSED)) {
                            //Utils.setBookingCancel(true);
                            preferences.setBookingCancellable(true);
                            Utils.setAmbulanceStatus("");
                            preferences.setIsSelf(true);

                            sendNotification("Trip closed");

                            preferences.setTripState(false);
                            Utils.setAmbulanceStatus("");
//                        boolean canCancel = true;
                            // {duration=35901, booking_status=Trip Closed, cost=0, type=bookingStatusUpdate, speed=50, totalDistance=2445}
                            EmergencyDetails bookingDetails = new EmergencyDetails();
                            bookingDetails.setDuration(data.get("duration"));
                            bookingDetails.setCost(data.get("cost"));
                            bookingDetails.setDistance(data.get("totalDistance"));
                            bookingDetails.setSpeed(data.get("speed"));

                            Intent intent = new Intent(this, TripDetailsActivity.class);
                            intent.putExtra("trip_closed_details", bookingDetails);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
//                    }catch (Exception e){e.printStackTrace();}
                    }

                    if (type.equalsIgnoreCase(CANCELLED) || type.equalsIgnoreCase(CANCELLED_AMBULANCE)
                            || type.equalsIgnoreCase(COMMAND_CENTRE_CANCELLED)) {
                        preferences.setIsSelf(true);
                        preferences.setTripState(false);
                        Utils.setAmbulanceStatus("");
                        Intent intent = new Intent(this, DashboardActivity.class);
                        intent.putExtra("cancelled", "cancelled");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        try {
                            NotificationManager notificationManager =
                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.cancelAll();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                    showAlert(this,"Your trip has been cancelled.\n For more details please contact support@esahai.com.");
                    }

                    if (type.equalsIgnoreCase(ASSIGNING)) {
                        preferences.setTripState(false);
                        Utils.setAmbulanceStatus("");
                        Intent intent = new Intent(this, SearchAmbulanceActivity.class);
                        intent.putExtra("assigning", "assigning");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                    if (type.equalsIgnoreCase(BLOOD_REQUEST)) {
                        try {
                            String title = remoteMessage.getData().get("title");
                            Log.i("blood---", title);
                            PromoDetails promoDetails = new PromoDetails();
                            promoDetails.setTitle(title);
                            EsahaiApplication.myApplication.notifyObservers(Constants.BLOOD_REQUEST, promoDetails);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (type.equalsIgnoreCase(LOGOUT)) {
                        preferences.clearPreferences();
                        EsahaiApplication.myApplication.notifyObservers(Constants.LOGOUT, "");
                    }

                    if (type.equalsIgnoreCase(BANNER)) {
                        try {
                            JSONObject jsonObject = new JSONObject(data.get("title"));
                            String image = jsonObject.getString("bannerPath");
                            String title = jsonObject.getString("title");
                            String description = jsonObject.getString("description");
                            Bitmap bitmap = Utils.getBitmapFromUrl(image);
                            sendNotificationWithImage(bitmap, title, description);
                            PromoDetails promoDetails = new PromoDetails();
                            promoDetails.setTitle(title);
                            promoDetails.setBanner_path(image);
                            promoDetails.setDescription(description);
                            EsahaiApplication.getApplication().notifyObservers(Constants.DISPLAY_NOTIFICATION, promoDetails);
//                        NotificationDisplay notificationDisplay = new CommonActivity();
//                        invoke(notificationDisplay,image,title,description);
                        /*Intent intent = new Intent(this, BaseActivity.class);
                        intent.putExtra("banner", image);
                        intent.putExtra("description", description);
                        intent.putExtra("title",title);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            }

            // Check if message contains a notification payload.
            /*if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            }
//            sendNotification(remoteMessage.getNotification().getBody());*/

        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void sendNotificationWithImage(Bitmap bitmap, String title, String description) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, new Intent(),
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.BigPictureStyle notifyStyle = new NotificationCompat.BigPictureStyle()
//                .setSmallIcon(getNotificationIcon())
                .setBigContentTitle(title)
                .setSummaryText(description)
//                .setStyle(new NotificationCompat.BigPictureStyle()
//                        .bigPicture(bitmap))/*Notification with Image*/
                .bigPicture(bitmap);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this);

        Notification notification = mBuilder
                .setSmallIcon(getNotificationIcon())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(notifyStyle)
                .setContentTitle(title)
                .setContentText(description)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentIntent(pendingIntent).build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1 /* ID of notification */, notification);
    }

    // [END receive_message]



    public static int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.online_notification : R.drawable.online_notification;
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        preferences = new AppPreferences(this);
        if (messageBody.equalsIgnoreCase(BOOKING_CONFIRMED) || messageBody.equalsIgnoreCase(TRANSFER_BOOKING) ||
                messageBody.equalsIgnoreCase(AMBULANCE_TRANSFER)) {

            Intent intent = new Intent(this, TrackAmbulanceActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.putExtra("bookingDetails", preferences.getBookingDetails());
            PendingIntent pendingIntent;
            if (preferences.isTrip() == true) {
                pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);
            } else {
                pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, new Intent(),
                        PendingIntent.FLAG_ONE_SHOT);
            }
            String title = null;
            if(messageBody.equalsIgnoreCase(BOOKING_CONFIRMED)){
                title = "Booking confirmed";
            }else if(messageBody.equalsIgnoreCase(TRANSFER_BOOKING)){
                title = "Booking Transferred";
            }

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle(title)
                    .setContentText("Your booking is confirmed")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setColor(getResources().getColor(
                            R.color.colorPrimary));


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } else if (messageBody.equalsIgnoreCase("Location update")) {
           /* Intent intent = new Intent(this, BookingConfirmedActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, new Intent(),
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle("Location update")
                    .setContentText("Ambulance location update")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setColor(getResources().getColor(
                            R.color.colorPrimary));

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } else /*if(messageBody.equalsIgnoreCase("booking_status"))*/ {
           /* Intent intent = new Intent(this, BookingConfirmedActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
            Intent intent;
            if (messageBody.trim().equalsIgnoreCase("Trip closed")) {
                intent = new Intent(this, TripDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                intent.putExtra("status", "locationUpdate");
            }else if(messageBody.trim().equalsIgnoreCase(Constants.STARTED)){
                intent = new Intent(this, TrackAmbulanceActivity.class);
            } else {

                Log.d("intent","IN_ELSE");
                if (preferences.isTrip() == true){
                    Log.d("intent","intent is true_TrackActivity");
                    intent = new Intent(this, TrackAmbulanceActivity.class);
                }else{
                    Log.d("intent","intent is false_Dashboard");
                    EsahaiApplication.myApplication.notifyObservers(EsahaiApplication.bookingStatusUpdate,"");
                    intent = new Intent(this, DashboardActivity.class);
                }

                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.putExtra("status", "locationUpdate");
            }
            PendingIntent pendingIntent;
           // if (preferences.isTrip() == true) {
                pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);
            //} else {
              //  pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, new Intent(),
                //        PendingIntent.FLAG_ONE_SHOT);
            //}

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle("Trip")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setColor(getResources().getColor(
                            R.color.colorPrimary));

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }



    public void handlePrivacyPolicy() {
        if (Utils.isNetworkAvailable(getBaseContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<PrivacyPolicyResponse> getPolicy = apiService.getPrivacyPolicy();

            Log.i("policy response", "Calling the P&P API");
            getPolicy.enqueue(new Callback<PrivacyPolicyResponse>() {
                @Override
                public void onResponse(Call<PrivacyPolicyResponse> call, Response<PrivacyPolicyResponse> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("policy response", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                String privacyPolicy = response.body().getResponseData().getPrivacy_policy();
                                preferences.setPrivacyPolicy(privacyPolicy);
                            }
                        }

                    } catch (Exception e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PrivacyPolicyResponse> call, Throwable t) {
                    t.printStackTrace();
//                    Utils.hideProgressDialog();
//                    Utils.showToast(PrivacyPolicyActivity.this, getResources().getString(R.string.network_slow));

                }
            });

        } else {

//            Utils.hideProgressDialog();
//            Utils.showToast(PrivacyPolicyActivity.this,getResources().getString(R.string.network_error));}

        }
    }

    private void handleFAQ() {
        if (Utils.isNetworkAvailable(getBaseContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            HashMap<String, Object> faqRequest = new HashMap<>();
            faqRequest.put("customerApp", true);
            faqRequest.put("ambulanceApp", false);
            faqRequest.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

            Log.i("faq request", Arrays.asList(faqRequest).toString());
            Call<FaqsList> faqsList = apiService.getFaqsList(faqRequest, preferences.getCustomerId(), preferences.getAccessToken());
            faqsList.enqueue(new Callback<FaqsList>() {
                @Override
                public void onResponse(Call<FaqsList> call, Response<FaqsList> response) {
                    try {
                        if (response != null) {
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                ArrayList<FaqDetails> faqs = response.body().getResponseData();

                                Log.d("faqs", "faqs_Response" + faqs);
                                if (faqs != null && faqs.size() > 0) {
//                                    Utils.setFAQsList(faqs);
                                    preferences.setFAQs(faqs);
                                }
                            }
                        } else {
                            Log.d("Faq", "Response is null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<FaqsList> call, Throwable t) {
//                    Utils.showToast(getBaseContext(), getResources().getString(R.string.network_slow));
                }
            });


        } else {
//            Utils.showToast(getBaseContext(), getResources().getString(R.string.network_error));
        }
    }

    private void handleTermsAndConditions() {
        if (Utils.isNetworkAvailable(getBaseContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<TermsAndConditionsResponse> getTerms = apiService.getTermsAndConditions();

            Log.i("terms response", "Calling the T&C APIr");
            getTerms.enqueue(new Callback<TermsAndConditionsResponse>() {
                @Override
                public void onResponse(Call<TermsAndConditionsResponse> call, Response<TermsAndConditionsResponse> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("terms response", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                preferences.setTNC(response.body().getResponseData().get(0).getTerms_and_conditions());
                            }
                        }

                    } catch (Exception e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<TermsAndConditionsResponse> call, Throwable t) {

                }
            });

        }
    }


    private void handleComeToKnowBy(){
        if(Utils.isNetworkAvailable(getBaseContext())){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ComeToKnowBy> getTerms = apiService.getComeToKnow();
            getTerms.enqueue(new Callback<ComeToKnowBy>() {
                @Override
                public void onResponse(Call<ComeToKnowBy> call, Response<ComeToKnowBy> response) {
                    try{
                        if(response.body()!=null){
                            if(Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE){
                                ArrayList<KnowBy> knowBy = response.body().getResponseData();
                                preferences.setComeToKnow(knowBy);
                            }
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<ComeToKnowBy> call, Throwable t) {

                }
            });
        }
    }

    private void handleEmergencies() {
        if (Utils.isNetworkAvailable(getBaseContext())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<EmergencyTypeList> emergencyTypeListCall = apiService.getEmergencyTypes();
            emergencyTypeListCall.enqueue(new Callback<EmergencyTypeList>() {
                @Override
                public void onResponse(Call<EmergencyTypeList> call, Response<EmergencyTypeList> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("emergenciesList", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                ArrayList<EmergencyType> emergencyList = new ArrayList<EmergencyType>();
                                emergencyList = response.body().getResponseData();
                                preferences.setEmergencies(emergencyList);

                                if (emergencyList != null) {
                                    for (int i = 0; i < emergencyList.size(); i++) {
                                        if(emergencyList.get(i).getType_name().equalsIgnoreCase("Medical Taxi")){
                                            preferences.setMedicalTaxiId(emergencyList.get(i).getId());
                                        }
                                    }
                                }

                            }
                        }
                        else{
                            Log.d("Response","Response_dashboard is null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<EmergencyTypeList> call, Throwable t) {

                }
            });

        }

    }

    private void getActiveBooking() {


        Log.d("Activebooking", "ActivebookingID");
        try {
            if (Utils.isNetworkAvailable(this)) {
                HashMap<String, Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id", preferences.getCustomerId());
                bookingDetails.put("ambulanceApp", false);
                bookingDetails.put("customerApp", true);
                bookingDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

                Log.i("Activebooking", Arrays.asList(bookingDetails).toString());
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<BookingStatus> activeBooking = apiService.getActiveBooking(bookingDetails, preferences.getCustomerId(), preferences.getAccessToken());
                //Utils.showProgressDialog(SearchAmbulanceActivity.this);
                activeBooking.enqueue(new Callback<BookingStatus>() {
                    @Override
                    public void onResponse(Call<BookingStatus> call, Response<BookingStatus> response) {
                        try {
                            if (response.body() != null) {
                                Log.i("Activebooking ", "active booking response!=NULL" + response.body().toString());

                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {


                                    preferences.setBookingDetails(response.body().getResponseData());

                                    preferences.setBookingId(response.body().getResponseData().booking_id);

                                    preferences.setHospitalLat(response.body().getResponseData().hospital_latitude);
                                    preferences.setHospitalLng(response.body().getResponseData().hospital_longitude);
                                    preferences.setMedicalTaxi(response.body().getResponseData().is_medical_taxi());
//                                    preferences.setPatient_MobileNumber("");
                                    /*if(response.body().getResponseData().booking_status.
                                            equalsIgnoreCase("Way Back to Hospital")){
                                        preferences.setBookingCancellable(false);
                                        Log.d("ActiveBooking","Way back to hospital");
                                    }else{
                                        preferences.setBookingCancellable(true);
                                    }*/
                                    preferences.setTripState(true);
                                    Utils.setAmbulanceStatus(response.body().getResponseData().booking_status);

                                    Intent intent = new Intent(getBaseContext(), TrackAmbulanceActivity.class);
                                    intent.putExtra("bookingDetails", response.body().getResponseData());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);

                                } else {

                                    Log.d("booking", "status_message" + response.body().getStatusMessage());

                                }

                            } else {
                                Log.d("Activebooking", "Activebooking_Error" + response.body().getStatusMessage());

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<BookingStatus> call, Throwable t) {
//                        Utils.hideProgressDialog();
//                        Utils.showToast(getBaseContext(), getResources().getString(R.string.network_slow));

                    }
                });


            } else {
                Log.d("Activebooking", "Activebooking_No internet");
//                Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_error));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
