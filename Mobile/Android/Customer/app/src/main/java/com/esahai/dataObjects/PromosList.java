package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 2/3/17.
 */
public class PromosList {

    /*
    "statusCode": "300",
  "statusMessage": "Promo Banners fetched successfully",
  "Result":
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<PromoDetails> Result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<PromoDetails> getResult() {
        return Result;
    }

    public void setResult(ArrayList<PromoDetails> result) {
        Result = result;
    }

    @Override
    public String toString() {
        return "PromosList{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", Result=" + Result +
                '}';
    }
}
