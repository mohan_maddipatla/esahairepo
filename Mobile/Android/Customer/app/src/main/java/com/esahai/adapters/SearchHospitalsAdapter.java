package com.esahai.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.activities.NearByHospitalsActivity;
import com.esahai.activities.NearestHospitalsActivity;
import com.esahai.dataObjects.HospitalDetails;

import java.util.ArrayList;

/**
 * Created by ragamai on 4/10/16.
 */
public class SearchHospitalsAdapter extends RecyclerView.Adapter<SearchHospitalsAdapter.ViewHolder> implements View.OnClickListener {

    Context context;
    ArrayList<HospitalDetails> hospitals;
    RecyclerView recycle_list_hospitals;
    double firstDis,first,second = 0.0;
    String distanceText = "",distanceText1 = "";


    public SearchHospitalsAdapter(Context context, ArrayList<HospitalDetails> hospitals, RecyclerView recycle_list_hospitals) {
        this.context = context;
        this.hospitals = hospitals;
        this.recycle_list_hospitals = recycle_list_hospitals;
    }

    @Override
    public SearchHospitalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.hospitals_list_item, parent, false);
        v.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SearchHospitalsAdapter.ViewHolder holder, int position) {
            try {
                if(!hospitals.get(position).getGroup_name().
                        equalsIgnoreCase(context.getResources().getString(R.string.no_nearby_hospitals))) {
                    firstDis = Double.parseDouble(hospitals.get(position).getDistance());
                    first = 5 * (Math.ceil(Math.abs(firstDis / 5)));
                    distanceText = "Hospitals from (" + Math.round(second) + " - " + Math.round(first) + ") Kms";

                    if (firstDis < first) {

                        if (holder.textView_distance.getText().toString().equalsIgnoreCase(distanceText)) {
                            holder.textView_distance.setVisibility(View.GONE);
                        }

                        if (!holder.textView_distance.getText().toString().equalsIgnoreCase(distanceText)) {
                            if (first != second) {
                                distanceText1 = "Hospitals from (" + Math.round(second) + " - " + Math.round(first) + ") Kms";
                                second = first;
                                holder.textView_distance.setText(distanceText1);
                                holder.textView_distance.setVisibility(View.VISIBLE);
                            } else {
                                holder.textView_distance.setVisibility(View.GONE);
                            }
                        }
                    }

                }

                if (!hospitals.get(position).getGroup_name().equals("")) {
                    holder.textView_hospital_name.setText(hospitals.get(position).getGroup_name());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

    }

    @Override
    public int getItemCount() {
        return hospitals.size();
    }

    @Override
    public void onClick(View v) {
        int itemPosition = recycle_list_hospitals.getChildLayoutPosition(v);
        HospitalDetails hospitalDetails = hospitals.get(itemPosition);

        if(context instanceof NearestHospitalsActivity){
            ((NearestHospitalsActivity)context).mapForSelectedHospital(hospitalDetails);
        }
        if(context instanceof NearByHospitalsActivity){
            ((NearByHospitalsActivity)context).mapForSelectedHospital(hospitalDetails);
        }

    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView textView_hospital_name,textView_hospital_distance,textView_distance;

        public ViewHolder(View view) {
            super(view);
            textView_hospital_name = (TextView)view.findViewById(R.id.textView_hospital_name);
            textView_hospital_distance = (TextView)view.findViewById(R.id.textView_hospital_distance);
            textView_distance = (TextView)view.findViewById(R.id.textView_distance);
            textView_hospital_distance.setVisibility(View.GONE);
            textView_distance.setVisibility(View.GONE);
        }



    }
}
