package com.esahai.dataObjects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ragamai on 4/10/16.
 */
public class BookingConfirmedDetails implements Serializable{

    /*
    "duration": "1 min",
		"emergencyLatitude": 17.4431423,
		"ambulanceHospital": "108 Service",
		"cost": "100",
		"distance": "0.161",
		"driverMobileNumber": "9490012854",
		"driverName": "Prashanth",
		"driverProfilePic": "http:\/\/54.200.233.193\/drivers\/driver-1475323528346.png",
		"ambulanceNumber": "TS09AB108",
		"emergencyLongitude": 78.3852503
     */

    public String duration;
    public double emergencyLatitude;
    public double emergencyLongitude;
    public double ambulance_latitude;
    public double ambulance_longitude;
    public String ambulanceHospital;
    public String cost;
    public String distance;
    public String driverMobileNumber;
    public String driverName;
    public String driverProfilePic;
    public String ambulanceNumber;
    public boolean is_medical_taxi;

    @SerializedName("booking_id")
    public String booking_id;

    @SerializedName("booking_status")
    public String booking_status;

    @SerializedName("hospital_longitude")
    public String hospital_longitude;

    @SerializedName("hospital_latitude")
    public String hospital_latitude;



    public String getHospital_longitude() {
        return hospital_longitude;
    }

    public void setHospital_longitude(String hospital_longitude) {
        this.hospital_longitude = hospital_longitude;
    }

    public String getHospital_latitude() {
        return hospital_latitude;
    }

    public void setHospital_latitude(String hospital_latitude) {
        this.hospital_latitude = hospital_latitude;
    }



    public String getBookingId() {
        return booking_id;
    }

    public void setBookingId(String bookingId) {
        this.booking_id = bookingId;
    }

    public String getBookingStatus() {
        return booking_status;
    }

    public void setBookingStatus(String bookingStatus) {
        this.booking_status = bookingStatus;
    }




    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getEmergencyLatitude() {
        return emergencyLatitude;
    }

    public void setEmergencyLatitude(double emergencyLatitude) {
        this.emergencyLatitude = emergencyLatitude;
    }

    public double getEmergencyLongitude() {
        return emergencyLongitude;
    }

    public void setEmergencyLongitude(double emergencyLongitude) {
        this.emergencyLongitude = emergencyLongitude;
    }

    public String getAmbulanceHospital() {
        return ambulanceHospital;
    }

    public void setAmbulanceHospital(String ambulanceHospital) {
        this.ambulanceHospital = ambulanceHospital;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDriverMobileNumber() {
        return driverMobileNumber;
    }

    public void setDriverMobileNumber(String driverMobileNumber) {
        this.driverMobileNumber = driverMobileNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverProfilePic() {
        return driverProfilePic;
    }

    public void setDriverProfilePic(String driverProfilePic) {
        this.driverProfilePic = driverProfilePic;
    }

    public String getAmbulanceNumber() {
        return ambulanceNumber;
    }

    public void setAmbulanceNumber(String ambulanceNumber) {
        this.ambulanceNumber = ambulanceNumber;
    }

    public double getAmbulance_latitude() {
        return ambulance_latitude;
    }

    public void setAmbulance_latitude(double ambulance_latitude) {
        this.ambulance_latitude = ambulance_latitude;
    }

    public double getAmbulance_longitude() {
        return ambulance_longitude;
    }

    public void setAmbulance_longitude(double ambulance_longitude) {
        this.ambulance_longitude = ambulance_longitude;
    }

    public boolean is_medical_taxi() {
        return is_medical_taxi;
    }

    public void setIs_medical_taxi(boolean is_medical_taxi) {
        this.is_medical_taxi = is_medical_taxi;
    }

    @Override
    public String toString() {
        return "BookingConfirmedDetails{" +
                "duration='" + duration + '\'' +
                ", emergencyLatitude=" + emergencyLatitude +
                ", emergencyLongitude=" + emergencyLongitude +
                ", ambulance_latitude=" + ambulance_latitude +
                ", ambulance_longitude=" + ambulance_longitude +
                ", ambulanceHospital='" + ambulanceHospital + '\'' +
                ", cost='" + cost + '\'' +
                ", distance='" + distance + '\'' +
                ", driverMobileNumber='" + driverMobileNumber + '\'' +
                ", driverName='" + driverName + '\'' +
                ", driverProfilePic='" + driverProfilePic + '\'' +
                ", ambulanceNumber='" + ambulanceNumber + '\'' +
                ", is_medical_taxi=" + is_medical_taxi +
                ", booking_id='" + booking_id + '\'' +
                ", booking_status='" + booking_status + '\'' +
                ", hospital_longitude='" + hospital_longitude + '\'' +
                ", hospital_latitude='" + hospital_latitude + '\'' +
                '}';
    }
}
