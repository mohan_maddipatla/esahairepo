package com.esahai.dataObjects;

/**
 * Created by ragamai on 21/10/16.
 */
public class Address {

    public String formatted_address;

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    @Override
    public String toString() {
        return "Address{" +
                "formatted_address='" + formatted_address + '\'' +
                '}';
    }
}
