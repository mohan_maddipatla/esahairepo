package com.esahai.dataObjects;

import java.io.Serializable;

/**
 * Created by ragamai on 30/9/16.
 */
public class AmbulanceDetails implements Serializable {
    /*
    {"id":"52",
    "ambulance_id":"6",
    "driver_id":"4",
    "status":"Online",
    "created_date":"2016-09-29 17:17:04",
    "updated_date":"2016-09-29 18:42:17",
    "longitude":"78.3922",
    "latitude":"17.4946",
    "geoHashCode":"umumyng5ttpp",
    "distance":"6.114",
    "duration":"16 mins"}
     */

    public String id;
    public String ambulance_id;
    public String driver_id;
    public String status;
    public String created_date;
    public String updated_date;
    public String longitude;
    public String latitude;
    public String geoHashCode;
    public String distance;
    public String duration;
    public String ambulance_number;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmbulance_id() {
        return ambulance_id;
    }

    public void setAmbulance_id(String ambulance_id) {
        this.ambulance_id = ambulance_id;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getGeoHashCode() {
        return geoHashCode;
    }

    public void setGeoHashCode(String geoHashCode) {
        this.geoHashCode = geoHashCode;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAmbulance_number() {
        return ambulance_number;
    }

    public void setAmbulance_number(String ambulance_number) {
        this.ambulance_number = ambulance_number;
    }

    @Override
    public String toString() {
        return "AmbulanceDetails{" +
                "id='" + id + '\'' +
                ", ambulance_id='" + ambulance_id + '\'' +
                ", driver_id='" + driver_id + '\'' +
                ", status='" + status + '\'' +
                ", created_date='" + created_date + '\'' +
                ", updated_date='" + updated_date + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", geoHashCode='" + geoHashCode + '\'' +
                ", distance='" + distance + '\'' +
                ", duration='" + duration + '\'' +
                ", ambulance_number='" + ambulance_number + '\'' +
                '}';
    }
}
