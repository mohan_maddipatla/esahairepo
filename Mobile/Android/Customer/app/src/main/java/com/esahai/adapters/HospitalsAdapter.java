package com.esahai.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esahai.R;
import com.esahai.activities.NearByHospitalsActivity;
import com.esahai.dataObjects.HospitalDetails;

import java.util.ArrayList;

/**
 * Created by ragamai on 4/10/16.
 */
public class HospitalsAdapter extends RecyclerView.Adapter<HospitalsAdapter.ViewHolder> implements View.OnClickListener {

    Context context;
    ArrayList<HospitalDetails> hospitals;
    RecyclerView recycle_list_hospitals;


    public HospitalsAdapter(Context context, ArrayList<HospitalDetails> hospitals, RecyclerView recycle_list_hospitals) {
        this.context = context;
        this.hospitals = hospitals;
        this.recycle_list_hospitals = recycle_list_hospitals;
    }

    @Override
    public HospitalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.hospitals_list_item, parent, false);
        v.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HospitalsAdapter.ViewHolder holder, int position) {
            try {
                if(!hospitals.get(position).getGroup_name().equals("")) {
                    holder.textView_hospital_name.setText(hospitals.get(position).getGroup_name());
                }
                if(!hospitals.get(position).getDistance().equals("")) {
                    holder.textView_hospital_distance.setText(hospitals.get(position).getDistance() + " Kms");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

    }

    @Override
    public int getItemCount() {
        return hospitals.size();
    }

    @Override
    public void onClick(View v) {
        int itemPosition = recycle_list_hospitals.getChildLayoutPosition(v);
        HospitalDetails hospitalDetails = hospitals.get(itemPosition);

        if(context instanceof NearByHospitalsActivity){
            ((NearByHospitalsActivity)context).mapForSelectedHospital(hospitalDetails);
        }

    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView textView_hospital_name,textView_hospital_distance;

        public ViewHolder(View view) {
            super(view);
            textView_hospital_name = (TextView)view.findViewById(R.id.textView_hospital_name);
            textView_hospital_distance = (TextView)view.findViewById(R.id.textView_hospital_distance);
        }



    }
}
