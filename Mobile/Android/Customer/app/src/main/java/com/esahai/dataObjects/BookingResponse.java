package com.esahai.dataObjects;

/**
 * Created by ragamai on 30/9/16.
 */
public class BookingResponse {
    /*
    "statusCode": "300",
 	"statusMessage": "your request saved successfully",
     */

    public BookingDetails responseData;
    public String statusCode;
    public String statusMessage;


    public BookingDetails getResponseData() {
        return responseData;
    }

    public void setResponseData(BookingDetails responseData) {
        this.responseData = responseData;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "BookingDetailsRequest{" +
                "responseData=" + responseData +
                ", statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}
