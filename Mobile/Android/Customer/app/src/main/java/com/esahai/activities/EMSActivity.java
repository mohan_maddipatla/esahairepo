package com.esahai.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.esahai.Extras.Constants;
import com.esahai.Extras.EsahaiApplication;
import com.esahai.Extras.Utils;
import com.esahai.R;
import com.esahai.Utils.AppObserver;
import com.esahai.adapters.SearchHospitalsAdapter;
import com.esahai.dataObjects.BookingResponse;
import com.esahai.dataObjects.BookingStatus;
import com.esahai.dataObjects.EmergencyType;
import com.esahai.dataObjects.EmergencyTypeList;
import com.esahai.dataObjects.GoogleAddress;
import com.esahai.dataObjects.GoogleLatLng;
import com.esahai.dataObjects.HospitalDetails;
import com.esahai.dataObjects.HospitalList;
import com.esahai.dataObjects.PromoDetails;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EMSActivity extends CommonActivity implements LocationListener, AppObserver,
        RadioGroup.OnCheckedChangeListener, View.OnClickListener {


    ArrayAdapter<String>  emergency_type_adapter;
    LocationManager locationManager;
    String selected_location = "", drop_location = "";
    ApiInterface apiInterface, apiInterface1;
    String emergency_type, hospital;
    AppPreferences preferences;

    LinearLayout layout_hospital;
    LinearLayout layout_location;


    RadioButton privateRadioBtn;
    RadioButton hospitalRadioBtn;
    RadioButton anyRadioBtn;

    RadioGroup radioGroup_destination;
    RadioButton radioButton_location,radioButton_hospital;

    double drop_latitude,drop_longitude, hospital_latitude, hospital_longitude;
    ArrayList<EmergencyType> emergencyList,emergencies;

    String emergency_id;

    String address;
    String group_type_id;
    LinearLayout layout_mobile;
    int is_self = 1;
    EditText editText_phone_number;
    LinearLayout layout_main;
    private Spinner  spinner_emergency;
    private Button callAmbulanceBtn;
    private TextView text_location,textView_drop_location;
    private ArrayList<String> EMERGENCY_TYPE;
    private double latitude;
    private double longitude;
    private int SEARCH_LOCATION = 100;
    private String hospitalId;
    RadioGroup radio_group_ambulance_for;
    RadioButton radio_button_myself,radio_button_someone;

    public static final int timeDifference = 1000 * 30;

    boolean isLocationFetchedFromUser = false;

    boolean isbtnSubmitClicked;

    Location currentLocation = null;

    int DROP_LOCAION = 1002;
    boolean isHospitalsAvailable = true;

    public static final long MIN_TIME = 1000 * 60;
    public static final float MIN_DISTANCE = 100;
    private int SEARCH_HOSPITAL = 101;
    TextView textView_drop_hospital;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ems);

        //Defining toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.ambulance));
        final ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Utils.closeKeyBoard(EMSActivity.this,editText_phone_number);
                    Intent i = new Intent(EMSActivity.this,DashboardActivity.class);
                    startActivity(i);
                    finish();
                }catch (Exception e){e.printStackTrace();}
            }
        });


        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        preferences = new AppPreferences(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface1 = ApiClient.getGoogleClient().create(ApiInterface.class);

        initViews();


        try {
            getLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (Utils.isNetworkAvailable(this)) {

            getActiveBooking();

        } else {
            Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_error));
        }

        try{
            getEmergenciesList();
        }catch (Exception e){e.printStackTrace();}

        setAdaptersForSpinners();

    }

    private void getEmergenciesList() {
        if(preferences.getEmergencies()!=null && preferences.getEmergencies().size() > 0){
            emergencyList = new ArrayList<EmergencyType>();
            emergencies = new ArrayList<EmergencyType>();
            emergencies = preferences.getEmergencies();
             for(int i = 0 ; i < emergencies.size() ; i++){
                if(!emergencies.get(i).getType_name().equalsIgnoreCase("Medical Taxi")) {
                    emergencyList.add(emergencies.get(i));
                }

             }

            Log.i("emer list", Arrays.asList(emergencyList).toString());
        }else{
            networkCallForEmergencies();
        }
    }

    private void networkCallForEmergencies() {
        if (Utils.isNetworkAvailable(EMSActivity.this)) {

            Call<EmergencyTypeList> emergencyTypeListCall = apiInterface.getEmergencyTypes();
            emergencyTypeListCall.enqueue(new Callback<EmergencyTypeList>() {
                @Override
                public void onResponse(Call<EmergencyTypeList> call, Response<EmergencyTypeList> response) {
                    try {
                        if (response.body() != null) {
                            Log.i("emergenciesList", response.body().toString());
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                ArrayList<EmergencyType> emergencyList = new ArrayList<EmergencyType>();
                                emergencyList = response.body().getResponseData();
                                preferences.setEmergencies(emergencyList);

                                if (emergencyList != null ) {
                                    EMERGENCY_TYPE = new ArrayList<String>();
                                    for (int i = 0; i < emergencyList.size(); i++) {
                                        if(!preferences.getEmergencies().get(i).getType_name().equalsIgnoreCase("Medical Taxi")) {
                                            EMERGENCY_TYPE.add(emergencyList.get(i).getType_name());
                                        }
                                        else{preferences.setMedicalTaxiId(emergencyList.get(i).getId());}
                                    }
                                    emergency_type_adapter = new ArrayAdapter<String>(EMSActivity.this,
                                            android.R.layout.simple_dropdown_item_1line, EMERGENCY_TYPE);
                                    spinner_emergency.setAdapter(emergency_type_adapter);
                                }

                            }
                        }
                        else{
                            Log.d("Response","Response_dashboard is null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<EmergencyTypeList> call, Throwable t) {
                    Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_slow));

                }
            });

        } else {
            Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_error));
        }

    }

    private void requestPermissionForLocation() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1338);
    }

    private void getLocation() {
        locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionForLocation();
        } else {
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);


                Location location;
                location = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location == null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                latitude = location.getLatitude();
                longitude = location.getLongitude();


                if (!selected_location.equals("")) {
                    text_location.setText(selected_location);
                    address = selected_location;
                } else {
                    Log.d("latlng", "networkCallForLatLng1" + latitude + "," + longitude);
                    address = networkCallForLatLng(latitude, longitude);
                    text_location.setText(address);
                }

                PayloadBuilder builder = new PayloadBuilder();
                builder.putAttrLocation("userLocation", latitude, longitude);

                MoEHelper.getInstance(this).trackEvent("lastSeen", builder.build());

                networkCallForHospitals();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    ArrayList<HospitalDetails> hospitals;
    private void networkCallForHospitals() {
        Utils.hideProgressDialog();
        HashMap<String, Object> locationDetails = new HashMap<>();
        Location location;
        location = new Location("");

        try {
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            Log.i("location", latitude + "," + longitude + "-->" + location.getLatitude() + "," + location.getLongitude());
        } catch (Exception e) {
            Utils.hideProgressDialog();
            e.printStackTrace();
        }

        locationDetails.put("emergencyGeoHash", Utils.getGeoHashCode(location));
        locationDetails.put("latitude", latitude);
        locationDetails.put("longitude", longitude);
        locationDetails.put("groupType", "2");
        locationDetails.put("customerApp", true);
        locationDetails.put("ambulanceApp", false);
        locationDetails.put("emergency_type", emergency_id);
        locationDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));
        hospitals = new ArrayList<HospitalDetails>();
        Log.i("location details", Arrays.asList(locationDetails).toString());
        Utils.showProgressDialog(EMSActivity.this);
        if (Utils.isNetworkAvailable(EMSActivity.this)) {
            Call<HospitalList> getHospitalsList = apiInterface.getNearByHospitalsList(locationDetails, preferences.getCustomerId(), preferences.getAccessToken());
//            Utils.showProgressDialog(NearByHospitalsActivity.this);
            getHospitalsList.enqueue(new Callback<HospitalList>() {
                @Override
                public void onResponse(Call<HospitalList> call, Response<HospitalList> response) {
                    Utils.hideProgressDialog();
                    try {
                        if (response.body() != null) {
                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                Log.i("hospitals list", response.body().toString());

                                hospitals = response.body().getResponseData();
                                if(hospitals!=null) {
                                    isHospitalsAvailable = true;
                                    textView_drop_hospital.setText(getResources().getString(R.string.any_near));
                                    textView_drop_hospital.setClickable(true);
                                }else{
                                    isHospitalsAvailable = false;
                                    textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                                    textView_drop_hospital.setClickable(false);
                                }

                            }
                            else{
                                isHospitalsAvailable = false;
                                textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                                textView_drop_hospital.setClickable(false);
                            }
                        }
                        else{
                            isHospitalsAvailable = false;
                            textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                            textView_drop_hospital.setClickable(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }

                }

                @Override
                public void onFailure(Call<HospitalList> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_slow));

                }
            });
        } else {

            Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_error));
            Utils.hideProgressDialog();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("onresume", "------------");

        EsahaiApplication.getApplication().addObserver(this);
        /*try {
            if (!isLocationFetchedFromUser) {
                getLocation();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/


        if (preferences != null) {
            if (preferences.isNoAmbulance() == true) {
                preferences.setNoAmbulance(false);
                showAlertDialog(getResources().getString(R.string.no_ambulance_contact_cc), true);
            }
        }


        if (!Utils.isGPSOn(this)) {
            Utils.turnOnGPS(this);
        } else {
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                    (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                requestPermissionForLocation();
            } else {
                locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, EMSActivity.this);
            }

        }
    }

    @Override
    public void onStop() {
        EsahaiApplication.getApplication().deleteObserver(this);
        super.onStop();
    }

    private void setAdaptersForSpinners() {

        if (emergencyList != null ) {
            EMERGENCY_TYPE = new ArrayList<String>();
            for (int i = 0; i < emergencyList.size(); i++) {
                EMERGENCY_TYPE.add(emergencyList.get(i).getType_name());
            }
            emergency_type_adapter = new ArrayAdapter<String>(EMSActivity.this,
                    android.R.layout.simple_dropdown_item_1line, EMERGENCY_TYPE);
            spinner_emergency.setAdapter(emergency_type_adapter);
        }
        else{
            EMERGENCY_TYPE = new ArrayList<String>();
            EMERGENCY_TYPE.add("Critical Emergency");
            emergency_type_adapter = new ArrayAdapter<String>(EMSActivity.this,
                    android.R.layout.simple_dropdown_item_1line, EMERGENCY_TYPE);
            spinner_emergency.setAdapter(emergency_type_adapter);
        }


        spinnerSelectItems();

    }

    private void spinnerSelectItems() {

        //emergency type spinner
        spinner_emergency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    emergency_type = (String) parent.getItemAtPosition(position);
                    if(emergencyList!=null) {
                        emergency_id = emergencyList.get(position).getId();
                        networkCallForHospitals();
                    }

                }catch (Exception e){}

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void initViews() {

        spinner_emergency = (Spinner) findViewById(R.id.spinner_emergency);
//        spinner_hospital = (Spinner) findViewById(R.id.spinner_hospital);

        textView_drop_hospital = (TextView)findViewById(R.id.textView_drop_hospital);
        text_location = (TextView) findViewById(R.id.current_location);
        textView_drop_location = (TextView)findViewById(R.id.textView_drop_location);

        layout_hospital = (LinearLayout) findViewById(R.id.layout_hospital);
        layout_location = (LinearLayout) findViewById(R.id.location_layout);

        callAmbulanceBtn = (Button) findViewById(R.id.call_ambulance);

        layout_mobile = (LinearLayout) findViewById(R.id.layout_mobile);
        editText_phone_number = (EditText) findViewById(R.id.editText_phone_number);
        layout_main = (LinearLayout) findViewById(R.id.layout_main);

        privateRadioBtn = (RadioButton) findViewById(R.id.private_btn);
        hospitalRadioBtn = (RadioButton) findViewById(R.id.hospital_btn);
        anyRadioBtn = (RadioButton) findViewById(R.id.any_btn);

        radioGroup_destination = (RadioGroup)findViewById(R.id.radioGroup_destination);
        radioButton_location = (RadioButton)findViewById(R.id.radioButton_location);
        radioButton_hospital = (RadioButton)findViewById(R.id.radioButton_hospital);

        radioGroup_destination.setOnCheckedChangeListener(EMSActivity.this);
        layout_location.setVisibility(View.GONE);
        layout_hospital.setVisibility(View.VISIBLE);

        radio_group_ambulance_for = (RadioGroup)findViewById(R.id.radio_group_ambulance_for);
        radio_button_myself = (RadioButton)findViewById(R.id.radio_button_myself);
        radio_button_someone = (RadioButton)findViewById(R.id.radio_button_someone);
        radio_group_ambulance_for.setOnCheckedChangeListener(this);

        textView_drop_hospital.setOnClickListener(this);
        text_location.setOnClickListener(this);
        textView_drop_location.setOnClickListener(this);
        callAmbulanceBtn.setOnClickListener(this);


        editText_phone_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (editText_phone_number.getText().toString().length() == 10) {

                    editText_phone_number.setCursorVisible(false);
                    InputMethodManager in = (InputMethodManager) EMSActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(editText_phone_number.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


                } else {

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });



    }

    private void bookAmbulance() {
        if (Utils.isNetworkAvailable(EMSActivity.this)) {
            /*
            ON SELECTING LOCATION RADIO BUTTON
             */
            if(radioButton_location.isChecked()) {

                isbtnSubmitClicked = false;


                if (is_self == 0) {

                    if (editText_phone_number.getText().toString().length() == 0 ||
                            editText_phone_number.getText().toString().length() < 10) {
                        Utils.showToast(EMSActivity.this, getResources().getString(R.string.valid_mobile_number));
                    } else {

                        try {

                            Log.d("Booking", "'! NoAmbulancesFound'");
                            if (!isbtnSubmitClicked) {

                                isbtnSubmitClicked = true;

                                Log.d("Booking", "Button Submit clicked = " + isbtnSubmitClicked);


                                if (!editText_phone_number.getText().toString().
                                        equalsIgnoreCase("0000000000")) {

                                    if(textView_drop_location.getText().toString().length() == 0 ||
                                            textView_drop_location.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_location))) {
                                        Utils.showToast(EMSActivity.this, getResources().getString(R.string.select_location));
                                    }
                                    else if(text_location.getText().toString().equals(textView_drop_location.getText().toString())){
                                        Utils.showToast(EMSActivity.this,getResources().getString(R.string.same_locations) );
                                    }else {
                                        /*if (textView_drop_hospital.getText().toString().
                                                equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
                                                Utils.showToast(EMSActivity.this,getResources().getString(R.string.select_hospital));
                                                isbtnSubmitClicked = false;
                                        } else {*/
                                        hospitalId = "Any";
                                        hospital = "Any nearby hospital";
                                                facebookEventForBooking();
                                                callNetworkMethod();
//                                        }
                                    }


                                } else {
                                    editText_phone_number.requestFocus();
                                    editText_phone_number.setError(getResources().getString(R.string.valid_mobile_number));
                                    isbtnSubmitClicked = false;
                                }

                            }

                            Log.d("Profile", "ForSomeOne");

                            if (!editText_phone_number.getText().toString().isEmpty()) {
                                Log.d("Profile", "ForSomeOne" + editText_phone_number.getText().toString());
                                preferences.setPatient_MobileNumber(
                                        editText_phone_number.getText().toString());

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } else if (is_self == 1) {


                    try {

                        Log.d("Booking", "'! NoAmbulancesFound'___is_self==1");
                        Log.d("Booking", "isbtnSubmitClicked:::" + isbtnSubmitClicked);
                        if (!isbtnSubmitClicked) {
                            isbtnSubmitClicked = true;

                            Log.d("Booking", "is_self==1___Button Submit clicked = " + isbtnSubmitClicked);

                            if(textView_drop_location.getText().toString().length() == 0 ||
                                    textView_drop_location.getText().toString().equalsIgnoreCase(getResources().getString(R.string.select_location))) {
                                Utils.showToast(EMSActivity.this, getResources().getString(R.string.select_location));
                            }
                            else if(text_location.getText().toString().equals(textView_drop_location.getText().toString())){
                                Utils.showToast(EMSActivity.this,getResources().getString(R.string.same_locations) );
                            }else {
                                /*if (textView_drop_hospital.getText().toString().
                                        equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
                                        Utils.showToast(EMSActivity.this,getResources().getString(R.string.select_hospital));
                                        isbtnSubmitClicked = false;
                                    } else {*/
                                hospitalId = "Any";
                                hospital = "Any nearby hospital";
                                        editText_phone_number.setText("");
                                        facebookEventForBooking();
                                        callNetworkMethod();
//                                    }
                                }

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }else if(radioButton_hospital.isChecked()){

                isbtnSubmitClicked = false;

                /*
            ON SELECTING HOSPITAK RADIO BUTTON
             */

                if (is_self == 0) {

                    if (editText_phone_number.getText().toString().length() == 0 ||
                            editText_phone_number.getText().toString().length() < 10) {
                        Utils.showToast(EMSActivity.this, getResources().getString(R.string.valid_mobile_number));
                    } else {

                        try {

                            if (!isbtnSubmitClicked) {

                                isbtnSubmitClicked = true;

                                if (!editText_phone_number.getText().toString().
                                        equalsIgnoreCase("0000000000")) {

//                                    if (textView_drop_hospital.getText().toString().
//                                            equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
//                                            Utils.showToast(EMSActivity.this,getResources().getString(R.string.select_hospital));
//                                            isbtnSubmitClicked = false;
//                                        }else{
                                    if(textView_drop_hospital.getText().toString().
                                            equals(getResources().getString(R.string.any_near))){
                                        hospitalId = "Any";
                                        hospital = "Any nearby hospital";


                                    }

                                            facebookEventForBooking();
                                            callNetworkMethod();
//                                        }
                                } else {
                                    editText_phone_number.requestFocus();
                                    editText_phone_number.setError(getResources().getString(R.string.valid_mobile_number));
                                    isbtnSubmitClicked = false;
                                }

                            }

                            Log.d("Profile", "ForSomeOne");

                            if (!editText_phone_number.getText().toString().isEmpty()) {
                                Log.d("Profile", "ForSomeOne" + editText_phone_number.getText().toString());
                                preferences.setPatient_MobileNumber(
                                        editText_phone_number.getText().toString());

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } else if (is_self == 1) {


                    try {

                        Log.d("Booking", "'! NoAmbulancesFound'___is_self==1");
                        Log.d("Booking", "isbtnSubmitClicked:::" + isbtnSubmitClicked);
                        if (!isbtnSubmitClicked) {
                            isbtnSubmitClicked = true;

                            Log.d("Booking", "is_self==1___Button Submit clicked = " + isbtnSubmitClicked);

//                            if (textView_drop_hospital.getText().toString().
//                                    equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
//                                    Utils.showToast(EMSActivity.this,getResources().getString(R.string.select_hospital));
//                                    isbtnSubmitClicked = false;
//                                }else{
                            if(textView_drop_hospital.getText().toString().
                                    equals(getResources().getString(R.string.any_near))){
                                hospitalId = "Any";
                                hospital = "Any nearby hospital";


                            }
                                    editText_phone_number.setText("");
                                    facebookEventForBooking();
                                    callNetworkMethod();
//                                }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        } else {
            showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
        }
    }

    private void getAmbulanceType(){
        try {
            if (Utils.getAmbulanceTypes() != null && Utils.getAmbulanceTypes().size() > 0) {
                for (int i = 0; i < Utils.getAmbulanceTypes().size(); i++) {
                    if (Utils.getAmbulanceTypes().get(i).getType_name().equalsIgnoreCase("Any Ambulance")) {
                        group_type_id = Utils.getAmbulanceTypes().get(i).getId();
                    }
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

    private void callNetworkMethod() {

        if (Utils.isNetworkAvailable(this)) {
            //callAmbulanceBtn.setEnabled(false);
            final AppPreferences preference = new AppPreferences(this);
            String token = preference.getAccessToken();
            String encrypted_mobile_number = Utils.encrypt(preference.getMobileNumber());
            String customerId = preference.getCustomerId();
//            convertAddress(text_location.getText().toString());
            networkCallForConvertAddress(text_location.getText().toString());


            Location location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);


            Log.d("Ambulance", "Latitude::::"+drop_latitude +
                    " Longitude::::"+drop_longitude);

            String geoHash = Utils.getGeoHashCode(location);



            HashMap<String, Object> createBooking = new HashMap<>();
            HashMap<String, Object> bookingDetails = new HashMap<>();

            if (preference.getCustomerName() != null && !preference.getCustomerName().equalsIgnoreCase("")) {
                bookingDetails.put("customer_name", preference.getCustomerName());
            } else {
                bookingDetails.put("customer_name", "No Name");
            }

            if (is_self == 0) {
                bookingDetails.put("patient_number", /*preference.getCountryCode()+""+*/editText_phone_number.getText().toString());
            } else {
                bookingDetails.put("patient_number", "");
            }
            bookingDetails.put("is_self", is_self);
            bookingDetails.put("customer_mobile", encrypted_mobile_number);
            bookingDetails.put("booked_by", "Customer");

            bookingDetails.put("emergency_type", emergency_type);
//            bookingDetails.put("emergency_type", "Critical Emergency");

            bookingDetails.put("emergency_longitude", longitude);
            bookingDetails.put("emergency_latitude", latitude);
            bookingDetails.put("emergency_geoHashCode", geoHash);

            if(radioButton_hospital.isChecked()) {
                bookingDetails.put("hospital", hospital);
            }else{
                bookingDetails.put("hospital", textView_drop_location.getText().toString());
            }


            bookingDetails.put("customer_id", customerId);



            Log.d("booking_request", " latitude::" + latitude + " longitude::" + longitude);


//            if(hospital_ambulance == true) {
            Location hospitalLocation = new Location("");
            Location dest_location = new Location("");
            if(!radioButton_hospital.isChecked()){
                createBooking.put("group_id", "Any");
                bookingDetails.put("drop_latitude", drop_latitude);
                bookingDetails.put("drop_longitude", drop_longitude);
                dest_location.setLatitude(drop_latitude);
                dest_location.setLongitude(drop_longitude);
                String dropGeoHash = Utils.getGeoHashCode(dest_location);
                bookingDetails.put("drop_geoHashCode", dropGeoHash);
            }else {

                createBooking.put("group_id", hospitalId);
                createBooking.put("hospital_latitude", hospital_latitude);
                createBooking.put("hospital_longitude", hospital_longitude);
                hospitalLocation.setLatitude(hospital_latitude);
                hospitalLocation.setLongitude(hospital_longitude);
            }
            createBooking.put("emergency_type", emergency_id);


            if (privateRadioBtn.isChecked()){

                group_type_id = "10";

            }
            else if(hospitalRadioBtn.isChecked()){
                group_type_id = "9";
            }
            else{
                group_type_id = "8";
            }
            createBooking.put("group_type_id", group_type_id);


            Log.d("booking", "getUserAge_fromPref"
                    + preferences.getUser_Age());
            //if (is_self == 0) {
            createBooking.put("age", preferences.getUser_Age());
            //}

//            }

            createBooking.put("params", bookingDetails);
            createBooking.put("customerApp", true);
            createBooking.put("address", text_location.getText().toString());
            createBooking.put("gcmToken", FirebaseInstanceId.getInstance().getToken());


            createBooking.put("hospital_geoHashCode", Utils.getGeoHashCode(hospitalLocation));
            createBooking.put("customer_mobile", Utils.encrypt(preference.getMobileNumber()));

            Log.i("booking details", String.valueOf(Arrays.asList(createBooking)));

            Log.d("eSahai", "USER_ID::::" + customerId);
            Log.d("eSahai", "TOKEN::::" + token);


            Call<BookingResponse> booking = apiInterface.createBooking(createBooking, customerId, token);
            Utils.showProgressDialog(this);
            // callAmbulanceBtn.setEnabled(true);

            booking.enqueue(new Callback<BookingResponse>() {
                @Override
                public void onResponse(Call<BookingResponse> call, Response<BookingResponse> response) {
                    Utils.hideProgressDialog();

                    String customerCareNumber;

                    try {
                        if (response.body() != null) {

                            if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {
                                Log.i("create booking response", response.body().toString());
                                preference.setBookingId(response.body().getResponseData().getBooking_id());


                                Log.d("Response", "setCustomercareNumber Response::::" + response.body().getResponseData().getCommandCentreNumber());

                                customerCareNumber = response.body().getResponseData().getCommandCentreNumber();

                                Log.i("create booking response", response.body().toString());
                                if (customerCareNumber != null && !customerCareNumber.equalsIgnoreCase("null")
                                        && !customerCareNumber.isEmpty()) {

                                    preference.setCustomerCareNumber(response.body().getResponseData().getCommandCentreNumber());
                                    Log.d("Response", "setCustomercareNumber Response::::" + response.body().getResponseData().getCommandCentreNumber());

                                }
                                Log.i("create booking response", response.body().toString());
                                EsahaiApplication.getApplication().trackEvent("Dashboard", "Booking-->Booking id",
                                        response.body().getResponseData().getBooking_id());

                                isbtnSubmitClicked = false;
                                Intent ambulance = new Intent(EMSActivity.this, SearchAmbulanceActivity.class);
                                ambulance.putExtra("latitude", latitude);
                                ambulance.putExtra("longitude", longitude);
                                ambulance.putExtra("booking_id", preference.getBookingId());

                                preferences.setMedicalTaxi(false);

                                startActivity(ambulance);

                            } else {

                                Log.d("SUBMIT", "Button submit clicked in " +
                                        "else dialog previous booking---> " + isbtnSubmitClicked);
                                isbtnSubmitClicked = false;
                                Utils.hideProgressDialog();
                                showAlertDialog("" + response.body().getStatusMessage() +
                                        " Do you want to call: "
                                        + preference.getCustomerCareNumber(), true);
                                //Utils.showToast(getActivity(),response.body().getStatusMessage()
                                // +preference.getCustomerCareNumber());

                            }
                        } else {
                            isbtnSubmitClicked = false;
                        }
                    } catch (Exception e) {
                        isbtnSubmitClicked = false;
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<BookingResponse> call, Throwable t) {
                    isbtnSubmitClicked = false;
                    Log.i("error", t.toString());
                    Utils.hideProgressDialog();
                    Utils.showToast(EMSActivity.this, EMSActivity.this.getResources().getString(R.string.network_slow));

                }
            });


        } else {/*Utils.showToast(getActivity(),"Check your Internet Connection");*/
            isbtnSubmitClicked = false;
            showAlertDialog(getResources().getString(R.string.network_error_contact_cc), true);
        }

    }

    private void facebookEventForBooking() {
        try {
            if(preferences.isProd()) {
                AppEventsLogger logger = AppEventsLogger.newLogger(this);
                logger.logEvent(AppEventsConstants.EVENT_NAME_PURCHASED);
            }
        }catch (Exception e){e.printStackTrace();}
//        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if(resultCode == RESULT_OK) {
                if (requestCode == SEARCH_LOCATION) {
                    if (data != null) {

                        //callAmbulanceBtn.setEnabled(false);
                        isLocationFetchedFromUser = true;
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        locationManager.removeUpdates(this);
                        selected_location = (String) data.getExtras().get("location");
                        latitude = data.getExtras().getDouble("latitude");
                        longitude = data.getExtras().getDouble("longitude");
                        Log.d("lat lang", "dash onactivity lat langi = " + latitude + ", " + longitude);
//                        Utils.showProgressDialog(this);
                        if (!selected_location.equals("")) {
                            text_location.setText(selected_location);


//                        networkCallforHospitals();
                        } else {
                            Log.i("entering", "yes");

                            String address = networkCallForLatLng(latitude, longitude);

                            text_location.setText(address);

                        }
                        networkCallForHospitals();
                    }
                } else if (requestCode == DROP_LOCAION) {
                    drop_location = (String) data.getExtras().get("location");
                    drop_latitude = data.getExtras().getDouble("latitude");
                    drop_longitude = data.getExtras().getDouble("longitude");
                    Log.d("Medical_Taxi", "___onActivityResult_drop_latitude::: " + drop_latitude + ", drop_longitude::: " + drop_longitude);
//                    Utils.showProgressDialog(getActivity());
                    if (!drop_location.equals("")) {
                        textView_drop_location.setText(drop_location);
//
                    } else {
                        Log.i("entering", "yes");
                        textView_drop_location.setText("");

                    }

                }
                else if(requestCode == SEARCH_HOSPITAL){
                    hospital_latitude = data.getExtras().getDouble("hospital_latitude");
                    hospital_longitude = data.getExtras().getDouble("hospital_longitude");
                    hospital = data.getExtras().getString("hospital");
                    hospitalId = data.getExtras().getString("hospital_id");
                    textView_drop_hospital.setText(hospital);
                    Log.i("hosp details",hospital+" , "+hospitalId+" , "+hospital_latitude+" , "+hospital_longitude);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String networkCallForLatLng(final double latitude, final double longitude) {

        Log.d("latlng", "address::: lat" + latitude + "long" + longitude);
        if (Utils.isNetworkAvailable(this)) {
            Call<GoogleAddress> getLatLng = apiInterface1.getAddress(latitude + "," + longitude,
                    this.getResources().getString(R.string.geo_coding_key));

            getLatLng.enqueue(new Callback<GoogleAddress>() {
                @Override
                public void onResponse(Call<GoogleAddress> call, Response<GoogleAddress> response) {
                    try {
                        if (response.body() != null && response.body().getStatus().equalsIgnoreCase("OK")) {
                            Log.i("latLng response", response.body().toString());
                            address = response.body().getResults().get(0).getFormatted_address();
                        } else {
                            address = latitude + "," + longitude;
                        }
                        text_location.setText(address);
                        Utils.hideProgressDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<GoogleAddress> call, Throwable t) {

                    Utils.hideProgressDialog();
                    Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_slow));

                }
            });


        } else {
            address = latitude + "," + longitude;
            text_location.setText(address);
            Utils.hideProgressDialog();
            Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_error));
        }
        return address;
    }

    private void networkCallForConvertAddress(String address) {
        Log.d("address 8", "address" + address);
        if (address != null) {
            if (Utils.isNetworkAvailable(this)) {

                Log.i("eSahai", "Internet is on");

                Call<GoogleLatLng> convertAddress = apiInterface1.getLatLng(address,
                        getResources().getString(R.string.geo_coding_key));

                if (convertAddress.toString() != null) {
                    Log.i("eSahai", "convertAddress:::" + convertAddress);
                }

                convertAddress.enqueue(new Callback<GoogleLatLng>() {
                    @Override
                    public void onResponse(Call<GoogleLatLng> call, Response<GoogleLatLng> response) {
                        try {

                            Log.i("eSahai", "IN on_response");

                            if (response.body() != null && response.body().getStatus().equals("OK")) {
                                Log.i("address res", response.body().toString());
                                //latitude = response.body().getResults().get(0).getGeometry().getLocation().getLat();
                                //longitude = response.body().getResults().get(0).getGeometry().getLocation().getLng();
                                Log.i("address latlng", latitude + " , " + longitude);
                            } else {
                                //  Utils.showToast(getActivity(), getActivity().getResources().getString(R.string.network_error));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<GoogleLatLng> call, Throwable t) {

                        Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_slow));

                    }
                });

            } else {
                Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_error));
            }
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.i("location change", "changeeee");
        try {

            // if (text_location.getText().toString().equalsIgnoreCase("Fetching...")) {
            if (!isLocationFetchedFromUser) {
                Log.i("location change", location.getLatitude() + "," + location.getLongitude());
                if (isBetterLocation(location, currentLocation)) {
                    currentLocation = location;
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    Log.d("latlng", "networkCallForLatLng2" + latitude + "," + longitude);
                    text_location.setText(networkCallForLatLng(latitude, longitude));
//                    networkCallforHospitals();
                }
            }
            //  }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onPause() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            requestPermissionForLocation();
        } else {
            try {
                locationManager.removeUpdates(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    private void showAlertDialog(String message, final boolean isNetwork) {
        String yes, no;
        if (isNetwork) {
            yes = getResources().getString(R.string.yes);
            no = getResources().getString(R.string.no);
            Log.d("Dialog", "Dialog_IF set to : " + isNetwork);
        } else {
            yes = "Connect to Customer service";
            no = "Try other options ";
            Log.d("Dialog", "Dialog_ELSE set to : " + isNetwork);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (ActivityCompat.checkSelfPermission(EMSActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            if (preferences.getCustomerCareNumber() != null) {

                                Log.d("CustomerCare", "Number IS::::" + preferences.getCustomerCareNumber());
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + preferences.getCustomerCareNumber()));
                                startActivity(intent);

                            } else {
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + 0));
                                startActivity(intent);
                            }
                            dialog.cancel();
                        } else {
                            requestPermissionForCall();
//                    Toast.makeText(BookingConfirmedActivity.this,"Permission denied",Toast.LENGTH_SHORT).show();
                        }
                    }


                });
        if (isNetwork) {
            builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        }
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestPermissionForCall() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }


    private void getActiveBooking() {


        Log.d("Activebooking", "ActivebookingID");
        try {
            if (Utils.isNetworkAvailable(this)) {
                HashMap<String, Object> bookingDetails = new HashMap<>();
                bookingDetails.put("customer_id", preferences.getCustomerId());
                bookingDetails.put("ambulanceApp", false);
                bookingDetails.put("customerApp", true);
                bookingDetails.put("customer_mobile", Utils.encrypt(preferences.getMobileNumber()));

                Log.i("Activebooking", Arrays.asList(bookingDetails).toString());

                Call<BookingStatus> activeBooking = apiInterface.getActiveBooking(bookingDetails, preferences.getCustomerId(), preferences.getAccessToken());
                activeBooking.enqueue(new Callback<BookingStatus>() {
                    @Override
                    public void onResponse(Call<BookingStatus> call, Response<BookingStatus> response) {
                        try {
                            if (response.body() != null) {
                                Log.i("Activebooking ", "active booking response!=NULL" + response.body().toString());

                                if (Integer.parseInt(response.body().getStatusCode()) == Constants.SUCCESS_CODE) {


                                    preferences.setBookingDetails(response.body().getResponseData());

                                    preferences.setBookingId(response.body().getResponseData().booking_id);

                                    preferences.setHospitalLat(response.body().getResponseData().hospital_latitude);
                                    preferences.setHospitalLng(response.body().getResponseData().hospital_longitude);

                                    /*if(response.body().getResponseData().booking_status.
                                            equalsIgnoreCase("Way Back to Hospital")){
                                        preferences.setBookingCancellable(false);
                                        Log.d("ActiveBooking","Way back to hospital");
                                    }else{
                                        preferences.setBookingCancellable(true);
                                    }*/

                                    Utils.setAmbulanceStatus(response.body().getResponseData().booking_status);

                                    Intent intent = new Intent(EMSActivity.this, TrackAmbulanceActivity.class);
                                    intent.putExtra("bookingDetails", response.body().getResponseData());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);

                                } else {

                                    Log.d("booking", "status_message" + response.body().getStatusMessage());
                                    //Utils.showToast(getActivity(),response.body().getStatusMessage());

                                }

                            } else {
                                //Toast.makeText(getActivity(), response.body().getStatusMessage(), Toast.LENGTH_SHORT).show();
                                Log.d("Activebooking", "Activebooking_Error" + response.body().getStatusMessage());

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<BookingStatus> call, Throwable t) {
                        Utils.hideProgressDialog();
                        Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_slow));

                    }
                });


            } else {
                Log.d("Activebooking", "Activebooking_No internet");
                Utils.showToast(EMSActivity.this, getResources().getString(R.string.network_error));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // checking is better location
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {

        Log.d("Location_updates", "currentBestLocation" + currentBestLocation);
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > timeDifference;
        boolean isSignificantlyOlder = timeDelta < -timeDifference;
        boolean isNewer = timeDelta > 0;


       /* Log.d("Location_updates", "isSignificantlyNewer" + isSignificantlyNewer);//false
        Log.d("Location_updates", "isSignificantlyOlder" + isSignificantlyOlder);//false
        Log.d("Location_updates", "isNewer" + isNewer);   //true
        Log.d("Location_updates", "timeDelta" + timeDelta); //   19986   */


        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }



        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta >= 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 40;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        /*Log.d("Location_updates", "accuracyDelta" + accuracyDelta);//0
        Log.d("Location_updates", "isLessAccurate" + isLessAccurate);  //false
        Log.d("Location_updates", "isMoreAccurate" + isMoreAccurate); //false
        Log.d("Location_updates", "isSignificantlyLessAccurate" + isSignificantlyLessAccurate);  //false
        Log.d("Location_updates", "isFromSameProvider" + isFromSameProvider);      //true

        */
        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            //Log.d("Location_updates", "IN_isMoreAccurate");
            return false;
        } else if (isNewer && !isLessAccurate) {
            //Log.d("Location_updates", "IN_isNewer && !isLessAccurate");
            return false;
        } else if (isNewer && isSignificantlyLessAccurate && isFromSameProvider) {
            Log.d("Location_updates", "IN_isNewer && !isSignificantlyLessAccurate && isFromSameProvider");
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    @Override
    public void update(int event, Object arg) {

        if (event == EsahaiApplication.bookingStatusUpdate) {
            getActiveBooking();
        }
        if (event == Constants.DISPLAY_NOTIFICATION) {
            PromoDetails promoDetails = (PromoDetails) arg;
            showDialogForBanner(promoDetails.getBanner_path(), promoDetails.getTitle(), promoDetails.getDescription());
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        try {
            if (group == radioGroup_destination){
                switch (checkedId) {
                    case R.id.radioButton_location:
                        layout_location.setVisibility(View.VISIBLE);
                        layout_hospital.setVisibility(View.GONE);
//                        textView_drop_hospital.setText(getResources().getString(R.string.any_near));
                        if(isHospitalsAvailable){
                            textView_drop_hospital.setText(getResources().getString(R.string.any_near));
                            textView_drop_hospital.setClickable(true);
                        }else{
                            textView_drop_hospital.setText(getResources().getString(R.string.no_nearby_hospitals));
                            textView_drop_hospital.setClickable(false);
                        }
                        break;
                    case R.id.radioButton_hospital:
//                        networkCallforHospitals();
                        layout_location.setVisibility(View.GONE);
                        layout_hospital.setVisibility(View.VISIBLE);
                        textView_drop_location.setText(getResources().getString(R.string.select_location));
                        break;
                }
            }
            else if(group == radio_group_ambulance_for){
                switch (checkedId){
                    case R.id.radio_button_myself:
                        editText_phone_number.setVisibility(View.GONE);
                        is_self = 1;
                        preferences.setIsSelf(true);
                        editText_phone_number.setText("");
                        try{
                            Utils.closeKeyBoard(EMSActivity.this,editText_phone_number);
                        }catch (Exception e){e.printStackTrace();}
                        break;
                    case R.id.radio_button_someone:
                        editText_phone_number.setVisibility(View.VISIBLE);
                        is_self = 0;
                        preferences.setIsSelf(false);
                        break;
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.current_location:
                if (!Utils.isGPSOn(EMSActivity.this)) {
                    Utils.turnOnGPS(EMSActivity.this);
                } else {
                    Intent i = new Intent(EMSActivity.this, MapActivity.class);
                    startActivityForResult(i, SEARCH_LOCATION);
                }
                break;
            case R.id.textView_drop_location:
                if (!Utils.isGPSOn(EMSActivity.this)) {
                    Utils.turnOnGPS(EMSActivity.this);
                } else {
                    Intent i = new Intent(EMSActivity.this, MapActivity.class);
                    startActivityForResult(i, DROP_LOCAION);
                }
                break;
            case R.id.textView_drop_hospital:
                if (!Utils.isGPSOn(EMSActivity.this)) {
                    Utils.turnOnGPS(EMSActivity.this);
                } else {
                    Intent i = new Intent(EMSActivity.this, NearestHospitalsActivity.class);
                    i.putExtra("pickUpLatitude",latitude);
                    i.putExtra("pickUpLongitude",longitude);
                    i.putExtra("emergencyType",emergency_id);
                    startActivityForResult(i, SEARCH_HOSPITAL);
                }
                break;
            case R.id.call_ambulance:

                if (!text_location.getText().toString().equalsIgnoreCase("") &&
                        !text_location.getText().toString().
                                equalsIgnoreCase(getResources().getString(R.string.pickup_location))
                        &&text_location.getText().toString() != null
                        &&!text_location.getText().toString().isEmpty()) {

                    if (radioButton_hospital.isChecked()) {
                        if (isHospitalsAvailable) {
                            if (textView_drop_hospital.getText().toString().
                                    equalsIgnoreCase(getResources().getString(R.string.select_hospital))) {
                                Utils.showToast(EMSActivity.this, getResources().getString(R.string.select_hospital));
                            } else {
                                bookAmbulance();
                            }
                        } else {
                            showAlertDialog(getResources().getString(R.string.no_hospitals), true);
                        }
                    } else {
                        bookAmbulance();
                    }
                }
                else{

                    Utils.showToast(EMSActivity.this,getResources().getString(R.string.pickup_location_error));
                }

                break;
        }
    }
}
