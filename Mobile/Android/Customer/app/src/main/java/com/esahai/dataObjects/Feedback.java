package com.esahai.dataObjects;

import java.io.Serializable;

/**
 * Created by ragamai on 9/11/16.
 */
public class Feedback implements Serializable{

    /*
    "id": "34",
    "points": "3",
    "reason": "good"
     */

    public String id;
    public String points;
    public String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id='" + id + '\'' +
                ", points='" + points + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
