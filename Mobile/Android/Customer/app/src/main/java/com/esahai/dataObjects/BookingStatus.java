package com.esahai.dataObjects;

/**
 * Created by adheesh on 07/12/16.
 */

public class BookingStatus {

    public BookingConfirmedDetails responseData;
    public String statusCode;
    public String statusMessage;

    public BookingConfirmedDetails getResponseData() {
        return responseData;
    }

    public void setResponseData(BookingConfirmedDetails responseData) {
        this.responseData = responseData;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }


    @Override
    public String toString() {
        return "BookingDetailsRequest{" +
                "responseData=" + responseData +
                ", statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }

}
