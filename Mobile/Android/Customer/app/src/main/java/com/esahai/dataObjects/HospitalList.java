package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 3/10/16.
 */
public class HospitalList {
    /*
    "statusCode": "300",
	"statusMessage": "Hospital List fetched successfully"
     */

    public String statusCode;
    public String statusMessage;
    public ArrayList<HospitalDetails> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<HospitalDetails> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<HospitalDetails> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "HospitalList{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
