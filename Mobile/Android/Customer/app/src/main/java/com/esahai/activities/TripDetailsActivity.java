package com.esahai.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.esahai.Extras.EsahaiApplication;
import com.esahai.R;
import com.esahai.Extras.Utils;
import com.esahai.dataObjects.EmergencyDetails;
import com.esahai.preferences.AppPreferences;
import com.esahai.webServices.ApiClient;
import com.esahai.webServices.ApiInterface;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ragamai on 15/9/16.
 */
public class TripDetailsActivity extends CommonActivity implements View.OnClickListener{

    Button button_done;
    TextView text_total_distance,text_trip_time,text_avg_speed,text_trip_cost;
    AppPreferences preference;
    ApiInterface apiService;
    EmergencyDetails emergencyDetails;
    WebView webView_map;
    boolean fromPN = false;
    int rating;
    String comment;
    /*
    https://maps.googleapis.com/maps/api/staticmap?zoom=10&size=600x300&maptype=roadmap&markers=color:green|17.56,78.45&markers=color:red|17.87,78.12&key=AIzaSyBVU8VkJ5QUqhX8BJDsgPL8R3Ua7xffFkw
     */


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_trip_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtTitle = (TextView) findViewById(R.id.tv_toolbartitle);
        txtTitle.setText(getResources().getString(R.string.trip_details));

        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        text_total_distance = (TextView)findViewById(R.id.text_total_distance);
        text_trip_time = (TextView)findViewById(R.id.text_trip_time);
        text_avg_speed = (TextView)findViewById(R.id.text_avg_speed);
        text_trip_cost = (TextView)findViewById(R.id.text_trip_cost);
        button_done = (Button)findViewById(R.id.button_done);
        webView_map = (WebView)findViewById(R.id.webView_map);
        button_done.setOnClickListener(this);

        preference = new AppPreferences(TripDetailsActivity.this);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        try {
            if (getIntent().hasExtra("trip_details")) {
                fromPN = false;
                emergencyDetails = (EmergencyDetails) getIntent().getExtras().getSerializable("trip_details");
                setData();
            }
            else{
                fromPN = true;
                emergencyDetails = (EmergencyDetails) getIntent().getExtras().getSerializable("trip_closed_details");
                setData();

            }
        }catch (Exception e){e.printStackTrace();}


        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (fromPN == false) {
                        finish();
                    } else {
                        Intent i = new Intent(TripDetailsActivity.this, DashboardActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                        cancelNotifications();
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        });

    }

    private void feedbackDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TripDetailsActivity.this)
                .setTitle(getResources().getString(R.string.feedback_txt));
        final FrameLayout frameView = new FrameLayout(TripDetailsActivity.this);
        builder.setView(frameView);

        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.feedback, frameView);
        Button button_submit = (Button)dialoglayout.findViewById(R.id.button_submit);
        Button button_cancel = (Button)dialoglayout.findViewById(R.id.button_cancel);
        final RatingBar ratingBar = (RatingBar)dialoglayout.findViewById(R.id.ratingBar);
        final EditText editText_comment = (EditText)dialoglayout.findViewById(R.id.editText_comment);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.button_color), PorterDuff.Mode.SRC_ATOP);

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rating= (int) ratingBar.getRating();
                comment = editText_comment.getText().toString();
                if(rating != 0){

                    sendFeedBack();
                    alertDialog.dismiss();

                    Intent i = new Intent(TripDetailsActivity.this, DashboardActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                    cancelNotifications();


                }
                else{Utils.showToast(TripDetailsActivity.this,getResources().getString(R.string.give_rating));}
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent i = new Intent(TripDetailsActivity.this, DashboardActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                cancelNotifications();
            }
        });

        alertDialog.show();
    }

    private void sendFeedBack() {
        if(Utils.isNetworkAvailable(TripDetailsActivity.this)){
            HashMap<String,Object> request = new HashMap<>();
            //points,booking_id,customer_id
            request.put("points",rating);
            request.put("booking_id",preference.getBookingId()/*emergencyDetails.getBooking_id()*/);
            request.put("customer_id",preference.getCustomerId());
            request.put("comment",comment);
            request.put("customerApp",true);
            request.put("customer_mobile",Utils.encrypt(preference.getMobileNumber()));


            Log.d("feedback","preffff::"+preference.getMobileNumber());
            Log.d("feedback","Encrypted::"+Utils.encrypt(preference.getMobileNumber()));
            Log.i("feedback request",Arrays.asList(request).toString());

            Call<ResponseBody> sendFeedBack = apiService.sendFeedback(request,preference.getCustomerId(),preference.getAccessToken());
//            Utils.showProgressDialog(TripDetailsActivity.this);
            sendFeedBack.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    Utils.hideProgressDialog();
                    try{
                        if(response.body()!=null){
                            Log.i("feedback response",response.body().string());
                            EsahaiApplication.getApplication().trackEvent("Send feedback", "Feedback on the trip", "Feedback sent successfully");
                        }
                    }catch (Exception e){e.printStackTrace();}
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Utils.hideProgressDialog();
                }
            });
        }
    }

    /*private void networkCallForTripDetails() {
        if(Utils.isNetworkAvailable(TripDetailsActivity.this)){

            *//*
            {
              "booking_id": 122264,
              "customerApp": true
            }
             *//*
            HashMap<String, Object> request = new HashMap<>();
            request.put("booking_id",bookingId);
            request.put("customerApp",true);

            Call<ResponseBody> tripDetails = apiService.getTripDetails(request,preference.getCustomerId(),preference.getAccessToken());
            Utils.showProgressDialog(TripDetailsActivity.this);
            tripDetails.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Utils.hideProgressDialog();
                    try {
                        if(response.body()!=null) {
                            Log.i("trip details", response.body().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Utils.hideProgressDialog();
                    Utils.showToast(TripDetailsActivity.this, t.toString());
                }
            });

        }else{Utils.showToast(TripDetailsActivity.this,getResources().getString(R.string.network_error));}
    }*/

    private void setData() throws UnsupportedEncodingException {
        if(emergencyDetails!=null){
            if(emergencyDetails.getDistance()!=null) {
                text_total_distance.setText(emergencyDetails.getDistance() + " Km");
            }else{
                text_total_distance.setText("0 Km");
            }

            if(emergencyDetails.getDuration()!=null) {
                text_trip_time.setText(emergencyDetails.getDuration());
            }else{
                text_trip_time.setText("0 sec");
            }

            if((emergencyDetails.getCost()!=null)) {
                text_trip_cost.setText(getResources().getString(R.string.Rs)+ emergencyDetails.getCost());
            }else{
                text_trip_cost.setText(getResources().getString(R.string.Rs)+ 0);
            }
            if(emergencyDetails.getSpeed()!=null){
                text_avg_speed.setText(emergencyDetails.getSpeed());
            }

            try {
                if(emergencyDetails.getDuration()!=null) {
                    long millis = Long.parseLong(emergencyDetails.getDuration());

                    String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                            TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                            TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                    text_trip_time.setText("" + hms);
                }

                if(emergencyDetails.getDistance()!=null){

                    double roundedDistance = Math.round( (Double.parseDouble(emergencyDetails.getDistance())/1000) * 1000.0 ) / 1000.0;
                    //text_total_distance.setText((Double.parseDouble(emergencyDetails.getDistance())/1000)+" Kms");
                    text_total_distance.setText(roundedDistance+" Kms");

                }

                if(emergencyDetails.getSpeed()!=null){
                    text_avg_speed.setText(emergencyDetails.getSpeed());
                }


            }
            catch (Exception e){e.printStackTrace();}


           /* //set map image
            String origin = emergencyDetails.getEmergency_latitude()+","+emergencyDetails.getEmergency_longitude();
            String path = "weight:3|color:blue|geodesic:true|"+origin+"|17.4948,78.3996";
            path = URLEncoder.encode(path, "UTF-8");
            String url = "https://maps.googleapis.com/maps/api/staticmap?size=300x200&path="+path+"&maptype=roadmap&markers=color:green|"+origin+"|&markers=color:red|17.4948,78.3996&key="+getResources().getString(R.string.static_maps_key);
            webView_map.loadUrl(url);
            Log.i("map url",url);*/


        }

    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            if(fromPN == false){
                finish();
            }else {
                Intent i = new Intent(TripDetailsActivity.this, DashboardActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                cancelNotifications();
            }
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void cancelNotifications(){
        try {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_done){

            if(fromPN == false){
                finish();
            }else {
                feedbackDialog();

            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
