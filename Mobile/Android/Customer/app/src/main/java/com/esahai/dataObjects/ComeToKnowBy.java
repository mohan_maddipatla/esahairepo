package com.esahai.dataObjects;

import java.util.ArrayList;

/**
 * Created by ragamai on 31/1/17.
 */
public class ComeToKnowBy {

    /*
    "statusCode": "300",
    "responseData"
     */

    public String statusCode;
    public ArrayList<KnowBy> responseData;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ArrayList<KnowBy> getResponseData() {
        return responseData;
    }

    public void setResponseData(ArrayList<KnowBy> responseData) {
        this.responseData = responseData;
    }

    @Override
    public String toString() {
        return "ComeToKnowBy{" +
                "statusCode='" + statusCode + '\'' +
                ", responseData=" + responseData +
                '}';
    }
}
