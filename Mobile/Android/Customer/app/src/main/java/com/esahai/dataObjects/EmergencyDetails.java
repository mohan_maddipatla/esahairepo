package com.esahai.dataObjects;

import java.io.Serializable;

/**
 * Created by ragamai on 10/10/16.
 */
public class EmergencyDetails implements Serializable {

    /*
    "booking_id": "761698",
 				"customer_mobile": "8686544692",
 				"customer_name": "ragamai",
 				"booked_by": "Customer",
 				"emergency_type": "Critical Emergency",
 				"emergency_longitude": "78.3852503",
 				"emergency_latitude": "17.4431423",
 				"ambulance_id": "6",
 				"hospital": "Any Nearby Hospital",
 				"ambulance_start_longitude": "78.3860447",
 				"ambulance_start_latitude": "17.4440289",
 				"created_datetime": "2016-11-09 06:01:05",
 				"updated_datetime": "2016-11-09 06:01:20",
 				"distance": "undefined",
 				"duration": "undefined",
 				"cost": "undefined",
 				"emergency_geoHashCode": "tepepxqmstqu",
 				"ambulance_start_geoHashCode": "tepepxwfz4xm",
 				"booking_status": "Trip Closed",
 				"customer_id": "66056056",
 				"eme_address": "Vittal Rao Nagar Rd, Vittal Rao Nagar, HITEC City, Hyderabad, Telangana 500081, India",
 				"ambulance_number": "TS07AB0001",
 				"group_type_id": "25",
 				"group_id": "Any",
 				"emergency_type_id": "1",
 				"driver_profile": "http://dev.esahai.in/drivers/driver-1478257833770.*"
     */

    public String booking_id;
    public String customer_mobile;
    public String customer_name;
    public String booked_by;
    public String emergency_type;
    public String emergency_longitude;
    public String emergency_latitude;
    public String ambulance_id;
    public String hospital;

    public String ambulance_start_longitude;
    public String ambulance_start_latitude;
    public String created_datetime;
    public String updated_datetime;
    public String distance;
    public String duration;
    public String cost;
    public String emergency_geoHashCode;
    public String ambulance_start_geoHashCode;

    public String booking_status;
    public String customer_id;
    public String eme_address;
    public String ambulance_number;
    public String group_id;
    public String emergency_type_id;
    public String driver_profile;
    public Feedback feedback;

    public String hospital_latitude;
    public String hospital_longitude;
    public String hospital_geoHashCode;
    public String driver_name;
    public String speed;

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getBooked_by() {
        return booked_by;
    }

    public void setBooked_by(String booked_by) {
        this.booked_by = booked_by;
    }

    public String getEmergency_type() {
        return emergency_type;
    }

    public void setEmergency_type(String emergency_type) {
        this.emergency_type = emergency_type;
    }

    public String getEmergency_longitude() {
        return emergency_longitude;
    }

    public void setEmergency_longitude(String emergency_longitude) {
        this.emergency_longitude = emergency_longitude;
    }

    public String getEmergency_latitude() {
        return emergency_latitude;
    }

    public void setEmergency_latitude(String emergency_latitude) {
        this.emergency_latitude = emergency_latitude;
    }

    public String getAmbulance_id() {
        return ambulance_id;
    }

    public void setAmbulance_id(String ambulance_id) {
        this.ambulance_id = ambulance_id;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getAmbulance_start_longitude() {
        return ambulance_start_longitude;
    }

    public void setAmbulance_start_longitude(String ambulance_start_longitude) {
        this.ambulance_start_longitude = ambulance_start_longitude;
    }

    public String getAmbulance_start_latitude() {
        return ambulance_start_latitude;
    }

    public void setAmbulance_start_latitude(String ambulance_start_latitude) {
        this.ambulance_start_latitude = ambulance_start_latitude;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getUpdated_datetime() {
        return updated_datetime;
    }

    public void setUpdated_datetime(String updated_datetime) {
        this.updated_datetime = updated_datetime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getEmergency_geoHashCode() {
        return emergency_geoHashCode;
    }

    public void setEmergency_geoHashCode(String emergency_geoHashCode) {
        this.emergency_geoHashCode = emergency_geoHashCode;
    }

    public String getAmbulance_start_geoHashCode() {
        return ambulance_start_geoHashCode;
    }

    public void setAmbulance_start_geoHashCode(String ambulance_start_geoHashCode) {
        this.ambulance_start_geoHashCode = ambulance_start_geoHashCode;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getEmergency_type_id() {
        return emergency_type_id;
    }

    public void setEmergency_type_id(String emergency_type_id) {
        this.emergency_type_id = emergency_type_id;
    }

    public String getDriver_profile() {
        return driver_profile;
    }

    public void setDriver_profile(String driver_profile) {
        this.driver_profile = driver_profile;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public String getEme_address() {
        return eme_address;
    }

    public void setEme_address(String eme_address) {
        this.eme_address = eme_address;
    }

    public String getAmbulance_number() {
        return ambulance_number;
    }

    public void setAmbulance_number(String ambulance_number) {
        this.ambulance_number = ambulance_number;
    }

    public String getHospital_latitude() {
        return hospital_latitude;
    }

    public void setHospital_latitude(String hospital_latitude) {
        this.hospital_latitude = hospital_latitude;
    }

    public String getHospital_geoHashCode() {
        return hospital_geoHashCode;
    }

    public void setHospital_geoHashCode(String hospital_geoHashCode) {
        this.hospital_geoHashCode = hospital_geoHashCode;
    }

    public String getHospital_longitude() {
        return hospital_longitude;
    }

    public void setHospital_longitude(String hospital_longitude) {
        this.hospital_longitude = hospital_longitude;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "EmergencyDetails{" +
                "booking_id='" + booking_id + '\'' +
                ", customer_mobile='" + customer_mobile + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", booked_by='" + booked_by + '\'' +
                ", emergency_type='" + emergency_type + '\'' +
                ", emergency_longitude='" + emergency_longitude + '\'' +
                ", emergency_latitude='" + emergency_latitude + '\'' +
                ", ambulance_id='" + ambulance_id + '\'' +
                ", hospital='" + hospital + '\'' +
                ", ambulance_start_longitude='" + ambulance_start_longitude + '\'' +
                ", ambulance_start_latitude='" + ambulance_start_latitude + '\'' +
                ", created_datetime='" + created_datetime + '\'' +
                ", updated_datetime='" + updated_datetime + '\'' +
                ", distance='" + distance + '\'' +
                ", duration='" + duration + '\'' +
                ", cost='" + cost + '\'' +
                ", emergency_geoHashCode='" + emergency_geoHashCode + '\'' +
                ", ambulance_start_geoHashCode='" + ambulance_start_geoHashCode + '\'' +
                ", booking_status='" + booking_status + '\'' +
                ", customer_id='" + customer_id + '\'' +
                ", eme_address='" + eme_address + '\'' +
                ", ambulance_number='" + ambulance_number + '\'' +
                ", group_id='" + group_id + '\'' +
                ", emergency_type_id='" + emergency_type_id + '\'' +
                ", driver_profile='" + driver_profile + '\'' +
                ", feedback=" + feedback +
                ", hospital_latitude='" + hospital_latitude + '\'' +
                ", hospital_longitude='" + hospital_longitude + '\'' +
                ", hospital_geoHashCode='" + hospital_geoHashCode + '\'' +
                ", driver_name='" + driver_name + '\'' +
                ", speed='" + speed + '\'' +
                '}';
    }
}
