package com.esahai.dataObjects;

/**
 * Created by ragamai on 30/9/16.
 */
public class BookingDetails {

   /*
   "booking_id": "382715",
		"customer_mobile": "8686544692",
		"customer_name": "Ragamai",
		"booked_by": "Customer",
		"emergency_type": "Critical Emergency",
		"emergency_longitude": "78.3858983",
		"emergency_latitude": "17.4441168",
		"ambulance_id": null,
		"hospital": "Any near hospital",
		"ambulance_start_longitude": null,
		"ambulance_start_latitude": null,
		"created_datetime": "2016-09-30 14:26:43",
		"updated_datetime": null,
		"distance": null,
		"duration": null,
		"cost": null,
		"emergency_geoHashCode": "tepepxwg7g41",
		"ambulance_start_geoHashCode": null
    */


    public String booking_id;
    public String customer_mobile;
    public String customer_name;
    public String booked_by;
    public String emergency_type;
    public double emergency_longitude;
    public double emergency_latitude;
    public String ambulance_id;
    public String hospital;
    public double ambulance_start_longitude;
    public double ambulance_start_latitude;
    public String created_datetime;
    public String updated_datetime;
    public String distance;
    public String duration;
    public String cost;
    public String emergency_geoHashCode;
    public String ambulance_start_geoHashCode;
    public String commandCentreNumber;//commandCentreNumber


    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getBooked_by() {
        return booked_by;
    }

    public void setBooked_by(String booked_by) {
        this.booked_by = booked_by;
    }

    public String getEmergency_type() {
        return emergency_type;
    }

    public void setEmergency_type(String emergency_type) {
        this.emergency_type = emergency_type;
    }

    public double getEmergency_longitude() {
        return emergency_longitude;
    }

    public void setEmergency_longitude(double emergency_longitude) {
        this.emergency_longitude = emergency_longitude;
    }

    public double getEmergency_latitude() {
        return emergency_latitude;
    }

    public void setEmergency_latitude(double emergency_latitude) {
        this.emergency_latitude = emergency_latitude;
    }

    public String getAmbulance_id() {
        return ambulance_id;
    }

    public void setAmbulance_id(String ambulance_id) {
        this.ambulance_id = ambulance_id;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public double getAmbulance_start_longitude() {
        return ambulance_start_longitude;
    }

    public void setAmbulance_start_longitude(double ambulance_start_longitude) {
        this.ambulance_start_longitude = ambulance_start_longitude;
    }

    public double getAmbulance_start_latitude() {
        return ambulance_start_latitude;
    }

    public void setAmbulance_start_latitude(double ambulance_start_latitude) {
        this.ambulance_start_latitude = ambulance_start_latitude;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getUpdated_datetime() {
        return updated_datetime;
    }

    public void setUpdated_datetime(String updated_datetime) {
        this.updated_datetime = updated_datetime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getEmergency_geoHashCode() {
        return emergency_geoHashCode;
    }

    public void setEmergency_geoHashCode(String emergency_geoHashCode) {
        this.emergency_geoHashCode = emergency_geoHashCode;
    }

    public String getAmbulance_start_geoHashCode() {
        return ambulance_start_geoHashCode;
    }

    public void setAmbulance_start_geoHashCode(String ambulance_start_geoHashCode) {
        this.ambulance_start_geoHashCode = ambulance_start_geoHashCode;
    }

    public String getCommandCentreNumber() {
        return commandCentreNumber;
    }

    public void setCommandCentreNumber(String commandCentreNumber) {
        this.commandCentreNumber = commandCentreNumber;
    }

    @Override
    public String toString() {
        return "BookingDetails{" +
                "booking_id='" + booking_id + '\'' +
                ", customer_mobile='" + customer_mobile + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", booked_by='" + booked_by + '\'' +
                ", emergency_type='" + emergency_type + '\'' +
                ", emergency_longitude=" + emergency_longitude +
                ", emergency_latitude=" + emergency_latitude +
                ", ambulance_id='" + ambulance_id + '\'' +
                ", hospital='" + hospital + '\'' +
                ", ambulance_start_longitude=" + ambulance_start_longitude +
                ", ambulance_start_latitude=" + ambulance_start_latitude +
                ", created_datetime='" + created_datetime + '\'' +
                ", updated_datetime='" + updated_datetime + '\'' +
                ", distance='" + distance + '\'' +
                ", duration='" + duration + '\'' +
                ", cost='" + cost + '\'' +
                ", emergency_geoHashCode='" + emergency_geoHashCode + '\'' +
                ", ambulance_start_geoHashCode='" + ambulance_start_geoHashCode + '\'' +
                ", commandCentreNumber='" + commandCentreNumber + '\'' +
                '}';
    }
}
